%module proshade

%{
#define SWIG_FILE_WITH_INIT
#include <vector>
#include <string>
#include <memory>
#include "Python.h"
#include "ProSHADE.h"
%}

%include "std_string.i"
%include "std_vector.i"
%include "std_shared_ptr.i"
%include "stl.i"
%include "typemaps.i"

namespace std 
{
    %template( StringList     )   vector< string >;
    %template( VecDouble      )   vector< double >;
    %template( VecVecDouble   )   vector<vector<double> >;
}

%include "ProSHADE.h" 
