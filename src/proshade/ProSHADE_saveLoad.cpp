/*! \file ProSHADE_saveLoad.cpp
 \brief This file contains functions required for saving and reading from the database of structures.
 
 This file contains the functions required to pre-compute and save any number of files into a binary
 database file as well as loading these back and constructing the ProSHADE_settings and ProSHADE_data
 objects back from the database file. This allows for the final functionality implemented here to take
 place; that is to search a single structure against the whole database.
 
 This file is part of the ProSHADE library for calculating
 shape descriptors and symmetry operators of protein structures.
 This is a prototype code, which is by no means complete or fully
 tested. Its use is at your own risk only. There is no quarantee
 that the results are correct.
 
 \author    Michal Tykac
 \author    Garib N. Murshudov
 \version   0.6.6
 \date      JAN 2019
 */

//============================================ RVAPI
#include <rvapi_interface.h>

//============================================ ProSHADE
#include "ProSHADE.h"
#include "ProSHADE_internal.h"
#include "ProSHADE_misc.h"

/*! \brief Function responsible for saving the structure database.
 
 This function saves all the settings into a binary file and then proceeds to compute the spherical harmonics decomposition
 using these same settings. Consequently, it saves the SH coefficients for all supplied structures as well as the pre-computed
 RRP matrices (first descriptor requirements) into the same binary file. This file now becomes the 'database' of structures
 against which other structures can be compared without the need for computing the SH and RRP again and again.
 
 \param[in] settings The proper settings to be applied and saved with the database.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_distances::saveDatabase ( ProSHADE::ProSHADE_settings *settings )
{
    //======================================== Sanity checks
    if ( settings->databaseName == "" )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Tried to build a database of files, but the filename to which it should be saved is empty. Terminating ..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Missing the database name for building database." << "</font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }
        
        exit ( -1 );
    }
    
    if ( settings->structFiles.size() < 1 )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Tried to build a database of files, but the supplied no files. Terminating ..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "There are no files to be saved into the database." << "</font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }
        
        exit ( -1 );
    }
    
    if ( settings->verbose > 3 )
    {
        std::cout << ">> Sanity check passed." << std::endl;
    }
    
    //======================================== Open file for output
    std::ofstream dbFile                      ( settings->databaseName, std::ios::out | std::ios::binary);

    //======================================== Check file for output
    if ( dbFile.fail () )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Canno open the database file " << settings->databaseName << " . Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Cannot open database with name " << settings->databaseName << ". Could you have no permissions to write to the requested database location?" << "</font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Save the settings
    this->mapResolution                       = settings->mapResolution;
    this->bandwidth                           = settings->bandwidth;
    this->glIntegOrder                        = settings->glIntegOrder;
    this->theta                               = settings->theta;
    this->phi                                 = settings->phi;
    this->bFactorValue                        = settings->bFactorValue;
    this->bFactorChange                       = settings->bFactorChange;
    this->noIQRsFromMap                       = settings->noIQRsFromMap;
    this->shellSpacing                        = settings->shellSpacing;
    this->manualShells                        = settings->manualShells;
    this->useCOM                              = settings->useCOM;
    this->firstLineCOM                        = settings->firstLineCOM;
    this->extraSpace                          = settings->extraSpace;
    this->alpha                               = settings->alpha;
    this->mPower                              = settings->mPower;
    this->ignoreLs                            = settings->ignoreLs;
    this->energyLevelDist                     = settings->energyLevelDist;
    this->traceSigmaDist                      = settings->traceSigmaDist;
    this->fullRotFnDist                       = settings->fullRotFnDist;
    this->usePhase                            = settings->usePhase;
    this->saveWithAndWithout                  = settings->saveWithAndWithout;
    this->enLevelsThreshold                   = settings->enLevelsThreshold;
    this->trSigmaThreshold                    = settings->trSigmaThreshold;
    this->structFiles                         = settings->structFiles;
    
    if ( settings->htmlReport )
    {
        //==================================== Create section
        rvapi_add_section                     ( "DBSettingsSection",
                                                "Database Settings",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                false );
        settings->htmlReportLine             += 1;
        
        std::stringstream hlpSS;
        hlpSS << "<pre>" << "Map resolution: ";
        int hlpIt                             = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        std::stringstream hlpSS2;
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->mapResolution * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "DBSettingsSection",
                                                0,
                                                0,
                                                1,
                                                1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Bandwidth: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->bandwidth == 0 ) { hlpSS2 << "  AUTO"; }
        else { hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->bandwidth * 1000.0 ) / 1000.0; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               1,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Gauss-Legendre Integration order: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->glIntegOrder == 0 ) { hlpSS2 << "  AUTO"; }
        else { hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->glIntegOrder * 1000.0 ) / 1000.0; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               2,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Theta angle sampling: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->theta == 0 ) { hlpSS2 << "  AUTO"; }
        else { hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->theta * 1000.0 ) / 1000.0; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               3,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Phi angle sampling: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->phi == 0 ) { hlpSS2 << "  AUTO"; }
        else { hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->phi * 1000.0 ) / 1000.0; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               4,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Set all PDB file B-factors to: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->bFactorValue == 0 ) { hlpSS2 << "  AUTO"; }
        else { hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->bFactorValue * 1000.0 ) / 1000.0; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               5,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Change B-factors after map computation by: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->bFactorChange * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               6,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Map IQR from median threshold: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->noIQRsFromMap * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               7,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Distance between shells: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->shellSpacing == 0 ) { hlpSS2 << "  AUTO"; }
        else { hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->shellSpacing * 1000.0 ) / 1000.0; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               8,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Number of shells: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->manualShells == 0 ) { hlpSS2 << "  AUTO"; }
        else { hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->manualShells * 1000.0 ) / 1000.0; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               9,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Use Centre of Mass for centering: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->useCOM == 0 ) { hlpSS2 << " FALSE"; }
        else { hlpSS2 << "  TRUE"; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               10,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Add extra space to cell: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->extraSpace * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               11,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Raise Fourier coefficients to power: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->alpha * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               12,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Weight Energy Level matrix position by: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->mPower * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               13,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Ignore the following bands: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        for ( int iter = 0; iter < static_cast<int> ( this->ignoreLs.size() ); iter++ )
        {
            hlpSS2 << std::showpos << this->ignoreLs.at(iter) << " ";
        }
        hlpSS << "     " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               14,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Compute Energy Level distances: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->energyLevelDist == 0 ) { hlpSS2 << " FALSE"; }
        else { hlpSS2 << "  TRUE"; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               15,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Compute Trace Sigma distances: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->traceSigmaDist == 0 ) { hlpSS2 << " FALSE"; }
        else { hlpSS2 << "  TRUE"; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               16,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Compute Rotation Function distances: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->fullRotFnDist == 0 ) { hlpSS2 << " FALSE"; }
        else { hlpSS2 << "  TRUE"; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               17,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Use phase information: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->usePhase == 0 ) { hlpSS2 << " FALSE"; }
        else { hlpSS2 << "  TRUE"; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               18,
                                               0,
                                               1,
                                               1 );
        
        rvapi_flush                           ( );
    }
    
    if ( settings->htmlReport )
    {
        //==================================== Create section
        rvapi_add_section                     ( "DBFilesSection",
                                                "List of files",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                false );
        settings->htmlReportLine             += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== First, find sizes
    if ( settings->verbose > 0 )
    {
        std::cout << "Now detecting sizes of structures for database sorting." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Database settings saved ." << "</font>";
        rvapi_set_text                ( hlpSS.str().c_str(),
                                       "ProgressSection",
                                       settings->htmlReportLineProgress,
                                       1,
                                       1,
                                       1 );
        settings->htmlReportLineProgress += 1;
        
        rvapi_flush                   ( );
    }
    
    std::vector< std::array<double,2> > strSizes;
    std::array<double,2> hlpArr;
    std::vector<double> shSpc                 ( static_cast<unsigned int> ( settings->structFiles.size() ), 0.0 );
    std::vector<unsigned int> bndV            ( static_cast<unsigned int> ( settings->structFiles.size() ), 0.0 );
    std::vector<unsigned int> thtV            ( static_cast<unsigned int> ( settings->structFiles.size() ), 0.0 );
    std::vector<unsigned int> phV             ( static_cast<unsigned int> ( settings->structFiles.size() ), 0.0 );
    std::vector<unsigned int> glIntV          ( static_cast<unsigned int> ( settings->structFiles.size() ), 0.0 );
    std::vector<double> exSpV                 ( static_cast<unsigned int> ( settings->structFiles.size() ), settings->extraSpace );
    int filesSectionIter                      = 0;
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( settings->structFiles.size() ); iter++ )
    {
        if ( settings->verbose > 1 )
        {
            std::cout << ">> Now loading file " << iter+1 << " out of " << settings->structFiles.size() << std::endl;
        }
        
        //==================================== Create ProSHADE_data object to hold data and compute information
        ProSHADE_data* one                    = new ProSHADE_data ();
        
        //=================================== Re-set settings
        this->bandwidth                       = settings->bandwidth;
        this->glIntegOrder                    = settings->glIntegOrder;
        this->theta                           = settings->theta;
        this->phi                             = settings->phi;
        this->extraSpace                      = settings->extraSpace;
        
        //==================================== Read in the structure into one
        unsigned int fileType                 = checkFileType ( structFiles.at(iter) );
        if ( fileType == 2 )
        {
            one->getDensityMapFromMAP         ( this->structFiles.at(iter),
                                               &shSpc.at(iter),
                                                this->mapResolution,
                                               &bndV.at(iter),
                                               &thtV.at(iter),
                                               &phV.at(iter),
                                               &glIntV.at(iter),
                                               &exSpV.at(iter),
                                                settings->mapResDefault,
                                                settings->rotChangeDefault,
                                                settings,
                                                settings->overlayDefaults );
        }
        else if ( fileType == 1 )
        {
            one->getDensityMapFromPDB         ( this->structFiles.at(iter),
                                               &shSpc.at(iter),
                                                this->mapResolution,
                                               &bndV.at(iter),
                                               &thtV.at(iter),
                                               &phV.at(iter),
                                               &glIntV.at(iter),
                                               &exSpV.at(iter),
                                                settings->mapResDefault,
                                                settings,
                                                this->bFactorValue,
                                                this->firstLineCOM );
        }
        else
        {
            std::cerr << "!!! ProSHADE ERROR !!! Error loading file " << this->structFiles.at(iter) << " !!! Cannot detect the extension (currently, only PDB or MAP are allowed) and therefore cannot read the file." << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "Cannot open file named " << this->structFiles.at(iter) << "." << "</font>";
                rvapi_set_text                        ( hlpSS.str().c_str(),
                                                       "ProgressSection",
                                                       settings->htmlReportLineProgress,
                                                       1,
                                                       1,
                                                       1 );
                settings->htmlReportLineProgress     += 1;
                rvapi_flush                           ( );
            }
            
            exit ( -1 );
        }
        
        //==================================== Get volume
        hlpArr[0]                             = (one->_xRange-this->extraSpace) * (one->_yRange-this->extraSpace) * (one->_zRange-this->extraSpace);
        hlpArr[1]                             = static_cast<double> ( iter );
        
        if ( settings->databaseMaxVolume == 0.0 )
        {
            strSizes.emplace_back             ( hlpArr );
        }
        else
        {
            if ( hlpArr[0] < settings->databaseMaxVolume )
            {
                strSizes.emplace_back         ( hlpArr );
            }
            else
            {
                if ( settings->verbose > 2 )
                {
                    std::cout << ">>>>> Excluding file " << this->structFiles.at(iter) << " due to large size (databaseMaxVolume smaller than structure volunme)." << std::endl;
                }
                
                if ( settings->htmlReport )
                {
                    std::stringstream hlpSS;
                    hlpSS << "<font color=\"red\">" << "Excluding file " << this->structFiles.at(iter) << " from the database due to size constraints." << "</font>";
                    rvapi_set_text            ( hlpSS.str().c_str(),
                                                "DBFilesSection",
                                                filesSectionIter,
                                                1,
                                                1,
                                                1 );
                    filesSectionIter         += 1;
                    
                    rvapi_flush               ( );
                }
            }
        }
        
        //==================================== Clean up
        delete one;
    }
    
    //======================================== Set identical processing for structures
    double minSph                             = 1000000000.0;
    int minSphPos                             = 0;
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( settings->structFiles.size() ); iter++ )
    {
        if ( shSpc.at(iter) < minSph )
        {
            minSph                            = shSpc.at(iter);
            minSphPos                         = static_cast<int> ( iter );
        }
    }
    
    settings->shellSpacing                    = shSpc.at  ( minSphPos );
    settings->bandwidth                       = bndV.at   ( minSphPos );
    settings->theta                           = thtV.at   ( minSphPos );
    settings->phi                             = phV.at    ( minSphPos );
    settings->glIntegOrder                    = glIntV.at ( minSphPos );
    settings->extraSpace                      = exSpV.at  ( minSphPos );
    
    //======================================== Sort by volume
    std::sort                                 ( strSizes.begin(), strSizes.end(), [](const std::array<double,2>& a, const std::array<double,2>& b) { return a[0] < b[0]; });
    
    settings->databaseMinVolume               = 0.0;
    if ( strSizes.size() > 0 )
    {
        settings->databaseMinVolume           = strSizes.at(0)[0];
    }
    
    std::vector<std::string> hlpVec ( strSizes.size() );
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( strSizes.size() ); iter++ )
    {
        hlpVec.at(iter)                       = settings->structFiles.at(strSizes.at(iter)[1]);
    }
    
    settings->structFiles.clear               ( );
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( strSizes.size() ); iter++ )
    {
        settings->structFiles.emplace_back    ( hlpVec.at(iter) );
    }
    hlpVec.clear();
    strSizes.clear();
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Volume ordering of input structures complete." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        
        rvapi_flush                           ( );
    }
    
    //======================================== Initialise local variables
    unsigned int hlpUInt;
    
    //======================================== Write out the settings used to produce the database
    dbFile.write ( reinterpret_cast<char*>    ( &settings->databaseMinVolume                                ), sizeof( double       ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->mapResolution                                    ), sizeof( double       ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->bandwidth                                        ), sizeof( unsigned int ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->glIntegOrder                                     ), sizeof( unsigned int ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->theta                                            ), sizeof( unsigned int ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->phi                                              ), sizeof( unsigned int ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->bFactorValue                                     ), sizeof( double       ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->bFactorChange                                    ), sizeof( double       ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->noIQRsFromMap                                    ), sizeof( double       ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->maskBlurFactor                                   ), sizeof( double       ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->shellSpacing                                     ), sizeof( double       ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->manualShells                                     ), sizeof( unsigned int ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->usePhase                                         ), sizeof( bool         ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->saveWithAndWithout                               ), sizeof( bool         ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->useCOM                                           ), sizeof( bool         ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->firstLineCOM                                     ), sizeof( bool         ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->alpha                                            ), sizeof( double       ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->mPower                                           ), sizeof( double       ) );
    
    hlpUInt                                   = static_cast<unsigned int> ( settings->ignoreLs.size() );
    dbFile.write ( reinterpret_cast<char*>    ( &hlpUInt                                                    ), sizeof( unsigned int ) );
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( settings->ignoreLs.size() ); iter++ )
    {
        dbFile.write ( reinterpret_cast<char*> ( &settings->ignoreLs.at(iter)                            ), sizeof( unsigned int ) );
    }
    
    hlpUInt                                   = static_cast<unsigned int> ( settings->structFiles.size() );
    dbFile.write ( reinterpret_cast<char*>    ( &hlpUInt                                                    ), sizeof( unsigned int ) );
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( settings->structFiles.size() ); iter++ )
    {
        hlpUInt                               = settings->structFiles.at(iter).length();
        dbFile.write ( reinterpret_cast<char*> ( &hlpUInt                                                ), sizeof( unsigned int ) );
        dbFile.write ( settings->structFiles.at(iter).c_str(), sizeof( char ) * hlpUInt );
    }
    
    dbFile.write ( reinterpret_cast<char*>    ( &settings->peakHeightNoIQRs                                 ), sizeof( double       ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->peakDistanceForReal                              ), sizeof( double       ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->peakSurroundingPoints                            ), sizeof( int          ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->aaErrorTolerance                                 ), sizeof( double       ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->symGapTolerance                                  ), sizeof( double       ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->energyLevelDist                                  ), sizeof( bool         ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->traceSigmaDist                                   ), sizeof( bool         ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->fullRotFnDist                                    ), sizeof( bool         ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->enLevelsThreshold                                ), sizeof( double       ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->trSigmaThreshold                                 ), sizeof( double       ) );
    dbFile.write ( reinterpret_cast<char*>    ( &settings->taskToPerform                                    ), sizeof( ProSHADE::Task ) );
    
    hlpUInt                                   = settings->clearMapFile.length();
    dbFile.write ( reinterpret_cast<char*>    ( &hlpUInt                                                    ), sizeof( unsigned int ) );
    dbFile.write ( settings->clearMapFile.c_str(), sizeof( char ) * hlpUInt );
    
    hlpUInt                                   = settings->mapFragName.length();
    dbFile.write ( reinterpret_cast<char*>    ( &hlpUInt                                                    ), sizeof( unsigned int ) );
    dbFile.write ( settings->mapFragName.c_str(), sizeof( char ) * hlpUInt );
    
    if ( settings->verbose > 1 )
    {
        std::cout << ">> Settings written to " << settings->databaseName << "." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Database settings written into the database file." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
        settings->htmlReportLineProgress     += 1;
        
        rvapi_flush                           ( );
    }
    
    if ( settings->verbose > 0 )
    {
        std::cout << "Saving files in sorted volume order." << std::endl;
    }
    
    //======================================== For each file submitted for databasing
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( settings->structFiles.size() ); iter++ )
    {
        if ( settings->verbose > 1 )
        {
            std::cout << ">> Now saving file " << iter+1 << " out of " << settings->structFiles.size() << std::endl;
        }
        settings->usePhase                    = this->usePhase;
        
        //==================================== Create ProSHADE_data object to hold data and compute information
        ProSHADE_data* one                    = new ProSHADE_data ();
     
        //==================================== Re-set settings
        this->bandwidth                       = settings->bandwidth;
        this->glIntegOrder                    = settings->glIntegOrder;
        this->theta                           = settings->theta;
        this->phi                             = settings->phi;
        this->extraSpace                      = settings->extraSpace;
        
        //==================================== Read in the structure into one
        unsigned int fileType                 = checkFileType ( settings->structFiles.at(iter) );
        if ( fileType == 2 )
        {
            one->getDensityMapFromMAP         ( settings->structFiles.at(iter),
                                               &this->shellSpacing,
                                                this->mapResolution,
                                               &this->bandwidth,
                                               &this->theta,
                                               &this->phi,
                                               &this->glIntegOrder,
                                               &this->extraSpace,
                                                settings->mapResDefault,
                                                settings->rotChangeDefault,
                                                settings,
                                                settings->overlayDefaults );
        }
        else if ( fileType == 1 )
        {
            one->getDensityMapFromPDB         ( settings->structFiles.at(iter),
                                               &this->shellSpacing,
                                                this->mapResolution,
                                               &this->bandwidth,
                                               &this->theta,
                                               &this->phi,
                                               &this->glIntegOrder,
                                               &this->extraSpace,
                                                settings->mapResDefault,
                                                settings,
                                                this->bFactorValue,
                                                this->firstLineCOM );
        }
        else
        {
            std::cerr << "!!! ProSHADE ERROR !!! Error loading file " << this->structFiles.at(iter) << " !!! Cannot detect the extension (currently, only PDB or MAP are allowed) and therefore cannot read the file." << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "Cannot open file named " << this->structFiles.at(iter) << "." << "</font>";
                rvapi_set_text                        ( hlpSS.str().c_str(),
                                                       "ProgressSection",
                                                       settings->htmlReportLineProgress,
                                                       1,
                                                       1,
                                                       1 );
                settings->htmlReportLineProgress     += 1;
                rvapi_flush                           ( );
            }
            
            exit ( -1 );
        }
        
        if ( settings->verbose > 3 )
        {
            std::cout << ">>>>> Structure loaded." << std::endl;
        }
        
        //==================================== Deal with the centering and MAP data format, if applicable
        if ( saveWithAndWithout )
        {
            one->normaliseMap                 ( settings );
        }
        if ( this->usePhase )
        {
            one->keepPhaseInMap               ( this->alpha,
                                                this->bFactorChange,
                                               &this->bandwidth,
                                               &this->theta,
                                               &this->phi,
                                               &this->glIntegOrder,
                                                settings,
                                                this->useCOM,
                                                settings->noIQRsFromMap,
                                                settings->verbose,
                                                settings->clearMapData,
                                                settings->rotChangeDefault,
                                                settings->overlayDefaults,
                                                settings->maskBlurFactor,
                                                false );
        }
        else
        {
            one->removePhaseFromMap           ( this->alpha,
                                                this->bFactorChange,
                                                settings );
        }
        
        //==================================== Map the density onto spheres
        if ( saveWithAndWithout )
        {
            one->mapPhaselessToSphere         ( settings,
                                                this->theta,
                                                this->phi,
                                                this->shellSpacing,
                                                settings->manualShells,
                                                true );
        }
        else
        {
            one->mapPhaselessToSphere         ( settings,
                                               this->theta,
                                               this->phi,
                                               this->shellSpacing,
                                               settings->manualShells,
                                               false );
        }
        
        //==================================== Compute the Spherical Harmonics (SH) coefficients
        one->getSphericalHarmonicsCoeffs      ( this->bandwidth, settings );
        
        if ( settings->verbose > 3 )
        {
            std::cout << ">>>>> Spherical harmonics computed." << std::endl;
        }
        
        //==================================== Pre-compute the RRP matrices
        one->precomputeRotInvDescriptor       ( settings );
        
        //==================================== Save file settings
        dbFile.write ( reinterpret_cast<char*> ( &one->_fromPDB                                          ), sizeof( bool         ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_shellSpacing                                     ), sizeof( double       ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_maxExtraCellularSpace                            ), sizeof( double       ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_xRange                                           ), sizeof( double       ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_yRange                                           ), sizeof( double       ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_zRange                                           ), sizeof( double       ) );
        
        hlpUInt                               = one->_shellPlacement.size();
        dbFile.write ( reinterpret_cast<char*> ( &hlpUInt                                                ), sizeof( unsigned int ) );
        for ( unsigned int it = 0; it < hlpUInt; it++ )
        {
            dbFile.write ( reinterpret_cast<char*> ( &one->_shellPlacement.at(it)                        ), sizeof( double       ) );
        }
        
        dbFile.write ( reinterpret_cast<char*> ( &one->_mapResolution                                    ), sizeof( double       ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_maxMapU                                          ), sizeof( unsigned int ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_maxMapV                                          ), sizeof( unsigned int ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_maxMapW                                          ), sizeof( unsigned int ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_densityMapComputed                               ), sizeof( bool         ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_mapMean                                          ), sizeof( double       ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_mapSdev                                          ), sizeof( double       ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_fourierCoeffPower                                ), sizeof( double       ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_bFactorChange                                    ), sizeof( double       ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_maxMapRange                                      ), sizeof( double       ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_phaseRemoved                                     ), sizeof( bool         ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_usePhase                                         ), sizeof( bool         ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_keepOrRemove                                     ), sizeof( bool         ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_thetaAngle                                       ), sizeof( double       ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_phiAngle                                         ), sizeof( double       ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_noShellsWithData                                 ), sizeof( unsigned int ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_xCorrection                                      ), sizeof( unsigned int ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_yCorrection                                      ), sizeof( unsigned int ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_zCorrection                                      ), sizeof( unsigned int ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_xCorrErr                                         ), sizeof( double       ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_yCorrErr                                         ), sizeof( double       ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_zCorrErr                                         ), sizeof( double       ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_sphereMapped                                     ), sizeof( bool         ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_firstLineCOM                                     ), sizeof( bool         ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_bandwidthLimit                                   ), sizeof( unsigned int ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_oneDimmension                                    ), sizeof( unsigned int ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_sphericalCoefficientsComputed                    ), sizeof( bool         ) );
        dbFile.write ( reinterpret_cast<char*> ( &one->_rrpMatricesPrecomputed                           ), sizeof( bool         ) );
        
        //==================================== Save spherical harmonics values
        for ( unsigned int sh = 0; sh < one->_noShellsWithData; sh++ )
        {
            for ( unsigned int arrIt = 0; arrIt < ( one->_oneDimmension * one->_oneDimmension ); arrIt++ )
            {
                dbFile.write ( reinterpret_cast<char*> ( &one->_realSHCoeffs[sh][arrIt]                  ), sizeof( double       ) );
                dbFile.write ( reinterpret_cast<char*> ( &one->_imagSHCoeffs[sh][arrIt]                  ), sizeof( double       ) );
            }
        }
        
        //==================================== Save RRP matrices values
        for ( unsigned int bnIt = 0; bnIt < one->_bandwidthLimit; bnIt++ )
        {
            if ( !one->_keepOrRemove ) { if ( ( bnIt % 2 ) != 0 ) { continue; } }
            
            for ( unsigned int sh1 = 0; sh1 < one->_noShellsWithData; sh1++ )
            {
                for ( unsigned int sh2 = 0; sh2 < one->_noShellsWithData; sh2++ )
                {
                    dbFile.write ( reinterpret_cast<char*> ( &one->_rrpMatrices[bnIt][sh1][sh2]          ), sizeof( double       ) );
                }
            }
        }
        
        //==================================== If saveWithAndWithout is true, save the other phase option as well
        if ( saveWithAndWithout )
        {
            //================================ Save maps as well as the XYZ from/to values
            if ( one->_usePhase )
            {
                dbFile.write ( reinterpret_cast<char*> ( &one->_xFrom                                    ), sizeof( double       ) );
                dbFile.write ( reinterpret_cast<char*> ( &one->_xTo                                      ), sizeof( double       ) );
                dbFile.write ( reinterpret_cast<char*> ( &one->_yFrom                                    ), sizeof( double       ) );
                dbFile.write ( reinterpret_cast<char*> ( &one->_yTo                                      ), sizeof( double       ) );
                dbFile.write ( reinterpret_cast<char*> ( &one->_zFrom                                    ), sizeof( double       ) );
                dbFile.write ( reinterpret_cast<char*> ( &one->_zTo                                      ), sizeof( double       ) );
                
                for ( unsigned int mIt = 0; mIt < static_cast<unsigned int> ( (one->_maxMapU+1) * (one->_maxMapV+1) * (one->_maxMapW+1) ); mIt++ )
                {
                    dbFile.write ( reinterpret_cast<char*> ( &one->_densityMapCor[mIt]                   ), sizeof( double       ) );
                }
            }
            
            //================================ Reverse phase
            settings->usePhase               = !this->usePhase;
            
            //================================ Create ProSHADE_data object to hold data and compute information
            ProSHADE_data* two                = new ProSHADE_data ();
            
            //================================ Re-set settings
            this->bandwidth                   = settings->bandwidth;
            this->glIntegOrder                = settings->glIntegOrder;
            this->theta                       = settings->theta;
            this->phi                         = settings->phi;
            this->extraSpace                  = settings->extraSpace;
            
            //==================================== Read in the structure into two
            unsigned int fileType             = checkFileType ( settings->structFiles.at(iter) );
            if ( fileType == 2 )
            {
                two->getDensityMapFromMAP     ( settings->structFiles.at(iter),
                                               &this->shellSpacing,
                                                this->mapResolution,
                                               &this->bandwidth,
                                               &this->theta,
                                               &this->phi,
                                               &this->glIntegOrder,
                                               &this->extraSpace,
                                                settings->mapResDefault,
                                                settings->rotChangeDefault,
                                                settings,
                                                settings->overlayDefaults );
            }
            else if ( fileType == 1 )
            {
                two->getDensityMapFromPDB     ( settings->structFiles.at(iter),
                                               &this->shellSpacing,
                                                this->mapResolution,
                                               &this->bandwidth,
                                               &this->theta,
                                               &this->phi,
                                               &this->glIntegOrder,
                                               &this->extraSpace,
                                                settings->mapResDefault,
                                                settings,
                                                this->bFactorValue,
                                                this->firstLineCOM );
            }
            else
            {
                std::cerr << "!!! ProSHADE ERROR !!! Error loading file " << this->structFiles.at(iter) << " !!! Cannot detect the extension (currently, only PDB or MAP are allowed) and therefore cannot read the file." << std::endl;
                
                if ( settings->htmlReport )
                {
                    std::stringstream hlpSS;
                    hlpSS << "<font color=\"red\">" << "Cannot open file named " << this->structFiles.at(iter) << "." << "</font>";
                    rvapi_set_text            ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
                    settings->htmlReportLineProgress += 1;
                    rvapi_flush               ( );
                }
                
                exit ( -1 );
            }
            
            if ( settings->verbose > 3 )
            {
                std::cout << ">>>>> Structure loaded." << std::endl;
            }
            
            //================================ Deal with the centering and MAP data format, if applicable
            two->normaliseMap                 ( settings );
            if ( settings->usePhase )
            {
                two->keepPhaseInMap           ( this->alpha,
                                                this->bFactorChange,
                                               &this->bandwidth,
                                               &this->theta,
                                               &this->phi,
                                               &this->glIntegOrder,
                                                settings,
                                                this->useCOM,
                                                settings->noIQRsFromMap,
                                                settings->verbose,
                                                settings->clearMapData,
                                                settings->rotChangeDefault,
                                                settings->overlayDefaults,
                                                settings->maskBlurFactor,
                                                false );
            }
            else
            {
                two->removePhaseFromMap       ( this->alpha,
                                                this->bFactorChange,
                                                settings );
            }
            
            //================================ Map the density onto spheres
            two->mapPhaselessToSphere         ( settings,
                                                this->theta,
                                                this->phi,
                                                this->shellSpacing,
                                                settings->manualShells );
            
            //================================ Compute the Spherical Harmonics (SH) coefficients
            two->getSphericalHarmonicsCoeffs  ( this->bandwidth, settings );
            if ( settings->verbose > 3 )
            {
                std::cout << ">>>>> Spherical harmonics computed (phase reverse)." << std::endl;
            }
            
            //================================ Pre-compute the RRP matrices
            two->precomputeRotInvDescriptor   ( settings );
            
            //================================ Save file settings
            dbFile.write ( reinterpret_cast<char*> ( &two->_fromPDB                                          ), sizeof( bool         ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_shellSpacing                                     ), sizeof( double       ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_maxExtraCellularSpace                            ), sizeof( double       ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_xRange                                           ), sizeof( double       ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_yRange                                           ), sizeof( double       ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_zRange                                           ), sizeof( double       ) );
            
            hlpUInt                               = two->_shellPlacement.size();
            dbFile.write ( reinterpret_cast<char*> ( &hlpUInt                                                ), sizeof( unsigned int ) );
            for ( unsigned int it = 0; it < hlpUInt; it++ )
            {
                dbFile.write ( reinterpret_cast<char*> ( &two->_shellPlacement.at(it)                        ), sizeof( double       ) );
            }
            
            dbFile.write ( reinterpret_cast<char*> ( &two->_mapResolution                                    ), sizeof( double       ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_maxMapU                                          ), sizeof( unsigned int ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_maxMapV                                          ), sizeof( unsigned int ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_maxMapW                                          ), sizeof( unsigned int ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_densityMapComputed                               ), sizeof( bool         ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_mapMean                                          ), sizeof( double       ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_mapSdev                                          ), sizeof( double       ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_fourierCoeffPower                                ), sizeof( double       ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_bFactorChange                                    ), sizeof( double       ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_maxMapRange                                      ), sizeof( double       ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_phaseRemoved                                     ), sizeof( bool         ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_usePhase                                         ), sizeof( bool         ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_keepOrRemove                                     ), sizeof( bool         ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_thetaAngle                                       ), sizeof( double       ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_phiAngle                                         ), sizeof( double       ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_noShellsWithData                                 ), sizeof( unsigned int ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_xCorrection                                      ), sizeof( unsigned int ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_yCorrection                                      ), sizeof( unsigned int ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_zCorrection                                      ), sizeof( unsigned int ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_xCorrErr                                         ), sizeof( double       ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_yCorrErr                                         ), sizeof( double       ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_zCorrErr                                         ), sizeof( double       ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_sphereMapped                                     ), sizeof( bool         ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_firstLineCOM                                     ), sizeof( bool         ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_bandwidthLimit                                   ), sizeof( unsigned int ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_oneDimmension                                    ), sizeof( unsigned int ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_sphericalCoefficientsComputed                    ), sizeof( bool         ) );
            dbFile.write ( reinterpret_cast<char*> ( &two->_rrpMatricesPrecomputed                           ), sizeof( bool         ) );
            
            //================================ Save spherical harmonics values
            for ( unsigned int sh = 0; sh < two->_noShellsWithData; sh++ )
            {
                for ( unsigned int arrIt = 0; arrIt < ( two->_oneDimmension * two->_oneDimmension ); arrIt++ )
                {
                    dbFile.write ( reinterpret_cast<char*> ( &two->_realSHCoeffs[sh][arrIt]                  ), sizeof( double       ) );
                    dbFile.write ( reinterpret_cast<char*> ( &two->_imagSHCoeffs[sh][arrIt]                  ), sizeof( double       ) );
                }
            }
            
            //================================ Save RRP matrices values
            for ( unsigned int bnIt = 0; bnIt < two->_bandwidthLimit; bnIt++ )
            {
                if ( !two->_keepOrRemove ) { if ( ( bnIt % 2 ) != 0 ) { continue; } }
                
                for ( unsigned int sh1 = 0; sh1 < two->_noShellsWithData; sh1++ )
                {
                    for ( unsigned int sh2 = 0; sh2 < two->_noShellsWithData; sh2++ )
                    {
                        dbFile.write ( reinterpret_cast<char*> ( &two->_rrpMatrices[bnIt][sh1][sh2]          ), sizeof( double       ) );
                    }
                }
            }
            
            //================================ Save maps as well
            if ( two->_usePhase )
            {
                dbFile.write ( reinterpret_cast<char*> ( &two->_xFrom                                    ), sizeof( double       ) );
                dbFile.write ( reinterpret_cast<char*> ( &two->_xTo                                      ), sizeof( double       ) );
                dbFile.write ( reinterpret_cast<char*> ( &two->_yFrom                                    ), sizeof( double       ) );
                dbFile.write ( reinterpret_cast<char*> ( &two->_yTo                                      ), sizeof( double       ) );
                dbFile.write ( reinterpret_cast<char*> ( &two->_zFrom                                    ), sizeof( double       ) );
                dbFile.write ( reinterpret_cast<char*> ( &two->_zTo                                      ), sizeof( double       ) );
                
                for ( unsigned int mIt = 0; mIt < static_cast<unsigned int> ( two->_maxMapU * two->_maxMapV * two->_maxMapW ); mIt++ )
                {
                    dbFile.write ( reinterpret_cast<char*> ( &two->_densityMapCor[mIt]                   ), sizeof( double       ) );
                }
            }
            
            //==================================== Clean up
            delete two;
        }
        
        if ( settings->verbose > 3 )
        {
            std::cout << ">>>>> Structure saved." << std::endl;
        }
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "File " << this->structFiles.at(iter) << " successfully saved to the database file." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "DBFilesSection",
                                                filesSectionIter,
                                                1,
                                                1,
                                                1 );
            filesSectionIter                 += 1;
            
            rvapi_flush                       ( );
        }
        
        //==================================== Clean up
        delete one;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "File processing complete." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
        settings->htmlReportLineProgress     += 1;
        
        rvapi_flush                           ( );
    }
    
    //======================================== Close output file and done
    dbFile.close ( );
    
    //======================================== Done
    return ;
}

/*! \brief This function compares a single file agaisnt a database of pre-computed structures.
 
 This function does basically the same thing as the constructor for the ProSHADE_distances and even does fill in the internal values like
 the constructor would, so that the same public functions can then be used to retrieve the results. There are, however, a few points worth
 noting.
 
 1) All settings are read from the database file and therefore it is useless to supply them, the only exception being the thresholds
 on hierarchical comparison. Therefore, if one wants to change the settings, new database needs to be build.
 
 2) Since structures failing the size test are removed completely from the comparison, the list of database structures becomes useless,
 as one does not know which were removed and which were not. To allow knowing which structures obtained which distances, the settings
 structFiles vector is filled with the name of the one structure followed by all database structures which passed the test, as if it were
 given to the standard (i.e. no database) ProSHADE_distances class instead.
 
 \param[in] settings The settings structure will be filled with the database data, so it only needs to contain the single file to compare against the database. Its contents can later be used by the user for other purposes.
 \param[in] matchedStrNames Pointer to a vector of strings which will be filled with the database file names that passed the size test (whether something passed or failed is lost otherwise).
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_distances::compareAgainstDatabase ( ProSHADE::ProSHADE_settings* settings,
                                                                     std::vector<std::string>* matchedStrNames )
{
    //======================================== Local variables
    unsigned int hlpUInt, hlpUInt2;
    char* hlpChar;
    std::vector<std::string> dbStrNames;
    double minVol                             = 0.0;
    
    //======================================== Open the database file
    std::ifstream dbFile                      ( settings->databaseName, std::ios::in | std::ios::binary);
    
    //======================================== Check the database file
    if ( dbFile.fail() )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Cannot open the database file for reading " << settings->databaseName << " . Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Cannot open database with name " << settings->databaseName << ". Could you have no permissions to write to the requested database location?" << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    if ( settings->verbose > 3 )
    {
        std::cout << ">>>>>>>> Database file opened." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        //==================================== Record progress
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Database reading initiated." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Save the user input which is related to comparison and should not be loeaded from the database
    this->enLevelsThreshold                   = settings->enLevelsThreshold;
    this->trSigmaThreshold                    = settings->trSigmaThreshold;
    
    //======================================== Read in the database settings
    dbFile.read ( reinterpret_cast<char*>     ( &minVol                                                      ), sizeof ( double       ) );
    if ( minVol == 0.0 )
    {
        std::cerr << "!!! ProSHADE ERROR !!! The database file seems to be corrupted, or was produced with too small maximum allowed volume, as it is reporting minimal structure volume of 0.0 A. Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "The database file seems to be corrupted, or was produced with too small maximum allowed volume, as it is reporting minimal structure volume of 0.0 A. If this problem is not solved by decreasing the maximum allowed volume, this could be internal bug, in this case please report it." << "</font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }
        
        exit ( -1 );
    }
    
    dbFile.read ( reinterpret_cast<char*>     ( &settings->mapResolution                                     ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->bandwidth                                         ), sizeof ( unsigned int ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->glIntegOrder                                      ), sizeof ( unsigned int ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->theta                                             ), sizeof ( unsigned int ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->phi                                               ), sizeof ( unsigned int ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->bFactorValue                                      ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->bFactorChange                                     ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->noIQRsFromMap                                     ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->maskBlurFactor                                    ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->shellSpacing                                      ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->manualShells                                      ), sizeof ( unsigned int ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->usePhase                                          ), sizeof ( bool         ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->saveWithAndWithout                                ), sizeof ( bool         ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->useCOM                                            ), sizeof ( bool         ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->firstLineCOM                                      ), sizeof ( bool         ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->alpha                                             ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->mPower                                            ), sizeof ( double       ) );
    
    dbFile.read ( reinterpret_cast<char*>     ( &hlpUInt                                                     ), sizeof ( unsigned int ) );
    for ( unsigned int iter = 0; iter < hlpUInt; iter++ )
    {
        dbFile.read ( reinterpret_cast<char*> ( &hlpUInt2                                                ), sizeof ( unsigned int ) );
        settings->ignoreLs.emplace_back ( hlpUInt2 );
    }
    
    dbFile.read ( reinterpret_cast<char*>     ( &hlpUInt                                                     ), sizeof ( unsigned int ) );
    for ( unsigned int iter = 0; iter < hlpUInt; iter++ )
    {
        dbFile.read ( reinterpret_cast<char*> ( &hlpUInt2                                                ), sizeof ( unsigned int ) );
        hlpChar                               = new char[hlpUInt2+1];
        dbFile.read ( hlpChar, sizeof ( char ) * hlpUInt2 );
        hlpChar[hlpUInt2]                     = '\0';
        dbStrNames.emplace_back               ( hlpChar );
    }
    dbFile.read ( reinterpret_cast<char*>     ( &settings->peakHeightNoIQRs                                  ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->peakDistanceForReal                               ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->peakSurroundingPoints                             ), sizeof ( int          ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->aaErrorTolerance                                  ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->symGapTolerance                                   ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->energyLevelDist                                   ), sizeof ( bool         ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->traceSigmaDist                                    ), sizeof ( bool         ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->fullRotFnDist                                     ), sizeof ( bool         ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->enLevelsThreshold                                 ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->trSigmaThreshold                                  ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->taskToPerform                                     ), sizeof ( ProSHADE::Task ) );
    
    dbFile.read ( reinterpret_cast<char*>     ( &hlpUInt                                                     ), sizeof ( unsigned int ) );
    hlpChar                                   = new char[hlpUInt+1];
    dbFile.read ( hlpChar, sizeof ( char ) * hlpUInt );
    hlpChar[hlpUInt]                          = '\0';
    settings->clearMapFile                    = std::string ( hlpChar );
    
    dbFile.read ( reinterpret_cast<char*> ( &hlpUInt                                                     ), sizeof ( unsigned int ) );
    hlpChar                                   = new char[hlpUInt+1];
    dbFile.read ( hlpChar, sizeof ( char ) * hlpUInt );
    hlpChar[hlpUInt]                          = '\0';
    settings->mapFragName                     = std::string ( hlpChar );
    
    //======================================== Save the settings
    this->mapResolution                       = settings->mapResolution;
    this->bandwidth                           = settings->bandwidth;
    this->glIntegOrder                        = settings->glIntegOrder;
    this->theta                               = settings->theta;
    this->phi                                 = settings->phi;
    this->bFactorValue                        = settings->bFactorValue;
    this->bFactorChange                       = settings->bFactorChange;
    this->noIQRsFromMap                       = settings->noIQRsFromMap;
    this->shellSpacing                        = settings->shellSpacing;
    this->manualShells                        = settings->manualShells;
    this->useCOM                              = settings->useCOM;
    this->firstLineCOM                        = settings->firstLineCOM;
    this->extraSpace                          = settings->extraSpace;
    this->alpha                               = settings->alpha;
    this->mPower                              = settings->mPower;
    this->ignoreLs                            = settings->ignoreLs;
    this->energyLevelDist                     = settings->energyLevelDist;
    this->traceSigmaDist                      = settings->traceSigmaDist;
    this->fullRotFnDist                       = settings->fullRotFnDist;
    this->usePhase                            = settings->usePhase;
    this->saveWithAndWithout                  = settings->saveWithAndWithout;
    this->structFiles                         = settings->structFiles;
    
    if ( settings->verbose > 3 )
    {
        std::cout << ">>>>>>>> Database settings loaded." << std::endl;
    }
    
    
    if ( settings->htmlReport )
    {
        //==================================== Record progress
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Database settings read." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Now create the one strucutre, so that its dimmensions may be used for reducing the number of read structures
    ProSHADE_data* one                        = new ProSHADE_data ();
    
    //======================================== Read in into one
    matchedStrNames->clear                    ( );
    matchedStrNames->emplace_back             ( structFiles.at(0) );
    unsigned int fileType                     = checkFileType ( structFiles.at(0) );
    if ( fileType == 2 )
    {
        one->getDensityMapFromMAP             ( this->structFiles.at(0),
                                               &this->shellSpacing,
                                                this->mapResolution,
                                               &this->bandwidth,
                                               &this->theta,
                                               &this->phi,
                                               &this->glIntegOrder,
                                               &this->extraSpace,
                                                settings->mapResDefault,
                                                settings->rotChangeDefault,
                                                settings,
                                                settings->overlayDefaults );
    }
    else if ( fileType == 1 )
    {
        one->getDensityMapFromPDB             ( this->structFiles.at(0),
                                               &this->shellSpacing,
                                                this->mapResolution,
                                               &this->bandwidth,
                                               &this->theta,
                                               &this->phi,
                                               &this->glIntegOrder,
                                               &this->extraSpace,
                                                settings->mapResDefault,
                                                settings,
                                                this->bFactorValue,
                                                this->firstLineCOM );
    }
    else
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error loading file " << this->structFiles.at(0) << " !!! Cannot detect the extension (currently, only PDB or MAP are allowed) and therefore cannot read the file." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Cannot open file named " << this->structFiles.at(0) << " ." << "</font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }
        
        exit ( -1 );
    }
    if ( settings->verbose > 0 )
    {
        std::cout << "Structure 0 loaded." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        //==================================== Record progress
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Sought structure loaded." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Deal with the centering and MAP data format, if applicable
    one->normaliseMap                         ( settings );
    if ( this->usePhase )
    {
        one->keepPhaseInMap                   ( this->alpha,
                                                this->bFactorChange,
                                               &this->bandwidth,
                                               &this->theta,
                                               &this->phi,
                                               &this->glIntegOrder,
                                                settings,
                                                this->useCOM,
                                                settings->noIQRsFromMap,
                                                settings->verbose,
                                                settings->clearMapData,
                                                settings->rotChangeDefault,
                                                settings->overlayDefaults,
                                                settings->maskBlurFactor,
                                                false );
    }
    else
    {
        one->removePhaseFromMap               ( this->alpha,
                                                this->bFactorChange,
                                                settings );
    }
    
    //======================================== Map the density onto spheres
    one->mapPhaselessToSphere                 ( settings,
                                                this->theta,
                                                this->phi,
                                                this->shellSpacing,
                                                settings->manualShells );
    
    //======================================== Compute the Spherical Harmonics (SH) coefficients
    one->getSphericalHarmonicsCoeffs          ( this->bandwidth, settings );
    
    //======================================== If required, pre-compute the energy levels distance descriptor
    if ( this->energyLevelDist )
    {
        one->precomputeRotInvDescriptor       ( settings );
    }
    if ( settings->verbose > 3 )
    {
        std::cout << ">>>>>>>> Structure 0 spherical harmonics computed." << std::endl;
    }
    
    int structIterHTML                        = 0;
    if ( settings->htmlReport )
    {
        //==================================== Create section
        rvapi_add_section                     ( "FilesSection",
                                                "List of structures",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                false );
        settings->htmlReportLine             += 1;
        
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Structure to compare against the rest: " << this->structFiles.at(0) << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "FilesSection",
                                                structIterHTML,
                                                1,
                                                1,
                                                1 );
        structIterHTML                       += 1;
        
        rvapi_flush                           ( );
    }
    
    //======================================== Read in the database data into ProSHADE_data objects
    double volTolerance                       = ( 1.0 - settings->volumeTolerance );
    double volMin                             = ( ( static_cast<double> ( (one->_xRange-this->extraSpace) ) * volTolerance ) *
                                                  ( static_cast<double> ( (one->_yRange-this->extraSpace) ) * volTolerance ) *
                                                  ( static_cast<double> ( (one->_zRange-this->extraSpace) ) * volTolerance ) );
    volTolerance                              = ( 1.0 + settings->volumeTolerance );
    double volMax                             = ( ( static_cast<double> ( (one->_xRange-this->extraSpace) ) * volTolerance ) *
                                                  ( static_cast<double> ( (one->_yRange-this->extraSpace) ) * volTolerance ) *
                                                  ( static_cast<double> ( (one->_zRange-this->extraSpace) ) * volTolerance ) );
    std::vector<ProSHADE_data*> dbData;
    bool foundAlready                         = false;
    bool tooFarAlready                        = false;
    for ( unsigned int strIt = 0; strIt < static_cast<unsigned int> ( dbStrNames.size() ); strIt++ )
    {
        if ( tooFarAlready ) { break; }
        
        if ( settings->verbose > 2 )
        {
            std::cout << ">>>>> Starting to load structure " << strIt+1 << " from the database." << std::endl;
        }
        
        if ( !this->saveWithAndWithout )
        {
            dbData.emplace_back               ( new ProSHADE_data ( &dbFile, dbStrNames.at(strIt), volMin, volMax, settings->verbose, settings ) );
            
            if ( dbData.at(dbData.size()-1)->_mapResolution == -999.9 )
            {
                if ( foundAlready ) { tooFarAlready = true; }
                
                dbData.pop_back               ( );
                if ( settings->verbose > 0 )
                {
                    std::cout << "Structure " << strIt+1 << " read from the database, but will not be used - too different dimmensions - see the \'--dbSizeLim\' option." << std::endl;
                    
                    if ( settings->htmlReport )
                    {
                        std::stringstream hlpSS;
                        hlpSS << "<font color=\"orange\">" << " ... database entry " << strIt+1 << ": " << dbStrNames.at(strIt) << " will be ignored." << "</font>";
                        rvapi_set_text        ( hlpSS.str().c_str(),
                                                "FilesSection",
                                                structIterHTML,
                                                1,
                                                1,
                                                1 );
                        structIterHTML       += 1;
                    }
                }
            }
            else
            {
                foundAlready                  = true;
                matchedStrNames->emplace_back ( dbStrNames.at(strIt) );
                if ( settings->verbose > 0 )
                {
                    std::cout << "Structure " << strIt+1 << " read from the database." << std::endl;
                }
                
                if ( settings->htmlReport )
                {
                    std::stringstream hlpSS;
                    hlpSS << "<font color=\"green\">" << " ... database entry " << strIt+1 << ": " << dbStrNames.at(strIt) << " will be used." << "</font>";
                    rvapi_set_text            ( hlpSS.str().c_str(),
                                                "FilesSection",
                                                structIterHTML,
                                                1,
                                                1,
                                                1 );
                    structIterHTML           += 1;
                }
            }
        }
        else
        {
            dbData.emplace_back               ( new ProSHADE_data ( &dbFile, dbStrNames.at(strIt), volMin, volMax, settings->verbose, settings, this->saveWithAndWithout ) );
            dbData.emplace_back               ( new ProSHADE_data ( &dbFile, dbStrNames.at(strIt), volMin, volMax, settings->verbose, settings, this->saveWithAndWithout ) );
            
            if ( ( dbData.at(dbData.size()-2)->_mapResolution == -999.9 ) || ( dbData.at(dbData.size()-1)->_mapResolution == -999.9 ) )
            {
                if ( foundAlready ) { tooFarAlready = true; }
                
                dbData.pop_back               ( );
                dbData.pop_back               ( );
                if ( settings->verbose > 0 )
                {
                    std::cout << "Structure " << strIt+1 << " read from the database, but will not be used - too different dimmensions - see the \'--dbSizeLim\' option." << std::endl;
                    
                    if ( settings->htmlReport )
                    {
                        std::stringstream hlpSS;
                        hlpSS << "<font color=\"orange\">" << " ... database entry " << strIt+1 << ": " << dbStrNames.at(strIt) << " will be ignored." << "</font>";
                        rvapi_set_text        ( hlpSS.str().c_str(),
                                               "FilesSection",
                                               structIterHTML,
                                               1,
                                               1,
                                               1 );
                        structIterHTML       += 1;
                    }
                }
            }
            else
            {
                foundAlready                  = true;
                matchedStrNames->emplace_back ( dbStrNames.at(strIt) );
                if ( settings->verbose > 0 )
                {
                    std::cout << "Structure " << strIt+1 << " read from the database." << std::endl;
                }
                
                if ( settings->htmlReport )
                {
                    std::stringstream hlpSS;
                    hlpSS << "<font color=\"green\">" << " ... database entry " << strIt+1 << ": " << dbStrNames.at(strIt) << " will be used." << "</font>";
                    rvapi_set_text            ( hlpSS.str().c_str(),
                                               "FilesSection",
                                               structIterHTML,
                                               1,
                                               1,
                                               1 );
                    structIterHTML           += 1;
                }
            }
        }
    }
    
    if ( settings->htmlReport )
    {
        //==================================== Record progress
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Database imported." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Check for not finding any size matches
    if ( dbData.size() == 0 )
    {
        std::cout << "!!! ProSHADE WARNING !!! There was no database entry which would pass the size limitations and therefore cannot compute distances. This could be caused by submitting database file with small number or very narrowly distributed structure sizes. Alternatively, the size limit parameter ( \'--dbSizeLim\' ) could be set to too constricted values (or the default values are sub-optimal for this case). Now terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"orange\">" << "There was no database entry which would pass the size limitations and therefore cannot compute distances. This could be caused by submitting database file with small number or very narrowly distributed structure sizes. Alternatively, the size limit parameter ( \'--dbSizeLim\' ) could be set to too constricted values (or the default values are sub-optimal for this case)." << "</font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }
        
        exit ( 0 );
    }
    
    //======================================== Now create the two strucutre in case it is being used with both phase and reverse phase
    ProSHADE_data* two                        = new ProSHADE_data ();
    
    //======================================== Create the compare against all object
    if ( !this->saveWithAndWithout )
    {
        this->cmpObj                          = new ProSHADE_compareOneAgainstAll ( one, &dbData, this->ignoreLs, this->mPower, settings->verbose );
    }
    else
    {
        //==================================== Read in into one
        fileType                              = checkFileType ( structFiles.at(0) );
        if ( fileType == 2 )
        {
            two->getDensityMapFromMAP         ( this->structFiles.at(0),
                                               &this->shellSpacing,
                                                this->mapResolution,
                                               &this->bandwidth,
                                               &this->theta,
                                               &this->phi,
                                               &this->glIntegOrder,
                                               &this->extraSpace,
                                                settings->mapResDefault,
                                                settings->rotChangeDefault,
                                                settings,
                                                settings->overlayDefaults );
        }
        else if ( fileType == 1 )
        {
            two->getDensityMapFromPDB         ( this->structFiles.at(0),
                                               &this->shellSpacing,
                                                this->mapResolution,
                                               &this->bandwidth,
                                               &this->theta,
                                               &this->phi,
                                               &this->glIntegOrder,
                                               &this->extraSpace,
                                                settings->mapResDefault,
                                                settings,
                                                this->bFactorValue,
                                                this->firstLineCOM );
        }
        else
        {
            std::cerr << "!!! ProSHADE ERROR !!! Error loading file " << this->structFiles.at(0) << " !!! Cannot detect the extension (currently, only PDB or MAP are allowed) and therefore cannot read the file." << std::endl;
            exit (0);
        }
        
        //==================================== Deal with the centering and MAP data format, if applicable
        two->normaliseMap                     ( settings );
        if ( !this->usePhase )
        {
            two->keepPhaseInMap               ( this->alpha,
                                                this->bFactorChange,
                                               &this->bandwidth,
                                               &this->theta,
                                               &this->phi,
                                               &this->glIntegOrder,
                                                settings,
                                                this->useCOM,
                                                settings->noIQRsFromMap,
                                                settings->verbose,
                                                settings->clearMapData,
                                                settings->rotChangeDefault,
                                                settings->overlayDefaults,
                                                settings->maskBlurFactor,
                                                false );
        }
        else
        {
            two->removePhaseFromMap           ( this->alpha,
                                                this->bFactorChange,
                                                settings );
        }
        
        //==================================== Map the density onto spheres
        two->mapPhaselessToSphere             ( settings,
                                                this->theta,
                                                this->phi,
                                                this->shellSpacing,
                                                settings->manualShells,
                                                true );
        
        //==================================== Compute the Spherical Harmonics (SH) coefficients
        two->getSphericalHarmonicsCoeffs      ( this->bandwidth, settings );
        
        //==================================== If required, pre-compute the energy levels distance descriptor
        if ( this->energyLevelDist )
        {
            two->precomputeRotInvDescriptor   ( settings );
        }
        if ( settings->verbose > 3 )
        {
            std::cout << ">>>>>>>> Structure 0 spherical harmonics computed (reverse phase)." << std::endl;
        }
        
        this->cmpObj                          = new ProSHADE_compareOneAgainstAll ( one, two, &dbData, this->ignoreLs, this->mPower, settings->verbose );
    }
    
    //======================================== Save the decision whether to use phases or not
    if ( one->_keepOrRemove ) { this->cmpObj->_keepOrRemove = true; }
    else                      { this->cmpObj->_keepOrRemove = false; }
    
    //======================================== If using both phased and reverse, compute the translated and rotated versions
    if ( this->saveWithAndWithout )
    {
        this->cmpObj->alignDensities          ( settings );
    }
    
    //======================================== If required, compute the energy levels distances
    if ( this->energyLevelDist )
    {
        if ( settings->verbose > 0 )
        {
            std::cout << "Computing the cross-correlation distances." << std::endl;
        }
        if ( settings->htmlReport )
        {
            //==================================== Record progress
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Computing the Energy Level distances." << "</font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }

        this->energyLevelsDistances           = this->cmpObj->getEnergyLevelsDistance ( settings->verbose, settings );

        // ... and set which pairs not to follow further
        if ( this->enLevelsThreshold != -999.9 )
        {
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->cmpObj->all->size() ); iter++ )
            {
                if ( this->enLevelsThreshold > this->energyLevelsDistances.at(iter) )
                {
                    this->cmpObj->_enLevelsDoNotFollow.emplace_back ( 1 );
                }
                else
                {
                    this->cmpObj->_enLevelsDoNotFollow.emplace_back ( 0 );
                }
            }
        }
        else
        {
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->cmpObj->all->size() ); iter++ )
            {
                this->cmpObj->_enLevelsDoNotFollow.emplace_back ( 0 );
            }
        }
    }
    else
    {
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->cmpObj->all->size() ); iter++ )
        {
            this->cmpObj->_enLevelsDoNotFollow.emplace_back ( 0 );
        }
    }
    
    //======================================== Remove all structures with null pointer (i.e. those not passing the size test)
    this->glIntegOrderVec                     = std::vector<unsigned int> ( dbData.size() + 1 );
    for ( unsigned int strIt = 0; strIt < static_cast<unsigned int> ( dbData.size() + 1 ); strIt++ )
    {
        this->glIntegOrderVec.at(strIt)       = this->glIntegOrder;
    }
    
    //======================================== Pre-compute the E matrices in case either the trace sigmal or full rotation distance are required
    if ( this->traceSigmaDist || this->fullRotFnDist )
    {
        this->cmpObj->precomputeTrSigmaDescriptor ( this->shellSpacing, &this->glIntegOrderVec, settings );
    }
    
    //======================================== Compute the trace sigma distances, if required
    if ( this->traceSigmaDist )
    {
        if ( settings->verbose > 0 )
        {
            std::cout << "Computing the trace sigma distances." << std::endl;
        }
        if ( settings->htmlReport )
        {
            //==================================== Record progress
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Computing Trace Sigma distances." << "</font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }
        
        this->traceSigmaDistances             = this->cmpObj->getTrSigmaDistance ( settings->verbose, settings );
        
        if ( this->trSigmaThreshold == -999.9 )
        {
            this->cmpObj->_trSigmaDoNotFollow = this->cmpObj->_enLevelsDoNotFollow;
        }
        else
        {
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->traceSigmaDistances.size() ); iter++ )
            {
                if ( this->trSigmaThreshold > this->traceSigmaDistances.at(iter) )
                {
                    this->cmpObj->_trSigmaDoNotFollow.emplace_back ( 1 );
                }
                else
                {
                    this->cmpObj->_trSigmaDoNotFollow.emplace_back ( 0 );
                }
            }
        }
    }
    
    //======================================== If full rotation distance is required, pre-compute the requirements
    if ( this->fullRotFnDist )
    {
        this->cmpObj->getSO3InverseMap        ( settings );
        if ( settings->verbose > 1 )
        {
            std::cout << ">> Inverse SO(3) Fourier transform map computed." << std::endl;
        }
        
        this->cmpObj->getEulerAngles          ( settings );
        if ( settings->verbose > 2 )
        {
            std::cout << ">>>>> Optimal Euler angles obtained." << std::endl;
        }
        
        this->cmpObj->generateWignerMatrices  ( settings );
        if ( settings->verbose > 1 )
        {
            std::cout << ">> Wigner matrices calculated." << std::endl;
        }
        
        if ( settings->verbose > 0 )
        {
            std::cout << "Computing the rotation function distances." << std::endl;
        }
        if ( settings->htmlReport )
        {
            //==================================== Record progress
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Computing Rotation Function distances." << "</font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }
        
        this->fullRotationDistances           = this->cmpObj->getRotCoeffDistance ( settings->verbose, settings );
    }
    
    //======================================== Delete database entries
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( iter ); iter++ )
    {
        if ( dbData.at(iter) != nullptr )
        {
            delete dbData.at(iter);
            dbData.at(iter)                   = nullptr;
        }
    }
    dbData.clear                              ( );
    
    //======================================== Done
    return ;
}

/*! \brief This function fragments and compares a single file agaisnt a database of pre-computed structures.
 
 This function reads in the settings of the database and applies them to a single structure supplied in the settings parameter. It then fragments this single structure according to the
 settings and searches each fragment independently against the database, thus allowing finding fragments in the database.
 
 \param[in] settings The settings structure will be filled with the database data, so it only needs to contain the single file to compare against the database. Its contents can later be used by the user for other purposes.
 \param[in] matchedStrNames Pointer to a vector of strings which will be filled with the database file names that passed the size test (whether something passed or failed is lost otherwise).
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_distances::compareFragAgainstDatabase ( ProSHADE::ProSHADE_settings* settings,
                                                                         std::vector<std::string>* matchedStrNames )
{
    //======================================== Local variables
    unsigned int hlpUInt, hlpUInt2;
    char* hlpChar;
    std::vector<std::string> dbStrNames;
    double minVol                             = 0.0;
    
    if ( settings->verbose > 0 )
    {
        std::cout << "Computing distances between map fragments and the structure database." << std::endl;
    }
    
    //======================================== Open the database file
    std::ifstream dbFile                      ( settings->databaseName, std::ios::in | std::ios::binary);
    
    //======================================== Check the database file
    if ( dbFile.fail() )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Cannot open the database file for reading " << settings->databaseName << " . Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Cannot open database with name " << settings->databaseName << ". Could you have no permissions to write to the requested database location?" << "</font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Check fragmentation settings
    if ( settings->mapFragBoxSize == 0.0 )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Requested fragmentation database comparison, but you did not supply the fragmentation box size - use the '--mFrag' option to set this. Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Missing the mandatory argument determining the fragmentation box size. Please supply it using the --mFrag command line option." << "</font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }
        
        exit ( -1 );
    }
    if ( !settings->usePhase )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Requested map fragmentation without using phases - this does not make sense. Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Requested map fragmentation without using phases. This is not allowed." << "</font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }
        
        exit ( -1 );
    }
    
    if ( settings->verbose > 3 )
    {
        std::cout << ">>>>>>>> Database file opened for reading." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Database reading intialisation complete." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        
        rvapi_flush                           ( );
    }

    //======================================== Save the user input which is related to comparison and should not be loeaded from the database
    this->enLevelsThreshold                   = settings->enLevelsThreshold;
    this->trSigmaThreshold                    = settings->trSigmaThreshold;
    
    //======================================== Set internal user settings
    bool userCOM                              = false;
    if ( settings->useCOM == true ) { userCOM = true; }
    
    //======================================== Read in the database settings
    dbFile.read ( reinterpret_cast<char*>     ( &minVol                                                      ), sizeof ( double       ) );
    if ( minVol == 0.0 )
    {
        std::cerr << "!!! ProSHADE ERROR !!! The database file seems to be corrupted, or was produced with too small maximum allowed volume, as it is reporting minimal structure volume of 0.0 A. Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "The database file seems to be corrupted, or was produced with too small maximum allowed volume, as it is reporting minimal structure volume of 0.0 A. If this problem is not solved by decreasing the maximum allowed volume, this could be internal bug, in this case please report it." << "</font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }
        
        exit ( -1 );
    }
    
    dbFile.read ( reinterpret_cast<char*>     ( &settings->mapResolution                                     ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->bandwidth                                         ), sizeof ( unsigned int ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->glIntegOrder                                      ), sizeof ( unsigned int ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->theta                                             ), sizeof ( unsigned int ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->phi                                               ), sizeof ( unsigned int ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->bFactorValue                                      ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->bFactorChange                                     ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->noIQRsFromMap                                     ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->maskBlurFactor                                    ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->shellSpacing                                      ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->manualShells                                      ), sizeof ( unsigned int ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->usePhase                                          ), sizeof ( bool         ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->saveWithAndWithout                                ), sizeof ( bool         ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->useCOM                                            ), sizeof ( bool         ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->firstLineCOM                                      ), sizeof ( bool         ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->alpha                                             ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->mPower                                            ), sizeof ( double       ) );
    
    dbFile.read ( reinterpret_cast<char*>     ( &hlpUInt                                                     ), sizeof ( unsigned int ) );
    for ( unsigned int iter = 0; iter < hlpUInt; iter++ )
    {
        dbFile.read ( reinterpret_cast<char*> ( &hlpUInt2                                                ), sizeof ( unsigned int ) );
        settings->ignoreLs.emplace_back       ( hlpUInt2 );
    }
    
    dbFile.read ( reinterpret_cast<char*> ( &hlpUInt                                                     ), sizeof ( unsigned int ) );
    for ( unsigned int iter = 0; iter < hlpUInt; iter++ )
    {
        dbFile.read ( reinterpret_cast<char*> ( &hlpUInt2                                                ), sizeof ( unsigned int ) );
        hlpChar                               = new char[hlpUInt2+1];
        dbFile.read ( hlpChar, sizeof ( char ) * hlpUInt2 );
        hlpChar[hlpUInt2]                     = '\0';
        dbStrNames.emplace_back               ( hlpChar );
    }
    dbFile.read ( reinterpret_cast<char*>     ( &settings->peakHeightNoIQRs                                  ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->peakDistanceForReal                               ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->peakSurroundingPoints                             ), sizeof ( int          ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->aaErrorTolerance                                  ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->symGapTolerance                                   ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->energyLevelDist                                   ), sizeof ( bool         ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->traceSigmaDist                                    ), sizeof ( bool         ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->fullRotFnDist                                     ), sizeof ( bool         ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->enLevelsThreshold                                 ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->trSigmaThreshold                                  ), sizeof ( double       ) );
    dbFile.read ( reinterpret_cast<char*>     ( &settings->taskToPerform                                     ), sizeof ( ProSHADE::Task ) );
    
    dbFile.read ( reinterpret_cast<char*>     ( &hlpUInt                                                     ), sizeof ( unsigned int ) );
    hlpChar                                   = new char[hlpUInt+1];
    dbFile.read                               ( hlpChar, sizeof ( char ) * hlpUInt );
    hlpChar[hlpUInt]                          = '\0';
    settings->clearMapFile                    = std::string ( hlpChar );
    
    dbFile.read ( reinterpret_cast<char*>     ( &hlpUInt                                                     ), sizeof ( unsigned int ) );
    hlpChar                                   = new char[hlpUInt+1];
    dbFile.read                               ( hlpChar, sizeof ( char ) * hlpUInt );
    hlpChar[hlpUInt]                          = '\0';
    settings->mapFragName                     = std::string ( hlpChar );
    
    if ( settings->verbose > 1 )
    {
        std::cout << ">> Database settings loaded." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Database settings loaded." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
        settings->htmlReportLineProgress     += 1;
        
        rvapi_flush                           ( );
    }
    
    //======================================== Save the settings
    this->mapResolution                       = settings->mapResolution;
    this->bandwidth                           = settings->bandwidth;
    this->glIntegOrder                        = settings->glIntegOrder;
    this->theta                               = settings->theta;
    this->phi                                 = settings->phi;
    this->bFactorValue                        = settings->bFactorValue;
    this->bFactorChange                       = settings->bFactorChange;
    this->noIQRsFromMap                       = settings->noIQRsFromMap;
    this->shellSpacing                        = settings->shellSpacing;
    this->manualShells                        = settings->manualShells;
    this->useCOM                              = settings->useCOM;
    this->firstLineCOM                        = settings->firstLineCOM;
    this->extraSpace                          = settings->extraSpace;
    this->alpha                               = settings->alpha;
    this->mPower                              = settings->mPower;
    this->ignoreLs                            = settings->ignoreLs;
    this->energyLevelDist                     = settings->energyLevelDist;
    this->traceSigmaDist                      = settings->traceSigmaDist;
    this->fullRotFnDist                       = settings->fullRotFnDist;
    this->usePhase                            = settings->usePhase;
    this->saveWithAndWithout                  = settings->saveWithAndWithout;
    this->structFiles                         = settings->structFiles;
    
    if ( settings->htmlReport )
    {
        //==================================== Create section
        rvapi_add_section                     ( "DBSettingsSection",
                                               "Database Settings",
                                               "body",
                                               settings->htmlReportLine,
                                               0,
                                               1,
                                               1,
                                               false );
        settings->htmlReportLine             += 1;
        
        std::stringstream hlpSS;
        hlpSS << "<pre>" << "Map resolution: ";
        int hlpIt                             = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        std::stringstream hlpSS2;
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->mapResolution * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               0,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Bandwidth: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->bandwidth == 0 ) { hlpSS2 << "  AUTO"; }
        else { hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->bandwidth * 1000.0 ) / 1000.0; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               1,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Gauss-Legendre Integration order: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->glIntegOrder == 0 ) { hlpSS2 << "  AUTO"; }
        else { hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->glIntegOrder * 1000.0 ) / 1000.0; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               2,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Theta angle sampling: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->theta == 0 ) { hlpSS2 << "  AUTO"; }
        else { hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->theta * 1000.0 ) / 1000.0; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               3,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Phi angle sampling: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->phi == 0 ) { hlpSS2 << "  AUTO"; }
        else { hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->phi * 1000.0 ) / 1000.0; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               4,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Set all PDB file B-factors to: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->bFactorValue == 0 ) { hlpSS2 << "  AUTO"; }
        else { hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->bFactorValue * 1000.0 ) / 1000.0; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               5,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Change B-factors after map computation by: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->bFactorChange * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               6,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Map IQR from median threshold: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->noIQRsFromMap * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               7,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Distance between shells: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->shellSpacing == 0 ) { hlpSS2 << "  AUTO"; }
        else { hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->shellSpacing * 1000.0 ) / 1000.0; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               8,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Number of shells: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->manualShells == 0 ) { hlpSS2 << "  AUTO"; }
        else { hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->manualShells * 1000.0 ) / 1000.0; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               9,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Use Centre of Mass for centering: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->useCOM == 0 ) { hlpSS2 << " FALSE"; }
        else { hlpSS2 << "  TRUE"; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               10,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Add extra space to cell: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->extraSpace * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               11,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Raise Fourier coefficients to power: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->alpha * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               12,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Weight Energy Level matrix position by: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->mPower * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               13,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Ignore the following bands: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        for ( int iter = 0; iter < static_cast<int> ( this->ignoreLs.size() ); iter++ )
        {
            hlpSS2 << std::showpos << this->ignoreLs.at(iter) << " ";
        }
        hlpSS << "     " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               14,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Compute Energy Level distances: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->energyLevelDist == 0 ) { hlpSS2 << " FALSE"; }
        else { hlpSS2 << "  TRUE"; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               15,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Compute Trace Sigma distances: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->traceSigmaDist == 0 ) { hlpSS2 << " FALSE"; }
        else { hlpSS2 << "  TRUE"; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               16,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Compute Rotation Function distances: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->fullRotFnDist == 0 ) { hlpSS2 << " FALSE"; }
        else { hlpSS2 << "  TRUE"; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               17,
                                               0,
                                               1,
                                               1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Use phase information: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str ( std::string ( ) );
        if ( this->usePhase == 0 ) { hlpSS2 << " FALSE"; }
        else { hlpSS2 << "  TRUE"; }
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "DBSettingsSection",
                                               18,
                                               0,
                                               1,
                                               1 );
        
        rvapi_flush                           ( );
    }
    
    //======================================== Database settings check
    if ( !settings->usePhase )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Tried to compare map fragments to database created without phases. This would mean comparing fragments of Patterson-like maps and this is not something that one should do... Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Tried to compare map fragments to database created without phases. This is not allowed and it looks like an internal bug. Please report this case." << "</font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Now, create the one structure to be fragmented
    ProSHADE_data* one                        = new ProSHADE_data ();
    
    //======================================== Read in into one
    matchedStrNames->clear                    ( );
    matchedStrNames->emplace_back             ( structFiles.at(0) );
    
    unsigned int fileType                     = checkFileType ( structFiles.at(0) );
    if ( fileType == 2 )
    {
        one->getDensityMapFromMAP             ( this->structFiles.at(0),
                                               &this->shellSpacing,
                                                this->mapResolution,
                                               &this->bandwidth,
                                               &this->theta,
                                               &this->phi,
                                               &this->glIntegOrder,
                                               &this->extraSpace,
                                                settings->mapResDefault,
                                                settings->rotChangeDefault,
                                                settings,
                                                settings->overlayDefaults );
    }
    else if ( fileType == 1 )
    {
        one->getDensityMapFromPDB             ( this->structFiles.at(0),
                                               &this->shellSpacing,
                                                this->mapResolution,
                                               &this->bandwidth,
                                               &this->theta,
                                               &this->phi,
                                               &this->glIntegOrder,
                                               &this->extraSpace,
                                                settings->mapResDefault,
                                                settings,
                                                this->bFactorValue,
                                                this->firstLineCOM );
    }
    else
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error loading file " << this->structFiles.at(0) << " !!! Cannot detect the extension (currently, only PDB or MAP are allowed) and therefore cannot read the file." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Cannot open file " << this->structFiles.at(0) << " .</font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }
        
        exit ( -1 );
    }
    if ( settings->verbose > 2 )
    {
        std::cout << ">>>>> Structure 0 read from file." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "The structure for fragmenting loaded." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        
        rvapi_flush                           ( );
    }
    
    //======================================== Deal with the centering and MAP data format, if applicable
    one->keepPhaseInMap                       ( this->alpha,
                                                this->bFactorChange,
                                               &this->bandwidth,
                                               &this->theta,
                                               &this->phi,
                                               &this->glIntegOrder,
                                                settings,
                                                this->useCOM,
                                                settings->noIQRsFromMap,
                                                settings->verbose,
                                                settings->clearMapData,
                                                settings->rotChangeDefault,
                                                settings->overlayDefaults,
                                                settings->maskBlurFactor,
                                                false );
    
    //======================================== Fragment
    std::vector<ProSHADE_data*> frags         =  one->fragmentMap ( settings, userCOM );

    if ( settings->verbose > 0 )
    {
        std::cout << "Structure 0 fragmented." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Fragments obtained." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
        settings->htmlReportLineProgress     += 1;
        
        rvapi_flush                           ( );
    }
    
    //======================================== Check for not finding any size matches
    if ( frags.size() == 0 )
    {
        std::cout << "!!! ProSHADE WARNING !!! Did not find any fragmentation boxes fitting the requirements. Either decrease the box size (to have more and higher chance of reaching the minimal density fraction) or change the minimal density fraction (the -mFrag parameter) to let more boxes pass." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"orange\">" << "Did not find any fragmentation boxes fitting the requirements. Either decrease the box size (to have more and higher chance of reaching the minimal density fraction) or change the minimal density fraction (the -mFrag parameter) to let more boxes pass." << "</font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Determine size limits on database over all boxes
    double volTolerance                       = ( 1.0 - settings->volumeTolerance );
    double totMinVol                          = ( ( static_cast<double> ( frags.at(0)->_maxMapU ) * volTolerance ) *
                                                  ( static_cast<double> ( frags.at(0)->_maxMapV ) * volTolerance ) *
                                                  ( static_cast<double> ( frags.at(0)->_maxMapW ) * volTolerance ) );
    double volMin                             = totMinVol;
    for ( unsigned int iter = 1; iter < static_cast<unsigned int> ( frags.size() ); iter++ )
    {
        volMin                                = ( ( static_cast<double> ( frags.at(iter)->_maxMapU ) * volTolerance ) *
                                                  ( static_cast<double> ( frags.at(iter)->_maxMapV ) * volTolerance ) *
                                                  ( static_cast<double> ( frags.at(iter)->_maxMapW ) * volTolerance ) );
        totMinVol                             = std::min ( volMin, totMinVol );
    }
    
    volTolerance                              = ( 1.0 + settings->volumeTolerance );
    double totMaxVol                         = ( ( static_cast<double> ( frags.at(0)->_maxMapU ) * volTolerance ) *
                                                  ( static_cast<double> ( frags.at(0)->_maxMapV ) * volTolerance ) *
                                                  ( static_cast<double> ( frags.at(0)->_maxMapW ) * volTolerance ) );
    double volMax                             = totMaxVol;
    for ( unsigned int iter = 1; iter < static_cast<unsigned int> ( frags.size() ); iter++ )
    {
        volMax                                = ( ( static_cast<double> ( frags.at(iter)->_maxMapU ) * volTolerance ) *
                                                  ( static_cast<double> ( frags.at(iter)->_maxMapV ) * volTolerance ) *
                                                  ( static_cast<double> ( frags.at(iter)->_maxMapW ) * volTolerance ) );
        totMaxVol                             = std::max ( volMax, totMaxVol );
    }
    
    if ( settings->htmlReport )
    {
        //==================================== Create section
        rvapi_add_section                     ( "FragmentSection",
                                                "Fragment processing",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                false );
        settings->htmlReportLine             += 1;
        
        rvapi_flush                           ( );
    }
    
    //======================================== Complete the data processing for each map fragment
    for ( unsigned int frIt = 0; frIt < static_cast<unsigned int> ( frags.size() ); frIt++ )
    {
        if ( settings->verbose > 3 )
        {
            std::cout << ">>>>>>>> Computing spherical harmonics for fragment " << frIt << " out of " << frags.size() << "." << std::endl;
        }

        //==================================== Map the density onto spheres
        frags.at(frIt)->mapPhaselessToSphere  ( settings,
                                                frags.at(frIt)->_thetaAngle,
                                                frags.at(frIt)->_phiAngle,
                                                one->_shellSpacing,
                                                settings->manualShells );
        
        //==================================== Compute the Spherical Harmonics (SH) coefficients
        frags.at(frIt)->getSphericalHarmonicsCoeffs ( frags.at(frIt)->_bandwidthLimit, settings );
        
        //==================================== If required, pre-compute the energy levels distance descriptor
        if ( this->energyLevelDist )
        {
            frags.at(frIt)->precomputeRotInvDescriptor ( settings );
        }
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "Fragment " << frIt << " spherical harmonics computation done.";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "FragmentSection",
                                                frIt,
                                                1,
                                                1,
                                                1 );
            
            rvapi_flush                       ( );
        }

    }
    if ( settings->verbose > 1 )
    {
        std::cout << ">> Spherical harmonics computed for " << static_cast<unsigned int> ( frags.size() ) << " fragments." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Spherical harmonics computed for " << static_cast<unsigned int> ( frags.size() ) << " fragments." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
        settings->htmlReportLineProgress     += 1;
        
        rvapi_flush                           ( );
    }

    //======================================== Read in the database data into ProSHADE_data objects
    std::vector<ProSHADE_data*> dbData;
    for ( unsigned int strIt = 0; strIt < static_cast<unsigned int> ( dbStrNames.size() ); strIt++ )
    {
        if ( settings->verbose > 2 )
        {
            std::cout << ">>>>> Loading structure " << strIt << " from the database." << std::endl;
        }
        
        dbData.emplace_back                   ( new ProSHADE_data ( &dbFile, dbStrNames.at(strIt), totMinVol, totMaxVol, settings->verbose, settings ) );
        if ( dbData.at(dbData.size()-1)->_mapResolution == -999.9 )
        {
            dbData.pop_back                   ( );
        }
        else
        {
            matchedStrNames->emplace_back     ( dbStrNames.at(strIt) );
        }
    }

    //======================================== Check for not finding any size matches
    if ( dbData.size() == 0 )
    {
        std::cout << "!!! ProSHADE WARNING !!! There was no database entry which would pass the size limitations and therefore cannot compute distances. This could be caused by submitting database file with small number or very narrowly distributed structure sizes. Alternatively, the size limit parameter ( \'--dbSizeLim\' ) could be set to too constricted values (or the default values are sub-optimal for this case). Now terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"orange\">" << "There was no database entry which would pass the size limitations and therefore cannot compute distances. This could be caused by submitting database file with small number or very narrowly distributed structure sizes. Alternatively, the size limit parameter ( \'--dbSizeLim\' ) could be set to too constricted values (or the default values are sub-optimal for this case)." << "</font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }
        
        exit ( 0 );
    }
    
    if ( settings->htmlReport )
    {
        //==================================== Create section
        rvapi_add_section                     ( "FragmentDistancesSection",
                                                "Fragment distances computation",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                false );
        settings->htmlReportLine             += 1;
        
        rvapi_flush                           ( );
    }
    
    for ( unsigned int frIt = 0; frIt < static_cast<unsigned int> ( frags.size() ); frIt++ )
    {
        if ( settings->verbose > 0 )
        {
            std::cout << "Computing distances for fragment " << frIt << " ." << std::endl;
        }
        
        //==================================== Create the compare against all object
        this->cmpObj                          = new ProSHADE_compareOneAgainstAll ( frags.at(frIt), &dbData, this->ignoreLs, this->mPower, settings->verbose );
        
        //==================================== Save the decision whether to use phases or not
        if ( frags.at(frIt)->_keepOrRemove ) { this->cmpObj->_keepOrRemove = true; }
        else                                 { this->cmpObj->_keepOrRemove = false; }
        
        //==================================== If required, compute the energy levels distances
        if ( this->energyLevelDist )
        {
            this->energyLevelsDistances       = this->cmpObj->getEnergyLevelsDistance ( settings->verbose, settings );
            this->fragEnergyLevelsDistances.emplace_back ( this->energyLevelsDistances );
            
            // ... and set which pairs not to follow further
            if ( this->enLevelsThreshold != -999.9 )
            {
                for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->cmpObj->all->size() ); iter++ )
                {
                    if ( this->enLevelsThreshold > this->energyLevelsDistances.at(iter) )
                    {
                        this->cmpObj->_enLevelsDoNotFollow.emplace_back ( 1 );
                    }
                    else
                    {
                        this->cmpObj->_enLevelsDoNotFollow.emplace_back ( 0 );
                    }
                }
            }
            else
            {
                for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->cmpObj->all->size() ); iter++ )
                {
                    this->cmpObj->_enLevelsDoNotFollow.emplace_back ( 0 );
                }
            }
        }
        else
        {
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->cmpObj->all->size() ); iter++ )
            {
                this->cmpObj->_enLevelsDoNotFollow.emplace_back ( 0 );
            }
        }
        
        if ( settings->verbose > 1 )
        {
            std::cout << ">> Cross-correlation distances computed ." << std::endl;
        }
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Energy Level distances computed for fragment " << frIt << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "FragmentDistancesSection",
                                                ( frIt * 3 ) + 0,
                                                1,
                                                1,
                                                1 );
            
            rvapi_flush                       ( );
        }
        
        //==================================== Sort out the integration order as this is not kept within the data objects...
        this->glIntegOrderVec                 = std::vector<unsigned int> ( dbData.size() + 1 );
        this->glIntegOrderVec.at(0)           = this->glIntegOrder;
        double distPerPointFraction           = 0.0;
        for ( unsigned int strIt = 0; strIt < static_cast<unsigned int> ( dbData.size() ); strIt++ )
        {
            distPerPointFraction              = static_cast<double> ( dbData.at(strIt)->_shellSpacing ) / ( dbData.at(strIt)->_maxMapRange / 2.0 );
            
            for ( unsigned int iter = 2; iter < static_cast<unsigned int> ( ProSHADE_internal_misc::glIntMaxDists.size() ); iter++ )
            {
                if ( ProSHADE_internal_misc::glIntMaxDists.at(iter) >= distPerPointFraction )
                {
                    this->glIntegOrderVec.at(strIt+1) = iter;
                }
            }
        }
        
        //==================================== Pre-compute the E matrices in case either the trace sigmal or full rotation distance are required
        if ( this->traceSigmaDist || this->fullRotFnDist )
        {
            this->cmpObj->precomputeTrSigmaDescriptor ( this->shellSpacing, &this->glIntegOrderVec, settings );
        }
        
        //==================================== Compute the trace sigma distances, if required
        if ( this->traceSigmaDist )
        {
            this->traceSigmaDistances         = this->cmpObj->getTrSigmaDistance ( settings->verbose, settings );
            this->fragTraceSigmaDistances.emplace_back ( this->traceSigmaDistances );
            
            if ( this->trSigmaThreshold == -999.9 )
            {
                this->cmpObj->_trSigmaDoNotFollow = this->cmpObj->_enLevelsDoNotFollow;
            }
            else
            {
                for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->traceSigmaDistances.size() ); iter++ )
                {
                    if ( this->trSigmaThreshold > this->traceSigmaDistances.at(iter) )
                    {
                        this->cmpObj->_trSigmaDoNotFollow.emplace_back ( 1 );
                    }
                    else
                    {
                        this->cmpObj->_trSigmaDoNotFollow.emplace_back ( 0 );
                    }
                }
            }
        }
        else
        {
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->traceSigmaDistances.size() ); iter++ )
            {
                this->cmpObj->_trSigmaDoNotFollow.emplace_back ( 0 );
            }
        }
        
        if ( settings->verbose > 1 )
        {
            std::cout << ">> Trace sigma distances computed ." << std::endl;
        }
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Trace Sigma distances computed for fragment " << frIt << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "FragmentDistancesSection",
                                               ( frIt * 3 ) + 1,
                                               1,
                                               1,
                                               1 );
            
            rvapi_flush                       ( );
        }
        
        //==================================== If full rotation distance is required, pre-compute the requirements
        if ( this->fullRotFnDist )
        {
            this->cmpObj->getSO3InverseMap    ( settings );
            this->cmpObj->getEulerAngles      ( settings );
            this->cmpObj->generateWignerMatrices ( settings );
            this->fullRotationDistances       = this->cmpObj->getRotCoeffDistance ( settings->verbose, settings );
            this->fragfullRotationDistances.emplace_back ( this->fullRotationDistances );
        }
        if ( settings->verbose > 1 )
        {
            std::cout << ">> Rotation function distances computed ." << std::endl;
        }
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Rotation Function distances computed for fragment " << frIt << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "FragmentDistancesSection",
                                               ( frIt * 3 ) + 2,
                                               1,
                                               1,
                                               1 );
            
            rvapi_flush                       ( );
        }
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Fragment distances computed." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        
        rvapi_flush                           ( );
    }
    
    //======================================== Done
    return ;
}


/*! \brief Overloaded constructor for the ProSHADE_data class using the database file.
 
 This is a special constructor for the ProSHADE_data class. It takes a pointer to an opened ifstream binary file
 and reads all the information to fill in a ProSHADE_data object from it, assuming the binary data have the correct
 format - this basically means that they were produced by the saveDatabase function.
 
 \param[in] dbFile Pointer to an opened file for finary reading with the database data.
 \param[in] fName The filename of the structure to be read in.
 \param[in] volThreMin The minimal volume the structure needs to have to be read in instead of just skipped.
 \param[in] volThreMax The maximum valume the structure needs to have to be read in instead of just skipped.
 \param[in] verbose Determines how loud the standard output should be.
 \param[in] settings An instance of the ProSHADE_settings class for allowing the HTML report.
 \param[out] X An object with all the data loaded from the database file and ready for further processing.
 
 \warning This is an internal function which should not be used by the user.
*/
ProSHADE_internal::ProSHADE_data::ProSHADE_data ( std::ifstream* dbFile,
                                                  std::string fName,
                                                  double volThreMin,
                                                  double volThreMax,
                                                  int verbose,
                                                  ProSHADE::ProSHADE_settings* settings,
                                                  bool saveWithAndWithout )
{
    //======================================== Sanity check
    if ( dbFile->fail() )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Cannot read from input database file. Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Cannot read from the database file. This looks like file corruption or internal bug, if repeating with a new database file does not help, please report this case." << "</font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Initialise object to null
    this->_inputFileName                      = fName;
    this->_densityMapMap                      = nullptr;
    this->_densityMapCor                      = nullptr;
    this->_densityMapCorCoords                = nullptr;
    this->_shellMappedData                    = nullptr;
    this->_realSHCoeffs                       = nullptr;
    this->_imagSHCoeffs                       = nullptr;
    this->_sphericalHarmonicsWeights          = nullptr;
    this->_semiNaiveTable                     = nullptr;
    this->_semiNaiveTableSpace                = nullptr;
    this->_shWorkspace                        = nullptr;
    this->_rrpMatrices                        = nullptr;
    this->_invRealData                        = nullptr;
    this->_invImagData                        = nullptr;
    
    this->_densityMapComputed                 = false;
    this->_phaseRemoved                       = false;
    this->_usePhase                           = false;
    this->_firstLineCOM                       = false;
    this->_sphereMapped                       = false;
    this->_sphericalCoefficientsComputed      = false;
    this->_rrpMatricesPrecomputed             = false;
    this->_wasBandwithGiven                   = true;
    this->_wasThetaGiven                      = true;
    this->_wasPhiGiven                        = true;
    this->_wasGlInterGiven                    = true;
    
    this->_xCorrection                        = 0;
    this->_yCorrection                        = 0;
    this->_zCorrection                        = 0;
    
    this->_mapMean                            = 0.0;
    this->_mapSdev                            = 1.0;
    
    //======================================== Declare local variables
    unsigned int hlpUInt;
    double hlpD;

    //======================================== Read in the structure settings
    dbFile->read ( reinterpret_cast<char*>    ( &this->_fromPDB                                              ), sizeof ( bool         ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_shellSpacing                                         ), sizeof ( double       ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_maxExtraCellularSpace                                ), sizeof ( double       ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_xRange                                               ), sizeof ( double       ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_yRange                                               ), sizeof ( double       ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_zRange                                               ), sizeof ( double       ) );
    
    dbFile->read ( reinterpret_cast<char*>    ( &hlpUInt                                                     ), sizeof ( unsigned int ) );
    for ( unsigned int it = 0; it < hlpUInt; it++ )
    {
        dbFile->read ( reinterpret_cast<char*> ( &hlpD                                                       ), sizeof ( double       ) );
        this->_shellPlacement.emplace_back    ( hlpD );
    }
    
    dbFile->read ( reinterpret_cast<char*>    ( &this->_mapResolution                                        ), sizeof ( double       ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_maxMapU                                              ), sizeof ( unsigned int ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_maxMapV                                              ), sizeof ( unsigned int ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_maxMapW                                              ), sizeof ( unsigned int ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_densityMapComputed                                   ), sizeof ( bool         ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_mapMean                                              ), sizeof ( double       ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_mapSdev                                              ), sizeof ( double       ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_fourierCoeffPower                                    ), sizeof ( double       ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_bFactorChange                                        ), sizeof ( double       ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_maxMapRange                                          ), sizeof ( double       ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_phaseRemoved                                         ), sizeof ( bool         ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_usePhase                                             ), sizeof ( bool         ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_keepOrRemove                                         ), sizeof ( bool         ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_thetaAngle                                           ), sizeof ( double       ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_phiAngle                                             ), sizeof ( double       ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_noShellsWithData                                     ), sizeof ( unsigned int ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_xCorrection                                          ), sizeof ( unsigned int ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_yCorrection                                          ), sizeof ( unsigned int ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_zCorrection                                          ), sizeof ( unsigned int ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_xCorrErr                                             ), sizeof ( double       ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_yCorrErr                                             ), sizeof ( double       ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_zCorrErr                                             ), sizeof ( double       ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_sphereMapped                                         ), sizeof ( bool         ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_firstLineCOM                                         ), sizeof ( bool         ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_bandwidthLimit                                       ), sizeof ( unsigned int ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_oneDimmension                                        ), sizeof ( unsigned int ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_sphericalCoefficientsComputed                        ), sizeof ( bool         ) );
    dbFile->read ( reinterpret_cast<char*>    ( &this->_rrpMatricesPrecomputed                               ), sizeof ( bool         ) );
    
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> Settings loaded." << std::endl;
    }
    
    //======================================== Check the volume
    if ( !( ( ( (this->_xRange-this->_maxExtraCellularSpace) * (this->_yRange-this->_maxExtraCellularSpace) * (this->_zRange-this->_maxExtraCellularSpace) ) > volThreMin ) &&
            ( ( (this->_xRange-this->_maxExtraCellularSpace) * (this->_yRange-this->_maxExtraCellularSpace) * (this->_zRange-this->_maxExtraCellularSpace) ) < volThreMax ) ) )
    {
        //==================================== Set flag to ignore this structure further
        this->_mapResolution                  = -999.9;
    }
    
    //======================================== Initialise the spherical harmonics coefficient arrays
    this->_realSHCoeffs                       = new double* [this->_noShellsWithData];
    for ( unsigned int i = 0; i < this->_noShellsWithData; i++ ) { this->_realSHCoeffs[i] = new double [this->_oneDimmension * this->_oneDimmension]; }
    this->_imagSHCoeffs                       = new double* [this->_noShellsWithData];
    for ( unsigned int i = 0; i < this->_noShellsWithData; i++ ) { this->_imagSHCoeffs[i] = new double [this->_oneDimmension * this->_oneDimmension]; }
    
    //======================================== Read in the spherical harmonics coefficient arrays
    for ( unsigned int sh = 0; sh < this->_noShellsWithData; sh++ )
    {
        for ( unsigned int arrIt = 0; arrIt < ( this->_oneDimmension * this->_oneDimmension ); arrIt++ )
        {
            dbFile->read ( reinterpret_cast<char*> ( &this->_realSHCoeffs[sh][arrIt]                      ), sizeof ( double       ) );
            dbFile->read ( reinterpret_cast<char*> ( &this->_imagSHCoeffs[sh][arrIt]                      ), sizeof ( double       ) );
        }
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> Spherical harmonics loaded." << std::endl;
    }
    
    //======================================== Initialise the RRP matrices data arrays
    this->_rrpMatrices                        = new double** [this->_bandwidthLimit];
    for ( unsigned int bwIt = 0; bwIt < this->_bandwidthLimit; bwIt++ )
    {
        //==================================== Odd bands are 0, so just ignore them
        if ( !this->_keepOrRemove ) { if ( ( bwIt % 2 ) != 0 ) { continue; } }
        
        this->_rrpMatrices[bwIt]              = new double* [this->_noShellsWithData];
        for ( unsigned int shIt = 0; shIt < this->_noShellsWithData; shIt++ )
        {
            this->_rrpMatrices[bwIt][shIt]    = new double [this->_noShellsWithData];
        }
    }
    
    //======================================== Read in the RRP matrices data arrays
    for ( unsigned int bwIt = 0; bwIt < this->_bandwidthLimit; bwIt++ )
    {
        //==================================== Odd bands are 0, so just ignore them
        if ( !this->_keepOrRemove ) { if ( ( bwIt % 2 ) != 0 ) { continue; } }
        
        for ( unsigned int sh1 = 0; sh1 < this->_noShellsWithData; sh1++ )
        {
            for ( unsigned int sh2 = 0; sh2 < this->_noShellsWithData; sh2++ )
            {
                dbFile->read ( reinterpret_cast<char*> ( &this->_rrpMatrices[bwIt][sh1][sh2]              ), sizeof ( double       ) );
            }
        }
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> RRP Matrices loaded." << std::endl;
    }
    
    //======================================== Normalise back to usual meaning
    if ( saveWithAndWithout )
    {
        if ( this->_usePhase  )
        {
            dbFile->read ( reinterpret_cast<char*> ( &this->_xFrom                                        ), sizeof ( double       ) );
            dbFile->read ( reinterpret_cast<char*> ( &this->_xTo                                          ), sizeof ( double       ) );
            dbFile->read ( reinterpret_cast<char*> ( &this->_yFrom                                        ), sizeof ( double       ) );
            dbFile->read ( reinterpret_cast<char*> ( &this->_yTo                                          ), sizeof ( double       ) );
            dbFile->read ( reinterpret_cast<char*> ( &this->_zFrom                                        ), sizeof ( double       ) );
            dbFile->read ( reinterpret_cast<char*> ( &this->_zTo                                          ), sizeof ( double       ) );
            
            this->_densityMapCor              = new double [(this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1)];
            for ( unsigned int mIt = 0; mIt < static_cast<unsigned int> ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) ); mIt++ )
            {
                dbFile->read ( reinterpret_cast<char*> ( &this->_densityMapCor[mIt]                       ), sizeof ( double       ) );
            }
        }
    }
    
    this->_xSamplingRate                      = this->_xRange / static_cast<double> ( this->_maxMapU );
    this->_ySamplingRate                      = this->_yRange / static_cast<double> ( this->_maxMapV );
    this->_zSamplingRate                      = this->_zRange / static_cast<double> ( this->_maxMapW );
    
    //======================================== Done
    return ;
}
