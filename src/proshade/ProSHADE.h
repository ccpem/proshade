/*! \file ProSHADE.h
 \brief The main header file containing all declarations the user of the library needs.
 
 This is the header file containig declarations of all classes
 and functions the user needs to run and use the results of the
 ProSHADE library. This is the only file in the library that needs to
 be included in any software which intends to use the library.
 
 This file is part of the ProSHADE library for calculating
 shape descriptors and symmetry operators of protein structures.
 This is a prototype code, which is by no means complete or fully
 tested. Its use is at your own risk only. There is no quarantee
 that the results are correct.
 
 \author    Michal Tykac
 \author    Garib N. Murshudov
 \version   0.6.6
 \date      JAN 2019
 */

//============================================ Apply -pedantic and -Wshadow to my code only (some of the dependencies fill not compile if this is allowed for all)
//#define __PROSHADE_DEVEL_CODE__

//============================================ Include once
#ifndef __PROSHADE_LIBRARY__
#define __PROSHADE_LIBRARY__

//============================================ Standard library
#include <iostream>
#include <fstream>
#include <sstream>
#include <iterator>
#include <string>
#include <vector>
#include <array>
#include <numeric>
#include <functional>
#include <algorithm>
#include <ctime>
#include <chrono>
#include <memory>
#include <exception>
#include <getopt.h>
#include <limits>
#include <string.h>
#include <cmath>
#include <sys/stat.h>

//============================================ Development compilation pragmas
#ifdef __PROSHADE_DEVEL_CODE__
#pragma GCC diagnostic error "-Wshadow"
#pragma GCC diagnostic error "-Wpedantic"
#endif

typedef double fftw_complex[2];

/*! \namespace ProSHADE
 \brief This namespace contains all the external objects and their forward declarations.
 
 The ProSHADE namespace wraps around all the objects that are needed to use the library.
 */
namespace ProSHADE
{
    //======================================== The Task data type
    enum Task { NA, Symmetry, Distances, DistancesFrag, Features, BuildDB, HalfMaps, RotateMap, OverlayMap, SimpleRebox };
    
    /*! \class ProSHADE_settings
     \brief This class stores all the settings and is passed to the executive classes instead of multitude of parameters.
     
     The ProSHADE_settings class is a simple way of keeping all the settings together and easy to set by the user. Its
     constructor sets it to the default settings, so that if the user does not want to change these, he just needs to
     pass the object as is created and all is done.
     */
    class ProSHADE_settings
    {
    public:
        //==================================== Settings regarding resolutions
        double                                mapResolution;                       //!< This is the internal resolution at which the calculations are done, not necessarily the resolution of the data.
        bool                                  wasResolutionGiven;                  //!< Variable stating whether the resolution value was given by the user, or decided automatically.
        unsigned int                          bandwidth;                           //!< This parameter determines the angular resolution of the spherical harmonics decomposition.
        bool                                  wasBandGiven;                        //!< Variable stating whether the bandwidth value was given by the user, or decided automatically.
        unsigned int                          glIntegOrder;                        //!< This parameter controls the Gauss-Legendre integration order and so the radial resolution.
        unsigned int                          theta;                               //!< This parameter is the longitude of the spherical grid mapping. It should be 2 * bandwidth unless there is a specific reason to change it.
        unsigned int                          phi;                                 //!< This parameter is the latitudd of the spherical grid mapping. It should be 2 * bandwidth unless there is a specific reason to change it.
        bool                                  mapResDefault;                       //!< This variable states if default resolution should be used, or whether the user has supplied a different value.
        
        //==================================== Settings regarding B factors
        double                                bFactorValue;                        //!< This is the value to which all B-factors of PDB files will be changed to.
        double                                bFactorChange;                       //!< This value will be used to change the B-factors if required by the user.
        bool                                  wasBChangeGiven;                     //!< Variable stating whether the B factor change (sharpening/blurring) value was given by the user, or decided automatically.
        
        //==================================== Setting regarding maps and removing noise
        double                                noIQRsFromMap;                       //!< This is the number of interquartile distances from mean that is used to threshold the map masking.
        
        //==================================== Settings regarding concentric shells
        double                                shellSpacing;                        //!< This parameter determines how far the radial shells should be from each other.
        bool                                  wasShellSpacingGiven;                //!< Variable stating whether the distance between shells value was given by the user, or decided automatically.
        unsigned int                          manualShells;                        //!< Should the user require so, the maximum number of radial shells can be set.
        
        //==================================== Settings regarding phase
        bool                                  usePhase;                            //!< Here the user can decide whether to use phase information or whether to ignore it completely.
        bool                                  saveWithAndWithout;                  //!< This option decides whether both with and without phase spherical harmonics should be saved, or whether only the values decided by the above usePhase variable will be saved.
        
        //==================================== Settings regarding map with phases
        bool                                  useCOM;                              //!< Should the Centre of Mass (COM) be used to center the structure in the cell?
        bool                                  firstLineCOM;                        //!< This is a special option for metal detection, please leave false.
        
        //==================================== Settings regarding space around the structure in lattice
        double                                extraSpace;                          //!< What should be the distance added on both sides to the structure, so that the next cell density would not affect the results?
        bool                                  wasExtraSpaceGiven;                  //!< Variable stating whether the extra cell space value was given by the user, or decided automatically.
        
        //==================================== Settings regarding weighting the distances
        double                                alpha;                               //!< This parameter determines the power to which the |F|'s should be raised.
        double                                mPower;                              //!< This parameter determines the scaling for trace sigma descriptor.
        
        //==================================== Settings regarding bands to ignore
        std::vector<int>                      ignoreLs;                            //!< This vector lists all the bandwidth values which should be ignored and not part of the computations.
        
        //==================================== Settings regarding the structures to use
        std::vector <std::string>             structFiles;                         //!< This vector should contain all the structures that are being dealt with, but this does not yet work!
        
        //==================================== Settings regarding peak finding
        double                                peakHeightNoIQRs;                    //!< How many interquartile ranges should be used to distinguish 'false' peaks from the true ones?
        double                                peakDistanceForReal;                 //!< Threshold for determining 'missing peaks' existence.
        int                                   peakSurroundingPoints;               //!< For a peak to exist, how many points in every direction need to be smalled than the middle value?
        
        //==================================== Settings regarding axis and angle error tolerance
        double                                aaErrorTolerance;                    //!< The tolerance parameter on matching axes for the angle-axis representation of rotations.
        double                                symGapTolerance;                     //!< For C-symmetries - if there are many, only those with average peak height - parameter * top symmetry value will be shown in the output.
        
        //==================================== Settings regarding which distances to compute
        bool                                  energyLevelDist;                     //!< Should the energy level distances descriptor be computed.
        bool                                  traceSigmaDist;                      //!< Should the trace sigma distances descriptor be computed.
        bool                                  fullRotFnDist;                       //!< Should the full rotation function distances descriptor be computed.
        
        //==================================== Settings regarding thresholds for hierarchical distance computation
        double                                enLevelsThreshold;                   //!< All structure pairs with energy level descriptor value less than this will not be subjected to any further descriptor computations.
        double                                trSigmaThreshold;                    //!< All structure pairs with trace sigma descriptor value less than this will not be subjected to any further descriptor computations.
        
        //==================================== Settings regarding which of the task to perform
        Task                                  taskToPerform;                       //!< This custom type variable determines which task to perfom (i.e. symmetry detection, distances computation or map features extraction).
        
        //==================================== Settings regarding where and if to save the clear map
        std::string                           clearMapFile;                        //!< If map features are to be extracted, should the clear map be saved (then give file name here), or not (then leave empty).
        bool                                  useCubicMaps;                        //!< When saving clear maps, should the rectangular or cubic (older versions of refmac need this) maps be produced?
        bool                                  clearMapData;                        //!< This value is used to decide whether the input maps should be cleared again, or not.
        double                                maskBlurFactor;                      //!< The is the amount of blurring to be used to create masks for maps.
        bool                                  maskBlurFactorGiven;                 //!< Was a specific value of the blurring factor requested by the user?
        
        //==================================== Settings regarding map fragmentation
        double                                mapFragBoxSize;                      //!< Should the clear map be fragmented into boxes? If so, put box size here, otherwise leave 0.
        std::string                           mapFragName;                         //!< The prefix of the files with the cut out boxes.
        double                                mapFragBoxFraction;                  //!< Fraction of box that needs to have density in order to be passed on.
        
        //==================================== Settings regarding structure database
        std::string                           databaseName;                        //!< The name of the bin file to which the database should be saved.
        double                                databaseMinVolume;                   //!< The smallest volume of a structure in the database.
        double                                databaseMaxVolume;                   //!< The largest volume allowed to exist in the database.
        double                                volumeTolerance;                     //!< The percentage tolerance on each dimmension when comparing one structure to entire database.
        
        //==================================== Settings regarding the symmetry type required
        unsigned int                          symmetryFold;                        //!< The required fold of the sought symmetry. Applicable to C and D symmetries, otherwise leave 0.
        std::string                           symmetryType;                        //!< The required symmetry type. If no symmetry is required, leave empty. Possible values are: C, D, T, O and I.
        
        //==================================== Settings regarding the map rotation mode
        double                                rotAngle;                            //!< The angle of the rotation to be done to the map structure in the map rotation mode.
        double                                rotXAxis;                            //!< The X-axis element of the rotation axis along which the rotation is to be done in the map rotation mode.
        double                                rotYAxis;                            //!< The Y-axis element of the rotation axis along which the rotation is to be done in the map rotation mode.
        double                                rotZAxis;                            //!< The Z-axis element of the rotation axis along which the rotation is to be done in the map rotation mode.
        bool                                  rotChangeDefault;                    //!< If map rotation is selected, the default automatic parameter decision is changed. This variable states if this should be done.
        double                                xTranslation;                        //!< The number of angstroms by which the structure should be translated along the X axis.
        double                                yTranslation;                        //!< The number of angstroms by which the structure should be translated along the Y axis.
        double                                zTranslation;                        //!< The number of angstroms by which the structure should be translated along the Z axis.
        
        //==================================== Settings regarding the map overlay mode
        bool                                  overlayDefaults;                     //!< If true, the shell spacing and distances will be doube to their typical values. This is to speed up map overlay mode.
        bool                                  dbDistOverlay;                       //!< This value is false in all conditions, unless programatically changed. If changed, distance computations will use the phaseless database to compute overlay before distances computation. This is computationally expensive and requires phaseless as well as phased database.
        unsigned int                          maxRotError;                         //!< This is the maximum allowed error in degrees for the rotation computation. This can be used to speed up the overlay computation, but should be off by default.
        std::vector<std::string>              deleteModels;                        //!< The filenames listed here consist of models which should have their density deleted from the map before matching.
        
        //==================================== Settings regarding the map saving mode
        std::string                           axisOrder;                           //!< A string specifying the order of the axis. Must have three characters and any permutation of 'x', 'y' and 'z' in it.
        
        //==================================== Settings regarding the RVAPI path
        std::string                           rvapiPath;                           //!< A string specifying the path to the RVAPI install folder. If not empty, this supersedes the path saved in the ProSHADE_rvapi.h file.
        
        //==================================== Settings regarding verbosity of the program
        bool                                  htmlReport;                          //!< Should HTML report for the run be created?
        int                                   htmlReportLine;                      //!< Iterator for current HTML line
        int                                   htmlReportLineProgress;              //!< Iterator for current HTML line in the progress bar
        int                                   verbose;                             //!< Should the software report on the progress, or just be quiet? Value between 0 (quiet) and 4 (loud)
        
    public:
        //==================================== Public functions available to user: Constructor and destructor
        ProSHADE_settings                                                ( );
        
        //==================================== Public functions available to user: Pecularities
        void                                  printSettings              ( );
        void                                  getCommandLineParams       ( int argc, char* argv[] );
        void                                  ignoreLsAddValuePy         ( const int val );
        void                                  appendStructure            ( std::string str );
        double                                modelRadius                ( std::string modelPath );
    };
    
    /*! \class ProSHADE
     \brief This class provides the access point to the library.
     
     This class codes the object that the user of the library needs to create (and presumably delete) in order to get access to the ProSHADE library. If the user is calling anyting else than the
     constructor and accessor functions of this class, something is not done right in the library or by the user.
     */
    class ProSHADE
    {
        //==================================== Friends
        friend class ProSHADE_settings;
        
    private:
        //==================================== Keep settings object
        ProSHADE_settings*                    settings;                          //!< This is a pointer to the original settings object used to create this ProSHADE run.
        
        //==================================== Decision bools
        bool                                  distancesAvailable;                //!< If true, the accessor functions for distances will work, otherwise they will fail.
        bool                                  symmetriesAvailable;               //!< If true, the accessor functions for symmetries will work, otherwise they will fail.
        bool                                  fragmentsAvailable;                //!> If true, the accessor functions for fragmentation will work, otherwise they will fail.
        
        //==================================== Values from the ProSHADE run
        std::vector<double>                   crossCorrDists;                    //!< Vector of Cross-correlation distances from the first file to all other files
        std::vector<double>                   traceSigmaDists;                   //!< Vector of Cross-correlation distances from the first file to all other files
        std::vector<double>                   rotFunctionDists;                  //!< Vector of Cross-correlation distances from the first file to all other files
        std::vector< std::vector< double > >  cyclicSymmetries;                  //!< A vector of arrays describing all the cyclic symmetries found in the data.
        std::vector< std::array<double,5> >   cyclicSymmetriesClear;             //!< A vector of arrays describing all the cyclic symmetries found in the data with the recommended one first. This is required for the recommended symmetry functions.
        std::vector< std::vector< std::array<double,6> > > dihedralSymmetries;   //!< A vector of vectors of arrays describing all the dihedral symmetries found in the data.
        std::vector< std::vector< std::array<double,6> > > dihedralSymmetriesClear; //!< A vector of vectors of arrays describing all the dihedral symmetries found in the data with the recommended one first. This is required for the recommended symmetry functions.
        std::vector< std::array<double,5> >   tetrahedralSymmetry;               //!< A vector of arrays describing the tetrahedral symmetry found in the data.
        std::vector< std::array<double,5> >   octahedralSymmetry;                //!< A vector of arrays describing the octahedral symmetry found in the data.
        std::vector< std::array<double,5> >   icosahedralSymmetry;               //!< A vector of arrays describing the icosahedral symmetry found in the data.
        std::vector< std::string >            fragmentList;                      //!< A vector of strings containing the paths to all saved map fragments.
        
    public:
        //==================================== Public functions available to user: Constructor and destructor
        ProSHADE                                                         ( ProSHADE_settings *settings );
        ~ProSHADE                                                        ( void );
        
        //==================================== Public functions available to user: Accessor functions for distances
        std::vector<double>                   getCrossCorrDists          ( void );
        std::vector<double>                   getTraceSigmaDists         ( void );
        std::vector<double>                   getRotFunctionDists        ( void );
        
        //==================================== Public functions available to user: Accessor functions for symmetries (C++)
        std::vector< std::vector< double > >  getCyclicSymmetries        ( void );
        std::vector< std::array<double,5> >   getClearCyclicSymmetries   ( void );
        std::vector< std::vector<std::array< double,6> > > getDihedralSymmetries ( void );
        std::vector< std::vector<std::array< double,6> > > getClearDihedralSymmetries ( void );
        std::vector< std::array<double,5> >   getTetrahedralSymmetries   ( void );
        std::vector< std::array<double,5> >   getOctahedralSymmetries    ( void );
        std::vector< std::array<double,5> >   getIcosahedralSymmetries   ( void );
        std::vector< std::array<double,5> >   getRecommendedSymmetry     ( void );
        std::vector< std::array<double,5> >   getSpecificSymmetryElements ( std::string symType, int symFold = 0 );
        
        //==================================== Public functions available to user: Accessor functions for symmetries (Python)
        std::vector< double >                 getDihedralSymmetriesPy    ( void );
        std::vector< double >                 getTetrahedralSymmetriesPy ( void );
        std::vector< double >                 getOctahedralSymmetriesPy  ( void );
        std::vector< double >                 getIcosahedralSymmetriesPy ( void );
        std::vector< double >                 getRecommendedSymmetryElementsPy ( void );
        std::vector< double >                 getSpecificSymmetryElementsPy ( std::string symType, int symFold = 0 );
        
        //==================================== Public functions available to user: Accessor functions for fragmentation
        std::vector< std::string >            getMapFragments            ( void );
        
        //==================================== Public functions available to user: Miscellaneous
        std::string                           getProSHADEVersion         ( void );
    };
}

//============================================ END
#endif
