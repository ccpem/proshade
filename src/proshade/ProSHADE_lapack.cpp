/*! \file ProSHADE_lapack.cpp
 \brief This file contains all the functions needed to access LAPACK library for linear algebra computations.
 
 The functions defined here make use of the LAPACK-E interface to call the LAPACK library
 directly from C++. This allows fast and numerically stable computation of linear algebra,
 such as the matrix decomposition onto singular values.
 
 This file is part of the ProSHADE library for calculating
 shape descriptors and symmetry operators of protein structures.
 This is a prototype code, which is by no means complete or fully
 tested. Its use is at your own risk only. There is no quarantee
 that the results are correct.
 
 \author    Michal Tykac
 \author    Garib N. Murshudov
 \version   0.6.6
 \date      JAN 2019
 */

//============================================ Lapack-E
#include <complex>
#define lapack_complex_double std::complex<double>
#define lapack_complex_float  std::complex<float>
#define LAPACK_COMPLEX_CUSTOM
#include <lapacke.h>

//============================================ RVAPI
#include <rvapi_interface.h>

//============================================ ProSHADE
#include "ProSHADE.h"
#include "ProSHADE_internal.h"

/*! \brief This function computes the singular value decomposition (SVD) using the LAPACK library.
 
 This function directly accesses the pre-computed E matrices (it assumes they have been computed and they will be as long as
 the ProSHADE library was called appropriately). It then submits these to the LAPACK ZGESDD routine using the LAPACKE C++
 LAPACK interface. The results are checked and the diagonal of the singular values Sigma matrix is returned.
 
 \param[in] band The bandwidth of the E matrices to be submitted to ZGESDD.
 \param[in] dom The dimmensionality of the matrix.
 \param[out] X A vector containing the diagonal values of the Sigma matrix (singular values matrix) as returned by LAPACK.
 
 \warning This function only works with double precision.
 \warning This is an internal function which should not be used by the user.
 */
std::vector<double> ProSHADE_internal::ProSHADE_comparePairwise::getSingularValues ( unsigned int band,
                                                                                     unsigned int dim,
                                                                                     ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Initialise local variables
    double *singularValues                    = new double[dim];
    __extension__ std::complex<double> rotMatU[dim][dim];
    __extension__ std::complex<double> rotMatV[dim][dim];
    std::vector<double> ret;
    int returnValue                           = 0;
    
    //======================================== Load SH coeffs into array in column-major order
    std::complex<double> *matrixToDecompose   = new std::complex<double>[dim*dim];
    for ( unsigned int rowIt = 0; rowIt < dim; rowIt++ )
    {
        for ( unsigned int colIt = 0; colIt < dim; colIt++ )
        {
            matrixToDecompose[(colIt*dim)+rowIt] = std::complex<double> ( this->_trSigmaEMatrix[band][rowIt][colIt][0],
                                                                          this->_trSigmaEMatrix[band][rowIt][colIt][1] );
        }
    }
    
    //======================================== Call the LAPACK ZGESSD function
    returnValue                               = LAPACKE_zgesdd ( LAPACK_COL_MAJOR, 'A', dim, dim, matrixToDecompose, dim, singularValues, *rotMatU, dim, *rotMatV, dim );
    
    //======================================== Check for errors
    if ( returnValue != 0 )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Complex Matrix SVD algorithm did not converge, cannot proceed!" << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "The LAPACK Complex Matrix SVD algorithm did not converge, cannot proceed with computation of Trace Sigma distances. This problem could be resolved by changing the resolution or not using the Trace Sigma distances; these are not good solutions, but they are all I have got right now." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Save results in output format
    for ( unsigned int iter = 0; iter < dim; iter++ )
    {
        ret.emplace_back                      ( singularValues[iter] );
    }
    
    //======================================== Free memory
    delete[] singularValues;
    delete[] matrixToDecompose;
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief This function computes the singular value decomposition (SVD) using the LAPACK library.
 
 This function directly accesses the pre-computed E matrices (it assumes they have been computed and they will be as long as
 the ProSHADE library was called appropriately). It then submits these to the LAPACK ZGESDD routine using the LAPACKE C++
 LAPACK interface. The results are checked and the diagonal of the singular values Sigma matrix is returned.
 
 \param[in] band The bandwidth of the E matrices to be submitted to ZGESDD.
 \param[in] dom The dimmensionality of the matrix.
 \param[out] X A vector containing the diagonal values of the Sigma matrix (singular values matrix) as returned by LAPACK.
 
 \warning This function only works with double precision.
 \warning This is an internal function which should not be used by the user.
 */
std::vector<double> ProSHADE_internal::ProSHADE_compareOneAgainstAll::getSingularValues ( unsigned int strNo, unsigned int band, unsigned int dim, ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Initialise local variables
    double *singularValues                    = new double[dim];
    __extension__ std::complex<double> rotMatU[dim][dim];
    __extension__ std::complex<double> rotMatV[dim][dim];
    std::vector<double> ret;
    int returnValue                           = 0;
    
    //======================================== Load SH coeffs into array in column-major order
    std::complex<double> *matrixToDecompose   = new std::complex<double>[dim*dim];
    for ( unsigned int rowIt = 0; rowIt < dim; rowIt++ )
    {
        for ( unsigned int colIt = 0; colIt < dim; colIt++ )
        {
            matrixToDecompose[(colIt*dim)+rowIt] = std::complex<double> ( this->_EMatrices.at(strNo).at(band).at(rowIt).at(colIt)[0],
                                                                          this->_EMatrices.at(strNo).at(band).at(rowIt).at(colIt)[1] );
        }
    }
    
    //======================================== Call the LAPACK ZGESSD routine
    returnValue                               = LAPACKE_zgesdd ( LAPACK_COL_MAJOR, 'A', dim, dim, matrixToDecompose, dim, singularValues, *rotMatU, dim, *rotMatV, dim );
    
    //======================================== Check for errors
    if ( returnValue != 0 )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Complex Matrix SVD algorithm did not converge, cannot proceed!" << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "The LAPACK Complex Matrix SVD algorithm did not converge, cannot proceed with computation of Trace Sigma distances. This problem could be resolved by changing the resolution or not using the Trace Sigma distances; these are not good solutions, but they are all I have got right now." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Save results in output format
    for ( unsigned int iter = 0; iter < dim; iter++ )
    {
        ret.emplace_back                      ( singularValues[iter] );
    }
    
    //======================================== Free memory
    delete[] singularValues;
    delete[] matrixToDecompose;
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief This function computes the singular value decomposition of its input using LAPACK and returns the U and V rotation matrices.
 
 This is a variation on the getSingularValues function just above. It uses the LAPACK library ZGESDD routine through the LAPACKE interface
 to obtain singular value decomposition, but this time does not return the diagonal of Sigma, but rather the U and V rotation matrices.
 
 \param[in] mat The matrix to be decomposed using ZGESDD routine.
 \param[in] dom The dimmensionality of the matrix.
 \param[out] X A vector of vectors encoding the U and V matrices resulting from SVD.
 
 \warning This function only works with double precision.
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::vector< std::vector<double> > > ProSHADE_internal::ProSHADE_comparePairwise::getSingularValuesUandVMatrix ( std::vector< std::vector<double> > mat,
                                                                                                                              unsigned int dim,
                                                                                                                              ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Initialise local variables
    double *singularValues                    = new double[dim];
    __extension__ std::complex<double> rotMatU[dim][dim];
    __extension__ std::complex<double> rotMatV[dim][dim];
    std::vector< std::vector< std::vector<double> > > ret;
    std::vector< std::vector<double> > retHlp;
    std::vector<double> hlpVec;
    int returnValue                           = 0;
    
    //======================================== Load SH coeffs into array in column-major order
    std::complex<double> *matrixToDecompose   = new std::complex<double>[dim*dim];
    for ( unsigned int rowIt = 0; rowIt < dim; rowIt++ )
    {
        for ( unsigned int colIt = 0; colIt < dim; colIt++ )
        {
            matrixToDecompose[(colIt*dim)+rowIt] = std::complex<double> ( mat.at(rowIt).at(colIt),
                                                                          0.0 );
        }
    }
    
    //======================================== Call the LAPACK ZGESSD function
    returnValue                               = LAPACKE_zgesdd ( LAPACK_COL_MAJOR, 'A', dim, dim, matrixToDecompose, dim, singularValues, *rotMatU, dim, *rotMatV, dim );
    
    //======================================== Check for errors
    if ( returnValue != 0 )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Complex Matrix SVD algorithm did not converge, cannot proceed!" << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "The LAPACK Complex Matrix SVD algorithm did not converge, cannot proceed with computation of Trace Sigma distances. This problem could be resolved by changing the resolution or not using the Trace Sigma distances; these are not good solutions, but they are all I have got right now." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Save results in output format
    for ( unsigned int iter = 0; iter < dim; iter++ ) { hlpVec.emplace_back ( 0.0 ); }
    for ( unsigned int iter = 0; iter < dim; iter++ ) { retHlp.emplace_back ( hlpVec ); }
    ret.emplace_back                          ( retHlp ); ret.emplace_back ( retHlp );

    //======================================== Save U
    for ( unsigned int rowIt = 0; rowIt < dim; rowIt++ )
    {
        for ( unsigned int colIt = 0; colIt < dim; colIt++ )
        {
            ret.at(0).at(rowIt).at(colIt)     = rotMatU[rowIt][colIt].real();
        }
    }
    
    //======================================== Save V
    for ( unsigned int rowIt = 0; rowIt < dim; rowIt++ )
    {
        for ( unsigned int colIt = 0; colIt < dim; colIt++ )
        {
            ret.at(1).at(rowIt).at(colIt)     = rotMatV[rowIt][colIt].real();
        }
    }
    
    //======================================== Free memory
    delete[] singularValues;
    delete[] matrixToDecompose;
    
    //======================================== Done
    return ( ret );
    
}
