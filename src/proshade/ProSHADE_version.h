/*! \file ProSHADE_version.cpp
 \brief This file contains the version of the ProSHADE code.
 
 This has the only purpose to store the version of the code.
 
 This file is part of the ProSHADE library for calculating
 shape descriptors and symmetry operators of protein structures.
 This is a prototype code, which is by no means complete or fully
 tested. Its use is at your own risk only. There is no quarantee
 that the results are correct.
 
 \author    Michal Tykac
 \author    Garib N. Murshudov
 \version   0.6.6
 \date      JAN 2019
 */

//============================================ Version
#ifndef __PROSHADE_VERSION__
#define __PROSHADE_VERSION__ "0.6.6 (JAN 2019)"
#endif
