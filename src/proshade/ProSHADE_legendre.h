/*! \file ProSHADE_legendre.h
 \brief This file contains all the functions related to computing the Gauss-Legendre integration variables.
 
 This file contains functions required to compute the Gauss-Legendre integration variables such
 as the abscissas and the weights for any integer order of integration. This code was adapted
 from John Burkardt's web. It is build around finding the zeroes of the Legendre polynomial
 of given order, expending the series and then finding the roots. These can then be used to
 obtain the values of interest - abscissas and weights.
 
 This file is part of the ProSHADE library for calculating
 shape descriptors and symmetry operators of protein structures.
 This is a prototype code, which is by no means complete or fully
 tested. Its use is at your own risk only. There is no quarantee
 that the results are correct.
 
 Code adapted from: John Burkardt (http://people.sc.fsu.edu/~jburkardt/cpp_src/legendre_rule_fast/legendre_rule_fast.html)
 
 \author    Michal Tykac
 \author    Garib N. Murshudov
 \version   0.6.6
 \date      JAN 2019
 */

#ifndef __LEGENDRE_PROSHADE__
#define __LEGENDRE_PROSHADE__

//============================================ Standard Library
#include <iostream>
#include <cmath>
#include <vector>

//============================================ RVAPI
#include <rvapi_interface.h>

/*! \namespace ProSHADE_legendre
 \brief This namespace contains the functions related to the Gauss-Legendre integration variables.
 
 This namespace is an adaptation of John Burkardt's code and contains functions required to compute the
 Legendre polynomial roots, abscissas and weights. 
 */
namespace ProSHADE_internal_legendre
{
    /*! \brief This function obtains the Legendre polynomial values and its derivative at zero for any positive integer order polynomial.
     
     This function takes the positive integer order of the Legendre polynomial and uses the recursive
     properties of the polynomials to work up to the order, computing the value at zero and its derivative
     for all lesser orders. It then returns the final values.
     
     \param[in] order Positive integer order of the Legendre polynomial which value at zero we want.
     \param[in] polyValue Pointer to variable which will store the resulting polynomial value at zero.
     \param[in] deriValue Pointer to variable which will store the derivative of the zero value.
     
     \warning This is an internal function which should not be used by the user.
     */
    inline void getPolyAtZero ( unsigned int order,
                                double *polyValue,
                                double *deriValue )
    {
        //==================================== Initialise
        double hlpVal                         = 0.0;
        double prevPoly                       = 1.0;
        double prevPrevPoly                   = 0.0;
        double prevDeri                       = 0.0;
        double prevPrevDeri                   = 0.0;
        
        for ( unsigned int ordIt = 0; ordIt < order; ordIt++ )
        {
            hlpVal                            = static_cast<double> ( ordIt );
            *polyValue                        = -hlpVal * prevPrevPoly / ( hlpVal + 1.0 );
            *deriValue                        = ( ( 2.0 * hlpVal + 1.0 ) * prevPoly - hlpVal * prevPrevDeri ) / ( hlpVal + 1.0 );
            prevPrevPoly                      = prevPoly;
            prevPoly                          = *polyValue;
            prevPrevDeri                      = prevDeri;
            prevDeri                          = *deriValue;
        }
        
        //==================================== Done
        return ;
        
    }
    
    /*! \brief This function finds the next value of the polynomial.
     
     Given the previous value of the polynomial, the distance to proceed and the number of steps to
     take, this function finds the next value of the polynomial using Taylor series.
     
     \param[in] from Current polynomial position.
     \param[in] to Polynomial position to move to.
     \param[in] valAtFrom The current value of the polynomial at the <from> position.
     \param[in] noSteps Number of steps in which to reach the <to> position.
     \param[out] X The polynomial value at the <to> position.
     
     \warning This is an internal function which should not be used by the user.
     */
    inline double advancePolyValue ( double from,
                                     double to,
                                     double valAtFrom,
                                     unsigned int noSteps )
    {
        //==================================== Initialise
        double hlpVal                         = 0.0;
        double stepSize                       = 0.0;
        double valChange                      = 0.0;
        double valSecChange                   = 0.0;
        double squareSteps                    = 0.0;
        double curVal                         = 0.0;
        unsigned int taylorSeriesCap          = 10;
        
        stepSize                              = ( to - from ) / static_cast<double> ( taylorSeriesCap );
        squareSteps                           = sqrt ( static_cast<double> ( noSteps * ( noSteps + 1 ) ) );
        curVal                                = from;
        
        for ( unsigned int iter = 0; iter < taylorSeriesCap; iter++ )
        {
            hlpVal                            = ( 1.0 - valAtFrom ) * ( 1.0 + valAtFrom );
            valChange                         = - stepSize * hlpVal / ( squareSteps * sqrt ( hlpVal ) - 0.5 * valAtFrom * sin ( 2.0 * curVal ) );
            valAtFrom                         = valAtFrom + valChange;
            
            curVal                            = curVal + stepSize;
            
            hlpVal                            = ( 1.0 - valAtFrom ) * ( 1.0 + valAtFrom );
            valSecChange                      = - stepSize * hlpVal / ( squareSteps * sqrt ( hlpVal ) - 0.5 * valAtFrom * sin ( 2.0 * curVal ) );
            valAtFrom                         = valAtFrom + 0.5 * ( valSecChange - valChange );
        }
        
        //==================================== Done
        return valAtFrom;
        
    }
    
    /*! \brief This function evaluates the Taylor expansion.
     
     This function takes the series array, the target value and the cap on Taylor expansion and proceeds to
     evaluate the series. The main use of this is to evaluate the series twice, one where the series evaluation
     'overshoots' and once where it 'undershoots' and taking value in between those, thus adding accuracy.
     
     \param[in] series Pointer to array with the series values.
     \param[in] target The target location on the series value.
     \param[in] terms The Taylor expansion cap.
     \param[out] X The value of the series at the target location.
     
     \warning This is an internal function which should not be used by the user.
     */
    inline double evaluateSeries ( double *series,
                                   double target,
                                   unsigned int terms )
    {
        //==================================== Initalise
        double factorialValue                 = 1.0;
        double value                          = 0.0;
        
        for ( unsigned int iter = 1; iter <= terms; iter++ )
        {
            value                             = value + series[iter] * factorialValue;
            factorialValue                    = factorialValue * target;
        }
        
        //==================================== Done
        return ( value );
        
    }
    
    /*! \brief This function finds the first root for Legendre polynomials of odd order.
     
     The Legendre polynomials with odd order have zero as the first root, but the even oder polenomials
     have different value and this function serves the purpose of finding this value (i.e. the first
     root of the polynomial if the order is even).
     
     \param[in] polyAtZero The value of the polynomial at zero.
     \param[in] order The positive integer value of the polynomial order.
     \param[in] abscAtZero Pointer to variable storing the abscissa value at zero.
     \param[in] weightAtZero Pointer to variable storing the weight value at zero.
     
     \warning This is an internal function which should not be used by the user.
     */
    inline void getFirstEvenRoot ( double polyAtZero,
                                   unsigned int order,
                                   double *abscAtZero,
                                   double *weighAtZero )
    {
        //==================================== Initialise
        *abscAtZero                           = advancePolyValue ( 0.0, -M_PI / 2.0, 0.0, order );
        double hlp                            = 0.0;
        double hlpVal                         = static_cast<double> ( order );
        unsigned int taylorSeriesCap          = 30;
        double *abscSteps;
        double *weightSteps;

        //==================================== Allocate memory
        abscSteps                             = new double [taylorSeriesCap+2];
        weightSteps                           = new double [taylorSeriesCap+1];
        
        //==================================== Pre-set values
        abscSteps[0]                          = 0.0;
        abscSteps[1]                          = polyAtZero;
        weightSteps[0]                        = 0.0;
        
        //==================================== Fill in abscissa and weight steps
        for ( unsigned int iter = 0; iter <= taylorSeriesCap - 2; iter = iter + 2 )
        {
            hlp                               = static_cast<double> ( iter );
            
            abscSteps[iter+2]                 = 0.0;
            abscSteps[iter+3]                 = ( hlp * ( hlp + 1.0 ) - hlpVal * ( hlpVal + 1.0 ) ) * abscSteps[iter+1] / (hlp + 1.0) / (hlp + 2.0 );
            
            weightSteps[iter+1]               = 0.0;
            weightSteps[iter+2]               = ( hlp + 2.0 ) * abscSteps[iter+3];
        }
        
        //==================================== Find abscissa and weights
        for ( unsigned int iter = 0; iter < 5; iter++ )
        {
            *abscAtZero                       = *abscAtZero - evaluateSeries ( abscSteps, *abscAtZero, taylorSeriesCap ) / evaluateSeries ( weightSteps, *abscAtZero, taylorSeriesCap-1 );
        }
        *weighAtZero                          = evaluateSeries ( weightSteps,
                                                                *abscAtZero,
                                                                 taylorSeriesCap-1 );
        
        //==================================== Free memory
        delete abscSteps;
        delete weightSteps;
        
        //==================================== Done
        return ;
        
    }
    
    /*! \brief This function completes the Legendre polynomial series assuming you have obtained the first values.
     
     Given that the polynomial value at zero is known, this function will complete the Legendre polynomial and with it
     the absicassas and weights for the Gauss-Legendre integration using the other functions defined above.
     
     \param[in] order The positive integer value of the polynomial order.
     \param[in] abscissa Pointer to a vector of abscissas containing the first value.
     \param[in] weights Pointer to a vector of weights containing the first value.
     
     \warning This is an internal function which should not be used by the user.
     */
    inline void completeLegendreSeries ( unsigned int order,
                                         std::vector<double>* abscissa,
                                         std::vector<double>* weights )
    {
        //==================================== Initialise
        double hlpTaylorVal                   = 0.0;
        double hlpOrderVal                    = static_cast<double> ( order );
        double abscValueChange                = 0.0;
        double prevAbsc                       = 0.0;
        double *hlpAbscSeries;
        double *hlpWeightSeries;
        unsigned int taylorSeriesCap          = 30;
        unsigned int noSeriesElems            = 0;
        unsigned int oddEvenSwitch            = 0;
        
        //==================================== Pre-set values
        if ( order % 2 == 1 )
        {
            noSeriesElems                     = ( order - 1 ) / 2 - 1;
            oddEvenSwitch                     = 1;
        }
        else
        {
            noSeriesElems                     = order / 2 - 1;
            oddEvenSwitch                     = 0;
        }
        
        //==================================== Allocate memory
        hlpAbscSeries                         = new double[taylorSeriesCap+2];
        hlpWeightSeries                       = new double[taylorSeriesCap+1];
        
        //==================================== For each series element
        for ( unsigned int serIt = noSeriesElems + 1; serIt < order - 1; serIt++ )
        {
            //================================ Init loop
            prevAbsc                          = abscissa->at(serIt);
            abscValueChange                   = advancePolyValue ( M_PI/2.0, -M_PI/2.0, prevAbsc, order ) - prevAbsc;
            
            //================================ Init abscissas
            hlpAbscSeries[0]                  = 0.0;
            hlpAbscSeries[1]                  = 0.0;
            hlpAbscSeries[2]                  = weights->at(serIt);
            
            //================================ Init weights
            hlpWeightSeries[0]                = 0.0;
            hlpWeightSeries[1]                = hlpAbscSeries[2];
            
            //================================ Taylor expansion
            for ( unsigned int tayIt = 0; tayIt <= taylorSeriesCap - 2; tayIt++ )
            {
                hlpTaylorVal                  = static_cast<double> ( tayIt );
                
                hlpAbscSeries[tayIt+3]        = ( 2.0 * prevAbsc * ( hlpTaylorVal + 1.0 ) * hlpAbscSeries[tayIt+2]
                                                  + ( hlpTaylorVal * ( hlpTaylorVal + 1.0 ) - hlpOrderVal * ( hlpOrderVal + 1.0 ) ) * hlpAbscSeries[tayIt+1] /
                                                    ( hlpTaylorVal + 1.0 ) ) / ( 1.0 - prevAbsc ) / ( 1.0 + prevAbsc ) / ( hlpTaylorVal + 2.0 );
                
                hlpWeightSeries[tayIt+2]      = ( hlpTaylorVal + 2.0 ) * hlpAbscSeries[tayIt+3];
            }
            
            //================================ Sum over results
            for ( unsigned int iter = 0; iter < 5; iter++ )
            { 
                abscValueChange               = abscValueChange - evaluateSeries ( hlpAbscSeries,   abscValueChange, taylorSeriesCap   ) /
                                                                  evaluateSeries ( hlpWeightSeries, abscValueChange, taylorSeriesCap-1 );
            }
            
            //================================ Save results
            abscissa->at(serIt+1)             = prevAbsc + abscValueChange;
            weights->at(serIt+1)              = evaluateSeries ( hlpWeightSeries, abscValueChange, taylorSeriesCap - 1 );
        }
        
        for ( unsigned int serIt = 0; serIt <= noSeriesElems + oddEvenSwitch; serIt++ )
        {
            abscissa->at(serIt)               = - abscissa->at(order-serIt-1);
            weights->at(serIt)                = weights->at(order-serIt-1);
        }
        
        //==================================== Free memory
        delete hlpAbscSeries;
        delete hlpWeightSeries;
        
        //==================================== Done
        return ;
        
    }
    
    /*! \brief This function takes everytging together and computes the abscissas and weights for the Gauss-Legendre integration given only the order.
     
     For the comfort of usage, this function takes everything together and computes the abscissas and weights for the Gauss-Legendre
     integration by first finding the polynomial value at zero, then finding the root and completing the series. 
     
     \param[in] order The positive integer value of the polynomial order required.
     \param[in] abscissa Pointer to a vector where abscissas will be saved.
     \param[in] weights Pointer to a vector where weights will be saved.
     \param[in] settings ProSHADE_settings container with infotmation about HTML reporting.
     
     \warning This is an internal function which should not be used by the user.
     */
    inline void getLegendreAbscAndWeights ( unsigned int order,
                                            std::vector<double>* abscissa,
                                            std::vector<double>* weights,
                                            ProSHADE::ProSHADE_settings* settings )
    {
        //==================================== Sanity check
        if ( order < 1 )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Trying to compute legendre quadrature with order " << order << " which seems too low!" << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "The automatic determination of the Gauss-Legendre integration order has determined the optimal order to be less than 1. This may happen for very small maps, if this is the case, please manually set the integration value to 10-ish and re-run. If the map is reasonably large, please report this as a bug." << "</font>";
                rvapi_set_text                    ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                       ( );
            }
            
            exit ( -1 );
        }
        
        //==================================== Initialise
        double polyValue                      = 0.0;
        double deriValue                      = 0.0;
        double weightSum                      = 0.0;
        
        //==================================== Find the polynomial and derivative values at 0
        getPolyAtZero                         ( order,
                                               &polyValue,
                                               &deriValue );
        
        //==================================== If the order is odd, then 0 is a root ...
        if ( order % 2 == 1 )
        {
            abscissa->at((order-1)/2)         = polyValue;
            weights->at((order-1)/2)          = deriValue;
        }
        else
        {
            // ... and if order is even, find the first root
            getFirstEvenRoot                  ( polyValue, order, &abscissa->at(order/2), &weights->at(order/2) );
        }
        
        //==================================== Now, having computed the first roots, complete the series
        completeLegendreSeries                ( order,
                                                abscissa,
                                                weights );
        
        //==================================== Correct weights by anscissa values
        for ( unsigned int iter = 0; iter < order; iter++ )
        {
            weights->at(iter)                 = 2.0 / ( 1.0 - abscissa->at(iter) ) / ( 1.0 + abscissa->at(iter) ) / weights->at(iter) / weights->at(iter);
            weightSum                         = weightSum + weights->at(iter);
        }
        
        //==================================== Normalise weights
        for ( unsigned int iter = 0; iter < order; iter++ )
        {
            weights->at(iter)                 = 2.0 * weights->at(iter) / weightSum;
        }
        
        //==================================== Done
        return ;
        
    }
}

#endif
