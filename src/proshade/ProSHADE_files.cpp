/*! \file ProSHADE_files.cpp
 \brief This source file contains function and globals required for platform-independent file detection.
 
 The following code is a source file for function to locate all files
 within a given path and with given extension. To use the function,
 simply supply the address of vector of strings to which the paths
 to all recovered files are to be saved, the path to start
 searching and the extension required. For no extension, use "".
 
 This file is part of the ProSHADE library for calculating
 shape descriptors and symmetry operators of protein structures.
 This is a prototype code, which is by no means complete or fully
 tested. Its use is at your own risk only. There is no quarantee
 that the results are correct.
 
 \author    Michal Tykac
 \author    Garib N. Murshudov
 \version   0.6.6
 \date      JAN 2019
 */

//============================================ ProSHADE
#include "ProSHADE_files.h"

/*! \brief Function to recursively search for files with given extension in the file path supplied.
 
    This function searches a given folder and all subfolders (recursively) for any and all files with
    the extension given. It will return all files if no extension is given (i.e. the extension parameter
    is ""). Upon completing the search, the funciton will save the full paths to all found files in a
    vector and returns it.
 
    \param[in] saveFiles This is a pointer to the vector of strings to which all found file paths are to be saved.
    \param[in] path This is the path which should be searched (including all sub-directories).
    \param[in] ext This is the extension which the files are required to have. Leave "" for any extension.
    \param[out] X Bool value indicating whether the function have failed or suceeded.
 
    \warning The function is recursive by default.
    \warning The function was never tested on windows.
    \warning There is no guarantee that the function return order will not change between runs with the same parameters.
    \warning This is an internal function which should not be used by the user.
 */
bool ProSHADE_internal_files::findFiles ( std::vector <std::string>* saveFiles,
                                          const char* path,
                                          const char* ext )
{
    //======================================== Locals
    DIR* dir;
    dirent* entry;
    
    //======================================== Open directory
    dir                                       = opendir ( path );
    if ( dir == NULL ) { std::cerr << "!!! ProSHADE ERROR !!! Cannot open directory " << path << std::endl; return false; }
    
    while ( ( entry = readdir ( dir ) ) )
    {
        //==================================== Ignore entries starting with '.'
        if ( entry->d_name[0] == '.' ) { ; }
        else
        {
            if ( entry->d_type == DT_DIR )
            {
                //============================ Is directory - recurse
                std::string newPath           = std::string ( path ) + std::string ( entry->d_name ) + systemDirectorySeparator;
                if ( !findFiles ( saveFiles, newPath.c_str(), ext ) )
                {
                    std::cerr << "Directory recursion error!" << std::endl;
                    return false;
                }
            }
            else
            {
                //============================ Is file - check and save
                std::string fileName          = std::string ( entry->d_name );
                std::string extension         = fileName.substr ( fileName.length() - std::string(ext).length(), fileName.length() - 1);
                if ( std::strcmp ( extension.c_str(), std::string ( ext ).c_str() ) == 0 )
                {
                    //======================== Has required extension?
                    saveFiles->push_back      ( std::string ( path ) + std::string ( entry->d_name ) );
                }
            }
        }
    }
    closedir                                  ( dir );
    
    //======================================== Done
    return true;
}
