/*! \file ProSHADE.cpp
 \brief This file contains the constructors, destructors and other general functions intended to be used by the user.
 
 This is the cpp file containig the constructors, destructors and general functions
 which do not really fit to any other cpp file. This is where the execution logic is
 implemented, while all the other cpp files contain logic for specific purposes only.
 
 This file is part of the ProSHADE library for calculating
 shape descriptors and symmetry operators of protein structures.
 This is a prototype code, which is by no means complete or fully
 tested. Its use is at your own risk only. There is no quarantee
 that the results are correct.
 
 \author    Michal Tykac
 \author    Garib N. Murshudov
 \version   0.6.6
 \date      JAN 2019
 */

//============================================ Clipper
#include <clipper/clipper.h>
#include <clipper/clipper-contrib.h>
#include <clipper/clipper-ccp4.h>
#include <clipper/clipper-mmdb.h>
#include <clipper/clipper-minimol.h>

//============================================ FFTW3 + SOFT
#ifdef __cplusplus
extern "C" {
#endif
#include <fftw3.h>
#include <wrap_fftw.h>
#include <makeweights.h>
#include <s2_primitive.h>
#include <s2_cospmls.h>
#include <s2_legendreTransforms.h>
#include <s2_semi_fly.h>
#include <rotate_so3_utils.h>
#include <utils_so3.h>
#include <soft_fftw.h>
#include <rotate_so3_fftw.h>
#ifdef __cplusplus
}
#endif

//============================================ CMAPLIB
#include "cmaplib.h"

//============================================ ProSHADE
#include "ProSHADE.h"
#include "ProSHADE_internal.h"
#include "ProSHADE_files.h"
#include "ProSHADE_misc.h"
#include "ProSHADE_legendre.h"

//============================================ RVAPI
#include <rvapi_interface.h>


/*! \brief This function checks the input file for being either PDB or MAP formatted.
 
 This function is used to determine which data format the input data are in and if it is supported. This is done by basically trying to
 read the file using MMDB and Clipper, so it should be extension independent.
 
 \param[in] fileName The file name (and path) to the file to be checked.
 \param[out] X An unsigned int value 0 for not recognised format, 1 for PDB and 2 for MAP formats.
 
 \warning This is an internal function which should not be used by the user.
 */
unsigned int ProSHADE_internal::checkFileType ( std::string fileName )
{
    //======================================== Try readin as PDB
    clipper::mmdb::CMMDBManager *mfile = new clipper::mmdb::CMMDBManager ( );
    if ( mfile->ReadCoorFile ( fileName.c_str() ) )
    {
        //==================================== Not PDB, try MAP
        clipper::CCP4MAPfile mapFile;
        
        try
        {
            mapFile.open_read( fileName );
        }
        catch (const clipper::Message_base &exc)
        {
            //================================ Not MAP either...
            delete mfile;
            return ( 0 );
        }
        
        //==================================== This is a MAP file, report so
        delete mfile;
        return ( 2 );
    }
    else
    {
        //==================================== This is a PDB file, report so
        delete mfile;
        return ( 1 );
    }
}

/*! \brief Contructor for the ProSHADE_data class.
 
 This is the constructor for the main structural data holding class. It simply sets all the default parameters and
 nulls all the pointers so that no errors occur when wrong function call order is used.
 
 \param[out] X Object capable of holding the structure data required for further analysis.
 
 \warning This class should not be directly accessed by the user.
 */
ProSHADE_internal::ProSHADE_data::ProSHADE_data (  )
{
    //======================================== Initialise pointers
    this->_densityMapMap                      = nullptr;
    this->_densityMapCor                      = nullptr;
    this->_densityMapCorCoords                = nullptr;
    this->_realSHCoeffs                       = nullptr;
    this->_imagSHCoeffs                       = nullptr;
    this->_sphericalHarmonicsWeights          = nullptr;
    this->_semiNaiveTable                     = nullptr;
    this->_semiNaiveTableSpace                = nullptr;
    this->_shWorkspace                        = nullptr;
    this->_rrpMatrices                        = nullptr;
    this->_invRealData                        = nullptr;
    this->_invImagData                        = nullptr;
    this->_shellMappedData                    = nullptr;
    
    //======================================== Initialise checks
    this->_densityMapComputed                 = false;
    this->_phaseRemoved                       = false;
    this->_firstLineCOM                       = false;
    this->_sphereMapped                       = false;
    this->_sphericalCoefficientsComputed      = false;
    this->_rrpMatricesPrecomputed             = false;
    this->_wasBandwithGiven                   = true;
    this->_wasThetaGiven                      = true;
    this->_wasPhiGiven                        = true;
    this->_wasGlInterGiven                    = true;
    
    //======================================== Initialise COM
    this->_xCorrection                        = 0;
    this->_yCorrection                        = 0;
    this->_zCorrection                        = 0;
    
    //======================================== Initialise map valus (to infinity)
    this->_xFrom                              = std::numeric_limits<float>::infinity();
    this->_yFrom                              = std::numeric_limits<float>::infinity();
    this->_zFrom                              = std::numeric_limits<float>::infinity();
    this->_xTo                                = std::numeric_limits<float>::infinity();
    this->_yTo                                = std::numeric_limits<float>::infinity();
    this->_zTo                                = std::numeric_limits<float>::infinity();
    this->_xRange                             = std::numeric_limits<float>::infinity();
    this->_yRange                             = std::numeric_limits<float>::infinity();
    this->_zRange                             = std::numeric_limits<float>::infinity();
    
    //======================================== Initialise map normalisation
    this->_mapMean                            = 0.0;
    this->_mapSdev                            = 1.0;
    this->_noShellsWithData                   = 0;
    
    //======================================== Done
    
}

/*! \brief Copy contructor for the ProSHADE_data class.
 
 This is the copy constructor for the main structural data holding class. It simply sets all the default parameters as copies
 of the copied object, allocating all memory as need be.
 
 \param[out] X Object holding the same information as the original object.
 
 \warning This class should not be directly accessed by the user.
 */
ProSHADE_internal::ProSHADE_data::ProSHADE_data ( ProSHADE_data *copyFrom )
{
    //======================================== Variables regarding the reading in of the data
    this->_inputFileName                      = copyFrom->_inputFileName;
    this->_fromPDB                            = copyFrom->_fromPDB;
    
    //==================================== Variables regarding the shells
    this->_shellSpacing                       = copyFrom->_shellSpacing;
    this->_maxExtraCellularSpace              = copyFrom->_maxExtraCellularSpace;
    this->_xRange                             = copyFrom->_xRange;
    this->_yRange                             = copyFrom->_yRange;
    this->_zRange                             = copyFrom->_zRange;
    this->_xSamplingRate                      = copyFrom->_xSamplingRate;
    this->_ySamplingRate                      = copyFrom->_ySamplingRate;
    this->_zSamplingRate                      = copyFrom->_zSamplingRate;
    this->_xFrom                              = copyFrom->_xFrom;
    this->_yFrom                              = copyFrom->_yFrom;
    this->_zFrom                              = copyFrom->_zFrom;
    this->_xTo                                = copyFrom->_xTo;
    this->_yTo                                = copyFrom->_yTo;
    this->_zTo                                = copyFrom->_zTo;
    this->_shellPlacement                     = std::vector<double> ( copyFrom->_shellPlacement.size() );

    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( copyFrom->_shellPlacement.size() ); iter++ )
    {
        this->_shellPlacement.at(iter)        = copyFrom->_shellPlacement.at(iter);
    }
    
    //==================================== Variables regarding the density map
    this->_mapResolution                      = copyFrom->_mapResolution;
    this->_maxMapU                            = copyFrom->_maxMapU;
    this->_maxMapV                            = copyFrom->_maxMapV;
    this->_maxMapW                            = copyFrom->_maxMapW;
    this->_densityMapComputed                 = copyFrom->_densityMapComputed;
    this->_mapMean                            = copyFrom->_mapMean;
    this->_mapSdev                            = copyFrom->_mapSdev;
    
    if ( copyFrom->_densityMapMap != nullptr )
    {
        this->_densityMapMap                  = new float[(copyFrom->_maxMapU+1) * (copyFrom->_maxMapV+1) * (copyFrom->_maxMapW+1)];
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (copyFrom->_maxMapU+1) * (copyFrom->_maxMapV+1) * (copyFrom->_maxMapW+1) ); iter++)
        {
            this->_densityMapMap[iter]        = copyFrom->_densityMapMap[iter];
        }
    }
    else
    {
        this->_densityMapMap                  = nullptr;
    }
    
    //==================================== Variables regarding the phase removal from map
    this->_fourierCoeffPower                  = copyFrom->_fourierCoeffPower;
    this->_bFactorChange                      = copyFrom->_bFactorChange;
    this->_maxMapRange                        = copyFrom->_maxMapRange;
    this->_phaseRemoved                       = copyFrom->_phaseRemoved;
    this->_keepOrRemove                       = copyFrom->_keepOrRemove;
    
    if ( copyFrom->_densityMapCor != nullptr )
    {
        this->_densityMapCor                  = new double[(copyFrom->_maxMapU+1) * (copyFrom->_maxMapV+1) * (copyFrom->_maxMapW+1)];
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (copyFrom->_maxMapU+1) * (copyFrom->_maxMapV+1) * (copyFrom->_maxMapW+1) ); iter++)
        {
            this->_densityMapCor[iter]        = copyFrom->_densityMapCor[iter];
        }
    }
    else
    {
        this->_densityMapCor                  = nullptr;
    }
    
    if ( copyFrom->_densityMapCorCoords != nullptr )
    {
        this->_densityMapCorCoords            = new std::array<double,3>[(copyFrom->_maxMapU+1) * (copyFrom->_maxMapV+1) * (copyFrom->_maxMapW+1)];
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (copyFrom->_maxMapU+1) * (copyFrom->_maxMapV+1) * (copyFrom->_maxMapW+1) ); iter++)
        {
            this->_densityMapCorCoords[iter][0] = copyFrom->_densityMapCorCoords[iter][0];
            this->_densityMapCorCoords[iter][1] = copyFrom->_densityMapCorCoords[iter][1];
            this->_densityMapCorCoords[iter][2] = copyFrom->_densityMapCorCoords[iter][2];
        }
    }
    else
    {
        this->_densityMapCorCoords            = nullptr;
    }
        
    //==================================== Variables regarding the sphere mapping of phaseless data
    this->_thetaAngle                         = copyFrom->_thetaAngle;
    this->_phiAngle                           = copyFrom->_phiAngle;
    this->_noShellsWithData                   = copyFrom->_noShellsWithData;
    this->_xCorrection                        = copyFrom->_xCorrection;
    this->_yCorrection                        = copyFrom->_yCorrection;
    this->_zCorrection                        = copyFrom->_zCorrection;
    this->_xCorrErr                           = copyFrom->_xCorrErr;
    this->_yCorrErr                           = copyFrom->_yCorrErr;
    this->_zCorrErr                           = copyFrom->_zCorrErr;
    this->_sphereMapped                       = copyFrom->_sphereMapped;
    this->_firstLineCOM                       = copyFrom->_firstLineCOM;
    
    if ( copyFrom->_shellMappedData != nullptr )
    {
        this->_shellMappedData                = new double*[this->_noShellsWithData];
        for ( unsigned int sh = 0; sh < this->_noShellsWithData; sh++ )
        {
            this->_shellMappedData[sh]            = new double[static_cast<unsigned int> ( this->_thetaAngle * this->_phiAngle )];
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->_thetaAngle * this->_phiAngle ); iter++ )
            {
                this->_shellMappedData[sh][iter] = copyFrom->_shellMappedData[sh][iter];
            }
        }
    }
    else
    {
        this->_shellMappedData                = nullptr;
    }
    
    //==================================== Variables regarding the spherical harmonics decomposition
    this->_bandwidthLimit                     = copyFrom->_bandwidthLimit;
    this->_oneDimmension                      = copyFrom->_oneDimmension;
    this->_sphericalCoefficientsComputed      = copyFrom->_sphericalCoefficientsComputed;
    this->_wasBandwithGiven                   = copyFrom->_wasBandwithGiven;
    this->_wasThetaGiven                      = copyFrom->_wasThetaGiven;
    this->_wasPhiGiven                        = copyFrom->_wasPhiGiven;
    this->_wasGlInterGiven                    = copyFrom->_wasGlInterGiven;
    
    if ( copyFrom->_realSHCoeffs != nullptr )
    {
        this->_realSHCoeffs                   = new double*[static_cast<unsigned int> ( this->_noShellsWithData )];
        for ( unsigned int shIt = 0; shIt < this->_noShellsWithData; shIt++ )
        {
            this->_realSHCoeffs[shIt]         = new double [static_cast<unsigned int> ( this->_oneDimmension * this->_oneDimmension )];
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->_oneDimmension * this->_oneDimmension ); iter++ )
            {
                this->_realSHCoeffs[shIt][iter] = copyFrom->_realSHCoeffs[shIt][iter];
            }
        }
    }
    else
    {
        this->_realSHCoeffs                   = nullptr;
    }
    
    if ( copyFrom->_imagSHCoeffs != nullptr )
    {
        this->_imagSHCoeffs                   = new double*[static_cast<unsigned int> ( this->_noShellsWithData )];
        for ( unsigned int shIt = 0; shIt < this->_noShellsWithData; shIt++ )
        {
            this->_imagSHCoeffs[shIt]         = new double [static_cast<unsigned int> ( this->_oneDimmension * this->_oneDimmension )];
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->_oneDimmension * this->_oneDimmension ); iter++ )
            {
                this->_imagSHCoeffs[shIt][iter] = copyFrom->_imagSHCoeffs[shIt][iter];
            }
        }
    }
    else
    {
        this->_imagSHCoeffs                   = nullptr;
    }
    
    if ( copyFrom->_sphericalHarmonicsWeights != nullptr )
    {
        this->_sphericalHarmonicsWeights      = new double [static_cast<unsigned int> ( this->_bandwidthLimit * 4 )];
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->_bandwidthLimit * 4 ); iter++ )
        {
            this->_sphericalHarmonicsWeights[iter] = copyFrom->_sphericalHarmonicsWeights[iter];
        }
    }
    else
    {
        this->_sphericalHarmonicsWeights      = nullptr;
    }
    
    if ( copyFrom->_shWorkspace != nullptr )
    {
        this->_shWorkspace                    = (fftw_complex *) fftw_malloc ( sizeof(fftw_complex) * ( (  8 * this->_bandwidthLimit * this->_bandwidthLimit ) +
                                                                                                        ( 10 * this->_bandwidthLimit ) ) );
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (  8 * this->_bandwidthLimit * this->_bandwidthLimit ) + ( 10 * this->_bandwidthLimit ) ); iter++ )
        {
            this->_shWorkspace[iter][0]       = copyFrom->_shWorkspace[iter][0];
            this->_shWorkspace[iter][1]       = copyFrom->_shWorkspace[iter][1];
        }
    }
    else
    {
        this->_shWorkspace                    = nullptr;
    }
    
    if ( copyFrom->_semiNaiveTableSpace != nullptr )
    {
        this->_semiNaiveTableSpace            = new double [static_cast<unsigned int> ( Reduced_Naive_TableSize     ( this->_bandwidthLimit, this->_bandwidthLimit ) +
                                                                                        Reduced_SpharmonicTableSize ( this->_bandwidthLimit, this->_bandwidthLimit ) )];
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( Reduced_Naive_TableSize     ( this->_bandwidthLimit, this->_bandwidthLimit ) + Reduced_SpharmonicTableSize ( this->_bandwidthLimit, this->_bandwidthLimit ) ); iter++ )
        {
            this->_semiNaiveTableSpace[iter]  = copyFrom->_semiNaiveTableSpace[iter];
        }
    }
    else
    {
        this->_semiNaiveTableSpace            = nullptr;
    }
    
    if ( copyFrom->_semiNaiveTable != nullptr )
    {
        this->_semiNaiveTable                 = nullptr;
        this->_semiNaiveTable                 = SemiNaive_Naive_Pml_Table ( this->_bandwidthLimit,
                                                                            this->_bandwidthLimit,
                                                                            this->_semiNaiveTableSpace,
                                                                            reinterpret_cast<double*> ( this->_shWorkspace ) );
    }
    else
    {
        this->_semiNaiveTable                 = nullptr;
    }
    
    //==================================== Variables regarding spherical harmonics inverse
    if ( copyFrom->_invRealData != nullptr )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error copying the ProSHADE_data object. This should not happen, please report this case." << std::endl;
        exit ( -1 );
    }
    else
    {
        this->_invRealData                    = nullptr;
    }
    
    if ( copyFrom->_invImagData != nullptr )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error copying the ProSHADE_data object. This should not happen, please report this case." << std::endl;
        exit ( -1 );
    }
    else
    {
        this->_invImagData                    = nullptr;
    }
    
    //==================================== Variables regarding the energy levels descriptor pre-calculation
    if ( copyFrom->_rrpMatrices != nullptr )
    {
        this->_rrpMatrices                    = new double** [this->_bandwidthLimit];
        for ( unsigned int bwIt = 0; bwIt < this->_bandwidthLimit; bwIt++ )
        {
            //==================================== Odd bands are 0, so just ignore them
            if ( !this->_keepOrRemove ) { if ( ( bwIt % 2 ) != 0 ) { continue; } }
            
            this->_rrpMatrices[bwIt]          = new double*  [this->_noShellsWithData];
            for ( unsigned int shIt = 0; shIt < this->_noShellsWithData; shIt++ )
            {
                this->_rrpMatrices[bwIt][shIt]= new double   [this->_noShellsWithData];
                for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->_noShellsWithData ); iter++ )
                {
                    this->_rrpMatrices[bwIt][shIt][iter] = copyFrom->_rrpMatrices[bwIt][shIt][iter];
                }
            }
        }
    }
    else
    {
        this->_rrpMatrices                    = nullptr;
    }
    
    this->_rrpMatricesPrecomputed             = copyFrom->_rrpMatricesPrecomputed;
    
    //======================================== Done
    
}

/*! \brief Destructor for the ProSHADE_data class.
 
 This is the destructor for the main structural data holding class. It is responsible for checking if all the allocated memory has
 been released and if not, release it itself.
 
 \warning This is an internal function which should not be used by the user.
 */
ProSHADE_internal::ProSHADE_data::~ProSHADE_data (  )
{
    //======================================== Free memory
    if ( this->_densityMapMap                 != nullptr ) { delete[]    this->_densityMapMap            ; }
    if ( this->_densityMapCor                 != nullptr ) { delete[]    this->_densityMapCor            ; }
    if ( this->_densityMapCorCoords           != nullptr ) { delete[]    this->_densityMapCorCoords      ; }
    if ( this->_shellMappedData               != nullptr ) { delete[]    this->_shellMappedData          ; }
    if ( this->_sphericalHarmonicsWeights     != nullptr ) { delete[]    this->_sphericalHarmonicsWeights; }
    if ( this->_semiNaiveTableSpace           != nullptr ) { delete[]    this->_semiNaiveTableSpace      ; }
    

    if ( this->_shWorkspace                   != nullptr ) { fftw_free ( this->_shWorkspace )            ; }

    if ( this->_rrpMatrices                   != nullptr ) { for ( unsigned int i = 0; i < this->_bandwidthLimit; i++ ) { if ( i % 2 == 1 ) { continue; } delete[] this->_rrpMatrices[i]; } }
    
    if ( this->_realSHCoeffs != nullptr && this->_imagSHCoeffs != nullptr )
    {
        for ( unsigned int shIt = 0; shIt < this->_noShellsWithData; shIt++ )
        {
            delete[] this->_realSHCoeffs[shIt];
            delete[] this->_imagSHCoeffs[shIt];
        }
        delete[] this->_realSHCoeffs;
        delete[] this->_imagSHCoeffs;
    }

    if ( this->_invRealData != nullptr && this->_invImagData != nullptr )
    {
        for ( unsigned int shIt = 0; shIt < this->_noShellsWithData; shIt++ )
        {
            delete[] this->_invRealData[shIt];
            delete[] this->_invImagData[shIt];
        }
        delete[] this->_invRealData;
        delete[] this->_invImagData;
    }
    
    //======================================== Done
}

/*! \brief Contructor for the ProSHADE_comparePairwise class.
 
 This is the constructor for the executive class responsible for comparing two structures with identical settings. This is currently used only for
 symmetry detection as the distance computation has its designated class ProSHADE_distances. The constructor is responsible for sanity checks, setting
 up parameters and determining the integration variables.
 
 \param[in] cmpObj1 This is the pointer to the first structure data holding object.
 \param[in] cmpObj2 This is the pointer to the second structure data holding object.
 \param[in] mPower This is the weighting parameter for energy level distances computation.
 \param[in] ignoreL This vector should contain all bands to be ignored in the computations.
 \param[in] order This is the order of the Gauss-Legendre integration to be used.
 \param[in] settings ProSHADE_settings container class with information about the HTML report printing, if needed.
 \param[out] X Object capable of comparing the two structures.
 
 \warning This class should not be directly accessed by the user.
 */
ProSHADE_internal::ProSHADE_comparePairwise::ProSHADE_comparePairwise ( ProSHADE_data *cmpObj1,
                                                                        ProSHADE_data *cmpObj2,
                                                                        double mPower,
                                                                        std::vector<int> ignoreL,
                                                                        unsigned int order,
                                                                        ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Initialise internal values
    this->_bothRRPsPreComputed                = false;
    
    //======================================== Sanity checks
    if ( cmpObj1->_bandwidthLimit != cmpObj2->_bandwidthLimit )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file comparison between " << cmpObj1->_inputFileName << " AND " << cmpObj2->_inputFileName << " !!! The bandwidths are different, use the same bandwidth to make the strucutres comparable." << std::endl;
        exit ( -1 );
    }
    
    if ( ( cmpObj1->_thetaAngle != cmpObj2->_thetaAngle ) || ( cmpObj1->_phiAngle != cmpObj2->_phiAngle ) )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file comparison between " << cmpObj1->_inputFileName << " AND " << cmpObj2->_inputFileName << " !!! The theta or phi angles are different, use the same theta and phi angles to make the strucutres comparable." << std::endl;
        exit ( -1 );
    }
    
    if ( ( cmpObj1->_rrpMatricesPrecomputed ) && ( cmpObj2->_rrpMatricesPrecomputed ) )
    {
        this->_bothRRPsPreComputed            = true;
    }
    
    if ( cmpObj1->_shellSpacing != cmpObj2->_shellSpacing )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file comparison between " << cmpObj1->_inputFileName << " AND " << cmpObj2->_inputFileName << " !!! The shell spacing distances are different, use the same shell spacing distance to make the strucutres comparable." << std::endl;
        exit ( -1 );
    }
    
    if ( cmpObj1->_keepOrRemove != cmpObj2->_keepOrRemove )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file comparison between " << cmpObj1->_inputFileName << " AND " << cmpObj2->_inputFileName << " !!! The phase treatment is different (i.e. one structure has phases removed, the other does not), use the same phase treatment to make the strucutres comparable." << std::endl;
        exit ( -1 );
    }
    
    if ( order < 2 )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file comparison between " << cmpObj1->_inputFileName << " AND " << cmpObj2->_inputFileName << " !!! The Gauss-Legendre integration order is too low, select a higher value than 1!" << std::endl;
        exit ( -1 );
    }
    
    //======================================== Initialise internal values
    this->_bandwidthLimit                     = cmpObj1->_bandwidthLimit;
    this->_thetaAngle                         = cmpObj1->_thetaAngle;
    this->_phiAngle                           = cmpObj1->_phiAngle;
    this->_matrixPowerWeight                  = mPower;
    this->_lsToIgnore                         = ignoreL;
    this->_minShellsToUse                     = std::min ( cmpObj1->_noShellsWithData, cmpObj2->_noShellsWithData );
    this->_obj1RRPs                           = cmpObj1->_rrpMatrices;
    this->_obj2RRPs                           = cmpObj2->_rrpMatrices;
    this->_shellSpacing                       = cmpObj1->_shellSpacing;
    this->_obj1RealCoeffs                     = cmpObj1->_realSHCoeffs;
    this->_obj1ImagCoeffs                     = cmpObj1->_imagSHCoeffs;
    this->_obj2RealCoeffs                     = cmpObj2->_realSHCoeffs;
    this->_obj2ImagCoeffs                     = cmpObj2->_imagSHCoeffs;
    this->_noShellsObj1                       = cmpObj1->_noShellsWithData;
    this->_noShellsObj2                       = cmpObj2->_noShellsWithData;
    this->_maxShellsToUse                     = std::max ( cmpObj1->_noShellsWithData, cmpObj2->_noShellsWithData );
    this->_keepOrRemove                       = cmpObj1->_keepOrRemove;
    this->_glIntegrationOrder                 = order;
    
    this->_trSigmaEMatrix                     = nullptr;
    this->_so3Coeffs                          = nullptr;
    this->_so3InvCoeffs                       = nullptr;
    this->_so3Workspace1                      = nullptr;
    this->_so3Workspace2                      = nullptr;
    this->_so3Workspace3                      = nullptr;
    
    this->_distanceRotInv                     = 0.0;
    this->_distanceTrSigma                    = 0.0;
    this->_peakHeightThr                      = 0.0;

    this->_rotInvComputed                     = false;
    this->_trSigmaPreComputed                 = false;
    this->_trSigmaComputed                    = false;
    this->_so3InvMapComputed                  = false;
    this->_eulerAnglesFound                   = false;
    this->_wignerMatricesComputed             = false;
    this->_CSymmsFound                        = false;
    this->_DSymmsFound                        = false;
    
    //======================================== Compute weights and abscissas for GL Integration
    this->_glAbscissas                        = std::vector<double> ( this->_glIntegrationOrder );
    this->_glWeights                          = std::vector<double> ( this->_glIntegrationOrder );
    ProSHADE_internal_legendre::getLegendreAbscAndWeights ( this->_glIntegrationOrder, &(this->_glAbscissas), &(this->_glWeights), settings );
    
    //======================================== Done
    
}

/*! \brief Destructor for the ProSHADE_comparePairwise class.
 
 The simple destructor is responsible for checking that all the memory has been appropriately released before and if not,
 then it releases it itself.
 
 \warning This is an internal function which should not be used by the user.
 */
ProSHADE_internal::ProSHADE_comparePairwise::~ProSHADE_comparePairwise ( )
{
    //======================================== Free memory
    if ( this->_trSigmaEMatrix                != nullptr ) { delete[]    this->_trSigmaEMatrix           ; }
    if ( this->_so3Workspace3                 != nullptr ) { delete[]    this->_so3Workspace3            ; }
    if ( this->_so3Coeffs                     != nullptr ) { fftw_free ( this->_so3Coeffs               ); }
    if ( this->_so3InvCoeffs                  != nullptr ) { fftw_free ( this->_so3InvCoeffs            ); }
    if ( this->_so3Workspace1                 != nullptr ) { fftw_free ( this->_so3Workspace1           ); }
    if ( this->_so3Workspace2                 != nullptr ) { fftw_free ( this->_so3Workspace2           ); }
    if ( this->_so3InvCoeffs                  != nullptr ) { fftw_free ( this->_so3InvCoeffs            ); }
    
    //======================================== Done
    
}

/*! \brief Function to compute real part of the Gauss-Legendre integration over spherical harmonic values in different shells.
 
 This function takes the real parts of the spherical harmonics value in different shells and proceeds to compute
 the Gauss-Legendre integration over them. It assumes that the abscissas, weights and the integration order have
 all been determined up front, which they should have been by normal library usage.
 
 \param[in] vals Pointer to a vector of values over which the integration to be done.
 \param[out] X The real part of Gauss-Legendre integration over the shperical harmonics values.
 
 \warning This function only works with double precision.
 \warning This is an internal function not intended to be used by the user.
 */
double ProSHADE_internal::ProSHADE_comparePairwise::gl20IntRR ( std::vector<double>* vals )
{
    //======================================== Initialise local variables
    double ret;
    
    //======================================== Rescale to <order> points
    std::vector< std::array<double,2> > intData ( static_cast<unsigned int> ( this->_glAbscissas.size() ) );
    std::array<double,2> posVals;
    unsigned int lesserPos                    = 0;
    unsigned int upperPos                     = 0;
    double lesserWeight                       = 0.0;
    double upperWeight                        = 0.0;
    for ( unsigned int absIter = 0; absIter < static_cast<unsigned int> ( this->_glAbscissas.size() ); absIter++ )
    {
        //==================================== Init loop
        posVals[0]                            = 0.0;
        posVals[1]                            = 0.0;
        
        //==================================== Find real position of abscissas
        posVals[0]                            = ( ( this->_glAbscissas.at(absIter) + 1.0 ) / 2.0 ) * ( this->_shellSpacing * static_cast<double> ( vals->size() ) );
        
        //==================================== Find lesser and upper bounds
        for ( unsigned int valIt = 0; valIt < static_cast<unsigned int> ( vals->size() ); valIt++ )
        {
            if ( ( (valIt*this->_shellSpacing) <=  posVals[0] ) && ( ( (valIt+1)*this->_shellSpacing) > posVals[0] ) )
            {
                lesserPos                     = static_cast<unsigned int> ( valIt );
                upperPos                      = static_cast<unsigned int> ( valIt + 1 );
                break;
            }
        }
        
        //==================================== Linear Interpolation
        lesserWeight                          = 0.0;
        upperWeight                           = 0.0;
        if ( lesserPos != 0 )
        {
            //================================ Here we realise that the lesser and upper bounds were determined on scale 1 ... N, while our values are on scale 0 ... N-1 and therefore after determining the linear interpolation weights, we subtract 1 from both lesserPos and upperPos; however ...
            lesserWeight                      = upperPos - ( posVals[0] / this->_shellSpacing );
            upperWeight                       = 1.0 - lesserWeight;
            
            posVals[1]                        = ( lesserWeight * vals->at(lesserPos-1) ) + ( upperWeight * vals->at(upperPos-1) );
        }
        else
        {
            //================================ ... this then means that we would require position -1 for when the integration value is between 0 and the first shell. To resolve this, we assume that the values are 0 below the first shell and proceed as follows:
            upperWeight                       = 1.0 - ( upperPos - ( posVals[0] / this->_shellSpacing ) );
            
            posVals[1]                        = ( upperWeight * vals->at(upperPos-1) );
        }
        
        intData.at(absIter) = posVals;
    }
    
    //======================================== Integrate
    ret                                       = 0.0;
    
    for ( unsigned int absPoint = 0; absPoint < static_cast<unsigned int> ( intData.size() ); absPoint++ )
    {
        ret                                  += ( this->_glWeights.at(absPoint) * intData.at(absPoint)[1] );
    }
    
    //======================================== Normalise
    ret                                      *= ( ( static_cast<double> ( vals->size() ) * this->_shellSpacing ) / 2.0 );
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief Function to compute the Gauss-Legendre integration over spherical harmonic values in different shells.
 
 This function takes the complex spherical harmonics values in different shells and proceeds to compute
 the Gauss-Legendre integration over them. It assumes that the abscissas, weights and the integration order have
 all been determined up front, which they should have been by normal library usage.
 
 \param[in] vals Pointer to a vector of values over which the integration to be done.
 \param[out] X The Gauss-Legendre integration over the shperical harmonics values.
 
 \warning This function only works with double precision.
 \warning This is an internal function not intended to be used by the user.
 */
std::array<double,2> ProSHADE_internal::ProSHADE_comparePairwise::gl20IntCR ( std::vector< std::array<double,2> >* vals )
{
    //======================================== Initialise local variables
    std::array<double,2> ret;
    
    //======================================== Rescale to <order> points
    std::vector< std::array<double,3> > intData;
    std::array<double,3> posVals;
    unsigned int lesserPos                    = 0;
    unsigned int upperPos                     = 0;
    double lesserWeight                       = 0.0;
    double upperWeight                        = 0.0;
    for ( unsigned int absIter = 0; absIter < static_cast<unsigned int> ( this->_glAbscissas.size() ); absIter++ )
    {
        //==================================== Init
        posVals[0]                            = 0.0; posVals[1] = 0.0; posVals[2] = 0.0;
        
        //==================================== Find real position of abscissas
        posVals[0]                            = ( ( this->_glAbscissas.at(absIter) + 1.0 ) / 2.0 ) * ( this->_shellSpacing * static_cast<double> ( vals->size() ) );
        
        //==================================== Find lesser and upper bounds
        for ( unsigned int valIt = 0; valIt < static_cast<unsigned int> ( vals->size() ); valIt++ )
        {
            if ( ( (valIt*this->_shellSpacing) <=  posVals[0] ) && ( ( (valIt+1)*this->_shellSpacing) > posVals[0] ) )
            {
                lesserPos                     = static_cast<unsigned int> ( valIt );
                upperPos                      = static_cast<unsigned int> ( valIt + 1 );
                break;
            }
        }
        
        //==================================== Linear Interpolation
        lesserWeight                          = 0.0;
        upperWeight                           = 0.0;
        if ( lesserPos != 0 )
        {
            //================================ Here we realise that the lesser and upper bounds were determined on scale 1 ... N, while our values are on scale 0 ... N-1 and therefore after determining the linear interpolation weights, we subtract 1 from both lesserPos and upperPos; however ...
            lesserWeight                      = upperPos - ( posVals[0] / this->_shellSpacing );
            upperWeight                       = 1.0 - lesserWeight;
            
            posVals[1]                        = ( lesserWeight * vals->at(lesserPos-1)[0] ) + ( upperWeight * vals->at(upperPos-1)[0] );
            posVals[2]                        = ( lesserWeight * vals->at(lesserPos-1)[1] ) + ( upperWeight * vals->at(upperPos-1)[1] );
        }
        else
        {
            //================================ ... this then means that we would require position -1 for when the integration value is between 0 and the first shell. To resolve this, we assume that the values are 0 below the first shell and proceed as follows:
            upperWeight                       = 1.0 - ( upperPos - ( posVals[0] / this->_shellSpacing ) );
            
            posVals[1]                        = ( upperWeight * vals->at(upperPos-1)[0] );
            posVals[2]                        = ( upperWeight * vals->at(upperPos-1)[1] );
        }
        
        intData.emplace_back ( posVals );
    }
    
    //======================================== Integrate
    ret[0]                                    = 0.0;
    ret[1]                                    = 0.0;
    
    for ( unsigned int absPoint = 0; absPoint < static_cast<unsigned int> ( intData.size() ); absPoint++ )
    {
        ret[0]                               += ( this->_glWeights.at(absPoint) * intData.at(absPoint)[1] );
        ret[1]                               += ( this->_glWeights.at(absPoint) * intData.at(absPoint)[2] );
    }
    
    //======================================== Normalise
    ret[0]                                   *= ( ( static_cast<double> ( vals->size() ) * this->_shellSpacing ) / 2.0 );
    ret[1]                                   *= ( ( static_cast<double> ( vals->size() ) * this->_shellSpacing ) / 2.0 );
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief Contructor for the ProSHADE_compareOneAgainstAll class.
 
 This is the constructor for the executive class responsible for comparing two or more structures with different settings. This is currently internally used for
 distances computation; however the user should use the designated distances computation class ProSHADE_distances. The constructor is responsible for sanity checks, setting
 up parameters and determining the integration variables.
 
 \param[in] one This is the pointer to the first structure data holding object which will be compared againt the rest.
 \param[in] all This is the pointer to an array of structure data containing all the other structures to be compared against the one.
 \param[in] ignoreL This vector should contain all bands to be ignored in the computations.
 \param[in] matrixPowerWeight This is the weighting parameter for energy level distances computation.
 \param[out] X Object capable of comparing the two or more structures.
 
 \warning This class should not be directly accessed by the user.
 */
ProSHADE_internal::ProSHADE_compareOneAgainstAll::ProSHADE_compareOneAgainstAll ( ProSHADE_data *oneStr,
                                                                                  std::vector<ProSHADE_data*> *allStrs,
                                                                                  std::vector<int> ignoreL,
                                                                                  double matrixPowerWeight,
                                                                                  int verbose )
{
    //======================================== Initialise internal values
    this->one                                 = oneStr;
    this->two                                 = nullptr;
    this->all                                 = allStrs;
    std::array<double,3> emptyAngs;
    double checkValue                         = 0.0;
    emptyAngs[0]                              = -999.0;
    emptyAngs[1]                              = -999.0;
    emptyAngs[2]                              = -999.0;
    
    //======================================== Sanity checks
    if ( all->size() < 1 )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file comparison !!! The compare against part has not enough entries." << std::endl;
        exit ( -1 );
    }
    
    checkValue                                = static_cast<double> ( this->one->_keepOrRemove );
    for ( unsigned int strIt = 0; strIt < static_cast<unsigned int> ( this->all->size() ); strIt++ )
    {
        if ( checkValue != static_cast<double> ( this->all->at(strIt)->_keepOrRemove ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Error in file comparison between " << this->one->_inputFileName << " AND " << this->all->at(strIt)->_inputFileName << " !!! The phase treatment is different, use the same phase treatment (i.e. remove all, or keep all) to make the strucutres comparable." << std::endl;
            exit ( -1 );
        }
    }

    //======================================== Initialise internal values
    this->_lsToIgnore                         = ignoreL;
    this->_matrixPowerWeight                  = matrixPowerWeight;
    
    this->_energyLevelsComputed               = false;
    this->_trSigmaPreComputed                 = false;
    this->_trSigmaComputed                    = false;
    this->_so3InvMapComputed                  = false;
    this->_eulerAnglesFound                   = false;
    this->_wignerMatricesComputed             = false;
    this->_fullDistComputed                   = false;
    
    //======================================== Done
    
}

/*! \brief Contructor for the ProSHADE_compareOneAgainstAll class with reverse phase allowed.
 
 This is the constructor for the executive class responsible for comparing two or more structures with different settings. This is currently internally used for
 distances computation; however the user should use the designated distances computation class ProSHADE_distances. The constructor is responsible for sanity checks, setting
 up parameters and determining the integration variables.
 
 \param[in] one This is the pointer to the first structure data holding object which will be compared againt the rest.
 \param[in] one This is the pointer to the second structure data holding object (same as the first, but with reverse phase) which will be compared againt the rest.
 \param[in] all This is the pointer to an array of structure data containing all the other structures to be compared against the one. This assumes this vector has been read from a database with phase reversal allowed, so that one structure is with phase and the other is without. Othewise, there be dragons.
 \param[in] ignoreL This vector should contain all bands to be ignored in the computations.
 \param[in] matrixPowerWeight This is the weighting parameter for energy level distances computation.
 \param[out] X Object capable of comparing the two or more structures.
 
 \warning This class should not be directly accessed by the user.
 */
ProSHADE_internal::ProSHADE_compareOneAgainstAll::ProSHADE_compareOneAgainstAll ( ProSHADE_data *oneStr,
                                                                                  ProSHADE_data *twoStr,
                                                                                  std::vector<ProSHADE_data*> *allStrs,
                                                                                  std::vector<int> ignoreL,
                                                                                  double matrixPowerWeight,
                                                                                  int verbose )
{
    //======================================== Initialise internal values
    this->one                                 = oneStr;
    this->two                                 = twoStr;
    this->all                                 = allStrs;
    std::array<double,3> emptyAngs;
    double checkValue                         = 0.0;
    emptyAngs[0]                              = -999.0;
    emptyAngs[1]                              = -999.0;
    emptyAngs[2]                              = -999.0;
    
    //======================================== Sanity checks
    if ( all->size() < 1 )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file comparison !!! The compare against part has not enough entries." << std::endl;
        exit ( -1 );
    }
    
    checkValue                                = static_cast<double> ( this->one->_keepOrRemove );
    for ( unsigned int strIt = 0; strIt < static_cast<unsigned int> ( this->all->size() ); strIt += 2 )
    {
        if ( checkValue != static_cast<double> ( this->all->at(strIt)->_keepOrRemove ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Error in file comparison between " << this->one->_inputFileName << " AND " << this->all->at(strIt)->_inputFileName << " !!! The phase treatment is different, use the same phase treatment (i.e. remove all, or keep all) to make the strucutres comparable." << std::endl;
            exit ( -1 );
        }
    }
    
    checkValue                                = static_cast<double> ( this->two->_keepOrRemove );
    for ( unsigned int strIt = 1; strIt < static_cast<unsigned int> ( this->all->size() ); strIt += 2 )
    {
        if ( checkValue != static_cast<double> ( this->all->at(strIt)->_keepOrRemove ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Error in file comparison between " << this->one->_inputFileName << " AND " << this->all->at(strIt)->_inputFileName << " !!! The phase treatment is different, use the same phase treatment (i.e. remove all, or keep all) to make the strucutres comparable." << std::endl;
            exit ( -1 );
        }
    }
    
    //======================================== Initialise internal values
    this->_lsToIgnore                         = ignoreL;
    this->_matrixPowerWeight                  = matrixPowerWeight;
    
    this->_energyLevelsComputed               = false;
    this->_trSigmaPreComputed                 = false;
    this->_trSigmaComputed                    = false;
    this->_so3InvMapComputed                  = false;
    this->_eulerAnglesFound                   = false;
    this->_wignerMatricesComputed             = false;
    this->_fullDistComputed                   = false;
    
    //======================================== Done
    
}

/*! \brief Destructor for the ProSHADE_compareOneAgainstAll class.
 
 This destructor is empty as its role has been taken over by the classes this are directly used by the user; this is because
 the data should only be deleted when the user is done using the user-accessible class and not before that.
 
 \warning This is an internal function which should not be used by the user.
 */
ProSHADE_internal::ProSHADE_compareOneAgainstAll::~ProSHADE_compareOneAgainstAll ( )
{
    //======================================== Done
    if ( this->one != nullptr )    { delete this->one; }
    if ( this->two != nullptr )    { delete this->two; }
    
    this->all->clear                          ( );
}

/*! \brief Function to compute real part of the Gauss-Legendre integration over spherical harmonic values in different shells.
 
 This function takes the real parts of the spherical harmonics value in different shells and proceeds to compute
 the Gauss-Legendre integration over them. It assumes that the abscissas, weights and the integration order have
 all been determined up front, which they should have been by normal library usage.
 
 \param[in] vals Pointer to a vector of values over which the integration to be done.
 \param[in] shellSep The distance between the shells.
 \param[in] glAbscissas Pointer to a vector of the abscissas for the integration.
 \param[in] glWeights Pointer to a vector of weights for the integration.
 \param[out] X The real part of Gauss-Legendre integration over the shperical harmonics values.
 
 \warning This function only works with double precision.
 \warning This is an internal function not intended to be used by the user.
 */
double ProSHADE_internal::ProSHADE_compareOneAgainstAll::glIntRR ( std::vector<double>* vals,
                                                                   double shellSep,
                                                                   std::vector<double>* glAbscissas,
                                                                   std::vector<double>* glWeights )
{
    //======================================== Initialise local variables
    double ret;
    
    //======================================== Rescale to <order> points
    std::vector< std::array<double,2> > intData;
    std::array<double,2> posVals;
    unsigned int lesserPos                    = 0;
    unsigned int upperPos                     = 0;
    double lesserWeight                       = 0.0;
    double upperWeight                        = 0.0;
    
    for ( unsigned int absIter = 0; absIter < static_cast<unsigned int> ( glAbscissas->size() ); absIter++ )
    {
        //==================================== Init
        posVals[0]                            = 0.0;
        posVals[1]                            = 0.0;
        
        //==================================== Find real position of abscissas
        posVals[0]                            = ( ( glAbscissas->at(absIter) + 1.0 ) / 2.0 ) * ( shellSep * static_cast<double> ( vals->size() ) );
        
        //==================================== Find lesser and upper bounds
        for ( unsigned int valIt = 0; valIt < static_cast<unsigned int> ( vals->size() ); valIt++ )
        {
            if ( ( (valIt*shellSep) <=  posVals[0] ) && ( ( (valIt+1)*shellSep) > posVals[0] ) )
            {
                lesserPos                     = static_cast<unsigned int> ( valIt );
                upperPos                      = static_cast<unsigned int> ( valIt + 1 );
                break;
            }
        }
        
        //==================================== Linear Interpolation
        lesserWeight                          = 0.0;
        upperWeight                           = 0.0;
        if ( lesserPos != 0 )
        {
            //================================ Here we realise that the lesser and upper bounds were determined on scale 1 ... N, while our values are on scale 0 ... N-1 and therefore after determining the linear interpolation weights, we subtract 1 from both lesserPos and upperPos; however ...
            lesserWeight                      = upperPos - ( posVals[0] / shellSep );
            upperWeight                       = 1.0 - lesserWeight;
            
            posVals[1]                        = ( lesserWeight * vals->at(lesserPos-1) ) + ( upperWeight * vals->at(upperPos-1) );
        }
        else
        {
            //================================ ... this then means that we would require position -1 for when the integration value is between 0 and the first shell. To resolve this, we assume that the values are 0 below the first shell and proceed as follows:
            upperWeight                       = 1.0 - ( upperPos - ( posVals[0] / shellSep ) );
            
            posVals[1]                        = ( upperWeight * vals->at(upperPos-1) );
        }
        
        intData.emplace_back ( posVals );
    }
    
    //======================================== Integrate
    ret                                       = 0.0;
    
    for ( unsigned int absPoint = 0; absPoint < static_cast<unsigned int> ( intData.size() ); absPoint++ )
    {
        ret                                  += ( glWeights->at(absPoint) * intData.at(absPoint)[1] );
    }
    
    //======================================== Normalise
    ret                                      *= ( ( static_cast<double> ( vals->size() ) * shellSep ) / 2.0 );
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief Function to compute the Gauss-Legendre integration over spherical harmonic values in different shells.
 
 This function takes the complex spherical harmonics values in different shells and proceeds to compute
 the Gauss-Legendre integration over them. It assumes that the abscissas, weights and the integration order have
 all been determined up front, which they should have been by normal library usage.
 
 \param[in] vals Pointer to a vector of values over which the integration to be done.
 \param[in] shellSep The distance between the shells.
 \param[in] glAbscissas Pointer to a vector of the abscissas for the integration.
 \param[in] glWeights Pointer to a vector of weights for the integration.
 \param[out] X The Gauss-Legendre integration over the shperical harmonics values.
 
 \warning This function only works with double precision.
 \warning This is an internal function not intended to be used by the user.
 */
std::array<double,2> ProSHADE_internal::ProSHADE_compareOneAgainstAll::glIntCR ( std::vector< std::array<double,2> >* vals,
                                                                                 double shellSep,
                                                                                 std::vector<double> *glAbscissas,
                                                                                 std::vector<double> *glWeights )
{
    //======================================== Initialise local variables
    std::array<double,2> ret;
    
    //======================================== Rescale to <order> points
    std::vector< std::array<double,3> > intData;
    std::array<double,3> posVals;
    unsigned int lesserPos                    = 0;
    unsigned int upperPos                     = 0;
    double lesserWeight                       = 0.0;
    double upperWeight                        = 0.0;
    
    for ( unsigned int absIter = 0; absIter < static_cast<unsigned int> ( glAbscissas->size() ); absIter++ )
    {
        //==================================== Init
        posVals[0]                            = 0.0;
        posVals[1]                            = 0.0;
        posVals[2]                            = 0.0;
        
        //==================================== Find real position of abscissas
        posVals[0]                            = ( ( glAbscissas->at(absIter) + 1.0 ) / 2.0 ) * ( shellSep * static_cast<double> ( vals->size() ) );
        
        //==================================== Find lesser and upper bounds
        for ( unsigned int valIt = 0; valIt < static_cast<unsigned int> ( vals->size() ); valIt++ )
        {
            if ( ( (valIt*shellSep) <=  posVals[0] ) && ( ( (valIt+1)*shellSep) > posVals[0] ) )
            {
                lesserPos                     = static_cast<unsigned int> ( valIt );
                upperPos                      = static_cast<unsigned int> ( valIt + 1 );
                break;
            }
        }
        
        //==================================== Linear Interpolation
        lesserWeight                          = 0.0;
        upperWeight                           = 0.0;
        if ( lesserPos != 0 )
        {
            //================================ Here we realise that the lesser and upper bounds were determined on scale 1 ... N, while our values are on scale 0 ... N-1 and therefore after determining the linear interpolation weights, we subtract 1 from both lesserPos and upperPos; however ...
            lesserWeight                      = upperPos - ( posVals[0] / shellSep );
            upperWeight                       = 1.0 - lesserWeight;
            
            posVals[1]                        = ( lesserWeight * vals->at(lesserPos-1)[0] ) + ( upperWeight * vals->at(upperPos-1)[0] );
            posVals[2]                        = ( lesserWeight * vals->at(lesserPos-1)[1] ) + ( upperWeight * vals->at(upperPos-1)[1] );
        }
        else
        {
            //================================ ... this then means that we would require position -1 for when the integration value is between 0 and the first shell. To resolve this, we assume that the values are 0 below the first shell and proceed as follows:
            upperWeight                       = 1.0 - ( upperPos - ( posVals[0] / shellSep ) );
            
            posVals[1]                        = ( upperWeight * vals->at(upperPos-1)[0] );
            posVals[2]                        = ( upperWeight * vals->at(upperPos-1)[1] );
        }
        
        intData.emplace_back ( posVals );
    }
    
    //======================================== Integrate
    ret[0]                                    = 0.0;
    ret[1]                                    = 0.0;
    
    for ( unsigned int absPoint = 0; absPoint < static_cast<unsigned int> ( intData.size() ); absPoint++ )
    {
        ret[0]                               += ( glWeights->at(absPoint) * intData.at(absPoint)[1] );
        ret[1]                               += ( glWeights->at(absPoint) * intData.at(absPoint)[2] );
    }
    
    //======================================== Normalise
    ret[0]                                   *= ( ( static_cast<double> ( vals->size() ) * shellSep ) / 2.0 );
    ret[1]                                   *= ( ( static_cast<double> ( vals->size() ) * shellSep ) / 2.0 );
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief Contructor for the ProSHADE_symmetry class.
 
 This is an empty constructor to allow access to the internal functions without the need for initialisation and computation of any results.
 
 \param[out] X Empty object used only to allow access to internal functions without the need for initialisation.
 
 \warning This is an internal function which should not be used by the user.
 */
ProSHADE_internal::ProSHADE_symmetry::ProSHADE_symmetry ( )
{
    ;
}

/*! \brief Contructor for the ProSHADE_symmetry class.
 
 This is the constructor for the executive class which should be used by the user to obtain symmetry information about a single
 structure. This constructor contains all the symmetry running and reporting logic, while it relies on lower level functions for
 the specific computations. All computations are done immediately as the constructor is called, this may take some time, but all
 other operations are almost immediate and no more computations are required ever again. It should be used by the user instead
 of any other lower level classes.
 
 \param[out] X Object containing all symmetry information that ProSHADE library can provide.
 
 \warning This is an internal function which should not be used by the user.
 */
ProSHADE_internal::ProSHADE_symmetry::ProSHADE_symmetry ( ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Save the settings
    this->mapResolution                       = settings->mapResolution;
    this->bandwidth                           = std::vector<unsigned int> { { settings->bandwidth   , settings->bandwidth    } };
    this->glIntegOrder                        = std::vector<unsigned int> { { settings->glIntegOrder, settings->glIntegOrder } };
    this->theta                               = std::vector<unsigned int> { { settings->theta       , settings->theta        } };
    this->phi                                 = std::vector<unsigned int> { { settings->phi         , settings->phi          } };
    this->bFactorValue                        = settings->bFactorValue;
    this->bFactorChange                       = settings->bFactorChange;
    this->noIQRsFromMap                       = settings->noIQRsFromMap;
    this->shellSpacing                        = settings->shellSpacing;
    this->manualShells                        = settings->manualShells;
    this->useCOM                              = settings->useCOM;
    this->firstLineCOM                        = settings->firstLineCOM;
    this->extraSpace                          = settings->extraSpace;
    this->alpha                               = settings->alpha;
    this->mPower                              = settings->mPower;
    this->ignoreLs                            = settings->ignoreLs;
    this->peakHeightNoIQRs                    = settings->peakHeightNoIQRs;
    this->peakDistanceForReal                 = settings->peakDistanceForReal;
    this->peakSurroundingPoints               = settings->peakSurroundingPoints;
    this->aaErrorTolerance                    = settings->aaErrorTolerance;
    this->symGapTolerance                     = settings->symGapTolerance;
    this->printFull                           = false;
    this->structFiles                         = settings->structFiles;
    
    double shSpacingObj1                      = settings->shellSpacing;
    double shSpacingObj2                      = settings->shellSpacing;
    
    this->xPos                                = 0.0;
    this->yPos                                = 0.0;
    this->zPos                                = 0.0;

    //======================================== In the case of overlap computation, do it
    if ( settings->taskToPerform == ProSHADE::OverlayMap )
    {
        //==================================== Sanity checks
        if ( settings->structFiles.size() != 2 )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Not enough files/too many files detected for map overlay mode. Please supply a two structures using the -f or -i command line options. Terminating..." << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "The number of input structures is not two. Do not know how to proceed." << "</font>";
                rvapi_set_text                ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                   ( );
            }
            
            exit ( -1 );
        }
        
        bool wasMapNameGiven                  = true;
        if ( settings->clearMapFile == "" )
        {
            std::cout << "!!! ProSHADE WARNING !!! The output file name was not set. You may want to use the \'--clearMap\' option to set it. Using the default name \'rotStr\'." << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"orange\">" << "There is no filename to which the output matching structure should be saved. Will use defaul name 'rotStr'." << "</font>";
                rvapi_set_text                ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                   ( );
            }
            
            if ( ProSHADE_internal::checkFileType ( settings->structFiles.at(0) ) == 2 )
            {
                settings->clearMapFile        = "rotStr";
            }
            if ( ProSHADE_internal::checkFileType ( settings->structFiles.at(0) ) == 1 )
            {
                settings->clearMapFile        = "rotStr";
            }
            wasMapNameGiven                   = false;
        }
        
        if ( settings->verbose > 2 )
        {
            std::cout << ">>>>> Sanity checks passed." << std::endl;
        }
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Will move structure " << settings->structFiles.at(1) << " to match into structure " << settings->structFiles.at(0) << " .</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        //==================================== Change default resolution for the case of two PDB files
        unsigned int fileType                 = checkFileType ( structFiles.at(0) );
        
        //==================================== Read in structure
        ProSHADE_data* pattStr1               = new ProSHADE_data ();
        if ( fileType == 2 )
        {
            pattStr1->getDensityMapFromMAP    ( this->structFiles.at(0),
                                               &shSpacingObj1,
                                                this->mapResolution,
                                               &this->bandwidth.at(0),
                                               &this->theta.at(0),
                                               &this->phi.at(0),
                                               &this->glIntegOrder.at(0),
                                               &this->extraSpace,
                                                settings->mapResDefault,
                                                settings->rotChangeDefault,
                                                settings,
                                                settings->overlayDefaults );
        }
        else if ( fileType == 1 )
        {
            pattStr1->getDensityMapFromPDB    ( this->structFiles.at(0),
                                               &shSpacingObj1,
                                                this->mapResolution,
                                               &this->bandwidth.at(0),
                                               &this->theta.at(0),
                                               &this->phi.at(0),
                                               &this->glIntegOrder.at(0),
                                               &this->extraSpace,
                                                settings->mapResDefault,
                                                settings,
                                                this->bFactorValue,
                                                this->firstLineCOM,
                                                settings->overlayDefaults );
        }
        else
        {
            std::cerr << "!!! ProSHADE ERROR !!! Error loading file " << this->structFiles.at(0) << " !!! Cannot detect the extension (currently, only PDB or MAP are allowed) and therefore cannot read the file. Terminating ..." << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "Cannot read file " << settings->structFiles.at(0) << " ." << "</font>";
                rvapi_set_text                ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                   ( );
            }
            
            exit ( -1 );
        }
        
        if ( settings->verbose > 0 )
        {
            std::cout << "Structure 1 loaded." << std::endl;
        }
        
        //==================================== Read in structure 2
        fileType                              = checkFileType ( structFiles.at(1) );
        ProSHADE_data* pattStr2               = new ProSHADE_data ();
        if ( fileType == 2 )
        {
            pattStr2->getDensityMapFromMAP    ( this->structFiles.at(1),
                                               &shSpacingObj2,
                                                this->mapResolution,
                                               &this->bandwidth.at(1),
                                               &this->theta.at(1),
                                               &this->phi.at(1),
                                               &this->glIntegOrder.at(1),
                                               &this->extraSpace,
                                                settings->mapResDefault,
                                                settings->rotChangeDefault,
                                                settings,
                                                settings->overlayDefaults );
        }
        else if ( fileType == 1 )
        {
            pattStr2->getDensityMapFromPDB    ( this->structFiles.at(1),
                                               &shSpacingObj2,
                                                this->mapResolution,
                                               &this->bandwidth.at(1),
                                               &this->theta.at(1),
                                               &this->phi.at(1),
                                               &this->glIntegOrder.at(1),
                                               &this->extraSpace,
                                                settings->mapResDefault,
                                                settings,
                                                this->bFactorValue,
                                                this->firstLineCOM,
                                                settings->overlayDefaults );
        }
        else
        {
            std::cerr << "!!! ProSHADE ERROR !!! Error loading file " << this->structFiles.at(1) << " !!! Cannot detect the extension (currently, only PDB or MAP are allowed) and therefore cannot read the file. Terminating ..." << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "Cannot read file " << settings->structFiles.at(1) << " ." << "</font>";
                rvapi_set_text                ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                   ( );
            }
            
            exit ( -1 );
        }
        
        if ( settings->verbose > 0 )
        {
            std::cout << "Structure 2 loaded." << std::endl;
        }
        
        //==================================== Force same band, theta, phi and glInteg
        unsigned int maxBand                  = std::max ( this->bandwidth.at(0)   , this->bandwidth.at(1)    );
        unsigned int maxTheta                 = std::max ( this->theta.at(0)       , this->theta.at(1)        );
        unsigned int maxPhi                   = std::max ( this->phi.at(0)         , this->phi.at(1)          );
        unsigned int maxGlInteg               = std::max ( this->glIntegOrder.at(0), this->glIntegOrder.at(1) );
        double maxSpacing                     = std::max ( shSpacingObj1, shSpacingObj2 );
        
        if ( ( shSpacingObj1 != shSpacingObj2 )         || ( this->bandwidth.at(0) != this->bandwidth.at(1) ) ||
             ( this->theta.at(0) != this->theta.at(1) ) || ( this->phi.at(0) != this->phi.at(1) )             ||
             ( this->glIntegOrder.at(0) != this->glIntegOrder.at(1) ) )
        {
            this->bandwidth.at(0)             = maxBand;
            this->bandwidth.at(1)             = maxBand;
            this->theta.at(0)                 = maxTheta;
            this->theta.at(1)                 = maxTheta;
            this->phi.at(0)                   = maxPhi;
            this->phi.at(1)                   = maxPhi;
            this->glIntegOrder.at(0)          = maxGlInteg;
            this->glIntegOrder.at(1)          = maxGlInteg;
            shSpacingObj1                     = maxSpacing;
            shSpacingObj2                     = maxSpacing;
            
            //================================ Re-read in structure 1
            delete pattStr1;
            shSpacingObj1                     = shSpacingObj2;
            
            pattStr1                          = new ProSHADE_data ();
            fileType                          = checkFileType ( structFiles.at(0) );
            if ( fileType == 2 )
            {
                pattStr1->getDensityMapFromMAP ( this->structFiles.at(0),
                                                &shSpacingObj1,
                                                 this->mapResolution,
                                                &this->bandwidth.at(0),
                                                &this->theta.at(0),
                                                &this->phi.at(0),
                                                &this->glIntegOrder.at(0),
                                                &this->extraSpace,
                                                 settings->mapResDefault,
                                                 settings->rotChangeDefault,
                                                 settings,
                                                 settings->overlayDefaults );
            }
            else if ( fileType == 1 )
            {
                pattStr1->getDensityMapFromPDB ( this->structFiles.at(0),
                                                &shSpacingObj1,
                                                 this->mapResolution,
                                                &this->bandwidth.at(0),
                                                &this->theta.at(0),
                                                &this->phi.at(0),
                                                &this->glIntegOrder.at(0),
                                                &this->extraSpace,
                                                 settings->mapResDefault,
                                                 settings,
                                                 this->bFactorValue,
                                                 this->firstLineCOM,
                                                 settings->overlayDefaults );
            }
            else
            {
                std::cerr << "!!! ProSHADE ERROR !!! Error loading file " << this->structFiles.at(0) << " !!! Cannot detect the extension (currently, only PDB or MAP are allowed) and therefore cannot read the file. Terminating ..." << std::endl;
                
                if ( settings->htmlReport )
                {
                    std::stringstream hlpSS;
                    hlpSS << "<font color=\"red\">" << "Cannot read file " << settings->structFiles.at(0) << " ." << "</font>";
                    rvapi_set_text                ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                    settings->htmlReportLineProgress += 1;
                    rvapi_flush                   ( );
                }
                
                exit ( -1 );
            }
            
            //============================ Re-read in structure 2
            delete pattStr2;
            shSpacingObj2                     = shSpacingObj1;
            
            fileType                          = checkFileType ( structFiles.at(1) );
            pattStr2                          = new ProSHADE_data ();
            if ( fileType == 2 )
            {
                pattStr2->getDensityMapFromMAP ( this->structFiles.at(1),
                                                &shSpacingObj2,
                                                 this->mapResolution,
                                                &this->bandwidth.at(1),
                                                &this->theta.at(1),
                                                &this->phi.at(1),
                                                &this->glIntegOrder.at(1),
                                                &this->extraSpace,
                                                 settings->mapResDefault,
                                                 settings->rotChangeDefault,
                                                 settings,
                                                 settings->overlayDefaults );
            }
            else if ( fileType == 1 )
            {
                pattStr2->getDensityMapFromPDB ( this->structFiles.at(1),
                                                &shSpacingObj2,
                                                 this->mapResolution,
                                                &this->bandwidth.at(1),
                                                &this->theta.at(1),
                                                &this->phi.at(1),
                                                &this->glIntegOrder.at(1),
                                                &this->extraSpace,
                                                 settings->mapResDefault,
                                                 settings,
                                                 this->bFactorValue,
                                                 this->firstLineCOM,
                                                 settings->overlayDefaults );
            }
            else
            {
                std::cerr << "!!! ProSHADE ERROR !!! Error loading file " << this->structFiles.at(1) << " !!! Cannot detect the extension (currently, only PDB or MAP are allowed) and therefore cannot read the file. Terminating ..." << std::endl;
                
                if ( settings->htmlReport )
                {
                    std::stringstream hlpSS;
                    hlpSS << "<font color=\"red\">" << "Cannot read file " << settings->structFiles.at(1) << " ." << "</font>";
                    rvapi_set_text                ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                    settings->htmlReportLineProgress += 1;
                    rvapi_flush                   ( );
                }
                
                exit ( -1 );
            }
        }
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Loaded structures in Patterson mode for rotation computation." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        //==================================== Remove models (if any) from the map
        if ( settings->deleteModels.size() > 0 )
        {
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( settings->deleteModels.size() ); iter++ )
            {
                pattStr1->deleteModel         ( settings->deleteModels.at(iter) );
            }
        }

        //==================================== Remove phase and get spherical coordinatse
        pattStr1->removePhaseFromMapOverlay   ( this->alpha,
                                                this->bFactorChange,
                                                settings );
        
        pattStr1->mapPhaselessToSphere        ( settings,
                                                this->theta.at(0),
                                                this->phi.at(0),
                                                shSpacingObj1,
                                                settings->manualShells );
        
        pattStr1->getSphericalHarmonicsCoeffs ( this->bandwidth.at(0), settings );
        if ( settings->verbose > 1 )
        {
            std::cout << ">> Structure 1 spherical harmonics computed." << std::endl;
        }
        
        pattStr2->removePhaseFromMapOverlay   ( this->alpha,
                                                this->bFactorChange,
                                                settings );
        
        pattStr2->mapPhaselessToSphere        ( settings,
                                                this->theta.at(1),
                                                this->phi.at(1),
                                                shSpacingObj2,
                                                settings->manualShells );
        
        pattStr2->getSphericalHarmonicsCoeffs ( this->bandwidth.at(1), settings );
        if ( settings->verbose > 1 )
        {
            std::cout << ">> Structure 2 spherical harmonics computed." << std::endl;
        }
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Spherical harmonics for Patterson maps computed." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        //==================================== Get the Compare_Pairwise object to compute E matrices and find the maximum SOFT peak
        ProSHADE_comparePairwise* cmpObj      = new ProSHADE_comparePairwise ( pattStr1,
                                                                               pattStr2,
                                                                               this->mPower,
                                                                               this->ignoreLs,
                                                                               std::max ( this->glIntegOrder.at(0),
                                                                                          this->glIntegOrder.at(1) ),
                                                                               settings );
        
        //==================================== Get the angles
        cmpObj->precomputeTrSigmaDescriptor   ( );
        if ( settings->verbose > 2 )
        {
            std::cout << ">>>>> E matrices constructed." << std::endl;
        }
        
        cmpObj->getSO3InverseMap              ( settings );
        if ( settings->verbose > 2 )
        {
            std::cout << ">>>>> Inverse SO(3) Fourier transform map obtained." << std::endl;
        }
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Rotation function map computed." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        double pattCorrelation                = 0.0;
        
        std::array<double,3> euAngs           = cmpObj->getEulerAngles ( settings, &pattCorrelation );
        
        if ( settings->verbose > 1 )
        {
            std::cout << ">> Patterson map based rotation angles obtained ( " << euAngs[0] << ", " << euAngs[1] << " and " << euAngs[2] << " )." << std::endl;
        }
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Found optimal overlay rotation angles." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        if ( settings->htmlReport )
        {
            //==================================== Create section
            rvapi_add_section                 ( "RotationSection",
                                                "Rotation information",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                false );
            settings->htmlReportLine         += 1;
            
            rvapi_flush                       ( );
            
            std::stringstream hlpSS;
            hlpSS << "<pre>" << "Rotation in Euler Angles: ";
            int hlpIt                         = static_cast<int> ( 70 - hlpSS.str().length() );
            for ( int iter = 0; iter < hlpIt; iter++ )
            {
                hlpSS << ".";
            }
            
            std::stringstream hlpSS2;
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( euAngs[0] * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            hlpSS << " " << hlpSS2.str() << "   ";
            hlpSS2.str( std::string ( ) );
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( euAngs[1] * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            hlpSS << hlpSS2.str() << "   ";
            hlpSS2.str( std::string ( ) );
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( euAngs[2] * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            hlpSS << hlpSS2.str() << "</pre>";
            
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "RotationSection",
                                                0,
                                                1,
                                                1,
                                                1 );
            
            hlpSS.str ( std::string ( ) );
            hlpSS << "<pre>" << "Rotation in Angle-Axis: ";
            hlpIt                             = static_cast<int> ( 70 - hlpSS.str().length() );
            for ( int iter = 0; iter < hlpIt; iter++ )
            {
                hlpSS << ".";
            }
            
            double X, Y, Z, Ang;
            ProSHADE_internal_misc::getAxisAngleFromEuler ( euAngs[0], euAngs[1], euAngs[2], &X, &Y, &Z, &Ang, false );
            
            hlpSS2.str( std::string ( ) );
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( X * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            hlpSS << " " << hlpSS2.str() << "   ";
            hlpSS2.str( std::string ( ) );
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( Y * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            hlpSS << hlpSS2.str() << "   ";
            hlpSS2.str( std::string ( ) );
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( Z * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            hlpSS << hlpSS2.str() << "   ";
            hlpSS2.str( std::string ( ) );
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( Ang * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            hlpSS << hlpSS2.str() << "</pre>";
            
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "RotationSection",
                                               1,
                                               1,
                                               1,
                                               1 );
            
            std::array<double,5> arrHlp;
            arrHlp[0]                         = Ang;
            arrHlp[1]                         = X;
            arrHlp[2]                         = Y;
            arrHlp[3]                         = Z;
            arrHlp[4]                         = 0.0;
            std::vector< std::vector<double> > rMat = ProSHADE_internal_misc::getMatrixFromAxisAngle ( arrHlp );
            
            hlpSS.str ( std::string ( ) );
            hlpSS << "<pre>" << "Rotation as Rotation Matrix: ";
            hlpIt                             = static_cast<int> ( 70 - hlpSS.str().length() );
            for ( int iter = 0; iter < hlpIt; iter++ )
            {
                hlpSS << ".";
            }
            
            hlpSS2.str( std::string ( ) );
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( rMat.at(0).at(0) * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            hlpSS << " " << hlpSS2.str() << "   ";
            hlpSS2.str( std::string ( ) );
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( rMat.at(0).at(1) * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            hlpSS << hlpSS2.str() << "   ";
            hlpSS2.str( std::string ( ) );
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( rMat.at(0).at(2) * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            hlpSS << hlpSS2.str() << "</pre>";
            
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "RotationSection",
                                               2,
                                               1,
                                               1,
                                               1 );
            
            hlpSS.str ( std::string ( ) );
            hlpSS << "<pre>" << " ... ";
            hlpIt                             = static_cast<int> ( 70 - hlpSS.str().length() );
            for ( int iter = 0; iter < hlpIt; iter++ )
            {
                hlpSS << " ";
            }
            
            hlpSS2.str( std::string ( ) );
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( rMat.at(1).at(0) * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            hlpSS << " " << hlpSS2.str() << "   ";
            hlpSS2.str( std::string ( ) );
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( rMat.at(1).at(1) * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            hlpSS << hlpSS2.str() << "   ";
            hlpSS2.str( std::string ( ) );
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( rMat.at(1).at(2) * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            hlpSS << hlpSS2.str() << "</pre>";
            
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "RotationSection",
                                               3,
                                               1,
                                               1,
                                               1 );
            
            hlpSS.str ( std::string ( ) );
            hlpSS << "<pre>" << " ... ";
            hlpIt                             = static_cast<int> ( 70 - hlpSS.str().length() );
            for ( int iter = 0; iter < hlpIt; iter++ )
            {
                hlpSS << " ";
            }
            
            hlpSS2.str( std::string ( ) );
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( rMat.at(2).at(0) * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            hlpSS << " " << hlpSS2.str() << "   ";
            hlpSS2.str( std::string ( ) );
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( rMat.at(2).at(1) * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            hlpSS << hlpSS2.str() << "   ";
            hlpSS2.str( std::string ( ) );
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( rMat.at(2).at(2) * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            hlpSS << hlpSS2.str() << "</pre>";
            
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "RotationSection",
                                               4,
                                               1,
                                               1,
                                               1 );
            
            rvapi_flush                       ( );
        }
        
        //==================================== Free memory
        delete cmpObj;
        delete pattStr1;
        delete pattStr2;
        
        //==================================== Prepare vatiables
        std::array<double,4> translationVec;
        double xMapMov                        = 0.0;
        double yMapMov                        = 0.0;
        double zMapMov                        = 0.0;
        double xMapTotMov                     = 0.0;
        double yMapTotMov                     = 0.0;
        double zMapTotMov                     = 0.0;
        
        while ( true )
        {
            //================================ Load structure 2 with phases
            pattStr2                          = new ProSHADE_data ();
            fileType                          = checkFileType ( structFiles.at(1) );
            if ( fileType == 2 )
            {
                pattStr2->getDensityMapFromMAP ( this->structFiles.at(1),
                                                &shSpacingObj2,
                                                 this->mapResolution,
                                                &this->bandwidth.at(1),
                                                &this->theta.at(1),
                                                &this->phi.at(1),
                                                &this->glIntegOrder.at(1),
                                                &this->extraSpace,
                                                 settings->mapResDefault,
                                                 settings->rotChangeDefault,
                                                 settings,
                                                 settings->overlayDefaults );
            }
            else if ( fileType == 1 )
            {
                pattStr2->getDensityMapFromPDB ( this->structFiles.at(1),
                                                &shSpacingObj2,
                                                 this->mapResolution,
                                                &this->bandwidth.at(1),
                                                &this->theta.at(1),
                                                &this->phi.at(1),
                                                &this->glIntegOrder.at(1),
                                                &this->extraSpace,
                                                 settings->mapResDefault,
                                                 settings,
                                                 this->bFactorValue,
                                                 this->firstLineCOM,
                                                 settings->overlayDefaults );
            }
            else
            {
                std::cerr << "!!! ProSHADE ERROR !!! Error loading file " << this->structFiles.at(1) << " !!! Cannot detect the extension (currently, only PDB or MAP are allowed) and therefore cannot read the file. Terminating ..." << std::endl;
                
                if ( settings->htmlReport )
                {
                    std::stringstream hlpSS;
                    hlpSS << "<font color=\"red\">" << "Cannot read file " << settings->structFiles.at(1) << " ." << "</font>";
                    rvapi_set_text                ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                    settings->htmlReportLineProgress += 1;
                    rvapi_flush                   ( );
                }
                
                exit ( -1 );
            }
            
//            std::array<double,4> str2CandD    = pattStr2->getCOMandDist ( settings );
            pattStr2->keepPhaseInMap          ( this->alpha,
                                                this->bFactorChange,
                                               &this->bandwidth.at(1),
                                               &this->theta.at(1),
                                               &this->phi.at(1),
                                               &this->glIntegOrder.at(1),
                                                settings,
                                                this->useCOM,
                                                settings->noIQRsFromMap,
                                                settings->verbose,
                                                settings->clearMapData,
                                                settings->rotChangeDefault,
                                                settings->overlayDefaults,
                                                settings->maskBlurFactor,
                                                settings->maskBlurFactorGiven );
            
            pattStr2->mapPhaselessToSphere    ( settings,
                                                this->theta.at(1),
                                                this->phi.at(1),
                                                shSpacingObj2,
                                                settings->manualShells,
                                                true,
                                                true );
            
            pattStr2->getSphericalHarmonicsCoeffs ( this->bandwidth.at(1), settings );
            if ( settings->verbose > 1 )
            {
                std::cout << ">> Structure 2 spherical harmonics computed with phase." << std::endl;
            }
            
            //================================ Read in structure 1, this time with phases
            fileType                          = checkFileType ( structFiles.at(0) );
            pattStr1                          = new ProSHADE_data ();
            
            if ( fileType == 2 )
            {
                pattStr1->getDensityMapFromMAP ( this->structFiles.at(0),
                                                &shSpacingObj1,
                                                 this->mapResolution,
                                                &this->bandwidth.at(0),
                                                &this->theta.at(0),
                                                &this->phi.at(0),
                                                &this->glIntegOrder.at(0),
                                                &this->extraSpace,
                                                 settings->mapResDefault,
                                                 settings->rotChangeDefault,
                                                 settings,
                                                 settings->overlayDefaults );
            }
            else if ( fileType == 1 )
            {
                pattStr1->getDensityMapFromPDB ( this->structFiles.at(0),
                                                &shSpacingObj1,
                                                 this->mapResolution,
                                                &this->bandwidth.at(0),
                                                &this->theta.at(0),
                                                &this->phi.at(0),
                                                &this->glIntegOrder.at(0),
                                                &this->extraSpace,
                                                 settings->mapResDefault,
                                                 settings,
                                                 this->bFactorValue,
                                                 this->firstLineCOM,
                                                 settings->overlayDefaults );
            }
            else
            {
                std::cerr << "!!! ProSHADE ERROR !!! Error loading file " << this->structFiles.at(0) << " !!! Cannot detect the extension (currently, only PDB or MAP are allowed) and therefore cannot read the file. Terminating ..." << std::endl;
                
                if ( settings->htmlReport )
                {
                    std::stringstream hlpSS;
                    hlpSS << "<font color=\"red\">" << "Cannot read file " << settings->structFiles.at(0) << " ." << "</font>";
                    rvapi_set_text                ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                    settings->htmlReportLineProgress += 1;
                    rvapi_flush                   ( );
                }
                
                exit ( -1 );
            }
        
            pattStr1->keepPhaseInMap          ( this->alpha,
                                                this->bFactorChange,
                                               &this->bandwidth.at(0),
                                               &this->theta.at(0),
                                               &this->phi.at(0),
                                               &this->glIntegOrder.at(0),
                                                settings,
                                                this->useCOM,
                                                settings->noIQRsFromMap,
                                                settings->verbose,
                                                settings->clearMapData,
                                                settings->rotChangeDefault,
                                                settings->overlayDefaults,
                                                settings->maskBlurFactor,
                                                settings->maskBlurFactorGiven );
            
            pattStr1->mapPhaselessToSphere    ( settings,
                                                this->theta.at(0),
                                                this->phi.at(0),
                                                shSpacingObj1,
                                                settings->manualShells,
                                                true,
                                                true );
            
            pattStr1->getSphericalHarmonicsCoeffs ( this->bandwidth.at(0), settings );
            if ( settings->verbose > 1 )
            {
                std::cout << ">> Structure 1 spherical harmonics computed with phase." << std::endl;
            }
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"green\">" << "Spherical harmonics computed for translation computation (with phases)." << "</font>";
                rvapi_set_text                ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                   ( );
            }
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<pre>" << "Rotation about coordinate location (A): ";
                int hlpIt                     = static_cast<int> ( 70 - hlpSS.str().length() );
                for ( int iter = 0; iter < hlpIt; iter++ )
                {
                    hlpSS << ".";
                }
                
                std::stringstream hlpSS2;
                double pos                    = ( ( pattStr2->_xFrom * pattStr2->_xSamplingRate ) + pattStr2->_xRange ) / 2.0;
                hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( pos * 1000.0 ) / 1000.0;
                if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
                if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
                hlpSS << " " << hlpSS2.str() << "   ";
                hlpSS2.str( std::string ( ) );
                pos                           = ( ( pattStr2->_yFrom * pattStr2->_ySamplingRate ) + pattStr2->_yRange ) / 2.0;
                hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( pos * 1000.0 ) / 1000.0;
                if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
                if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
                hlpSS << hlpSS2.str() << "   ";
                hlpSS2.str( std::string ( ) );
                pos                           = ( ( pattStr2->_zFrom * pattStr2->_zSamplingRate ) + pattStr2->_zRange ) / 2.0;
                hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( pos * 1000.0 ) / 1000.0;
                if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
                if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
                hlpSS << hlpSS2.str() << "</pre>";
                
                rvapi_set_text                ( hlpSS.str().c_str(),
                                                "RotationSection",
                                                5,
                                                1,
                                                1,
                                                1 );
                rvapi_flush                   ( );
            }
            
            //================================ Find rotation angles
            cmpObj                            = new ProSHADE_comparePairwise ( pattStr2,
                                                                               pattStr2,
                                                                               this->mPower,
                                                                               this->ignoreLs,
                                                                               std::max ( this->glIntegOrder.at(0),
                                                                                          this->glIntegOrder.at(1) ),
                                                                               settings );
            
            //================================ Find 'opposite' Euler angles and set them
            // Get mat from Euler
            double mat22                      =  cos ( euAngs[1]  );
            double mat02                      = -sin ( euAngs[1]  ) * cos ( euAngs[2] );
            double mat12                      =  sin ( euAngs[1]  ) * sin ( euAngs[2] );
            double mat20                      =  cos ( euAngs[0] ) * sin ( euAngs[1]  );
            double mat21                      =  sin ( euAngs[0] ) * sin ( euAngs[1]  );
            
            // Transpose
            double rMat02                     = mat20;
            double rMat20                     = mat02;
            double rMat21                     = mat12;
            double rMat12                     = mat21;
            
            // Get Euler from map
            euAngs[0]                         = atan2 ( rMat21,  rMat20 );
            euAngs[1]                         = acos  ( mat22 );
            euAngs[2]                         = atan2 ( rMat12, -rMat02 );
            
            if ( euAngs[0] < 0.0 ) { euAngs[0]= 2.0 * M_PI + euAngs[0]; }
            if ( euAngs[1] < 0.0 ) { euAngs[1]=       M_PI + euAngs[1]; }
            if ( euAngs[2] < 0.0 ) { euAngs[2]= 2.0 * M_PI + euAngs[2]; }
            
            cmpObj->setEulerAngles            ( euAngs[0], euAngs[1], euAngs[2] );

            //================================ Rotate by the Euler
            cmpObj->rotateStructure           ( pattStr2,
                                                settings,
                                                settings->clearMapFile,
                                                settings->verbose,
                                                settings->axisOrder,
                                                true );
            
            //================================ Find optimal translation
            ProSHADE_data str1Copy            = ProSHADE_data ( pattStr1 );
            translationVec                    = cmpObj->getTranslationFunctionMap ( &str1Copy, pattStr2, &xMapMov, &yMapMov, &zMapMov );
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"green\">" << "Translation computed." << "</font>";
                rvapi_set_text                ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                   ( );
            }
            
            if ( settings->htmlReport )
            {
                //==================================== Create section
                rvapi_add_section                 ( "TranslationSection",
                                                    "Translation information",
                                                    "body",
                                                    settings->htmlReportLine,
                                                    0,
                                                    1,
                                                    1,
                                                    false );
                settings->htmlReportLine         += 1;
                
                rvapi_flush                       ( );
                
                std::stringstream hlpSS;
                hlpSS << "<pre>" << "Translation from lowest indices (A): ";
                int hlpIt                     = static_cast<int> ( 70 - hlpSS.str().length() );
                for ( int iter = 0; iter < hlpIt; iter++ )
                {
                    hlpSS << ".";
                }
                
                std::stringstream hlpSS2;
                hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( -translationVec[0] * 1000.0 ) / 1000.0;
                if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
                if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
                hlpSS << " " << hlpSS2.str() << "   ";
                hlpSS2.str( std::string ( ) );
                hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( -translationVec[1] * 1000.0 ) / 1000.0;
                if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
                if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
                hlpSS << hlpSS2.str() << "   ";
                hlpSS2.str( std::string ( ) );
                hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( -translationVec[2] * 1000.0 ) / 1000.0;
                if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
                if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
                hlpSS << hlpSS2.str() << "</pre>";
                
                rvapi_set_text                ( hlpSS.str().c_str(),
                                                "TranslationSection",
                                                0,
                                                1,
                                                1,
                                                1 );
                
                hlpSS.str( std::string () );
                hlpSS << "<pre>" << "Translation from lowest indices (index): ";
                hlpIt                         = static_cast<int> ( 70 - hlpSS.str().length() );
                for ( int iter = 0; iter < hlpIt; iter++ )
                {
                    hlpSS << ".";
                }
                
                hlpSS2.str( std::string ( ) );
                int pos                       = static_cast<int> ( ( -translationVec[0] / pattStr2->_xSamplingRate ) );
                hlpSS2 << std::showpos << pos;
                if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
                if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
                hlpSS << " " << hlpSS2.str() << "   ";
                hlpSS2.str( std::string ( ) );
                pos                           = static_cast<int> ( ( -translationVec[1] / pattStr2->_ySamplingRate ) );
                hlpSS2 << std::showpos << pos;
                if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
                if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
                hlpSS << hlpSS2.str() << "   ";
                hlpSS2.str( std::string ( ) );
                pos                           = static_cast<int> ( ( -translationVec[2] / pattStr2->_zSamplingRate ) );
                hlpSS2 << std::showpos << pos;
                if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
                if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
                hlpSS << hlpSS2.str() << "</pre>";
                
                rvapi_set_text                ( hlpSS.str().c_str(),
                                               "TranslationSection",
                                               1,
                                               1,
                                               1,
                                               1 );
                
                hlpSS.str ( std::string ( ) );
                hlpSS << "<pre>" << "Shift for visualisation (A): ";
                hlpIt                         = static_cast<int> ( 70 - hlpSS.str().length() );
                for ( int iter = 0; iter < hlpIt; iter++ )
                {
                    hlpSS << ".";
                }
                
                hlpSS2.str ( std::string ( ) );
                hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( (xMapTotMov * pattStr2->_xSamplingRate) * 1000.0 ) / 1000.0;
                if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
                if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
                hlpSS << " " << hlpSS2.str() << "   ";
                hlpSS2.str( std::string ( ) );
                hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( (yMapTotMov *  pattStr2->_ySamplingRate) * 1000.0 ) / 1000.0;
                if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
                if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
                hlpSS << hlpSS2.str() << "   ";
                hlpSS2.str( std::string ( ) );
                hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( (zMapTotMov * pattStr2->_zSamplingRate) * 1000.0 ) / 1000.0;
                if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
                if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
                hlpSS << hlpSS2.str() << "</pre>";
                
                rvapi_set_text                ( hlpSS.str().c_str(),
                                               "TranslationSection",
                                               2,
                                               1,
                                               1,
                                               1 );
                
                hlpSS.str( std::string () );
                hlpSS << "<pre>" << "Shift for visualisation (index): ";
                hlpIt                         = static_cast<int> ( 70 - hlpSS.str().length() );
                for ( int iter = 0; iter < hlpIt; iter++ )
                {
                    hlpSS << ".";
                }
                
                hlpSS2.str( std::string ( ) );
                pos                           = static_cast<int> ( xMapTotMov );
                hlpSS2 << std::showpos << pos;
                if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
                if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
                hlpSS << " " << hlpSS2.str() << "   ";
                hlpSS2.str( std::string ( ) );
                pos                           = static_cast<int> ( yMapTotMov );
                hlpSS2 << std::showpos << pos;
                if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
                if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
                hlpSS << hlpSS2.str() << "   ";
                hlpSS2.str( std::string ( ) );
                pos                           = static_cast<int> ( zMapTotMov );
                hlpSS2 << std::showpos << pos;
                if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
                if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
                hlpSS << hlpSS2.str() << "</pre>";
                
                rvapi_set_text                ( hlpSS.str().c_str(),
                                               "TranslationSection",
                                               3,
                                               1,
                                               1,
                                               1 );

                
                rvapi_flush                   ( );
            }
            
            if ( settings->verbose > 2 )
            {
                std::cout << ">>>>> Translation vector obtained from translation function ( " << translationVec[0] << ", " << translationVec[1] << " and " << translationVec[2] << " )." << std::endl;
            }
            
            //================================ Done
            break;
        }
        
        //==================================== Translate map
        pattStr2->translateMap                ( -translationVec[0],
                                                -translationVec[1],
                                                -translationVec[2] );
        
        if ( settings->verbose > 0 )
        {
            std::cout << "Resulting structure translated."  << std::endl;
        }
        
        //==================================== Write the matched map and pdb outputs
        if ( !pattStr2->_fromPDB )
        {
            if ( !wasMapNameGiven )
            {
                std::stringstream strStr;
                strStr << settings->clearMapFile << ".map";
                pattStr2->writeMap            ( strStr.str(), pattStr2->_densityMapCor );
            }
            else
            {
                if ( ( settings->clearMapFile.find ( ".map" ) != std::string::npos ) || ( settings->clearMapFile.find ( ".mrc" ) != std::string::npos ) )
                {
                    pattStr2->writeMap        ( settings->clearMapFile, pattStr2->_densityMapCor );
                }
                else
                {
                    if ( settings->clearMapFile.find ( ".pdb" ) != std::string::npos )
                    {
                        std::stringstream strStr;
                        strStr << settings->clearMapFile << "_errOut.map";
                        std::cerr << "!!! ProSHADE ERROR !!! Requested to output PDB file for MAP output. ProSHADE cannot convert MAP to PDB - outputting map into file " << strStr.str() << " ." << std::endl;
                        pattStr2->writeMap    ( strStr.str(), pattStr2->_densityMapCor );
                    }
                    else
                    {
                        std::stringstream strStr;
                        strStr << settings->clearMapFile << ".map";
                        pattStr2->writeMap    ( strStr.str(), pattStr2->_densityMapCor );
                    }
                }
            }
        }
        else
        {
            if ( !wasMapNameGiven )
            {
                std::stringstream strStr;
                strStr << settings->clearMapFile << ".map";
                pattStr2->writeMap            ( strStr.str(), pattStr2->_densityMapCor );
                strStr.str ( std::string ( ) );
                strStr.clear();
                strStr << settings->clearMapFile << ".pdb";
                pattStr2->writePDB            ( this->structFiles.at(1),
                                                strStr.str(),
                                                euAngs[0],
                                                euAngs[1],
                                                euAngs[2],
                                                translationVec[0] + xMapMov,
                                                translationVec[1] + yMapMov,
                                                translationVec[2] + zMapMov );
                std::cout << "!!! still here" << std::endl;
            }
            else
            {
                if ( ( settings->clearMapFile.find ( ".map" ) != std::string::npos ) || ( settings->clearMapFile.find ( ".mrc" ) != std::string::npos ) )
                {
                    pattStr2->writeMap        ( settings->clearMapFile, pattStr2->_densityMapCor );
                }
                else
                {
                    if ( settings->clearMapFile.find ( ".pdb" ) != std::string::npos )
                    {
                        pattStr2->writePDB    ( this->structFiles.at(1),
                                                settings->clearMapFile,
                                                euAngs[0],
                                                euAngs[1],
                                                euAngs[2],
                                                translationVec[0] + xMapMov,
                                                translationVec[1] + yMapMov,
                                                translationVec[2] + zMapMov );
                    }
                    else
                    {
                        std::stringstream strStr;
                        strStr << settings->clearMapFile << ".map";
                        pattStr2->writeMap    ( strStr.str(), pattStr2->_densityMapCor );
                        strStr.str ( std::string ( ) );
                        strStr.clear();
                        strStr << settings->clearMapFile << ".pdb";
                        pattStr2->writePDB    ( this->structFiles.at(1),
                                                strStr.str(),
                                                euAngs[0],
                                                euAngs[1],
                                                euAngs[2],
                                                translationVec[0] + xMapMov,
                                                translationVec[1] + yMapMov,
                                                translationVec[2] + zMapMov );
                    }
                }
            }
        }
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Overlay structure saved to " << settings->clearMapFile << " ." << "</font>";
            rvapi_set_text                ( hlpSS.str().c_str(),
                                           "ProgressSection",
                                           settings->htmlReportLineProgress,
                                           1,
                                           1,
                                           1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                   ( );
        }
        
        //==================================== Clear memory
        delete cmpObj;
        delete pattStr2;
        delete pattStr1;
        
        //==================================== End report
        if ( settings->verbose > 0 )
        {
            std::cout << std::endl << "-----------------------------------------------------------" << std::endl;
            std::cout << "|                         COMPLETED                       |" << std::endl;
            std::cout << "-----------------------------------------------------------" << std::endl << std::endl;
        }
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "COMPLETED." << "</font>";
            rvapi_set_text                ( hlpSS.str().c_str(),
                                           "ProgressSection",
                                           settings->htmlReportLineProgress,
                                           1,
                                           1,
                                           1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                   ( );
        }
        
        if ( settings->verbose > 0 )
        {
            std::cout << "Rotated structure written to file: " << settings->clearMapFile << " as required." << std::endl << std::endl;
        }
        
        if ( settings->htmlReport )
        {
            //==================================== Create section
            rvapi_add_section                 ( "ResultsSection",
                                                "Results",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                true );
            settings->htmlReportLine         += 1;
            
            std::stringstream hlpSS;
            hlpSS << "<pre><b>" << "Patterson maps optimal rotation (Euler angles): ";
            int hlpIt                         = static_cast<int> ( 70 - hlpSS.str().length() );
            for ( int iter = 0; iter < hlpIt; iter++ )
            {
                hlpSS << ".";
            }
            
            std::stringstream hlpSS2;
            double pos                        = euAngs[0];
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( pos * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
            hlpSS << " " << hlpSS2.str() << "   ";
            
            hlpSS2.str( std::string ( ) );
            pos                               = euAngs[1];
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( pos * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
            hlpSS << hlpSS2.str() << "   ";
            
            hlpSS2.str( std::string ( ) );
            pos                               = euAngs[2];
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( pos * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
            hlpSS << hlpSS2.str() << "</b></pre>";
            
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                0,
                                                0,
                                                1,
                                                1 );
            
            hlpSS.str ( std::string ( ) );
            hlpSS << "<pre><b>" << "Phased maps optimal translation (A): ";
            hlpIt                             = static_cast<int> ( 70 - hlpSS.str().length() );
            for ( int iter = 0; iter < hlpIt; iter++ )
            {
                hlpSS << ".";
            }
            
            hlpSS2.str( std::string ( ) );
            pos                               = translationVec[0];
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( pos * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
            hlpSS << " " << hlpSS2.str() << "   ";
            
            hlpSS2.str( std::string ( ) );
            pos                               = translationVec[1];
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( pos * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
            hlpSS << hlpSS2.str() << "   ";
            
            hlpSS2.str( std::string ( ) );
            pos                               = translationVec[2];
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( pos * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
            hlpSS << hlpSS2.str() << "</b></pre>";
            
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                1,
                                                0,
                                                1,
                                                1 );
            
            hlpSS.str ( std::string ( ) );
            hlpSS << "<pre><b>" << "Correlation between rotated Patterson maps: ";
            hlpIt                             = static_cast<int> ( 70 - hlpSS.str().length() );
            for ( int iter = 0; iter < hlpIt; iter++ )
            {
                hlpSS << ".";
            }
            
            hlpSS2.str( std::string ( ) );
            pos                               = pattCorrelation;
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( pos * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
            hlpSS << " " << hlpSS2.str() << "</b></pre>";
            
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                2,
                                                0,
                                                1,
                                                1 );
            
            hlpSS.str ( std::string ( ) );
            hlpSS << "<pre><b>" << "Correlation between translated maps with phases: ";
            hlpIt                             = static_cast<int> ( 70 - hlpSS.str().length() );
            for ( int iter = 0; iter < hlpIt; iter++ )
            {
                hlpSS << ".";
            }
            
            hlpSS2.str( std::string ( ) );
            pos                               = translationVec[3];
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( pos * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
            hlpSS << " " << hlpSS2.str() << "</b></pre>";
            
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ResultsSection",
                                               3,
                                               0,
                                               1,
                                               1 );
            
            rvapi_flush                       ( );
        }
        
        //==================================== Done
        return ;
    }
    
    //======================================== In the case of simple rotation, do it instead
    if ( settings->taskToPerform == ProSHADE::RotateMap )
    {
        //==================================== Sanity checks
        if ( settings->structFiles.size() != 1 )
        {
            std::cerr << "!!! ProSHADE ERROR !!! No files/too many files detected for map rotation mode. Please supply a single map file using the -f or -i command line options. Terminating..." << std::endl;
            exit ( -1 );
        }
        
        if ( checkFileType ( settings->structFiles.at(0) ) != 2 )
        {
            std::cerr << "!!! ProSHADE ERROR !!! The input file is corrupted or not a ccp4 MAP file formatted. Terminating..." << std::endl;
            exit ( -1 );
        }
        
        if ( settings->clearMapFile == "" )
        {
            std::cout << "!!! ProSHADE WARNING !!! The output file name was not set. You may want to use the \'--clearMap\' option to set it. Using the default name \'rotMap.map\'." << std::endl;
            settings->clearMapFile              = "rotMap.map";
        }
        
        if ( ( settings->rotAngle == 0.0 ) && ( settings->rotXAxis == 0.0 ) && ( settings->rotYAxis == 0.0 ) && ( settings->rotZAxis == 0.0 ) )
        {
            std::cout << "!!! ProSHADE WARNING !!! There is no rotation to be done, but will progress as if there were. It would be faster just to copy the input..." << std::endl;
        }
        
        if ( settings->verbose > 2 )
        {
            std::cout << ">>>>> Sanity checks passed." << std::endl;
        }
        
        //==================================== Load the structure
        ProSHADE_data* rotStr                 = new ProSHADE_data ();
        rotStr->getDensityMapFromMAP          ( this->structFiles.at(0),
                                               &this->shellSpacing,
                                                this->mapResolution,
                                               &this->bandwidth.at(0),
                                               &this->theta.at(0),
                                               &this->phi.at(0),
                                               &this->glIntegOrder.at(0),
                                               &this->extraSpace,
                                                settings->mapResDefault,
                                                settings->rotChangeDefault,
                                                settings,
                                                settings->overlayDefaults );
        if ( settings->verbose > 0 )
        {
            std::cout << "Structure loaded." << std::endl;
        }
        
        rotStr->translateMap                  ( settings->xTranslation,
                                                settings->yTranslation,
                                                settings->zTranslation );
        
        //==================================== Decompose structure to SH
        rotStr->keepPhaseInMap                ( this->alpha,
                                                this->bFactorChange,
                                               &this->bandwidth.at(0),
                                               &this->theta.at(0),
                                               &this->phi.at(0),
                                               &this->glIntegOrder.at(0),
                                                settings,
                                                this->useCOM,
                                                settings->noIQRsFromMap,
                                                settings->verbose,
                                                settings->clearMapData,
                                                settings->rotChangeDefault,
                                                settings->overlayDefaults,
                                                settings->maskBlurFactor,
                                                settings->maskBlurFactorGiven );
        
        ProSHADE_comparePairwise* cmpObj      = nullptr;
        if ( settings->rotAngle != 0.0 )
        {
            rotStr->mapPhaselessToSphere      ( settings,
                                                this->theta.at(0),
                                                this->phi.at(0),
                                                this->shellSpacing,
                                                settings->manualShells,
                                                true,
                                                settings->rotChangeDefault );
            rotStr->getSphericalHarmonicsCoeffs ( this->bandwidth.at(0), settings );
            if ( settings->verbose > 1 )
            {
                std::cout << ">> Structure spherical harmonics computed." << std::endl;
            }
            
            //================================ Get comparison object
            cmpObj                            = new ProSHADE_comparePairwise ( rotStr,
                                                                               rotStr,
                                                                               this->mPower,
                                                                               this->ignoreLs,
                                                                               this->glIntegOrder.at(0),
                                                                               settings );
            
            //================================ Convert Axis-angle to Euler
            double euAlpha                    = 0.0;
            double euBeta                     = 0.0;
            double euGamma                    = 0.0;
            
            ProSHADE_internal_misc::getEulerFromAxisAngle ( &euAlpha,
                                                            &euBeta,
                                                            &euGamma,
                                                             settings->rotXAxis,
                                                             settings->rotYAxis,
                                                             settings->rotZAxis,
                                                             settings->rotAngle );
            
            cmpObj->setEulerAngles            ( euAlpha, euBeta, euGamma );
            
            if ( settings->verbose > 2 )
            {
                printf ( ">>>>> Preparation for rotation complete. Euler angles are %+.3f ; %+.3f ; %+.3f\n", euAlpha, euBeta, euGamma );
            }
        }
        else
        {
            //================================ Get comparison object
            cmpObj                            = new ProSHADE_comparePairwise ( rotStr,
                                                                               rotStr,
                                                                               this->mPower,
                                                                               this->ignoreLs,
                                                                               this->glIntegOrder.at(0),
                                                                               settings );
            
            cmpObj->setEulerAngles            ( 0.0, 0.0, 0.0 );
            
            if ( settings->verbose > 2 )
            {
                printf ( ">>>>> Preparation for rotation complete. Euler angles are 0.000 ; 0.000 ; 0.000\n" );
            }
        }
        
        //==================================== Rotate by the Euler
        cmpObj->rotateStructure               ( rotStr,
                                                settings,
                                                settings->clearMapFile,
                                                settings->verbose,
                                                settings->axisOrder,
                                                false );
        
        //==================================== Clear memory
        delete cmpObj;
        delete rotStr;
        
        if ( settings->verbose > 0 )
        {
            std::cout << std::endl << "-----------------------------------------------------------" << std::endl;
            std::cout << "|                         COMPLETED                       |" << std::endl;
            std::cout << "-----------------------------------------------------------" << std::endl << std::endl;
        }
        
        if ( settings->verbose > 0 )
        {
            std::cout << "Rotated structure written to file: " << settings->clearMapFile << " as required." << std::endl << std::endl; 
        }
        
        //==================================== Done
        return ;
    }
    
    //======================================== Report progress
    if ( settings->verbose > 0 )
    {
        std::cout << "-----------------------------------------------------------" << std::endl;
        std::cout << "|                        MODE: Symmetry                   |" << std::endl;
        std::cout << "-----------------------------------------------------------" << std::endl << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        //================================ Report title
        rvapi_set_text                        ( "<h1>ProSHADE Results: Symmetry</h1>",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1 );
        settings->htmlReportLine             += 1;
        
        //==================================== Create section
        rvapi_add_section                     ( "ProgressSection",
                                                "Progress",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                true );
        settings->htmlReportLine             += 1;
        
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Starting computation of symmetry for the input structure." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        
        rvapi_flush                           ( );
    }
    
    //======================================== Sanity checks
    if ( settings->structFiles.size() != 1 )
    {
        std::cerr << "!!! ProSHADE ERROR !!! No files/too many files detected for map symmetry mode. Please supply a single map file using the -f or -i command line options. Terminating..." << std::endl;
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\"><b>" << "Incorrect number of input files detected. The symmetry mode can process only a single structure at a time." << "<b></font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }
        exit ( -1 );
    }
    
    //======================================== Create the structure object
    unsigned int fileType                     = checkFileType ( structFiles.at(0) );
    ProSHADE_data* symStr                     = new ProSHADE_data ();
    if ( fileType == 2 )
    {
        symStr->getDensityMapFromMAP          ( this->structFiles.at(0),
                                               &this->shellSpacing,
                                                this->mapResolution,
                                               &this->bandwidth.at(0),
                                               &this->theta.at(0),
                                               &this->phi.at(0),
                                               &this->glIntegOrder.at(0),
                                               &this->extraSpace,
                                                settings->mapResDefault,
                                                settings->rotChangeDefault,
                                                settings,
                                                settings->overlayDefaults );
    }
    else if ( fileType == 1 )
    {
        symStr->getDensityMapFromPDB          ( this->structFiles.at(0),
                                               &this->shellSpacing,
                                                this->mapResolution,
                                               &this->bandwidth.at(0),
                                               &this->theta.at(0),
                                               &this->phi.at(0),
                                               &this->glIntegOrder.at(0),
                                               &this->extraSpace,
                                                settings->mapResDefault,
                                                settings,
                                                this->bFactorValue,
                                                this->firstLineCOM );
    }
    else
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error loading file " << this->structFiles.at(0) << " !!! Cannot detect the extension (currently, only PDB or MAP are allowed) and therefore cannot read the file. Terminating ..." << std::endl;
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\"><b>" << "Error loading file " << this->structFiles.at(0) << " !!! Cannot detect the extension (currently, only PDB or MAP are allowed) and therefore cannot read the file." << "<b></font>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
            settings->htmlReportLineProgress     += 1;
            rvapi_flush                           ( );
        }
        exit ( -1 );
    }
    if ( settings->verbose > 0 )
    {
        std::cout << "Structure loaded." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Structure " << settings->structFiles.at(0) << " loaded." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Deal with position of the centre of rotation
    this->xPos                                = symStr->_xFrom + ((symStr->_maxMapU+1)/2);
    this->yPos                                = symStr->_yFrom + ((symStr->_maxMapV+1)/2);
    this->zPos                                = symStr->_zFrom + ((symStr->_maxMapW+1)/2);
    
    this->xPos                               *= symStr->_xSamplingRate;
    this->yPos                               *= symStr->_ySamplingRate;
    this->zPos                               *= symStr->_zSamplingRate;
    
    //======================================== Write file if so required
    if ( settings->clearMapFile != "" )
    {
        if ( settings->verbose > 1 )
        {
            std::cout << ">> Saving the structure into the requested location ( " << settings->clearMapFile << " )." << std::endl;
        }
        
        symStr->writeMap                      ( settings->clearMapFile, symStr->_densityMapMap );
    }
    
    symStr->keepPhaseInMap                    ( this->alpha,
                                                this->bFactorChange,
                                               &this->bandwidth.at(0),
                                               &this->theta.at(0),
                                               &this->phi.at(0),
                                               &this->glIntegOrder.at(0),
                                                settings,
                                                this->useCOM,
                                                settings->noIQRsFromMap,
                                                settings->verbose,
                                                settings->clearMapData,
                                                settings->rotChangeDefault,
                                                settings->overlayDefaults,
                                                settings->maskBlurFactor,
                                                settings->maskBlurFactorGiven );
    
    symStr->mapPhaselessToSphere              ( settings,
                                                this->theta.at(0),
                                                this->phi.at(0),
                                                this->shellSpacing,
                                                settings->manualShells, true, false );
    symStr->getSphericalHarmonicsCoeffs       ( this->bandwidth.at(0), settings );
    if ( settings->verbose > 1 )
    {
        std::cout << ">> Structure spherical harmonics computed." << std::endl;
    }
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Spherical harmonics decomposition computed." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Find the rotation function peaks
    ProSHADE_comparePairwise* cmpObj          = new ProSHADE_comparePairwise ( symStr,
                                                                               symStr,
                                                                               this->mPower,
                                                                               this->ignoreLs,
                                                                               this->glIntegOrder.at(0),
                                                                               settings );
    
    cmpObj->precomputeTrSigmaDescriptor       ( );
    if ( settings->verbose > 2 )
    {
        std::cout << ">>>>> E matrices constructed." << std::endl;
    }
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Shell integration complete." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    cmpObj->getSO3InverseMap                  ( settings );
    if ( settings->verbose > 2 )
    {
        std::cout << ">>>>> Inverse SO(3) Fourier transform map obtained." << std::endl;
    }
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "SO(3) Fourier Transform computed." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    this->rfPeaks                             = cmpObj->getSO3Peaks ( settings,
                                                                      this->peakHeightNoIQRs,
                                                                      false,
                                                                      this->peakSurroundingPoints,
                                                                      this->peakDistanceForReal,
                                                                      settings->verbose );
    if ( settings->verbose > 1 )
    {
        std::cout << ">> Peaks obtained." << std::endl;
    }
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Rotation map peaks detection complete." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Find C symmetries
    this->cnSymm                              = cmpObj->findCnSymmetry ( this->rfPeaks,
                                                                         settings,
                                                                         this->aaErrorTolerance,
                                                                         false,
                                                                         0.33,
                                                                         settings->verbose );

    if ( settings->verbose > 0 )
    {
        std::cout << "C symmetries detected." << std::endl;
    }
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Cyclic symmetries detected." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    if ( settings->symmetryFold != 0 )
    {
        bool foundFold                        = false;
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->cnSymm.size() ); iter++ )
        {
            if ( this->cnSymm.at(iter)[0] == settings->symmetryFold ) { foundFold = true; }
        }
        
        if ( !foundFold )
        {
            if ( settings->verbose > 2 )
            {
                std::cout << ">>>>> The requested fold was not found. Searching for it specifically." << std::endl;
            }
            
            double mapPeakHeight              = 0.0;
            double mapPeakMax                 = 0.0;
            unsigned int iterMax              = 0;
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->cnSymm.size() ); iter++ )
            {
                mapPeakHeight                 = cmpObj->maxAvgPeakForSymmetry ( this->cnSymm.at(iter)[1],
                                                                                this->cnSymm.at(iter)[2],
                                                                                this->cnSymm.at(iter)[3],
                                                                                static_cast<double> ( settings->symmetryFold ),
                                                                                settings );
                if ( mapPeakMax < mapPeakHeight ) { mapPeakMax = mapPeakHeight; iterMax = iter; }
            }

            if ( this->cnSymm.size() > 0 )
            {
                if ( mapPeakMax > ( this->cnSymm.at(iterMax)[4] * ( 1.0 - settings->symGapTolerance ) ) )
                {
                    std::array<double,5> hlpArr;
                    hlpArr[0]                 = static_cast<double> ( settings->symmetryFold );
                    hlpArr[1]                 = this->cnSymm.at(iterMax)[1];
                    hlpArr[2]                 = this->cnSymm.at(iterMax)[2];
                    hlpArr[3]                 = this->cnSymm.at(iterMax)[3];
                    hlpArr[4]                 = mapPeakMax;
                    this->cnSymm.emplace_back ( hlpArr );
                }
            }
        }
    }
    
    //======================================== Find D Symmetries
    this->dnSymm                              = cmpObj->findDnSymmetry ( this->cnSymm , this->aaErrorTolerance );
    if ( this->cnSymm.size() != 0 )
    {
        this->cnSymmClear                     = cmpObj->findCnSymmetryClear ( this->cnSymm, settings, this->symGapTolerance, &this->printFull );
    }
    if ( this->dnSymm.size() != 0 )
    {
        this->dnSymmClear                     = cmpObj->findDnSymmetryClear ( this->dnSymm, settings, this->symGapTolerance, &this->printFull );
    }
    if ( settings->verbose > 0 )
    {
        std::cout << "D symmetries detected." << std::endl;
    }
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Dihedral symmetries detected." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Find polyhedral symmetries
    this->icosSymm                            = cmpObj->findIcosSymmetry ( this->cnSymm, &this->icosSymmPeakAvg, this->aaErrorTolerance );
    this->octaSymm                            = cmpObj->findOctaSymmetry ( this->cnSymm, &this->octaSymmPeakAvg, this->aaErrorTolerance );
    this->tetrSymm                            = cmpObj->findTetrSymmetry ( this->cnSymm, &this->tetrSymmPeakAvg, this->aaErrorTolerance );
    if ( settings->verbose > 0 )
    {
        std::cout << "T, O and I symmetries detected." << std::endl;
    }
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Tetrahedral, Octahedral and Icosahedral symmetries detected." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    if ( this->icosSymm.size() > 0 )
    {
        for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->icosSymm.size() ); icoIt++ )
        {
            this->icosAxes                    = this->generateIcosAxes ( cmpObj, settings, this->icosSymm.at(icoIt), this->cnSymm, this->aaErrorTolerance, settings->verbose );
            
            if ( this->icosAxes.size() == 31 )
            {
                break;
            }
        }
        if ( this->icosAxes.size() != 31 )
        {
            std::cout << "!!! ProSHADE WARNING !!! It looks like icosahedral symmetry, but cannot find all the elements. Please report this case. Sorry for the inconvenience." << std::endl;
        }
        else
        {
            this->icosElems                   = this->generateIcosElements ( this->icosAxes, settings, settings->verbose );
        }
    }
    
    if ( ( ( this->octaSymm.size() > 0 ) && ( this->icosAxes.size() == 0 ) )  ||
         ( ( settings->symmetryType == "O" ) && ( this->octaSymm.size() > 0 ) ) )
    {
        for ( unsigned int octIt = 0; octIt < static_cast<unsigned int> ( this->octaSymm.size() ); octIt++ )
        {
            this->octaAxes                    = this->generateOctaAxes ( cmpObj, settings, this->octaSymm.at(octIt), this->cnSymm, this->aaErrorTolerance, settings->verbose );
            
            if ( this->octaAxes.size() == 13 )
            {
                break;
            }
        }
        if ( this->octaAxes.size() != 13 )
        {
            std::cout << "!!! ProSHADE WARNING !!! It looks like (cub)octahedral symmetry, but cannot find all the elements. Please report this case. Sorry for the inconvenience." << std::endl;
        }
        else
        {
            this->octaElems                   = this->generateOctaElements ( this->octaAxes, settings, settings->verbose );
        }
    }
    
    if ( ( ( this->tetrSymm.size() > 0 ) && ( this->octaAxes.size() == 0 ) && ( this->icosAxes.size() == 0 ) ) ||
         ( ( settings->symmetryType == "T" ) && ( this->tetrSymm.size() > 0 ) ) )
    {
        for ( unsigned int tetIt = 0; tetIt < static_cast<unsigned int> ( this->tetrSymm.size() ); tetIt++ )
        {
            this->tetrAxes                    = this->generateTetrAxes ( cmpObj, settings, this->tetrSymm.at(tetIt), this->cnSymm, this->aaErrorTolerance, settings->verbose );
            
            if ( this->tetrAxes.size() == 6 )
            {
                break;
            }
        }
        if ( this->tetrAxes.size() != 6 )
        {
            std::cout << "!!! ProSHADE WARNING !!! It looks like tetrahedral symmetry, but cannot find all the elements. Please report this case. Sorry for the inconvenience." << std::endl;
        }
        else
        {
            this->tetrElems                   = this->generateTetrElements ( this->tetrAxes, settings, settings->verbose );
        }
    }
    
    if ( settings->verbose > 0 )
    {
        std::cout << ">> Generation of T, O and I symmetry group elements complete." << std::endl;
    }
    
    if ( settings->verbose > 0 )
    {
        std::cout << std::endl << "-----------------------------------------------------------" << std::endl;
        std::cout << "|                         COMPLETED                       |" << std::endl;
        std::cout << "-----------------------------------------------------------" << std::endl << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Symmetry detection completed." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Keep data type in memory
    this->inputStructureDataType              = symStr->_fromPDB;
    
    //======================================== Free memory
    cmpObj->freeInvMap ( );
    delete cmpObj;
    delete symStr;
    
    //======================================== Done
    return ;
    
}

/*! \brief This function prints the cleared results to the screen.
 
 This version of the function prints the results to the screen with regard to the user specified expected symmetry. It not only
 searches for the requested symmetry in the already detected symmetries, but it also does extra searches if the requested symmetry
 cannot be found in the already detected ones.
 
 \param[in] symmetryType String with the type of symmetry that is being sought after. Allowed values are C, D, T, O and I.
 \param[in] symmetryFold The fold of the symmetry being sought after. Only used for C and D symmetry types.
 \param[in] verbose Int value determining how verbal the function should be. It has currently no effect, though...
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_symmetry::printResultsRequest ( std::string symmetryType, unsigned int symmetryFold, int verbose )
{
    //======================================== Claim the symmetry to be the highest detected
    if ( verbose > 0 )
    {
        printf ( "-----------------------------------------------------------\n" );
        printf ( "|                         RESULTS                         |\n" );
        printf ( "-----------------------------------------------------------\n\n" );
    }
    
    if ( symmetryType == "I" )
    {
        if ( static_cast<unsigned int> ( this->icosSymm.size() ) > 0 )
        {
            printf ( "Detected Icosahedral symmetry as requested\n\n" );
            printf ( "Symmetry axes table:\n" );
            printf ( "-----------------------------------------------------------\n" );
            printf ( "Symmetry Fold     x       y       z      Angle      Peak\n" );
            printf ( "  Type                                             height\n" );
            printf ( "-----------------------------------------------------------\n" );
            
            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->icosAxes.size() ); icoIt++ )
            {
                printf ( "   C      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->icosAxes.at(icoIt)[0] ), this->icosAxes.at(icoIt)[1], this->icosAxes.at(icoIt)[2], this->icosAxes.at(icoIt)[3], static_cast<int> ( this->icosAxes.at(icoIt)[0] ), this->icosAxes.at(icoIt)[4] );
            }
            printf ( "\n" );
            
            printf ( "Symmetry elements table:\n" );
            printf ( "-----------------------------------------------------------\n" );
            printf ( "Symmetry          x          y          z          Angle        \n" );
            printf ( "  Type                                             (deg)        \n" );
            printf ( "-----------------------------------------------------------\n" );
            
            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->icosElems.size() ); icoIt++ )
            {
                if ( this->icosElems.at(icoIt)[0] != 1.0 )
                {
                    printf ( "   C%d           %+.2f      %+.2f      %+.2f        %+.1f    \n", static_cast<int> ( this->icosElems.at(icoIt)[0] ), this->icosElems.at(icoIt)[1], this->icosElems.at(icoIt)[2], this->icosElems.at(icoIt)[3], this->icosElems.at(icoIt)[4] );
                }
                else
                {
                    printf ( "   E            %+.2f      %+.2f      %+.2f        %+.1f    \n", this->icosElems.at(icoIt)[1], this->icosElems.at(icoIt)[2], this->icosElems.at(icoIt)[3], this->icosElems.at(icoIt)[4] );
                }
            }
            printf ( "\n" );
        }
        else
        {
            std::cout << "!!! ProSHADE WARNING !!! Could not detect the requested symmetry. You can try changing the resolution or searching for different symmetry from the alternatives list printed below." << std::endl << std::endl;
        }
    }
    
    if ( symmetryType == "O" )
    {
        if ( static_cast<unsigned int> ( this->octaSymm.size() ) > 0 )
        {
            printf ( "Detected (Cub)octahedral symmetry as requested\n\n" );
            printf ( "Symmetry axes table:\n" );
            printf ( "-----------------------------------------------------------\n" );
            printf ( "Symmetry Fold     x       y       z      Angle      Peak\n" );
            printf ( "  Type                                             height\n" );
            printf ( "-----------------------------------------------------------\n" );
            
            for ( unsigned int octIt = 0; octIt < static_cast<unsigned int> ( this->octaAxes.size() ); octIt++ )
            {
                printf ( "   C      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->octaAxes.at(octIt)[0] ), this->octaAxes.at(octIt)[1], this->octaAxes.at(octIt)[2], this->octaAxes.at(octIt)[3], static_cast<int> ( this->octaAxes.at(octIt)[0] ), this->octaAxes.at(octIt)[4] );
            }
            printf ( "\n" );
            
            printf ( "Symmetry elements table:\n" );
            printf ( "-----------------------------------------------------------\n" );
            printf ( "Symmetry          x          y          z          Angle        \n" );
            printf ( "  Type                                             (deg)        \n" );
            printf ( "-----------------------------------------------------------\n" );
            
            for ( unsigned int octIt = 0; octIt < static_cast<unsigned int> ( this->octaElems.size() ); octIt++ )
            {
                if ( this->octaElems.at(octIt)[0] != 1.0 )
                {
                    printf ( "   C%d           %+.2f      %+.2f      %+.2f        %+.1f    \n", static_cast<int> ( this->octaElems.at(octIt)[0] ), this->octaElems.at(octIt)[1], this->octaElems.at(octIt)[2], this->octaElems.at(octIt)[3], this->octaElems.at(octIt)[4] );
                }
                else
                {
                    printf ( "   E            %+.2f      %+.2f      %+.2f        %+.1f    \n", this->octaElems.at(octIt)[1], this->octaElems.at(octIt)[2], this->octaElems.at(octIt)[3], this->octaElems.at(octIt)[4] );
                }
            }
            printf ( "\n" );
        }
        else
        {
            std::cout << "!!! ProSHADE WARNING !!! Could not detect the requested symmetry. You can try changing the resolution or searching for different symmetry from the alternatives list printed below.\n\n" << std::endl << std::endl;
        }
    }
    
    if ( symmetryType == "T" )
    {
        if ( static_cast<unsigned int> ( this->tetrSymm.size() ) > 0 )
        {
            printf ( "Detected Tetrahedral symmetry as requested\n\n" );
            printf ( "Symmetry axes table:\n" );
            printf ( "-----------------------------------------------------------\n" );
            printf ( "Symmetry Fold     x       y       z      Angle      Peak\n" );
            printf ( "  Type                                             height\n" );
            printf ( "-----------------------------------------------------------\n" );
            
            for ( unsigned int tetIt = 0; tetIt < static_cast<unsigned int> ( this->tetrAxes.size() ); tetIt++ )
            {
                printf ( "   C      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->tetrAxes.at(tetIt)[0] ), this->tetrAxes.at(tetIt)[1], this->tetrAxes.at(tetIt)[2], this->tetrAxes.at(tetIt)[3], static_cast<int> ( this->tetrAxes.at(tetIt)[0] ), this->tetrAxes.at(tetIt)[4] );
            }
            printf ( "\n" );
            
            printf ( "Symmetry elements table:\n" );
            printf ( "-----------------------------------------------------------\n" );
            printf ( "Symmetry          x          y          z          Angle        \n" );
            printf ( "  Type                                             (deg)        \n" );
            printf ( "-----------------------------------------------------------\n" );
            
            for ( unsigned int tetIt = 0; tetIt < static_cast<unsigned int> ( this->tetrElems.size() ); tetIt++ )
            {
                if ( this->tetrElems.at(tetIt)[0] != 1.0 )
                {
                    printf ( "   C%d           %+.2f      %+.2f      %+.2f        %+.1f    \n", static_cast<int> ( this->tetrElems.at(tetIt)[0] ), this->tetrElems.at(tetIt)[1], this->tetrElems.at(tetIt)[2], this->tetrElems.at(tetIt)[3], this->tetrElems.at(tetIt)[4] );
                }
                else
                {
                    printf ( "   E            %+.2f      %+.2f      %+.2f        %+.1f    \n", this->tetrElems.at(tetIt)[1], this->tetrElems.at(tetIt)[2], this->tetrElems.at(tetIt)[3], this->tetrElems.at(tetIt)[4] );
                }
            }
            printf ( "\n" );
        }
        else
        {
            std::cout << "!!! ProSHADE WARNING !!! Could not detect the requested symmetry. You can try changing the resolution or searching for different symmetry from the alternatives list printed below.\n\n" << std::endl << std::endl;
        }
    }
    
    if ( symmetryType == "D" )
    {
        if ( static_cast<unsigned int> ( this->dnSymm.size() ) > 0 )
        {
            bool reqFound = false;
            for ( unsigned int dIt = 0; dIt < static_cast<unsigned int> ( this->dnSymm.size() ); dIt++ )
            {
                if ( reqFound ) { break; }
                unsigned int howManyTwos = 0;
                for ( unsigned int dItt = 0; dItt < static_cast<unsigned int> ( this->dnSymm.at(dIt).size() ); dItt++ )
                {
                    if ( reqFound ) { break; }
                    if ( symmetryFold != 2 )
                    {
                        if ( this->dnSymm.at(dIt).at(dItt)[0] == symmetryFold )
                        {
                            printf ( "Detected Dihedral symmetry as requested\n\n" );
                            printf ( "Symmetry axes table:\n" );
                            printf ( "-----------------------------------------------------------\n" );
                            printf ( "Symmetry Fold     x       y       z      Angle      Peak\n" );
                            printf ( "  Type                                             height\n" );
                            printf ( "-----------------------------------------------------------\n" );
                            for ( unsigned int it = 0; it < static_cast<unsigned int> ( this->dnSymm.at(dIt).size() ); it++ )
                            {
                                printf ( "   C      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ), this->dnSymm.at(dIt).at(it)[1], this->dnSymm.at(dIt).at(it)[2], this->dnSymm.at(dIt).at(it)[3], static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ), this->dnSymm.at(dIt).at(it)[4] );
                            }
                            
                            printf ( "\n" );
                            
                            printf ( "Symmetry elements table:\n" );
                            printf ( "-----------------------------------------------------------\n" );
                            printf ( "Symmetry          x          y          z          Angle        \n" );
                            printf ( "  Type                                             (deg)        \n" );
                            printf ( "-----------------------------------------------------------\n" );
                            
                            printf ( "   E            %+.2f      %+.2f      %+.2f        %+.1f    \n", 1.0, 0.0, 0.0, 0.0 );
                            for ( unsigned int it = 0; it < 2; it++ )
                            {
                                if ( static_cast<int> ( this->dnSymm.at(0).at(it)[0] ) % 2 == 0 )
                                {
                                    for ( int iter = -std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter++ )
                                    {
                                        if ( iter == 0 ) { continue; }
                                        if ( iter == -std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ) ) { continue; }
                                        printf ( "   C%d           %+.2f      %+.2f      %+.2f        %+.1f    \n", static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ), this->dnSymm.at(dIt).at(it)[1], this->dnSymm.at(dIt).at(it)[2], this->dnSymm.at(dIt).at(it)[3], iter * ( 360.0 / static_cast<double> ( this->dnSymm.at(dIt).at(it)[0] ) ) );
                                    }
                                }
                                else
                                {
                                    for ( int iter = -std::floor( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter++ )
                                    {
                                        if ( iter == 0 ) { continue; }
                                        printf ( "   C%d           %+.2f      %+.2f      %+.2f        %+.1f    \n", static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ), this->dnSymm.at(dIt).at(it)[1], this->dnSymm.at(dIt).at(it)[2], this->dnSymm.at(dIt).at(it)[3], iter * ( 360.0 / static_cast<double> ( this->dnSymm.at(dIt).at(it)[0] ) ) );
                                    }
                                }
                            }
                            
                            printf ( "\n" );
                            reqFound = true;
                            break;
                        }
                        else
                        {
                            if ( this->dnSymm.at(dIt).at(dItt)[0] == 2 )
                            {
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    if ( symmetryFold == 2 )
                    {
                        if ( this->dnSymm.at(dIt).at(dItt)[0] == 2 )
                        {
                            howManyTwos += 1;
                        }
                        if ( this->dnSymm.at(dIt).at(dItt)[0] == symmetryFold && howManyTwos == 2 )
                        {
                            printf ( "Detected Dihedral symmetry as requested\n\n" );
                            printf ( "Symmetry axes table:\n" );
                            printf ( "-----------------------------------------------------------\n" );
                            printf ( "Symmetry Fold     x       y       z      Angle      Peak\n" );
                            printf ( "  Type                                             height\n" );
                            printf ( "-----------------------------------------------------------\n" );
                            for ( unsigned int it = 0; it < static_cast<unsigned int> ( this->dnSymm.at(dIt).size() ); it++ )
                            {
                                if ( this->dnSymm.at(dIt).at(it)[0] == 2.0 )
                                {
                                    printf ( "   C      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ), this->dnSymm.at(dIt).at(it)[1], this->dnSymm.at(dIt).at(it)[2], this->dnSymm.at(dIt).at(it)[3], static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ), this->dnSymm.at(dIt).at(it)[4] );
                                }
                            }
                            
                            printf ( "\n" );
                            
                            printf ( "Symmetry elements table:\n" );
                            printf ( "-----------------------------------------------------------\n" );
                            printf ( "Symmetry          x          y          z          Angle        \n" );
                            printf ( "  Type                                             (deg)        \n" );
                            printf ( "-----------------------------------------------------------\n" );
                            
                            printf ( "   E            %+.2f      %+.2f      %+.2f        %+.1f    \n", 1.0, 0.0, 0.0, 0.0 );
                            for ( unsigned int it = 0; it < 2; it++ )
                            {
                                if ( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) % 2 == 0 )
                                {
                                    for ( int iter = -std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter++ )
                                    {
                                        if ( iter == 0 ) { continue; }
                                        if ( iter == -std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ) ) { continue; }
                                        printf ( "   C%d           %+.2f      %+.2f      %+.2f        %+.1f    \n", static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ), this->dnSymm.at(dIt).at(it)[1], this->dnSymm.at(dIt).at(it)[2], this->dnSymm.at(dIt).at(it)[3], iter * ( 360.0 / static_cast<double> ( this->dnSymm.at(dIt).at(it)[0] ) ) );
                                    }
                                }
                                else
                                {
                                    for ( int iter = -std::floor( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter++ )
                                    {
                                        if ( iter == 0 ) { continue; }
                                        printf ( "   C%d           %+.2f      %+.2f      %+.2f        %+.1f    \n", static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ), this->dnSymm.at(dIt).at(it)[1], this->dnSymm.at(dIt).at(it)[2], this->dnSymm.at(dIt).at(it)[3], iter * ( 360.0 / static_cast<double> ( this->dnSymm.at(dIt).at(it)[0] ) ) );
                                    }
                                }
                            }
                            
                            printf ( "\n" );
                            reqFound = true;
                            break;
                        }
                    }
                }
            }
            if ( !reqFound )
            {
                std::cout << "!!! ProSHADE WARNING !!! Could not detect the requested dihedral symmetry, but detected dihedral symmetries with different fold. You can try changing the resolution or selecting one of the alternative symmetries printed below.\n\n" << std::endl << std::endl;
            }
        }
        else
        {
            std::cerr << "!!! Warning !!! Could not detect the requested dihedral symmetry. You can try changing the resolution or searching for different symmetry from the alternatives list printed below.\n\n" << std::endl << std::endl;
        }
    }
    
    if ( symmetryType == "C" )
    {
        if ( static_cast<unsigned int> ( this->cnSymm.size() ) > 0 )
        {
            bool reqFound = false;
            for ( unsigned int dIt = 0; dIt < static_cast<unsigned int> ( this->cnSymm.size() ); dIt++ )
            {
                if ( this->cnSymm.at(dIt)[0] == symmetryFold )
                {
                    printf ( "Detected Cyclic symmetry as requested\n\n" );
                    printf ( "Symmetry axes table:\n" );
                    printf ( "-----------------------------------------------------------\n" );
                    printf ( "Symmetry Fold     x       y       z      Angle      Peak\n" );
                    printf ( "  Type                                             height\n" );
                    printf ( "-----------------------------------------------------------\n" );
                    printf ( "   C      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n\n", static_cast<int> ( this->cnSymm.at(dIt)[0] ), this->cnSymm.at(dIt)[1], this->cnSymm.at(dIt)[2], this->cnSymm.at(dIt)[3], static_cast<int> ( this->cnSymm.at(dIt)[0] ), static_cast<double> ( this->cnSymm.at(dIt)[4] ) );
                    
                    printf ( "\n" );
                    
                    printf ( "Symmetry elements table:\n" );
                    printf ( "-----------------------------------------------------------\n" );
                    printf ( "Symmetry          x          y          z          Angle        \n" );
                    printf ( "  Type                                             (deg)        \n" );
                    printf ( "-----------------------------------------------------------\n" );
                    
                    printf ( "   E            %+.2f      %+.2f      %+.2f        %+.1f    \n", 1.0, 0.0, 0.0, 0.0 );
                    if ( static_cast<int> ( this->cnSymm.at(dIt)[0] ) % 2 == 0 )
                    {
                        for ( int iter = -std::ceil( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ); iter++ )
                        {
                            if ( iter == 0 ) { continue; }
                            if ( iter == -std::ceil( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ) ) { continue; }
                            printf ( "   C%d           %+.2f      %+.2f      %+.2f        %+.1f    \n", static_cast<int> ( this->cnSymm.at(dIt)[0] ), this->cnSymm.at(dIt)[1], this->cnSymm.at(dIt)[2], this->cnSymm.at(dIt)[3], iter * ( 360.0 / static_cast<double> ( this->cnSymm.at(dIt)[0] ) ) );
                        }
                    }
                    else
                    {
                        for ( int iter = -std::floor( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ); iter++ )
                        {
                            if ( iter == 0 ) { continue; }
                            printf ( "   C%d           %+.2f      %+.2f      %+.2f        %+.1f    \n", static_cast<int> ( this->cnSymm.at(dIt)[0] ), this->cnSymm.at(dIt)[1], this->cnSymm.at(dIt)[2], this->cnSymm.at(dIt)[3], iter * ( 360.0 / static_cast<double> ( this->cnSymm.at(dIt)[0] ) ) );
                        }
                    }
                    printf ( "\n" );
                    reqFound = true;
                    break;
                }
            }
            if ( !reqFound )
            {
                std::cout << "!!! ProSHADE WARNING !!! Could not detect the requested cyclic symmetry, but detected cyclic symmetries with different folds. You can try changing the resolution or selecting one of the alternative symmetries printed below.\n\n" << std::endl << std::endl;
            }
        }
        else
        {
            std::cerr << "!!! Warning !!! Could not detect the requested cyclic symmetry. You can try changing the resolution or searching for different symmetry from the alternatives list printed below.\n\n" << std::endl << std::endl;
        }
    }
    
    //======================================== Print alternativs
    printf ( "Alternatives:\n" );
    printf ( "-----------------------------------------------------------\n" );
    printf ( "Symmetry Fold     x       y       z      Angle      Peak\n" );
    printf ( "  Type                                             height\n" );
    printf ( "-----------------------------------------------------------\n" );
    for ( unsigned int gNo = 0; gNo < static_cast<unsigned int> ( this->cnSymm.size() ); gNo++ )
    {
        printf ( "   C      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->cnSymm.at(gNo)[0] ), this->cnSymm.at(gNo)[1], this->cnSymm.at(gNo)[2], this->cnSymm.at(gNo)[3], static_cast<int> ( this->cnSymm.at(gNo)[0] ), static_cast<double> ( this->cnSymm.at(gNo)[4] ) );
    }
    
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->dnSymm.size() ); iter++ )
    {
        printf ( "   D      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->dnSymm.at(iter).at(0)[0] ), this->dnSymm.at(iter).at(0)[1], this->dnSymm.at(iter).at(0)[2], this->dnSymm.at(iter).at(0)[3], static_cast<int> ( this->dnSymm.at(iter).at(0)[0] ), this->dnSymm.at(iter).at(0)[5] );
        
        for ( unsigned int mem = 1; mem < static_cast<unsigned int> ( this->dnSymm.at(iter).size() ); mem++ )
        {
            printf ( "          %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->dnSymm.at(iter).at(mem)[0] ), this->dnSymm.at(iter).at(mem)[1], this->dnSymm.at(iter).at(mem)[2], this->dnSymm.at(iter).at(mem)[3], static_cast<int> ( this->dnSymm.at(iter).at(mem)[0] ), this->dnSymm.at(iter).at(mem)[5] );
        }
    }
    
    if ( this->tetrElems.size() > 0 )
    {
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->tetrSymm.size() ); iter++ )
        {
            printf ( "   T      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->tetrSymm.at(iter).at(0)[0] ), this->tetrSymm.at(iter).at(0)[1], this->tetrSymm.at(iter).at(0)[2], this->tetrSymm.at(iter).at(0)[3], static_cast<int> ( this->tetrSymm.at(iter).at(0)[0] ), this->tetrSymm.at(iter).at(0)[4] );
            printf ( "          %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->tetrSymm.at(iter).at(1)[0] ), this->tetrSymm.at(iter).at(1)[1], this->tetrSymm.at(iter).at(1)[2], this->tetrSymm.at(iter).at(1)[3], static_cast<int> ( this->tetrSymm.at(iter).at(1)[0] ), this->tetrSymm.at(iter).at(1)[4] );
        }
    }
    
    if ( this->octaElems.size() > 0 )
    {
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->octaSymm.size() ); iter++ )
        {
            printf ( "   O      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->octaSymm.at(iter).at(0)[0] ), this->octaSymm.at(iter).at(0)[1], this->octaSymm.at(iter).at(0)[2], this->octaSymm.at(iter).at(0)[3], static_cast<int> ( this->octaSymm.at(iter).at(0)[0] ), this->octaSymm.at(iter).at(0)[4] );
            printf ( "          %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->octaSymm.at(iter).at(1)[0] ), this->octaSymm.at(iter).at(1)[1], this->octaSymm.at(iter).at(1)[2], this->octaSymm.at(iter).at(1)[3], static_cast<int> ( this->octaSymm.at(iter).at(1)[0] ), this->octaSymm.at(iter).at(1)[4] );
        }
    }
    
    if ( this->icosElems.size() > 0 )
    {
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->icosSymm.size() ); iter++ )
        {
            printf ( "   I      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->icosSymm.at(iter).at(0)[0] ), this->icosSymm.at(iter).at(0)[1], this->icosSymm.at(iter).at(0)[2], this->icosSymm.at(iter).at(0)[3], static_cast<int> ( this->icosSymm.at(iter).at(0)[0] ), this->icosSymm.at(iter).at(0)[4] );
            printf ( "          %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->icosSymm.at(iter).at(1)[0] ), this->icosSymm.at(iter).at(1)[1], this->icosSymm.at(iter).at(1)[2], this->icosSymm.at(iter).at(1)[3], static_cast<int> ( this->icosSymm.at(iter).at(1)[0] ), this->icosSymm.at(iter).at(1)[4] );
        }
    }
    std::cout << "-----------------------------------------------------------" << std::endl << std::endl;
    
    //======================================== Done
    return ;
    
}

/*! \brief This function prints the cleared results to the HTML report file.
 
 This version of the function prints the results to the HTML file with regard to the user specified expected symmetry. It not only
 searches for the requested symmetry in the already detected symmetries, but it also does extra searches if the requested symmetry
 cannot be found in the already detected ones.
 
 \param[in] symmetryType String with the type of symmetry that is being sought after. Allowed values are C, D, T, O and I.
 \param[in] symmetryFold The fold of the symmetry being sought after. Only used for C and D symmetry types.
 \param[in] settings The ProSHADE_settings class instance containing all the information needed to write properly into the HTML output file.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_symmetry::printResultsRequestHTML ( std::string symmetryType,
                                                                     unsigned int symmetryFold,
                                                                     ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Claim the symmetry to be the requested one
    if ( settings->htmlReport )
    {
        //==================================== Create review section
        rvapi_add_section                     ( "ReviewSection",
                                                "Review",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                false );
        settings->htmlReportLine             += 1;
        
        std::stringstream hlpSS;
        hlpSS << "<pre>" << "Requested symmetry :                 ";
        if ( symmetryType == "I" )
        {
            hlpSS << "Icosahedral" << "</pre>";
        }
        if ( symmetryType == "O" )
        {
            hlpSS << "Octahedral" << "</pre>";
        }
        if ( symmetryType == "T" )
        {
            hlpSS << "Tetrahedral" << "</pre>";
        }
        if ( symmetryType == "D" )
        {
            hlpSS << "Dihedral with fold " << ProSHADE_internal_misc::to_string_with_precision ( symmetryFold ) << "</pre>";
        }
        if ( symmetryType == "C" )
        {
            hlpSS << "Cyclic with fold " << ProSHADE_internal_misc::to_string_with_precision ( symmetryFold ) << "</pre>";
        }
            
            
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ReviewSection",
                                                0,
                                                0,
                                                1,
                                                1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Input structure    :                 " << this->structFiles.at(0) << "</pre>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ReviewSection",
                                               1,
                                               0,
                                               1,
                                               1 );
        rvapi_flush                           ( );
        
        //==================================== Create results section
        rvapi_add_section                     ( "ResultsSection",
                                                "Results",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                true );
        settings->htmlReportLine             += 1;
    }
    
    int totTabRows                            = 0;
    bool foundRequest                         = true;
    
    if ( symmetryType == "I" )
    {
        if ( ( static_cast<unsigned int> ( this->icosSymm.size() ) > 0 ) && ( settings->htmlReport ) && ( static_cast<unsigned int> ( this->icosElems.size() ) > 0 ) )
        {
            std::stringstream hlpSS;
            hlpSS << "<b>" << "Detected <i>ICOSAHEDRAL<i> symmetry as requested." << "</b>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ResultsSection",
                                               0,
                                               0,
                                               1,
                                               1 );
            rvapi_flush                       ( );
            
            //==================================== Symmetry axes table
            rvapi_add_table                   ( "SymmetryTypeTable",
                                                "Detected symmetry axes",
                                                "ResultsSection",
                                                1,
                                                0,
                                                static_cast<unsigned int> ( this->icosAxes.size() ),
                                                8,
                                                1 );
            
            // ... Column headers
            int columnIter                    = 0;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the symmetry type (Cyclic = C; Dihedral = D; Tetrahedral = T; Octahedral = O; Icosahedral = I).";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "Symmetry type", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the symmetry fold; this is only interesting for Cyclic and Dihedral symmetries.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "Symmetry fold", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the x-axis element of the symmetry axis.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the y-axis element of the symmetry axis.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the z-axis element of the symmetry axis.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry axis in degrees.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the average peak height (correlation after rotation) for all the symmetry rotations. The closer this number is to 1.0, the more reliable the symmetry existence is.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "Peak height", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            // ... Row headers
            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->icosAxes.size() ); icoIt++ )
            {
                std::stringstream hlpSS2;
                hlpSS2 << "Symmetry axis #" << icoIt+1;
                rvapi_put_vert_theader        ( "SymmetryTypeTable", hlpSS2.str().c_str(), "", icoIt );
            }
            
            // ... Fill in data
            int prec                              = 4;
            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->icosAxes.size() ); icoIt++ )
            {
                std::stringstream hlpSS2;
                hlpSS2 << "C";
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 0 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << static_cast<int> ( this->icosAxes.at(icoIt)[0] );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 1 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->icosAxes.at(icoIt)[1], prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 2 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->icosAxes.at(icoIt)[2], prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 3 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->icosAxes.at(icoIt)[3], prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 4 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( ( ( 2.0 * M_PI ) / static_cast<double> ( this->icosAxes.at(icoIt)[0] ) ) * ( 180.0 / M_PI ), prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 5 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->icosAxes.at(icoIt)[4], prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 6 );
            }
            
            //==================================== Symmetry elements table
            rvapi_add_table                   ( "SymmetryElementsTable",
                                                "Detected symmetry elements",
                                                "ResultsSection",
                                                static_cast<unsigned int> ( this->icosAxes.size() ) + 1,
                                                0,
                                                static_cast<unsigned int> ( this->icosElems.size() ),
                                                7,
                                                -1 );
            totTabRows                        = static_cast<unsigned int> ( this->icosAxes.size() ) + 1 + static_cast<unsigned int> ( this->icosElems.size() );
            
            // ... Column headers
            columnIter                        = 0;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the symmetry element type (Cyclic = C; Identity = E).";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "Symmetry element type", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the symmetry element fold.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "Symmetry element fold", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the x-axis element of the symmetry element axis.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the y-axis element of the symmetry element axis.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the z-axis element of the symmetry element axis.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry element axis in degrees.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            // ... Row headers
            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->icosElems.size() ); icoIt++ )
            {
                std::stringstream hlpSS2;
                hlpSS2 << "Symmetry element #" << icoIt+1;
                rvapi_put_vert_theader        ( "SymmetryElementsTable", hlpSS2.str().c_str(), "", icoIt );
            }
            
            // ... Fill in data
            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->icosElems.size() ); icoIt++ )
            {
                if ( this->icosElems.at(icoIt)[0] != 1.0 )
                {
                    std::stringstream hlpSS3;
                    hlpSS3 << "C";
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS3.str().c_str(), icoIt, 0 );
                }
                else
                {
                    std::stringstream hlpSS3;
                    hlpSS3 << "E";
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS3.str().c_str(), icoIt, 0 );
                }
                
                std::stringstream hlpSS2;
                hlpSS2 << static_cast<int> ( this->icosElems.at(icoIt)[0] );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 1 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->icosElems.at(icoIt)[1], prec );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 2 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->icosElems.at(icoIt)[2], prec );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 3 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->icosElems.at(icoIt)[3], prec );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 4 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->icosElems.at(icoIt)[4], prec );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 5 );
            }
            
            rvapi_flush                       ( );
        }
        else
        {
            std::stringstream hlpSS;
            hlpSS << "<b><font color=\"orange\">" << "Could not detect the requested symmetry. You can try changing the resolution or searching for different symmetry from the alternatives list printed below." << "</font></b>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                0,
                                                0,
                                                1,
                                                1 );
            rvapi_flush                       ( );
            foundRequest                      = false;
        }
    }
    
    if ( symmetryType == "O" )
    {
        if ( ( static_cast<unsigned int> ( this->octaSymm.size() ) > 0 ) && ( settings->htmlReport ) && ( static_cast<unsigned int> ( this->octaElems.size() ) > 0 ) )
        {
            //================================ State result
            std::stringstream hlpSS;
            hlpSS << "<b>" << "Detected <i>OCTAHEDRAL</i> symmetry as requested." << "</b>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                0,
                                                0,
                                                1,
                                                1 );
            rvapi_flush                       ( );
            
            //================================ Symmetry axes table
            rvapi_add_table                   ( "SymmetryTypeTable",
                                                "Detected symmetry axes",
                                                "ResultsSection",
                                                1,
                                                0,
                                                static_cast<unsigned int> ( this->octaAxes.size() ),
                                                8,
                                                1 );
            
            // ... Column headers
            int columnIter                    = 0;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the symmetry type (Cyclic = C; Dihedral = D; Tetrahedral = T; Octahedral = O; Icosahedral = I).";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "Symmetry type", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the symmetry fold; this is only interesting for Cyclic and Dihedral symmetries.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "Symmetry fold", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the x-axis element of the symmetry axis.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the y-axis element of the symmetry axis.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the z-axis element of the symmetry axis.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry axis in degrees.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the average peak height (correlation after rotation) for all the symmetry rotations. The closer this number is to 1.0, the more reliable the symmetry existence is.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "Peak height", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            // ... Row headers
            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->octaAxes.size() ); icoIt++ )
            {
                std::stringstream hlpSS2;
                hlpSS2 << "Symmetry axis #" << icoIt+1;
                rvapi_put_vert_theader        ( "SymmetryTypeTable", hlpSS2.str().c_str(), "", icoIt );
            }
            
            // ... Fill in data
            int prec                          = 4;
            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->octaAxes.size() ); icoIt++ )
            {
                std::stringstream hlpSS2;
                hlpSS2 << "C";
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 0 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << static_cast<int> ( this->octaAxes.at(icoIt)[0] );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 1 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->octaAxes.at(icoIt)[1], prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 2 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->octaAxes.at(icoIt)[2], prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 3 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->octaAxes.at(icoIt)[3], prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 4 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( ( ( 2.0 * M_PI ) / static_cast<double> ( this->octaAxes.at(icoIt)[0] ) ) * ( 180.0 / M_PI ), prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 5 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->octaAxes.at(icoIt)[4], prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 6 );
            }
            
            //================================ Symmetry elements table
            rvapi_add_table                   ( "SymmetryElementsTable",
                                                "Detected symmetry elements",
                                                "ResultsSection",
                                                static_cast<unsigned int> ( this->octaAxes.size() ) + 1,
                                                0,
                                                static_cast<unsigned int> ( this->octaElems.size() ),
                                                7,
                                               -1 );
            totTabRows                        = static_cast<unsigned int> ( this->octaAxes.size() ) + 1 + static_cast<unsigned int> ( this->octaElems.size() );
            
            // ... Column headers
            columnIter                        = 0;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the symmetry element type (Cyclic = C; Identity = E).";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "Symmetry element type", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the symmetry element fold.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "Symmetry element fold", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the x-axis element of the symmetry element axis.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the y-axis element of the symmetry element axis.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the z-axis element of the symmetry element axis.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry element axis in degrees.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            // ... Row headers
            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->octaElems.size() ); icoIt++ )
            {
                std::stringstream hlpSS2;
                hlpSS2 << "Symmetry element #" << icoIt+1;
                rvapi_put_vert_theader        ( "SymmetryElementsTable", hlpSS2.str().c_str(), "", icoIt );
            }
            
            // ... Fill in data
            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->octaElems.size() ); icoIt++ )
            {
                if ( this->octaElems.at(icoIt)[0] != 1.0 )
                {
                    std::stringstream hlpSS3;
                    hlpSS3 << "C";
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS3.str().c_str(), icoIt, 0 );
                }
                else
                {
                    std::stringstream hlpSS3;
                    hlpSS3 << "E";
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS3.str().c_str(), icoIt, 0 );
                }
                
                std::stringstream hlpSS2;
                hlpSS2 << static_cast<int> ( this->octaElems.at(icoIt)[0] );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 1 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->octaElems.at(icoIt)[1], prec );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 2 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->octaElems.at(icoIt)[2], prec );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 3 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->octaElems.at(icoIt)[3], prec );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 4 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->octaElems.at(icoIt)[4], prec );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 5 );
            }
            
            rvapi_flush                       ( );
        }
        else
        {
            std::stringstream hlpSS;
            hlpSS << "<b><font color=\"orange\">" << "Could not detect the requested symmetry. You can try changing the resolution or searching for different symmetry from the alternatives list printed below." << "</font></b>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                0,
                                                0,
                                                1,
                                                1 );
            rvapi_flush                       ( );
            foundRequest                      = false;
        }
    }
    
    if ( symmetryType == "T" )
    {
        if ( ( static_cast<unsigned int> ( this->tetrSymm.size() ) > 0 ) && ( settings->htmlReport ) && ( static_cast<unsigned int> ( this->tetrElems.size() ) > 0 ) )
        {
            //============================ State result
            std::stringstream hlpSS;
            hlpSS << "<b>" << "Detected <i>TETRAHEDRAL</i> symmetry as requested." << "</b>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                0,
                                                0,
                                                1,
                                                1 );
            rvapi_flush                       ( );
            
            //============================ Symmetry axes table
            rvapi_add_table                   ( "SymmetryTypeTable",
                                                "Detected symmetry axes",
                                                "ResultsSection",
                                                1,
                                                0,
                                                static_cast<unsigned int> ( this->tetrAxes.size() ),
                                                8,
                                                1 );
            
            // ... Column headers
            int columnIter                = 0;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the symmetry type (Cyclic = C; Dihedral = D; Tetrahedral = T; Octahedral = O; Icosahedral = I).";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "Symmetry type", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the symmetry fold; this is only interesting for Cyclic and Dihedral symmetries.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "Symmetry fold", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the x-axis element of the symmetry axis.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the y-axis element of the symmetry axis.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the z-axis element of the symmetry axis.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry axis in degrees.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the average peak height (correlation after rotation) for all the symmetry rotations. The closer this number is to 1.0, the more reliable the symmetry existence is.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "Peak height", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            // ... Row headers
            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->tetrAxes.size() ); icoIt++ )
            {
                std::stringstream hlpSS2;
                hlpSS2 << "Symmetry axis #" << icoIt+1;
                rvapi_put_vert_theader        ( "SymmetryTypeTable", hlpSS2.str().c_str(), "", icoIt );
            }
            
            // ... Fill in data
            int prec                          = 4;
            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->tetrAxes.size() ); icoIt++ )
            {
                std::stringstream hlpSS2;
                hlpSS2 << "C";
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 0 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << static_cast<int> ( this->tetrAxes.at(icoIt)[0] );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 1 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->tetrAxes.at(icoIt)[1], prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 2 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->tetrAxes.at(icoIt)[2], prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 3 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->tetrAxes.at(icoIt)[3], prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 4 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( ( ( 2.0 * M_PI ) / static_cast<double> ( this->tetrAxes.at(icoIt)[0] ) ) * ( 180.0 / M_PI ), prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 5 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->tetrAxes.at(icoIt)[4], prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 6 );
            }
            
            //============================ Symmetry elements table
            rvapi_add_table                   ( "SymmetryElementsTable",
                                                "Detected symmetry elements",
                                                "ResultsSection",
                                                static_cast<unsigned int> ( this->tetrAxes.size() ) + 1,
                                                0,
                                                static_cast<unsigned int> ( this->tetrElems.size() ),
                                                7,
                                               -1 );
            totTabRows                        = static_cast<unsigned int> ( this->tetrAxes.size() ) + 1 + static_cast<unsigned int> ( this->tetrElems.size() );
            
            // ... Column headers
            columnIter                        = 0;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the symmetry element type (Cyclic = C; Identity = E).";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "Symmetry element type", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the symmetry element fold.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "Symmetry element fold", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the x-axis element of the symmetry element axis.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the y-axis element of the symmetry element axis.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the z-axis element of the symmetry element axis.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry element axis in degrees.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            // ... Row headers
            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->tetrElems.size() ); icoIt++ )
            {
                std::stringstream hlpSS2;
                hlpSS2 << "Symmetry element #" << icoIt+1;
                rvapi_put_vert_theader        ( "SymmetryElementsTable", hlpSS2.str().c_str(), "", icoIt );
            }
            
            // ... Fill in data
            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->tetrElems.size() ); icoIt++ )
            {
                std::stringstream hlpSS3;
                if ( this->tetrElems.at(icoIt)[0] != 1.0 )
                {
                    std::stringstream hlpSS2;
                    hlpSS2 << "C";
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 0 );
                }
                else
                {
                    std::stringstream hlpSS2;
                    hlpSS2 << "E";
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 0 );
                }
                
                hlpSS3.str                    ( std::string ( ) );
                hlpSS3 << static_cast<int> ( this->tetrElems.at(icoIt)[0] );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS3.str().c_str(), icoIt, 1 );
                
                hlpSS3.str                    ( std::string ( ) );
                hlpSS3 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->tetrElems.at(icoIt)[1], prec );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS3.str().c_str(), icoIt, 2 );
                
                hlpSS3.str                    ( std::string ( ) );
                hlpSS3 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->tetrElems.at(icoIt)[2], prec );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS3.str().c_str(), icoIt, 3 );
                
                hlpSS3.str                    ( std::string ( ) );
                hlpSS3 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->tetrElems.at(icoIt)[3], prec );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS3.str().c_str(), icoIt, 4 );
                
                hlpSS3.str                    ( std::string ( ) );
                hlpSS3 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->tetrElems.at(icoIt)[4], prec );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS3.str().c_str(), icoIt, 5 );
            }
            
            rvapi_flush                       ( );
        }
        else
        {
            std::stringstream hlpSS;
            hlpSS << "<b><font color=\"orange\">" << "Could not detect the requested symmetry. You can try changing the resolution or searching for different symmetry from the alternatives list printed below." << "</font></b>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                0,
                                                0,
                                                1,
                                                1 );
            rvapi_flush                       ( );
            foundRequest                      = false;
        }
    }
    
    if ( symmetryType == "D" )
    {
        if ( static_cast<unsigned int> ( this->dnSymm.size() ) > 0 )
        {
            bool reqFound                     = false;
            for ( unsigned int dIt = 0; dIt < static_cast<unsigned int> ( this->dnSymm.size() ); dIt++ )
            {
                if ( reqFound ) { break; }
                unsigned int howManyTwos      = 0;
                for ( unsigned int dItt = 0; dItt < static_cast<unsigned int> ( this->dnSymm.at(dIt).size() ); dItt++ )
                {
                    if ( reqFound ) { break; }
                    if ( symmetryFold != 2 )
                    {
                        if ( this->dnSymm.at(dIt).at(dItt)[0] == symmetryFold )
                        {
                            std::stringstream hlpSS;
                            hlpSS << "<b>" << "Detected <i>DIHEDRAL</i> symmetry as requested." << "</b>";
                            rvapi_set_text    ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                0,
                                                0,
                                                1,
                                                1 );
                            rvapi_flush       ( );
                            
                            //======================== Symmetry axes table
                            rvapi_add_table   ( "SymmetryTypeTable",
                                                "Detected symmetry axes",
                                                "ResultsSection",
                                                1,
                                                0,
                                                this->dnSymm.at(dIt).size(),
                                                8,
                                                1 );
                            
                            // ... Column headers
                            int columnIter    = 0;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the symmetry type (Cyclic = C; Dihedral = D; Tetrahedral = T; Octahedral = O; Icosahedral = I).";
                            rvapi_put_horz_theader ( "SymmetryTypeTable", "Symmetry type", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the symmetry fold; this is only interesting for Cyclic and Dihedral symmetries.";
                            rvapi_put_horz_theader ( "SymmetryTypeTable", "Symmetry fold", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the x-axis element of the symmetry axis.";
                            rvapi_put_horz_theader ( "SymmetryTypeTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the y-axis element of the symmetry axis.";
                            rvapi_put_horz_theader ( "SymmetryTypeTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the z-axis element of the symmetry axis.";
                            rvapi_put_horz_theader ( "SymmetryTypeTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry axis in degrees.";
                            rvapi_put_horz_theader ( "SymmetryTypeTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the average peak height (correlation after rotation) for all the symmetry rotations. The closer this number is to 1.0, the more reliable the symmetry existence is.";
                            rvapi_put_horz_theader ( "SymmetryTypeTable", "Peak height", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            // ... Row headers
                            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->dnSymm.at(dIt).size() ); icoIt++ )
                            {
                                hlpSS.str     ( std::string ( ) );
                                hlpSS << "Symmetry axis #" << icoIt+1;
                                rvapi_put_vert_theader( "SymmetryTypeTable", hlpSS.str().c_str(), "", icoIt );
                            }
                            
                            // ... Fill in data
                            int prec          = 4;
                            for ( unsigned int it = 0; it < static_cast<unsigned int> ( this->dnSymm.at(dIt).size() ); it++ )
                            {
                                std::stringstream hlpSS2;
                                hlpSS2 << "C";
                                rvapi_put_table_string ( "SymmetryTypeTable", hlpSS2.str().c_str(), it, 0 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] );
                                rvapi_put_table_string ( "SymmetryTypeTable", hlpSS2.str().c_str(), it, 1 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[1], prec );
                                rvapi_put_table_string ( "SymmetryTypeTable", hlpSS2.str().c_str(), it, 2 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[2], prec );
                                rvapi_put_table_string ( "SymmetryTypeTable", hlpSS2.str().c_str(), it, 3 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[3], prec );
                                rvapi_put_table_string ( "SymmetryTypeTable", hlpSS2.str().c_str(), it, 4 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( ( ( 2.0 * M_PI ) / static_cast<double> ( this->dnSymm.at(dIt).at(it)[0] ) ) * ( 180.0 / M_PI ), prec );
                                rvapi_put_table_string ( "SymmetryTypeTable", hlpSS2.str().c_str(), it, 5 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[4], prec );
                                rvapi_put_table_string ( "SymmetryTypeTable", hlpSS2.str().c_str(), it, 6 );
                            }
                            
                            //======================== Symmetry elements table
                            int totRows               = 0;
                            for ( unsigned int it = 0; it < 2; it++ )
                            {
                                if ( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) % 2 == 0 )
                                {
                                    for ( int iter = -std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter++ )
                                    {
                                        if ( iter == 0 ) { continue; }
                                        if ( iter == -std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ) ) { continue; }
                                        
                                        totRows += 1;
                                    }
                                }
                                else
                                {
                                    for ( int iter = -std::floor( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter++ )
                                    {
                                        if ( iter == 0 ) { continue; }
                                        
                                        totRows += 1;
                                    }
                                }
                            }
                            totRows          += 1;
                            
                            rvapi_add_table   ( "SymmetryElementsTable",
                                                "Detected symmetry elements",
                                                "ResultsSection",
                                                this->dnSymm.at(dIt).size() + 1,
                                                0,
                                                totRows,
                                                7,
                                               -1 );
                            totTabRows        = static_cast<unsigned int> ( totRows ) + this->dnSymm.at(dIt).size() + 1;
                            
                            // ... Column headers
                            columnIter        = 0;
                            
                            hlpSS.str    ( std::string ( ) );
                            hlpSS << "This column states the symmetry element type (Cyclic = C; Identity = E).";
                            rvapi_put_horz_theader ( "SymmetryElementsTable", "Symmetry element type", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str    ( std::string ( ) );
                            hlpSS << "This column states the symmetry element fold.";
                            rvapi_put_horz_theader ( "SymmetryElementsTable", "Symmetry element fold", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str    ( std::string ( ) );
                            hlpSS << "This column states the x-axis element of the symmetry element axis.";
                            rvapi_put_horz_theader ( "SymmetryElementsTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str    ( std::string ( ) );
                            hlpSS << "This column states the y-axis element of the symmetry element axis.";
                            rvapi_put_horz_theader ( "SymmetryElementsTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str    ( std::string ( ) );
                            hlpSS << "This column states the z-axis element of the symmetry element axis.";
                            rvapi_put_horz_theader ( "SymmetryElementsTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str    ( std::string ( ) );
                            hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry element axis in degrees.";
                            rvapi_put_horz_theader ( "SymmetryElementsTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            // ... Row headers
                            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( totRows ); icoIt++ )
                            {
                                std::stringstream hlpSS2;
                                hlpSS2 << "Symmetry element #" << icoIt+1;
                                rvapi_put_vert_theader ( "SymmetryElementsTable", hlpSS2.str().c_str(), "", icoIt );
                            }
                            
                            // ... Fill in data
                            int rowCount      = 1;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "E";
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 0 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << static_cast<int> ( 1 );
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 1 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << std::setprecision ( prec ) << std::showpos << 1;
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 2 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << std::setprecision ( prec ) << std::showpos << 0;
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 3 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << std::setprecision ( prec ) << std::showpos << 0;
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 4 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << std::setprecision ( prec ) << std::showpos << 0;
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 5 );
                            
                            for ( unsigned int it = 0; it < 2; it++ )
                            {
                                if ( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) % 2 == 0 )
                                {
                                    for ( int iter = -std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter++ )
                                    {
                                        if ( iter == 0 ) { continue; }
                                        if ( iter == -std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ) ) { continue; }
                                        
                                        std::stringstream hlpSS2;
                                        hlpSS2 << "C";
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 0 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 1 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[1], prec );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 2 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[2], prec );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 3 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[3], prec );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 4 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( iter * ( 360.0 / static_cast<double> ( this->dnSymm.at(dIt).at(it)[0] ) ), prec );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 5 );
                                        rowCount += 1;
                                    }
                                }
                                else
                                {
                                    for ( int iter = -std::floor( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter++ )
                                    {
                                        if ( iter == 0 ) { continue; }
                                        
                                        std::stringstream hlpSS2;
                                        hlpSS2 << "C";
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 0 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 1 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[1], prec );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 2 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[2], prec );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 3 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[3], prec );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 4 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( iter * ( 360.0 / static_cast<double> ( this->dnSymm.at(dIt).at(it)[0] ) ), prec );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 5 );
                                        rowCount += 1;
                                    }
                                }
                            }
                            
                            rvapi_flush       ( );
                            reqFound          = true;
                            break;
                        }
                        else
                        {
                            if ( this->dnSymm.at(dIt).at(dItt)[0] == 2 )
                            {
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    if ( symmetryFold == 2 )
                    {
                        if ( this->dnSymm.at(dIt).at(dItt)[0] == 2 )
                        {
                            howManyTwos += 1;
                        }
                        if ( this->dnSymm.at(dIt).at(dItt)[0] == symmetryFold && howManyTwos == 2 )
                        {
                            std::stringstream hlpSS;
                            hlpSS << "<b>" << "Detected <i>DIHEDRAL</i> symmetry as requested." << "</b>";
                            rvapi_set_text    ( hlpSS.str().c_str(),
                                               "ResultsSection",
                                               0,
                                               0,
                                               1,
                                               1 );
                            rvapi_flush       ( );
                            
                            //======================== Symmetry axes table
                            rvapi_add_table   ( "SymmetryTypeTable",
                                               "Detected symmetry axes",
                                               "ResultsSection",
                                               1,
                                               0,
                                               2,
                                               8,
                                               1 );
                            
                            // ... Column headers
                            int columnIter    = 0;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the symmetry type (Cyclic = C; Dihedral = D; Tetrahedral = T; Octahedral = O; Icosahedral = I).";
                            rvapi_put_horz_theader ( "SymmetryTypeTable", "Symmetry type", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the symmetry fold; this is only interesting for Cyclic and Dihedral symmetries.";
                            rvapi_put_horz_theader ( "SymmetryTypeTable", "Symmetry fold", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the x-axis element of the symmetry axis.";
                            rvapi_put_horz_theader ( "SymmetryTypeTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the y-axis element of the symmetry axis.";
                            rvapi_put_horz_theader ( "SymmetryTypeTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the z-axis element of the symmetry axis.";
                            rvapi_put_horz_theader ( "SymmetryTypeTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry axis in degrees.";
                            rvapi_put_horz_theader ( "SymmetryTypeTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the average peak height (correlation after rotation) for all the symmetry rotations. The closer this number is to 1.0, the more reliable the symmetry existence is.";
                            rvapi_put_horz_theader ( "SymmetryTypeTable", "Peak height", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            // ... Row headers
                            int rCount        = 1;
                            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->dnSymm.at(dIt).size() ); icoIt++ )
                            {
                                if ( this->dnSymm.at(dIt).at(icoIt)[0] != 2 ) { continue; }
                                hlpSS.str     ( std::string ( ) );
                                hlpSS << "Symmetry axis #" << rCount;
                                rvapi_put_vert_theader( "SymmetryTypeTable", hlpSS.str().c_str(), "", rCount-1 );
                                rCount       += 1;
                            }
                            
                            // ... Fill in data
                            int prec          = 4;
                            rCount            = 0;
                            for ( unsigned int it = 0; it < static_cast<unsigned int> ( this->dnSymm.at(dIt).size() ); it++ )
                            {
                                if ( this->dnSymm.at(dIt).at(it)[0] != 2 ) { continue; }
                                
                                std::stringstream hlpSS2;
                                hlpSS2 << "C";
                                rvapi_put_table_string ( "SymmetryTypeTable", hlpSS2.str().c_str(), rCount, 0 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] );
                                rvapi_put_table_string ( "SymmetryTypeTable", hlpSS2.str().c_str(), rCount, 1 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[1], prec );
                                rvapi_put_table_string ( "SymmetryTypeTable", hlpSS2.str().c_str(), rCount, 2 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[2], prec );
                                rvapi_put_table_string ( "SymmetryTypeTable", hlpSS2.str().c_str(), rCount, 3 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[3], prec );
                                rvapi_put_table_string ( "SymmetryTypeTable", hlpSS2.str().c_str(), rCount, 4 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( ( ( 2.0 * M_PI ) / static_cast<double> ( this->dnSymm.at(dIt).at(it)[0] ) ) * ( 180.0 / M_PI ), prec );
                                rvapi_put_table_string ( "SymmetryTypeTable", hlpSS2.str().c_str(), rCount, 5 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[4], prec );
                                rvapi_put_table_string ( "SymmetryTypeTable", hlpSS2.str().c_str(), rCount, 6 );
                                
                                rCount       += 1;
                            }
                            
                            //======================== Symmetry elements table
                            int totRows               = 0;
                            for ( unsigned int it = 0; it < static_cast<unsigned int> ( this->dnSymm.at(dIt).size() ); it++ )
                            {
                                if ( this->dnSymm.at(dIt).at(it)[0] != 2 ) { continue; }
                                
                                if ( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) % 2 == 0 )
                                {
                                    for ( int iter = -std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter++ )
                                    {
                                        if ( iter == 0 ) { continue; }
                                        if ( iter == -std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ) ) { continue; }
                                        
                                        totRows += 1;
                                    }
                                }
                                else
                                {
                                    for ( int iter = -std::floor( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter++ )
                                    {
                                        if ( iter == 0 ) { continue; }
                                        
                                        totRows += 1;
                                    }
                                }
                            }
                            totRows          += 1;
                            
                            rvapi_add_table   ( "SymmetryElementsTable",
                                                "Detected symmetry elements",
                                                "ResultsSection",
                                                this->dnSymm.at(dIt).size() + 1,
                                                0,
                                                totRows,
                                                7,
                                                -1 );
                            totTabRows        = static_cast<unsigned int> ( totRows ) + this->dnSymm.at(dIt).size() + 1;
                            
                            // ... Column headers
                            columnIter        = 0;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the symmetry element type (Cyclic = C; Identity = E).";
                            rvapi_put_horz_theader ( "SymmetryElementsTable", "Symmetry element type", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the symmetry element fold.";
                            rvapi_put_horz_theader ( "SymmetryElementsTable", "Symmetry element fold", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the x-axis element of the symmetry element axis.";
                            rvapi_put_horz_theader ( "SymmetryElementsTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the y-axis element of the symmetry element axis.";
                            rvapi_put_horz_theader ( "SymmetryElementsTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the z-axis element of the symmetry element axis.";
                            rvapi_put_horz_theader ( "SymmetryElementsTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry element axis in degrees.";
                            rvapi_put_horz_theader ( "SymmetryElementsTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
                            columnIter       += 1;
                            
                            // ... Row headers
                            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( totRows ); icoIt++ )
                            {
                                std::stringstream hlpSS2;
                                hlpSS2 << "Symmetry element #" << icoIt+1;
                                rvapi_put_vert_theader ( "SymmetryElementsTable", hlpSS2.str().c_str(), "", icoIt );
                            }
                            
                            // ... Fill in data
                            int rowCount      = 1;
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "E";
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 0 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << static_cast<int> ( 1 );
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 1 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << std::setprecision ( prec ) << std::showpos << 1;
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 2 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << std::setprecision ( prec ) << std::showpos << 0;
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 3 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << std::setprecision ( prec ) << std::showpos << 0;
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 4 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << std::setprecision ( prec ) << std::showpos << 0;
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 5 );
                            
                            for ( unsigned int it = 0; it < static_cast<unsigned int> ( this->dnSymm.at(dIt).size() ); it++ )
                            {
                                if ( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) % 2 == 0 )
                                {
                                    if ( this->dnSymm.at(dIt).at(it)[0] != 2 ) { continue; }
                                    
                                    for ( int iter = -std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter++ )
                                    {
                                        if ( iter == 0 ) { continue; }
                                        if ( iter == -std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ) ) { continue; }
                                        
                                        std::stringstream hlpSS2;
                                        hlpSS2 << "C";
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 0 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 1 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[1], prec );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 2 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[2], prec );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 3 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[3], prec );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 4 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( iter * ( 360.0 / static_cast<double> ( this->dnSymm.at(dIt).at(it)[0] ) ), prec );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 5 );
                                        rowCount += 1;
                                    }
                                }
                                else
                                {
                                    if ( this->dnSymm.at(dIt).at(it)[0] != 2 ) { continue; }
                                    
                                    for ( int iter = -std::floor( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter++ )
                                    {
                                        if ( iter == 0 ) { continue; }
                                        
                                        std::stringstream hlpSS2;
                                        hlpSS2 << "C";
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 0 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 1 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[1], prec );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 2 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[2], prec );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 3 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(dIt).at(it)[3], prec );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 4 );
                                        
                                        hlpSS2.str ( std::string ( ) );
                                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( iter * ( 360.0 / static_cast<double> ( this->dnSymm.at(dIt).at(it)[0] ) ), prec );
                                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 5 );
                                        rowCount += 1;
                                    }
                                }
                            }
                            
                            rvapi_flush       ( );
                            reqFound = true;
                            break;
                        }
                    }
                }
            }
            if ( !reqFound )
            {
                std::stringstream hlpSS;
                hlpSS << "<b><font color=\"orange\">" << "Could not detect the requested dihedral symmetry, but detected dihedral symmetries with different fold. You can try changing the resolution or selecting one of the alternative symmetries printed below." << "</font></b>";
                rvapi_set_text                ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                0,
                                                0,
                                                1,
                                                1 );
                rvapi_flush                   ( );
                foundRequest                  = false;
            }
        }
        else
        {
            std::stringstream hlpSS;
            hlpSS << "<b><font color=\"orange\">" << "Could not detect the requested symmetry. You can try changing the resolution or searching for different symmetry from the alternatives list printed below." << "</font></b>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                0,
                                                0,
                                                1,
                                                1 );
            rvapi_flush                       ( );
            foundRequest                      = false;
        }
    }
    
    if ( symmetryType == "C" )
    {
        if ( static_cast<unsigned int> ( this->cnSymm.size() ) > 0 )
        {
            bool reqFound = false;
            for ( unsigned int dIt = 0; dIt < static_cast<unsigned int> ( this->cnSymm.size() ); dIt++ )
            {
                if ( this->cnSymm.at(dIt)[0] == symmetryFold )
                {
                    //==================== State result
                    std::stringstream hlpSS;
                    hlpSS << "<b>" << "Detected <i>CYCLIC</i> symmetry as requested." << "</b>";
                    rvapi_set_text            ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                0,
                                                0,
                                                1,
                                                1 );
                    rvapi_flush               ( );
                    
                    //==================== Symmetry axes table
                    rvapi_add_table           ( "SymmetryTypeTable",
                                                "Detected symmetry axes",
                                                "ResultsSection",
                                                1,
                                                0,
                                                2,
                                                8,
                                                2 );
                    
                    // ... Column headers
                    int columnIter            = 0;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the symmetry type (Cyclic = C; Dihedral = D; Tetrahedral = T; Octahedral = O; Icosahedral = I).";
                    rvapi_put_horz_theader    ( "SymmetryTypeTable", "Symmetry type", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the symmetry fold; this is only interesting for Cyclic and Dihedral symmetries.";
                    rvapi_put_horz_theader    ( "SymmetryTypeTable", "Symmetry fold", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the x-axis element of the symmetry axis.";
                    rvapi_put_horz_theader    ( "SymmetryTypeTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the y-axis element of the symmetry axis.";
                    rvapi_put_horz_theader    ( "SymmetryTypeTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the z-axis element of the symmetry axis.";
                    rvapi_put_horz_theader    ( "SymmetryTypeTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry axis in degrees.";
                    rvapi_put_horz_theader    ( "SymmetryTypeTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the average peak height (correlation after rotation) for all the symmetry rotations. The closer this number is to 1.0, the more reliable the symmetry existence is.";
                    rvapi_put_horz_theader( "SymmetryTypeTable", "Peak height", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    // ... Row headers
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "Symmetry axis #" << 1;
                    rvapi_put_vert_theader    ( "SymmetryTypeTable", hlpSS.str().c_str(), "", 0 );
                    
                    // ... Fill in data
                    int prec                  = 4;
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "C";
                    rvapi_put_table_string    ( "SymmetryTypeTable", hlpSS.str().c_str(), 0, 0 );
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << static_cast<int> ( this->cnSymm.at(dIt)[0] );
                    rvapi_put_table_string    ( "SymmetryTypeTable", hlpSS.str().c_str(), 0, 1 );
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymm.at(dIt)[1], prec );
                    rvapi_put_table_string    ( "SymmetryTypeTable", hlpSS.str().c_str(), 0, 2 );
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymm.at(dIt)[2], prec );
                    rvapi_put_table_string    ( "SymmetryTypeTable", hlpSS.str().c_str(), 0, 3 );
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymm.at(dIt)[3], prec );
                    rvapi_put_table_string    ( "SymmetryTypeTable", hlpSS.str().c_str(), 0, 4 );
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( ( ( 2.0 * M_PI ) / static_cast<double> ( this->cnSymm.at(dIt)[0] ) ) * ( 180.0 / M_PI ), prec );
                    rvapi_put_table_string    ( "SymmetryTypeTable", hlpSS.str().c_str(), 0, 5 );
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymm.at(dIt)[4], prec );
                    rvapi_put_table_string    ( "SymmetryTypeTable", hlpSS.str().c_str(), 0, 6 );
                    
                    //==================== Symmetry elements table
                    int totRows               = 0;
                    if ( static_cast<int> ( this->cnSymm.at(dIt)[0] ) % 2 == 0 )
                    {
                        for ( int iter = -std::ceil( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ); iter++ )
                        {
                            if ( iter == 0 ) { continue; }
                            if ( iter == -std::ceil( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ) ) { continue; }
                            
                            totRows          += 1;
                        }
                    }
                    else
                    {
                        for ( int iter = -std::floor( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ); iter++ )
                        {
                            if ( iter == 0 ) { continue; }
                            
                            totRows          += 1;
                        }
                    }
                    totRows                  += 1;
                    
                    rvapi_add_table           ( "SymmetryElementsTable",
                                                "Detected symmetry elements",
                                                "ResultsSection",
                                                3,
                                                0,
                                                totRows,
                                                7,
                                               -1 );
                    totTabRows                = static_cast<unsigned int> ( totRows ) + 3;
                    
                    // ... Column headers
                    columnIter                = 0;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the symmetry element type (Cyclic = C; Identity = E).";
                    rvapi_put_horz_theader    ( "SymmetryElementsTable", "Symmetry element type", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the symmetry element fold.";
                    rvapi_put_horz_theader    ( "SymmetryElementsTable", "Symmetry element fold", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the x-axis element of the symmetry element axis.";
                    rvapi_put_horz_theader    ( "SymmetryElementsTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the y-axis element of the symmetry element axis.";
                    rvapi_put_horz_theader    ( "SymmetryElementsTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the z-axis element of the symmetry element axis.";
                    rvapi_put_horz_theader    ( "SymmetryElementsTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry element axis in degrees.";
                    rvapi_put_horz_theader    ( "SymmetryElementsTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    // ... Row headers
                    for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( totRows ); icoIt++ )
                    {
                        std::stringstream hlpSS2;
                        hlpSS2 << "Symmetry element #" << std::to_string ( icoIt+1 );
                        rvapi_put_vert_theader( "SymmetryElementsTable", hlpSS2.str().c_str(), "", icoIt );
                    }
                    
                    // ... Fill in data
                    int rowCount              = 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "E";
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 0 );
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << static_cast<int> ( 1 );
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 1 );
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << std::setprecision ( prec ) << std::showpos << 1;
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 2 );
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << std::setprecision ( prec ) << std::showpos << 0;
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 3 );
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << std::setprecision ( prec ) << std::showpos << 0;
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 4 );
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << std::setprecision ( prec ) << std::showpos << 0;
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 5 );
                    
                    if ( static_cast<int> ( this->cnSymm.at(dIt)[0] ) % 2 == 0 )
                    {
                        for ( int iter = -std::ceil( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ); iter++ )
                        {
                            if ( iter == 0 ) { continue; }
                            if ( iter == -std::ceil( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ) ) { continue; }
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "C";
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 0 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << static_cast<int> ( this->cnSymm.at(dIt)[0] );
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 1 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymm.at(dIt)[1], prec );
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 2 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymm.at(dIt)[2], prec );
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 3 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymm.at(dIt)[3], prec );
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 4 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( iter * ( 360.0 / static_cast<double> ( this->cnSymm.at(dIt)[0] ) ), prec );
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 5 );
                            rowCount += 1;
                        }
                    }
                    else
                    {
                        for ( int iter = -std::floor( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ); iter++ )
                        {
                            if ( iter == 0 ) { continue; }
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << "C";
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 0 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << static_cast<int> ( this->cnSymm.at(dIt)[0] );
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 1 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymm.at(dIt)[1], prec );
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 2 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymm.at(dIt)[2], prec );
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 3 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymm.at(dIt)[3], prec );
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 4 );
                            
                            hlpSS.str         ( std::string ( ) );
                            hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( iter * ( 360.0 / static_cast<double> ( this->cnSymm.at(dIt)[0] ) ), prec );
                            rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 5 );
                            rowCount += 1;
                        }
                    }
                    
                    rvapi_flush               ( );
                    reqFound = true;
                    break;
                }
            }
            if ( !reqFound )
            {
                std::stringstream hlpSS;
                hlpSS << "<b><font color=\"orange\">" << "Could not detect the requested cyclic symmetry, but detected other cyclic symmetries with different fold. You can try changing the resolution or selecting one of the alternative symmetries printed below." << "</font></b>";
                rvapi_set_text                ( hlpSS.str().c_str(),
                                               "ResultsSection",
                                               0,
                                               0,
                                               1,
                                               1 );
                rvapi_flush                   ( );
                foundRequest                  = false;
            }
        }
        else
        {
            std::stringstream hlpSS;
            hlpSS << "<b><font color=\"orange\">" << "Could not detect the requested symmetry. You can try changing the resolution or searching for different symmetry from the alternatives list printed below." << "</font></b>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ResultsSection",
                                               0,
                                               0,
                                               1,
                                               1 );
            rvapi_flush                       ( );
            foundRequest                      = false;
        }
    }
    
    //======================================== Print alternatives
    if ( foundRequest )
    {
        rvapi_add_table                       ( "AlternativesTable",
                                                "Alternative Symmetries Detected",
                                                "ResultsSection",
                                                totTabRows,
                                                0,
                                                static_cast<unsigned int> ( this->cnSymm.size() ) + static_cast<unsigned int> ( this->dnSymm.size() ) +
                                                static_cast<unsigned int> ( this->tetrSymm.size() ) + static_cast<unsigned int> ( this->octaSymm.size() ) +
                                                static_cast<unsigned int> ( this->icosSymm.size() ),
                                                7,
                                               -1 );
    }
    else
    {
        rvapi_add_table                       ( "AlternativesTable",
                                                "Alternative Symmetries Detected",
                                                "ResultsSection",
                                                totTabRows,
                                                0,
                                                static_cast<unsigned int> ( this->cnSymm.size() ) + static_cast<unsigned int> ( this->dnSymm.size() ) +
                                                static_cast<unsigned int> ( this->tetrSymm.size() ) + static_cast<unsigned int> ( this->octaSymm.size() ) +
                                                static_cast<unsigned int> ( this->icosSymm.size() ),
                                                7,
                                                1 );
    }
    
    // ... Column headers
    int columnIter                            = 0;
    
    std::stringstream hlpSS;
    hlpSS << "This column states the symmetry type (Cyclic = C; Dihedral = D).";
    rvapi_put_horz_theader                    ( "AlternativesTable", "Symmetry element type", hlpSS.str().c_str(), columnIter );
    columnIter                               += 1;
    
    hlpSS.str                                 ( std::string ( ) );
    hlpSS << "This column states the symmetry fold.";
    rvapi_put_horz_theader                    ( "AlternativesTable", "Symmetry element fold", hlpSS.str().c_str(), columnIter );
    columnIter                               += 1;
    
    hlpSS.str                                 ( std::string ( ) );
    hlpSS << "This column states the x-axis element of the symmetry axis.";
    rvapi_put_horz_theader                    ( "AlternativesTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
    columnIter                               += 1;
    
    hlpSS.str                                 ( std::string ( ) );
    hlpSS << "This column states the y-axis element of the symmetry axis.";
    rvapi_put_horz_theader                    ( "AlternativesTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
    columnIter                               += 1;
    
    hlpSS.str                                 ( std::string ( ) );
    hlpSS << "This column states the z-axis element of the symmetry axis.";
    rvapi_put_horz_theader                    ( "AlternativesTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
    columnIter                               += 1;
    
    hlpSS.str                                 ( std::string ( ) );
    hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry axis in degrees.";
    rvapi_put_horz_theader                    ( "AlternativesTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
    columnIter                               += 1;
    
    hlpSS.str                                 ( std::string ( ) );
    hlpSS << "This column states the average peak height (correlation after rotation) for all the symmetry rotations. The closer this number is to 1.0, the more reliable the symmetry existence is.";
    rvapi_put_horz_theader                    ( "AlternativesTable", "Peak Height", hlpSS.str().c_str(), columnIter );
    columnIter                               += 1;
    
    // ... Row headers
    int maxCAlts                              = 0;
    int maxDAlts                              = 0;
    for ( unsigned int gNo = 0; gNo < static_cast<unsigned int> ( this->cnSymm.size() ); gNo++ )
    {
        std::stringstream hlpSS2;
        hlpSS2 << "Alternative Symmetry #" << gNo+1;
        rvapi_put_vert_theader                ( "AlternativesTable", hlpSS2.str().c_str(), "Reported symmetry alternative (i.e. also detected, but with lower reliability)", gNo );
        maxCAlts                              = gNo;
    }
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->dnSymm.size() ); iter++ )
    {
        std::stringstream hlpSS2;
        hlpSS2 << "Alternative Symmetry #" << iter + maxCAlts + 2;
        rvapi_put_vert_theader                ( "AlternativesTable", hlpSS2.str().c_str(), "Reported symmetry alternative (i.e. also detected, but with lower reliability)", iter + maxCAlts + 1 );
        maxDAlts                              = iter + maxCAlts;
    }
    maxDAlts                                 += 1;
    if ( static_cast<int> ( this->tetrElems.size() ) > 0 )
    {
        std::stringstream hlpSS2;
        hlpSS2 << "Alternative Symmetry #" <<  maxDAlts + 2;
        rvapi_put_vert_theader                ( "AlternativesTable", hlpSS2.str().c_str(), "Reported symmetry alternative (i.e. also detected, but with lower reliability)", maxDAlts + 1 );
        maxDAlts                             += 1;
    }
    if ( static_cast<int> ( this->octaElems.size() ) > 0 )
    {
        std::stringstream hlpSS2;
        hlpSS2 << "Alternative Symmetry #" << maxDAlts + 2;
        rvapi_put_vert_theader                ( "AlternativesTable", hlpSS2.str().c_str(), "Reported symmetry alternative (i.e. also detected, but with lower reliability)", maxDAlts + 1 );
        maxDAlts                             += 1;
    }
    if ( static_cast<int> ( this->icosElems.size() ) > 0 )
    {
        std::stringstream hlpSS2;
        hlpSS2 << "Alternative Symmetry #" << maxDAlts + 2;
        rvapi_put_vert_theader                ( "AlternativesTable", hlpSS2.str().c_str(), "Reported symmetry alternative (i.e. also detected, but with lower reliability)", maxDAlts + 1 );
        maxDAlts                             += 1;
    }
    
    int rowCount                              = 0;
    int prec                                  = 4;
    for ( unsigned int gNo = 0; gNo < static_cast<unsigned int> ( this->cnSymm.size() ); gNo++ )
    {
        std::stringstream hlpSS2;
        hlpSS2 << "C";
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 0 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << static_cast<int> ( static_cast<int> ( this->cnSymm.at(gNo)[0] ) );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 1 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymm.at(gNo)[1], prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 2 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymm.at(gNo)[2], prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 3 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymm.at(gNo)[3], prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 4 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( ( ( 2.0 * M_PI ) / static_cast<double> ( this->cnSymm.at(gNo)[0] ) ) * ( 180.0 / M_PI ), prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 5 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( static_cast<double> ( this->cnSymm.at(gNo)[4] ), prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 6 );
        rowCount                             += 1;
    }
    
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->dnSymm.size() ); iter++ )
    {
        std::stringstream hlpSS2;
        hlpSS2 << "D";
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 0 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << static_cast<int> ( static_cast<int> ( this->dnSymm.at(iter).at(0)[0] ) );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 1 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(iter).at(0)[1], prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 2 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(iter).at(0)[2], prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 3 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(iter).at(0)[3], prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 4 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( ( ( 2.0 * M_PI ) / static_cast<double> ( this->dnSymm.at(iter).at(0)[0] ) ) * ( 180.0 / M_PI ), prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 5 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( static_cast<double> ( this->dnSymm.at(iter).at(0)[4] ), prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 6 );
        rowCount                             += 1;
    }
    
    if ( static_cast<int> ( this->tetrElems.size() ) > 0 )
    {
        std::stringstream hlpSS2;
        hlpSS2 << "T";
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 0 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << "N/A";
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 1 );
        
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 2 );
        
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 3 );
        
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 4 );
        
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 5 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( static_cast<double> ( this->tetrSymm.at(0).at(0)[4] ), prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 6 );
        rowCount                             += 1;
    }
    
    if ( static_cast<int> ( this->octaElems.size() ) > 0 )
    {
        std::stringstream hlpSS2;
        hlpSS2 << "O";
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 0 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << "N/A";
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 1 );
        
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 2 );
        
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 3 );
        
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 4 );
        
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 5 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( static_cast<double> ( this->octaSymm.at(0).at(0)[4] ), prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 6 );
        rowCount                             += 1;
    }
    
    if ( static_cast<int> ( this->icosElems.size() ) > 0 )
    {
        std::stringstream hlpSS2;
        hlpSS2 << "I";
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 0 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << "N/A";
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 1 );
        
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 2 );
        
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 3 );
        
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 4 );
        
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 5 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( static_cast<double> ( this->icosSymm.at(0).at(0)[4] ), prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 6 );
        rowCount                             += 1;
    }
    
    rvapi_flush                               ( );
    
    //======================================== Done
    return ;
    
}

/*! \brief This function prints the cleared results to the screen.
 
 This version of the function prints the results to the screen with regard to the distribution of average peak height for the
 symmetries, so only the most relevant symmetries are shown. It has a chance of missing a true symmetry, but this typically
 happens in cases where the settings are disruptive for the prpcedure or when the computation has somehow failed.
 
 \param[in] verbose Int value determining how verbal the function should be. It has currently no effect, though...
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_symmetry::printResultsClear ( int verbose )
{
    //======================================== Claim the symmetry to be the highest detected
    if ( verbose > 0 )
    {
        printf ( "-----------------------------------------------------------\n" );
        printf ( "|                         RESULTS                         |\n" );
        printf ( "-----------------------------------------------------------\n\n" );
    }
    
    if ( ( static_cast<unsigned int> ( this->icosSymm.size() ) > 0 ) && ( static_cast<unsigned int> ( this->icosElems.size() ) == 0 ) )
    {
        printf ( "This appears like icosahedral symmetry, but not all axes could be detected. Maybe decreasing resolution or relaxing the peak similarity requirement would improve the detection. Proceeding this what was detected completely.\n\n" );
    }
    
    if ( ( static_cast<unsigned int> ( this->icosSymm.size() ) > 0 ) && ( static_cast<unsigned int> ( this->icosElems.size() ) > 0 ) )
    {
        printf ( "Detected Icosahedral symmetry\n\n" );
        printf ( "Symmetry axes table:\n" );
        printf ( "-----------------------------------------------------------\n" );
        printf ( "Symmetry Fold     x       y       z      Angle      Peak\n" );
        printf ( "  Type                                             height\n" );
        printf ( "-----------------------------------------------------------\n" );

        for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->icosAxes.size() ); icoIt++ )
        {
            printf ( "   C      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->icosAxes.at(icoIt)[0] ), this->icosAxes.at(icoIt)[1], this->icosAxes.at(icoIt)[2], this->icosAxes.at(icoIt)[3], static_cast<int> ( this->icosAxes.at(icoIt)[0] ), this->icosAxes.at(icoIt)[4] );
        }
        printf ( "\n" );
        
        printf ( "Symmetry elements table:\n" );
        printf ( "-----------------------------------------------------------\n" );
        printf ( "Symmetry          x          y          z          Angle        \n" );
        printf ( "  Type                                             (deg)        \n" );
        printf ( "-----------------------------------------------------------\n" );
        
        for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->icosElems.size() ); icoIt++ )
        {
            if ( this->icosElems.at(icoIt)[0] != 1.0 )
            {
                printf ( "   C%d           %+.2f      %+.2f      %+.2f        %+.1f    \n", static_cast<int> ( this->icosElems.at(icoIt)[0] ), this->icosElems.at(icoIt)[1], this->icosElems.at(icoIt)[2], this->icosElems.at(icoIt)[3], this->icosElems.at(icoIt)[4] );
            }
            else
            {
                printf ( "   E            %+.2f      %+.2f      %+.2f        %+.1f    \n", this->icosElems.at(icoIt)[1], this->icosElems.at(icoIt)[2], this->icosElems.at(icoIt)[3], this->icosElems.at(icoIt)[4] );
            }
        }
        printf ( "\n" );
    }
    else
    {
        if ( ( static_cast<unsigned int> ( this->octaSymm.size() ) > 0 ) && ( static_cast<unsigned int> ( this->octaElems.size() ) == 0 ) )
        {
            printf ( "This appears like octahedral symmetry, but not all axes could be detected. Maybe decreasing resolution or relaxing the peak similarity requirement would improve the detection. Proceeding this what was detected completely.\n\n" );
        }
        
        if ( ( static_cast<unsigned int> ( this->octaSymm.size() ) > 0 ) && ( static_cast<unsigned int> ( this->octaElems.size() ) > 0 ) )
        {
            printf ( "Detected (Cub)octahedral symmetry\n\n" );
            printf ( "Symmetry axes table:\n" );
            printf ( "-----------------------------------------------------------\n" );
            printf ( "Symmetry Fold     x       y       z      Angle      Peak\n" );
            printf ( "  Type                                             height\n" );
            printf ( "-----------------------------------------------------------\n" );
            
            for ( unsigned int octIt = 0; octIt < static_cast<unsigned int> ( this->octaAxes.size() ); octIt++ )
            {
                printf ( "   C      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->octaAxes.at(octIt)[0] ), this->octaAxes.at(octIt)[1], this->octaAxes.at(octIt)[2], this->octaAxes.at(octIt)[3], static_cast<int> ( this->octaAxes.at(octIt)[0] ), this->octaAxes.at(octIt)[4] );
            }
            printf ( "\n" );
            
            printf ( "Symmetry elements table:\n" );
            printf ( "-----------------------------------------------------------\n" );
            printf ( "Symmetry          x          y          z          Angle        \n" );
            printf ( "  Type                                             (deg)        \n" );
            printf ( "-----------------------------------------------------------\n" );
            
            for ( unsigned int octIt = 0; octIt < static_cast<unsigned int> ( this->octaElems.size() ); octIt++ )
            {
                if ( this->octaElems.at(octIt)[0] != 1.0 )
                {
                    printf ( "   C%d           %+.2f      %+.2f      %+.2f        %+.1f    \n", static_cast<int> ( this->octaElems.at(octIt)[0] ), this->octaElems.at(octIt)[1], this->octaElems.at(octIt)[2], this->octaElems.at(octIt)[3], this->octaElems.at(octIt)[4] );
                }
                else
                {
                    printf ( "   E            %+.2f      %+.2f      %+.2f        %+.1f    \n", this->octaElems.at(octIt)[1], this->octaElems.at(octIt)[2], this->octaElems.at(octIt)[3], this->octaElems.at(octIt)[4] );
                }
            }
            printf ( "\n" );
        }
        else
        {
            if ( ( static_cast<unsigned int> ( this->tetrSymm.size() ) > 0 ) && ( static_cast<unsigned int> ( this->tetrElems.size() ) == 0 ) )
            {
                if ( ( static_cast<unsigned int> ( this->octaSymm.size() ) > 0 ) && ( static_cast<unsigned int> ( this->octaElems.size() ) == 0 ) )
                {
                    printf ( "This appears like tetrahedral symmetry, but not all axes could be detected. Maybe decreasing resolution or relaxing the peak similarity requirement would improve the detection. Proceeding this what was detected completely.\n\n" );
                }
            }
            
            if ( ( static_cast<unsigned int> ( this->tetrSymm.size() ) > 0 ) && ( static_cast<unsigned int> ( this->tetrElems.size() ) > 0 ) )
            {
                printf ( "Detected Tetrahedral symmetry\n\n" );
                printf ( "Symmetry axes table:\n" );
                printf ( "-----------------------------------------------------------\n" );
                printf ( "Symmetry Fold     x       y       z      Angle      Peak\n" );
                printf ( "  Type                                             height\n" );
                printf ( "-----------------------------------------------------------\n" );
                
                for ( unsigned int tetIt = 0; tetIt < static_cast<unsigned int> ( this->tetrAxes.size() ); tetIt++ )
                {
                    printf ( "   C      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->tetrAxes.at(tetIt)[0] ), this->tetrAxes.at(tetIt)[1], this->tetrAxes.at(tetIt)[2], this->tetrAxes.at(tetIt)[3], static_cast<int> ( this->tetrAxes.at(tetIt)[0] ), this->tetrAxes.at(tetIt)[4] );
                }
                printf ( "\n" );
                
                printf ( "Symmetry elements table:\n" );
                printf ( "-----------------------------------------------------------\n" );
                printf ( "Symmetry          x          y          z          Angle        \n" );
                printf ( "  Type                                             (deg)        \n" );
                printf ( "-----------------------------------------------------------\n" );
                
                for ( unsigned int tetIt = 0; tetIt < static_cast<unsigned int> ( this->tetrElems.size() ); tetIt++ )
                {
                    if ( this->tetrElems.at(tetIt)[0] != 1.0 )
                    {
                        printf ( "   C%d           %+.2f      %+.2f      %+.2f        %+.1f    \n", static_cast<int> ( this->tetrElems.at(tetIt)[0] ), this->tetrElems.at(tetIt)[1], this->tetrElems.at(tetIt)[2], this->tetrElems.at(tetIt)[3], this->tetrElems.at(tetIt)[4] );
                    }
                    else
                    {
                        printf ( "   E            %+.2f      %+.2f      %+.2f        %+.1f    \n", this->tetrElems.at(tetIt)[1], this->tetrElems.at(tetIt)[2], this->tetrElems.at(tetIt)[3], this->tetrElems.at(tetIt)[4] );
                    }
                }
                printf ( "\n" );
            }
            else
            {
                if ( static_cast<unsigned int> ( this->dnSymmClear.size() ) > 0 )
                {
                    printf ( "Detected Dihedral symmetry\n\n" );
                    printf ( "Symmetry axes table:\n" );
                    printf ( "-----------------------------------------------------------\n" );
                    printf ( "Symmetry Fold     x       y       z      Angle      Peak\n" );
                    printf ( "  Type                                             height\n" );
                    printf ( "-----------------------------------------------------------\n" );
                    printf ( "   C      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->dnSymmClear.at(0).at(0)[0] ), this->dnSymmClear.at(0).at(0)[1], this->dnSymmClear.at(0).at(0)[2], this->dnSymmClear.at(0).at(0)[3], static_cast<int> ( this->dnSymmClear.at(0).at(0)[0] ), this->dnSymmClear.at(0).at(0)[4] );
                    printf ( "   C      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n\n", static_cast<int> ( this->dnSymmClear.at(0).at(1)[0] ), this->dnSymmClear.at(0).at(1)[1], this->dnSymmClear.at(0).at(1)[2], this->dnSymmClear.at(0).at(1)[3], static_cast<int> ( this->dnSymmClear.at(0).at(1)[0] ), this->dnSymmClear.at(0).at(1)[4] );
                    
                    printf ( "\n" );
                    
                    printf ( "Symmetry elements table:\n" );
                    printf ( "-----------------------------------------------------------\n" );
                    printf ( "Symmetry          x          y          z          Angle        \n" );
                    printf ( "  Type                                             (deg)        \n" );
                    printf ( "-----------------------------------------------------------\n" );
                    
                    printf ( "   E            %+.2f      %+.2f      %+.2f        %+.1f    \n", 1.0, 0.0, 0.0, 0.0 );
                    for ( unsigned int it = 0; it < 2; it++ )
                    {
                        if ( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) % 2 == 0 )
                        {
                            for ( int iter = -std::ceil( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ); iter++ )
                            {
                                if ( iter == 0 ) { continue; }
                                if ( iter == -std::ceil( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ) ) { continue; }
                                printf ( "   C%d           %+.2f      %+.2f      %+.2f        %+.1f    \n", static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ), this->dnSymmClear.at(0).at(it)[1], this->dnSymmClear.at(0).at(it)[2], this->dnSymmClear.at(0).at(it)[3], iter * ( 360.0 / static_cast<double> ( this->dnSymmClear.at(0).at(it)[0] ) ) );
                            }
                        }
                        else
                        {
                            for ( int iter = -std::floor( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ); iter++ )
                            {
                                if ( iter == 0 ) { continue; }
                                printf ( "   C%d           %+.2f      %+.2f      %+.2f        %+.1f    \n", static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ), this->dnSymmClear.at(0).at(it)[1], this->dnSymmClear.at(0).at(it)[2], this->dnSymmClear.at(0).at(it)[3], iter * ( 360.0 / static_cast<double> ( this->dnSymmClear.at(0).at(it)[0] ) ) );
                            }
                        }
                    }
                        
                    printf ( "\n" );
                }
                else
                {
                    if ( static_cast<unsigned int> ( this->cnSymmClear.size() ) > 0 )
                    {
                        printf ( "Detected Cyclic symmetry\n\n" );
                        printf ( "Symmetry axes table:\n" );
                        printf ( "-----------------------------------------------------------\n" );
                        printf ( "Symmetry Fold     x       y       z      Angle      Peak\n" );
                        printf ( "  Type                                             height\n" );
                        printf ( "-----------------------------------------------------------\n" );
                        printf ( "   C      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n\n", static_cast<int> ( this->cnSymmClear.at(0)[0] ), this->cnSymmClear.at(0)[1], this->cnSymmClear.at(0)[2], this->cnSymmClear.at(0)[3], static_cast<int> ( this->cnSymmClear.at(0)[0] ), static_cast<double> ( this->cnSymmClear.at(0)[4] ) );
                        
                        printf ( "\n" );
                        
                        printf ( "Symmetry elements table:\n" );
                        printf ( "-----------------------------------------------------------\n" );
                        printf ( "Symmetry          x          y          z          Angle        \n" );
                        printf ( "  Type                                             (deg)        \n" );
                        printf ( "-----------------------------------------------------------\n" );
                        
                        printf ( "   E            %+.2f      %+.2f      %+.2f        %+.1f    \n", 1.0, 0.0, 0.0, 0.0 );
                        if ( static_cast<int> ( this->cnSymmClear.at(0)[0] ) % 2 == 0 )
                        {
                            for ( int iter = -std::ceil( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ); iter++ )
                            {
                                if ( iter == 0 ) { continue; }
                                if ( iter == -std::ceil( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ) ) { continue; }
                                printf ( "   C%d           %+.2f      %+.2f      %+.2f        %+.1f    \n", static_cast<int> ( this->cnSymmClear.at(0)[0] ), this->cnSymmClear.at(0)[1], this->cnSymmClear.at(0)[2], this->cnSymmClear.at(0)[3], iter * ( 360.0 / static_cast<double> ( this->cnSymmClear.at(0)[0] ) ) );
                            }
                        }
                        else
                        {
                            for ( int iter = -std::floor( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ); iter++ )
                            {
                                if ( iter == 0 ) { continue; }
                                printf ( "   C%d           %+.2f      %+.2f      %+.2f        %+.1f    \n", static_cast<int> ( this->cnSymmClear.at(0)[0] ), this->cnSymmClear.at(0)[1], this->cnSymmClear.at(0)[2], this->cnSymmClear.at(0)[3], iter * ( 360.0 / static_cast<double> ( this->cnSymmClear.at(0)[0] ) ) );
                            }
                        }
                        printf ( "\n" );
                    }
                    else
                    {
                        printf ( "Detected no symmetry.\n\n" );
                    }
                }
            }
        }
    }
    
    //======================================== Print alternativs
    printf ( "Alternatives:\n" );
    printf ( "-----------------------------------------------------------\n" );
    printf ( "Symmetry Fold     x       y       z      Angle      Peak\n" );
    printf ( "  Type                                             height\n" );
    printf ( "-----------------------------------------------------------\n" );
    for ( unsigned int gNo = 0; gNo < static_cast<unsigned int> ( this->cnSymm.size() ); gNo++ )
    {
        printf ( "   C      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->cnSymm.at(gNo)[0] ), this->cnSymm.at(gNo)[1], this->cnSymm.at(gNo)[2], this->cnSymm.at(gNo)[3], static_cast<int> ( this->cnSymm.at(gNo)[0] ), static_cast<double> ( this->cnSymm.at(gNo)[4] ) );
    }

    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->dnSymm.size() ); iter++ )
    {
        printf ( "   D      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->dnSymm.at(iter).at(0)[0] ), this->dnSymm.at(iter).at(0)[1], this->dnSymm.at(iter).at(0)[2], this->dnSymm.at(iter).at(0)[3], static_cast<int> ( this->dnSymm.at(iter).at(0)[0] ), this->dnSymm.at(iter).at(0)[5] );
        
        for ( unsigned int mem = 1; mem < static_cast<unsigned int> ( this->dnSymm.at(iter).size() ); mem++ )
        {
            printf ( "          %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( this->dnSymm.at(iter).at(mem)[0] ), this->dnSymm.at(iter).at(mem)[1], this->dnSymm.at(iter).at(mem)[2], this->dnSymm.at(iter).at(mem)[3], static_cast<int> ( this->dnSymm.at(iter).at(mem)[0] ), this->dnSymm.at(iter).at(mem)[5] );
        }
    }

    std::cout << "-----------------------------------------------------------" << std::endl << std::endl;
    
    //======================================== Done
    return ;
    
}

/*! \brief This function prints the cleared results to the HTML file.
 
 This version of the function prints the results to the screen with regard to the distribution of average peak height for the
 symmetries, so only the most relevant symmetries are shown. It has a chance of missing a true symmetry, but this typically
 happens in cases where the settings are disruptive for the procedure or when the computation has somehow failed.
 
 \param[in] settings The ProSHADE_settings object containing all the information about the output and other things.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_symmetry::printResultsClearHTML ( ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Claim the symmetry to be the highest detected
    if ( settings->htmlReport )
    {
        //==================================== Create review section
        rvapi_add_section                     ( "ReviewSection",
                                                "Review",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                false );
        settings->htmlReportLine             += 1;
        
        std::stringstream hlpSS;
        hlpSS << "<pre>" << "Requested symmetry :                 " << "N/A" << "</pre>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ReviewSection",
                                                0,
                                                0,
                                                1,
                                                1 );
        
        hlpSS.str ( std::string ( ) );
        hlpSS << "<pre>" << "Input structure    :                 " << this->structFiles.at(0) << "</pre>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ReviewSection",
                                                1,
                                                0,
                                                1,
                                                1 );
        rvapi_flush                           ( );
        
        //==================================== Create results section
        rvapi_add_section                     ( "ResultsSection",
                                                "Results",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                true );
        settings->htmlReportLine             += 1;
    }
    
    int totTabRows                            = 0;
    
    if ( ( static_cast<unsigned int> ( this->icosSymm.size() ) > 0 ) && ( settings->htmlReport ) && ( static_cast<unsigned int> ( this->icosElems.size() ) == 0 ) )
    {
        //==================================== State result
        std::stringstream hlpSS;
        hlpSS << "<b><font color=\"orange\">" << "This appears like icosahedral symmetry, but not all axes could be detected. Maybe decreasing resolution or relaxing the peak similarity requirement would improve the detection. Proceeding this what was detected completely." << "</font></b>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ResultsSection",
                                               0,
                                               0,
                                               1,
                                               1 );
        rvapi_flush                           ( );
    }
    
    if ( ( static_cast<unsigned int> ( this->icosSymm.size() ) > 0 ) && ( settings->htmlReport ) && ( static_cast<unsigned int> ( this->icosElems.size() ) > 0 ) )
    {
        //==================================== State result
        std::stringstream hlpSS;
        hlpSS << "<b>" << "Detected <i>ICOSAHEDRAL</i> symmetry." << "</b>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                1,
                                                0,
                                                1,
                                                1 );
        rvapi_flush                           ( );
        
        //==================================== Symmetry axes table
        rvapi_add_table                       ( "SymmetryTypeTable",
                                                "Detected symmetry axes",
                                                "ResultsSection",
                                                2,
                                                0,
                                                static_cast<unsigned int> ( this->icosAxes.size() ),
                                                8,
                                                1 );
        
        // ... Column headers
        int columnIter                        = 0;
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "This column states the symmetry type (Cyclic = C; Dihedral = D; Tetrahedral = T; Octahedral = O; Icosahedral = I).";
        rvapi_put_horz_theader                ( "SymmetryTypeTable", "Symmetry type", hlpSS.str().c_str(), columnIter );
        columnIter                           += 1;
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "This column states the symmetry fold; this is only interesting for Cyclic and Dihedral symmetries.";
        rvapi_put_horz_theader                ( "SymmetryTypeTable", "Symmetry fold", hlpSS.str().c_str(), columnIter );
        columnIter                           += 1;
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "This column states the x-axis element of the symmetry axis.";
        rvapi_put_horz_theader                ( "SymmetryTypeTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
        columnIter                           += 1;
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "This column states the y-axis element of the symmetry axis.";
        rvapi_put_horz_theader                ( "SymmetryTypeTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
        columnIter                           += 1;
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "This column states the z-axis element of the symmetry axis.";
        rvapi_put_horz_theader                ( "SymmetryTypeTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
        columnIter                           += 1;
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry axis in degrees.";
        rvapi_put_horz_theader                ( "SymmetryTypeTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
        columnIter                           += 1;
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "This column states the average peak height (correlation after rotation) for all the symmetry rotations. The closer this number is to 1.0, the more reliable the symmetry existence is.";
        rvapi_put_horz_theader                ( "SymmetryTypeTable", "Peak height", hlpSS.str().c_str(), columnIter );
        columnIter                           += 1;
        
        // ... Row headers
        for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->icosAxes.size() ); icoIt++ )
        {
            std::stringstream hlpSS2;
            hlpSS2 << "Symmetry axis #" << icoIt+1;
            rvapi_put_vert_theader            ( "SymmetryTypeTable", hlpSS2.str().c_str(), "", icoIt );
        }
        
        // ... Fill in data
        int prec                              = 4;
        for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->icosAxes.size() ); icoIt++ )
        {
            std::stringstream hlpSS2;
            hlpSS2 << "C";
            rvapi_put_table_string            ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 0 );
            
            hlpSS2.str                        ( std::string ( ) );
            hlpSS2 << static_cast<int> ( this->icosAxes.at(icoIt)[0] );
            rvapi_put_table_string            ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 1 );
            
            hlpSS2.str                        ( std::string ( ) );
            hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->icosAxes.at(icoIt)[1], prec );
            rvapi_put_table_string            ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 2 );
            
            hlpSS2.str                        ( std::string ( ) );
            hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->icosAxes.at(icoIt)[2], prec );
            rvapi_put_table_string            ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 3 );
            
            hlpSS2.str                        ( std::string ( ) );
            hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->icosAxes.at(icoIt)[3], prec );
            rvapi_put_table_string            ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 4 );
            
            hlpSS2.str                        ( std::string ( ) );
            hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( ( ( 2.0 * M_PI ) / static_cast<double> ( this->icosAxes.at(icoIt)[0] ) ) * ( 180.0 / M_PI ), prec );
            rvapi_put_table_string            ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 5 );
            
            hlpSS2.str                        ( std::string ( ) );
            hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->icosAxes.at(icoIt)[4], prec );
            rvapi_put_table_string            ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 6 );
        }
        
        //==================================== Symmetry elements table
        rvapi_add_table                       ( "SymmetryElementsTable",
                                                "Detected symmetry elements",
                                                "ResultsSection",
                                                static_cast<unsigned int> ( this->icosAxes.size() ) + 2,
                                                0,
                                                static_cast<unsigned int> ( this->icosElems.size() ),
                                                7,
                                               -1 );
        totTabRows                            = static_cast<unsigned int> ( this->icosAxes.size() ) + 2 + static_cast<unsigned int> ( this->icosElems.size() );
        
        // ... Column headers
        columnIter                            = 0;
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "This column states the symmetry element type (Cyclic = C; Identity = E).";
        rvapi_put_horz_theader                ( "SymmetryElementsTable", "Symmetry element type", hlpSS.str().c_str(), columnIter );
        columnIter                           += 1;
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "This column states the symmetry element fold.";
        rvapi_put_horz_theader                ( "SymmetryElementsTable", "Symmetry element fold", hlpSS.str().c_str(), columnIter );
        columnIter                           += 1;
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "This column states the x-axis element of the symmetry element axis.";
        rvapi_put_horz_theader                ( "SymmetryElementsTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
        columnIter                           += 1;
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "This column states the y-axis element of the symmetry element axis.";
        rvapi_put_horz_theader                ( "SymmetryElementsTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
        columnIter                           += 1;
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "This column states the z-axis element of the symmetry element axis.";
        rvapi_put_horz_theader                ( "SymmetryElementsTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
        columnIter                           += 1;
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry element axis in degrees.";
        rvapi_put_horz_theader                ( "SymmetryElementsTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
        columnIter                           += 1;
        
        // ... Row headers
        for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->icosElems.size() ); icoIt++ )
        {
            std::stringstream hlpSS2;
            hlpSS2 << "Symmetry element #" << icoIt+1;
            rvapi_put_vert_theader            ( "SymmetryElementsTable", hlpSS2.str().c_str(), "", icoIt );
        }
        
        // ... Fill in data
        for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->icosElems.size() ); icoIt++ )
        {
            if ( this->icosElems.at(icoIt)[0] != 1.0 )
            {
                std::stringstream hlpSS3;
                hlpSS3 << "C";
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS3.str().c_str(), icoIt, 0 );
            }
            else
            {
                std::stringstream hlpSS3;
                hlpSS3 << "E";
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS3.str().c_str(), icoIt, 0 );
            }
            
            std::stringstream hlpSS2;
            hlpSS2 << static_cast<int> ( this->icosElems.at(icoIt)[0] );
            rvapi_put_table_string            ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 1 );
            
            hlpSS2.str                        ( std::string ( ) );
            hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->icosElems.at(icoIt)[1], prec );
            rvapi_put_table_string            ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 2 );
            
            hlpSS2.str                        ( std::string ( ) );
            hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->icosElems.at(icoIt)[2], prec );
            rvapi_put_table_string            ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 3 );
            
            hlpSS2.str                        ( std::string ( ) );
            hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->icosElems.at(icoIt)[3], prec );
            rvapi_put_table_string            ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 4 );
            
            hlpSS2.str                        ( std::string ( ) );
            hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->icosElems.at(icoIt)[4], prec );
            rvapi_put_table_string            ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 5 );
        }
        
        rvapi_flush                           ( );
    }
    else
    {
        if ( ( static_cast<unsigned int> ( this->octaSymm.size() ) > 0 ) && ( settings->htmlReport ) && ( static_cast<unsigned int> ( this->octaElems.size() ) == 0 ) )
        {
            //==================================== State result
            std::stringstream hlpSS;
            hlpSS << "<b><font color=\"orange\">" << "This appears like octahedral symmetry, but not all axes could be detected. Maybe decreasing resolution or relaxing the peak similarity requirement would improve the detection. Proceeding this what was detected completely." << "</font></b>";
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "ResultsSection",
                                                   0,
                                                   0,
                                                   1,
                                                   1 );
            rvapi_flush                           ( );
        }
        
        if ( ( static_cast<unsigned int> ( this->octaSymm.size() ) > 0 ) && ( settings->htmlReport ) )
        {
            //================================ State result
            std::stringstream hlpSS;
            hlpSS << "<b>" << "Detected <i>OCTAHEDRAL</i> symmetry." << "</b>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                1,
                                                0,
                                                1,
                                                1 );
            rvapi_flush                       ( );
            
            //================================ Symmetry axes table
            rvapi_add_table                   ( "SymmetryTypeTable",
                                                "Detected symmetry axes",
                                                "ResultsSection",
                                                2,
                                                0,
                                                static_cast<unsigned int> ( this->octaAxes.size() ),
                                                8,
                                                1 );
            
            // ... Column headers
            int columnIter                    = 0;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the symmetry type (Cyclic = C; Dihedral = D; Tetrahedral = T; Octahedral = O; Icosahedral = I).";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "Symmetry type", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the symmetry fold; this is only interesting for Cyclic and Dihedral symmetries.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "Symmetry fold", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the x-axis element of the symmetry axis.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the y-axis element of the symmetry axis.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the z-axis element of the symmetry axis.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry axis in degrees.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the average peak height (correlation after rotation) for all the symmetry rotations. The closer this number is to 1.0, the more reliable the symmetry existence is.";
            rvapi_put_horz_theader            ( "SymmetryTypeTable", "Peak height", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            // ... Row headers
            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->octaAxes.size() ); icoIt++ )
            {
                std::stringstream hlpSS2;
                hlpSS2 << "Symmetry axis #" << icoIt+1;
                rvapi_put_vert_theader        ( "SymmetryTypeTable", hlpSS2.str().c_str(), "", icoIt );
            }
            
            // ... Fill in data
            int prec                          = 4;
            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->octaAxes.size() ); icoIt++ )
            {
                std::stringstream hlpSS2;
                hlpSS2 << "C";
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 0 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << static_cast<int> ( this->octaAxes.at(icoIt)[0] );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 1 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->octaAxes.at(icoIt)[1], prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 2 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->octaAxes.at(icoIt)[2], prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 3 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->octaAxes.at(icoIt)[3], prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 4 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( ( ( 2.0 * M_PI ) / static_cast<double> ( this->octaAxes.at(icoIt)[0] ) ) * ( 180.0 / M_PI ), prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 5 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->octaAxes.at(icoIt)[4], prec );
                rvapi_put_table_string        ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 6 );
            }
            
            //================================ Symmetry elements table
            rvapi_add_table                   ( "SymmetryElementsTable",
                                                "Detected symmetry elements",
                                                "ResultsSection",
                                                static_cast<unsigned int> ( this->octaAxes.size() ) + 2,
                                                0,
                                                static_cast<unsigned int> ( this->octaElems.size() ),
                                                7,
                                               -1 );
            totTabRows                        = static_cast<unsigned int> ( this->octaAxes.size() ) + 2 + static_cast<unsigned int> ( this->octaElems.size() );
            
            // ... Column headers
            columnIter                        = 0;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the symmetry element type (Cyclic = C; Identity = E).";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "Symmetry element type", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the symmetry element fold.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "Symmetry element fold", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the x-axis element of the symmetry element axis.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the y-axis element of the symmetry element axis.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the z-axis element of the symmetry element axis.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry element axis in degrees.";
            rvapi_put_horz_theader            ( "SymmetryElementsTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
            columnIter                       += 1;
            
            // ... Row headers
            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->octaElems.size() ); icoIt++ )
            {
                std::stringstream hlpSS2;
                hlpSS2 << "Symmetry element #" << icoIt+1;
                rvapi_put_vert_theader        ( "SymmetryElementsTable", hlpSS2.str().c_str(), "", icoIt );
            }
            
            // ... Fill in data
            for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->octaElems.size() ); icoIt++ )
            {
                if ( this->octaElems.at(icoIt)[0] != 1.0 )
                {
                    std::stringstream hlpSS3;
                    hlpSS3 << "C";
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS3.str().c_str(), icoIt, 0 );
                }
                else
                {
                    std::stringstream hlpSS3;
                    hlpSS3 << "E";
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS3.str().c_str(), icoIt, 0 );
                }
                
                std::stringstream hlpSS2;
                hlpSS2 << static_cast<int> ( this->octaElems.at(icoIt)[0] );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 1 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->octaElems.at(icoIt)[1], prec );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 2 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->octaElems.at(icoIt)[2], prec );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 3 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->octaElems.at(icoIt)[3], prec );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 4 );
                
                hlpSS2.str                    ( std::string ( ) );
                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->octaElems.at(icoIt)[4], prec );
                rvapi_put_table_string        ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 5 );
            }
            
            rvapi_flush                       ( );
        }
        else
        {
            if ( ( static_cast<unsigned int> ( this->tetrSymm.size() ) > 0 ) && ( settings->htmlReport ) && ( static_cast<unsigned int> ( this->tetrElems.size() ) == 0 ) )
            {
                //==================================== State result
                std::stringstream hlpSS;
                hlpSS << "<b><font color=\"orange\">" << "This appears like tetrahedral symmetry, but not all axes could be detected. Maybe decreasing resolution or relaxing the peak similarity requirement would improve the detection. Proceeding this what was detected completely." << "</font></b>";
                rvapi_set_text                ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                0,
                                                0,
                                                1,
                                                1 );
                rvapi_flush                   ( );
            }

            
            if ( ( static_cast<unsigned int> ( this->tetrSymm.size() ) > 0 ) && ( settings->htmlReport ) && ( static_cast<unsigned int> ( this->tetrElems.size() ) > 0 ) )
            {
                //============================ State result
                std::stringstream hlpSS;
                hlpSS << "<b>" << "Detected <i>TETRAHEDRAL</i> symmetry." << "</b>";
                rvapi_set_text                ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                1,
                                                0,
                                                1,
                                                1 );
                rvapi_flush                   ( );
                
                //============================ Symmetry axes table
                rvapi_add_table               ( "SymmetryTypeTable",
                                                "Detected symmetry axes",
                                                "ResultsSection",
                                                2,
                                                0,
                                                static_cast<unsigned int> ( this->tetrAxes.size() ),
                                                8,
                                                1 );
                
                // ... Column headers
                int columnIter                = 0;
                
                hlpSS.str                     ( std::string ( ) );
                hlpSS << "This column states the symmetry type (Cyclic = C; Dihedral = D; Tetrahedral = T; Octahedral = O; Icosahedral = I).";
                rvapi_put_horz_theader        ( "SymmetryTypeTable", "Symmetry type", hlpSS.str().c_str(), columnIter );
                columnIter                   += 1;
                
                hlpSS.str                     ( std::string ( ) );
                hlpSS << "This column states the symmetry fold; this is only interesting for Cyclic and Dihedral symmetries.";
                rvapi_put_horz_theader        ( "SymmetryTypeTable", "Symmetry fold", hlpSS.str().c_str(), columnIter );
                columnIter                   += 1;
                
                hlpSS.str                     ( std::string ( ) );
                hlpSS << "This column states the x-axis element of the symmetry axis.";
                rvapi_put_horz_theader        ( "SymmetryTypeTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
                columnIter                   += 1;
                
                hlpSS.str                     ( std::string ( ) );
                hlpSS << "This column states the y-axis element of the symmetry axis.";
                rvapi_put_horz_theader        ( "SymmetryTypeTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
                columnIter                   += 1;
                
                hlpSS.str                     ( std::string ( ) );
                hlpSS << "This column states the z-axis element of the symmetry axis.";
                rvapi_put_horz_theader        ( "SymmetryTypeTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
                columnIter                   += 1;
                
                hlpSS.str                     ( std::string ( ) );
                hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry axis in degrees.";
                rvapi_put_horz_theader        ( "SymmetryTypeTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
                columnIter                   += 1;
                
                hlpSS.str                     ( std::string ( ) );
                hlpSS << "This column states the average peak height (correlation after rotation) for all the symmetry rotations. The closer this number is to 1.0, the more reliable the symmetry existence is.";
                rvapi_put_horz_theader        ( "SymmetryTypeTable", "Peak height", hlpSS.str().c_str(), columnIter );
                columnIter                   += 1;
                
                // ... Row headers
                for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->tetrAxes.size() ); icoIt++ )
                {
                    std::stringstream hlpSS2;
                    hlpSS2 << "Symmetry axis #" << icoIt+1;
                    rvapi_put_vert_theader    ( "SymmetryTypeTable", hlpSS2.str().c_str(), "", icoIt );
                }
                
                // ... Fill in data
                int prec                          = 4;
                for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->tetrAxes.size() ); icoIt++ )
                {
                    std::stringstream hlpSS2;
                    hlpSS2 << "C";
                    rvapi_put_table_string    ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 0 );
                    
                    hlpSS2.str                ( std::string ( ) );
                    hlpSS2 << static_cast<int> ( this->tetrAxes.at(icoIt)[0] );
                    rvapi_put_table_string    ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 1 );
                    
                    hlpSS2.str                ( std::string ( ) );
                    hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->tetrAxes.at(icoIt)[1], prec );
                    rvapi_put_table_string    ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 2 );
                    
                    hlpSS2.str                ( std::string ( ) );
                    hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->tetrAxes.at(icoIt)[2], prec );
                    rvapi_put_table_string    ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 3 );
                    
                    hlpSS2.str                ( std::string ( ) );
                    hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->tetrAxes.at(icoIt)[3], prec );
                    rvapi_put_table_string    ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 4 );
                    
                    hlpSS2.str                ( std::string ( ) );
                    hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( ( ( 2.0 * M_PI ) / static_cast<double> ( this->tetrAxes.at(icoIt)[0] ) ) * ( 180.0 / M_PI ), prec );
                    rvapi_put_table_string    ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 5 );
                    
                    hlpSS2.str                ( std::string ( ) );
                    hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->tetrAxes.at(icoIt)[4], prec );
                    rvapi_put_table_string    ( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 6 );
                }
                
                //============================ Symmetry elements table
                rvapi_add_table               ( "SymmetryElementsTable",
                                                "Detected symmetry elements",
                                                "ResultsSection",
                                                static_cast<unsigned int> ( this->tetrAxes.size() ) + 2,
                                                0,
                                                static_cast<unsigned int> ( this->tetrElems.size() ),
                                                7,
                                               -1 );
                totTabRows                    = static_cast<unsigned int> ( this->tetrAxes.size() ) + 2 + static_cast<unsigned int> ( this->tetrElems.size() );
                
                // ... Column headers
                columnIter                    = 0;
                
                hlpSS.str                     ( std::string ( ) );
                hlpSS << "This column states the symmetry element type (Cyclic = C; Identity = E).";
                rvapi_put_horz_theader        ( "SymmetryElementsTable", "Symmetry element type", hlpSS.str().c_str(), columnIter );
                columnIter                   += 1;
                
                hlpSS.str                     ( std::string ( ) );
                hlpSS << "This column states the symmetry element fold.";
                rvapi_put_horz_theader        ( "SymmetryElementsTable", "Symmetry element fold", hlpSS.str().c_str(), columnIter );
                columnIter                   += 1;
                
                hlpSS.str                     ( std::string ( ) );
                hlpSS << "This column states the x-axis element of the symmetry element axis.";
                rvapi_put_horz_theader        ( "SymmetryElementsTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
                columnIter                   += 1;
                
                hlpSS.str                     ( std::string ( ) );
                hlpSS << "This column states the y-axis element of the symmetry element axis.";
                rvapi_put_horz_theader        ( "SymmetryElementsTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
                columnIter                   += 1;
                
                hlpSS.str                     ( std::string ( ) );
                hlpSS << "This column states the z-axis element of the symmetry element axis.";
                rvapi_put_horz_theader        ( "SymmetryElementsTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
                columnIter                   += 1;
                
                hlpSS.str                     ( std::string ( ) );
                hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry element axis in degrees.";
                rvapi_put_horz_theader        ( "SymmetryElementsTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
                columnIter                   += 1;
                
                // ... Row headers
                for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->tetrElems.size() ); icoIt++ )
                {
                    std::stringstream hlpSS2;
                    hlpSS2 << "Symmetry element #" << icoIt+1;
                    rvapi_put_vert_theader    ( "SymmetryElementsTable", hlpSS2.str().c_str(), "", icoIt );
                }
                
                // ... Fill in data
                for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->tetrElems.size() ); icoIt++ )
                {
                    std::stringstream hlpSS3;
                    if ( this->tetrElems.at(icoIt)[0] != 1.0 )
                    {
                        std::stringstream hlpSS2;
                        hlpSS2 << "C";
                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 0 );
                    }
                    else
                    {
                        std::stringstream hlpSS2;
                        hlpSS2 << "E";
                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), icoIt, 0 );
                    }
                    
                    hlpSS3.str                ( std::string ( ) );
                    hlpSS3 << static_cast<int> ( this->tetrElems.at(icoIt)[0] );
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS3.str().c_str(), icoIt, 1 );
                    
                    hlpSS3.str                ( std::string ( ) );
                    hlpSS3 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->tetrElems.at(icoIt)[1], prec );
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS3.str().c_str(), icoIt, 2 );
                    
                    hlpSS3.str                ( std::string ( ) );
                    hlpSS3 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->tetrElems.at(icoIt)[2], prec );
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS3.str().c_str(), icoIt, 3 );
                    
                    hlpSS3.str                ( std::string ( ) );
                    hlpSS3 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->tetrElems.at(icoIt)[3], prec );
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS3.str().c_str(), icoIt, 4 );
                    
                    hlpSS3.str                ( std::string ( ) );
                    hlpSS3 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->tetrElems.at(icoIt)[4], prec );
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS3.str().c_str(), icoIt, 5 );
                }
                
                rvapi_flush                   ( );
            }
            else
            {
                if ( ( static_cast<unsigned int> ( this->dnSymmClear.size() ) > 0 ) && ( settings->htmlReport ) )
                {
                    //======================== State result
                    std::stringstream hlpSS;
                    hlpSS << "<b>" << "Detected <i>DIHEDRAL</i> symmetry." << "</b>";
                    rvapi_set_text            ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                1,
                                                0,
                                                1,
                                                1 );
                    rvapi_flush               ( );
                    
                    //======================== Symmetry axes table
                    rvapi_add_table           ( "SymmetryTypeTable",
                                                "Detected symmetry axes",
                                                "ResultsSection",
                                                2,
                                                0,
                                                3,
                                                8,
                                                1 );
                    
                    // ... Column headers
                    int columnIter            = 0;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the symmetry type (Cyclic = C; Dihedral = D; Tetrahedral = T; Octahedral = O; Icosahedral = I).";
                    rvapi_put_horz_theader    ( "SymmetryTypeTable", "Symmetry type", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the symmetry fold; this is only interesting for Cyclic and Dihedral symmetries.";
                    rvapi_put_horz_theader    ( "SymmetryTypeTable", "Symmetry fold", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the x-axis element of the symmetry axis.";
                    rvapi_put_horz_theader    ( "SymmetryTypeTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the y-axis element of the symmetry axis.";
                    rvapi_put_horz_theader    ( "SymmetryTypeTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the z-axis element of the symmetry axis.";
                    rvapi_put_horz_theader    ( "SymmetryTypeTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry axis in degrees.";
                    rvapi_put_horz_theader    ( "SymmetryTypeTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the average peak height (correlation after rotation) for all the symmetry rotations. The closer this number is to 1.0, the more reliable the symmetry existence is.";
                    rvapi_put_horz_theader    ( "SymmetryTypeTable", "Peak height", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    // ... Row headers
                    for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->dnSymmClear.at(0).size() ); icoIt++ )
                    {
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << "Symmetry axis #" << icoIt+1;
                        rvapi_put_vert_theader( "SymmetryTypeTable", hlpSS.str().c_str(), "", icoIt );
                    }
                    
                    // ... Fill in data
                    int prec                  = 4;
                    for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( this->dnSymmClear.at(0).size() ); icoIt++ )
                    {
                        std::stringstream hlpSS2;
                        hlpSS2 << "C";
                        rvapi_put_table_string( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 0 );
                        
                        hlpSS2.str            ( std::string ( ) );
                        hlpSS2 << static_cast<int> ( this->dnSymmClear.at(0).at(icoIt)[0] );
                        rvapi_put_table_string( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 1 );
                        
                        hlpSS2.str            ( std::string ( ) );
                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymmClear.at(0).at(icoIt)[1], prec );
                        rvapi_put_table_string( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 2 );
                        
                        hlpSS2.str            ( std::string ( ) );
                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymmClear.at(0).at(icoIt)[2], prec );
                        rvapi_put_table_string( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 3 );
                        
                        hlpSS2.str            ( std::string ( ) );
                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymmClear.at(0).at(icoIt)[3], prec );
                        rvapi_put_table_string( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 4 );
                        
                        hlpSS2.str            ( std::string ( ) );
                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( ( ( 2.0 * M_PI ) / static_cast<double> ( this->dnSymmClear.at(0).at(icoIt)[0] ) ) * ( 180.0 / M_PI ), prec );
                        rvapi_put_table_string( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 5 );
                        
                        hlpSS2.str            ( std::string ( ) );
                        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymmClear.at(0).at(icoIt)[4], prec );
                        rvapi_put_table_string( "SymmetryTypeTable", hlpSS2.str().c_str(), icoIt, 6 );
                    }
                    
                    //======================== Symmetry elements table
                    int totRows               = 0;
                    for ( unsigned int it = 0; it < 2; it++ )
                    {
                        if ( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) % 2 == 0 )
                        {
                            for ( int iter = -std::ceil( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ); iter++ )
                            {
                                if ( iter == 0 ) { continue; }
                                if ( iter == -std::ceil( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ) ) { continue; }
                                
                                totRows      += 1;
                            }
                        }
                        else
                        {
                            for ( int iter = -std::floor( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ); iter++ )
                            {
                                if ( iter == 0 ) { continue; }
                                
                                totRows      += 1;
                            }
                        }
                    }
                    totRows                  += 1;
                    totTabRows                = static_cast<unsigned int> ( totRows ) + 6;
                    
                    rvapi_add_table           ( "SymmetryElementsTable",
                                                "Detected symmetry elements",
                                                "ResultsSection",
                                                6,
                                                0,
                                                totRows,
                                                7,
                                                -1 );
                    
                    // ... Column headers
                    columnIter                = 0;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the symmetry element type (Cyclic = C; Identity = E).";
                    rvapi_put_horz_theader    ( "SymmetryElementsTable", "Symmetry element type", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the symmetry element fold.";
                    rvapi_put_horz_theader    ( "SymmetryElementsTable", "Symmetry element fold", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the x-axis element of the symmetry element axis.";
                    rvapi_put_horz_theader    ( "SymmetryElementsTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the y-axis element of the symmetry element axis.";
                    rvapi_put_horz_theader    ( "SymmetryElementsTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the z-axis element of the symmetry element axis.";
                    rvapi_put_horz_theader    ( "SymmetryElementsTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry element axis in degrees.";
                    rvapi_put_horz_theader    ( "SymmetryElementsTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
                    columnIter               += 1;
                    
                    // ... Row headers
                    for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( totRows ); icoIt++ )
                    {
                        std::stringstream hlpSS2;
                        hlpSS2 << "Symmetry element #" << icoIt+1;
                        rvapi_put_vert_theader( "SymmetryElementsTable", hlpSS2.str().c_str(), "", icoIt );
                    }
                    
                    // ... Fill in data
                    int rowCount              = 1;

                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << "E";
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 0 );
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << static_cast<int> ( 1 );
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 1 );
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << std::setprecision ( prec ) << std::showpos << 1;
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 2 );
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << std::setprecision ( prec ) << std::showpos << 0;
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 3 );
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << std::setprecision ( prec ) << std::showpos << 0;
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 4 );
                    
                    hlpSS.str                 ( std::string ( ) );
                    hlpSS << std::setprecision ( prec ) << std::showpos << 0;
                    rvapi_put_table_string    ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 5 );
                        
                    for ( unsigned int it = 0; it < 2; it++ )
                    {
                        if ( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) % 2 == 0 )
                        {
                            for ( int iter = -std::ceil( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ); iter++ )
                            {
                                if ( iter == 0 ) { continue; }
                                if ( iter == -std::ceil( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ) ) { continue; }
                                
                                std::stringstream hlpSS2;
                                hlpSS2 << "C";
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 0 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 1 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymmClear.at(0).at(it)[1], prec );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 2 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymmClear.at(0).at(it)[2], prec );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 3 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymmClear.at(0).at(it)[3], prec );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 4 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( iter * ( 360.0 / static_cast<double> ( this->dnSymmClear.at(0).at(it)[0] ) ), prec );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 5 );
                                rowCount += 1;
                            }
                        }
                        else
                        {
                            for ( int iter = -std::floor( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ); iter++ )
                            {
                                if ( iter == 0 ) { continue; }
                                
                                std::stringstream hlpSS2;
                                hlpSS2 << "C";
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 0 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 1 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymmClear.at(0).at(it)[1], prec );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 2 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymmClear.at(0).at(it)[2], prec );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 3 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymmClear.at(0).at(it)[3], prec );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 4 );
                                
                                hlpSS2.str    ( std::string ( ) );
                                hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( iter * ( 360.0 / static_cast<double> ( this->dnSymmClear.at(0).at(it)[0] ) ), prec );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS2.str().c_str(), rowCount, 5 );
                                rowCount += 1;
                            }
                        }
                    }
                    
                    rvapi_flush               ( );
                }
                else
                {
                    if ( ( static_cast<unsigned int> ( this->cnSymmClear.size() ) > 0 ) && ( settings->htmlReport ) )
                    {
                        //==================== State result
                        std::stringstream hlpSS;
                        hlpSS << "<b>" << "Detected <i>CYCLIC</i> symmetry." << "</b>";
                        rvapi_set_text        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                1,
                                                0,
                                                1,
                                                1 );
                        rvapi_flush           ( );
                        
                        //==================== Symmetry axes table
                        rvapi_add_table       ( "SymmetryTypeTable",
                                                "Detected symmetry axes",
                                                "ResultsSection",
                                                2,
                                                0,
                                                3,
                                                8,
                                                2 );
                        
                        // ... Column headers
                        int columnIter        = 0;
                        
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << "This column states the symmetry type (Cyclic = C; Dihedral = D; Tetrahedral = T; Octahedral = O; Icosahedral = I).";
                        rvapi_put_horz_theader( "SymmetryTypeTable", "Symmetry type", hlpSS.str().c_str(), columnIter );
                        columnIter           += 1;
                        
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << "This column states the symmetry fold; this is only interesting for Cyclic and Dihedral symmetries.";
                        rvapi_put_horz_theader( "SymmetryTypeTable", "Symmetry fold", hlpSS.str().c_str(), columnIter );
                        columnIter           += 1;
                        
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << "This column states the x-axis element of the symmetry axis.";
                        rvapi_put_horz_theader( "SymmetryTypeTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
                        columnIter           += 1;
                        
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << "This column states the y-axis element of the symmetry axis.";
                        rvapi_put_horz_theader( "SymmetryTypeTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
                        columnIter           += 1;
                    
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << "This column states the z-axis element of the symmetry axis.";
                        rvapi_put_horz_theader( "SymmetryTypeTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
                        columnIter           += 1;
                        
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry axis in degrees.";
                        rvapi_put_horz_theader( "SymmetryTypeTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
                        columnIter           += 1;
                        
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << "This column states the average peak height (correlation after rotation) for all the symmetry rotations. The closer this number is to 1.0, the more reliable the symmetry existence is.";
                        rvapi_put_horz_theader( "SymmetryTypeTable", "Peak height", hlpSS.str().c_str(), columnIter );
                        columnIter           += 1;
                        
                        // ... Row headers
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << "Symmetry axis #" << 1;
                        rvapi_put_vert_theader( "SymmetryTypeTable", hlpSS.str().c_str(), "", 0 );
                        
                        // ... Fill in data
                        int prec              = 4;
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << "C";
                        rvapi_put_table_string ( "SymmetryTypeTable", hlpSS.str().c_str(), 0, 0 );
                        
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << static_cast<int> ( this->cnSymmClear.at(0)[0] );
                        rvapi_put_table_string ( "SymmetryTypeTable", hlpSS.str().c_str(), 0, 1 );
                        
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymmClear.at(0)[1], prec );
                        rvapi_put_table_string ( "SymmetryTypeTable", hlpSS.str().c_str(), 0, 2 );
                        
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymmClear.at(0)[2], prec );
                        rvapi_put_table_string ( "SymmetryTypeTable", hlpSS.str().c_str(), 0, 3 );
                        
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymmClear.at(0)[3], prec );
                        rvapi_put_table_string ( "SymmetryTypeTable", hlpSS.str().c_str(), 0, 4 );
                        
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( ( ( 2.0 * M_PI ) / static_cast<double> ( this->cnSymmClear.at(0)[0] ) ) * ( 180.0 / M_PI ), prec );
                        rvapi_put_table_string ( "SymmetryTypeTable", hlpSS.str().c_str(), 0, 5 );
                        
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymmClear.at(0)[4], prec );
                        rvapi_put_table_string ( "SymmetryTypeTable", hlpSS.str().c_str(), 0, 6 );
                        
                        //==================== Symmetry elements table
                        int totRows           = 0;
                        if ( static_cast<int> ( this->cnSymmClear.at(0)[0] ) % 2 == 0 )
                        {
                            for ( int iter = -std::ceil( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ); iter++ )
                            {
                                if ( iter == 0 ) { continue; }
                                if ( iter == -std::ceil( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ) ) { continue; }
                                
                                totRows      += 1;
                            }
                        }
                        else
                        {
                            for ( int iter = -std::floor( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ); iter++ )
                            {
                                if ( iter == 0 ) { continue; }
                                
                                totRows      += 1;
                            }
                        }
                        totRows              += 1;
                        totTabRows            = static_cast<unsigned int> ( totRows ) + 5;
                        
                        rvapi_add_table       ( "SymmetryElementsTable",
                                                "Detected symmetry elements",
                                                "ResultsSection",
                                                5,
                                                0,
                                                totRows,
                                                7,
                                                -1 );
                        
                        // ... Column headers
                        columnIter            = 0;
                        
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << "This column states the symmetry element type (Cyclic = C; Identity = E).";
                        rvapi_put_horz_theader( "SymmetryElementsTable", "Symmetry element type", hlpSS.str().c_str(), columnIter );
                        columnIter           += 1;
                        
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << "This column states the symmetry element fold.";
                        rvapi_put_horz_theader( "SymmetryElementsTable", "Symmetry element fold", hlpSS.str().c_str(), columnIter );
                        columnIter           += 1;
                        
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << "This column states the x-axis element of the symmetry element axis.";
                        rvapi_put_horz_theader( "SymmetryElementsTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
                        columnIter           += 1;
                        
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << "This column states the y-axis element of the symmetry element axis.";
                        rvapi_put_horz_theader( "SymmetryElementsTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
                        columnIter           += 1;
                        
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << "This column states the z-axis element of the symmetry element axis.";
                        rvapi_put_horz_theader( "SymmetryElementsTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
                        columnIter           += 1;
                        
                        hlpSS.str             ( std::string ( ) );
                        hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry element axis in degrees.";
                        rvapi_put_horz_theader( "SymmetryElementsTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
                        columnIter           += 1;
                        
                        // ... Row headers
                        for ( unsigned int icoIt = 0; icoIt < static_cast<unsigned int> ( totRows ); icoIt++ )
                        {
                            std::stringstream hlpSS2;
                            hlpSS2 << "Symmetry element #" << std::to_string ( icoIt+1 );
                            rvapi_put_vert_theader( "SymmetryElementsTable", hlpSS2.str().c_str(), "", icoIt );
                        }

                        // ... Fill in data
                        int rowCount              = 1;

                        hlpSS.str             ( std::string ( ) );
                        hlpSS << "E";
                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 0 );

                        hlpSS.str             ( std::string ( ) );
                        hlpSS << static_cast<int> ( 1 );
                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 1 );

                        hlpSS.str             ( std::string ( ) );
                        hlpSS << std::setprecision ( prec ) << std::showpos << 1;
                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 2 );

                        hlpSS.str             ( std::string ( ) );
                        hlpSS << std::setprecision ( prec ) << std::showpos << 0;
                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 3 );

                        hlpSS.str             ( std::string ( ) );
                        hlpSS << std::setprecision ( prec ) << std::showpos << 0;
                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 4 );

                        hlpSS.str             ( std::string ( ) );
                        hlpSS << std::setprecision ( prec ) << std::showpos << 0;
                        rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), 0, 5 );

                        if ( static_cast<int> ( this->cnSymmClear.at(0)[0] ) % 2 == 0 )
                        {
                            for ( int iter = -std::ceil( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ); iter++ )
                            {
                                if ( iter == 0 ) { continue; }
                                if ( iter == -std::ceil( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ) ) { continue; }

                                hlpSS.str     ( std::string ( ) );
                                hlpSS << "C";
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 0 );

                                hlpSS.str     ( std::string ( ) );
                                hlpSS << static_cast<int> ( this->cnSymmClear.at(0)[0] );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 1 );

                                hlpSS.str     ( std::string ( ) );
                                hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymmClear.at(0)[1], prec );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 2 );

                                hlpSS.str     ( std::string ( ) );
                                hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymmClear.at(0)[2], prec );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 3 );

                                hlpSS.str     ( std::string ( ) );
                                hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymmClear.at(0)[3], prec );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 4 );

                                hlpSS.str     ( std::string ( ) );
                                hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( iter * ( 360.0 / static_cast<double> ( this->cnSymmClear.at(0)[0] ) ), prec );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 5 );
                                rowCount += 1;
                            }
                        }
                        else
                        {
                            for ( int iter = -std::floor( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ); iter++ )
                            {
                                if ( iter == 0 ) { continue; }

                                hlpSS.str     ( std::string ( ) );
                                hlpSS << "C";
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 0 );

                                hlpSS.str     ( std::string ( ) );
                                hlpSS << static_cast<int> ( this->cnSymmClear.at(0)[0] );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 1 );

                                hlpSS.str     ( std::string ( ) );
                                hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymmClear.at(0)[1], prec );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 2 );

                                hlpSS.str     ( std::string ( ) );
                                hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymmClear.at(0)[2], prec );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 3 );

                                hlpSS.str     ( std::string ( ) );
                                hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymmClear.at(0)[3], prec );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 4 );

                                hlpSS.str     ( std::string ( ) );
                                hlpSS << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( iter * ( 360.0 / static_cast<double> ( this->cnSymmClear.at(0)[0] ) ), prec );
                                rvapi_put_table_string ( "SymmetryElementsTable", hlpSS.str().c_str(), rowCount, 5 );
                                rowCount += 1;
                            }
                        }

                        rvapi_flush               ( );
                    }
                    else
                    {
                        printf ( "Detected no symmetry.\n\n" );
                    }
                }
            }
        }
    }
    
    //======================================== Print alternatives    
    rvapi_add_table                           ( "AlternativesTable",
                                                "Alternative Symmetries Detected",
                                                "ResultsSection",
                                                totTabRows,
                                                0,
                                                static_cast<unsigned int> ( this->cnSymm.size() ) + static_cast<unsigned int> ( this->dnSymm.size() ),
                                                7,
                                                -1 );
    
    // ... Column headers
    int columnIter                            = 0;
    
    std::stringstream hlpSS;
    hlpSS << "This column states the symmetry type (Cyclic = C; Dihedral = D).";
    rvapi_put_horz_theader                    ( "AlternativesTable", "Symmetry element type", hlpSS.str().c_str(), columnIter );
    columnIter                               += 1;
    
    hlpSS.str                                 ( std::string ( ) );
    hlpSS << "This column states the symmetry fold.";
    rvapi_put_horz_theader                    ( "AlternativesTable", "Symmetry element fold", hlpSS.str().c_str(), columnIter );
    columnIter                               += 1;
    
    hlpSS.str                                 ( std::string ( ) );
    hlpSS << "This column states the x-axis element of the symmetry axis.";
    rvapi_put_horz_theader                    ( "AlternativesTable", "<i>X</i>-axis element", hlpSS.str().c_str(), columnIter );
    columnIter                               += 1;
    
    hlpSS.str                                 ( std::string ( ) );
    hlpSS << "This column states the y-axis element of the symmetry axis.";
    rvapi_put_horz_theader                    ( "AlternativesTable", "<i>Y</i>-axis element", hlpSS.str().c_str(), columnIter );
    columnIter                               += 1;
    
    hlpSS.str                                 ( std::string ( ) );
    hlpSS << "This column states the z-axis element of the symmetry axis.";
    rvapi_put_horz_theader                    ( "AlternativesTable", "<i>Z</i>-axis element", hlpSS.str().c_str(), columnIter );
    columnIter                               += 1;
    
    hlpSS.str                                 ( std::string ( ) );
    hlpSS << "This column states the angle (in the sense of the angle-axis representation) of the symmetry axis in degrees.";
    rvapi_put_horz_theader                    ( "AlternativesTable", "Angle (degrees)", hlpSS.str().c_str(), columnIter );
    columnIter                               += 1;
    
    hlpSS.str                                 ( std::string ( ) );
    hlpSS << "This column states the average peak height (correlation after rotation) for all the symmetry rotations. The closer this number is to 1.0, the more reliable the symmetry existence is.";
    rvapi_put_horz_theader                    ( "AlternativesTable", "Peak Height", hlpSS.str().c_str(), columnIter );
    columnIter                               += 1;
    
    // ... Row headers
    int maxCAlts                              = 0;
    for ( unsigned int gNo = 0; gNo < static_cast<unsigned int> ( this->cnSymm.size() ); gNo++ )
    {
        std::stringstream hlpSS2;
        hlpSS2 << "Alternative Symmetry #" << gNo+1;
        rvapi_put_vert_theader                ( "AlternativesTable", hlpSS2.str().c_str(), "Reported symmetry alternative (i.e. also detected, but with lower reliability)", gNo );
        maxCAlts                              = gNo;
    }
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->dnSymm.size() ); iter++ )
    {
        std::stringstream hlpSS2;
        hlpSS2 << "Alternative Symmetry #" << iter + maxCAlts + 2;
        rvapi_put_vert_theader                ( "AlternativesTable", hlpSS2.str().c_str(), "Reported symmetry alternative (i.e. also detected, but with lower reliability)", iter + maxCAlts + 1 );
    }
    
    int rowCount                              = 0;
    int prec                                  = 4;
    for ( unsigned int gNo = 0; gNo < static_cast<unsigned int> ( this->cnSymm.size() ); gNo++ )
    {
        std::stringstream hlpSS2;
        hlpSS2 << "C";
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 0 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << static_cast<int> ( static_cast<int> ( this->cnSymm.at(gNo)[0] ) );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 1 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymm.at(gNo)[1], prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 2 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymm.at(gNo)[2], prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 3 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->cnSymm.at(gNo)[3], prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 4 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( ( ( 2.0 * M_PI ) / static_cast<double> ( this->cnSymm.at(gNo)[0] ) ) * ( 180.0 / M_PI ), prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 5 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( static_cast<double> ( this->cnSymm.at(gNo)[4] ), prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 6 );
        rowCount                             += 1;
    }
    
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->dnSymm.size() ); iter++ )
    {
        std::stringstream hlpSS2;
        hlpSS2 << "D";
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 0 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << static_cast<int> ( static_cast<int> ( this->dnSymm.at(iter).at(0)[0] ) );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 1 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(iter).at(0)[1], prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 2 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(iter).at(0)[2], prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 3 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( this->dnSymm.at(iter).at(0)[3], prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 4 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( ( ( 2.0 * M_PI ) / static_cast<double> ( this->dnSymm.at(iter).at(0)[0] ) ) * ( 180.0 / M_PI ), prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 5 );
        
        hlpSS2.str                            ( std::string ( ) );
        hlpSS2 << std::setprecision ( prec ) << std::showpos << ProSHADE_internal_misc::to_string_with_precision ( static_cast<double> ( this->dnSymm.at(iter).at(0)[4] ), prec );
        rvapi_put_table_string                ( "AlternativesTable", hlpSS2.str().c_str(), rowCount, 6 );
        rowCount                             += 1;
    }
    rvapi_flush                               ( );
    
    //======================================== Done
    return ;
    
}

/*! \brief This function gives the user programmatical access to the symmetry peaks of the symmetry detection procedure.
 
 This function gives a programmatical access to the results of the symmetry detection procedure peaks. Each peak reported in the output
 is where the overlay function of the object has value higher than the background as detected by the procedure. The output format is as
 follows:
 
 Each vector entry is a distinct peak with its features encoded accordingly:
    [0]      Euler Angle alpha value
    [1]      Euler Angle beta value
    [2]      Euler Angle gamma value
    [3]      Peak height
    [4]      X-axis element of the axis for Angle-Axis representation
    [5]      Y-axis element of the axis for Angle-Axis representation
    [6]      Z-axis element of the axis for Angle-Axis representation
    [7]      Angle in radians for Angle-Axis representation
 
 \param[out] X Vector of peaks and their features.
 
 \warning The results are reported in double precision.
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::array<double,8> > ProSHADE_internal::ProSHADE_symmetry::getRotFnPeaks ( )
{
    //======================================== Return
    return ( this->rfPeaks );
    
}

/*! \brief This function gives the user programmatical access to the detected C-symmetries.
 
 This function gives a programmatical access to the results of the symmetry detection procedure C-symmetries list. This is a list of
 all detected C symmetries and is encoded as follows:
 
 Each vector entry is a distinct C symmetry with its features encoded accordingly:
 [0]      The fold of the symmetry
 [1]      X-axis element of the axis for Angle-Axis representation
 [2]      Y-axis element of the axis for Angle-Axis representation
 [3]      Z-axis element of the axis for Angle-Axis representation
 [4]      Average peak height for the symmetry
 
 \param[out] X Vector of C-symmetries and their features.
 
 \warning The results are reported in double precision.
 */
std::vector< std::array<double,5> > ProSHADE_internal::ProSHADE_symmetry::getCSymmetries ( )
{
    //======================================== Return
    return ( this->cnSymm );
    
}

/*! \brief This function gives the user programmatical access to the detected D-symmetries.
 
 This function gives a programmatical access to the results of the symmetry detection procedure D-symmetries list. This is a list of
 all detected D(ihedral) symmetries and is encoded as follows:
 
 Each vector entry is a distinct D symmetry with its features encoded accordingly:
 [0]      The fold of the symmetry
 [1]      X-axis element average of the axis for Angle-Axis representation
 [2]      Y-axis element average of the axis for Angle-Axis representation
 [3]      Z-axis element average of the axis for Angle-Axis representation
 [4]      <do not use>
 [5]      Average peak height for the symmetry
 
 \param[out] X Vector of D-symmetries and their features.
 
 \warning The results are reported in double precision.
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::vector< std::array<double,6> > > ProSHADE_internal::ProSHADE_symmetry::getDSymmetries ( )
{
    //======================================== Return
    return ( this->dnSymm );
    
}

/*! \brief This is the constructor for the ProSHADE_distances executive class the user should use to compute and access distances between any number of structures.
 
 This constructor takes a list of structures and the settings required by the user and then proceeds to use the distances computation logic
 implemented here to obtain all required distances between the first structure and all the rest. All the computations are done as the object
 is being constructed, this may take some time, but all the results are then available immediately without any forther computations. This is
 the object user should use to obtain the shape distances.
 
 \param[in] settings Instance of the ProSHADE_settings class containing all the settings values.
 
 \warning This is an internal function which should not be used by the user.
 */
ProSHADE_internal::ProSHADE_distances::ProSHADE_distances ( ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Initialise pointers
    this->cmpObj                              = nullptr;
    
    //======================================== If this is called only to save database, redirect to the proper function and quit
    if ( settings->taskToPerform == ProSHADE::BuildDB )
    {
        //==================================== Report progress
        if ( settings->verbose > 0 )
        {
            std::cout << "-----------------------------------------------------------" << std::endl;
            std::cout << "|                        MODE: BuildDB                    |" << std::endl;
            std::cout << "-----------------------------------------------------------" << std::endl << std::endl;
        }
        
        this->saveDatabase                    ( settings );
        
        if ( settings->verbose > 0 )
        {
            std::cout << std::endl << "-----------------------------------------------------------" << std::endl;
            std::cout << "|                         COMPLETED                       |" << std::endl;
            std::cout << "-----------------------------------------------------------" << std::endl << std::endl;
            
            std::cout << "Database saved to: " << settings->databaseName << std::endl << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"green\">" << "Database saved to " << settings->databaseName << " ." << "</font>";
                rvapi_set_text                ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
                settings->htmlReportLineProgress += 1;
                
                rvapi_flush                   ( );
            }
        }
        return ;
    }

    //======================================== If this is called to fragment and compare one structure against database, redirect to the proper function and quit
    if ( ( settings->databaseName != "" ) && ( settings->structFiles.size() == 1 ) && ( settings->taskToPerform == ProSHADE::DistancesFrag ) )
    {
        //==================================== Load the database and compare
        compareFragAgainstDatabase ( settings, &settings->structFiles );
        
        //==================================== Save the settings
        this->mapResolution                   = settings->mapResolution;
        this->bandwidth                       = settings->bandwidth;
        this->glIntegOrder                    = settings->glIntegOrder;
        this->theta                           = settings->theta;
        this->phi                             = settings->phi;
        this->bFactorValue                    = settings->bFactorValue;
        this->bFactorChange                   = settings->bFactorChange;
        this->noIQRsFromMap                   = settings->noIQRsFromMap;
        this->shellSpacing                    = settings->shellSpacing;
        this->manualShells                    = settings->manualShells;
        this->useCOM                          = settings->useCOM;
        this->firstLineCOM                    = settings->firstLineCOM;
        this->extraSpace                      = settings->extraSpace;
        this->alpha                           = settings->alpha;
        this->mPower                          = settings->mPower;
        this->ignoreLs                        = settings->ignoreLs;
        this->energyLevelDist                 = settings->energyLevelDist;
        this->traceSigmaDist                  = settings->traceSigmaDist;
        this->fullRotFnDist                   = settings->fullRotFnDist;
        this->usePhase                        = settings->usePhase;
        this->saveWithAndWithout              = settings->saveWithAndWithout;
        this->enLevelsThreshold               = settings->enLevelsThreshold;
        this->trSigmaThreshold                = settings->trSigmaThreshold;
        this->structFiles                     = settings->structFiles;
        
        //==================================== Done
        return ;
    }
    
    //======================================== If this is called to compare one structure against database, redirect to the proper function and quit
    if ( ( settings->databaseName != "" ) && ( settings->structFiles.size() == 1 ) )
    {
        //==================================== Load the database and compare, keeping the task value unchanged by the database reading
        ProSHADE::Task origTask               = settings->taskToPerform;
        compareAgainstDatabase ( settings, &settings->structFiles );
        
        settings->taskToPerform               = origTask;
        
        //==================================== Save the settings
        this->mapResolution                   = settings->mapResolution;
        this->bandwidth                       = settings->bandwidth;
        this->glIntegOrder                    = settings->glIntegOrder;
        this->theta                           = settings->theta;
        this->phi                             = settings->phi;
        this->bFactorValue                    = settings->bFactorValue;
        this->bFactorChange                   = settings->bFactorChange;
        this->noIQRsFromMap                   = settings->noIQRsFromMap;
        this->shellSpacing                    = settings->shellSpacing;
        this->manualShells                    = settings->manualShells;
        this->useCOM                          = settings->useCOM;
        this->firstLineCOM                    = settings->firstLineCOM;
        this->extraSpace                      = settings->extraSpace;
        this->alpha                           = settings->alpha;
        this->mPower                          = settings->mPower;
        this->ignoreLs                        = settings->ignoreLs;
        this->energyLevelDist                 = settings->energyLevelDist;
        this->traceSigmaDist                  = settings->traceSigmaDist;
        this->fullRotFnDist                   = settings->fullRotFnDist;
        this->usePhase                        = settings->usePhase;
        this->saveWithAndWithout              = settings->saveWithAndWithout;
        this->enLevelsThreshold               = settings->enLevelsThreshold;
        this->trSigmaThreshold                = settings->trSigmaThreshold;
        this->structFiles                     = settings->structFiles;
        
        //==================================== Done
        return ;
    }
    
    //======================================== Check for ambiguity
    if ( ( settings->databaseName != "" ) && ( settings->structFiles.size() > 1 ) )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Ambiguity detected. There is a database name supplied, suggesting that you want to compare something against it. However, there is also multiple files supplied, suggesting you want to compare them against each other. Note, that to compare to databse, only a single file can be supplied - this is to remove this ambiguity. Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            //==================================== Record progress
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Ambiguity detected. There is a database name supplied, suggesting that you want to compare something against it. However, there is also multiple files supplied, suggesting you want to compare them against each other. Please note that to compare to databse, only a single file can be supplied - this is to remove this ambiguity." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Save the settings
    this->mapResolution                       = settings->mapResolution;
    this->bandwidth                           = settings->bandwidth;
    this->glIntegOrder                        = settings->glIntegOrder;
    this->theta                               = settings->theta;
    this->phi                                 = settings->phi;
    this->bFactorValue                        = settings->bFactorValue;
    this->bFactorChange                       = settings->bFactorChange;
    this->noIQRsFromMap                       = settings->noIQRsFromMap;
    this->shellSpacing                        = settings->shellSpacing;
    this->manualShells                        = settings->manualShells;
    this->useCOM                              = settings->useCOM;
    this->firstLineCOM                        = settings->firstLineCOM;
    this->extraSpace                          = settings->extraSpace;
    this->alpha                               = settings->alpha;
    this->mPower                              = settings->mPower;
    this->ignoreLs                            = settings->ignoreLs;
    this->energyLevelDist                     = settings->energyLevelDist;
    this->traceSigmaDist                      = settings->traceSigmaDist;
    this->fullRotFnDist                       = settings->fullRotFnDist;
    this->usePhase                            = settings->usePhase;
    this->saveWithAndWithout                  = settings->saveWithAndWithout;
    this->enLevelsThreshold                   = settings->enLevelsThreshold;
    this->trSigmaThreshold                    = settings->trSigmaThreshold;
    this->structFiles                         = settings->structFiles;
    
    //======================================== Sanity checks
    if ( !this->energyLevelDist && !this->traceSigmaDist && !this->fullRotFnDist )
    {
        //==================================== No distances required
        std::cerr << "!!! ProSHADE ERROR !!! Initialising ProSHADE_distances object with no distances required! Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            //==================================== Record progress
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "There are no required distances to be computed and therefore nothing to do..." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    if ( this->structFiles.size() < 2 )
    {
        //==================================== Not enough structures
        std::cerr << "!!! ProSHADE ERROR !!! There are less than two structures submitted to the ProSHADE_distances class, cannot compute distances! Did you forget to supply the database file name? Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            //==================================== Record progress
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "There are less than two structures submitted to the distance computing functionality." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    if ( ( this->enLevelsThreshold != -999.9 ) && ( !this->energyLevelDist ) )
    {
        //==================================== Invalid settings
        std::cerr << "!!! ProSHADE ERROR!!! Energy levels descriptor is not required, but its threshold for hierarchical distance computation was set. This makes no sense! Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            //==================================== Record progress
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Energy levels descriptor is not required, but its threshold for hierarchical distance computation was set. Either you set these values, in which case please decide what should be done, or you did not, in which case this is an internal bug and in this case please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    if ( ( this->trSigmaThreshold != -999.9 ) && ( !this->traceSigmaDist ) )
    {
        //==================================== Invalid settings
        std::cerr << "!!! ProSHADE ERROR !!! Trace sigma descriptor is not required, but its threshold for hierarchical distance computation was set. This makes no sense! Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            //==================================== Record progress
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Trace sigma descriptor is not required, but its threshold for hierarchical distance computation was set. Either you set these values, in which case please decide what should be done, or you did not, in which case this is an internal bug and in this case please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    if ( settings->verbose > 0 )
    {
        std::cout << "Finding distances between the structure " << settings->structFiles.at(0) << " against all other structures." << std::endl;
    }
    
    if ( settings->verbose > 3 )
    {
        std::cout << ">>>>>>>> Sanity checks passed." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        //==================================== Record progress
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Sanity checks passed" << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    if ( settings->htmlReport )
    {
        //==================================== Create section
        rvapi_add_section                     ( "InputFilesSection",
                                                "List of input structures",
                                                "body",
                                                settings->htmlReportLine-1,
                                                0,
                                                1,
                                                1,
                                                false );
        settings->htmlReportLine             += 1;
        rvapi_flush();
        
        // Record structure 0
        std::stringstream hlpSS;
        hlpSS << "<b>" << settings->structFiles.at(0) << "</b>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "InputFilesSection",
                                                0,
                                                1,
                                                1,
                                                1 );
        
        for ( unsigned int vecIt = 1; vecIt < static_cast<unsigned int> ( this->structFiles.size() ); vecIt++ )
        {
            hlpSS.str                         ( std::string ( ) );
            hlpSS << settings->structFiles.at(vecIt);
            rvapi_set_text                        ( hlpSS.str().c_str(),
                                                   "InputFilesSection",
                                                   vecIt,
                                                   1,
                                                   1,
                                                   1 );
        }
        
        
        rvapi_flush                           ( );
    }
    
    //======================================== Initialise vectors
    this->bandwidthVec                        = std::vector<unsigned int> ( this->structFiles.size(), this->bandwidth );
    this->thetaVec                            = std::vector<unsigned int> ( this->structFiles.size(), this->theta );
    this->phiVec                              = std::vector<unsigned int> ( this->structFiles.size(), this->phi );
    this->glIntegOrderVec                     = std::vector<unsigned int> ( this->structFiles.size(), this->glIntegOrder );
    this->extraSpaceVec                       = std::vector<double>       ( this->structFiles.size(), this->extraSpace );
 
    //======================================== Create the one object to compare everything else against
    ProSHADE_data* one                        = new ProSHADE_data ();
    
    //======================================== Read in the structure into one
    unsigned int fileType                     = checkFileType ( structFiles.at(0) );
    if ( fileType == 2 )
    {
        one->getDensityMapFromMAP             ( this->structFiles.at(0),
                                               &this->shellSpacing,
                                               this->mapResolution,
                                               &this->bandwidthVec.at(0),
                                               &this->thetaVec.at(0),
                                               &this->phiVec.at(0),
                                               &this->glIntegOrderVec.at(0),
                                               &this->extraSpaceVec.at(0),
                                               settings->mapResDefault,
                                               settings->rotChangeDefault,
                                               settings,
                                               settings->overlayDefaults );
    }
    else if ( fileType == 1 )
    {
        one->getDensityMapFromPDB             ( this->structFiles.at(0),
                                               &this->shellSpacing,
                                               this->mapResolution,
                                               &this->bandwidthVec.at(0),
                                               &this->thetaVec.at(0),
                                               &this->phiVec.at(0),
                                               &this->glIntegOrderVec.at(0),
                                               &this->extraSpaceVec.at(0),
                                               settings->mapResDefault,
                                               settings,
                                               this->bFactorValue,
                                               this->firstLineCOM );
    }
    else
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error loading file " << this->structFiles.at(0) << " !!! Cannot detect the extension (currently, only PDB or MAP are allowed) and therefore cannot read the file." << std::endl;
        exit ( -1 );
    }
    if ( settings->verbose > 1 )
    {
        std::cout << ">> Structure " << this->structFiles.at(0) << " loaded." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        //==================================== Record progress
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Structure " << this->structFiles.at(0) << " loaded." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Deal with the centering and MAP data format, if applicable
    one->normaliseMap                         ( settings );
    if ( this->usePhase )
    {
        one->keepPhaseInMap                   ( this->alpha,
                                                this->bFactorChange,
                                               &this->bandwidthVec.at(0),
                                               &this->thetaVec.at(0),
                                               &this->phiVec.at(0),
                                               &this->glIntegOrderVec.at(0),
                                                settings,
                                                this->useCOM,
                                                settings->noIQRsFromMap,
                                                settings->verbose,
                                                settings->clearMapData,
                                                settings->rotChangeDefault,
                                                settings->overlayDefaults,
                                                settings->maskBlurFactor,
                                                settings->maskBlurFactorGiven );
    }
    else
    {
        one->removePhaseFromMap               ( this->alpha,
                                                this->bFactorChange,
                                                settings );
    }
    
    //======================================== Map the density onto spheres
    one->mapPhaselessToSphere                 ( settings,
                                                this->thetaVec.at(0),
                                                this->phiVec.at(0),
                                                this->shellSpacing,
                                                settings->manualShells );

    //======================================== Compute the Spherical Harmonics (SH) coefficients
    one->getSphericalHarmonicsCoeffs          ( this->bandwidthVec.at(0), settings );
    
    //======================================== If required, pre-compute the energy levels distance descriptor
    if ( this->energyLevelDist )
    {
        one->precomputeRotInvDescriptor       ( settings );
    }
    
    if ( settings->verbose > 2 )
    {
        std::cout << ">>>>> Structure " << this->structFiles.at(0) << " spherical harmonics computed." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        //==================================== Record progress
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Structure " << this->structFiles.at(0) << " spherical harmonics computed." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Initialise comparison values
    for ( unsigned int vecIt = 1; vecIt < static_cast<unsigned int> ( this->structFiles.size() ); vecIt++ )
    {
        this->bandwidthVec.at(vecIt)          = this->bandwidthVec.at(0);
        this->thetaVec.at(vecIt)              = this->thetaVec.at(0);
        this->phiVec.at(vecIt)                = this->phiVec.at(0);
        this->glIntegOrderVec.at(vecIt)       = this->glIntegOrderVec.at(0);
        this->extraSpaceVec.at(vecIt)         = this->extraSpaceVec.at(0);
    }
    
    //======================================== Read in the objects to compare against one and get SH Coeffs
    std::vector<ProSHADE_data*> objs ( this->structFiles.size() - 1 );
    for ( unsigned int strIt = 0; strIt < static_cast<unsigned int> ( this->structFiles.size() - 1 ); strIt++ )
    {
        //==================================== Initialise the object
        objs.at(strIt)                        = new ProSHADE_data ();
        
        //==================================== Read in the objects and determine maximum band, theta, phi, integration order and extra cell space, if not specified by user
        fileType                              = checkFileType ( structFiles.at(strIt+1) );
        if ( fileType == 2 )
        {
            objs.at(strIt)->getDensityMapFromMAP ( this->structFiles.at(strIt+1),
                                                  &this->shellSpacing,
                                                   this->mapResolution,
                                                  &this->bandwidthVec.at(strIt+1),
                                                  &this->thetaVec.at(strIt+1),
                                                  &this->phiVec.at(strIt+1),
                                                  &this->glIntegOrderVec.at(strIt+1),
                                                  &this->extraSpaceVec.at(strIt+1),
                                                   settings->mapResDefault,
                                                   settings->rotChangeDefault,
                                                   settings,
                                                   settings->overlayDefaults );
        }
        else if ( fileType == 1 )
        {
            objs.at(strIt)->getDensityMapFromPDB ( this->structFiles.at(strIt+1),
                                                  &this->shellSpacing,
                                                   this->mapResolution,
                                                  &this->bandwidthVec.at(strIt+1),
                                                  &this->thetaVec.at(strIt+1),
                                                  &this->phiVec.at(strIt+1),
                                                  &this->glIntegOrderVec.at(strIt+1),
                                                  &this->extraSpaceVec.at(strIt+1),
                                                   settings->mapResDefault,
                                                   settings,
                                                   this->bFactorValue,
                                                   this->firstLineCOM );
        }
        else
        {
            std::cerr << "!!! ProSHADE ERROR !!! Error loading file " << this->structFiles.at(strIt+1) << " !!! Cannot detect the extension (currently, only PDB or MAP are allowed) and therefore cannot read the file." << std::endl;
            exit ( -1 );
        }

        if ( settings->verbose > 1 )
        {
            std::cout << ">> Structure " << this->structFiles.at(strIt+1) << " loaded." << std::endl;
        }
        
        if ( settings->htmlReport )
        {
            //================================ Record progress
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Structure " << this->structFiles.at(strIt+1) << " loaded." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        //==================================== Deal with phase and MAP format data in general
        objs.at(strIt)->normaliseMap          ( settings );
        if ( this->usePhase )
        {
            objs.at(strIt)->keepPhaseInMap  ( this->alpha,
                                              this->bFactorChange,
                                             &this->bandwidthVec.at(strIt+1),
                                             &this->thetaVec.at(strIt+1),
                                             &this->phiVec.at(strIt+1),
                                             &this->glIntegOrderVec.at(strIt+1),
                                              settings,
                                              this->useCOM,
                                              settings->noIQRsFromMap,
                                              settings->verbose,
                                              settings->clearMapData,
                                              settings->rotChangeDefault,
                                              settings->overlayDefaults,
                                              settings->maskBlurFactor,
                                              settings->maskBlurFactorGiven );
        }
        else
        {
            objs.at(strIt)->removePhaseFromMap ( this->alpha,
                                                 this->bFactorChange,
                                                 settings );
        }

        //==================================== Map the density onto spheres
        objs.at(strIt)->mapPhaselessToSphere  ( settings,
                                                this->thetaVec.at(strIt+1),
                                                this->phiVec.at(strIt+1),
                                                this->shellSpacing,
                                                settings->manualShells );
        //==================================== Now compute the Spherical Harmonics (SH) coefficients
        objs.at(strIt)->getSphericalHarmonicsCoeffs( this->bandwidthVec.at(strIt+1), settings );
        
        //==================================== And if required, the energy levels distance pre-computation
        if ( this->energyLevelDist )
        {
            objs.at(strIt)->precomputeRotInvDescriptor ( settings );
        }
        
        if ( settings->verbose > 2 )
        {
            std::cout << ">>>>> Structure " << this->structFiles.at(strIt+1) << " spherical harmonics computed." << std::endl;
        }
        
        if ( settings->htmlReport )
        {
            //==================================== Record progress
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Structure " << this->structFiles.at(strIt+1) << " spherical harmonics computed." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
    }
    
    //======================================== Create the compare against all object
    this->cmpObj                              = new ProSHADE_compareOneAgainstAll ( one, &objs, this->ignoreLs, this->mPower, settings->verbose );
    
    //======================================== Save the decision whether to use phases or not
    if ( one->_keepOrRemove ) { this->cmpObj->_keepOrRemove = true; }
    else                      { this->cmpObj->_keepOrRemove = false; }
    
    //======================================== If required, compute the energy levels distances
    if ( this->energyLevelDist )
    {
        if ( settings->verbose > 0 )
        {
            std::cout << "Computing the cross-correlation distances." << std::endl;
        }
        
        if ( settings->htmlReport )
        {
            //================================ Record progress
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Computing the cross-correlation distances." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        this->energyLevelsDistances           = this->cmpObj->getEnergyLevelsDistance ( settings->verbose, settings );
        
        //==================================== ... and set which pairs not to follow further
        if ( this->enLevelsThreshold != -999.9 )
        {
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->cmpObj->all->size() ); iter++ )
            {
                if ( this->enLevelsThreshold > this->energyLevelsDistances.at(iter) )
                {
                    this->cmpObj->_enLevelsDoNotFollow.emplace_back ( 1 );
                }
                else
                {
                    this->cmpObj->_enLevelsDoNotFollow.emplace_back ( 0 );
                }
            }
        }
        else
        {
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->cmpObj->all->size() ); iter++ )
            {
                this->cmpObj->_enLevelsDoNotFollow.emplace_back ( 0 );
            }
        }
    }
    else
    {
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->cmpObj->all->size() ); iter++ )
        {
            this->cmpObj->_enLevelsDoNotFollow.emplace_back ( 0 );
        }
    }
    
    //======================================== Pre-compute the E matrices in case either the trace sigmal or full rotation distance are required
    if ( this->traceSigmaDist || this->fullRotFnDist )
    {
        this->cmpObj->precomputeTrSigmaDescriptor ( this->shellSpacing, &this->glIntegOrderVec, settings );
    }
    
    //======================================== Compute the trace sigma distances, if required
    if ( this->traceSigmaDist )
    {
        if ( settings->verbose > 0 )
        {
            std::cout << "Computing the trace sigma distances." << std::endl;
        }
        if ( settings->htmlReport )
        {
            //================================ Record progress
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Computing the trace sigma distances." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        this->traceSigmaDistances             = this->cmpObj->getTrSigmaDistance ( settings->verbose, settings );
        
        if ( this->trSigmaThreshold == -999.9 )
        {
            this->cmpObj->_trSigmaDoNotFollow = this->cmpObj->_enLevelsDoNotFollow;
        }
        else
        {
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->traceSigmaDistances.size() ); iter++ )
            {
                if ( this->trSigmaThreshold > this->traceSigmaDistances.at(iter) )
                {
                    this->cmpObj->_trSigmaDoNotFollow.emplace_back ( 1 );
                }
                else
                {
                    this->cmpObj->_trSigmaDoNotFollow.emplace_back ( 0 );
                }
            }
        }
    }
    else
    {
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->cmpObj->all->size() ); iter++ )
        {
            this->cmpObj->_trSigmaDoNotFollow.emplace_back ( 0 );
        }
    }
    
    //======================================== If full rotation distance is required, pre-compute the requirements
    if ( this->fullRotFnDist )
    {
        if ( settings->verbose > 0 )
        {
            std::cout << "Computing the rotation function distances." << std::endl;
        }
        if ( settings->htmlReport )
        {
            //================================ Record progress
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Computing the rotation function distances." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        this->cmpObj->getSO3InverseMap        ( settings );
        if ( settings->verbose > 2 )
        {
            std::cout << ">>>>> Inverse SO(3) Fourier transform maps obtained." << std::endl;
        }
        
        this->cmpObj->getEulerAngles          ( settings );
        if ( settings->verbose > 3 )
        {
            std::cout << ">>>>>>>> Optimal Euler angles calculated." << std::endl;
        }
        
        this->cmpObj->generateWignerMatrices  ( settings );
        if ( settings->verbose > 2 )
        {
            std::cout << ">>>>> Wigner matrices computed." << std::endl;
        }
        
        this->fullRotationDistances           = this->cmpObj->getRotCoeffDistance ( settings->verbose, settings );
    }
    
    //======================================== Free memory
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( objs.size() ); iter++ )
    {
        if ( objs.at(iter) != nullptr )
        {
            delete objs.at(iter);
        }
    }
    objs.clear                                ( );
    
    delete cmpObj;
    cmpObj                                    = nullptr;
}

/*! \brief This is the destructor for the ProSHADE_distances class.
 
 The is nothing particular about this destructor, it just frees the comparison object if it has been created.

 \warning This is an internal function which should not be used by the user.
 */

ProSHADE_internal::ProSHADE_distances::~ProSHADE_distances ( )
{
    if ( this->cmpObj != nullptr ) { delete this->cmpObj; }
}

/*! \brief This function returns a vector of energy level distances between the first and all other structures.
 
 This function allows programmatical access to the results of the ProSHADE distances calculations. It returns a
 vector of energy level distances from the first structure to all remaining structures.
 
 \param[out] X Vector of energy level distances.
 
 \warning The results are reported in double precision.
 \warning This is an internal function which should not be used by the user.
 */
std::vector<double> ProSHADE_internal::ProSHADE_distances::getEnergyLevelsDistances ( )
{
    //======================================== Sanity check
    if ( !this->energyLevelDist )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in structure comparison. !!! The energy level distances were not required when the ProSHADE_distances object was initialised (the setUp structure parameter) and therefore they cannot now be obtained." << std::endl;
        exit ( -1 );
    }
    
    if ( this->cmpObj != nullptr )
    {
        if ( !this->cmpObj->_energyLevelsComputed )
        {
            std::cerr << " !!! ProSHADE ERROR !!! Error in structure comparison. !!! The energy level distances were not computed before their values are required." << std::endl;
            exit ( -1 );
        }
    }
    
    //======================================== Return
    return ( this->energyLevelsDistances );
    
}

/*! \brief This function returns a vector of vectors of energy level distances between each fragment and the whole database.
 
 This function allows programmatical access to the results of the ProSHADE distances calculations. It returns a
 vector of vectors of energy level distances from the fragment to all passing database entries.
 
 \param[out] X Vector of vectors of energy level distances.
 
 \warning The results are reported in double precision.
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::vector<double> > ProSHADE_internal::ProSHADE_distances::getFragEnergyLevelsDistances ( )
{
    //======================================== Sanity check
    if ( !this->energyLevelDist )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in structure comparison. !!! The energy level distances were not required when the ProSHADE_distances object was initialised (the setUp structure parameter) and therefore they cannot now be obtained." << std::endl;
        exit ( -1 );
    }
    
    //======================================== Return
    return ( this->fragEnergyLevelsDistances );
    
}

/*! \brief This function returns a vector of trace sigma distances between the first and all other structures.
 
 This function allows programmatical access to the results of the ProSHADE distances calculations. It returns a
 vector of trace sigma distances from the first structure to all remaining structures. If it contains values -999.9,
 then these mean that this structure did not pass the threshold for energy level descriptor and therefore was not
 subjected to trace sigma descriptor computation.
 
 \param[out] X Vector of trace sigma distances.
 
 \warning The results are reported in double precision.
 \warning This is an internal function which should not be used by the user.
 */
std::vector<double> ProSHADE_internal::ProSHADE_distances::getTraceSigmaDistances ( )
{
    //======================================== Sanity check
    if ( !this->traceSigmaDist )
    {
        std::cerr << "!!! ProSHADE ERROR!!! Error in structure comparison. !!! The trace sigma distances were not required when the ProSHADE_distances object was initialised (the setUp structure parameter) and therefore they cannot now be obtained." << std::endl;
        exit ( -1 );
    }
    
    if ( this->cmpObj != nullptr )
    {
        if ( !this->cmpObj->_trSigmaComputed )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Error in structure comparison. !!! The trace sigma distances were not computed before their values are required." << std::endl;
            exit ( -1 );
        }
    }
    
    //======================================== Return
    return ( this->traceSigmaDistances );
    
}

/*! \brief This function prints the cleared results to the screen.
 
 This version of the function prints the results to the screen with regard to the user specified expected symmetry. It not only
 searches for the requested symmetry in the already detected symmetries, but it also does extra searches if the requested symmetry
 cannot be found in the already detected ones.
 
 \param[in] symmetryType String with the type of symmetry that is being sought after. Allowed values are C, D, T, O and I.
 \param[in] symmetryFold The fold of the symmetry being sought after. Only used for C and D symmetry types.
 \param[in] settings
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_symmetry::saveSymmetryElementsJSON ( std::string symmetryType, unsigned int symmetryFold, ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Claim the symmetry to be the highest detected
    if ( settings->verbose < 0 )
    {
        std::cout << "Now saving the JSONS files for symmetry elements." << std::endl;
    }
    
    //======================================== Clear any previous data
    std::ofstream jsonElements;
    jsonElements.open                     ( "./proshade_report/symmetryElements.json", std::ios::out );
    jsonElements << "{\n";
    jsonElements << "\t\"no_elements\": 0\n";
    jsonElements << "}\n";
    jsonElements.close                    ( );
    
    if ( symmetryType == "" )
    {
        //==================================== Open the JSON file
        std::ofstream jsonElements;
        jsonElements.open                     ( "./proshade_report/symmetryElements.json", std::ios::out );
        jsonElements << "{\n";
        
        //==================================== If icosahedral
        if ( static_cast<unsigned int> ( this->icosElems.size() ) > 0 )
        {
            //================================ Write number of elements
            jsonElements << "\t\"no_elements\": " << static_cast<int> ( this->icosElems.size() ) << ",\n";
            
            for ( unsigned int icosIt = 0; icosIt < static_cast<unsigned int> ( this->icosElems.size() ); icosIt++ )
            {
                std::stringstream ss;
                ss << "sym_element_" << icosIt + 1;
                
                std::array< double, 5 > inputArr;
                inputArr[0]               = ( ( M_PI * 2.0 ) / static_cast<double> ( this->icosElems.at(icosIt)[4] ) );
                inputArr[1]               = this->icosElems.at(icosIt)[1];
                inputArr[2]               = this->icosElems.at(icosIt)[2];
                inputArr[3]               = this->icosElems.at(icosIt)[3];
                inputArr[4]               = this->icosElems.at(icosIt)[4];
                std::vector < std::vector<double> > rMat = ProSHADE_internal_misc::getMatrixFromAxisAngle ( inputArr );
                
                jsonElements << "\t\"" << ss.str() << "\": \n\t\t{\n";
                jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
                jsonElements << "\t\t\t\t\t{ \"m_11\": " << rMat.at(0).at(0) << ", \"m_12\": " << rMat.at(0).at(1) << ", \"m_13\": " << rMat.at(0).at(2) << ", \"m_21\": " << rMat.at(1).at(0) << ", \"m_22\": " << rMat.at(1).at(1) << ", \"m_23\": " << rMat.at(1).at(2) << ", \"m_31\": " << rMat.at(2).at(0) << ", \"m_32\": " << rMat.at(2).at(1) << ", \"m_33\": " << rMat.at(2).at(2) << " }\n";
                jsonElements << "\t\t\t\t],\n";
                jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
                jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
                jsonElements << "\t\t\t\t]\n";
                
                if ( icosIt == ( static_cast<unsigned int> ( this->icosElems.size() ) - 1 ) )
                {
                    jsonElements << "\t\t}\n";
                }
                else
                {
                    jsonElements << "\t\t},\n";
                }
            }
        }
        
        //==================================== If octahedral
        else if ( static_cast<unsigned int> ( this->octaElems.size() ) > 0 )
        {
            //================================ Write number of elements
            jsonElements << "\t\"no_elements\": " << static_cast<int> ( this->octaElems.size() ) << ",\n";
            
            for ( unsigned int octaIt = 0; octaIt < static_cast<unsigned int> ( this->octaElems.size() ); octaIt++ )
            {
                std::stringstream ss;
                ss << "sym_element_" << octaIt + 1;
                
                std::array< double, 5 > inputArr;
                inputArr[0]               = ( ( M_PI * 2.0 ) / static_cast<double> ( this->octaElems.at(octaIt)[4] ) );
                inputArr[1]               = this->octaElems.at(octaIt)[1];
                inputArr[2]               = this->octaElems.at(octaIt)[2];
                inputArr[3]               = this->octaElems.at(octaIt)[3];
                inputArr[4]               = this->octaElems.at(octaIt)[4];
                std::vector < std::vector<double> > rMat = ProSHADE_internal_misc::getMatrixFromAxisAngle ( inputArr );
                
                jsonElements << "\t\"" << ss.str() << "\": \n\t\t{\n";
                jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
                jsonElements << "\t\t\t\t\t{ \"m_11\": " << rMat.at(0).at(0) << ", \"m_12\": " << rMat.at(0).at(1) << ", \"m_13\": " << rMat.at(0).at(2) << ", \"m_21\": " << rMat.at(1).at(0) << ", \"m_22\": " << rMat.at(1).at(1) << ", \"m_23\": " << rMat.at(1).at(2) << ", \"m_31\": " << rMat.at(2).at(0) << ", \"m_32\": " << rMat.at(2).at(1) << ", \"m_33\": " << rMat.at(2).at(2) << " }\n";
                jsonElements << "\t\t\t\t],\n";
                jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
                jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
                jsonElements << "\t\t\t\t]\n";
                
                if ( octaIt == ( static_cast<unsigned int> ( this->octaElems.size() ) - 1 ) )
                {
                    jsonElements << "\t\t}\n";
                }
                else
                {
                    jsonElements << "\t\t},\n";
                }
            }
        }
        
        //==================================== If tetrahedral
        else if ( static_cast<unsigned int> ( this->tetrElems.size() ) > 0 )
        {
            //================================ Write number of elements
            jsonElements << "\t\"no_elements\": " << static_cast<int> ( this->tetrElems.size() ) << ",\n";
            
            for ( unsigned int tetIt = 0; tetIt < static_cast<unsigned int> ( this->tetrElems.size() ); tetIt++ )
            {
                std::stringstream ss;
                ss << "sym_element_" << tetIt + 1;
                
                std::array< double, 5 > inputArr;
                inputArr[0]               = ( ( M_PI * 2.0 ) / static_cast<double> ( this->tetrElems.at(tetIt)[4] ) );
                inputArr[1]               = this->tetrElems.at(tetIt)[1];
                inputArr[2]               = this->tetrElems.at(tetIt)[2];
                inputArr[3]               = this->tetrElems.at(tetIt)[3];
                inputArr[4]               = this->tetrElems.at(tetIt)[4];
                std::vector < std::vector<double> > rMat = ProSHADE_internal_misc::getMatrixFromAxisAngle ( inputArr );
                
                jsonElements << "\t\"" << ss.str() << "\": \n\t\t{\n";
                jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
                jsonElements << "\t\t\t\t\t{ \"m_11\": " << rMat.at(0).at(0) << ", \"m_12\": " << rMat.at(0).at(1) << ", \"m_13\": " << rMat.at(0).at(2) << ", \"m_21\": " << rMat.at(1).at(0) << ", \"m_22\": " << rMat.at(1).at(1) << ", \"m_23\": " << rMat.at(1).at(2) << ", \"m_31\": " << rMat.at(2).at(0) << ", \"m_32\": " << rMat.at(2).at(1) << ", \"m_33\": " << rMat.at(2).at(2) << " }\n";
                jsonElements << "\t\t\t\t],\n";
                jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
                jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
                jsonElements << "\t\t\t\t]\n";
                
                if ( tetIt == ( static_cast<unsigned int> ( this->tetrElems.size() ) - 1 ) )
                {
                    jsonElements << "\t\t}\n";
                }
                else
                {
                    jsonElements << "\t\t},\n";
                }
            }
        }
        
        //==================================== If dihedral
        else if ( static_cast<unsigned int> ( this->dnSymmClear.size() ) > 0 )
        {
            //================================ Write number of elements
            jsonElements << "\t\"no_elements\": " << static_cast<int> ( this->dnSymmClear.at(0).at(0)[0] ) + static_cast<int> ( this->dnSymmClear.at(0).at(1)[0] ) - 1 << ",\n";
            
            //================================ Write identity element
            jsonElements << "\t\"sym_element_1\": \n\t\t{\n";
            jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
            jsonElements << "\t\t\t\t\t{ \"m_11\": 1.0, \"m_12\": 0.0, \"m_13\": 0.0, \"m_21\": 0.0, \"m_22\": 1.0, \"m_23\": 0.0, \"m_31\": 0.0, \"m_32\": 0.0, \"m_33\": 1.0 }\n";
            jsonElements << "\t\t\t\t],\n";
            jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
            jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
            jsonElements << "\t\t\t\t]\n";
            jsonElements << "\t\t},\n";
            
            unsigned int symElID              = 2;
            for ( unsigned int it = 0; it < 2; it++ )
            {
                if ( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) % 2 == 0 )
                {
                    for ( int iter = -std::ceil( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ); iter++ )
                    {
                        if ( iter == 0 ) { continue; }
                        if ( iter == -std::ceil( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ) ) { continue; }
                        
                        std::stringstream ss;
                        ss << "sym_element_" << symElID;
                        symElID                  += 1;
                        
                        std::array< double, 5 > inputArr;
                        inputArr[0]               = iter * ( ( M_PI * 2.0 ) / static_cast<double> ( this->dnSymmClear.at(0).at(it)[0] ) );
                        inputArr[1]               = this->dnSymmClear.at(0).at(it)[1];
                        inputArr[2]               = this->dnSymmClear.at(0).at(it)[2];
                        inputArr[3]               = this->dnSymmClear.at(0).at(it)[3];
                        inputArr[4]               = this->dnSymmClear.at(0).at(it)[4];
                        std::vector < std::vector<double> > rMat = ProSHADE_internal_misc::getMatrixFromAxisAngle ( inputArr );
                        
                        jsonElements << "\t\"" << ss.str() << "\": \n\t\t{\n";
                        jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
                        jsonElements << "\t\t\t\t\t{ \"m_11\": " << rMat.at(0).at(0) << ", \"m_12\": " << rMat.at(0).at(1) << ", \"m_13\": " << rMat.at(0).at(2) << ", \"m_21\": " << rMat.at(1).at(0) << ", \"m_22\": " << rMat.at(1).at(1) << ", \"m_23\": " << rMat.at(1).at(2) << ", \"m_31\": " << rMat.at(2).at(0) << ", \"m_32\": " << rMat.at(2).at(1) << ", \"m_33\": " << rMat.at(2).at(2) << " }\n";
                        jsonElements << "\t\t\t\t],\n";
                        jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
                        jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
                        jsonElements << "\t\t\t\t]\n";
                        
                        if ( iter == std::ceil( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ) )
                        {
                            if ( it != 0 )
                            {
                                jsonElements << "\t\t}\n";
                            }
                            else
                            {
                                jsonElements << "\t\t},\n";
                            }
                        }
                        else
                        {
                            jsonElements << "\t\t},\n";
                        }
                    }
                }
                else
                {
                    for ( int iter = -std::floor( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ); iter++ )
                    {
                        if ( iter == 0 ) { continue; }
                        
                        std::stringstream ss;
                        ss << "sym_element_" << symElID;
                        symElID                  += 1;
                        
                        std::array< double, 5 > inputArr;
                        inputArr[0]               = iter * ( ( M_PI * 2.0 ) / static_cast<double> ( this->dnSymmClear.at(0).at(it)[0] ) );
                        inputArr[1]               = this->dnSymmClear.at(0).at(it)[1];
                        inputArr[2]               = this->dnSymmClear.at(0).at(it)[2];
                        inputArr[3]               = this->dnSymmClear.at(0).at(it)[3];
                        inputArr[4]               = this->dnSymmClear.at(0).at(it)[4];
                        std::vector < std::vector<double> > rMat = ProSHADE_internal_misc::getMatrixFromAxisAngle ( inputArr );
                        
                        jsonElements << "\t\"" << ss.str() << "\": \n\t\t{\n";
                        jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
                        jsonElements << "\t\t\t\t\t{ \"m_11\": " << rMat.at(0).at(0) << ", \"m_12\": " << rMat.at(0).at(1) << ", \"m_13\": " << rMat.at(0).at(2) << ", \"m_21\": " << rMat.at(1).at(0) << ", \"m_22\": " << rMat.at(1).at(1) << ", \"m_23\": " << rMat.at(1).at(2) << ", \"m_31\": " << rMat.at(2).at(0) << ", \"m_32\": " << rMat.at(2).at(1) << ", \"m_33\": " << rMat.at(2).at(2) << " }\n";
                        jsonElements << "\t\t\t\t],\n";
                        jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
                        jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
                        jsonElements << "\t\t\t\t]\n";
                        
                        if ( iter == std::floor( static_cast<int> ( this->dnSymmClear.at(0).at(it)[0] ) / 2.0 ) )
                        {
                            if ( it != 0 )
                            {
                                jsonElements << "\t\t}\n";
                            }
                            else
                            {
                                jsonElements << "\t\t},\n";
                            }
                        }
                        else
                        {
                            jsonElements << "\t\t},\n";
                        }
                    }
                }
            }
        }
        
        //==================================== If cyclic
        else if ( static_cast<unsigned int> ( this->cnSymmClear.size() ) > 0 )
        {
            //================================ Write number of elements
            jsonElements << "\t\"no_elements\": " << static_cast<int> ( this->cnSymmClear.at(0)[0] ) << ",\n";
            
            //================================ Write identity element
            jsonElements << "\t\"sym_element_1\": \n\t\t{\n";
            jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
            jsonElements << "\t\t\t\t\t{ \"m_11\": 1.0, \"m_12\": 0.0, \"m_13\": 0.0, \"m_21\": 0.0, \"m_22\": 1.0, \"m_23\": 0.0, \"m_31\": 0.0, \"m_32\": 0.0, \"m_33\": 1.0 }\n";
            jsonElements << "\t\t\t\t],\n";
            jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
            jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
            jsonElements << "\t\t\t\t]\n";
            jsonElements << "\t\t},\n";
            
            unsigned int symElID              = 2;
            if ( static_cast<int> ( this->cnSymmClear.at(0)[0] ) % 2 == 0 )
            {
                for ( int iter = -std::ceil( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ); iter++ )
                {
                    if ( iter == 0 ) { continue; }
                    if ( iter == -std::ceil( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ) ) { continue; }
                    
                    std::stringstream ss;
                    ss << "sym_element_" << symElID;
                    symElID                  += 1;
                    
                    std::array< double, 5 > inputArr;
                    inputArr[0]               = iter * ( ( M_PI * 2.0 ) / static_cast<double> ( this->cnSymmClear.at(0)[0] ) );
                    inputArr[1]               = this->cnSymmClear.at(0)[1];
                    inputArr[2]               = this->cnSymmClear.at(0)[2];
                    inputArr[3]               = this->cnSymmClear.at(0)[3];
                    inputArr[4]               = this->cnSymmClear.at(0)[4];
                    std::vector < std::vector<double> > rMat = ProSHADE_internal_misc::getMatrixFromAxisAngle ( inputArr );
                    
                    jsonElements << "\t\"" << ss.str() << "\": \n\t\t{\n";
                    jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
                    jsonElements << "\t\t\t\t\t{ \"m_11\": " << rMat.at(0).at(0) << ", \"m_12\": " << rMat.at(0).at(1) << ", \"m_13\": " << rMat.at(0).at(2) << ", \"m_21\": " << rMat.at(1).at(0) << ", \"m_22\": " << rMat.at(1).at(1) << ", \"m_23\": " << rMat.at(1).at(2) << ", \"m_31\": " << rMat.at(2).at(0) << ", \"m_32\": " << rMat.at(2).at(1) << ", \"m_33\": " << rMat.at(2).at(2) << " }\n";
                    jsonElements << "\t\t\t\t],\n";
                    jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
                    jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
                    jsonElements << "\t\t\t\t]\n";
                    
                    if ( iter == std::ceil( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ) )
                    {
                        jsonElements << "\t\t}\n";
                    }
                    else
                    {
                        jsonElements << "\t\t},\n";
                    }
                }
            }
            else
            {
                for ( int iter = -std::floor( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ); iter++ )
                {
                    if ( iter == 0 ) { continue; }
                    
                    std::stringstream ss;
                    ss << "sym_element_" << symElID;
                    symElID                  += 1;
                    
                    std::array< double, 5 > inputArr;
                    inputArr[0]               = iter * ( ( M_PI * 2.0 ) / static_cast<double> ( this->cnSymmClear.at(0)[0] ) );
                    inputArr[1]               = this->cnSymmClear.at(0)[1];
                    inputArr[2]               = this->cnSymmClear.at(0)[2];
                    inputArr[3]               = this->cnSymmClear.at(0)[3];
                    inputArr[4]               = this->cnSymmClear.at(0)[4];
                    std::vector < std::vector<double> > rMat = ProSHADE_internal_misc::getMatrixFromAxisAngle ( inputArr );
                    
                    jsonElements << "\t\"" << ss.str() << "\": \n\t\t{\n";
                    jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
                    jsonElements << "\t\t\t\t\t{ \"m_11\": " << rMat.at(0).at(0) << ", \"m_12\": " << rMat.at(0).at(1) << ", \"m_13\": " << rMat.at(0).at(2) << ", \"m_21\": " << rMat.at(1).at(0) << ", \"m_22\": " << rMat.at(1).at(1) << ", \"m_23\": " << rMat.at(1).at(2) << ", \"m_31\": " << rMat.at(2).at(0) << ", \"m_32\": " << rMat.at(2).at(1) << ", \"m_33\": " << rMat.at(2).at(2) << " }\n";
                    jsonElements << "\t\t\t\t],\n";
                    jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
                    jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
                    jsonElements << "\t\t\t\t]\n";
                    
                    if ( iter == std::floor( static_cast<int> ( this->cnSymmClear.at(0)[0] ) / 2.0 ) )
                    {
                        jsonElements << "\t\t}\n";
                    }
                    else
                    {
                        jsonElements << "\t\t},\n";
                    }
                }
            }
        }
        
        //==================================== If none of the above
        else
        {
            jsonElements << "\t\"no_elements\": 0\n";
        }
        
        //==================================== Close the JSON file
        jsonElements << "}\n";
        jsonElements.close                    ( );
    }
    
    if ( symmetryType == "I" )
    {
        //==================================== Open the JSON file
        std::ofstream jsonElements;
        jsonElements.open                     ( "./proshade_report/symmetryElements.json", std::ios::out );
        jsonElements << "{\n";
        
        if ( static_cast<unsigned int> ( this->icosSymm.size() ) > 0 )
        {
            //================================ Write number of elements
            jsonElements << "\t\"no_elements\": " << static_cast<int> ( this->icosElems.size() ) << ",\n";
            
            for ( unsigned int icosIt = 0; icosIt < static_cast<unsigned int> ( this->icosElems.size() ); icosIt++ )
            {
                std::stringstream ss;
                ss << "sym_element_" << icosIt + 1;
                
                std::array< double, 5 > inputArr;
                inputArr[0]               = ( ( M_PI * 2.0 ) / static_cast<double> ( this->icosElems.at(icosIt)[4] ) );
                inputArr[1]               = this->icosElems.at(icosIt)[1];
                inputArr[2]               = this->icosElems.at(icosIt)[2];
                inputArr[3]               = this->icosElems.at(icosIt)[3];
                inputArr[4]               = this->icosElems.at(icosIt)[4];
                std::vector < std::vector<double> > rMat = ProSHADE_internal_misc::getMatrixFromAxisAngle ( inputArr );
                
                jsonElements << "\t\"" << ss.str() << "\": \n\t\t{\n";
                jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
                jsonElements << "\t\t\t\t\t{ \"m_11\": " << rMat.at(0).at(0) << ", \"m_12\": " << rMat.at(0).at(1) << ", \"m_13\": " << rMat.at(0).at(2) << ", \"m_21\": " << rMat.at(1).at(0) << ", \"m_22\": " << rMat.at(1).at(1) << ", \"m_23\": " << rMat.at(1).at(2) << ", \"m_31\": " << rMat.at(2).at(0) << ", \"m_32\": " << rMat.at(2).at(1) << ", \"m_33\": " << rMat.at(2).at(2) << " }\n";
                jsonElements << "\t\t\t\t],\n";
                jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
                jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
                jsonElements << "\t\t\t\t]\n";
                
                if ( icosIt == ( static_cast<unsigned int> ( this->icosElems.size() ) - 1 ) )
                {
                    jsonElements << "\t\t}\n";
                }
                else
                {
                    jsonElements << "\t\t},\n";
                }
            }
        }
        else
        {
            std::cout << "!!! ProSHADE WARNING !!! Could not detect the requested symmetry. You can try changing the resolution or searching for different symmetry from the alternatives list printed below." << std::endl << std::endl;
            jsonElements << "\t\"no_elements\": 0\n";
        }
        
        //==================================== Close the JSON file
        jsonElements << "}\n";
        jsonElements.close                    ( );
    }
    
    if ( symmetryType == "O" )
    {
        //==================================== Open the JSON file
        std::ofstream jsonElements;
        jsonElements.open                     ( "./proshade_report/symmetryElements.json", std::ios::out );
        jsonElements << "{\n";
        
        if ( static_cast<unsigned int> ( this->octaSymm.size() ) > 0 )
        {
            //================================ Write number of elements
            jsonElements << "\t\"no_elements\": " << static_cast<int> ( this->octaElems.size() ) << ",\n";
            
            for ( unsigned int octaIt = 0; octaIt < static_cast<unsigned int> ( this->octaElems.size() ); octaIt++ )
            {
                std::stringstream ss;
                ss << "sym_element_" << octaIt + 1;
                
                std::array< double, 5 > inputArr;
                inputArr[0]               = ( ( M_PI * 2.0 ) / static_cast<double> ( this->octaElems.at(octaIt)[4] ) );
                inputArr[1]               = this->octaElems.at(octaIt)[1];
                inputArr[2]               = this->octaElems.at(octaIt)[2];
                inputArr[3]               = this->octaElems.at(octaIt)[3];
                inputArr[4]               = this->octaElems.at(octaIt)[4];
                std::vector < std::vector<double> > rMat = ProSHADE_internal_misc::getMatrixFromAxisAngle ( inputArr );
                
                jsonElements << "\t\"" << ss.str() << "\": \n\t\t{\n";
                jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
                jsonElements << "\t\t\t\t\t{ \"m_11\": " << rMat.at(0).at(0) << ", \"m_12\": " << rMat.at(0).at(1) << ", \"m_13\": " << rMat.at(0).at(2) << ", \"m_21\": " << rMat.at(1).at(0) << ", \"m_22\": " << rMat.at(1).at(1) << ", \"m_23\": " << rMat.at(1).at(2) << ", \"m_31\": " << rMat.at(2).at(0) << ", \"m_32\": " << rMat.at(2).at(1) << ", \"m_33\": " << rMat.at(2).at(2) << " }\n";
                jsonElements << "\t\t\t\t],\n";
                jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
                jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
                jsonElements << "\t\t\t\t]\n";
                
                if ( octaIt == ( static_cast<unsigned int> ( this->octaElems.size() ) - 1 ) )
                {
                    jsonElements << "\t\t}\n";
                }
                else
                {
                    jsonElements << "\t\t},\n";
                }
            }
        }
        else
        {
            std::cout << "!!! ProSHADE WARNING !!! Could not detect the requested symmetry. You can try changing the resolution or searching for different symmetry from the alternatives list printed below.\n\n" << std::endl << std::endl;
            jsonElements << "\t\"no_elements\": 0\n";
        }
        
        //==================================== Close the JSON file
        jsonElements << "}\n";
        jsonElements.close                    ( );
    }
    
    if ( symmetryType == "T" )
    {
        //==================================== Open the JSON file
        std::ofstream jsonElements;
        jsonElements.open                     ( "./proshade_report/symmetryElements.json", std::ios::out );
        jsonElements << "{\n";
        
        if ( static_cast<unsigned int> ( this->tetrSymm.size() ) > 0 )
        {
            //================================ Write number of elements
            jsonElements << "\t\"no_elements\": " << static_cast<int> ( this->tetrElems.size() ) << ",\n";
            
            for ( unsigned int tetIt = 0; tetIt < static_cast<unsigned int> ( this->tetrElems.size() ); tetIt++ )
            {
                std::stringstream ss;
                ss << "sym_element_" << tetIt + 1;
                
                std::array< double, 5 > inputArr;
                inputArr[0]               = ( ( M_PI * 2.0 ) / static_cast<double> ( this->tetrElems.at(tetIt)[4] ) );
                inputArr[1]               = this->tetrElems.at(tetIt)[1];
                inputArr[2]               = this->tetrElems.at(tetIt)[2];
                inputArr[3]               = this->tetrElems.at(tetIt)[3];
                inputArr[4]               = this->tetrElems.at(tetIt)[4];
                std::vector < std::vector<double> > rMat = ProSHADE_internal_misc::getMatrixFromAxisAngle ( inputArr );
                
                jsonElements << "\t\"" << ss.str() << "\": \n\t\t{\n";
                jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
                jsonElements << "\t\t\t\t\t{ \"m_11\": " << rMat.at(0).at(0) << ", \"m_12\": " << rMat.at(0).at(1) << ", \"m_13\": " << rMat.at(0).at(2) << ", \"m_21\": " << rMat.at(1).at(0) << ", \"m_22\": " << rMat.at(1).at(1) << ", \"m_23\": " << rMat.at(1).at(2) << ", \"m_31\": " << rMat.at(2).at(0) << ", \"m_32\": " << rMat.at(2).at(1) << ", \"m_33\": " << rMat.at(2).at(2) << " }\n";
                jsonElements << "\t\t\t\t],\n";
                jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
                jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
                jsonElements << "\t\t\t\t]\n";
                
                if ( tetIt == ( static_cast<unsigned int> ( this->tetrElems.size() ) - 1 ) )
                {
                    jsonElements << "\t\t}\n";
                }
                else
                {
                    jsonElements << "\t\t},\n";
                }
            }
        }
        else
        {
            std::cout << "!!! ProSHADE WARNING !!! Could not detect the requested symmetry. You can try changing the resolution or searching for different symmetry from the alternatives list printed below.\n\n" << std::endl << std::endl;
            jsonElements << "\t\"no_elements\": 0\n";
        }
        
        //==================================== Close the JSON file
        jsonElements << "}\n";
        jsonElements.close                    ( );
    }
    
    if ( symmetryType == "D" )
    {
        //==================================== Open the JSON file
        std::ofstream jsonElements;
        jsonElements.open                     ( "./proshade_report/symmetryElements.json", std::ios::out );
        jsonElements << "{\n";
        
        if ( static_cast<unsigned int> ( this->dnSymm.size() ) > 0 )
        {
            bool reqFound = false;
            for ( unsigned int dIt = 0; dIt < static_cast<unsigned int> ( this->dnSymm.size() ); dIt++ )
            {
                if ( reqFound ) { break; }
                unsigned int howManyTwos = 0;
                for ( unsigned int dItt = 0; dItt < static_cast<unsigned int> ( this->dnSymm.at(dIt).size() ); dItt++ )
                {
                    if ( reqFound ) { break; }
                    if ( symmetryFold != 2 )
                    {
                        if ( this->dnSymm.at(dIt).at(dItt)[0] == symmetryFold )
                        {
                            //================================ Write number of elements
                            jsonElements << "\t\"no_elements\": " << static_cast<int> ( this->dnSymm.at(dIt).at(0)[0] ) + static_cast<int> ( this->dnSymm.at(dIt).at(1)[0] ) - 1 << ",\n";
                            
                            //================================ Write identity element
                            jsonElements << "\t\"sym_element_1\": \n\t\t{\n";
                            jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
                            jsonElements << "\t\t\t\t\t{ \"m_11\": 1.0, \"m_12\": 0.0, \"m_13\": 0.0, \"m_21\": 0.0, \"m_22\": 1.0, \"m_23\": 0.0, \"m_31\": 0.0, \"m_32\": 0.0, \"m_33\": 1.0 }\n";
                            jsonElements << "\t\t\t\t],\n";
                            jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
                            jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
                            jsonElements << "\t\t\t\t]\n";
                            jsonElements << "\t\t},\n";
                            
                            unsigned int symElID              = 2;
                            for ( unsigned int it = 0; it < 2; it++ )
                            {
                                if ( static_cast<int> ( this->dnSymm.at(0).at(it)[0] ) % 2 == 0 )
                                {
                                    for ( int iter = -std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter++ )
                                    {
                                        if ( iter == 0 ) { continue; }
                                        if ( iter == -std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ) ) { continue; }
                                        
                                        std::stringstream ss;
                                        ss << "sym_element_" << symElID;
                                        symElID                  += 1;
                                        
                                        std::array< double, 5 > inputArr;
                                        inputArr[0]               = iter * ( ( M_PI * 2.0 ) / static_cast<double> ( this->dnSymm.at(dIt).at(it)[0] ) );
                                        inputArr[1]               = this->dnSymm.at(dIt).at(it)[1];
                                        inputArr[2]               = this->dnSymm.at(dIt).at(it)[2];
                                        inputArr[3]               = this->dnSymm.at(dIt).at(it)[3];
                                        inputArr[4]               = this->dnSymm.at(dIt).at(it)[4];
                                        std::vector < std::vector<double> > rMat = ProSHADE_internal_misc::getMatrixFromAxisAngle ( inputArr );
                                        
                                        jsonElements << "\t\"" << ss.str() << "\": \n\t\t{\n";
                                        jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
                                        jsonElements << "\t\t\t\t\t{ \"m_11\": " << rMat.at(0).at(0) << ", \"m_12\": " << rMat.at(0).at(1) << ", \"m_13\": " << rMat.at(0).at(2) << ", \"m_21\": " << rMat.at(1).at(0) << ", \"m_22\": " << rMat.at(1).at(1) << ", \"m_23\": " << rMat.at(1).at(2) << ", \"m_31\": " << rMat.at(2).at(0) << ", \"m_32\": " << rMat.at(2).at(1) << ", \"m_33\": " << rMat.at(2).at(2) << " }\n";
                                        jsonElements << "\t\t\t\t],\n";
                                        jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
                                        jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
                                        jsonElements << "\t\t\t\t]\n";
                                        
                                        if ( iter == std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ) )
                                        {
                                            if ( it != 0 )
                                            {
                                                jsonElements << "\t\t}\n";
                                            }
                                            else
                                            {
                                                jsonElements << "\t\t},\n";
                                            }
                                        }
                                        else
                                        {
                                            jsonElements << "\t\t},\n";
                                        }
                                    }
                                }
                                else
                                {
                                    for ( int iter = -std::floor( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter++ )
                                    {
                                        if ( iter == 0 ) { continue; }
                                        
                                        std::stringstream ss;
                                        ss << "sym_element_" << symElID;
                                        symElID                  += 1;
                                        
                                        std::array< double, 5 > inputArr;
                                        inputArr[0]               = iter * ( ( M_PI * 2.0 ) / static_cast<double> ( this->dnSymm.at(dIt).at(it)[0] ) );
                                        inputArr[1]               = this->dnSymm.at(dIt).at(it)[1];
                                        inputArr[2]               = this->dnSymm.at(dIt).at(it)[2];
                                        inputArr[3]               = this->dnSymm.at(dIt).at(it)[3];
                                        inputArr[4]               = this->dnSymm.at(dIt).at(it)[4];
                                        std::vector < std::vector<double> > rMat = ProSHADE_internal_misc::getMatrixFromAxisAngle ( inputArr );
                                        
                                        jsonElements << "\t\"" << ss.str() << "\": \n\t\t{\n";
                                        jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
                                        jsonElements << "\t\t\t\t\t{ \"m_11\": " << rMat.at(0).at(0) << ", \"m_12\": " << rMat.at(0).at(1) << ", \"m_13\": " << rMat.at(0).at(2) << ", \"m_21\": " << rMat.at(1).at(0) << ", \"m_22\": " << rMat.at(1).at(1) << ", \"m_23\": " << rMat.at(1).at(2) << ", \"m_31\": " << rMat.at(2).at(0) << ", \"m_32\": " << rMat.at(2).at(1) << ", \"m_33\": " << rMat.at(2).at(2) << " }\n";
                                        jsonElements << "\t\t\t\t],\n";
                                        jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
                                        jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
                                        jsonElements << "\t\t\t\t]\n";
                                        
                                        if ( iter == std::floor( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ) )
                                        {
                                            if ( it != 0 )
                                            {
                                                jsonElements << "\t\t}\n";
                                            }
                                            else
                                            {
                                                jsonElements << "\t\t},\n";
                                            }
                                        }
                                        else
                                        {
                                            jsonElements << "\t\t},\n";
                                        }
                                    }
                                }
                            }
                            
                            reqFound = true;
                            break;
                        }
                        else
                        {
                            if ( this->dnSymm.at(dIt).at(dItt)[0] == 2 )
                            {
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    if ( symmetryFold == 2 )
                    {
                        if ( this->dnSymm.at(dIt).at(dItt)[0] == 2 )
                        {
                            howManyTwos += 1;
                        }
                        if ( this->dnSymm.at(dIt).at(dItt)[0] == symmetryFold && howManyTwos == 2 )
                        {
                            //================================ Write number of elements
                            jsonElements << "\t\"no_elements\": " << static_cast<int> ( this->dnSymm.at(dIt).at(0)[0] ) + static_cast<int> ( this->dnSymm.at(dIt).at(1)[0] ) - 1 << ",\n";
                            
                            //================================ Write identity element
                            jsonElements << "\t\"sym_element_1\": \n\t\t{\n";
                            jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
                            jsonElements << "\t\t\t\t\t{ \"m_11\": 1.0, \"m_12\": 0.0, \"m_13\": 0.0, \"m_21\": 0.0, \"m_22\": 1.0, \"m_23\": 0.0, \"m_31\": 0.0, \"m_32\": 0.0, \"m_33\": 1.0 }\n";
                            jsonElements << "\t\t\t\t],\n";
                            jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
                            jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
                            jsonElements << "\t\t\t\t]\n";
                            jsonElements << "\t\t},\n";
                            
                            unsigned int symElID              = 2;
                            for ( unsigned int it = 0; it < 2; it++ )
                            {
                                if ( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) % 2 == 0 )
                                {
                                    for ( int iter = -std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter++ )
                                    {
                                        if ( iter == 0 ) { continue; }
                                        if ( iter == -std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ) ) { continue; }
                                        
                                        std::stringstream ss;
                                        ss << "sym_element_" << symElID;
                                        symElID                  += 1;
                                        
                                        std::array< double, 5 > inputArr;
                                        inputArr[0]               = iter * ( ( M_PI * 2.0 ) / static_cast<double> ( this->dnSymm.at(dIt).at(it)[0] ) );
                                        inputArr[1]               = this->dnSymm.at(dIt).at(it)[1];
                                        inputArr[2]               = this->dnSymm.at(dIt).at(it)[2];
                                        inputArr[3]               = this->dnSymm.at(dIt).at(it)[3];
                                        inputArr[4]               = this->dnSymm.at(dIt).at(it)[4];
                                        std::vector < std::vector<double> > rMat = ProSHADE_internal_misc::getMatrixFromAxisAngle ( inputArr );
                                        
                                        jsonElements << "\t\"" << ss.str() << "\": \n\t\t{\n";
                                        jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
                                        jsonElements << "\t\t\t\t\t{ \"m_11\": " << rMat.at(0).at(0) << ", \"m_12\": " << rMat.at(0).at(1) << ", \"m_13\": " << rMat.at(0).at(2) << ", \"m_21\": " << rMat.at(1).at(0) << ", \"m_22\": " << rMat.at(1).at(1) << ", \"m_23\": " << rMat.at(1).at(2) << ", \"m_31\": " << rMat.at(2).at(0) << ", \"m_32\": " << rMat.at(2).at(1) << ", \"m_33\": " << rMat.at(2).at(2) << " }\n";
                                        jsonElements << "\t\t\t\t],\n";
                                        jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
                                        jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
                                        jsonElements << "\t\t\t\t]\n";
                                        
                                        if ( iter == std::ceil( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ) )
                                        {
                                            if ( it != 0 )
                                            {
                                                jsonElements << "\t\t}\n";
                                            }
                                            else
                                            {
                                                jsonElements << "\t\t},\n";
                                            }
                                        }
                                        else
                                        {
                                            jsonElements << "\t\t},\n";
                                        }
                                    }
                                }
                                else
                                {
                                    for ( int iter = -std::floor( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ); iter++ )
                                    {
                                        if ( iter == 0 ) { continue; }
                                        
                                        std::stringstream ss;
                                        ss << "sym_element_" << symElID;
                                        symElID                  += 1;
                                        
                                        std::array< double, 5 > inputArr;
                                        inputArr[0]               = iter * ( ( M_PI * 2.0 ) / static_cast<double> ( this->dnSymm.at(dIt).at(it)[0] ) );
                                        inputArr[1]               = this->dnSymm.at(dIt).at(it)[1];
                                        inputArr[2]               = this->dnSymm.at(dIt).at(it)[2];
                                        inputArr[3]               = this->dnSymm.at(dIt).at(it)[3];
                                        inputArr[4]               = this->dnSymm.at(dIt).at(it)[4];
                                        std::vector < std::vector<double> > rMat = ProSHADE_internal_misc::getMatrixFromAxisAngle ( inputArr );
                                        
                                        jsonElements << "\t\"" << ss.str() << "\": \n\t\t{\n";
                                        jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
                                        jsonElements << "\t\t\t\t\t{ \"m_11\": " << rMat.at(0).at(0) << ", \"m_12\": " << rMat.at(0).at(1) << ", \"m_13\": " << rMat.at(0).at(2) << ", \"m_21\": " << rMat.at(1).at(0) << ", \"m_22\": " << rMat.at(1).at(1) << ", \"m_23\": " << rMat.at(1).at(2) << ", \"m_31\": " << rMat.at(2).at(0) << ", \"m_32\": " << rMat.at(2).at(1) << ", \"m_33\": " << rMat.at(2).at(2) << " }\n";
                                        jsonElements << "\t\t\t\t],\n";
                                        jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
                                        jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
                                        jsonElements << "\t\t\t\t]\n";
                                        
                                        if ( iter == std::floor( static_cast<int> ( this->dnSymm.at(dIt).at(it)[0] ) / 2.0 ) )
                                        {
                                            if ( it != 0 )
                                            {
                                                jsonElements << "\t\t}\n";
                                            }
                                            else
                                            {
                                                jsonElements << "\t\t},\n";
                                            }
                                        }
                                        else
                                        {
                                            jsonElements << "\t\t},\n";
                                        }
                                    }
                                }
                            }
                            
                            reqFound = true;
                            break;
                        }
                    }
                }
            }
            if ( !reqFound )
            {
                std::cout << "!!! ProSHADE WARNING !!! Could not detect the requested dihedral symmetry, but detected dihedral symmetries with different fold. You can try changing the resolution or selecting one of the alternative symmetries printed below.\n\n" << std::endl << std::endl;
                jsonElements << "\t\"no_elements\": 0\n";
            }
        }
        else
        {
            std::cerr << "!!! Warning !!! Could not detect the requested dihedral symmetry. You can try changing the resolution or searching for different symmetry from the alternatives list printed below.\n\n" << std::endl << std::endl;
            jsonElements << "\t\"no_elements\": 0\n";
        }
        
        //==================================== Close the JSON file
        jsonElements << "}\n";
        jsonElements.close                    ( );
    }
    
    if ( symmetryType == "C" )
    {
        //==================================== Open the JSON file
        std::ofstream jsonElements;
        jsonElements.open                     ( "./proshade_report/symmetryElements.json", std::ios::out );
        jsonElements << "{\n";
        
        if ( static_cast<unsigned int> ( this->cnSymm.size() ) > 0 )
        {
            bool reqFound = false;
            for ( unsigned int dIt = 0; dIt < static_cast<unsigned int> ( this->cnSymm.size() ); dIt++ )
            {
                if ( this->cnSymm.at(dIt)[0] == symmetryFold )
                {
                    //================================ Write number of elements
                    jsonElements << "\t\"no_elements\": " << static_cast<int> ( this->cnSymm.at(dIt)[0] ) << ",\n";
                    
                    //================================ Write identity element
                    jsonElements << "\t\"sym_element_1\": \n\t\t{\n";
                    jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
                    jsonElements << "\t\t\t\t\t{ \"m_11\": 1.0, \"m_12\": 0.0, \"m_13\": 0.0, \"m_21\": 0.0, \"m_22\": 1.0, \"m_23\": 0.0, \"m_31\": 0.0, \"m_32\": 0.0, \"m_33\": 1.0 }\n";
                    jsonElements << "\t\t\t\t],\n";
                    jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
                    jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
                    jsonElements << "\t\t\t\t]\n";
                    jsonElements << "\t\t},\n";
                    
                    unsigned int symElID              = 2;
                    if ( static_cast<int> ( this->cnSymm.at(dIt)[0] ) % 2 == 0 )
                    {
                        for ( int iter = -std::ceil( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ); iter <= std::ceil( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ); iter++ )
                        {
                            if ( iter == 0 ) { continue; }
                            if ( iter == -std::ceil( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ) ) { continue; }
                            
                            std::stringstream ss;
                            ss << "sym_element_" << symElID;
                            symElID                  += 1;
                            
                            std::array< double, 5 > inputArr;
                            inputArr[0]               = iter * ( ( M_PI * 2.0 ) / static_cast<double> ( this->cnSymm.at(dIt)[0] ) );
                            inputArr[1]               = this->cnSymm.at(dIt)[1];
                            inputArr[2]               = this->cnSymm.at(dIt)[2];
                            inputArr[3]               = this->cnSymm.at(dIt)[3];
                            inputArr[4]               = this->cnSymm.at(dIt)[4];
                            std::vector < std::vector<double> > rMat = ProSHADE_internal_misc::getMatrixFromAxisAngle ( inputArr );
                            
                            jsonElements << "\t\"" << ss.str() << "\": \n\t\t{\n";
                            jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
                            jsonElements << "\t\t\t\t\t{ \"m_11\": " << rMat.at(0).at(0) << ", \"m_12\": " << rMat.at(0).at(1) << ", \"m_13\": " << rMat.at(0).at(2) << ", \"m_21\": " << rMat.at(1).at(0) << ", \"m_22\": " << rMat.at(1).at(1) << ", \"m_23\": " << rMat.at(1).at(2) << ", \"m_31\": " << rMat.at(2).at(0) << ", \"m_32\": " << rMat.at(2).at(1) << ", \"m_33\": " << rMat.at(2).at(2) << " }\n";
                            jsonElements << "\t\t\t\t],\n";
                            jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
                            jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
                            jsonElements << "\t\t\t\t]\n";
                            
                            if ( iter == std::ceil( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ) )
                            {
                                jsonElements << "\t\t}\n";
                            }
                            else
                            {
                                jsonElements << "\t\t},\n";
                            }
                        }
                    }
                    else
                    {
                        for ( int iter = -std::floor( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ); iter <= std::floor( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ); iter++ )
                        {
                            if ( iter == 0 ) { continue; }
                            
                            std::stringstream ss;
                            ss << "sym_element_" << symElID;
                            symElID                  += 1;
                            
                            std::array< double, 5 > inputArr;
                            inputArr[0]               = iter * ( ( M_PI * 2.0 ) / static_cast<double> ( this->cnSymm.at(dIt)[0] ) );
                            inputArr[1]               = this->cnSymm.at(dIt)[1];
                            inputArr[2]               = this->cnSymm.at(dIt)[2];
                            inputArr[3]               = this->cnSymm.at(dIt)[3];
                            inputArr[4]               = this->cnSymm.at(dIt)[4];
                            std::vector < std::vector<double> > rMat = ProSHADE_internal_misc::getMatrixFromAxisAngle ( inputArr );
                            
                            jsonElements << "\t\"" << ss.str() << "\": \n\t\t{\n";
                            jsonElements << "\t\t\t\"rotation\":\n\t\t\t\t[\n";
                            jsonElements << "\t\t\t\t\t{ \"m_11\": " << rMat.at(0).at(0) << ", \"m_12\": " << rMat.at(0).at(1) << ", \"m_13\": " << rMat.at(0).at(2) << ", \"m_21\": " << rMat.at(1).at(0) << ", \"m_22\": " << rMat.at(1).at(1) << ", \"m_23\": " << rMat.at(1).at(2) << ", \"m_31\": " << rMat.at(2).at(0) << ", \"m_32\": " << rMat.at(2).at(1) << ", \"m_33\": " << rMat.at(2).at(2) << " }\n";
                            jsonElements << "\t\t\t\t],\n";
                            jsonElements << "\t\t\t\"translation\":\n\t\t\t\t[\n";
                            jsonElements << "\t\t\t\t\t{ \"t_x\": " << this->xPos << ", \"t_y\": " << this->yPos << ", \"t_z\": " << this->zPos << " }\n";
                            jsonElements << "\t\t\t\t]\n";
                            
                            if ( iter == std::floor( static_cast<int> ( this->cnSymm.at(dIt)[0] ) / 2.0 ) )
                            {
                                jsonElements << "\t\t}\n";
                            }
                            else
                            {
                                jsonElements << "\t\t},\n";
                            }
                        }
                    }
                    
                    reqFound = true;
                    break;
                }
            }
            if ( !reqFound )
            {
                std::cout << "!!! ProSHADE WARNING !!! Could not detect the requested cyclic symmetry, but detected cyclic symmetries with different folds. You can try changing the resolution or selecting one of the alternative symmetries printed below.\n\n" << std::endl << std::endl;
                jsonElements << "\t\"no_elements\": 0\n";
            }
        }
        else
        {
            std::cerr << "!!! Warning !!! Could not detect the requested cyclic symmetry. You can try changing the resolution or searching for different symmetry from the alternatives list printed below.\n\n" << std::endl << std::endl;
            jsonElements << "\t\"no_elements\": 0\n";
        }
        
        //==================================== Close the JSON file
        jsonElements << "}\n";
        jsonElements.close                    ( );
    }
    
    //======================================== Done
    return ;
    
}

/*! \brief This function returns a vector of vectors of trace sigma distances between each fragment and the whole database.
 
 This function allows programmatical access to the results of the ProSHADE distances calculations. It returns a
 vector of vectors of trace sigma distances from the fragment to all passing database entries.
 
 \param[out] X Vector of vectors of trace sigma distances.
 
 \warning The results are reported in double precision.
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::vector<double> > ProSHADE_internal::ProSHADE_distances::getFragTraceSigmaDistances ( )
{
    //======================================== Sanity check
    if ( !this->cmpObj->_trSigmaComputed )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in structure comparison. !!! The trace sigma distances were not computed before their values are required." << std::endl;
        exit ( -1 );
    }
    
    //======================================== Return
    return ( this->fragTraceSigmaDistances );
    
}

/*! \brief This function returns a vector of full rotation function distances between the first and all other structures.
 
 This function allows programmatical access to the results of the ProSHADE distances calculations. It returns a
 vector of full rotation function distances from the first structure to all remaining structures. If it contains values -999.9,
 then these mean that this structure did not pass the threshold for energy level or trace sigma descriptors and therefore was not
 subjected to trace sigma descriptor computation.
 
 \param[out] X Vector of full rotation function distances.
 
 \warning The results are reported in double precision.
 \warning This is an internal function which should not be used by the user.
 */
std::vector<double> ProSHADE_internal::ProSHADE_distances::getFullRotationDistances ( )
{
    //======================================== Sanity check
    if ( !this->fullRotFnDist )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in structure comparison. !!! The full rotation function distances were not required when the ProSHADE_distances object was initialised (the setUp structure parameter) and therefore they cannot now be obtained." << std::endl;
        exit ( -1 );
    }
    
    if ( this->cmpObj != nullptr )
    {
        if ( !this->cmpObj->_fullDistComputed )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Error in structure comparison. !!! The full rotation function distances were not computed before their values are required." << std::endl;
            exit ( -1 );
        }
    }
    
    //======================================== Return
    return ( this->fullRotationDistances );
    
}

/*! \brief This function returns a vector of vectors of full rotation function distances between each fragment and the whole database.
 
 This function allows programmatical access to the results of the ProSHADE distances calculations. It returns a
 vector of vectors of full rotation function distances from the fragment to all passing database entries.
 
 \param[out] X Vector of vectors of full rotation function distances.
 
 \warning The results are reported in double precision.
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::vector<double> > ProSHADE_internal::ProSHADE_distances::getFragFullRotationDistances ( )
{
    //======================================== Sanity check
    if ( !this->cmpObj->_fullDistComputed )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in structure comparison. !!! The full rotation function distances were not computed before their values are required." << std::endl;
        exit ( -1 );
    }
    
    //======================================== Return
    return ( this->fragfullRotationDistances );
    
}

/*! \brief This is the constructor for the ProSHADE_mapFeatures executive class the user should use to compute and access map features and splitting.
 
 This constructor takes a single structure (.MAP) name (path) and proceeds to compute the features. This is done by simply calling the appropriate function
 and then doing some sanity checks. The user should use this class to obtain the features as this is the tested and current approach way.
 
 \param[in] settings Instance of the ProSHADE_settings class containing all the settings values.
 
 \warning This is an internal function which should not be used by the user.
 */
ProSHADE_internal::ProSHADE_mapFeatures::ProSHADE_mapFeatures ( ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Default values
    this->fragFilesExist                      = false;
    this->fragFiles.clear                     ( );
    this->one                                 = nullptr;
    
    //======================================== Retain settings
    this->fragBoxSize                         = settings->mapFragBoxSize;
    this->mapFragName                         = settings->mapFragName;
    this->mapFragBoxFraction                  = settings->mapFragBoxFraction;
    
    //======================================== If half-maps processing is required, do it
    if ( settings->taskToPerform == ProSHADE::HalfMaps )
    {
        if ( settings->verbose != 0 )
        {
            std::cout << "-----------------------------------------------------------" << std::endl;
            std::cout << "|                       MODE: HalfMaps                    |" << std::endl;
            std::cout << "-----------------------------------------------------------" << std::endl << std::endl;
        }
        
        //==================================== Sanity checks
        if ( settings->structFiles.size() != 2 )
        {
            std::cerr << "!!! ProSHADE ERROR  !!! Attempted to process half-maps, but did supply ";
            if ( settings->structFiles.size() < 2 ) { std::cerr << "less "; }
            if ( settings->structFiles.size() > 2 ) { std::cerr << "more "; }
            std::cerr << "than 2 files. Please use the \'-f\' option to supply only the two half-maps map files. Terminating ..." << std::endl << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "There are not exactly two input structures. Please supply only two files to the half-maps re-boxing functionality." << "</font>";
                rvapi_set_text                ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                   ( );
            }
            
            exit ( -1 );
        }
        
        if ( settings->clearMapFile == "" )
        {
            std::cout << "!!! ProSHADE WARNING !!! Missing the file name as to where the new half-maps are to be saved. Will use \"proshade_reboxed_half1.map\" and \"proshade_reboxed_half2.map\", however, if these files already exist, they will be over-written." << std::endl << std::endl;
            settings->clearMapFile            = "proshade_reboxed";
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"orange\">" << "Missing the file name as to where the new half-maps are to be saved. Will use 'proshade_reboxed_half1.map' and 'proshade_reboxed_half2.map', however, if these files already exist, they will be over-written." << "</font>";
                rvapi_set_text                ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                   ( );
            }
        }
        
        unsigned int fileTy1                  = checkFileType ( settings->structFiles.at(0) );
        unsigned int fileTy2                  = checkFileType ( settings->structFiles.at(1) );
        if ( ( fileTy1 != 2 ) || ( fileTy2 != 2 ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Cannot recognise one or both of the half-maps format - are you sure these are in the CCP4 MAP format? Terminating..." << std::endl << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "Cannot recognise the file format of the input file ";
                
                if ( ( fileTy1 != 2 ) && ( fileTy2 = 2 ) )
                {
                    hlpSS << settings->structFiles.at(0) << "</font>";
                }
                if ( ( fileTy1 = 2 ) && ( fileTy2 != 2 ) )
                {
                    hlpSS << settings->structFiles.at(1) << "</font>";
                }
                if ( ( fileTy1 != 2 ) && ( fileTy2 != 2 ) )
                {
                    hlpSS << settings->structFiles.at(0) << " and " << settings->structFiles.at(1) << "</font>";
                }
                
                rvapi_set_text                ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                   ( );
            }
            
            exit ( -1 );
        }
        
        //==================================== Deal with the maps
        double xTrans, yTrans, zTrans;
        dealWithHalfMaps ( settings, &xTrans, &yTrans, &zTrans );
        
        if ( settings->verbose > 0 )
        {
            std::cout << std::endl << "-----------------------------------------------------------" << std::endl;
            std::cout << "|                         COMPLETED                       |" << std::endl;
            std::cout << "-----------------------------------------------------------" << std::endl << std::endl;
        }
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "COMPLETED." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        if ( settings->verbose > 0 )
        {
            std::cout << "-----------------------------------------------------------" << std::endl;
            std::cout << "|                          RESULTS                        |" << std::endl;
            std::cout << "-----------------------------------------------------------" << std::endl << std::endl;
            
            std::cout << "Half maps saved into files:" << std::endl << "   " << settings->clearMapFile << "_half1.map" << std::endl << "   " << settings->clearMapFile << "_half2.map" << std::endl << std::endl;
        }
        
        //==================================== Done
        return ;
    }
    
    if ( settings->taskToPerform == ProSHADE::SimpleRebox )
    {        
        unsigned int fileType                 = checkFileType ( settings->structFiles.at(0) );
        if ( fileType != 2 )
        {
            std::cerr << "!!! ProSHADE ERROR !!! The file supplied ( " << settings->structFiles.at(0) << " ) is not recognised as a map formatted file. Please check for file corruption and make sure that it is a MAP formatted file. Terminating ..." << std::endl << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "The file supplied ( " << settings->structFiles.at(0) << " ) is not recognised as a map formatted file. Please check for file corruption and make sure that it is a MAP formatted file." << "</font>";
                rvapi_set_text                ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                   ( );
            }
            
            exit ( -1 );
        }
        
        if ( settings->clearMapFile == "" )
        {
            std::cout << "!!! ProSHADE WARNING !!! Missing the file name as to where the new re-boxed map is to be saved. Will use \"proshade_reboxed.map\", however, if this file already exists, it will be over-written. To supply the file name, please use the \"--clearMap\" command line option." << std::endl << std::endl;
            settings->clearMapFile            = "proshade_reboxed.map";
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"orange\">" << "Missing the file name as to where the new re-boxed map is to be saved. Will use 'proshade_reboxed.map', however, if this file already exists, it will be over-written." << "</font>";
                rvapi_set_text                ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                   ( );
            }
        }
        
        if ( settings->verbose > 3 )
        {
            std::cout << ">>>>>>>> Sanity checks passed." << std::endl;
        }
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Sanity checks passed." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        //==================================== Create the structure object
        this->one                             = new ProSHADE_data ();
        
        //==================================== Run re-boxing
        if ( settings->verbose != 0 )
        {
            std::cout << "Applying the re-boxing algorithm." << std::endl;
        }
        
        std::array<double,6> dims             = one->getDensityMapFromMAPRebox        ( settings->structFiles.at(0),
                                                                                        settings->noIQRsFromMap,
                                                                                        settings->extraSpace,
                                                                                        settings->verbose,
                                                                                        settings->useCubicMaps,
                                                                                        settings->maskBlurFactor,
                                                                                        settings->maskBlurFactorGiven,
                                                                                        settings );
        
        //==================================== Report progress
        if ( settings->verbose > 0 )
        {
            std::cout << std::endl << "-----------------------------------------------------------" << std::endl;
            std::cout << "|                         COMPLETED                       |" << std::endl;
            std::cout << "-----------------------------------------------------------" << std::endl << std::endl;
        }
        
        //======================================== Write out map
        one->writeMap ( settings->clearMapFile, one->_densityMapCor );
        
        if ( settings->verbose > 0 )
        {
            std::cout << "The re-boxed map has been saved to the " << settings->clearMapFile << " file." << std::endl << std::endl;
        }
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "COMPLETED." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        if ( settings->htmlReport )
        {
            rvapi_add_section                 ( "ResultsSection",
                                                "Results",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                true );
            settings->htmlReportLine         += 1;
            
            std::stringstream hlpSS;
            hlpSS << "<pre><b>" << "The re-boxed structure is available at:     </b>" << settings->clearMapFile << "</pre>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ResultsSection",
                                               0,
                                               0,
                                               1,
                                               1 );
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "<pre><b>" << "Original structure dims:                    </b>" << dims[0] << " x " << dims[1] << " x " << dims[2] << "</pre>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ResultsSection",
                                               1,
                                               0,
                                               1,
                                               1 );

            hlpSS.str                         ( std::string ( ) );
            hlpSS << "<pre><b>" << "Re-boxed structure dims:                    </b>" << dims[3] << " x " << dims[4] << " x " << dims[5] << "</pre>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ResultsSection",
                                               2,
                                               0,
                                               1,
                                               1 );
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "<pre><b>" << "New volume as percentage of old volume:     </b>" << ( ( dims[3] * dims[4] * dims[5] ) / ( dims[0] * dims[1] * dims[2] ) ) * 100 << " %</pre>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ResultsSection",
                                               3,
                                               0,
                                               1,
                                               1 );
            
            hlpSS.str                         ( std::string ( ) );
            hlpSS << "<pre><b>" << "Linear processing speed-up:                 </b>" << ( ( dims[0] * dims[1] * dims[2] ) / ( dims[3] * dims[4] * dims[5] ) ) - 1.0 << " times</pre>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ResultsSection",
                                               4,
                                               0,
                                               1,
                                               1 );
        }
        
        //==================================== Done
        return ;
    }
    
    //======================================== Report progress
    if ( settings->verbose > -1 )
    {
        std::cout << "-----------------------------------------------------------" << std::endl;
        std::cout << "|                        MODE: Features                   |" << std::endl;
        std::cout << "-----------------------------------------------------------" << std::endl << std::endl;
    }
    
    //======================================== Initialise valies
    this->maxMapU                             = 0.0;
    this->maxMapV                             = 0.0;
    this->maxMapW                             = 0.0;
    this->xRange                              = 0.0;
    this->yRange                              = 0.0;
    this->zRange                              = 0.0;
    
    //======================================== Create the structure object
    this->one                                 = new ProSHADE_data ();
    
    //======================================== Read in the structure into one
    unsigned int fileType                     = checkFileType ( settings->structFiles.at(0) );
    if ( fileType == 2 )
    {
        if ( settings->verbose != 0 )
        {
            std::cout << "Computing the map features." << std::endl;
        }
        one->getDensityMapFromMAPFeatures     ( settings->structFiles.at(0),
                                               &this->minDensPreNorm,
                                               &this->maxDensPreNorm,
                                               &this->minDensPostNorm,
                                               &this->maxDensPostNorm,
                                               &this->postNormMean,
                                               &this->postNormSdev,
                                               &this->maskVolume,
                                               &this->totalVolume,
                                               &this->maskMean,
                                               &this->maskSdev,
                                               &this->maskMin,
                                               &this->maskMax,
                                               &this->maskDensityRMS,
                                               &this->allDensityRMS,
                                               &this->origRanges,
                                               &this->origDims,
                                                settings->noIQRsFromMap,
                                                settings->extraSpace,
                                                settings->verbose,
                                                settings->useCubicMaps,
                                                settings->maskBlurFactor,
                                                settings->maskBlurFactorGiven,
                                                true,
                                                settings );
    }
    else if ( fileType == 1 )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error loading file " << settings->structFiles.at(0) << " !!! Attempted to obtain map features on PDB file. This is not possible - MAP file is required." << std::endl;
        exit ( -1 );
    }
    else
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error loading file " << settings->structFiles.at(0) << " !!! Cannot detect the extension (currently, only PDB or MAP are allowed) and therefore cannot read the file." << std::endl;
        exit ( -1 );
    }
    
    if ( settings->verbose > 0 )
    {
        std::cout << std::endl << "-----------------------------------------------------------" << std::endl;
        std::cout << "|                         COMPLETED                       |" << std::endl;
        std::cout << "-----------------------------------------------------------" << std::endl << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "COMPLETED." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Write out map, if needed
    if ( settings->clearMapFile != "" )
    {
        one->writeMap ( settings->clearMapFile, one->_densityMapCor );
        
        if ( settings->htmlReport )
        {
            //==================================== Create section
            rvapi_add_section                 ( "FilenameSection",
                                                "Saved file",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                false );
            settings->htmlReportLine         += 1;
            
            std::stringstream hlpSS;
            hlpSS << "<pre>" << "File saved as:                                                         " << settings->clearMapFile << "</pre>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "FilenameSection",
                                                0,
                                                1,
                                                1,
                                                1 );
            
            rvapi_flush                       ( );
        }
    }
    
    //======================================== Done
    
}

/*! \brief This is the ProSHADE_mapFeatures class destructor responsible for releasing memory.
 
 This destructor simply calls the structure data holding object destructor and then is done as this class does not do much
 memory management. Sinple.
 
 \warning This is an internal function which should not be used by the user.
 */
ProSHADE_internal::ProSHADE_mapFeatures::~ProSHADE_mapFeatures ( )
{
    //======================================== Free memory
    if ( this->one != nullptr )
    {
        delete this->one;
    }
}

/*! \brief This function returns the paths to all fragment files saved by fragmentation functionality.
 
 This is a simple accessor function with a check, if the fragments computation was attempted, it will return the list of fragment files (or empty list in case no fragments passed the
 fragmentation criteria); otherwise it will give error stating that fragmentation was not computed and therefore that it makes no sense to want to get the list.
 
 \param[out] X A vector of strings containing the paths to all fragment files created by the fragmentMap function called by this object.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector<std::string> ProSHADE_internal::ProSHADE_mapFeatures::getFragmentsList (  )
{
    //======================================== Sanity check
    if ( !this->fragFilesExist )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Attempted to obtain the list of fragment files without doing the fragmentation first. Terminating..." << std::endl;
        exit                                  ( -1 );
    }
    
    //======================================== If valid, return
    return                                    ( this->fragFiles );
}

/* \brief This function writes out a map in the CCP4 MAP format.
 
 This function writes a density map (given as array of doubles by the second argument) to the file specified by the first argument. It makes use of all the default values in the object, 
 if they are present and will stop execution and print error if they are not. While all the other parameters do have default values in this sense, they can all be set by the user to 
 allow great control over the map output. Internally, the function uses the CCP4's CMAPLIB functionality.
 
 \param[in] fileName String file name of where the map should be saved. Will override if already exists.
 \param[in] map Double pointer to array of map values in the U, V, W order.
 \param[in] axOrder String with the order of the axes. Must have three letters containing any permutation of 'x', 'y' and 'z'. Default value is 'xyz'.
 \param[in] xFrom A value from which the x-axis should start. Can be negative, but should be integer. Float is used for internal purposes only!
 \param[in] yFrom A value from which the y-axis should start. Can be negative, but should be integer. Float is used for internal purposes only!
 \param[in] zFrom A value from which the z-axis should start. Can be negative, but should be integer. Float is used for internal purposes only!
 \param[in] xTo A value to which the x-axis should run to. Can be negative, but should be integer. Float is used for internal purposes only!
 \param[in] yTo A value to which the y-axis should run to. Can be negative, but should be integer. Float is used for internal purposes only!
 \param[in] zTo A value to which the z-axis should run to. Can be negative, but should be integer. Float is used for internal purposes only!
 \param[in] xRange Dimension size (x-axis) in angstroms.
 \param[in] yRange Dimension size (y-axis) in angstroms.
 \param[in] zRange Dimension size (z-axis) in angstroms.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_data::writeMap ( std::string fileName,
                                                  double* map,
                                                  std::string axOrder,
                                                  float xFrom,
                                                  float yFrom,
                                                  float zFrom,
                                                  float xTo,
                                                  float yTo,
                                                  float zTo,
                                                  float xRange,
                                                  float yRange,
                                                  float zRange )
{
    //======================================== Open file for writing
    int myMapMode                             = 1;
    CMap_io::CMMFile *mapFile                 = nullptr;
    mapFile                                   = reinterpret_cast<CMap_io::CMMFile*> ( CMap_io::ccp4_cmap_open ( fileName.c_str() , myMapMode ) );
    if ( mapFile == NULL )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Cannot open file " << fileName << " for writing. Terminating..." << std::endl;
        exit ( -1 );
    }
    
    //======================================== Header: Initialisation
    const char* title                         = "ProSHADE genrated map                                                           ";
    int grid[3];
    int axisOrder[3];
    int dims[3];
    int axFrom[3];
    float cell[6];
    
    //======================================== Header: Decide axis order
    for ( unsigned int iter = 0; iter < 3; iter++ )
    {
        if ( axOrder[iter] == 'x' ) { axisOrder[iter] = 1; }
        if ( axOrder[iter] == 'y' ) { axisOrder[iter] = 2; }
        if ( axOrder[iter] == 'z' ) { axisOrder[iter] = 3; }
    }
    
    //======================================== Header: Default or identical limits?
    if ( std::isinf ( xFrom ) )
    {
        xFrom                                 = this->_xFrom;
        if ( std::isinf ( xFrom ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Did not supply the xFrom parameter to the writeMap function and there is no suitable default in the object. Terminating ..." << std::endl;
            exit ( -1 );
        }
    }
    if ( std::isinf ( yFrom ) )
    {
        yFrom                                 = this->_yFrom;
        if ( std::isinf ( yFrom ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Did not supply the yFrom parameter to the writeMap function and there is no suitable default in the object. Terminating ..." << std::endl;
            exit ( -1 );
        }
    }
    if ( std::isinf ( zFrom ) )
    {
        zFrom                                 = this->_zFrom;
        if ( std::isinf ( zFrom ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Did not supply the zFrom parameter to the writeMap function and there is no suitable default in the object. Terminating ..." << std::endl;
            exit ( -1 );
        }
    }
    if ( std::isinf ( xTo ) )
    {
        xTo                                   = this->_xTo;
        if ( std::isinf ( xTo ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Did not supply the xTo parameter to the writeMap function and there is no suitable default in the object. Terminating ..." << std::endl;
            exit ( -1 );
        }
    }
    if ( std::isinf ( yTo ) )
    {
        yTo                                   = this->_yTo;
        if ( std::isinf ( yTo ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Did not supply the yTo parameter to the writeMap function and there is no suitable default in the object. Terminating ..." << std::endl;
            exit ( -1 );
        }
    }
    if ( std::isinf ( zTo ) )
    {
        zTo                                   = this->_zTo;
        if ( std::isinf ( zTo ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Did not supply the zTo parameter to the writeMap function and there is no suitable default in the object. Terminating ..." << std::endl;
            exit ( -1 );
        }
    }
    
    //======================================== Header: Decide axis order, grid, axis limits and dimentions
    grid[axisOrder[0]-1] = xTo - xFrom + 1;
    grid[axisOrder[1]-1] = yTo - yFrom + 1;
    grid[axisOrder[2]-1] = zTo - zFrom + 1;
    
    axFrom[axisOrder[0]-1]                    = xFrom;
    axFrom[axisOrder[1]-1]                    = yFrom;
    axFrom[axisOrder[2]-1]                    = zFrom;
    
    dims[0]                                   = this->_maxMapU + 1;
    dims[1]                                   = this->_maxMapV + 1;
    dims[2]                                   = this->_maxMapW + 1;
    
    //======================================== Header: Get cell parameters
    if ( std::isinf ( xRange ) )
    {
        xRange                                = this->_xRange;
        if ( std::isinf ( xRange ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Did not supply the xRange parameter to the writeMap function and there is no suitable default in the object. Terminating ..." << std::endl;
            exit ( -1 );
        }
    }
    if ( std::isinf ( yRange ) )
    {
        yRange                                = this->_yRange;
        if ( std::isinf ( yRange ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Did not supply the yRange parameter to the writeMap function and there is no suitable default in the object. Terminating ..." << std::endl;
            exit ( -1 );
        }
    }
    if ( std::isinf ( zRange ) )
    {
        zRange                                = this->_zRange;
        if ( std::isinf ( zRange ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Did not supply the zRange parameter to the writeMap function and there is no suitable default in the object. Terminating ..." << std::endl;
            exit ( -1 );
        }
    }
    
    cell[0]                                   = xRange;
    cell[1]                                   = yRange;
    cell[2]                                   = zRange;
    cell[3]                                   = static_cast<float> ( 90.0 );
    cell[4]                                   = static_cast<float> ( 90.0 );
    cell[5]                                   = static_cast<float> ( 90.0 );
    
    //======================================== Header: Write the map header
    CMap_io::ccp4_cmap_set_cell               ( mapFile, cell );
    CMap_io::ccp4_cmap_set_grid               ( mapFile, grid );
    CMap_io::ccp4_cmap_set_order              ( mapFile, axisOrder );
    CMap_io::ccp4_cmap_set_dim                ( mapFile, dims );
    CMap_io::ccp4_cmap_set_origin             ( mapFile, axFrom );
    
    CMap_io::ccp4_cmap_set_spacegroup         ( mapFile, 1 );
    CMap_io::ccp4_cmap_set_title              ( mapFile, title );
    CMap_io::ccp4_cmap_set_datamode           ( mapFile, 2 );
    
    //======================================== Data: Write the map data
    std::vector<float> section( dims[axisOrder[1]-1] * dims[axisOrder[0]-1] );
    int index;
    int iters[3];
    int arrPos;
    
    for ( iters[2] = 0; iters[2] < dims[axisOrder[2]-1]; iters[2]++ )
    {
        index = 0;
        
        for ( iters[1] = 0; iters[1] < dims[axisOrder[1]-1]; iters[1]++ )
        {
            for ( iters[0] = 0; iters[0] < dims[axisOrder[0]-1]; iters[0]++ )
            {
                arrPos                        = iters[2]  + (dims[axisOrder[2]-1]) * ( iters[1]  + (dims[axisOrder[1]-1]) * iters[0] );
                
                section[ index++ ]            = static_cast<float> ( map[arrPos] );
            }
        }
        
        CMap_io::ccp4_cmap_write_section( mapFile, &section[0] );
    }
    
    CMap_io::ccp4_cmap_close ( mapFile );
    
    //======================================== Done
    return ;
    
}

/* \brief This function writes out a map in the CCP4 MAP format.
 
 This function writes a density map (given as array of floats by the second argument) to the file specified by the first argument. It makes use of all the default values in the object, 
 if they are present and will stop execution and print error if they are not. While all the other parameters do have default values in this sense, they can all be set by the user to 
 allow great control over the map output. Internally, the function uses the CCP4's CMAPLIB functionality.
 
 \param[in] fileName String file name of where the map should be saved. Will override if already exists.
 \param[in] map Float pointer to array of map values in the U, V, W order.
 \param[in] axOrder String with the order of the axes. Must have three letters containing any permutation of 'x', 'y' and 'z'. Default value is 'xyz'.
 \param[in] xFrom A value from which the x-axis should start. Can be negative, but should be integer. Float is used for internal purposes only!
 \param[in] yFrom A value from which the y-axis should start. Can be negative, but should be integer. Float is used for internal purposes only!
 \param[in] zFrom A value from which the z-axis should start. Can be negative, but should be integer. Float is used for internal purposes only!
 \param[in] xTo A value to which the x-axis should run to. Can be negative, but should be integer. Float is used for internal purposes only!
 \param[in] yTo A value to which the y-axis should run to. Can be negative, but should be integer. Float is used for internal purposes only!
 \param[in] zTo A value to which the z-axis should run to. Can be negative, but should be integer. Float is used for internal purposes only!
 \param[in] xRange Dimension size (x-axis) in angstroms.
 \param[in] yRange Dimension size (y-axis) in angstroms.
 \param[in] zRange Dimension size (z-axis) in angstroms.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_data::writeMap ( std::string fileName,
                                                  float* map,
                                                  std::string axOrder,
                                                  float xFrom,
                                                  float yFrom,
                                                  float zFrom,
                                                  float xTo,
                                                  float yTo,
                                                  float zTo,
                                                  float xRange,
                                                  float yRange,
                                                  float zRange )
{
    //======================================== Open file for writing
    int myMapMode                             = 1;
    CMap_io::CMMFile *mapFile                 = nullptr;
    mapFile                                   = reinterpret_cast<CMap_io::CMMFile*> ( CMap_io::ccp4_cmap_open ( fileName.c_str() , myMapMode ) );
    if ( mapFile == NULL )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Cannot open file " << fileName << " for writing. Terminating..." << std::endl;
        exit ( -1 );
    }
    
    //======================================== Header: Initialisation
    const char* title                         = "ProSHADE genrated map                                                           ";
    int grid[3];
    int axisOrder[3];
    int dims[3];
    int axFrom[3];
    float cell[6];
    
    //======================================== Header: Decide axis order
    for ( unsigned int iter = 0; iter < 3; iter++ )
    {
        if ( axOrder[iter] == 'x' ) { axisOrder[iter] = 1; }
        if ( axOrder[iter] == 'y' ) { axisOrder[iter] = 2; }
        if ( axOrder[iter] == 'z' ) { axisOrder[iter] = 3; }
    }
    
    //======================================== Header: Default or identical limits?
    if ( std::isinf ( xFrom ) )
    {
        xFrom                                 = this->_xFrom;
        if ( std::isinf ( xFrom ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Did not supply the xFrom parameter to the writeMap function and there is no suitable default in the object. Terminating ..." << std::endl;
            exit ( -1 );
        }
    }
    if ( std::isinf ( yFrom ) )
    {
        yFrom                                 = this->_yFrom;
        if ( std::isinf ( yFrom ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Did not supply the yFrom parameter to the writeMap function and there is no suitable default in the object. Terminating ..." << std::endl;
            exit ( -1 );
        }
    }
    if ( std::isinf ( zFrom ) )
    {
        zFrom                                 = this->_zFrom;
        if ( std::isinf ( zFrom ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Did not supply the zFrom parameter to the writeMap function and there is no suitable default in the object. Terminating ..." << std::endl;
            exit ( -1 );
        }
    }
    if ( std::isinf ( xTo ) )
    {
        xTo                                   = this->_xTo;
        if ( std::isinf ( xTo ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Did not supply the xTo parameter to the writeMap function and there is no suitable default in the object. Terminating ..." << std::endl;
            exit ( -1 );
        }
    }
    if ( std::isinf ( yTo ) )
    {
        yTo                                   = this->_yTo;
        if ( std::isinf ( yTo ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Did not supply the yTo parameter to the writeMap function and there is no suitable default in the object. Terminating ..." << std::endl;
            exit ( -1 );
        }
    }
    if ( std::isinf ( zTo ) )
    {
        zTo                                   = this->_zTo;
        if ( std::isinf ( zTo ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Did not supply the zTo parameter to the writeMap function and there is no suitable default in the object. Terminating ..." << std::endl;
            exit ( -1 );
        }
    }
    
    //======================================== Header: Decide axis order, grid, axis limits and dimentions
    grid[axisOrder[0]-1]                      = xTo - xFrom + 1;
    grid[axisOrder[1]-1]                      = yTo - yFrom + 1;
    grid[axisOrder[2]-1]                      = zTo - zFrom + 1;
    
    axFrom[axisOrder[0]-1]                    = xFrom + 1;
    axFrom[axisOrder[1]-1]                    = yFrom + 1;
    axFrom[axisOrder[2]-1]                    = zFrom + 1;
    
    dims[axisOrder[0]-1]                      = xTo - xFrom + 1;
    dims[axisOrder[1]-1]                      = yTo - yFrom + 1;
    dims[axisOrder[2]-1]                      = zTo - zFrom + 1;
    
    //======================================== Header: Get cell parameters
    if ( std::isinf ( xRange ) )
    {
        xRange                                = this->_xRange;
        if ( std::isinf ( xRange ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Did not supply the xRange parameter to the writeMap function and there is no suitable default in the object. Terminating ..." << std::endl;
            exit ( -1 );
        }
    }
    if ( std::isinf ( yRange ) )
    {
        yRange                                = this->_yRange;
        if ( std::isinf ( yRange ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Did not supply the yRange parameter to the writeMap function and there is no suitable default in the object. Terminating ..." << std::endl;
            exit ( -1 );
        }
    }
    if ( std::isinf ( zRange ) )
    {
        zRange                                = this->_zRange;
        if ( std::isinf ( zRange ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Did not supply the zRange parameter to the writeMap function and there is no suitable default in the object. Terminating ..." << std::endl;
            exit ( -1 );
        }
    }
    
    cell[0]                                   = xRange;
    cell[1]                                   = yRange;
    cell[2]                                   = zRange;
    cell[3]                                   = static_cast<float> ( 90.0 );
    cell[4]                                   = static_cast<float> ( 90.0 );
    cell[5]                                   = static_cast<float> ( 90.0 );
    
    //======================================== Header: Write the map header
    CMap_io::ccp4_cmap_set_cell               ( mapFile, cell );
    CMap_io::ccp4_cmap_set_grid               ( mapFile, grid );
    CMap_io::ccp4_cmap_set_order              ( mapFile, axisOrder );
    CMap_io::ccp4_cmap_set_dim                ( mapFile, dims );
    CMap_io::ccp4_cmap_set_origin             ( mapFile, axFrom );
    
    CMap_io::ccp4_cmap_set_spacegroup         ( mapFile, 1 );
    CMap_io::ccp4_cmap_set_title              ( mapFile, title );
    CMap_io::ccp4_cmap_set_datamode           ( mapFile, 2 );
    
    //======================================== Data: Write the map data
    std::vector<float> section( dims[axisOrder[1]-1] * dims[axisOrder[0]-1] );
    int index;
    int iters[3];
    int arrPos;
    
    for ( iters[2] = 0; iters[2] < dims[axisOrder[2]-1]; iters[2]++ )
    {
        index = 0;
        
        for ( iters[1] = 0; iters[1] < dims[axisOrder[1]-1]; iters[1]++ )
        {
            for ( iters[0] = 0; iters[0] < dims[axisOrder[0]-1]; iters[0]++ )
            {
                arrPos                        = iters[2]  + (dims[axisOrder[2]-1]) * ( iters[1]  + (dims[axisOrder[1]-1]) * iters[0] );
                
                section[ index++ ]            = static_cast<float> ( map[arrPos] );
            }
        }
        
        CMap_io::ccp4_cmap_write_section( mapFile, &section[0] );
    }
    
    CMap_io::ccp4_cmap_close ( mapFile );
    
    //======================================== Done
    return ;
    
}

/* \brief This function writes out a map in the CCP4 MAP format.
 
 This function writes a density map (given as array of floats by the second argument) to the file specified by the first argument. It makes use of all the default values in the object,
 if they are present and will stop execution and print error if they are not. While all the other parameters do have default values in this sense, they can all be set by the user to
 allow great control over the map output. Internally, the function uses the CCP4's CMAPLIB functionality.
 
 \param[in] fileName String file name of where the map should be saved. Will override if already exists.
 \param[in] map Float pointer to array of map values in the U, V, W order.
 \param[in] axOrder String with the order of the axes. Must have three letters containing any permutation of 'x', 'y' and 'z'. Default value is 'xyz'.
 \param[in] xFrom A value from which the x-axis should start. Can be negative, but should be integer. Float is used for internal purposes only!
 \param[in] yFrom A value from which the y-axis should start. Can be negative, but should be integer. Float is used for internal purposes only!
 \param[in] zFrom A value from which the z-axis should start. Can be negative, but should be integer. Float is used for internal purposes only!
 \param[in] xTo A value to which the x-axis should run to. Can be negative, but should be integer. Float is used for internal purposes only!
 \param[in] yTo A value to which the y-axis should run to. Can be negative, but should be integer. Float is used for internal purposes only!
 \param[in] zTo A value to which the z-axis should run to. Can be negative, but should be integer. Float is used for internal purposes only!
 \param[in] xRange Dimension size (x-axis) in angstroms.
 \param[in] yRange Dimension size (y-axis) in angstroms.
 \param[in] zRange Dimension size (z-axis) in angstroms.
 
 \warning This is an internal function which should not be used by the user.
 */

void ProSHADE_internal::ProSHADE_data::writePDB ( std::string templateName,
                                                  std::string outputName,
                                                  double rotEulA,
                                                  double rotEulB,
                                                  double rotEulG,
                                                  double trsX,
                                                  double trsY,
                                                  double trsZ )
{
    //======================================== Read in PDB template file
    clipper::mmdb::CMMDBManager *mfile        = new clipper::mmdb::CMMDBManager ( );
    if ( mfile->ReadCoorFile ( templateName.c_str() ) )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Cannot read file: " << templateName.c_str() << std::endl;
        exit ( -1 );
    }
    
    //======================================== Initialise Centre location
    double maxX                               = 0.0;
    double minX                               = 0.0;
    double maxY                               = 0.0;
    double minY                               = 0.0;
    double maxZ                               = 0.0;
    double minZ                               = 0.0;
    bool firstAtom                            = true;
    
    //======================================== Initialise MMDB crawl
    int noChains                              = 0;
    int noResidues                            = 0;
    int noAtoms                               = 0;
    
    clipper::mmdb::PPCChain chain;
    clipper::mmdb::PPCResidue residue;
    clipper::mmdb::PPCAtom atom;
    
    std::vector < std::array<double,3> > atCoords;
    std::array<double,3> hlpArr;
    
    //======================================== Get all chains
    mfile->GetChainTable                      ( 1, chain, noChains );
    firstAtom                                 = true;
    for ( unsigned int nCh = 0; nCh < static_cast<unsigned int> ( noChains ); nCh++ )
    {
        if ( chain[nCh] )
        {
            //================================ Get all residues
            chain[nCh]->GetResidueTable       ( residue, noResidues );
            
            for ( unsigned int nRes = 0; nRes < static_cast<unsigned int> ( noResidues ); nRes++ )
            {
                if ( residue[nRes] )
                {
                    //======================== Get all atoms
                    residue[nRes]->GetAtomTable ( atom, noAtoms );
                    
                    for ( unsigned int aNo = 0; aNo < static_cast<unsigned int> ( noAtoms ); aNo++ )
                    {
                        if ( atom[aNo] )
                        {
                            //================ Check for termination 'residue'
                            if ( atom[aNo]->Ter ) { continue; }
                            
                            //================ Save coords for easier manipulation
                            hlpArr[0]         = atom[aNo]->x;
                            hlpArr[1]         = atom[aNo]->y;
                            hlpArr[2]         = atom[aNo]->z;
                            atCoords.emplace_back ( hlpArr );
                            
                            //================ Find axis maxs and mins
                            if ( firstAtom )
                            {
                                maxX          = atom[aNo]->x;
                                minX          = atom[aNo]->x;
                                maxY          = atom[aNo]->y;
                                minY          = atom[aNo]->y;
                                maxZ          = atom[aNo]->z;
                                minZ          = atom[aNo]->z;
                                firstAtom     = false;
                            }
                            else
                            {
                                if ( atom[aNo]->x > maxX )  { maxX = atom[aNo]->x; } if ( atom[aNo]->x < minX ) { minX = atom[aNo]->x; }
                                if ( atom[aNo]->y > maxY )  { maxY = atom[aNo]->y; } if ( atom[aNo]->y < minY ) { minY = atom[aNo]->y; }
                                if ( atom[aNo]->z > maxZ )  { maxZ = atom[aNo]->z; } if ( atom[aNo]->z < minZ ) { minZ = atom[aNo]->z; }
                            }
                        }
                    }
                }
            }
        }
    }
    
    //======================================== Apply the rotation if need be
    if ( ( rotEulA != 0.0 ) || ( rotEulB != 0.0 ) || ( rotEulG != 0.0 ) )
    {
        // ... center on centre of cell
        double xRng                           = ( maxX - minX ) / 2.0;
        double yRng                           = ( maxY - minY ) / 2.0;
        double zRng                           = ( maxZ - minZ ) / 2.0;
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( atCoords.size() ); iter++ )
        {
            atCoords.at(iter)[0]              = ( atCoords.at(iter)[0] - minX ) - xRng;
            atCoords.at(iter)[1]              = ( atCoords.at(iter)[1] - minY ) - yRng;
            atCoords.at(iter)[2]              = ( atCoords.at(iter)[2] - minZ ) - zRng;
        }
        
        // ... get rotation matrix
        std::vector< std::vector < double > > rMat;
        std::vector< double > hlpVec;
        hlpVec.emplace_back                   (  cos ( rotEulA ) * cos ( rotEulB  ) * cos ( rotEulG ) - sin ( rotEulA ) * sin ( rotEulG ) );
        hlpVec.emplace_back                   (  sin ( rotEulA ) * cos ( rotEulB  ) * cos ( rotEulG ) + cos ( rotEulA ) * sin ( rotEulG ) );
        hlpVec.emplace_back                   ( -sin ( rotEulB  ) * cos ( rotEulG ) );
        rMat.emplace_back                     ( hlpVec ); hlpVec.clear();
        
        hlpVec.emplace_back                   ( -cos ( rotEulA ) * cos ( rotEulB  ) * sin ( rotEulG ) - sin ( rotEulA ) * cos ( rotEulG ) );
        hlpVec.emplace_back                   ( -sin ( rotEulA ) * cos ( rotEulB  ) * sin ( rotEulG ) + cos ( rotEulA ) * cos ( rotEulG ) );
        hlpVec.emplace_back                   (  sin ( rotEulB  ) * sin ( rotEulG ) );
        rMat.emplace_back                     ( hlpVec ); hlpVec.clear();
        
        hlpVec.emplace_back                   (  cos ( rotEulA ) * sin ( rotEulB  ) );
        hlpVec.emplace_back                   (  sin ( rotEulA ) * sin ( rotEulB  ) );
        hlpVec.emplace_back                   (  cos ( rotEulB  ) );
        rMat.emplace_back                     ( hlpVec ); hlpVec.clear();
        
        // ... rotate coords
        double hlpXVal                        = 0.0;
        double hlpYVal                        = 0.0;
        double hlpZVal                        = 0.0;
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( atCoords.size() ); iter++ )
        {
            hlpXVal                           = ( atCoords.at(iter)[0] * rMat.at(0).at(0) ) + ( atCoords.at(iter)[1] * rMat.at(1).at(0) ) + ( atCoords.at(iter)[2] * rMat.at(2).at(0) );
            hlpYVal                           = ( atCoords.at(iter)[0] * rMat.at(0).at(1) ) + ( atCoords.at(iter)[1] * rMat.at(1).at(1) ) + ( atCoords.at(iter)[2] * rMat.at(2).at(1) );
            hlpZVal                           = ( atCoords.at(iter)[0] * rMat.at(0).at(2) ) + ( atCoords.at(iter)[1] * rMat.at(1).at(2) ) + ( atCoords.at(iter)[2] * rMat.at(2).at(2) );
            
            atCoords.at(iter)[0]              = hlpXVal;
            atCoords.at(iter)[1]              = hlpYVal;
            atCoords.at(iter)[2]              = hlpZVal;
        }
        
        // ... move coords back to original positions
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( atCoords.size() ); iter++ )
        {
            atCoords.at(iter)[0]              = ( atCoords.at(iter)[0] + xRng ) + minX;
            atCoords.at(iter)[1]              = ( atCoords.at(iter)[1] + yRng ) + minY;
            atCoords.at(iter)[2]              = ( atCoords.at(iter)[2] + zRng ) + minZ;
        }
    }
    
    //======================================== Translate coordinates as required
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( atCoords.size() ); iter++ )
    {
        atCoords.at(iter)[0]                  = atCoords.at(iter)[0] + trsX;
        atCoords.at(iter)[1]                  = atCoords.at(iter)[1] + trsY;
        atCoords.at(iter)[2]                  = atCoords.at(iter)[2] + trsZ;
    }
    
    //======================================== Save coords back to the PDB structure
    unsigned int atIt                         = 0;
    for ( unsigned int nCh = 0; nCh < static_cast<unsigned int> ( noChains ); nCh++ )
    {
        if ( chain[nCh] )
        {
            //================================ Get all residues
            chain[nCh]->GetResidueTable       ( residue, noResidues );
            
            for ( unsigned int nRes = 0; nRes < static_cast<unsigned int> ( noResidues ); nRes++ )
            {
                if ( residue[nRes] )
                {
                    //======================== Get all atoms
                    residue[nRes]->GetAtomTable ( atom, noAtoms );
                    
                    for ( unsigned int aNo = 0; aNo < static_cast<unsigned int> ( noAtoms ); aNo++ )
                    {
                        if ( atom[aNo] )
                        {
                            //================ Check for termination 'residue'
                            if ( atom[aNo]->Ter ) { continue; }
                            
                            //================ Save coords for easier manipulation
                            atom[aNo]->SetCoordinates ( atCoords.at(atIt)[0],
                                                        atCoords.at(atIt)[1],
                                                        atCoords.at(atIt)[2],
                                                        atom[aNo]->occupancy,
                                                        atom[aNo]->tempFactor );
                            atIt             += 1;
                            
                        }
                    }
                }
            }
        }
    }
    
    //======================================== Write the PDB file
    if ( mfile->WritePDBASCII ( outputName.c_str() ) )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Failed to write out PDB file " << outputName.c_str() << "." << std::endl;
        exit ( -1 );
    }
    
    //======================================== Done
    return ;
}

/* \brief This function deletes a PDB model from the map densiry.
 
 This function takes a PDB file and deletes its density from the map density saved in this ProSHADE_data object. This is used to stop ProSHADE from searching matches between locations
 which may already have model assigned.
 .
 \param[in] modelPath The path to the PDB file containing the model.
 
 \warning This is an internal function which should not be used by the user.
 */

void ProSHADE_internal::ProSHADE_data::deleteModel ( std::string modelPath )
{
    //======================================== Init
    double vanDerWaals                        = 1.7;
    double smoothing                          = vanDerWaals * 2.0;
    
    //======================================== Read in file
    clipper::mmdb::CMMDBManager *mfile        = new clipper::mmdb::CMMDBManager ( );
    if ( mfile->ReadCoorFile ( modelPath.c_str() ) )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Cannot read model file: " << modelPath.c_str() << std::endl;
        exit ( -1 );
    }
    
    //======================================== Initialise MMDB crawl
    int noChains                              = 0;
    int noResidues                            = 0;
    int noAtoms                               = 0;
    std::vector<std::array<double,3>> coords;
    std::array<double,3> hlpArr;
    
    clipper::mmdb::PPCChain chain;
    clipper::mmdb::PPCResidue residue;
    clipper::mmdb::PPCAtom atom;

    //======================================== Get all chains
    mfile->GetChainTable                      ( 1, chain, noChains );
    for ( unsigned int nCh = 0; nCh < static_cast<unsigned int> ( noChains ); nCh++ )
    {
        if ( chain[nCh] )
        {
            //================================ Get all residues
            chain[nCh]->GetResidueTable       ( residue, noResidues );

            for ( unsigned int nRes = 0; nRes < static_cast<unsigned int> ( noResidues ); nRes++ )
            {
                if ( residue[nRes] )
                {
                    //======================== Get all atoms
                    residue[nRes]->GetAtomTable ( atom, noAtoms );

                    for ( unsigned int aNo = 0; aNo < static_cast<unsigned int> ( noAtoms ); aNo++ )
                    {
                        if ( atom[aNo] )
                        {
                            //================ Check for termination 'residue'
                            if ( atom[aNo]->Ter ) { continue; }

                            //================ Save the coords
                            hlpArr[0]         = atom[aNo]->x;
                            hlpArr[1]         = atom[aNo]->y;
                            hlpArr[2]         = atom[aNo]->z;
                            
                            //================ And save to vector
                            coords.emplace_back ( hlpArr );
                        }
                    }
                }
            }
        }
    }
    
    //======================================== Free memory
    delete mfile;
    
    //======================================== Allocate and pre-set bew mask
    double* remMask                           = new double[(this->_maxMapU + 1) * (this->_maxMapV + 1) * (this->_maxMapW + 1)];
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (this->_maxMapU + 1) * (this->_maxMapV + 1) * (this->_maxMapW + 1) ); iter++ )
    {
        remMask[iter]                         = 1.0;
    }
    
    //======================================== For each map position
    double xFromSM                            = 0.0;
    double xToSM                              = 0.0;
    double yFromSM                            = 0.0;
    double yToSM                              = 0.0;
    double zFromSM                            = 0.0;
    double zToSM                              = 0.0;
    
    double xRealStart                         = ( this->_xFrom  / static_cast<double>( this->_maxMapU + 1 ) ) * this->_xRange;
    double yRealStart                         = ( this->_yFrom  / static_cast<double>( this->_maxMapV + 1 ) ) * this->_yRange;
    double zRealStart                         = ( this->_zFrom  / static_cast<double>( this->_maxMapW + 1 ) ) * this->_zRange;
    
    double xRealStep                          = 1.0  / static_cast<double>( this->_maxMapU + 1 ) * this->_xRange;
    double yRealStep                          = 1.0  / static_cast<double>( this->_maxMapV + 1 ) * this->_yRange;
    double zRealStep                          = 1.0  / static_cast<double>( this->_maxMapW + 1 ) * this->_zRange;
    
    double xHlp                               = 0.0;
    double yHlp                               = 0.0;
    double zHlp                               = 0.0;
    
    double dist                               = 0.0;
    
    int arrPos                                = 0;
    int uIter                                 = 0;
    int vIter                                 = 0;
    int wIter                                 = 0;

    
    for ( int cIt = 0; cIt < static_cast<int> ( coords.size() ); cIt++ )
    {
        xFromSM                               = coords.at(cIt)[0] - smoothing;
        xToSM                                 = coords.at(cIt)[0] + smoothing;
        yFromSM                               = coords.at(cIt)[1] - smoothing;
        yToSM                                 = coords.at(cIt)[1] + smoothing;
        zFromSM                               = coords.at(cIt)[2] - smoothing;
        zToSM                                 = coords.at(cIt)[2] + smoothing;
        
        uIter                                 = 0;
        for ( double uIt = xRealStart; uIt < static_cast<double> ( ( this->_xTo / static_cast<double>( this->_maxMapU + 1 ) )  * this->_xRange ); uIt += xRealStep )
        {
            if ( ( uIt < xFromSM ) || ( uIt > xToSM ) ) { uIter += 1; continue; }
            xHlp                              = pow ( uIt - coords.at(cIt)[0], 2.0 );
            
            vIter                             = 0;
            for ( double vIt = yRealStart; vIt < static_cast<double> ( ( this->_yTo / static_cast<double>( this->_maxMapV + 1 ) ) * this->_yRange ); vIt += yRealStep )
            {
                if ( ( vIt < yFromSM ) || ( vIt > yToSM ) ) { vIter += 1; continue; }
                yHlp                          = pow ( vIt - coords.at(cIt)[1], 2.0 );
                
                wIter                         = 0;
                for ( double wIt = zRealStart; wIt < static_cast<double> ( ( this->_zTo / static_cast<double>( this->_maxMapW + 1 ) ) * this->_zRange ); wIt += zRealStep )
                {
                    if ( ( wIt < zFromSM ) || ( wIt > zToSM ) ) { wIter += 1; continue; }
                    zHlp                      = pow ( wIt - coords.at(cIt)[2], 2.0 );
                    
                    arrPos                    = wIter  + (this->_maxMapW + 1) * ( vIter  + (this->_maxMapV + 1) * uIter );
                    dist                      = sqrt ( xHlp + yHlp + zHlp );
                    
                    if ( dist < vanDerWaals ) { remMask[arrPos] = 0.0; continue; }
                    if ( dist < smoothing )
                    {
                        remMask[arrPos]       = ( 1.0 - cos ( ( M_PI * ( dist - vanDerWaals ) ) / ( smoothing - vanDerWaals ) ) ) / 2.0;
                    }
                    
                    wIter                    += 1;
                }
                
                vIter                        += 1;
            }
            
            uIter                            += 1;
        }
    }
    
    //======================================== Delete and see?
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (this->_maxMapU + 1) * (this->_maxMapV + 1) * (this->_maxMapW + 1) ); iter++ )
    {
        this->_densityMapMap[iter]           *= remMask[iter];
    }
    
    //======================================== Free memory
    delete[] remMask;
    
    //======================================== Done
    return ;
    
}
