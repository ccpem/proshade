/*! \file ProSHADE_clipper.cpp
 \brief This file contains the function definitions for all functions interfacing clipper and processing the data from it.
 
 This cpp file contains the functions to read in data from both, PDB and MAP files as well
 as functions to deal with the processed data up to the point of computing spherical harmoncis. 
 
 This file is part of the ProSHADE library for calculating
 shape descriptors and symmetry operators of protein structures.
 This is a prototype code, which is by no means complete or fully
 tested. Its use is at your own risk only. There is no quarantee
 that the results are correct.
 
 \author    Michal Tykac
 \author    Garib N. Murshudov
 \version   0.6.6
 \date      JAN 2019
 */

//============================================ Clipper
#include <clipper/clipper.h>
#include <clipper/clipper-contrib.h>
#include <clipper/clipper-ccp4.h>
#include <clipper/clipper-mmdb.h>
#include <clipper/clipper-minimol.h>

//============================================ FFTW3 + SOFT
#ifdef __cplusplus
extern "C" {
#endif
#include <fftw3.h>
#include <wrap_fftw.h>
#include <makeweights.h>
#include <s2_primitive.h>
#include <s2_cospmls.h>
#include <s2_legendreTransforms.h>
#include <s2_semi_fly.h>
#include <rotate_so3_utils.h>
#include <utils_so3.h>
#include <soft_fftw.h>
#include <rotate_so3_fftw.h>
#ifdef __cplusplus
}
#endif

//============================================ CMAPLIB
#include <cmaplib.h>

//============================================ RVAPI
#include <rvapi_interface.h>

//============================================ ProSHADE
#include "ProSHADE.h"
#include "ProSHADE_internal.h"
#include "ProSHADE_misc.h"

/*! \brief Function to read in the PDB file and compute the theoretical density map.
 
 This function reads in the coordinates from the PDB file, centers them in the cell and
 computes the theoretical density map. It also estimates the optimal settings if the user
 did not enter their own.
 
 \param[in] fileName The file name (and path) to the PDB file to be read in.
 \param[in] shellDistance The intended distance between shells.
 \param[in] resolution The resolution to which the map should be computed. This is the main parameter from which all other are derived.
 \param[in] bandwidth A pointer to variable which will hold the optimal bandwidth as determined by the function or the value supplied by the user.
 \param[in] theta A pointer to variable which will hold the optimal theta angle as determined by the function or the value supplied by the user.
 \param[in] phi A pointer to variable which will hold the optimal phi angle as determined by the function or the value supplied by the user.
 \param[in] extraSpace A pointer to variable which will hold the optimal between cells distance as determined by the function or the value supplied by the user.
 \param[in] mapResDefault A bool value specifying whether the resolution value supplied is the default, or whether it was supplied by the user.
 \param[in] settings The ProSHADE_settings container class with information about how to write the HTML report.
 \param[in] Bfactor Value to which all atomic B-factors will be changed. Default value is 80.
 \param[in] hpFirstLineCom Boolean value whether the first line of the PDB should be considered the center of mass. Leave with default false unless you know what you are doing.
 \param[in] overlayDefaults A bool value specifying whether the resulting map will be used for map overlay mode. If so, the shell distances are doubled to speed things up.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_data::getDensityMapFromPDB ( std::string fileName,
                                                              double *shellDistance,
                                                              double resolution,
                                                              unsigned int *bandwidth,
                                                              unsigned int *theta,
                                                              unsigned int *phi,
                                                              unsigned int *glIntegOrder,
                                                              double *extraSpace,
                                                              bool mapResDefault,
                                                              ProSHADE::ProSHADE_settings* settings,
                                                              double Bfactor,
                                                              bool hpFirstLineCom,
                                                              bool overlayDefaults )
{
    //======================================== Initialise internal values
    this->_inputFileName                      = fileName;
    this->_mapResolution                      = resolution;
    this->_shellSpacing                       = *shellDistance;
    this->_maxExtraCellularSpace              = *extraSpace;
    this->_firstLineCOM                       = hpFirstLineCom;
    
    //======================================== Read in file
    clipper::mmdb::CMMDBManager *mfile        = new clipper::mmdb::CMMDBManager ( );
    if ( mfile->ReadCoorFile ( this->_inputFileName.c_str() ) )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Cannot read file: " << this->_inputFileName.c_str() << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Cannot open file: " << this->_inputFileName.c_str() << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Find axis ranges & change B-factors if required
    double maxX                               = 0.0;
    double minX                               = 0.0;
    double maxY                               = 0.0;
    double minY                               = 0.0;
    double maxZ                               = 0.0;
    double minZ                               = 0.0;
    bool firstAtom                            = true;
    
    double xVal, yVal, zVal;
    
    //======================================== Initialise MMDB crawl
    int noChains                              = 0;
    int noResidues                            = 0;
    int noAtoms                               = 0;
    
    clipper::mmdb::PPCChain chain;
    clipper::mmdb::PPCResidue residue;
    clipper::mmdb::PPCAtom atom;
    
    double pdbXCom                            = 0.0;
    double pdbYCom                            = 0.0;
    double pdbZCom                            = 0.0;
    double bFacTot                            = 0.0;
    
    //======================================== Get all chains
    mfile->GetChainTable                      ( 1, chain, noChains );
    firstAtom                                 = true;
    for ( unsigned int nCh = 0; nCh < static_cast<unsigned int> ( noChains ); nCh++ )
    {
        if ( chain[nCh] )
        {
            //================================ Get all residues
            chain[nCh]->GetResidueTable       ( residue, noResidues );
            
            for ( unsigned int nRes = 0; nRes < static_cast<unsigned int> ( noResidues ); nRes++ )
            {
                if ( residue[nRes] )
                {
                    //======================== Get all atoms
                    residue[nRes]->GetAtomTable ( atom, noAtoms );
                    
                    for ( unsigned int aNo = 0; aNo < static_cast<unsigned int> ( noAtoms ); aNo++ )
                    {
                        if ( atom[aNo] )
                        {
                            //================ Check for termination 'residue'
                            if ( atom[aNo]->Ter ) { continue; }
                            
                            //================ Find axis maxs and mins
                            if ( firstAtom )
                            {
                                maxX          = atom[aNo]->x;
                                minX          = atom[aNo]->x;
                                maxY          = atom[aNo]->y;
                                minY          = atom[aNo]->y;
                                maxZ          = atom[aNo]->z;
                                minZ          = atom[aNo]->z;
                                
                                pdbXCom      += atom[aNo]->x;
                                pdbYCom      += atom[aNo]->y;
                                pdbZCom      += atom[aNo]->z;
                                if ( Bfactor != 0.0 )
                                {
                                    bFacTot  += 1.0;
                                }
                                else
                                {
                                    bFacTot  += atom[aNo]->tempFactor;
                                }
                                
                                
                                firstAtom     = false;
                            }
                            else
                            {
                                xVal          = atom[aNo]->x;
                                yVal          = atom[aNo]->y;
                                zVal          = atom[aNo]->z;
                                if ( xVal > maxX )  { maxX = xVal; } if ( xVal < minX ) { minX = xVal; }
                                if ( yVal > maxY )  { maxY = yVal; } if ( yVal < minY ) { minY = yVal; }
                                if ( zVal > maxZ )  { maxZ = zVal; } if ( zVal < minZ ) { minZ = zVal; }
                                
                                if ( Bfactor != 0.0 )
                                {
                                    bFacTot  += 1.0;
                                    pdbXCom  += atom[aNo]->x;
                                    pdbYCom  += atom[aNo]->y;
                                    pdbZCom  += atom[aNo]->z;
                                }
                                else
                                {
                                    bFacTot  += atom[aNo]->tempFactor;
                                    pdbXCom  += atom[aNo]->x * atom[aNo]->tempFactor;
                                    pdbYCom  += atom[aNo]->y * atom[aNo]->tempFactor;
                                    pdbZCom  += atom[aNo]->z * atom[aNo]->tempFactor;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    //======================================== Determine original PDB COM
    pdbXCom                                  /= bFacTot;
    pdbYCom                                  /= bFacTot;
    pdbZCom                                  /= bFacTot;
    
    //======================================== Determine max dimensions
    this->_xRange                             = (maxX - minX);
    this->_yRange                             = (maxY - minY);
    this->_zRange                             = (maxZ - minZ);
    this->_maxMapRange                        = std::max ( this->_xRange, std::max( this->_yRange, this->_zRange ) );
    
    //======================================== Save extracellular space and use internal value to avoid issues with clipper map computation
    this->_maxExtraCellularSpace              = *extraSpace;
    double ecsHlp                             = std::floor ( std::max ( 20.0, ( this->_maxMapRange / 4.0 ) ) );
    
    //======================================== Determine max dimensions
    this->_xRange                             = (maxX - minX) + ecsHlp;
    this->_yRange                             = (maxY - minY) + ecsHlp;
    this->_zRange                             = (maxZ - minZ) + ecsHlp;
    this->_maxMapRange                        = std::max ( this->_xRange, std::max( this->_yRange, this->_zRange ) );
    
    //======================================== Move structure to middle of map
    double xMov                               = (ecsHlp/2.0) - minX;
    double yMov                               = (ecsHlp/2.0) - minY;
    double zMov                               = (ecsHlp/2.0) - minZ;
    
    //======================================== Move structure to the center of map
    mfile->GetChainTable                      ( 1, chain, noChains );
    firstAtom                                 = true;
    for ( unsigned int nCh = 0; nCh < static_cast<unsigned int> ( noChains ); nCh++ )
    {
        if ( chain[nCh] )
        {
            //================================ Get all residues
            chain[nCh]->GetResidueTable       ( residue, noResidues );
            
            for ( unsigned int nRes = 0; nRes < static_cast<unsigned int> ( noResidues ); nRes++ )
            {
                if ( residue[nRes] )
                {
                    //======================== Get all atoms
                    residue[nRes]->GetAtomTable ( atom, noAtoms );
                    
                    for ( unsigned int aNo = 0; aNo < static_cast<unsigned int> ( noAtoms ); aNo++ )
                    {
                        if ( atom[aNo] )
                        {
                            //================ Check for termination 'residue'
                            if ( atom[aNo]->Ter ) { continue; }
                            
                            if ( Bfactor != 0.0 )
                            {
                                atom[aNo]->SetCoordinates ( atom[aNo]->x + xMov,
                                                            atom[aNo]->y + yMov,
                                                            atom[aNo]->z + zMov,
                                                            atom[aNo]->occupancy,
                                                            Bfactor );
                            } 
                            else
                            {
                                atom[aNo]->SetCoordinates ( atom[aNo]->x + xMov,
                                                            atom[aNo]->y + yMov,
                                                            atom[aNo]->z + zMov,
                                                            atom[aNo]->occupancy,
                                                            atom[aNo]->tempFactor );
                            }
                        }
                        
                        if ( firstAtom )
                        {
                            this->_xCorrErr   = atom[aNo]->x;
                            this->_yCorrErr   = atom[aNo]->y;
                            this->_zCorrErr   = atom[aNo]->z;
                            firstAtom         = false;
                        }
                    }
                }
            }
        }
    }
    
    //======================================== Initialise map variables
    clipper::Spacegroup spgr                  ( clipper::Spacegroup::P1 );
    clipper::Cell cell                        ( clipper::Cell_descr ( this->_xRange, this->_yRange, this->_zRange ) );
    clipper::Resolution reso;
    
    if ( settings->taskToPerform == ProSHADE::OverlayMap )
    {
        reso                                  = clipper::Resolution ( std::min ( std::max ( this->_mapResolution / 3.0, 2.0 ), this->_mapResolution ) );
    }
    else
    {
        reso                                  = clipper::Resolution ( this->_mapResolution );
    }
    
    
    const clipper::Grid_sampling grid         ( spgr, cell, reso );
    clipper::Xmap<float> *densityMap          = new clipper::Xmap<float> ( spgr, cell, grid );
    
    //======================================== Get Map
    int hndl                                  = mfile->NewSelection ( );
    mfile->SelectAtoms                        ( hndl, 0, 0, ::mmdb::SKEY_NEW );
    mfile->GetSelIndex                        ( hndl, atom, noAtoms );
    clipper::MMDBAtom_list *aList             = new clipper::MMDBAtom_list ( atom, noAtoms );
    clipper::EDcalc_aniso<clipper::ftype32> edCalc;
    edCalc                                    ( *densityMap, *aList );
    
    //======================================== Free memory
    delete mfile;
    
    //======================================== Find max U, V and W
    clipper::Xmap_base::Map_reference_index LastPos = (*densityMap).first();
    clipper::Xmap_base::Map_reference_index PrevPos = (*densityMap).first();
    for ( LastPos = (*densityMap).first(); !LastPos.last(); LastPos.next() ) { PrevPos = LastPos; }
    this->_maxMapU                            = PrevPos.coord().u();
    this->_maxMapV                            = PrevPos.coord().v();
    this->_maxMapW                            = PrevPos.coord().w();
    
    this->_preCBSU                             = this->_maxMapU;
    this->_preCBSV                             = this->_maxMapV;
    this->_preCBSW                             = this->_maxMapW;
    
    //======================================== Convert clipper Xmap to my map format
    // ... Allocate map data memory
    this->_densityMapMap                      = (float*) malloc ( ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) ) * sizeof ( float ) );
    if ( this->_densityMapMap == nullptr )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory for map data. Terminating... " << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Cannot allocate memory for map data. Did you run out of memory?" << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    // ... and copy
    int arrPos                                = 0;
    for ( unsigned int uIt = 0; uIt < (this->_maxMapU+1); uIt++ )
    {
        for ( unsigned int vIt = 0; vIt < (this->_maxMapV+1); vIt++ )
        {
            for ( unsigned int wIt = 0; wIt < (this->_maxMapW+1); wIt++ )
            {
                arrPos                        = wIt  + (this->_maxMapW + 1) * ( vIt  + (this->_maxMapV + 1) * uIt );
                
                clipper::Vec3<int> pos        ( uIt, vIt, wIt );
                clipper::Coord_grid cg        ( pos );
                
                this->_densityMapMap[arrPos]  = densityMap->get_data ( cg );
            }
        }        
    }
    
    //======================================== Free memory
    if ( densityMap != nullptr ) 
    { 
        delete densityMap; 
        densityMap                            = nullptr;
    }
    
    //======================================== Set internal map parameters
    this->_xFrom                              = 0;
    this->_yFrom                              = 0;
    this->_zFrom                              = 0;
    
    this->_xTo                                = this->_maxMapU;
    this->_yTo                                = this->_maxMapV;
    this->_zTo                                = this->_maxMapW;
    
    //======================================== Remove the extra space added for the map calculation
    // ... Find new settings
    maxX                                      = -1.0;
    maxY                                      = -1.0;
    maxZ                                      = -1.0;
    
    minX                                      = this->_maxMapU;
    minY                                      = this->_maxMapV;
    minZ                                      = this->_maxMapW;
    for ( unsigned int uIt = 0; uIt < (this->_maxMapU+1); uIt++ )
    {
        for ( unsigned int vIt = 0; vIt < (this->_maxMapV+1); vIt++ )
        {
            for ( unsigned int wIt = 0; wIt < (this->_maxMapW+1); wIt++ )
            {
                arrPos                        = wIt  + (this->_maxMapW + 1) * ( vIt  + (this->_maxMapV + 1) * uIt );
                if ( this->_densityMapMap[arrPos] > 0.005 )
                {
                    if ( maxX < uIt ) { maxX = uIt; } if ( minX > uIt ) { minX = uIt; }
                    if ( maxY < vIt ) { maxY = vIt; } if ( minY > vIt ) { minY = vIt; }
                    if ( maxZ < wIt ) { maxZ = wIt; } if ( minZ > wIt ) { minZ = wIt; }
                }
            }
        }
    }

    this->_xSamplingRate                      = this->_xRange / static_cast<double> ( this->_maxMapU + 1 );
    this->_ySamplingRate                      = this->_yRange / static_cast<double> ( this->_maxMapV + 1 );
    this->_zSamplingRate                      = this->_zRange / static_cast<double> ( this->_maxMapW + 1 );
    
    this->_xRange                             = this->_xRange - ( this->_maxMapU - ( maxX - minX ) ) * this->_xSamplingRate;
    this->_yRange                             = this->_yRange - ( this->_maxMapV - ( maxY - minY ) ) * this->_ySamplingRate;
    this->_zRange                             = this->_zRange - ( this->_maxMapW - ( maxZ - minZ ) ) * this->_zSamplingRate;
    this->_maxMapRange                        = std::max ( this->_xRange, std::max( this->_yRange, this->_zRange ) );
    
    // ... Create new map and fill it with data
    float *hlpMap                             = nullptr;
    hlpMap                                    = (float*) malloc ( static_cast<int>( (maxX - minX + 1) * (maxY - minY + 1) * (maxZ - minZ + 1) ) * sizeof ( float ) );
    if ( hlpMap == nullptr )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory for map data. Terminating... " << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Cannot allocate memory for map data. Did you run out of memory?" << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    unsigned int newU, newV, newW, hlpPos;
    for ( int uIt = 0; uIt < static_cast<int> (maxX - minX + 1); uIt++ )
    {
        for ( int vIt = 0; vIt < static_cast<int> (maxY - minY + 1); vIt++ )
        {
            for ( int wIt = 0; wIt < static_cast<int> (maxZ - minZ + 1); wIt++ )
            {
                newU                          = (uIt + minX);
                newV                          = (vIt + minY);
                newW                          = (wIt + minZ);
                
                hlpPos                    = newW  + (this->_maxMapW + 1) * ( newV  + (this->_maxMapV + 1) * newU  );
                arrPos                    = wIt + (maxZ - minZ + 1)    * ( vIt + (maxY - minY + 1) * uIt );
                
                hlpMap[arrPos]            = this->_densityMapMap[hlpPos];
            }
        }
    }
    
    this->_maxMapU                            = maxX - minX;
    this->_maxMapV                            = maxY - minY;
    this->_maxMapW                            = maxZ - minZ;
    
    this->_xTo                                = maxX - minX;
    this->_yTo                                = maxY - minY;
    this->_zTo                                = maxZ - minZ;
    
    // ... Copy
    if ( this->_densityMapMap != nullptr )
    {
        free                                  ( this->_densityMapMap );
        this->_densityMapMap                  = nullptr;
    }
    
    this->_densityMapMap                      = (float*) malloc ( ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) ) * sizeof ( float ) );
    if ( this->_densityMapMap == nullptr )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory for map data. Terminating... " << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Cannot allocate memory for map data. Did you run out of memory?" << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) ); iter++ )
    {
        this->_densityMapMap[iter]            = hlpMap[iter];
    }
    
    if ( hlpMap != nullptr )
    {
        free                                  ( hlpMap );
        hlpMap                                = nullptr;
    }
    
    if ( this->_maxExtraCellularSpace == -777.7 )
    {
        this->_maxExtraCellularSpace          = std::max ( 20.0, ( std::max( this->_xRange, std::max ( this->_yRange, this->_zRange ) ) - std::min( this->_xRange, std::min ( this->_yRange, this->_zRange ) ) ) );
       *extraSpace                            = this->_maxExtraCellularSpace;
    }
    
    if ( this->_maxExtraCellularSpace != 0.0 )
    {
        //==================================== Compute new stats
        int xDiff                             = static_cast<int>   ( std::ceil ( ( this->_xRange + this->_maxExtraCellularSpace ) / this->_xSamplingRate ) - this->_maxMapU );
        int yDiff                             = static_cast<int>   ( std::ceil ( ( this->_yRange + this->_maxExtraCellularSpace ) / this->_ySamplingRate ) - this->_maxMapV );
        int zDiff                             = static_cast<int>   ( std::ceil ( ( this->_zRange + this->_maxExtraCellularSpace ) / this->_zSamplingRate ) - this->_maxMapW );
        
        this->_xRange                        += xDiff * this->_xSamplingRate;
        this->_yRange                        += yDiff * this->_ySamplingRate;
        this->_zRange                        += zDiff * this->_zSamplingRate;
        
        if ( xDiff % 2 != 0 )
        {
            this->_xRange                    += this->_xSamplingRate;
            xDiff                             = static_cast<int>   ( std::ceil ( this->_xRange / this->_xSamplingRate ) - ( this->_maxMapU + 1 ) );
        }
        if ( yDiff % 2 != 0 )
        {
            this->_yRange                    += this->_ySamplingRate;
            yDiff                             = static_cast<int>   ( std::ceil ( this->_yRange / this->_ySamplingRate ) - ( this->_maxMapV + 1 ) );
        }
        if ( zDiff % 2 != 0 )
        {
            this->_zRange                    += this->_zSamplingRate;
            zDiff                             = static_cast<int>   ( std::ceil ( this->_zRange / this->_zSamplingRate ) - ( this->_maxMapW + 1 ) );
        }
        this->_maxMapRange                    = std::max ( this->_xRange, std::max( this->_yRange, this->_zRange ) );
        
        xDiff                                /= 2;
        yDiff                                /= 2;
        zDiff                                /= 2;
        
        this->_exCeSpDiffX                    = xDiff;
        this->_exCeSpDiffY                    = yDiff;
        this->_exCeSpDiffZ                    = zDiff;
        
        this->_xFrom                         -= xDiff;
        this->_yFrom                         -= yDiff;
        this->_zFrom                         -= zDiff;
        
        this->_xTo                           += xDiff;
        this->_yTo                           += yDiff;
        this->_zTo                           += zDiff;
        
        this->_maxMapU                        = this->_xTo - this->_xFrom;
        this->_maxMapV                        = this->_yTo - this->_yFrom;
        this->_maxMapW                        = this->_zTo - this->_zFrom;
        
        this->_xSamplingRate                  = this->_xRange / static_cast<double> ( this->_maxMapU + 1 );
        this->_ySamplingRate                  = this->_yRange / static_cast<double> ( this->_maxMapV + 1 );
        this->_zSamplingRate                  = this->_zRange / static_cast<double> ( this->_maxMapW + 1 );
        
        //==================================== Move the map
        hlpMap                                = nullptr;
        hlpMap                                = (float*) malloc ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) * sizeof ( float ) );
        if ( hlpMap == nullptr )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory for map moving data. Terminating... " << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "Cannot allocate memory for moving the map data. Did you run out of memory?" << "</font>";
                rvapi_set_text                    ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                       ( );
            }
            
            exit ( -1 );
        }
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) ); iter++ )
        {
            hlpMap[iter]                      = 0.0;
        }
        
        for ( int uIt = 0; uIt < static_cast<int> (this->_maxMapU+1); uIt++ )
        {
            for ( int vIt = 0; vIt < static_cast<int> (this->_maxMapV+1); vIt++ )
            {
                for ( int wIt = 0; wIt < static_cast<int> (this->_maxMapW+1); wIt++ )
                {
                    if ( ( uIt < xDiff ) || ( uIt > static_cast<int> ( this->_maxMapU - xDiff ) ) ||
                         ( vIt < yDiff ) || ( vIt > static_cast<int> ( this->_maxMapV - yDiff ) ) ||
                         ( wIt < zDiff ) || ( wIt > static_cast<int> ( this->_maxMapW - zDiff ) ) )
                    {
                        hlpPos                = wIt  + (this->_maxMapW + 1) * ( vIt  + (this->_maxMapV + 1) * uIt  );
                        hlpMap[hlpPos]        = 0.0;
                    }
                    else
                    {
                        newU                  = (uIt - xDiff);
                        newV                  = (vIt - yDiff);
                        newW                  = (wIt - zDiff);
                        
                        hlpPos                = wIt  + (this->_maxMapW + 1) * ( vIt  + (this->_maxMapV + 1) * uIt  );
                        arrPos                = newW + (this->_maxMapW + 1 - zDiff * 2) * ( newV + (this->_maxMapV + 1 - yDiff * 2) * newU );
                        
                        hlpMap[hlpPos]        = this->_densityMapMap[arrPos];
                    }
                }
            }
        }
        
        //==================================== Free memory
        if ( this->_densityMapMap != nullptr )
        {
            free                              ( this->_densityMapMap );
            this->_densityMapMap              = nullptr;
        }
        
        //==================================== Copy
        this->_densityMapMap                  = (float*) malloc ( ((this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1)) * sizeof ( float ) );
        if ( this->_densityMapMap == nullptr )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory for map data. Terminating... " << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "Cannot allocate memory for map data. Did you run out of memory?" << "</font>";
                rvapi_set_text                    ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                       ( );
            }
            
            exit ( -1 );
        }
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) ); iter++ )
        {
            this->_densityMapMap[iter]        = hlpMap[iter];
        }
        
        //==================================== Free memory
        if ( hlpMap != nullptr )
        {
            free                              ( hlpMap );
            hlpMap                            = nullptr;
        }
    }
    else
    {
        this->_exCeSpDiffX                    = 0;
        this->_exCeSpDiffY                    = 0;
        this->_exCeSpDiffZ                    = 0;
    }
    
    //======================================== Determine sampling ranges
    this->_xSamplingRate                      = this->_xRange / static_cast<double> ( this->_maxMapU + 1 );
    this->_ySamplingRate                      = this->_yRange / static_cast<double> ( this->_maxMapV + 1 );
    this->_zSamplingRate                      = this->_zRange / static_cast<double> ( this->_maxMapW + 1 );
    
    //======================================== Make sure map matches original PDB in visualisation
    double mapXCom                            = 0.0;
    double mapYCom                            = 0.0;
    double mapZCom                            = 0.0;
    double densTot                            = 0.0;
    for ( unsigned int uIt = 0; uIt < static_cast<unsigned int> ( (this->_maxMapU+1) ); uIt++ )
    {
        for ( unsigned int vIt = 0; vIt < static_cast<unsigned int> ( (this->_maxMapV+1) ); vIt++ )
        {
            for ( unsigned int wIt = 0; wIt < static_cast<unsigned int> ( (this->_maxMapW+1) ); wIt++ )
            {
                arrPos                        = wIt + (this->_maxMapW + 1) * ( vIt + (this->_maxMapV + 1) * uIt );
                
                if ( this->_densityMapMap[arrPos] != this->_densityMapMap[arrPos] )
                {
                    this->_densityMapMap[arrPos] = 0.0;
                }
                
                mapXCom                      += uIt * this->_densityMapMap[arrPos];
                mapYCom                      += vIt * this->_densityMapMap[arrPos];
                mapZCom                      += wIt * this->_densityMapMap[arrPos];
                densTot                      += this->_densityMapMap[arrPos];
            }
        }
    }
    
    mapXCom                                  /= densTot;
    mapYCom                                  /= densTot;
    mapZCom                                  /= densTot;
    
    mapXCom                                   = ( mapXCom + this->_xFrom ) * this->_xSamplingRate;
    mapYCom                                   = ( mapYCom + this->_yFrom ) * this->_ySamplingRate;
    mapZCom                                   = ( mapZCom + this->_zFrom ) * this->_zSamplingRate;
    
    double mapXMove                           = ( pdbXCom - mapXCom ) / this->_xSamplingRate;
    double mapYMove                           = ( pdbYCom - mapYCom ) / this->_ySamplingRate;
    double mapZMove                           = ( pdbZCom - mapZCom ) / this->_zSamplingRate;
    
    this->_xFrom                             += std::round ( mapXMove );
    this->_xTo                               += std::round ( mapXMove );
    this->_yFrom                             += std::round ( mapYMove );
    this->_yTo                               += std::round ( mapYMove );
    this->_zFrom                             += std::round ( mapZMove );
    this->_zTo                               += std::round ( mapZMove );
    
    //======================================== Check lims
    bool xFSignChange                         = false;
    if ( ( ( ( this->_xFrom - std::round ( mapXMove ) ) < 0 ) && ( this->_xFrom > 0 ) ) ||
         ( ( ( this->_xFrom - std::round ( mapXMove ) ) > 0 ) && ( this->_xFrom < 0 ) ) ) { xFSignChange = true; }
    bool xTSignChange                         = false;
    if ( ( ( ( this->_xTo   - std::round ( mapXMove ) ) < 0 ) && ( this->_xTo   > 0 ) ) ||
         ( ( ( this->_xTo   - std::round ( mapXMove ) ) > 0 ) && ( this->_xTo   < 0 ) ) ) { xTSignChange = true; }
    
    bool yFSignChange                         = false;
    if ( ( ( ( this->_yFrom - std::round ( mapYMove ) ) < 0 ) && ( this->_yFrom > 0 ) ) ||
         ( ( ( this->_yFrom - std::round ( mapYMove ) ) > 0 ) && ( this->_yFrom < 0 ) ) ) { yFSignChange = true; }
    bool yTSignChange                         = false;
    if ( ( ( ( this->_yTo   - std::round ( mapYMove ) ) < 0 ) && ( this->_yTo   > 0 ) ) ||
         ( ( ( this->_yTo   - std::round ( mapYMove ) ) > 0 ) && ( this->_yTo   < 0 ) ) ) { yTSignChange = true; }
    
    bool zFSignChange                         = false;
    if ( ( ( ( this->_zFrom - std::round ( mapZMove ) ) < 0 ) && ( this->_zFrom > 0 ) ) ||
         ( ( ( this->_zFrom - std::round ( mapZMove ) ) > 0 ) && ( this->_zFrom < 0 ) ) ) { zFSignChange = true; }
    bool zTSignChange                         = false;
    if ( ( ( ( this->_zTo   - std::round ( mapZMove ) ) < 0 ) && ( this->_zTo   > 0 ) ) ||
         ( ( ( this->_zTo   - std::round ( mapZMove ) ) > 0 ) && ( this->_zTo   < 0 ) ) ) { zTSignChange = true; }
    
    if ( ( xFSignChange && !xTSignChange ) || ( !xFSignChange && xTSignChange ) )
    {
        this->_xTo                           += 1;
    }
    if ( ( yFSignChange && !yTSignChange ) || ( !yFSignChange && yTSignChange ) )
    {
        this->_yTo                           += 1;
    }
    if ( ( zFSignChange && !zTSignChange ) || ( !zFSignChange && zTSignChange ) )
    {
        this->_zTo                           += 1;
    }
    
    //======================================== If bandwidth is not given, determine it
    if ( *bandwidth == 0 )
    {
        *bandwidth                            = std::ceil ( this->_maxMapRange / 4.0 );
    }
    
    if ( settings->maxRotError != 0 )
    {
        *bandwidth                            = static_cast<unsigned int> ( 180 / settings->maxRotError );
        this->_wasBandwithGiven               = true;
    }
    
    //======================================== Decide shell spacing, if not already decided
    if ( *shellDistance == 0 )
    {
        settings->shellSpacing                = static_cast<double> ( this->_maxMapRange / 2.0 ) / std::floor ( static_cast<double> ( this->_maxMapRange / 2.0 ) / static_cast<double> ( this->_mapResolution / 2.0 ) );
        this->_shellSpacing                   = settings->shellSpacing;
        *shellDistance                         = this->_shellSpacing;
        
        while ( std::floor ( this->_maxMapRange / this->_shellSpacing ) < 20 )
        {
            this->_shellSpacing              /= 2.0;
            settings->shellSpacing           /= 2.0;
            *shellDistance                    /= 2.0;
        }
        
    }
    
    //======================================== Automatically determine maximum number of shells
    this->_shellPlacement                     = std::vector<double> ( std::floor ( this->_maxMapRange / this->_shellSpacing ) );
    
    for ( unsigned int shellIter = 0; shellIter < static_cast<unsigned int> ( this->_shellPlacement.size() ); shellIter++ )
    {
        this->_shellPlacement.at(shellIter)   = (shellIter+1) * this->_shellSpacing;
    }
    
    //======================================== If theta and phi are not given, determine them
    if ( *theta == 0 ) { *theta               = 2 * (*bandwidth); }
    if ( *phi   == 0 ) { *phi                 = 2 * (*bandwidth); }
    
    //======================================== If Gauss-Legendre integration order is not given, decide it
    if ( *glIntegOrder == 0 )
    {
        double distPerPointFraction           = static_cast<double> ( this->_shellSpacing ) / ( this->_maxMapRange / 2.0 );
        
        for ( unsigned int iter = 2; iter < static_cast<unsigned int> ( ProSHADE_internal_misc::glIntMaxDists.size() ); iter++ )
        {
            if ( ProSHADE_internal_misc::glIntMaxDists.at(iter) >= distPerPointFraction )
            {
                *glIntegOrder                 = iter;
            }
        }
    }
 
    //======================================== Done
    this->_densityMapComputed                 = true;
    this->_fromPDB                            = true;
    
}

/*! \brief Function to read in the MAP file and provide the basic processing.
 
 This function reads in the CCP4's MAP format density map and checks if it is cubic. If so (non-cubic maps are not
 supported yet) it proceeds to check that the density is centered in the center of the cell and corrects if not and
 finally it proceeds to estimate the optimal settings for the parameters, unless these are provided by the user.
 
 \param[in] fileName The file name (and path) to the PDB file to be read in.
 \param[in] shellDistance The intended distance between shells.
 \param[in] resolution The resolution to which the map should be computed. This is the main parameter from which all other are derived.
 \param[in] bandwidth A pointer to variable which will hold the optimal bandwidth as determined by the function or the value supplied by the user.
 \param[in] theta A pointer to variable which will hold the optimal theta angle as determined by the function or the value supplied by the user.
 \param[in] phi A pointer to variable which will hold the optimal phi angle as determined by the function or the value supplied by the user.
 \param[in] extraSpace A pointer to variable which will hold the optimal between cells distance as determined by the function or the value supplied by the user.
 \param[in] mapResDefault A bool value specifying whether the resolution value supplied is the default, or whether it was supplied by the user.
 \param[in] rotDefaults A bool value telling the function whether map rotation is intended or not, as some settings do depend on this.
 \param[in] overlayDefaults A bool value specifying whether the resulting map will be used for map overlay mode. If so, the shell distances are doubled to speed things up.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_data::getDensityMapFromMAP ( std::string fileName,
                                                              double *shellDistance,
                                                              double resolution,
                                                              unsigned int *bandwidth,
                                                              unsigned int *theta,
                                                              unsigned int *phi,
                                                              unsigned int *glIntegOrder,
                                                              double *extraSpace,
                                                              bool mapResDefault,
                                                              bool rotDefaults,
                                                              ProSHADE::ProSHADE_settings* settings,
                                                              bool overlayDefaults )
{
    //======================================== Initialise internal values
    this->_inputFileName                      = fileName;
    this->_shellSpacing                       = *shellDistance;
    this->_mapResolution                      = resolution;
    this->_maxExtraCellularSpace              = *extraSpace;
    this->_firstLineCOM                       = false;
    
    if ( *extraSpace == -777.7 )
    {
        this->_maxExtraCellularSpace          = std::max ( 20.0, ( std::max( this->_xRange, std::max ( this->_yRange, this->_zRange ) ) - std::min( this->_xRange, std::min ( this->_yRange, this->_zRange ) ) ) );
       *extraSpace                            = this->_maxExtraCellularSpace;
    }
    
    //======================================== Initialise local variables
    CMap_io::CMMFile *mapFile                 = nullptr;
    float *cell                               = nullptr;
    int *dim                                  = nullptr;
    int *grid                                 = nullptr;
    int *order                                = nullptr;
    int *orig                                 = nullptr;
    int myMapMode                             = 0;
    double cornerAvg                          = 0.0;
    double centralAvg                         = 0.0;
    double cornerCount                        = 0.0;
    double centralCount                       = 0.0;
    
    //======================================== Allocate memory
    cell                                      = (float*) malloc ( 6 * sizeof ( float ) );
    dim                                       = (int*  ) malloc ( 3 * sizeof ( int   ) );
    grid                                      = (int*  ) malloc ( 3 * sizeof ( int   ) );
    order                                     = (int*  ) malloc ( 3 * sizeof ( int   ) );
    orig                                      = (int*  ) malloc ( 3 * sizeof ( int   ) );
    
    if ( ( cell == nullptr ) || ( dim == nullptr ) || ( order == nullptr ) || ( orig == nullptr ) || ( grid == nullptr ) )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory. Terminating... " << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Cannot allocate memory for map data. Did you run out of memory?" << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Read in the MAP file info
    mapFile                                   = reinterpret_cast<CMap_io::CMMFile*> ( CMap_io::ccp4_cmap_open ( fileName.c_str() , myMapMode ) );
    if ( mapFile == nullptr ) 
    {
        std::cerr << "!!! ProSHADE ERROR !!! Failed to open the file " << fileName << ". Check for typos and corruption of the file. Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Cannot open the file " << fileName << " . Could it be corrupted?" << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    CMap_io::ccp4_cmap_get_cell               ( mapFile, cell  );
    CMap_io::ccp4_cmap_get_dim                ( mapFile, dim   );
    CMap_io::ccp4_cmap_get_grid               ( mapFile, grid  );
    CMap_io::ccp4_cmap_get_order              ( mapFile, order );
    CMap_io::ccp4_cmap_get_origin             ( mapFile, orig  );
    
    //======================================== If CUBIC
    if ( ( ( cell[3] < (90.0 + 0.1) ) && ( cell[3] > (90.0 - 0.1) ) ) &&
         ( ( cell[4] < (90.0 + 0.1) ) && ( cell[4] > (90.0 - 0.1) ) ) &&
         ( ( cell[5] < (90.0 + 0.1) ) && ( cell[5] > (90.0 - 0.1) ) ) )
    {
        //==================================== Determine/Save dimmensions
        this->_xRange                         = cell[0];
        this->_yRange                         = cell[1];
        this->_zRange                         = cell[2];
        
        //==================================== Maximal cell dimensions
        this->_maxMapRange                    = std::max ( this->_xRange, std::max( this->_yRange, this->_zRange ) );
        
        //==================================== Save extracellular space
        this->_maxExtraCellularSpace          = *extraSpace;
        
        //==================================== Find max U, V and W
        if ( order[0] == 1 ) { this->_maxMapU = dim[0] - 1; this->_xFrom = orig[0]; }
        if ( order[0] == 2 ) { this->_maxMapU = dim[1] - 1; this->_yFrom = orig[0]; }
        if ( order[0] == 3 ) { this->_maxMapU = dim[2] - 1; this->_zFrom = orig[0]; }
        
        if ( order[1] == 1 ) { this->_maxMapV = dim[0] - 1; this->_xFrom = orig[1]; }
        if ( order[1] == 2 ) { this->_maxMapV = dim[1] - 1; this->_yFrom = orig[1]; }
        if ( order[1] == 3 ) { this->_maxMapV = dim[2] - 1; this->_zFrom = orig[1]; }
        
        if ( order[2] == 1 ) { this->_maxMapW = dim[0] - 1; this->_xFrom = orig[2]; }
        if ( order[2] == 2 ) { this->_maxMapW = dim[1] - 1; this->_yFrom = orig[2]; }
        if ( order[2] == 3 ) { this->_maxMapW = dim[2] - 1; this->_zFrom = orig[2]; }
        
        
        this->_preCBSU                         = this->_maxMapU;
        this->_preCBSV                         = this->_maxMapV;
        this->_preCBSW                         = this->_maxMapW;
        
        //==================================== Get the map start and end
        this->_xTo                            = this->_xFrom + this->_maxMapU;
        this->_yTo                            = this->_yFrom + this->_maxMapV;
        this->_zTo                            = this->_zFrom + this->_maxMapW;
        
        //==================================== Allocate map data memory
        this->_densityMapMap                  = (float*) malloc ( (dim[0]*dim[1]*dim[2]) * sizeof ( float ) );
        if ( this->_densityMapMap == nullptr )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory for map data. Terminating... " << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "Cannot allocate memory for map data. Did you run out of memory?" << "</font>";
                rvapi_set_text                    ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                       ( );
            }
            
            exit ( -1 );
        }
        
        //==================================== Check if the mode is compatible
        int mapMode                           = CMap_io::ccp4_cmap_get_datamode ( mapFile );
        if ( ( mapMode != 0 ) && ( mapMode != 2 ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Cannot read from the map file. The map file mode is not supported. Terminating..." << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "Cannot read from the map file. The map mode is neither 0, nor 2; other modes are not supperted at the moment." << "</font>";
                rvapi_set_text                    ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                       ( );
            }
            
            exit ( -1 );
        }
        
        //==================================== Read in map data
        // ... Find the stop positions (start is in orig variables) and the axis order
        int maxLim[3];
        int XYZOrder[3];
        int newU, newV, newW;
        int arrPos;
        
        for ( int iter = 0; iter < 3; iter++ ) 
        {
            maxLim[iter]                      = orig[iter] + dim[iter] - 1;
            XYZOrder[order[iter]-1]           = iter;
        }
            
        // ... Solve the dimensions and sizes
        int fastDimSize                       = ( maxLim[0] - orig[0] + 1 );
        int midDimSize                        = ( maxLim[1] - orig[1] + 1 ) * fastDimSize;
        std::vector<float> section( midDimSize );
        int index;
        int iters[3];
        
        // ... Read in the map data
        for ( iters[2] = orig[2]; iters[2] <= maxLim[2]; iters[2]++ ) 
        {
            index                             = 0;
            CMap_io::ccp4_cmap_read_section( mapFile, &section[0] );
            
            if ( mapMode == 0 ) 
            {
                for ( int iter = ( midDimSize - 1 ); iter >= 0; iter-- )  
                {
                    section[iter]             = static_cast<float> ( ( reinterpret_cast<unsigned char*> (&section[0]) )[iter] );
                }
            }
            
            for ( iters[1] = orig[1]; iters[1] <= maxLim[1]; iters[1]++ ) 
            {
                for ( iters[0] = orig[0]; iters[0] <= maxLim[0]; iters[0]++ ) 
                {
                    newU                      = iters[XYZOrder[0]] - orig[XYZOrder[0]];
                    newV                      = iters[XYZOrder[1]] - orig[XYZOrder[1]];
                    newW                      = iters[XYZOrder[2]] - orig[XYZOrder[2]];
                    arrPos                    = newW + (this->_maxMapW + 1) * ( newV + (this->_maxMapV + 1) * newU );
                    this->_densityMapMap[arrPos] = static_cast<float> ( section[ index++ ] );
                }
            }
        }
        
        //==================================== Get values for normalisation
        std::vector<double> vals ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) );
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( dim[0] * dim[1] * dim[2] ); iter++ )
        {
            vals.at(iter)                     = this->_densityMapMap[iter];
        }        
        std::sort                             ( vals.begin(), vals.end() );
        
        //==================================== Find mean and standard deviation for later normalisation
        this->_mapMean                        = std::accumulate ( vals.begin(), vals.end(), 0.0 ) / static_cast<double> ( vals.size() );
        this->_mapSdev                        = std::sqrt ( static_cast<double> ( std::inner_product ( vals.begin(), vals.end(), vals.begin(), 0.0 ) ) / static_cast<double> ( vals.size() ) - this->_mapMean * this->_mapMean );
        
        //==================================== Check if the map is not absolutely de-centered
        int noPToCheckU                       = static_cast<unsigned int> ( this->_maxMapU / 8.0 );
        int noPToCheckV                       = static_cast<unsigned int> ( this->_maxMapV / 8.0 );
        int noPToCheckW                       = static_cast<unsigned int> ( this->_maxMapW / 8.0 );
        
        for ( int uIt = 0; uIt < static_cast<int> (this->_maxMapU+1); uIt++ )
        {
            for ( int vIt = 0; vIt < static_cast<int> (this->_maxMapV+1); vIt++ )
            {
                for ( int wIt = 0; wIt < static_cast<int> (this->_maxMapW+1); wIt++ )
                {
                    //======================== Initialisation
                    arrPos                    = wIt + (this->_maxMapW + 1) * ( vIt + (this->_maxMapV + 1) * uIt );
                    
                    //======================== Check to which quadrant the value belongs to
                    if ( ( ( uIt <= static_cast<int> ( this->_maxMapU/2 + noPToCheckU ) ) && ( uIt >= static_cast<int> ( this->_maxMapU/2 - noPToCheckU ) ) ) &&
                         ( ( vIt <= static_cast<int> ( this->_maxMapV/2 + noPToCheckV ) ) && ( vIt >= static_cast<int> ( this->_maxMapV/2 - noPToCheckV ) ) ) &&
                         ( ( wIt <= static_cast<int> ( this->_maxMapW/2 + noPToCheckW ) ) && ( wIt >= static_cast<int> ( this->_maxMapW/2 - noPToCheckW ) ) ) )
                    {
                        centralAvg           += std::max ( this->_densityMapMap[arrPos], static_cast<float>(0.0) );
                        centralCount         += 1.0;
                    }
                    if ( ( ( uIt < noPToCheckU )                  && ( vIt < noPToCheckV                  ) && ( wIt < noPToCheckW                  ) ) ||
                         ( ( uIt < noPToCheckU )                  && ( vIt < noPToCheckV                  ) && ( wIt > static_cast<int> (this->_maxMapW-noPToCheckW) ) ) ||
                         ( ( uIt < noPToCheckU )                  && ( vIt > static_cast<int> (this->_maxMapV-noPToCheckV) ) && ( wIt < noPToCheckW                  ) ) ||
                         ( ( uIt < noPToCheckU )                  && ( vIt > static_cast<int> (this->_maxMapV-noPToCheckV) ) && ( wIt > static_cast<int> (this->_maxMapW-noPToCheckW) ) ) ||
                         ( ( uIt > static_cast<int> (this->_maxMapU-noPToCheckU) ) && ( vIt < noPToCheckV                  ) && ( wIt < noPToCheckW                  ) ) ||
                         ( ( uIt > static_cast<int> (this->_maxMapU-noPToCheckU) ) && ( vIt < noPToCheckV                  ) && ( wIt > static_cast<int> (this->_maxMapW-noPToCheckW) ) ) ||
                         ( ( uIt > static_cast<int> (this->_maxMapU-noPToCheckU) ) && ( vIt > static_cast<int> (this->_maxMapV-noPToCheckV) ) && ( wIt < noPToCheckW                  ) ) ||
                         ( ( uIt > static_cast<int> (this->_maxMapU-noPToCheckU) ) && ( vIt > static_cast<int> (this->_maxMapV-noPToCheckV) ) && ( wIt > static_cast<int> (this->_maxMapW-noPToCheckW) ) ) )
                    {
                        cornerAvg            += std::max ( this->_densityMapMap[arrPos], static_cast<float>(0.0) );
                        cornerCount          += 1.0;
                    }
                }
            }
        }
        
        cornerAvg                            /= cornerCount;
        centralAvg                           /= centralCount;
        
        //==================================== If density is in the corners
        if ( cornerAvg > centralAvg )
        {
            //================================ Initialise required variables
            float* hlpMap                     = nullptr;
            hlpMap                            = (float*) malloc ( (dim[0]*dim[1]*dim[2]) * sizeof ( float ) );
            if ( hlpMap == nullptr )
            {
                std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory for map data. Terminating... " << std::endl;
                
                if ( settings->htmlReport )
                {
                    std::stringstream hlpSS;
                    hlpSS << "<font color=\"red\">" << "Cannot allocate memory for map data. Did you run out of memory?" << "</font>";
                    rvapi_set_text                    ( hlpSS.str().c_str(),
                                                       "ProgressSection",
                                                       settings->htmlReportLineProgress,
                                                       1,
                                                       1,
                                                       1 );
                    settings->htmlReportLineProgress += 1;
                    rvapi_flush                       ( );
                }
                
                exit ( -1 );
            }
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( dim[0] * dim[1] * dim[2] ); iter++ )
            {
                hlpMap[iter]                  = this->_densityMapMap[iter];
            }
            
            //================================ Transform
            unsigned int hlpPos;
            for ( int uIt = 0; uIt < static_cast<int> (this->_maxMapU+1); uIt++ )
            {
                for ( int vIt = 0; vIt < static_cast<int> (this->_maxMapV+1); vIt++ )
                {
                    for ( int wIt = 0; wIt < static_cast<int> (this->_maxMapW+1); wIt++ )
                    {
                        //==================== If in lower half, add half; if in upper halp, subtract half
                        if ( uIt < static_cast<int> (this->_maxMapU+1)/2 ) { newU = uIt + (this->_maxMapU+1)/2; } else { newU = uIt - (this->_maxMapU+1)/2; }
                        if ( vIt < static_cast<int> (this->_maxMapV+1)/2 ) { newV = vIt + (this->_maxMapV+1)/2; } else { newV = vIt - (this->_maxMapV+1)/2; }
                        if ( wIt < static_cast<int> (this->_maxMapW+1)/2 ) { newW = wIt + (this->_maxMapW+1)/2; } else { newW = wIt - (this->_maxMapW+1)/2; }
                        
                        arrPos                = newW + (this->_maxMapW + 1) * ( newV + (this->_maxMapV + 1) * newU );
                        hlpPos                = wIt  + (this->_maxMapW + 1) * ( vIt  + (this->_maxMapV + 1) * uIt  );
                        
                        //==================== Set the value into correct coords in the new map
                        this->_densityMapMap[arrPos] = hlpMap[hlpPos];
                    }
                }
            }
            
            //================================ Free memory
            free ( hlpMap );
        }
    }
    else
    {
        std::cerr << "!!! ProSHADE ERROR !!! Non-orthogonal maps are not yet supported." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Non-orthogonal map detected. Only P1 maps are currently supported." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Free memory
    if ( cell != nullptr )
    {
        free                                  ( cell );
        cell                                  = nullptr;
    }
    if ( dim != nullptr )
    {
        free                                  ( dim );
        dim                                   = nullptr;
    }
    if ( order != nullptr )
    {
        free                                  ( order );
        order                                 = nullptr;
    }
    if ( orig != nullptr )
    {
        free                                  ( orig );
        orig                                  = nullptr;
    }
    
    //======================================== Close the file
    CMap_io::ccp4_cmap_close                  ( mapFile );
    
    //======================================== Determine sampling ranges
    this->_xSamplingRate                      = this->_xRange / static_cast<double> ( this->_maxMapU + 1 );
    this->_ySamplingRate                      = this->_yRange / static_cast<double> ( this->_maxMapV + 1 );
    this->_zSamplingRate                      = this->_zRange / static_cast<double> ( this->_maxMapW + 1 );
    double maxSamplingRate                    = std::max ( this->_xSamplingRate, std::max( this->_ySamplingRate, this->_zSamplingRate ) );
    
    //======================================== If extra cell space is given, apply
    this->_maxExtraCellularSpace              = *extraSpace;
    if ( *extraSpace != 0.0 )
    {
        //==================================== Compute new stats
        int xDiff                             = static_cast<int>   ( std::ceil ( ( this->_xRange + this->_maxExtraCellularSpace ) / this->_xSamplingRate ) - this->_maxMapU );
        int yDiff                             = static_cast<int>   ( std::ceil ( ( this->_yRange + this->_maxExtraCellularSpace ) / this->_ySamplingRate ) - this->_maxMapV );
        int zDiff                             = static_cast<int>   ( std::ceil ( ( this->_zRange + this->_maxExtraCellularSpace ) / this->_zSamplingRate ) - this->_maxMapW );
        
        this->_xRange                        += xDiff * this->_xSamplingRate;
        this->_yRange                        += yDiff * this->_ySamplingRate;
        this->_zRange                        += zDiff * this->_zSamplingRate;
        
        if ( xDiff % 2 != 0 )
        {
            this->_xRange                    += this->_xSamplingRate;
            xDiff                             = static_cast<int>   ( std::ceil ( this->_xRange / this->_xSamplingRate ) - ( this->_maxMapU + 1 ) );
        }
        if ( yDiff % 2 != 0 )
        {
            this->_yRange                    += this->_ySamplingRate;
            yDiff                             = static_cast<int>   ( std::ceil ( this->_yRange / this->_ySamplingRate ) - ( this->_maxMapV + 1 ) );
        }
        if ( zDiff % 2 != 0 )
        {
            this->_zRange                    += this->_zSamplingRate;
            zDiff                             = static_cast<int>   ( std::ceil ( this->_zRange / this->_zSamplingRate ) - ( this->_maxMapW + 1 ) );
        }

        xDiff                                /= 2;
        yDiff                                /= 2;
        zDiff                                /= 2;
    
        this->_exCeSpDiffX                    = xDiff;
        this->_exCeSpDiffY                    = yDiff;
        this->_exCeSpDiffZ                    = zDiff;
        
        this->_xFrom                         -= xDiff;
        this->_yFrom                         -= yDiff;
        this->_zFrom                         -= zDiff;
    
        this->_xTo                           += xDiff;
        this->_yTo                           += yDiff;
        this->_zTo                           += zDiff;
        
        this->_maxMapU                        = this->_xTo - this->_xFrom;
        this->_maxMapV                        = this->_yTo - this->_yFrom;
        this->_maxMapW                        = this->_zTo - this->_zFrom;
        
        this->_xSamplingRate                  = this->_xRange / static_cast<double> ( this->_maxMapU + 1 );
        this->_ySamplingRate                  = this->_yRange / static_cast<double> ( this->_maxMapV + 1 );
        this->_zSamplingRate                  = this->_zRange / static_cast<double> ( this->_maxMapW + 1 );
        
        //==================================== Move the map
        float *hlpMap                         = nullptr;
        hlpMap                                = (float*) malloc ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) * sizeof ( float ) );
        if ( hlpMap == nullptr )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory for map moving data. Terminating... " << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "Cannot allocate memory for map data. Did you run out of memory?" << "</font>";
                rvapi_set_text                    ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                       ( );
            }
            
            exit ( -1 );
        }
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) ); iter++ )
        {
            hlpMap[iter]                      = 0.0;
        }
        
        unsigned int newU, newV, newW, hlpPos, arrPos;
        for ( int uIt = 0; uIt < static_cast<int> (this->_maxMapU+1); uIt++ )
        {
            for ( int vIt = 0; vIt < static_cast<int> (this->_maxMapV+1); vIt++ )
            {
                for ( int wIt = 0; wIt < static_cast<int> (this->_maxMapW+1); wIt++ )
                {
                    if ( ( uIt < xDiff ) || ( uIt > static_cast<int> ( this->_maxMapU - xDiff ) ) ||
                         ( vIt < yDiff ) || ( vIt > static_cast<int> ( this->_maxMapV - yDiff ) ) ||
                         ( wIt < zDiff ) || ( wIt > static_cast<int> ( this->_maxMapW - zDiff ) ) )
                    {
                        hlpPos                = wIt  + (this->_maxMapW + 1) * ( vIt  + (this->_maxMapV + 1) * uIt  );
                        hlpMap[hlpPos]        = 0.0;
                    }
                    else
                    {
                        newU                  = (uIt - xDiff);
                        newV                  = (vIt - yDiff);
                        newW                  = (wIt - zDiff);
                        
                        hlpPos                = wIt  + (this->_maxMapW + 1) * ( vIt  + (this->_maxMapV + 1) * uIt  );
                        arrPos                = newW + (this->_maxMapW + 1 - zDiff * 2) * ( newV + (this->_maxMapV + 1 - yDiff * 2) * newU );
                        
                        hlpMap[hlpPos]        = this->_densityMapMap[arrPos];
                    }
                }
            }
        }

        //==================================== Free memory
        if ( this->_densityMapMap != nullptr )
        {
            free                              ( this->_densityMapMap );
            this->_densityMapMap              = nullptr;
        }
        
        //==================================== Copy
        this->_densityMapMap                  = (float*) malloc ( ((this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1)) * sizeof ( float ) );
        if ( this->_densityMapMap == nullptr )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory for map data. Terminating... " << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "Cannot allocate memory for map data. Did you run out of memory?" << "</font>";
                rvapi_set_text                    ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                       ( );
            }
            
            exit ( -1 );
        }
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) ); iter++ )
        {
            this->_densityMapMap[iter]        = hlpMap[iter];
        }
        
        //==================================== Free memory
        if ( hlpMap != nullptr )
        {
            free                              ( hlpMap );
            hlpMap                            = nullptr;
        }
    }
    else
    {
        this->_exCeSpDiffX                = 0;
        this->_exCeSpDiffY                = 0;
        this->_exCeSpDiffZ                = 0;
    }
    
    //======================================== Deal with oversampling
    if ( ( this->_mapResolution / 2.0 ) > maxSamplingRate )
    {
        maxSamplingRate                       = ( this->_mapResolution / 2.0 );
    }
    else
    {
        std::cerr << "!!! ProSHADE WARNING !!! The map sampling does not support the required resolution. This means that your map will be over-sampled and the computation will take longer than needed. Use higher resolution value to resolve." << std::endl;
    }
    
    //======================================== If bandwidth is not given, determine it
    if ( *bandwidth == 0 )
    {
        *bandwidth                            = std::ceil ( this->_maxMapRange / 4.0 );
    }
    
    if ( settings->maxRotError != 0 )
    {
        *bandwidth                            = static_cast<unsigned int> ( 180 / settings->maxRotError );
        this->_wasBandwithGiven               = true;
    }
    
    //======================================== Decide shell spacing, if not already decided
    if ( *shellDistance == 0 )
    {
        settings->shellSpacing                = static_cast<double> ( this->_maxMapRange / 2.0 ) / std::floor ( static_cast<double> ( this->_maxMapRange / 2.0 ) / static_cast<double> ( this->_mapResolution / 2.0 ) );
        this->_shellSpacing                   = settings->shellSpacing;
        *shellDistance                         = this->_shellSpacing;
        
        while ( std::floor ( this->_maxMapRange / this->_shellSpacing ) < 20 )
        {
            this->_shellSpacing              /= 2.0;
            settings->shellSpacing           /= 2.0;
            *shellDistance                   /= 2.0;
        }
    }
    
    //======================================== Automatically determine maximum number of shells
    this->_shellPlacement                     = std::vector<double> ( std::floor ( this->_maxMapRange / this->_shellSpacing ) );
    
    for ( unsigned int shellIter = 0; shellIter < static_cast<unsigned int> ( this->_shellPlacement.size() ); shellIter++ )
    {
        this->_shellPlacement.at(shellIter)   = (shellIter+1) * this->_shellSpacing;
    }
    
    //======================================== If theta and phi are not given, determine them
    if ( *theta == 0 ) { *theta               = 2 * (*bandwidth); }
    if ( *phi   == 0 ) { *phi                 = 2 * (*bandwidth); }
    
    //======================================== If Gauss-Legendre integration order is not given, decide it
    if ( *glIntegOrder == 0 )
    {
        double distPerPointFraction           = static_cast<double> ( this->_shellSpacing ) / ( this->_maxMapRange / 2.0 );
        
        for ( unsigned int iter = 2; iter < static_cast<unsigned int> ( ProSHADE_internal_misc::glIntMaxDists.size() ); iter++ )
        {
            if ( ProSHADE_internal_misc::glIntMaxDists.at(iter) >= distPerPointFraction )
            {
                *glIntegOrder                 = iter;
            }
        }
    }
    
    //======================================== Done
    this->_densityMapComputed                 = true;
    this->_fromPDB                            = false;
    
}

/*! \brief This function does normalises the map data.
 
 To make different structures more comparable, the internal map representation can be normalised as done by this function.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_data::normaliseMap ( ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Sanity checks
    if ( this->_densityMapMap == nullptr )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Attempted to normalise the internal map representation, but the values have already been deleted - either called the normaliseMap function too early or too late. Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Attempted to normalise the internal map representation, but the values have already been deleted - either called the normaliseMap function too early or too late. This seems like an internal bug, please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Get values
    std::vector<double> vals ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) );
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) ); iter++ )
    {
        vals.at(iter)                         = this->_densityMapMap[iter];
    }
    std::sort                                 ( vals.begin(), vals.end() );
    
    //======================================== Find mean and standard deviation for later normalisation
    this->_mapMean                            = std::accumulate ( vals.begin(), vals.end(), 0.0 ) / static_cast<double> ( vals.size() );
    this->_mapSdev                            = std::sqrt ( static_cast<double> ( std::inner_product ( vals.begin(), vals.end(), vals.begin(), 0.0 ) ) / static_cast<double> ( vals.size() ) - this->_mapMean * this->_mapMean );
    
    //======================================== Apply
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) ); iter++ )
    {
        this->_densityMapMap[iter]            = this->_densityMapMap[iter] / this->_mapSdev;
    }
    
    //======================================== Done
    return ;
}

/*! \brief This function gets COM and its distance to closes border.
 
 This function is used to evaluate whether overlay mode translation vector is too large so more space needs to be added to the structure, or whether the translation will be fine.
 
 \warning This is an internal function which should not be used by the user.
 */
std::array<double,4> ProSHADE_internal::ProSHADE_data::getCOMandDist ( ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Sanity checks
    if ( this->_densityMapMap == nullptr )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Attempted to get COM and distances for the internal map representation, but the values have already been deleted - either called the getCOMandDist function too early or too late. Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Attempted to normalise the internal map representation, but the values have already been deleted - either called the normaliseMap function too early or too late. This seems like an internal bug, please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Get COM
    unsigned int arrPos                       = 0;
    double totDens                            = 0.0;
    std::array<double,4> meanVals;
    meanVals[0]                               = 0.0;
    meanVals[1]                               = 0.0;
    meanVals[2]                               = 0.0;
    meanVals[3]                               = 0.0;
    std::vector<double> vals ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) );
    
    for ( unsigned int uIt = 0; uIt < static_cast<unsigned int> ( (this->_maxMapU+1) ); uIt++ )
    {
        for ( unsigned int vIt = 0; vIt < static_cast<unsigned int> ( (this->_maxMapV+1) ); vIt++ )
        {
            for ( unsigned int wIt = 0; wIt < static_cast<unsigned int> ( (this->_maxMapW+1) ); wIt++ )
            {
                arrPos                        = wIt + (this->_maxMapW + 1) * ( vIt + (this->_maxMapV + 1) * uIt );
                
                if ( this->_densityMapMap[arrPos] > 0.0 )
                {
                    meanVals[0]              += this->_densityMapMap[arrPos] * uIt;
                    meanVals[1]              += this->_densityMapMap[arrPos] * vIt;
                    meanVals[2]              += this->_densityMapMap[arrPos] * wIt;
                    
                    totDens                  += this->_densityMapMap[arrPos];
                }
            }
        }
    }
    
    meanVals[0]                              /= totDens;
    meanVals[1]                              /= totDens;
    meanVals[2]                              /= totDens;
    
    //======================================== Find distances to borders
    double distX                              = 0.0;
    double distY                              = 0.0;
    double distZ                              = 0.0;
    
    if ( meanVals[0] <= static_cast<double> ((this->_maxMapU+1)/2) )
    {
        distX                                 = meanVals[0] * this->_xSamplingRate;
    }
    else
    {
        distX                                 = ( static_cast<double> ( this->_maxMapU+1 ) - meanVals[0] ) * this->_xSamplingRate;
    }
    
    if ( meanVals[1] <= static_cast<double> ((this->_maxMapV+1)/2) )
    {
        distY                                 = meanVals[1] * this->_ySamplingRate;
    }
    else
    {
        distY                                 = ( static_cast<double> ( this->_maxMapV+1 ) - meanVals[1] ) * this->_ySamplingRate;
    }
    
    if ( meanVals[2] <= static_cast<double> ((this->_maxMapW+1)/2) )
    {
        distZ                                 = meanVals[2] * this->_zSamplingRate;
    }
    else
    {
        distZ                                 = ( static_cast<double> ( this->_maxMapW+1 ) - meanVals[2] ) * this->_zSamplingRate;
    }
    
    //======================================== Get total distance
    meanVals[3]                               = sqrt ( pow( distX, 2.0 ) + pow ( distY, 2.0 ) + pow ( distZ, 2.0 ) );
    
    //======================================== Done
    return ( meanVals );
}

/*! \brief This function does the map masking for cleaning and re-boxing maps.
 
 This function is called with the map dimmensions and blurring factor. It then proceeds to compute the fourier transform and add the blurring. Then, it takes the resulting map and
 it takes given number (default 3.0) interquartile ranges from the third quartile as a threshold to obtain the mask, which it then applies to the map.
 
 \param[in] hlpU The number of indices in the U dimmension.
 \param[in] hlpV The number of indices in the V dimmension.
 \param[in] hlpW The number of indices in the W dimmension.
 \param[in] blurBy The parameter stating what amount of blurring should be applied.
 \param[in] maxMapIQR The number of interquartile ranges to use as a threshold for map masking.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_data::maskMap ( int hlpU,
                                                 int hlpV,
                                                 int hlpW,
                                                 double blurBy,
                                                 double maxMapIQR,
                                                 double extraMaskSpace )
{
    //================================ Initialise the data structures
    fftw_complex*                             tmpIn;
    fftw_complex*                             tmpOut;
    tmpIn                                     = new fftw_complex[hlpU * hlpV * hlpW];
    tmpOut                                    = new fftw_complex[hlpU * hlpV * hlpW];
    fftw_complex* maskData                    = new fftw_complex [ hlpU * hlpV * hlpW ];
    fftw_complex* maskDataInv                 = new fftw_complex [ hlpU * hlpV * hlpW ];
    
    //================================ Initialise local variables
    int h, k, l;
    int uIt, vIt, wIt;
    double mag, phase, S;
    double normFactor                         = (hlpU * hlpV * hlpW);
    double bFacChange                         = blurBy;
    double real                               = 0.0;
    double imag                               = 0.0;
    unsigned int hlpIt                        = 0;
    int arrayPos                              = 0;
    fftw_plan p;
    
    //======================================== Load map data for Fourier
    for ( uIt = 0; uIt < hlpU; uIt++ )
    {
        for ( vIt = 0; vIt < hlpV; vIt++ )
        {
            for ( wIt = 0; wIt < hlpW; wIt++ )
            {
                //============================ Initialisation
                arrayPos                      = wIt + hlpW * ( vIt + hlpV * uIt );
                
                if ( this->_densityMapCor[arrayPos] == this->_densityMapCor[arrayPos] )
                {
                    tmpIn[arrayPos][0]        = ( this->_densityMapCor[arrayPos] - this->_mapMean ) / this->_mapSdev;
                    tmpIn[arrayPos][1]        = 0.0;
                }
                else
                {
                    tmpIn[arrayPos][0]        = 0.0;
                    tmpIn[arrayPos][1]        = 0.0;
                }
            }
        }
    }
    
    //======================================== Create plans (FFTW_MEASURE would change the arrays)
    p                                         = fftw_plan_dft_3d ( hlpU, hlpV, hlpW, tmpIn, tmpOut, FFTW_FORWARD, FFTW_ESTIMATE );
    fftw_execute                              ( p );
    
    //================================ Prepare FFTW plan for mask FFTW
    fftw_plan pMaskInv                        = fftw_plan_dft_3d ( hlpU, hlpV, hlpW, maskDataInv, maskData, FFTW_BACKWARD, FFTW_ESTIMATE );
    
    for ( uIt = 0; uIt < hlpU; uIt++ )
    {
        for ( vIt = 0; vIt < hlpV; vIt++ )
        {
            for ( wIt = 0; wIt < hlpW; wIt++ )
            {
                //==================== Var init
                arrayPos              = wIt + hlpW * ( vIt + hlpV * uIt );
                real                  = tmpOut[arrayPos][0];
                imag                  = tmpOut[arrayPos][1];
                
                //==================== Change the B-factors, if required
                if ( uIt > (hlpU / 2) ) { h = uIt - hlpU; } else { h = uIt; }
                if ( vIt > (hlpV / 2) ) { k = vIt - hlpV; } else { k = vIt; }
                if ( wIt > (hlpW / 2) ) { l = wIt - hlpW; } else { l = wIt; }
                
                //==================== Get magnitude and phase with mask parameters
                S                     = ( pow( static_cast<double> ( h ) / this->_xRange, 2.0 ) +
                                         pow( static_cast<double> ( k ) / this->_yRange, 2.0 ) +
                                         pow( static_cast<double> ( l ) / this->_zRange, 2.0 ) );
                mag                   = sqrt ( (real*real) + (imag*imag) ) * std::exp ( - ( ( bFacChange * S ) / 4.0 ) );
                phase                 = atan2 ( imag, real );
                
                //==================== Save the mask data
                maskDataInv[arrayPos][0] = ( mag * cos(phase) ) / normFactor;
                maskDataInv[arrayPos][1] = ( mag * sin(phase) ) / normFactor;
            }
        }
    }
    
    //======================================== Execute the inversions
    fftw_execute                              ( pMaskInv );
    
    //======================================== Get the map threshold
    std::vector<double> maskVals              ( hlpU * hlpV * hlpW );
    hlpIt                                     = 0;
    for ( uIt = 0; uIt < hlpU; uIt++ )
    {
        for ( vIt = 0; vIt < hlpV; vIt++ )
        {
            for ( wIt = 0; wIt < hlpW; wIt++ )
            {
                //============================ Save to vector
                arrayPos                      = wIt + hlpW * ( vIt + hlpV * uIt );
                maskVals.at(hlpIt)            = maskData[arrayPos][0];
                hlpIt                        += 1;
                
            }
        }
    }
    
    double medianPos                          = static_cast<unsigned int> ( maskVals.size() / 2 );
    std::sort ( maskVals.begin(), maskVals.end() );
    
    double interQuartileRange                 = maskVals.at(medianPos+(medianPos/2)) - maskVals.at(medianPos-(medianPos/2));
    double mapThresholdPlus                   = maskVals.at(medianPos+(medianPos/2)) + ( interQuartileRange * maxMapIQR );
    maskVals.clear();
    
    //======================================== Add extra space to mask
    int addPoints                             = static_cast<int> ( std::max ( std::ceil ( extraMaskSpace / static_cast<double> ( this->_xRange / (hlpU-1) ) ), std::max ( std::ceil ( extraMaskSpace / static_cast<double> ( this->_yRange / (hlpV-1) ) ), std::ceil ( extraMaskSpace / static_cast<double> ( this->_zRange / (hlpW-1) ) ) ) ) );
    int arrPosSearch                          = 0;
    int newU, newV, newW;
    bool breakOut                             = false;
    
    for ( uIt = 0; uIt < hlpU; uIt++ )
    {
        for ( vIt = 0; vIt < hlpV; vIt++ )
        {
            for ( wIt = 0; wIt < hlpW; wIt++ )
            {
                //============================ Var init
                arrayPos                      = wIt + hlpW * ( vIt + hlpV * uIt );
                
                //============================ If already in mask, ignore
                if ( maskData[arrayPos][0] > mapThresholdPlus )
                {
                    tmpIn[arrayPos][0]        = 1.0;
                    continue;
                }
                
                //============================ If not, find if it is close to mask
                tmpIn[arrayPos][0]            = 0.0;
                breakOut                      = false;
                for ( int uCh = -addPoints; uCh <= addPoints; uCh++ )
                {
                    if ( breakOut ) { break; }
                    for ( int vCh = -addPoints; vCh <= addPoints; vCh++ )
                    {
                        if ( breakOut ) { break; }
                        for ( int wCh = -addPoints; wCh <= addPoints; wCh++ )
                        {
                            if ( breakOut ) { break; }
                            if ( ( uCh == 0 ) && ( vCh == 0 ) && wCh == 0 ) { continue; }
                            
                            newU              = uIt + uCh;
                            newV              = vIt + vCh;
                            newW              = wIt + wCh;
                            
                            if ( newU < 0 ) { newU += hlpU; } if ( newU >= hlpU ) { newU -= hlpU; }
                            if ( newV < 0 ) { newV += hlpV; } if ( newV >= hlpV ) { newV -= hlpV; }
                            if ( newW < 0 ) { newW += hlpW; } if ( newW >= hlpW ) { newW -= hlpW; }
                            
                            arrPosSearch      = newW + hlpW * ( newV + hlpV * newU );
                            
                            if ( maskData[arrPosSearch][0] > mapThresholdPlus )
                            {
                                tmpIn[arrayPos][0] = 1.0;
                                breakOut      = true;
                            }
                        }
                    }
                }
            }
        }
    }
    
    //======================================== Apply the mask
    for ( uIt = 0; uIt < hlpU; uIt++ )
    {
        for ( vIt = 0; vIt < hlpV; vIt++ )
        {
            for ( wIt = 0; wIt < hlpW; wIt++ )
            {
                //============================ Var init
                arrayPos                     = wIt + hlpW * ( vIt + hlpV * uIt );
                
                //============================ Apply mask
                if ( tmpIn[arrayPos][0] == 0.0 )
                {
                    this->_densityMapCor[arrayPos] = 0.0;
                }
            }
        }
    }
    
    //================================ Clean up
    delete[] maskData;
    maskData                                  = nullptr;
    delete[] maskDataInv;
    maskDataInv                               = nullptr;
    delete[] tmpIn;
    tmpIn                                     = nullptr;
    delete[] tmpOut;
    tmpOut                                    = nullptr;
    
    //================================ Delete the plans
    fftw_destroy_plan                         ( p );
    fftw_destroy_plan                         ( pMaskInv );
    
    //================================ Done
    return ;
}

/*! \brief This function does the island detection support and decides whether different masking should be applied or whether the current level is fine.
 
 This function starts with island detection and then proceeds to check whether there are just too many small islands, so that higher masking threshold could be beneficial. If it decides
 that the masking is about right, it checks specifically clusters in the map corners, as these tend not to be correct. It then finishes, returning value stating that no more map masking
 threshold changes are neeeded.
 
 \param[in] hlpU The number of indices in the U dimmension.
 \param[in] hlpV The number of indices in the V dimmension.
 \param[in] hlpW The number of indices in the W dimmension.
 \param[in] verbose How much output would you like on the screen.
 \param[in] runAll If this parameter is true, will run even after determining that more masking would be beneficial.
 \param[out] notTooManyIslands Bool value being false if it seems like a decent number of islands and true otherwise.
 
 \warning This is an internal function which should not be used by the user.
 */
bool ProSHADE_internal::ProSHADE_data::removeIslands ( int hlpU,
                                                       int hlpV,
                                                       int hlpW,
                                                       int verbose,
                                                       bool runAll )
{
    //======================================== Get the list of islands
    std::vector< std::vector<int> > clusters  = this->findMapIslands ( hlpU, hlpV, hlpW, this->_densityMapCor );
    if ( clusters.size() == 0 )
    {
        return ( true );
    }
    
    //======================================== Decide which islands to keep
    int maxVolume                             = 0;
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( clusters.size() ); iter++ )
    {
        maxVolume                             = std::max ( maxVolume, static_cast<int> ( clusters.at(iter).size() ) );
    }
    
    //======================================== Are there too many islands?
    int fracLess10                            = 0;
    int volTop80                              = 0;
    int totVol                                = 0;
    int volLow20                              = 0;
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( clusters.size() ); iter++ )
    {
        totVol                               += clusters.at(iter).size();
        if ( static_cast<double> ( clusters.at(iter).size() ) > ( 0.8 * maxVolume ) )
        {
            volTop80                         += clusters.at(iter).size();
        }
        if ( static_cast<double> ( clusters.at(iter).size() ) < ( 0.1 * maxVolume ) )
        {
            fracLess10                       += 1;
        }
        if ( static_cast<double> ( clusters.at(iter).size() ) < ( 0.2 * maxVolume ) )
        {
            volLow20                         += clusters.at(iter).size();
        }
    }
    
    //======================================== More computations needed?
    if ( ( fracLess10 != 0 ) || ( clusters.size() > 5 ) )
    {
        if ( ( ( static_cast<double> ( fracLess10 ) / static_cast<double> ( clusters.size() ) ) > 0.5 ) ||
             ( ( static_cast<double> ( volTop80 )   / static_cast<double> ( totVol          ) ) < 0.9 ) ||
             ( ( static_cast<double> ( volLow20 )   / static_cast<double> ( totVol          ) ) > 0.1 ) )
        {
            if ( !runAll )
            {
                return ( true );
            }
        }
    }
    
    //======================================== Check for corners
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> Cluster check started." << std::endl;
    }

    //======================================== Apply the islands
    double *tmpIn                             = new double[hlpU * hlpV * hlpW];
    std::vector<int> removedClusters;
    
    for ( unsigned int i = 0; i < static_cast<unsigned int> ( hlpU * hlpV * hlpW ); i++ )
    {
        tmpIn[i]                              = this->_densityMapCor[i];
        this->_densityMapCor[i]               = 0.0;
    }

    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( clusters.size() ); iter++ )
    {
        if ( static_cast<double> ( clusters.at(iter).size() ) < ( 0.3 * static_cast<double> ( maxVolume ) ) )
        {
            continue;
        }
        else
        {
            removedClusters.emplace_back ( iter );
            for ( unsigned int it = 0; it < static_cast<unsigned int> ( clusters.at(iter).size() ); it++ )
            {
                this->_densityMapCor[clusters.at(iter).at(it)] = tmpIn[clusters.at(iter).at(it)];
            }
        }
    }
    
    //======================================== Check all values for having 3A to passing value
    int noPoints                              = std::ceil ( 3.0 / std::max ( this->_xRange / hlpU, std::max ( this->_yRange / hlpV, this->_zRange / hlpW ) ) );
    int arrPos                                = 0;
    int arrPos2                               = 0;
    bool addPoint                             = false;
    int uIt, vIt, wIt;
    int hlpUIt, hlpVIt, hlpWIt;

    for ( unsigned int rIt = 0; rIt < static_cast<unsigned int> ( removedClusters.size() ); rIt++ )
    {
        for ( int rPt = 0; rPt < static_cast<int> ( clusters.at(removedClusters.at(rIt)).size() ); rPt++ )
        {
            //================================ Point was removed. Check if it is at least 3A to the closest remaining point
            uIt                               = ( clusters.at(removedClusters.at(rIt)).at(rPt) / ( hlpW * hlpV ) ) % hlpW;
            vIt                               = static_cast<int> ( std::floor ( clusters.at(removedClusters.at(rIt)).at(rPt) / hlpW ) ) % hlpV;
            wIt                               = clusters.at(removedClusters.at(rIt)).at(rPt) - ( uIt * hlpW * hlpV ) - ( vIt * hlpW );

            addPoint                          = false;
            for ( int newX = uIt-noPoints; newX <= uIt+noPoints; newX++ )
            {
                if ( addPoint ) { break; }
                for ( int newY = vIt-noPoints; newY <= vIt+noPoints; newY++ )
                {
                    if ( addPoint ) { break; }
                    for ( int newZ = wIt-noPoints; newZ <= wIt+noPoints; newZ++ )
                    {
                        if ( ( newX == uIt ) && ( newY == vIt ) && ( newZ == wIt ) ) { continue; }

                        hlpUIt                = newX;
                        hlpVIt                = newY;
                        hlpWIt                = newZ;
                        
                        if ( hlpUIt < 0 ) { hlpUIt += hlpU; } if ( hlpUIt >= hlpU ) { hlpUIt -= hlpU; }
                        if ( hlpVIt < 0 ) { hlpVIt += hlpV; } if ( hlpVIt >= hlpV ) { hlpVIt -= hlpV; }
                        if ( hlpWIt < 0 ) { hlpWIt += hlpW; } if ( hlpWIt >= hlpW ) { hlpWIt -= hlpW; }
                        
                        arrPos2               = hlpWIt + hlpW * ( hlpVIt + hlpV * hlpUIt );
                        if ( this->_densityMapCor[arrPos2] > 0.0 ) { addPoint = true; break; }
                    }
                }
            }

            if ( addPoint )
            {
                arrPos                        = wIt + hlpW * ( vIt + hlpV * uIt );
                this->_densityMapCor[arrPos]  = -999.999;
            }
        }
    }

    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( hlpU * hlpV * hlpW ); iter++ )
    {
        if ( this->_densityMapCor[iter] == -999.999 )
        {
            this->_densityMapCor[iter]        = tmpIn[iter];
        }
    }
    
    //======================================== Free memory
    delete[] tmpIn;
    
    //======================================== Done
    return ( false );
}

/*! \brief This function shifts (changing header) the data along the three coordinate axis by the amount given by the three parameters.
 
 This function simply changes the header of a map to change the location of the map when visualised. This function will not affect the
 coordinate input data!
 
 \param[in] xShift The number of angstroms by which the structure should be shifted along the X axis.
 \param[in] yShift The number of angstroms by which the structure should be shifted along the Y axis.
 \param[in] zShift The number of angstroms by which the structure should be shifted along the Z axis.
 
 \warning This is an internal function which should not be used by the user.
 \warning This function will have no effect on coordinate input data.
 */
void ProSHADE_internal::ProSHADE_data::shiftMap ( double xShift,
                                                  double yShift,
                                                  double zShift )
{
    //======================================== Change xFrom, xTo, yFrom, yTo, zFrom and zTo
    this->_xFrom                             += xShift;
    this->_xTo                               += xShift;
    this->_yFrom                             += yShift;
    this->_yTo                               += yShift;
    this->_zFrom                             += zShift;
    this->_zTo                               += zShift;
    
    //======================================== Done
    
}

/*! \brief This function translates the data along the three coordinate axis by the amount given by the three parameters.
 
 This function uses the FFT method to translate the density map (if _densityMapCor exists, then _densityMapCor, otherwise _densityMapMap) by
 the distances given by the three arguments in angstroms. It does not require interpolation as all is done in Fourier space.
 
 \param[in] xShift The number of angstroms by which the structure should be translated along the X axis.
 \param[in] yShift The number of angstroms by which the structure should be translated along the Y axis.
 \param[in] zShift The number of angstroms by which the structure should be translated along the Z axis.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_data::translateMap ( double xShift,
                                                      double yShift,
                                                      double zShift )
{
    //======================================== Initialise
    int hlpU                                  = this->_maxMapU + 1;
    int hlpV                                  = this->_maxMapV + 1;
    int hlpW                                  = this->_maxMapW + 1;
    
    int arrayPos                              = 0;
    int h, k, l;
    double real                               = 0.0;
    double imag                               = 0.0;
    double exponent                           = 0.0;
    double trCoeffReal                        = 0.0;
    double trCoeffImag                        = 0.0;
    double normFactor                         = static_cast<double> ( hlpU * hlpV * hlpW );
    fftw_complex *translatedMap               = new fftw_complex [hlpU * hlpV * hlpW];
    fftw_complex *fCoeffs                     = new fftw_complex [hlpU * hlpV * hlpW];
    std::array<double,2> hlpArr;
    fftw_plan planForwardFourier;
    fftw_plan planBackwardFourier;
    
    
    //======================================== Create plans
    planForwardFourier                        = fftw_plan_dft_3d ( hlpU, hlpV, hlpW, translatedMap, fCoeffs, FFTW_FORWARD,  FFTW_ESTIMATE );
    planBackwardFourier                       = fftw_plan_dft_3d ( hlpU, hlpV, hlpW, fCoeffs, translatedMap, FFTW_BACKWARD, FFTW_ESTIMATE );
        
    //======================================== Decide which map variable should be translated
    if ( this->_densityMapCor == nullptr )
    {
        for ( int uIt = 0; uIt < hlpU; uIt++ )
        {
            for ( int vIt = 0; vIt < hlpV; vIt++ )
            {
                for ( int wIt = 0; wIt < hlpW; wIt++ )
                {
                    arrayPos                  = wIt + hlpW * ( vIt + hlpV * uIt );
                    
                    translatedMap[arrayPos][0]    = this->_densityMapMap[arrayPos];
                    translatedMap[arrayPos][1]    = 0.0;
                }
            }
        }
    }
    else
    {
        for ( int uIt = 0; uIt < hlpU; uIt++ )
        {
            for ( int vIt = 0; vIt < hlpV; vIt++ )
            {
                for ( int wIt = 0; wIt < hlpW; wIt++ )
                {
                    arrayPos                  = wIt + hlpW * ( vIt + hlpV * uIt );
                    
                    translatedMap[arrayPos][0]    = this->_densityMapCor[arrayPos];
                    translatedMap[arrayPos][1]    = 0.0;
                }
            }
        }
    }
    
    //======================================== Compute Fourier
    fftw_execute                              ( planForwardFourier );
    
    for ( int uIt = 0; uIt < hlpU; uIt++ )
    {
        for ( int vIt = 0; vIt < hlpV; vIt++ )
        {
            for ( int wIt = 0; wIt < hlpW; wIt++ )
            {
                //============================ Var init
                arrayPos                      = wIt + hlpW * ( vIt + hlpV * uIt );
                real                          = fCoeffs[arrayPos][0];
                imag                          = fCoeffs[arrayPos][1];

                //======================== Change the B-factors, if required
                if ( uIt > ((hlpU+1) / 2) ) { h = uIt - hlpU; } else { h = uIt; }
                if ( vIt > ((hlpV+1) / 2) ) { k = vIt - hlpV; } else { k = vIt; }
                if ( wIt > ((hlpW+1) / 2) ) { l = wIt - hlpW; } else { l = wIt; }

                //============================ Get translation coefficient change
                exponent                      = ( ( ( static_cast <double> ( h ) / this->_xRange ) * xShift ) +
                                                  ( ( static_cast <double> ( k ) / this->_yRange ) * yShift ) +
                                                  ( ( static_cast <double> ( l ) / this->_zRange ) * zShift ) ) * 2.0 * M_PI;

                trCoeffReal                   = cos ( exponent );
                trCoeffImag                   = sin ( exponent );

                hlpArr                        = ProSHADE_internal_misc::complexMultiplication ( &real, &imag, &trCoeffReal, &trCoeffImag );

                //============================ Save the mask data
                fCoeffs[arrayPos][0]          = hlpArr[0] / normFactor;
                fCoeffs[arrayPos][1]          = hlpArr[1] / normFactor;
            }
        }
    }

    //======================================== Compute inverse Fourier
    fftw_execute                              ( planBackwardFourier );
    
    //======================================== Decide which map variable should be translated and remove corners (sometimes have HUUUUUGE values for some reason...)
    if ( this->_densityMapCor == nullptr )
    {
        for ( int uIt = 0; uIt < hlpU; uIt++ )
        {
            for ( int vIt = 0; vIt < hlpV; vIt++ )
            {
                for ( int wIt = 0; wIt < hlpW; wIt++ )
                {
                    arrayPos                  = wIt + hlpW * ( vIt + hlpV * uIt );
                    this->_densityMapMap[arrayPos] = static_cast<float> ( translatedMap[arrayPos][0] );
                }
            }
        }
    }
    else
    {
        for ( int uIt = 0; uIt < hlpU; uIt++ )
        {
            for ( int vIt = 0; vIt < hlpV; vIt++ )
            {
                for ( int wIt = 0; wIt < hlpW; wIt++ )
                {                    
                    arrayPos                  = wIt + hlpW * ( vIt + hlpV * uIt );
                    this->_densityMapCor[arrayPos] = translatedMap[arrayPos][0];
                }
            }
        }
    }

    //======================================== Free memory
    fftw_destroy_plan                         ( planForwardFourier );
    fftw_destroy_plan                         ( planBackwardFourier );
    delete[] translatedMap;
    delete[] fCoeffs;
        
    //======================================== Done
    return ;
}

/*! \brief This function reads in the CCP4' MAP formatted file and re-boxes its contents using the masking procedure, adding some extra space if required.

 XXX
 
 \param[in] fileName The file name (and path) to the PDB file to be read in.
 \param[in] maxMapIQR A double value to specify the number of interquartile ranges from the median to be used to threshold map masking.
 \param[in] extraCS This value specifies how many angstroms should be added to each box size over the map determined minimum.
 \param[in] verbose Int value specifying how loud the function should in the standard output.
 \param[in] useCubicMaps This parameter determines if the clear map compued should be rectangular or cubic (older versions of refmac may require this).
 \param[in] maskBlurFactor How much should the map be blurred to compute the mask?
 \param[in] maskBlurFactorGiven Was the blurring factor value supplied by the user, or are we working with default?
 
 \warning This is an internal function which should not be used by the user.
 */
std::array<double,6> ProSHADE_internal::ProSHADE_data::getDensityMapFromMAPRebox ( std::string fileName,
                                                                                   double maxMapIQR,
                                                                                   double extraCS,
                                                                                   int verbose,
                                                                                   bool useCubicMaps,
                                                                                   double maskBlurFactor,
                                                                                   bool maskBlurFactorGiven,
                                                                                   ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Initialise internal values
    this->_inputFileName                      = fileName;
    this->_maxExtraCellularSpace              = extraCS;
    
    //======================================== Initialise local variables
    CMap_io::CMMFile *mapFile                 = nullptr;
    float *cell                               = nullptr;
    int *grid                                 = nullptr;
    int *dim                                  = nullptr;
    int *order                                = nullptr;
    int *orig                                 = nullptr;
    int myMapMode                             = 0;
    double cornerAvg                          = 0.0;
    double centralAvg                         = 0.0;
    double cornerCount                        = 0.0;
    double centralCount                       = 0.0;
    int XYZOrder[3];
    std::array<double,6>                      ret;
    
    //======================================== Allocate memory
    cell                                      = (float*) malloc ( 6 * sizeof ( float ) );
    grid                                      = (int*  ) malloc ( 3 * sizeof ( int   ) );
    dim                                       = (int*  ) malloc ( 3 * sizeof ( int   ) );
    order                                     = (int*  ) malloc ( 3 * sizeof ( int   ) );
    orig                                      = (int*  ) malloc ( 3 * sizeof ( int   ) );
    
    if ( ( cell == nullptr ) || ( dim == nullptr ) || ( order == nullptr ) || ( orig == nullptr ) || ( grid == nullptr ) )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory. Terminating... " << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Cannot allocate memory for map data. Could you have run out of memory?" << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Read in the MAP file info
    mapFile                                   = reinterpret_cast<CMap_io::CMMFile*> ( CMap_io::ccp4_cmap_open ( fileName.c_str() , myMapMode ) );
    if ( mapFile == nullptr )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Failed to open the file " << fileName << ". Check for typos and curruption of the file. Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Cannot open file " << fileName << ".</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    CMap_io::ccp4_cmap_get_cell               ( mapFile, cell  );
    CMap_io::ccp4_cmap_get_dim                ( mapFile, dim   );
    CMap_io::ccp4_cmap_get_grid               ( mapFile, grid  );
    CMap_io::ccp4_cmap_get_order              ( mapFile, order );
    CMap_io::ccp4_cmap_get_origin             ( mapFile, orig  );
    
    //======================================== If CUBIC
    if ( ( ( cell[3] < (90.0 + 0.1) ) && ( cell[3] > (90.0 - 0.1) ) ) &&
         ( ( cell[4] < (90.0 + 0.1) ) && ( cell[4] > (90.0 - 0.1) ) ) &&
         ( ( cell[5] < (90.0 + 0.1) ) && ( cell[5] > (90.0 - 0.1) ) ) )
    {
        //==================================== Determine/Save dimmensions
        this->_xRange                         = cell[0];
        this->_yRange                         = cell[1];
        this->_zRange                         = cell[2];
        
        //==================================== Maximal cell dimensions
        this->_maxMapRange                    = std::max ( this->_xRange, std::max( this->_yRange, this->_zRange ) );
        
        //==================================== Find max U, V and W
        if ( order[0] == 1 ) { this->_maxMapU = dim[0] - 1; this->_xFrom = orig[0]; }
        if ( order[0] == 2 ) { this->_maxMapU = dim[1] - 1; this->_yFrom = orig[0]; }
        if ( order[0] == 3 ) { this->_maxMapU = dim[2] - 1; this->_zFrom = orig[0]; }
        
        if ( order[1] == 1 ) { this->_maxMapV = dim[0] - 1; this->_xFrom = orig[1]; }
        if ( order[1] == 2 ) { this->_maxMapV = dim[1] - 1; this->_yFrom = orig[1]; }
        if ( order[1] == 3 ) { this->_maxMapV = dim[2] - 1; this->_zFrom = orig[1]; }
        
        if ( order[2] == 1 ) { this->_maxMapW = dim[0] - 1; this->_xFrom = orig[2]; }
        if ( order[2] == 2 ) { this->_maxMapW = dim[1] - 1; this->_yFrom = orig[2]; }
        if ( order[2] == 3 ) { this->_maxMapW = dim[2] - 1; this->_zFrom = orig[2]; }
        
        
        this->_preCBSU                         = this->_maxMapU;
        this->_preCBSV                         = this->_maxMapV;
        this->_preCBSW                         = this->_maxMapW;
        
        //==================================== Get the map start and end
        this->_xTo                            = this->_xFrom + this->_maxMapU;
        this->_yTo                            = this->_yFrom + this->_maxMapV;
        this->_zTo                            = this->_zFrom + this->_maxMapW;
        
        //==================================== Allocate map data memory
        this->_densityMapMap                  = (float*) malloc ( (dim[0]*dim[1]*dim[2]) * sizeof ( float ) );
        if ( this->_densityMapMap == nullptr )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory for map data. Terminating... " << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "Cannot allocate memory for map data. Did you run out of memory?" << "</font>";
                rvapi_set_text                    ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                       ( );
            }
            
            exit ( -1 );
        }
        
        //==================================== Check if the mode is compatible
        int mapMode                           = CMap_io::ccp4_cmap_get_datamode ( mapFile );
        if ( ( mapMode != 0 ) && ( mapMode != 2 ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Cannot read from the map file. The map file mode is not supported. Terminating..." << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "Cannot read from the map file. The map mode is neither 0, nor 2; other modes are not supperted at the moment." << "</font>";
                rvapi_set_text                    ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                       ( );
            }
            
            exit ( -1 );
        }
        
        //==================================== Read in map data
        // ... Find the stop positions (start is in orig variables) and the axis order
        int maxLim[3];
        int newU, newV, newW;
        int arrPos;
        
        for ( int iter = 0; iter < 3; iter++ )
        {
            maxLim[iter]                      = orig[iter] + dim[iter] - 1;
            XYZOrder[order[iter]-1]           = iter;
        }
        
        // ... Solve the dimensions and sizes
        int fastDimSize                       = ( maxLim[0] - orig[0] + 1 );
        int midDimSize                        = ( maxLim[1] - orig[1] + 1 ) * fastDimSize;
        std::vector<float> section( midDimSize );
        int index;
        int iters[3];
        
        // ... Read in the map data
        for ( iters[2] = orig[2]; iters[2] <= maxLim[2]; iters[2]++ )
        {
            index                             = 0;
            CMap_io::ccp4_cmap_read_section( mapFile, &section[0] );
            
            if ( mapMode == 0 )
            {
                for ( int iter = ( midDimSize - 1 ); iter >= 0; iter-- )
                {
                    section[iter]             = static_cast<float> ( ( reinterpret_cast<unsigned char*> (&section[0]) )[iter] );
                }
            }
            
            for ( iters[1] = orig[1]; iters[1] <= maxLim[1]; iters[1]++ )
            {
                for ( iters[0] = orig[0]; iters[0] <= maxLim[0]; iters[0]++ )
                {
                    newU                      = iters[XYZOrder[0]] - orig[XYZOrder[0]];
                    newV                      = iters[XYZOrder[1]] - orig[XYZOrder[1]];
                    newW                      = iters[XYZOrder[2]] - orig[XYZOrder[2]];
                    arrPos                    = newW + (this->_maxMapW + 1) * ( newV + (this->_maxMapV + 1) * newU );
                    this->_densityMapMap[arrPos] = static_cast<float> ( section[ index++ ] );
                }
            }
        }
        
        //==================================== Get values for normalisation
        std::vector<double> vals              ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) );
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( dim[0] * dim[1] * dim[2] ); iter++ )
        {
            vals.at(iter)                     = this->_densityMapMap[iter];
        }
        std::sort                             ( vals.begin(), vals.end() );
        
        //==================================== Find mean and standard deviation for later normalisation
        this->_mapMean                        = std::accumulate ( vals.begin(), vals.end(), 0.0 ) / static_cast<double> ( vals.size() );
        this->_mapSdev                        = std::sqrt ( static_cast<double> ( std::inner_product ( vals.begin(), vals.end(), vals.begin(), 0.0 ) ) / static_cast<double> ( vals.size() ) - this->_mapMean * this->_mapMean );
        vals.clear                            ( );
        
        //==================================== Check if the map is not absolutely de-centered
        int noPToCheckU                       = static_cast<unsigned int> ( this->_maxMapU / 8.0 );
        int noPToCheckV                       = static_cast<unsigned int> ( this->_maxMapV / 8.0 );
        int noPToCheckW                       = static_cast<unsigned int> ( this->_maxMapW / 8.0 );
        
        for ( int uIt = 0; uIt < static_cast<int> (this->_maxMapU+1); uIt++ )
        {
            for ( int vIt = 0; vIt < static_cast<int> (this->_maxMapV+1); vIt++ )
            {
                for ( int wIt = 0; wIt < static_cast<int> (this->_maxMapW+1); wIt++ )
                {
                    //======================== Initialisation
                    arrPos                    = wIt + (this->_maxMapW + 1) * ( vIt + (this->_maxMapV + 1) * uIt );
                    
                    //======================== Check to which quadrant the value belongs to
                    if ( ( ( uIt <= static_cast<int> ( this->_maxMapU/2 + noPToCheckU ) ) && ( uIt >= static_cast<int> ( this->_maxMapU/2 - noPToCheckU ) ) ) &&
                        ( ( vIt <= static_cast<int> ( this->_maxMapV/2 + noPToCheckV ) ) && ( vIt >= static_cast<int> ( this->_maxMapV/2 - noPToCheckV ) ) ) &&
                        ( ( wIt <= static_cast<int> ( this->_maxMapW/2 + noPToCheckW ) ) && ( wIt >= static_cast<int> ( this->_maxMapW/2 - noPToCheckW ) ) ) )
                    {
                        centralAvg           += std::max ( this->_densityMapMap[arrPos], static_cast<float>(0.0) );
                        centralCount         += 1.0;
                    }
                    if ( ( ( uIt < noPToCheckU )                  && ( vIt < noPToCheckV                  ) && ( wIt < noPToCheckW                  ) ) ||
                         ( ( uIt < noPToCheckU )                  && ( vIt < noPToCheckV                  ) && ( wIt > static_cast<int> (this->_maxMapW-noPToCheckW) ) ) ||
                         ( ( uIt < noPToCheckU )                  && ( vIt > static_cast<int> (this->_maxMapV-noPToCheckV) ) && ( wIt < noPToCheckW                  ) ) ||
                         ( ( uIt < noPToCheckU )                  && ( vIt > static_cast<int> (this->_maxMapV-noPToCheckV) ) && ( wIt > static_cast<int> (this->_maxMapW-noPToCheckW) ) ) ||
                         ( ( uIt > static_cast<int> (this->_maxMapU-noPToCheckU) ) && ( vIt < noPToCheckV                  ) && ( wIt < noPToCheckW                  ) ) ||
                         ( ( uIt > static_cast<int> (this->_maxMapU-noPToCheckU) ) && ( vIt < noPToCheckV                  ) && ( wIt > static_cast<int> (this->_maxMapW-noPToCheckW) ) ) ||
                         ( ( uIt > static_cast<int> (this->_maxMapU-noPToCheckU) ) && ( vIt > static_cast<int> (this->_maxMapV-noPToCheckV) ) && ( wIt < noPToCheckW                  ) ) ||
                         ( ( uIt > static_cast<int> (this->_maxMapU-noPToCheckU) ) && ( vIt > static_cast<int> (this->_maxMapV-noPToCheckV) ) && ( wIt > static_cast<int> (this->_maxMapW-noPToCheckW) ) ) )
                    {
                        cornerAvg            += std::max ( this->_densityMapMap[arrPos], static_cast<float>(0.0) );
                        cornerCount          += 1.0;
                    }
                }
            }
        }
        
        cornerAvg                            /= cornerCount;
        centralAvg                           /= centralCount;
        
        //==================================== If density is in the corners
        if ( cornerAvg > centralAvg )
        {
            //================================ Initialise required variables
            float* hlpMap                     = nullptr;
            hlpMap                            = (float*) malloc ( (dim[0]*dim[1]*dim[2]) * sizeof ( float ) );
            if ( hlpMap == nullptr )
            {
                std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory for map data. Terminating... " << std::endl;

                if ( settings->htmlReport )
                {
                    std::stringstream hlpSS;
                    hlpSS << "<font color=\"red\">" << "Cannot allocate memory for map data. Could you have run out of memory?" << "</font>";
                    rvapi_set_text                    ( hlpSS.str().c_str(),
                                                       "ProgressSection",
                                                       settings->htmlReportLineProgress,
                                                       1,
                                                       1,
                                                       1 );
                    settings->htmlReportLineProgress += 1;
                    rvapi_flush                       ( );
                }

                exit ( -1 );
            }
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( dim[0] * dim[1] * dim[2] ); iter++ )
            {
                hlpMap[iter]                  = this->_densityMapMap[iter];
            }

            //================================ Transform
            unsigned int hlpPos;
            for ( int uIt = 0; uIt < static_cast<int> (this->_maxMapU+1); uIt++ )
            {
                for ( int vIt = 0; vIt < static_cast<int> (this->_maxMapV+1); vIt++ )
                {
                    for ( int wIt = 0; wIt < static_cast<int> (this->_maxMapW+1); wIt++ )
                    {
                        //==================== If in lower half, add half; if in upper halp, subtract half
                        if ( uIt < static_cast<int> (this->_maxMapU+1)/2 ) { newU = uIt + (this->_maxMapU+1)/2; } else { newU = uIt - (this->_maxMapU+1)/2; }
                        if ( vIt < static_cast<int> (this->_maxMapV+1)/2 ) { newV = vIt + (this->_maxMapV+1)/2; } else { newV = vIt - (this->_maxMapV+1)/2; }
                        if ( wIt < static_cast<int> (this->_maxMapW+1)/2 ) { newW = wIt + (this->_maxMapW+1)/2; } else { newW = wIt - (this->_maxMapW+1)/2; }

                        arrPos                = newW + (this->_maxMapW + 1) * ( newV + (this->_maxMapV + 1) * newU );
                        hlpPos                = wIt  + (this->_maxMapW + 1) * ( vIt  + (this->_maxMapV + 1) * uIt  );

                        //==================== Set the value into correct coords in the new map
                        this->_densityMapMap[arrPos] = hlpMap[hlpPos];
                    }
                }
            }

            //================================ Free memory
            free                              ( hlpMap );
        }
        
        if ( verbose > 3 )
        {
            std::cout << ">>>>> Density moved from corners to center, if applicable." << std::endl;
        }
    }
    else
    {
        std::cerr << "!!! ProSHADE ERROR !!! Non-orthogonal maps are not yet supported." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Detected non-orthogonal map. Currently, only the P1 maps are supported." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Structure " << settings->structFiles.at(0) << " read in." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    if ( settings->htmlReport )
    {
        rvapi_add_section                     ( "OrigHeaderSection",
                                                "Original structure header",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                false );
        settings->htmlReportLine             += 1;
        
        //==================================== Print max U, V and W
        std::stringstream hlpSS;
        hlpSS << "<pre><b>" << "Rows, Columns and Sections sizes: " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        int prec                              = 6;
        int UVWMAX                            = std::max ( ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( this->_maxMapU+1 ), prec ).length(),
                                                std::max ( ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( this->_maxMapV+1 ), prec ).length(),
                                                           ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( this->_maxMapW+1 ), prec ).length() ) );
        int FMAX                              = std::max ( ProSHADE_internal_misc::to_string_with_precision ( this->_xFrom, prec ).length(),
                                                std::max ( ProSHADE_internal_misc::to_string_with_precision ( this->_yFrom, prec ).length(),
                                                           ProSHADE_internal_misc::to_string_with_precision ( this->_zFrom, prec ).length() ) );
        int TMAX                              = std::max ( ProSHADE_internal_misc::to_string_with_precision ( this->_xTo, prec ).length(),
                                                std::max ( ProSHADE_internal_misc::to_string_with_precision ( this->_yTo, prec ).length(),
                                                           ProSHADE_internal_misc::to_string_with_precision ( this->_zTo, prec ).length() ) );
        int CDMAX                             = std::max ( ProSHADE_internal_misc::to_string_with_precision ( this->_xRange, prec ).length(),
                                                std::max ( ProSHADE_internal_misc::to_string_with_precision ( this->_yRange, prec ).length(),
                                                           ProSHADE_internal_misc::to_string_with_precision ( this->_zRange, prec ).length() ) );
        int CAMAX                             = std::max ( ProSHADE_internal_misc::to_string_with_precision ( cell[3], prec ).length(),
                                                std::max ( ProSHADE_internal_misc::to_string_with_precision ( cell[4], prec ).length(),
                                                           ProSHADE_internal_misc::to_string_with_precision ( cell[5], prec ).length() ) );
        int spacer                            = std::max ( UVWMAX, std::max ( FMAX, std::max ( TMAX, std::max ( CDMAX, CAMAX ) ) ) );
        if ( spacer < 5 ) { spacer            = 5; }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << static_cast<int> ( this->_maxMapU+1 );
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( this->_maxMapU+1 ), prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";

        hlpSS << std::showpos << static_cast<int> ( this->_maxMapV+1 );
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( this->_maxMapV+1 ), prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";

        hlpSS << std::showpos << static_cast<int> ( this->_maxMapW+1 );
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( this->_maxMapW+1 ), prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "OrigHeaderSection",
                                                0,
                                                0,
                                                1,
                                                1 );
        
        //==================================== Print from and to lims
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Start and stop points on columns, rows, sections: " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << this->_xFrom;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( this->_xFrom, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << this->_xTo;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( this->_xTo, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << this->_yFrom;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( this->_yFrom, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << this->_yTo;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( this->_yTo, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << " " << this->_zFrom;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( this->_zFrom, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << this->_zTo;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( this->_zTo, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "OrigHeaderSection",
                                                1,
                                                0,
                                                1,
                                                1 );
        
        //==================================== Print cell dimensions
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Cell dimensions (Angstrom): " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << this->_xRange;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( this->_xRange, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << this->_yRange;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( this->_yRange, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << this->_zRange;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( this->_zRange, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "OrigHeaderSection",
                                                2,
                                                0,
                                                1,
                                                1 );
        
        //==================================== Print cell dimensions
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Cell angles (Degrees): " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << cell[3];
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( cell[3], prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << cell[4];
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( cell[4], prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << cell[5];
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( cell[5], prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "OrigHeaderSection",
                                                3,
                                                0,
                                                1,
                                                1 );
        
        //==================================== Print cell dimensions
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Fast, medium, slow axes: " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        if      ( XYZOrder[0] == 0 ) { hlpSS << "  X"; }
        else if ( XYZOrder[0] == 1 ) { hlpSS << "  Y"; }
        else                         { hlpSS << "  Z"; }
        for ( int iter = 0; iter < static_cast<int> ( spacer - 1 ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        if      ( XYZOrder[1] == 0 ) { hlpSS << "X"; }
        else if ( XYZOrder[1] == 1 ) { hlpSS << "Y"; }
        else                         { hlpSS << "Z"; }
        for ( int iter = 0; iter < static_cast<int> ( spacer - 1 ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        if      ( XYZOrder[2] == 0 ) { hlpSS << "X"; }
        else if ( XYZOrder[2] == 1 ) { hlpSS << "Y"; }
        else                         { hlpSS << "Z"; }
        for ( int iter = 0; iter < static_cast<int> ( spacer - 1 ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "OrigHeaderSection",
                                               4,
                                               0,
                                               1,
                                               1 );
        
        rvapi_flush                           ( );
    }
    
    //======================================== Set return values
    ret[0]                                    = this->_maxMapU + 1;
    ret[1]                                    = this->_maxMapV + 1;
    ret[2]                                    = this->_maxMapW + 1;
    
    //======================================== Free memory
    if ( cell != nullptr )
    {
        free                                  ( cell );
        cell                                  = nullptr;
    }
    if ( dim != nullptr )
    {
        free                                  ( dim );
        dim                                  = nullptr;
    }
    if ( order != nullptr )
    {
        free                                  ( order );
        order                                 = nullptr;
    }
    if ( orig != nullptr )
    {
        free                                  ( orig );
        orig                                  = nullptr;
    }
    
    //======================================== Close the file
    CMap_io::ccp4_cmap_close                  ( mapFile );
    
    //======================================== Local hlp variables
    int hlpU                                  = ( this->_maxMapU + 1 );
    int hlpV                                  = ( this->_maxMapV + 1 );
    int hlpW                                  = ( this->_maxMapW + 1 );
    
    //======================================== Initialise local variables for Fourier and Inverse Fourier
    this->_densityMapCor                      = new double      [hlpU * hlpV * hlpW];
    
    //======================================== Load map data for Fourier
    std::vector<double> vals                  ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) );
    
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) ); iter++ )
    {
        if ( this->_densityMapMap[iter] == this->_densityMapMap[iter] )
        {
            //================================ Map value is NOT nan
            this->_densityMapCor[iter]        = this->_densityMapMap[iter];
        }
        else
        {
            //================================ Map value is nan - this sometimes occurs for a couple of values, I have no clue as to why...
            this->_densityMapCor[iter]        = 0.0;
        }
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>> Map normalised." << std::endl;
    }
    
    //======================================== Map masking
    double *tmpMp                             = new double[hlpU * hlpV * hlpW];
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( hlpU * hlpV * hlpW ); iter++ )
    {
        tmpMp[iter]                           = this->_densityMapCor[iter];
    }
    
    bool notTooManyIslads                     = true;
    double blFr                               = 150.0;
    
    while ( notTooManyIslads )
    {
        //================================ Do not change user values
        blFr                                 += 50.0;
        if ( maskBlurFactorGiven )
        {
            blFr                              = maskBlurFactor;
            this->maskMap                     ( hlpU, hlpV, hlpW, blFr, maxMapIQR, 3.0 );
            break;
        }
        
        //================================ Compute mask and apply it
        this->maskMap                         ( hlpU, hlpV, hlpW, blFr, maxMapIQR, 3.0 );
        
        //================================ Detect islands
        if ( verbose > 3 )
        {
            std::cout << ">>>>>>>> Island detection started." << std::endl;
        }
        
        notTooManyIslads                      = this->removeIslands ( hlpU, hlpV, hlpW, verbose, false );
        
        if ( verbose > 3 )
        {
            std::cout << ">>>>> Map masked with factor of " << blFr << " and small islands were then removed.";
        }
        if ( notTooManyIslads )
        {
            if ( verbose > 3 )
            {
                std::cout << " However, too many islands remained. Trying higher blurring factor." << std::endl;
            }
            
            if ( blFr <= 500.0 )
            {
                for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( hlpU * hlpV * hlpW ); iter++ )
                {
                    this->_densityMapCor[iter] = tmpMp[iter];
                }
            }
        }
        else
        {
            if ( verbose > 3 )
            {
                std::cout << std::endl;
            }
        }
        
        
        if ( blFr > 500.0 )
        {
            std::cout << "!!! ProSHADE WARNING !!! Even blurring factor of 500 did not remove islands. Will not proceed with what we have, but be warned that either the masking is bugged or yout map has high levels of noise. You can consider using the -y ( or --no_clear ) options to avoid this message and the extra time for testing different blurring factors for this map." << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"orange\">" << "Even blurring factor of 500 did not remove islands. Will not proceed with what we have, but be warned that either the masking is bugged or yout map has high levels of noise. You can consider using the -y ( or --no_clear ) options to avoid this message and the extra time for testing different blurring factors for this map." << "</font>";
                rvapi_set_text                    ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                       ( );
            }
            
            break;
        }
    }
    
    //======================================== Once values are decided, run then WITHOUT the island detection
    if ( !maskBlurFactorGiven )
    {
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( hlpU * hlpV * hlpW ); iter++ )
        {
            this->_densityMapCor[iter]        = tmpMp[iter];
        }
        
        this->maskMap                         ( hlpU, hlpV, hlpW, blFr, maxMapIQR, 3.0 );
    }
    
    if ( verbose > 2 )
    {
        std::cout << ">> Map masked." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Map masked." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Determine the real size of the shape
    int coordPos                              = 0;
    int maxX                                  = hlpU/2;
    int minX                                  = hlpU/2;
    int maxY                                  = hlpV/2;
    int minY                                  = hlpV/2;
    int maxZ                                  = hlpW/2;
    int minZ                                  = hlpW/2;
    int maxXTot                               = hlpU/2;
    int minXTot                               = hlpU/2;
    int maxYTot                               = hlpV/2;
    int minYTot                               = hlpV/2;
    int maxZTot                               = hlpW/2;
    int minZTot                               = hlpW/2;
    
    for ( int uIt = 0; uIt < hlpU; uIt++ )
    {
        for ( int vIt = 0; vIt < hlpV; vIt++ )
        {
            for ( int wIt = 0; wIt < hlpW; wIt++ )
            {
                coordPos                      = wIt + hlpW * ( vIt  + hlpV * uIt );
                
                if ( this->_densityMapCor[coordPos] > 0.0 )
                {
                    maxX                      = std::max ( maxX, uIt );
                    minX                      = std::min ( minX, uIt );
                    maxY                      = std::max ( maxY, vIt );
                    minY                      = std::min ( minY, vIt );
                    maxZ                      = std::max ( maxZ, wIt );
                    minZ                      = std::min ( minZ, wIt );
                }
                maxXTot                       = std::max ( maxXTot, uIt );
                minXTot                       = std::min ( minXTot, uIt );
                maxYTot                       = std::max ( maxYTot, vIt );
                minYTot                       = std::min ( minYTot, vIt );
                maxZTot                       = std::max ( maxZTot, wIt );
                minZTot                       = std::min ( minZTot, wIt );
            }
        }
    }
    
    //======================================== Calculate the grid for only the shape
    int newUStart                             = static_cast<int> ( this->_xFrom + ( minXTot + minX ) );
    int newUEnd                               = static_cast<int> ( this->_xTo - ( maxXTot - maxX ) );
    int newVStart                             = static_cast<int> ( this->_yFrom + ( minYTot + minY ) );
    int newVEnd                               = static_cast<int> ( this->_yTo - ( maxYTot - maxY ) );
    int newWStart                             = static_cast<int> ( this->_zFrom + ( minZTot + minZ ) );
    int newWEnd                               = static_cast<int> ( this->_zTo - ( maxZTot - maxZ ) );
    
    //======================================== Add the extra space
    if ( this->_maxExtraCellularSpace > 0.0 )
    {
        int extraPTs                          = static_cast<int> ( std::ceil ( this->_maxExtraCellularSpace / ( static_cast<double> ( this->_maxMapRange ) / static_cast<double> ( std::max ( hlpU, std::max ( hlpV, hlpW ) ) ) ) ) );
        
        if ( (newUStart - extraPTs) > static_cast<int> ( this->_xFrom ) ) { newUStart -= static_cast<int> ( extraPTs ); }
        if ( (newVStart - extraPTs) > static_cast<int> ( this->_yFrom ) ) { newVStart -= static_cast<int> ( extraPTs ); }
        if ( (newWStart - extraPTs) > static_cast<int> ( this->_zFrom ) ) { newWStart -= static_cast<int> ( extraPTs ); }
        if ( (newUEnd   + extraPTs) < static_cast<int> ( this->_xTo ) )   { newUEnd   += static_cast<int> ( extraPTs ); }
        if ( (newVEnd   + extraPTs) < static_cast<int> ( this->_yTo ) )   { newVEnd   += static_cast<int> ( extraPTs ); }
        if ( (newWEnd   + extraPTs) < static_cast<int> ( this->_zTo ) )   { newWEnd   += static_cast<int> ( extraPTs ); }
    }
    
    //======================================== Free unnecessary memory
    if ( this->_densityMapCor != nullptr )
    {
        delete[] this->_densityMapCor;
        this->_densityMapCor                  = nullptr;
    }
    
    //======================================== Move map
    int newXDim                               = newUEnd - newUStart + 1;
    int newYDim                               = newVEnd - newVStart + 1;
    int newZDim                               = newWEnd - newWStart + 1;

    if ( ( this->_xFrom < 0 ) && ( ( this->_xFrom + newUStart ) >= 0 ) ) { newXDim += 1; }
    if ( ( this->_yFrom < 0 ) && ( ( this->_yFrom + newVStart ) >= 0 ) ) { newYDim += 1; }
    if ( ( this->_zFrom < 0 ) && ( ( this->_zFrom + newWStart ) >= 0 ) ) { newZDim += 1; }
    
    this->_densityMapCor                      = new double [newXDim * newYDim * newZDim];
    
    int arrPos, hlpPos;
    int newU, newV, newW;
    for ( int uIt = newUStart; uIt <= newUEnd; uIt++ )
    {
        for ( int vIt = newVStart; vIt <= newVEnd; vIt++ )
        {
            for ( int wIt = newWStart; wIt <= newWEnd; wIt++ )
            {
                newU                          = uIt - newUStart;
                newV                          = vIt - newVStart;
                newW                          = wIt - newWStart;
                hlpPos                        = newW + newZDim * ( newV + newYDim * newU );

                newU                          = uIt - this->_xFrom;
                newV                          = vIt - this->_yFrom;
                newW                          = wIt - this->_zFrom;
                arrPos                        = newW + (this->_maxMapW + 1) * ( newV + (this->_maxMapV + 1) * newU );
                
                this->_densityMapCor[hlpPos]  = tmpMp[arrPos];
            }
        }
    }
    
    if ( verbose > 0 )
    {
        std::cout << "Map re-boxed." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Map re-boxed." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Clear memory
    delete[] tmpMp;
    
    //======================================== Change the settings
    this->_xFrom                              = newUStart;
    this->_yFrom                              = newVStart;
    this->_zFrom                              = newWStart;
    
    this->_xTo                                = newUEnd;
    this->_yTo                                = newVEnd;
    this->_zTo                                = newWEnd;

    this->_xSamplingRate                      = static_cast<double> ( this->_xRange ) / static_cast<double> ( hlpU );
    this->_ySamplingRate                      = static_cast<double> ( this->_yRange ) / static_cast<double> ( hlpV );
    this->_zSamplingRate                      = static_cast<double> ( this->_zRange ) / static_cast<double> ( hlpW );
    
    this->_xRange                             = this->_xSamplingRate * ( newXDim );
    this->_yRange                             = this->_ySamplingRate * ( newYDim );
    this->_zRange                             = this->_zSamplingRate * ( newZDim );
    
    this->_maxMapU                            = newXDim - 1;
    this->_maxMapV                            = newYDim - 1;
    this->_maxMapW                            = newZDim - 1;
    
    this->_xSamplingRate                      = this->_xRange / static_cast<double> ( this->_maxMapU + 1 );
    this->_ySamplingRate                      = this->_yRange / static_cast<double> ( this->_maxMapV + 1 );
    this->_zSamplingRate                      = this->_zRange / static_cast<double> ( this->_maxMapW + 1 );
    
    //======================================== Free unnecessary memory
    if ( this->_densityMapMap != nullptr )
    {
        delete this->_densityMapMap;
        this->_densityMapMap                  = nullptr;
    }
    
    //======================================== Report the new structure header
    if ( settings->htmlReport )
    {
        rvapi_add_section                     ( "NewHeaderSection",
                                                "Re-boxed structure header",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                false );
        settings->htmlReportLine             += 1;
        
        //==================================== Print max U, V and W
        std::stringstream hlpSS;
        hlpSS << "<pre><b>" << "Rows, Columns and Sections sizes: " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        int prec                              = 6;
        int UVWMAX                            = std::max ( ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( this->_maxMapU+1 ), prec ).length(),
                                                std::max ( ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( this->_maxMapV+1 ), prec ).length(),
                                                           ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( this->_maxMapW+1 ), prec ).length() ) );
        int FMAX                              = std::max ( ProSHADE_internal_misc::to_string_with_precision ( this->_xFrom, prec ).length(),
                                                std::max ( ProSHADE_internal_misc::to_string_with_precision ( this->_yFrom, prec ).length(),
                                                           ProSHADE_internal_misc::to_string_with_precision ( this->_zFrom, prec ).length() ) );
        int TMAX                              = std::max ( ProSHADE_internal_misc::to_string_with_precision ( this->_xTo, prec ).length(),
                                                std::max ( ProSHADE_internal_misc::to_string_with_precision ( this->_yTo, prec ).length(),
                                                           ProSHADE_internal_misc::to_string_with_precision ( this->_zTo, prec ).length() ) );
        int CDMAX                             = std::max ( ProSHADE_internal_misc::to_string_with_precision ( this->_xRange, prec ).length(),
                                                std::max ( ProSHADE_internal_misc::to_string_with_precision ( this->_yRange, prec ).length(),
                                                           ProSHADE_internal_misc::to_string_with_precision ( this->_zRange, prec ).length() ) );
        int CAMAX                             = std::max ( ProSHADE_internal_misc::to_string_with_precision ( 90.000, prec ).length(),
                                                std::max ( ProSHADE_internal_misc::to_string_with_precision ( 90.000, prec ).length(),
                                                           ProSHADE_internal_misc::to_string_with_precision ( 90.000, prec ).length() ) );
        int spacer                            = std::max ( UVWMAX, std::max ( FMAX, std::max ( TMAX, std::max ( CDMAX, CAMAX ) ) ) );
        if ( spacer < 5 ) { spacer            = 5; }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << static_cast<int> ( this->_maxMapU+1 );
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( this->_maxMapU+1 ), prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << static_cast<int> ( this->_maxMapV+1 );
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( this->_maxMapV+1 ), prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << static_cast<int> ( this->_maxMapW+1 );
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( this->_maxMapW+1 ), prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "NewHeaderSection",
                                                0,
                                                0,
                                                1,
                                                1 );
        
        //==================================== Print from and to lims
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Start and stop points on columns, rows, sections: " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << this->_xFrom;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( this->_xFrom, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << this->_xTo;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( this->_xTo, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << this->_yFrom;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( this->_yFrom, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << this->_yTo;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( this->_yTo, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << " " << this->_zFrom;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( this->_zFrom, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << this->_zTo;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( this->_zTo, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "NewHeaderSection",
                                                1,
                                                0,
                                                1,
                                                1 );
        
        //==================================== Print cell dimensions
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Cell dimensions (Angstrom): " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << this->_xRange;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( this->_xRange, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << this->_yRange;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( this->_yRange, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << this->_zRange;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( this->_zRange, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "NewHeaderSection",
                                                2,
                                                0,
                                                1,
                                                1 );
        
        //==================================== Print cell dimensions
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Cell angles (Degrees): " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << 90.000;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( 90.000, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << 90.000;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( 90.000, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << 90.000;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( 90.000, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "NewHeaderSection",
                                                3,
                                                0,
                                                1,
                                                1 );
        
        //==================================== Print cell dimensions
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Fast, medium, slow axes: " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        if      ( XYZOrder[0] == 0 ) { hlpSS << "  X"; }
        else if ( XYZOrder[0] == 1 ) { hlpSS << "  Y"; }
        else                         { hlpSS << "  Z"; }
        for ( int iter = 0; iter < static_cast<int> ( spacer - 1 ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        if      ( XYZOrder[1] == 0 ) { hlpSS << "X"; }
        else if ( XYZOrder[1] == 1 ) { hlpSS << "Y"; }
        else                         { hlpSS << "Z"; }
        for ( int iter = 0; iter < static_cast<int> ( spacer - 1 ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        if      ( XYZOrder[2] == 0 ) { hlpSS << "X"; }
        else if ( XYZOrder[2] == 1 ) { hlpSS << "Y"; }
        else                         { hlpSS << "Z"; }
        for ( int iter = 0; iter < static_cast<int> ( spacer - 1 ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "NewHeaderSection",
                                                4,
                                                0,
                                                1,
                                                1 );
        
        rvapi_flush                           ( );
    }
    
    //======================================== Set return values
    ret[3]                                    = this->_maxMapU + 1;
    ret[4]                                    = this->_maxMapV + 1;
    ret[5]                                    = this->_maxMapW + 1;

    //======================================== Done
    return ( ret );
}

/*! \brief This function reads in the CCP4' MAP formatted file and computes its features.
 
 This function reads in the CCP4's MAP format density map and checks if it is cubic. If so (non-cubic maps are not
 supported yet) it proceeds to check that the density is centered in the center of the cell and corrects if not. Then,
 it does the density normalisation, masking and island detection and removal. During this, it computes the statistical
 features of the density both as a whole and the masked fraction only. You cannot set any parameters to this function
 regarding how the map should be processed - it will be processed by the defaults (however, note that there are actually
 only a little values that apply - for example the resolution is irrelevant to this function as it does not submit the map
 for any further processing) and centered on COM. If this is not what you want, us the symmetry or distances alternatives.
 
 \param[in] fileName The file name (and path) to the PDB file to be read in.
 \param[in] minDensPreNorm A pointer to variable storing the whole map minimum density value before normalisation.
 \param[in] maxDensPreNorm A pointer to variable storing the whole map maximum density value before normalisation.
 \param[in] minDensPostNorm A pointer to variable storing the whole map minimum density value after normalisation.
 \param[in] maxDensPostNorm A pointer to variable storing the whole map maximum density value after normalisation.
 \param[in] postNormMean A pointer to variable storing the whole map mean density value after normalisation.
 \param[in] postNormSdev A pointer to variable storing the whole map standard deviation of density values after normalisation.
 \param[in] maskVolume A pointer to variable storing the volume of masked density in A^3.
 \param[in] totalVolume A pointer to variable storing the total volume of the cell in A^3.
 \param[in] maskMean A pointer to variable storing the masked part of the map mean density value after normalisation.
 \param[in] maskSdev A pointer to variable storing the masked part of the map standard deviation density value after normalisation.
 \param[in] maskMin A pointer to variable storing the masked part of the map minimum density value after normalisation.
 \param[in] maskMax A pointer to variable storing the masked part of the map maximum density value after normalisation.
 \param[in] maskDensityRMS A pointer to variable storing the masked part of the map RMS value after normalisation.
 \param[in] allDensityRMS A pointer to variable storing the whole map RMS value after normalisation.
 \param[in] maxMapIQR A double value to specify the number of interquartile ranges from the median to be used to threshold map masking.
 \param[in] useCubicMaps This parameter determines if the clear map compued should be rectangular or cubic (older versions of refmac may require this).
 \param[in] maskBlurFactor How much should the map be blurred to compute the mask?
 \param[in] maskBlurFactorGiven Was the blurring factor value supplied by the user, or are we working with default?
 \param[in] reboxAtAll Should the re-boxing be done for the output structure, or not?
 \param[in] settings The settings contained required for writing into the HTML report, if required.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_data::getDensityMapFromMAPFeatures ( std::string fileName,
                                                                      double *minDensPreNorm,
                                                                      double *maxDensPreNorm,
                                                                      double *minDensPostNorm,
                                                                      double *maxDensPostNorm,
                                                                      double *postNormMean,
                                                                      double *postNormSdev,
                                                                      double *maskVolume,
                                                                      double* totalVolume,
                                                                      double *maskMean,
                                                                      double *maskSdev,
                                                                      double *maskMin,
                                                                      double *maskMax,
                                                                      double *maskDensityRMS,
                                                                      double *allDensityRMS,
                                                                      std::array<double,3> *origRanges,
                                                                      std::array<double,3> *origDims,
                                                                      double maxMapIQR,
                                                                      double extraCS,
                                                                      int verbose,
                                                                      bool useCubicMaps,
                                                                      double maskBlurFactor,
                                                                      bool maskBlurFactorGiven,
                                                                      bool reboxAtAll,
                                                                      ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Initialise internal values
    this->_inputFileName                      = fileName;
    
    //======================================== Initialise local variables
    CMap_io::CMMFile *mapFile                 = nullptr;
    float *cell                               = nullptr;
    int *dim                                  = nullptr;
    int *order                                = nullptr;
    int *orig                                 = nullptr;
    int myMapMode                             = 0;
    double cornerAvg                          = 0.0;
    double centralAvg                         = 0.0;
    double cornerCount                        = 0.0;
    double centralCount                       = 0.0;
    
    //======================================== Allocate memory
    cell                                      = (float*) malloc ( 6 * sizeof ( float ) );
    dim                                       = (int*  ) malloc ( 3 * sizeof ( int   ) );
    order                                     = (int*  ) malloc ( 3 * sizeof ( int   ) );
    orig                                      = (int*  ) malloc ( 3 * sizeof ( int   ) );
    
    if ( ( cell == nullptr ) || ( dim == nullptr ) || ( order == nullptr ) || ( orig == nullptr ) )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory. Terminating... " << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Cannot allocate memory for map data. Could you have run out of memory?" << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Read in the MAP file info
    mapFile                                   = reinterpret_cast<CMap_io::CMMFile*> ( CMap_io::ccp4_cmap_open ( fileName.c_str() , myMapMode ) );
    if ( mapFile == nullptr ) 
    {
        std::cerr << "!!! ProSHADE ERROR !!! Failed to open the file " << fileName << ". Check for typos and curruption of the file. Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Cannot open file " << fileName << ".</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    CMap_io::ccp4_cmap_get_cell               ( mapFile, cell  );
    CMap_io::ccp4_cmap_get_dim                ( mapFile, dim  );
    CMap_io::ccp4_cmap_get_order              ( mapFile, order );
    CMap_io::ccp4_cmap_get_origin             ( mapFile, orig );

    
    if ( verbose > 2 )
    {
        std::cout << ">> Map loaded." << std::endl;
    }
    
    //======================================== If CUBIC
    if ( ( ( cell[3] < (90.0 + 0.1) ) && ( cell[3] > (90.0 - 0.1) ) ) &&
         ( ( cell[4] < (90.0 + 0.1) ) && ( cell[4] > (90.0 - 0.1) ) ) &&
         ( ( cell[5] < (90.0 + 0.1) ) && ( cell[5] > (90.0 - 0.1) ) ) )
    {
        //==================================== Determine/Save dimmensions
        this->_xRange                         = cell[0];
        this->_yRange                         = cell[1];
        this->_zRange                         = cell[2];
        
        //==================================== Maximal cell dimensions
        this->_maxMapRange                    = std::max ( this->_xRange, std::max( this->_yRange, this->_zRange ) );
        
        //==================================== Find max U, V and W
        if ( order[0] == 1 ) { this->_maxMapU = dim[0] - 1; this->_xFrom = orig[0]; }
        if ( order[0] == 2 ) { this->_maxMapU = dim[1] - 1; this->_yFrom = orig[0]; }
        if ( order[0] == 3 ) { this->_maxMapU = dim[2] - 1; this->_zFrom = orig[0]; }
        
        if ( order[1] == 1 ) { this->_maxMapV = dim[0] - 1; this->_xFrom = orig[1]; }
        if ( order[1] == 2 ) { this->_maxMapV = dim[1] - 1; this->_yFrom = orig[1]; }
        if ( order[1] == 3 ) { this->_maxMapV = dim[2] - 1; this->_zFrom = orig[1]; }
        
        if ( order[2] == 1 ) { this->_maxMapW = dim[0] - 1; this->_xFrom = orig[2]; }
        if ( order[2] == 2 ) { this->_maxMapW = dim[1] - 1; this->_yFrom = orig[2]; }
        if ( order[2] == 3 ) { this->_maxMapW = dim[2] - 1; this->_zFrom = orig[2]; }
        
        //==================================== Get the map start and end
        this->_xTo                            = this->_xFrom + this->_maxMapU;
        this->_yTo                            = this->_yFrom + this->_maxMapV;
        this->_zTo                            = this->_zFrom + this->_maxMapW;
        
        //==================================== Allocate map data memory
        this->_densityMapMap                  = (float*) malloc ( (dim[0]*dim[1]*dim[2]) * sizeof ( float ) );
        if ( this->_densityMapMap == nullptr )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory for map data. Terminating... " << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "Cannot allocate memory for map data. Could you have run out of memory?" << "</font>";
                rvapi_set_text                    ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                       ( );
            }
            
            exit ( -1 );
        }
        
        //==================================== Check if the mode is compatible
        int mapMode                           = CMap_io::ccp4_cmap_get_datamode ( mapFile );
        if ( ( mapMode != 0 ) && ( mapMode != 2 ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Cannot read from the map file. The map file mode is not supported. Terminating..." << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "Cannot read from the map file. The map mode is neither 0, nor 2. Other map modes are currently not supported." << "</font>";
                rvapi_set_text                    ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                       ( );
            }
            
            exit ( -1 );
        }
        
        //==================================== Read in map data
        // ... Find the stop positions (start is in orig variables) and the axis order
        int maxLim[3];
        int XYZOrder[3];
        int newU, newV, newW;
        int arrPos;
        
        for ( int iter = 0; iter < 3; iter++ )
        {
            maxLim[iter]                      = orig[iter] + dim[iter] - 1;
            XYZOrder[order[iter]-1]           = iter;
        }
        
        // ... Solve the dimensions and sizes
        int fastDimSize                       = ( maxLim[0] - orig[0] + 1 );
        int midDimSize                        = ( maxLim[1] - orig[1] + 1 ) * fastDimSize;
        std::vector<float> section( midDimSize );
        int index;
        int iters[3];
        
        // ... Read in the map data
        for ( iters[2] = orig[2]; iters[2] <= maxLim[2]; iters[2]++ )
        {
            index                             = 0;
            CMap_io::ccp4_cmap_read_section( mapFile, &section[0] );
            
            if ( mapMode == 0 )
            {
                for ( int iter = ( midDimSize - 1 ); iter >= 0; iter-- )
                {
                    section[iter]             = static_cast<float> ( ( reinterpret_cast<unsigned char*> (&section[0]) )[iter] );
                }
            }
            
            for ( iters[1] = orig[1]; iters[1] <= maxLim[1]; iters[1]++ )
            {
                for ( iters[0] = orig[0]; iters[0] <= maxLim[0]; iters[0]++ )
                {
                    newU                      = iters[XYZOrder[0]] - orig[XYZOrder[0]];
                    newV                      = iters[XYZOrder[1]] - orig[XYZOrder[1]];
                    newW                      = iters[XYZOrder[2]] - orig[XYZOrder[2]];
                    arrPos                    = newW + (this->_maxMapW + 1) * ( newV + (this->_maxMapV + 1) * newU );
                    this->_densityMapMap[arrPos] = static_cast<float> ( section[ index++ ] );
                }
            }
        }

        //==================================== Get values for normalisation
        std::vector<double> vals;
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) ); iter++ )
        {
            if ( this->_densityMapMap[iter] != 0.0 )
            {
                vals.emplace_back             ( this->_densityMapMap[iter] );
            }
        }
        std::sort                             ( vals.begin(), vals.end() );
        
        *minDensPreNorm                       = vals.at(0);
        *maxDensPreNorm                       = vals.at(vals.size()-1);
        
        //==================================== Create results section
        if ( settings->htmlReport )
        {
            //==================================== Create section
            rvapi_add_section                 ( "ResultsSection",
                                                "Results",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                true );
            settings->htmlReportLine         += 1;
            
            std::stringstream hlpSS;
            hlpSS << "<pre>" << "Min / Max density pre normalisation: ";
            int hlpIt                         = static_cast<int> ( 70 - hlpSS.str().length() );
            for ( int iter = 0; iter < hlpIt; iter++ )
            {
                hlpSS << ".";
            }
            
            std::stringstream hlpSS2;
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( *minDensPreNorm * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
            hlpSS << " " << hlpSS2.str() << " / ";
            
            hlpSS2.str( std::string ( ) );
            hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( *maxDensPreNorm * 1000.0 ) / 1000.0;
            if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
            if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
            hlpSS << hlpSS2.str() << "</pre>";
            
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                0,
                                                1,
                                                1,
                                                1 );
            
            rvapi_flush                       ( );
        }
        
        //==================================== Find mean and standard deviation for later normalisation
        this->_mapMean                        = std::accumulate ( vals.begin(), vals.end(), 0.0 ) / static_cast<double> ( vals.size() );
        this->_mapSdev                        = std::sqrt ( static_cast<double> ( std::inner_product ( vals.begin(), vals.end(), vals.begin(), 0.0 ) ) / static_cast<double> ( vals.size() ) - this->_mapMean * this->_mapMean );
        vals.clear                            ( );
        
        //==================================== Check if the map is not absolutely de-centered
        int noPToCheckU                       = static_cast<unsigned int> ( this->_maxMapU / 8.0 );
        int noPToCheckV                       = static_cast<unsigned int> ( this->_maxMapV / 8.0 );
        int noPToCheckW                       = static_cast<unsigned int> ( this->_maxMapW / 8.0 );
        
        for ( int uIt = 0; uIt < static_cast<int> (this->_maxMapU+1); uIt++ )
        {
            for ( int vIt = 0; vIt < static_cast<int> (this->_maxMapV+1); vIt++ )
            {
                for ( int wIt = 0; wIt < static_cast<int> (this->_maxMapW+1); wIt++ )
                {
                    //======================== Initialisation
                    arrPos                    = wIt + (this->_maxMapW + 1) * ( vIt + (this->_maxMapV + 1) * uIt );
                    
                    //======================== Check to which quadrant the value belongs to
                    if ( ( ( uIt <= static_cast<int> ( this->_maxMapU/2 + noPToCheckU ) ) && ( uIt >= static_cast<int> ( this->_maxMapU/2 - noPToCheckU ) ) ) &&
                         ( ( vIt <= static_cast<int> ( this->_maxMapV/2 + noPToCheckV ) ) && ( vIt >= static_cast<int> ( this->_maxMapV/2 - noPToCheckV ) ) ) &&
                         ( ( wIt <= static_cast<int> ( this->_maxMapW/2 + noPToCheckW ) ) && ( wIt >= static_cast<int> ( this->_maxMapW/2 - noPToCheckW ) ) ) )
                    {
                        centralAvg           += std::max ( this->_densityMapMap[arrPos], static_cast<float>(0.0) );
                        centralCount         += 1.0;
                    }
                    if ( ( ( uIt < noPToCheckU )                  && ( vIt < noPToCheckV                  ) && ( wIt < noPToCheckW                  ) ) ||
                         ( ( uIt < noPToCheckU )                  && ( vIt < noPToCheckV                  ) && ( wIt > static_cast<int> (this->_maxMapW-noPToCheckW) ) ) ||
                         ( ( uIt < noPToCheckU )                  && ( vIt > static_cast<int> (this->_maxMapV-noPToCheckV) ) && ( wIt < noPToCheckW                  ) ) ||
                         ( ( uIt < noPToCheckU )                  && ( vIt > static_cast<int> (this->_maxMapV-noPToCheckV) ) && ( wIt > static_cast<int> (this->_maxMapW-noPToCheckW) ) ) ||
                         ( ( uIt > static_cast<int> (this->_maxMapU-noPToCheckU) ) && ( vIt < noPToCheckV                  ) && ( wIt < noPToCheckW                  ) ) ||
                         ( ( uIt > static_cast<int> (this->_maxMapU-noPToCheckU) ) && ( vIt < noPToCheckV                  ) && ( wIt > static_cast<int> (this->_maxMapW-noPToCheckW) ) ) ||
                         ( ( uIt > static_cast<int> (this->_maxMapU-noPToCheckU) ) && ( vIt > static_cast<int> (this->_maxMapV-noPToCheckV) ) && ( wIt < noPToCheckW                  ) ) ||
                         ( ( uIt > static_cast<int> (this->_maxMapU-noPToCheckU) ) && ( vIt > static_cast<int> (this->_maxMapV-noPToCheckV) ) && ( wIt > static_cast<int> (this->_maxMapW-noPToCheckW) ) ) )
                    {
                        cornerAvg            += std::max ( this->_densityMapMap[arrPos], static_cast<float>(0.0) );
                        cornerCount          += 1.0;
                    }
                }
            }
        }
        
        cornerAvg                            /= cornerCount;
        centralAvg                           /= centralCount;
        
        //==================================== If density is in the corners
        if ( cornerAvg > centralAvg )
        {
            //================================ Initialise required variables
            float* hlpMap                     = nullptr;
            hlpMap                            = (float*) malloc ( (dim[0]*dim[1]*dim[2]) * sizeof ( float ) );
            if ( hlpMap == nullptr )
            {
                std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory for map data. Terminating... " << std::endl;
                
                if ( settings->htmlReport )
                {
                    std::stringstream hlpSS;
                    hlpSS << "<font color=\"red\">" << "Cannot allocate memory for map data. Could you have run out of memory?" << "</font>";
                    rvapi_set_text                    ( hlpSS.str().c_str(),
                                                       "ProgressSection",
                                                       settings->htmlReportLineProgress,
                                                       1,
                                                       1,
                                                       1 );
                    settings->htmlReportLineProgress += 1;
                    rvapi_flush                       ( );
                }
                
                exit ( -1 );
            }
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( dim[0] * dim[1] * dim[2] ); iter++ )
            {
                hlpMap[iter]                  = this->_densityMapMap[iter];
            }
            
            //================================ Transform
            unsigned int hlpPos;
            for ( int uIt = 0; uIt < static_cast<int> (this->_maxMapU+1); uIt++ )
            {
                for ( int vIt = 0; vIt < static_cast<int> (this->_maxMapV+1); vIt++ )
                {
                    for ( int wIt = 0; wIt < static_cast<int> (this->_maxMapW+1); wIt++ )
                    {
                        //==================== If in lower half, add half; if in upper halp, subtract half
                        if ( uIt < static_cast<int> (this->_maxMapU+1)/2 ) { newU = uIt + (this->_maxMapU+1)/2; } else { newU = uIt - (this->_maxMapU+1)/2; }
                        if ( vIt < static_cast<int> (this->_maxMapV+1)/2 ) { newV = vIt + (this->_maxMapV+1)/2; } else { newV = vIt - (this->_maxMapV+1)/2; }
                        if ( wIt < static_cast<int> (this->_maxMapW+1)/2 ) { newW = wIt + (this->_maxMapW+1)/2; } else { newW = wIt - (this->_maxMapW+1)/2; }
                        
                        arrPos                = newW + (this->_maxMapW + 1) * ( newV + (this->_maxMapV + 1) * newU );
                        hlpPos                = wIt  + (this->_maxMapW + 1) * ( vIt  + (this->_maxMapV + 1) * uIt  );
                        
                        //==================== Set the value into correct coords in the new map
                        this->_densityMapMap[arrPos] = hlpMap[hlpPos];
                    }
                }
            }
            
            //================================ Free memory
            free                              ( hlpMap );
        }
        
        if ( verbose > 3 )
        {
            std::cout << ">>>>> Density moved from corners to center, if applicable." << std::endl;
        }
    }
    else
    {
        std::cerr << "!!! ProSHADE ERROR !!! Non-orthogonal maps are not yet supported." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Detected non-orthogonal map. Currently, only the P1 maps are supported." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Structure " << fileName << " read." << "</font>";
        rvapi_set_text                    ( hlpSS.str().c_str(),
                                           "ProgressSection",
                                           settings->htmlReportLineProgress,
                                           1,
                                           1,
                                           1 );
        settings->htmlReportLineProgress += 1;
        rvapi_flush                       ( );
    }
    
    //======================================== Free memory
    if ( cell != nullptr )
    {
        free                                  ( cell );
        cell                                  = nullptr;
    }
    if ( dim != nullptr )
    {
        free                                  ( dim );
        dim                                  = nullptr;
    }
    if ( order != nullptr )
    {
        free                                  ( order );
        order                                 = nullptr;
    }
    if ( orig != nullptr )
    {
        free                                  ( orig );
        orig                                  = nullptr;
    }
    
    //======================================== Close the file
    CMap_io::ccp4_cmap_close                  ( mapFile );

    //======================================== Determine sampling ranges
    this->_xSamplingRate                      = this->_xRange / static_cast<double> ( this->_maxMapU + 1 );
    this->_ySamplingRate                      = this->_yRange / static_cast<double> ( this->_maxMapV + 1 );
    this->_zSamplingRate                      = this->_zRange / static_cast<double> ( this->_maxMapW + 1 );
    
    bool addXOne                              = false;
    bool addYOne                              = false;
    bool addZOne                              = false;
    
    //======================================== Save ranges and dims
    (*origRanges)[0]                          = this->_xRange;
    (*origRanges)[1]                          = this->_yRange;
    (*origRanges)[2]                          = this->_zRange;
    
    (*origDims)[0]                            = this->_maxMapU;
    (*origDims)[1]                            = this->_maxMapV;
    (*origDims)[2]                            = this->_maxMapW;
    
    //======================================== If extra cell space is given, apply
    this->_maxExtraCellularSpace              = extraCS;
    if ( this->_maxExtraCellularSpace != 0.0 )
    {
        //==================================== Compute new stats
        int xDiff                             = static_cast<int>   ( std::ceil ( ( this->_xRange + this->_maxExtraCellularSpace ) / this->_xSamplingRate ) - this->_maxMapU );
        int yDiff                             = static_cast<int>   ( std::ceil ( ( this->_yRange + this->_maxExtraCellularSpace ) / this->_ySamplingRate ) - this->_maxMapV );
        int zDiff                             = static_cast<int>   ( std::ceil ( ( this->_zRange + this->_maxExtraCellularSpace ) / this->_zSamplingRate ) - this->_maxMapW );
        
        if ( ( this->_xFrom >= 0 ) && ( ( this->_xFrom - xDiff ) < 0 ) ) { addXOne = true; }
        if ( ( this->_yFrom >= 0 ) && ( ( this->_yFrom - yDiff ) < 0 ) ) { addYOne = true; }
        if ( ( this->_zFrom >= 0 ) && ( ( this->_zFrom - zDiff ) < 0 ) ) { addZOne = true; }
        
        this->_xRange                        += xDiff * this->_xSamplingRate;
        this->_yRange                        += yDiff * this->_ySamplingRate;
        this->_zRange                        += zDiff * this->_zSamplingRate;
        
        if ( xDiff % 2 != 0 )
        {
            this->_xRange                    += this->_xSamplingRate;
            xDiff                             = static_cast<int>   ( std::ceil ( this->_xRange / this->_xSamplingRate ) - ( this->_maxMapU + 1 ) );
        }
        if ( yDiff % 2 != 0 )
        {
            this->_yRange                    += this->_ySamplingRate;
            yDiff                             = static_cast<int>   ( std::ceil ( this->_yRange / this->_ySamplingRate ) - ( this->_maxMapV + 1 ) );
        }
        if ( zDiff % 2 != 0 )
        {
            this->_zRange                    += this->_zSamplingRate;
            zDiff                             = static_cast<int>   ( std::ceil ( this->_zRange / this->_zSamplingRate ) - ( this->_maxMapW + 1 ) );
        }
        
        xDiff                                /= 2;
        yDiff                                /= 2;
        zDiff                                /= 2;
        
        this->_xFrom                         -= xDiff;
        this->_yFrom                         -= yDiff;
        this->_zFrom                         -= zDiff;
        
        this->_xTo                           += xDiff;
        this->_yTo                           += yDiff;
        this->_zTo                           += zDiff;
        
        this->_maxMapU                        = this->_xTo - this->_xFrom;
        this->_maxMapV                        = this->_yTo - this->_yFrom;
        this->_maxMapW                        = this->_zTo - this->_zFrom;
        
        this->_xSamplingRate                  = this->_xRange / static_cast<double> ( this->_maxMapU + 1 );
        this->_ySamplingRate                  = this->_yRange / static_cast<double> ( this->_maxMapV + 1 );
        this->_zSamplingRate                  = this->_zRange / static_cast<double> ( this->_maxMapW + 1 );
        
        //==================================== Move the map
        float *hlpMap                         = nullptr;
        hlpMap                                = (float*) malloc ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) * sizeof ( float ) );
        if ( hlpMap == nullptr )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory for map moving data. Terminating... " << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "Cannot allocate memory for map data. Could you have run out of memory?" << "</font>";
                rvapi_set_text                    ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                       ( );
            }
            
            exit ( -1 );
        }
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) ); iter++ )
        {
            hlpMap[iter]                      = 0.0;
        }
        
        unsigned int newU, newV, newW, hlpPos, arrPos;
        for ( int uIt = 0; uIt < static_cast<int> (this->_maxMapU+1); uIt++ )
        {
            for ( int vIt = 0; vIt < static_cast<int> (this->_maxMapV+1); vIt++ )
            {
                for ( int wIt = 0; wIt < static_cast<int> (this->_maxMapW+1); wIt++ )
                {
                    if ( ( uIt < xDiff ) || ( uIt > static_cast<int> ( this->_maxMapU - xDiff ) ) ||
                         ( vIt < yDiff ) || ( vIt > static_cast<int> ( this->_maxMapV - yDiff ) ) ||
                         ( wIt < zDiff ) || ( wIt > static_cast<int> ( this->_maxMapW - zDiff ) ) )
                    {
                        hlpPos                = wIt  + (this->_maxMapW + 1) * ( vIt  + (this->_maxMapV + 1) * uIt  );
                        hlpMap[hlpPos]        = 0.0;
                    }
                    else
                    {
                        newU                  = (uIt - xDiff);
                        newV                  = (vIt - yDiff);
                        newW                  = (wIt - zDiff);
                        
                        hlpPos                = wIt  + (this->_maxMapW + 1) * ( vIt  + (this->_maxMapV + 1) * uIt  );
                        arrPos                = newW + (this->_maxMapW + 1 - zDiff * 2) * ( newV + (this->_maxMapV + 1 - yDiff * 2) * newU );
                        
                        hlpMap[hlpPos]        = this->_densityMapMap[arrPos];
                    }
                }
            }
        }
        
        //==================================== Free memory
        if ( this->_densityMapMap != nullptr )
        {
            free                              ( this->_densityMapMap );
            this->_densityMapMap              = nullptr;
        }
        
        //==================================== Copy
        this->_densityMapMap                  = (float*) malloc ( ((this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1)) * sizeof ( float ) );
        if ( this->_densityMapMap == nullptr )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory for map data. Terminating... " << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "Cannot allocate memory for map data. Could you have run out of memory?" << "</font>";
                rvapi_set_text                    ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                       ( );
            }
            
            exit ( -1 );
        }
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) ); iter++ )
        {
            this->_densityMapMap[iter]        = hlpMap[iter];
        }
        
        //==================================== Free memory
        if ( hlpMap != nullptr )
        {
            free                              ( hlpMap );
            hlpMap                            = nullptr;
        }
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Extra cell space added as required." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
    }
    
    //======================================== Local hlp variables
    int hlpU                                  = ( this->_maxMapU + 1 );
    int hlpV                                  = ( this->_maxMapV + 1 );
    int hlpW                                  = ( this->_maxMapW + 1 );
    
    //======================================== Initialise local variables for Fourier and Inverse Fourier
    int uIt, vIt, wIt;
    unsigned int arrayPos                     = 0;
    
    this->_densityMapCor                      = new double      [hlpU * hlpV * hlpW];
    
    //======================================== Load map data for Fourier
    std::vector<double> vals;
    std::vector<double> valsWZ;
    
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (this->_maxMapU+1) * (this->_maxMapV+1) * (this->_maxMapW+1) ); iter++ )
    {
        if ( this->_densityMapMap[iter] == this->_densityMapMap[iter] )
        {
            //================================ Map value is NOT nan
            if ( this->_densityMapMap[iter] != 0.0 )
            {
                vals.emplace_back             ( ( this->_densityMapMap[iter] - this->_mapMean ) / this->_mapSdev );
            }
            valsWZ.emplace_back               ( ( this->_densityMapMap[iter] - this->_mapMean ) / this->_mapSdev );
            
            this->_densityMapCor[iter]        = ( this->_densityMapMap[iter] - this->_mapMean ) / this->_mapSdev;
        }
        else
        {
            //================================ Map value is nan - this sometimes occurs for a couple of values, I have no clue as to why...
            this->_densityMapCor[iter]        = 0.0;
        }
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>> Map normalised." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Map normalisation complete." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== If not required, do not continue
    if ( !reboxAtAll )
    {
        vals.clear                            ( );
        return;
    }
    
    //======================================== Get post normalisation stats
    std::sort                                 ( vals.begin(), vals.end() );
    
    *minDensPostNorm                          = vals.at(0);
    *maxDensPostNorm                          = vals.at(vals.size()-1);
    
    *postNormMean                             = std::accumulate ( vals.begin(), vals.end(), 0.0 ) / static_cast<double> ( vals.size() );
    *postNormSdev                             = std::sqrt ( static_cast<double> ( std::inner_product ( vals.begin(), vals.end(), vals.begin(), 0.0 ) ) / static_cast<double> ( vals.size() ) - *postNormMean * *postNormMean );
    
    *allDensityRMS                            = 0.0;
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( valsWZ.size() ); iter++ )
    {
        *allDensityRMS                       += pow ( valsWZ.at(iter), 2.0 );
    }
    *allDensityRMS                           /= static_cast<double> ( valsWZ.size() - 1 );
    *allDensityRMS                            = sqrt ( *allDensityRMS );
    
    vals.clear                                ( );
    valsWZ.clear                              ( );
    
    //======================================== Print more to the results section
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<pre>" << "Mean (std. dev.) pre normalisation: ";
        int hlpIt                             = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        std::stringstream hlpSS2;
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->_mapMean * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << " ( ";
        
        hlpSS2.str( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( this->_mapSdev * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS <<  hlpSS2.str() << " ) " << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                1,
                                                1,
                                                1,
                                                1 );
        
        hlpSS.str( std::string ( "<pre>" ) );
        for ( int iter = 0; iter < 70; iter++ )
        {
            hlpSS << "=";
        }
        hlpSS << "</pre>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ResultsSection",
                                               2,
                                               1,
                                               1,
                                               1 );
        
        hlpSS.str( std::string ( ) );
        hlpSS << "<pre>" << "Min / Max density post normalisation: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( *minDensPostNorm * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << " / ";
        
        hlpSS2.str( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( *maxDensPostNorm * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                3,
                                                1,
                                                1,
                                                1 );
        
        hlpSS.str( std::string ( ) );
        hlpSS << "<pre>" << "Mean (std. dev.) post normalisation: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( *postNormMean * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << " ( ";
        
        hlpSS2.str( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( *postNormSdev * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS <<  hlpSS2.str() << " ) " << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                4,
                                                1,
                                                1,
                                                1 );
        
        hlpSS.str( std::string ( "<pre>" ) );
        for ( int iter = 0; iter < 70; iter++ )
        {
            hlpSS << "=";
        }
        hlpSS << "</pre>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                5,
                                                1,
                                                1,
                                                1 );
        
        rvapi_flush                       ( );
    }
    
    //======================================== Map cleaning
    this->_fromPDB                            = false;
    if ( !this->_fromPDB )
    {
        //==================================== Save original map to restore for each cycle
        double *tmpMp                         = new double[hlpU * hlpV * hlpW];
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( hlpU * hlpV * hlpW ); iter++ )
        {
            tmpMp[iter]                       = this->_densityMapCor[iter];
        }
        
        bool notTooManyIslads                 = true;
        double blFr                           = 150.0;
        
        while ( notTooManyIslads )
        {
            //================================ Do not change user values
            blFr                             += 50.0;
            if ( maskBlurFactorGiven )
            {
                blFr                          = maskBlurFactor;
                this->maskMap                 ( hlpU, hlpV, hlpW, blFr, maxMapIQR, 3.0 );
                this->removeIslands           ( hlpU, hlpV, hlpW, verbose, true );
                break;
            }
            
            //================================ Compute mask and apply it
            this->maskMap                     ( hlpU, hlpV, hlpW, blFr, maxMapIQR, 3.0 );
            
            //================================ Detect islands
            if ( verbose > 3 )
            {
                std::cout << ">>>>>>>> Island detection started." << std::endl;
            }
            
            notTooManyIslads                  = this->removeIslands ( hlpU, hlpV, hlpW, verbose, false );
            
            if ( maskBlurFactorGiven )
            {
                break;
            }
            
            if ( verbose > 3 )
            {
                std::cout << ">>>>> Map masked with factor of " << blFr << " and small islands were then removed.";
            }
            if ( notTooManyIslads )
            {
                if ( verbose > 3 )
                {
                    std::cout << " However, too many islands remained. Trying higher blurring factor." << std::endl;
                }
                
                if ( blFr <= 500.0 )
                {
                    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( hlpU * hlpV * hlpW ); iter++ )
                    {
                        this->_densityMapCor[iter] = tmpMp[iter];
                    }
                }
            }
            else
            {
                if ( verbose > 3 )
                {
                    std::cout << std::endl;
                }
            }

            
            if ( blFr > 500.0 )
            {
                std::cout << "!!! ProSHADE WARNING !!! Even blurring factor of 500 did not remove islands. Will not proceed with what we have, but be warned that either the masking is bugged or yout map has high levels of noise. You can consider using the -y ( or --no_clear ) options to avoid this message and the extra time for testing different blurring factors for this map." << std::endl;
                
                if ( settings->htmlReport )
                {
                    std::stringstream hlpSS;
                    hlpSS << "<font color=\"orange\">" << "Even blurring factor of 500 did not remove islands. Will not proceed with what we have, but be warned that either the masking is bugged or yout map has high levels of noise. You can consider using the -y ( or --no_clear ) options to avoid this message and the extra time for testing different blurring factors for this map." << "</font>";
                    rvapi_set_text                    ( hlpSS.str().c_str(),
                                                       "ProgressSection",
                                                       settings->htmlReportLineProgress,
                                                       1,
                                                       1,
                                                       1 );
                    settings->htmlReportLineProgress += 1;
                    rvapi_flush                       ( );
                }
                
                break;
            }
        }
        delete[] tmpMp;
    }
    
    if ( verbose > 2 )
    {
        std::cout << ">> Map masked." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Map masking complete." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Free unnecessary memory
    if ( this->_densityMapMap != nullptr )
    {
        delete this->_densityMapMap;
        this->_densityMapMap                  = nullptr;
    }
    
    //======================================== Mask stats
    vals.clear                                ( );
    
    for ( uIt = 0; uIt < hlpU; uIt++ )
    {
        for ( vIt = 0; vIt < hlpV; vIt++ )
        {
            for ( wIt = 0; wIt < hlpW; wIt++ )
            {
                arrayPos                      = wIt  + hlpW * ( vIt  + hlpV * uIt  );
                if ( this->_densityMapCor[arrayPos] != 0.0 ) { vals.emplace_back ( this->_densityMapCor[arrayPos] ); }
            }
        }
    }
    
    *maskVolume                               = static_cast<double> ( vals.size() );
    *totalVolume                              = hlpU * hlpV * hlpW;
    
    std::sort                                 ( vals.begin(), vals.end() );
    
    *maskMin                                  = vals.at(0);
    *maskMax                                  = vals.at(vals.size()-1);
    
    //======================================== Find mean and standard deviation for later normalisation
    *maskMean                                 = std::accumulate ( vals.begin(), vals.end(), 0.0 ) / static_cast<double> ( vals.size() );
    *maskSdev                                 = std::sqrt ( static_cast<double> ( std::inner_product ( vals.begin(), vals.end(), vals.begin(), 0.0 ) ) / static_cast<double> ( vals.size() ) - *maskMean * *maskMean );
    
    *maskDensityRMS                           = 0.0;
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( vals.size() ); iter++ )
    {
        *maskDensityRMS                      += pow ( vals.at(iter), 2.0 );
    }
    *maskDensityRMS                          /= static_cast<double> ( vals.size() - 1 );
    *maskDensityRMS                           = sqrt ( *maskDensityRMS );
    
    vals.clear                                ( );
    
    //======================================== Print after masking results
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<pre>" << "Min / Max density after masking: ";
        int hlpIt                             = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        std::stringstream hlpSS2;
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( *maskMin * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << " / ";
        
        hlpSS2.str( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( *maskMax * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << hlpSS2.str() << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ResultsSection",
                                               6,
                                               1,
                                               1,
                                               1 );
        
        hlpSS.str( std::string ( ) );
        hlpSS << "<pre>" << "Mean (std. dev.) within mask: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }

        hlpSS2.str( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( *maskMean * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << " ( ";

        hlpSS2.str( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( *maskSdev * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS <<  hlpSS2.str() << " ) " << "</pre>";

        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ResultsSection",
                                               7,
                                               1,
                                               1,
                                               1 );
        
        hlpSS.str( std::string ( "<pre>" ) );
        for ( int iter = 0; iter < 70; iter++ )
        {
            hlpSS << "=";
        }
        hlpSS << "</pre>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                8,
                                                1,
                                                1,
                                                1 );

        hlpSS.str( std::string ( ) );
        hlpSS << "<pre>" << "Total structure volume: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }

        hlpSS2.str( std::string ( ) );
        hlpSS2 << std::showpos << std::setprecision ( 10 ) << *totalVolume * 1000.0 / 1000.0;
        if ( hlpSS2.str().length() != 15 ) { int hlp = 15 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
        if ( hlpSS2.str().length() > 15 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << " A^3 " << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                9,
                                                1,
                                                1,
                                                1 );
        
        hlpSS.str( std::string ( ) );
        hlpSS << "<pre>" << "Total mask volume: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str( std::string ( ) );
        hlpSS2 << std::showpos << std::setprecision ( 10 ) << *maskVolume * 1000.0 / 1000.0;
        if ( hlpSS2.str().length() != 15 ) { int hlp = 15 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
        if ( hlpSS2.str().length() > 15 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << " A^3 " << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                10,
                                                1,
                                                1,
                                                1 );
        
        hlpSS.str( std::string ( "<pre>" ) );
        for ( int iter = 0; iter < 70; iter++ )
        {
            hlpSS << "=";
        }
        hlpSS << "</pre>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                11,
                                                1,
                                                1,
                                                1 );
        
        hlpSS.str( std::string ( ) );
        hlpSS << "<pre>" << "RMS of complete map: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( *allDensityRMS * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << " " << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                12,
                                                1,
                                                1,
                                                1 );
        
        hlpSS.str( std::string ( ) );
        hlpSS << "<pre>" << "RMS of masked map: ";
        hlpIt                                 = static_cast<int> ( 70 - hlpSS.str().length() );
        for ( int iter = 0; iter < hlpIt; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS2.str( std::string ( ) );
        hlpSS2 << std::showpos << ProSHADE_internal_misc::roundDouble ( *maskDensityRMS * 1000.0 ) / 1000.0;
        if ( hlpSS2.str().length() != 6 ) { int hlp = 6 - hlpSS2.str().length(); for ( int i = 0; i < hlp; i++ ) { hlpSS2 << " "; } }
        if ( hlpSS2.str().length() > 6 ) { hlpSS2.str( hlpSS2.str().substr( 0, 6 ) ); }
        hlpSS << " " << hlpSS2.str() << " " << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                13,
                                                1,
                                                1,
                                                1 );
        
        rvapi_flush                       ( );
    }
    
    //======================================== Get real dimensions, instead of unit grid
    double xReal                              = this->_xRange / static_cast<double> ( this->_maxMapU + 1 );
    double yReal                              = this->_yRange / static_cast<double> ( this->_maxMapV + 1 );
    double zReal                              = this->_zRange / static_cast<double> ( this->_maxMapW + 1 );
    
    //======================================== Find the COM
    std::array<double,3> meanVals;
    meanVals[0]                               = 0.0;
    meanVals[1]                               = 0.0;
    meanVals[2]                               = 0.0;
    
    double totDens                            = 0.0;
    
    for ( uIt = 0; uIt < hlpU; uIt++ )
    {
        for ( vIt = 0; vIt < hlpV; vIt++ )
        {
            for ( wIt = 0; wIt < hlpW; wIt++ )
            {
                arrayPos                      = wIt + hlpW * ( vIt + hlpV * uIt );
                
                meanVals[0]                  += this->_densityMapCor[arrayPos] * uIt;
                meanVals[1]                  += this->_densityMapCor[arrayPos] * vIt;
                meanVals[2]                  += this->_densityMapCor[arrayPos] * wIt;
                
                totDens                      += this->_densityMapCor[arrayPos];
            }
        }
    }
    
    meanVals[0]                              /= totDens;
    meanVals[1]                              /= totDens;
    meanVals[2]                              /= totDens;
    
    //======================================== Re-index the map with COM in the centre
    this->_xCorrErr                           = meanVals[0] - static_cast<double> ( this->_maxMapU + 1 )/2.0;
    this->_yCorrErr                           = meanVals[1] - static_cast<double> ( this->_maxMapV + 1 )/2.0;
    this->_zCorrErr                           = meanVals[2] - static_cast<double> ( this->_maxMapW + 1 )/2.0;
    
    this->_xCorrection                        = std::ceil ( this->_xCorrErr );
    this->_yCorrection                        = std::ceil ( this->_yCorrErr );
    this->_zCorrection                        = std::ceil ( this->_zCorrErr );
    
    //======================================== Initialise internal values
    this->_densityMapCorCoords                = new std::array<double,3> [hlpU * hlpV * hlpW];
    int newU, newV, newW, coordPos;
    
    //======================================== Save the final coords
    for ( uIt = 0; uIt < hlpU; uIt++ )
    {
        for ( vIt = 0; vIt < hlpV; vIt++ )
        {
            for ( wIt = 0; wIt < hlpW; wIt++ )
            {
                newU                          = static_cast<int> ( uIt ) - static_cast<int> ( this->_xCorrection );
                newV                          = static_cast<int> ( vIt ) - static_cast<int> ( this->_yCorrection );
                newW                          = static_cast<int> ( wIt ) - static_cast<int> ( this->_zCorrection );
                
                if ( newU < 0 ) { newU += hlpU; } if ( newU >= static_cast<int> ( hlpU ) ) { newU -= hlpU; }
                if ( newV < 0 ) { newV += hlpV; } if ( newV >= static_cast<int> ( hlpV ) ) { newV -= hlpV; }
                if ( newW < 0 ) { newW += hlpW; } if ( newW >= static_cast<int> ( hlpW ) ) { newW -= hlpW; }
                
                arrayPos                      = wIt  + hlpW * ( vIt  + hlpV * uIt  );
                coordPos                      = newW + hlpW * ( newV  + hlpV * newU );
                
                this->_densityMapCorCoords[coordPos][0] = ( static_cast<double> ( newU ) - static_cast<double> ( hlpU ) / 2.0 ) * xReal;
                this->_densityMapCorCoords[coordPos][1] = ( static_cast<double> ( newV ) - static_cast<double> ( hlpV ) / 2.0 ) * yReal;
                this->_densityMapCorCoords[coordPos][2] = ( static_cast<double> ( newW ) - static_cast<double> ( hlpW ) / 2.0 ) * zReal;
            }
        }
    }
    if ( verbose > 2 )
    {
        std::cout << ">> Map centered." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Map centered." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Determine the real size of the shape
    double maxX                               = 0.0;
    double minX                               = 0.0;
    double maxY                               = 0.0;
    double minY                               = 0.0;
    double maxZ                               = 0.0;
    double minZ                               = 0.0;
    double maxXTot                            = 0.0;
    double minXTot                            = 0.0;
    double maxYTot                            = 0.0;
    double minYTot                            = 0.0;
    double maxZTot                            = 0.0;
    double minZTot                            = 0.0;

    coordPos                                  = 0;
    for ( uIt = 0; uIt < hlpU; uIt++ )
    {
        for ( vIt = 0; vIt < hlpV; vIt++ )
        {
            for ( wIt = 0; wIt < hlpW; wIt++ )
            {
                coordPos                      = wIt + hlpW * ( vIt  + hlpV * uIt );

                if ( this->_densityMapCor[coordPos] > 0.0 )
                {
                    maxX                      = std::max ( maxX, this->_densityMapCorCoords[coordPos][0] );
                    minX                      = std::min ( minX, this->_densityMapCorCoords[coordPos][0] );
                    maxY                      = std::max ( maxY, this->_densityMapCorCoords[coordPos][1] );
                    minY                      = std::min ( minY, this->_densityMapCorCoords[coordPos][1] );
                    maxZ                      = std::max ( maxZ, this->_densityMapCorCoords[coordPos][2] );
                    minZ                      = std::min ( minZ, this->_densityMapCorCoords[coordPos][2] );
                }
                maxXTot                       = std::max ( maxXTot, this->_densityMapCorCoords[coordPos][0] );
                minXTot                       = std::min ( minXTot, this->_densityMapCorCoords[coordPos][0] );
                maxYTot                       = std::max ( maxYTot, this->_densityMapCorCoords[coordPos][1] );
                minYTot                       = std::min ( minYTot, this->_densityMapCorCoords[coordPos][1] );
                maxZTot                       = std::max ( maxZTot, this->_densityMapCorCoords[coordPos][2] );
                minZTot                       = std::min ( minZTot, this->_densityMapCorCoords[coordPos][2] );
            }
        }
    }
    
    //======================================== Calculate the grid for only the shape
    int newUStart                             = std::floor ( ( ( minX - minXTot ) / ( maxXTot - minXTot ) ) * ( this->_maxMapU + 1 ) );
    int newUEnd                               = std::ceil  ( ( ( maxX - minXTot ) / ( maxXTot - minXTot ) ) * ( this->_maxMapU + 1 ) );
    int newVStart                             = std::floor ( ( ( minY - minYTot ) / ( maxYTot - minYTot ) ) * ( this->_maxMapV + 1 ) );
    int newVEnd                               = std::ceil  ( ( ( maxY - minYTot ) / ( maxYTot - minYTot ) ) * ( this->_maxMapV + 1 ) );
    int newWStart                             = std::floor ( ( ( minZ - minZTot ) / ( maxZTot - minZTot ) ) * ( this->_maxMapW + 1 ) );
    int newWEnd                               = std::ceil  ( ( ( maxZ - minZTot ) / ( maxZTot - minZTot ) ) * ( this->_maxMapW + 1 ) );
    
    //======================================== Make cube. If you want rectangle (more efficient), remove the following 6 lines, but beware, the map ordering will be YXZ...
    if ( useCubicMaps )
    {
        newUStart                             = std::min( newUStart, std::min( newVStart, newWStart ) );
        newVStart                             = std::min( newUStart, std::min( newVStart, newWStart ) );
        newWStart                             = std::min( newUStart, std::min( newVStart, newWStart ) );
        newUEnd                               = std::max( newUEnd  , std::max( newVEnd  , newWEnd   ) );
        newVEnd                               = std::max( newUEnd  , std::max( newVEnd  , newWEnd   ) );
        newWEnd                               = std::max( newUEnd  , std::max( newVEnd  , newWEnd   ) );
    }
    
    //======================================== Move map
    int newXDim                               = newUEnd - newUStart + 1;
    int newYDim                               = newVEnd - newVStart + 1;
    int newZDim                               = newWEnd - newWStart + 1;
    
    if ( ( this->_xFrom < 0 ) && ( ( this->_xFrom + newUStart ) >= 0 ) ) { newXDim += 1; }
    if ( ( this->_yFrom < 0 ) && ( ( this->_yFrom + newVStart ) >= 0 ) ) { newYDim += 1; }
    if ( ( this->_zFrom < 0 ) && ( ( this->_zFrom + newWStart ) >= 0 ) ) { newZDim += 1; }
    
    double *hlpMap                            = nullptr;
    hlpMap                                    = (double*) malloc ( newXDim * newYDim * newZDim * sizeof ( double ) );
    if ( hlpMap == nullptr )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory for map moving data. Terminating... " << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Cannot allocate memory for map data. Could you have run out of memory?" << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    int arrPos, hlpPos;
    for ( uIt = this->_xFrom; uIt <= this->_xTo; uIt++ )
    {
        for ( vIt = this->_yFrom; vIt <= this->_yTo; vIt++ )
        {
            for ( wIt = this->_zFrom; wIt <= this->_zTo; wIt++ )
            {
                if ( ( uIt < (this->_xFrom+newUStart) ) || ( uIt > (this->_xFrom+newUEnd) ) ||
                     ( vIt < (this->_yFrom+newVStart) ) || ( vIt > (this->_yFrom+newVEnd) ) ||
                     ( wIt < (this->_zFrom+newWStart) ) || ( wIt > (this->_zFrom+newWEnd) ) )
                {
                    continue;
                }
                
                newU                          = uIt - this->_xFrom;
                newV                          = vIt - this->_yFrom;
                newW                          = wIt - this->_zFrom;
                arrPos                        = newW + (this->_maxMapW + 1) * ( newV + (this->_maxMapV + 1) * newU );
                
                newU                          = newU - newUStart;
                newV                          = newV - newVStart;
                newW                          = newW - newWStart;
                hlpPos                        = newW + newZDim * ( newV + newYDim * newU );
                
                hlpMap[hlpPos]                = this->_densityMapCor[arrPos];
            }
        }
    }
    
    //======================================== Change the settings
    if ( addXOne ) { this->_xFrom            -= 1; }
    if ( addYOne ) { this->_yFrom            -= 1; }
    if ( addZOne ) { this->_zFrom            -= 1; }
    
    this->_xFrom                             += newUStart;
    this->_yFrom                             += newVStart;
    this->_zFrom                             += newWStart;
    
    this->_xTo                               -= this->_maxMapU - newUEnd;
    this->_yTo                               -= this->_maxMapV - newVEnd;
    this->_zTo                               -= this->_maxMapW - newWEnd;
    
    this->_maxMapU                            = newXDim - 1;
    this->_maxMapV                            = newYDim - 1;
    this->_maxMapW                            = newZDim - 1;
    
    if ( ( this->_xFrom > 0 ) && ( this->_xTo > 0 ) ) { this->_xTo += 1; }
    if ( ( this->_yFrom > 0 ) && ( this->_yTo > 0 ) ) { this->_yTo += 1; }
    if ( ( this->_zFrom > 0 ) && ( this->_zTo > 0 ) ) { this->_zTo += 1; }
    
    this->_xRange                             = this->_xSamplingRate * ( this->_maxMapU + 1 );
    this->_yRange                             = this->_ySamplingRate * ( this->_maxMapV + 1 );
    this->_zRange                             = this->_zSamplingRate * ( this->_maxMapW + 1 );
    
    //======================================== Copy tmp to holder
    delete[] this->_densityMapCor;
    this->_densityMapCor                      = nullptr;
    this->_densityMapCor                      = new double [newXDim * newYDim * newZDim];
    
    for ( int iter = 0; iter < (newXDim * newYDim * newZDim); iter++ )
    {
        this->_densityMapCor[iter]            = hlpMap[iter];
    }
    
    if ( hlpMap != nullptr )
    {
        free                                  ( hlpMap );
        hlpMap                                = nullptr;
    }
    
    if ( verbose > 2 )
    {
        std::cout << ">> Map resized." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Map resized." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Done
    
}

/*! \brief This function removes the phase information from the density map.
 
 This function uses FFTW transform to get the phase and magnitude values and then sets phase to 0, thus removing all
 the phase information. It then processes the Patterson (or rather Patterson-like, as the coefficient could be different)
 map so that it can easily be decomposed onto spherical harmonics coefficients.
 
 \param[in] alpha The coefficient to which to raise the structure factors. 1 = Patterson map.
 \param[in] bFac At this point, it is still possible to change the B-factors, so if you so please, here is where to do it.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_data::removePhaseFromMap ( double alpha,
                                                            double bFac,
                                                            ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Sanity check
    if ( !this->_densityMapComputed )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file " << this->_inputFileName << " !!! Attempted to remove phase information from map before computing the map. Call the getDensityMapFromPDB function BEFORE the removePhaseFromMap function." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Attempted to remove phase information from map before computing the map. Call the getDensityMapFromPDB function BEFORE the removePhaseFromMap function. This looks like an internal bug - please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Initialise internal values
    this->_fourierCoeffPower                  = alpha;
    this->_bFactorChange                      = bFac;
    
    //======================================== Initialise local variables for Fourier and Inverse Fourier
    fftw_complex *tmpOut;
    fftw_complex *tmpIn;
    fftw_plan p;
    fftw_plan pInv;
    int uIt, vIt, wIt;
    double real, imag;
    int hlpU                                  = ( this->_maxMapU + 1 );
    int hlpV                                  = ( this->_maxMapV + 1 );
    int hlpW                                  = ( this->_maxMapW + 1 );
    
    this->_densityMapCor                      = new double [(this->_maxMapU+2) * (this->_maxMapV+2) * (this->_maxMapW+2)];
    tmpIn                                     = new fftw_complex[hlpU * hlpV * hlpW];
    tmpOut                                    = new fftw_complex[hlpU * hlpV * hlpW];
    
    //======================================== Create plans (FFTW_MEASURE would change the arrays)
    p                                         = fftw_plan_dft_3d ( hlpU, hlpV, hlpW, tmpIn, tmpOut, FFTW_FORWARD, FFTW_ESTIMATE );
    pInv                                      = fftw_plan_dft_3d ( hlpU, hlpV, hlpW, tmpOut, tmpIn, FFTW_BACKWARD, FFTW_ESTIMATE );
    
    //======================================== Load map data for Fourier
    unsigned int noMapPoints                  = 0;
    
    for ( uIt = 0; uIt < static_cast<int> (this->_maxMapU+1); uIt++ )
    {
        for ( vIt = 0; vIt < static_cast<int> (this->_maxMapV+1); vIt++ )
        {
            for ( wIt = 0; wIt < static_cast<int> (this->_maxMapW+1); wIt++ )
            {
                //============================ Initialisation
                noMapPoints                   = wIt + (this->_maxMapW + 1) * ( vIt + (this->_maxMapV + 1) * uIt );
                
                if ( this->_densityMapMap[noMapPoints] == this->_densityMapMap[noMapPoints] )
                {
                    tmpIn[noMapPoints][0]     = this->_densityMapMap[noMapPoints];
                    tmpIn[noMapPoints][1]     = 0.0;
                }
                else
                {
                    tmpIn[noMapPoints][0]     = 0.0;
                    tmpIn[noMapPoints][1]     = 0.0;
                }
            }
        }
    }
    
    //======================================== Calculate Fourier
    fftw_execute                              ( p );
    
    //======================================== Remove phase information
    unsigned int arrayPos                     = 0;
    int h, k, l;
    double mag, S;
    double normFactor                         = (hlpU * hlpV * hlpW);
    for ( uIt = 0; uIt < hlpU; uIt++ )
    {
        for ( vIt = 0; vIt < hlpV; vIt++ )
        {
            for ( wIt = 0; wIt < hlpW; wIt++ )
            {
                //============================ Var init
                arrayPos                      = wIt + hlpW * ( vIt + hlpV * uIt );
                real                          = tmpOut[arrayPos][0];
                imag                          = tmpOut[arrayPos][1];
                
                //============================ Change the B-factors, if required
                if ( uIt > (hlpU / 2) ) { h = uIt - hlpU; } else { h = uIt; }
                if ( vIt > (hlpV / 2) ) { k = vIt - hlpV; } else { k = vIt; }
                if ( wIt > (hlpW / 2) ) { l = wIt - hlpW; } else { l = wIt; }
                
                S                             = pow( static_cast<double> ( h ) / this->_xRange, 2.0 ) + pow( static_cast<double> ( k ) / this->_yRange, 2.0 ) + pow( static_cast<double> ( l ) / this->_zRange, 2.0 );
                mag                           = std::exp ( - ( ( this->_bFactorChange * S ) / 4.0 ) );
                
                //============================ Get magnitude and phase
                mag                          *= sqrt ( (real*real) + (imag*imag) );
                
                //============================ Magnitude power alpha
                mag                           = pow ( mag, this->_fourierCoeffPower );
                
                //============================ Phase removal
                tmpOut[arrayPos][0]           = mag;
                tmpOut[arrayPos][1]           = 0.0;
                
                //============================ Normalize
                tmpOut[arrayPos][0]          /= normFactor;
            }
        }
    }
    
    //======================================== Run backward Fourier
    fftw_execute                              ( pInv );
    
    //======================================== Free allocated memory
    fftw_destroy_plan                         ( p );
    fftw_destroy_plan                         ( pInv );
    
    //======================================== Initialise internal values
    this->_densityMapCorCoords                = new std::array<double,3> [(this->_maxMapU+2) * (this->_maxMapV+2) * (this->_maxMapW+2)];
    
    //======================================== Re-order (i.e. centralise) the Patterson
    int newU;
    int newV;
    int newW;
    int cenPosU;
    int cenPosV;
    int cenPosW;
    int nonTranslatedIter                     = 0;
    int coordPos;
    
    for ( uIt = 0; uIt < static_cast<int> (this->_maxMapU + 2); uIt++ )
    {
        for ( vIt = 0; vIt < static_cast<int> (this->_maxMapV + 2); vIt++ )
        {
            for ( wIt = 0; wIt < static_cast<int> (this->_maxMapW + 2); wIt++ )
            {
                if ( ( uIt == hlpU ) || ( vIt == hlpV ) || ( wIt == hlpW ) ) // Deal with the -x to +x-1 unsymmetric results problem by creating +x and copying values from -x (this is only possible when phases are removed as +x and -x must then by identical by symmetry)
                {
                    newU                      = uIt; if ( uIt == hlpU ) { newU = 0; }
                    newV                      = vIt; if ( vIt == hlpV ) { newV = 0; }
                    newW                      = wIt; if ( wIt == hlpW ) { newW = 0; }
                    
                    cenPosU                   = newU + (hlpU / 2); if ( cenPosU >= hlpU ) { cenPosU -= hlpU; }
                    cenPosV                   = newV + (hlpV / 2); if ( cenPosV >= hlpV ) { cenPosV -= hlpV; }
                    cenPosW                   = newW + (hlpW / 2); if ( cenPosW >= hlpW ) { cenPosW -= hlpW; }
                    
                    arrayPos                  = cenPosW + hlpW * ( cenPosV + hlpV * cenPosU );
                    
                    this->_densityMapCor[nonTranslatedIter]          = tmpIn[arrayPos][0];
                    this->_densityMapCorCoords[nonTranslatedIter][0] = uIt - hlpU / 2;
                    this->_densityMapCorCoords[nonTranslatedIter][1] = vIt - hlpV / 2;
                    this->_densityMapCorCoords[nonTranslatedIter][2] = wIt - hlpW / 2;
                }
                else // Proceed normally
                {
                    arrayPos                  = wIt + hlpW * ( vIt + hlpV * uIt );
                    
                    cenPosU                   = uIt + (hlpU / 2); if ( cenPosU >= hlpU ) { cenPosU -= hlpU; }
                    cenPosV                   = vIt + (hlpV / 2); if ( cenPosV >= hlpV ) { cenPosV -= hlpV; }
                    cenPosW                   = wIt + (hlpW / 2); if ( cenPosW >= hlpW ) { cenPosW -= hlpW; }
                    coordPos                  = cenPosW + (this->_maxMapW + 2) * ( cenPosV + (this->_maxMapV + 2) * cenPosU );
                    
                    this->_densityMapCor[coordPos]          = tmpIn[arrayPos][0];
                    this->_densityMapCorCoords[coordPos][0] = cenPosU - hlpU / 2;
                    this->_densityMapCorCoords[coordPos][1] = cenPosV - hlpV / 2;
                    this->_densityMapCorCoords[coordPos][2] = cenPosW - hlpW / 2;
                }
                nonTranslatedIter            += 1;
            }
        }
    }
    
    //======================================== Free allocated memory
    delete[] tmpOut;
    delete[] tmpIn;
    
    //======================================== Free unnecessary memory
    if ( this->_densityMapMap != nullptr )
    {
        delete this->_densityMapMap;
        this->_densityMapMap                  = nullptr;
    }
    
    //======================================== Done
    this->_phaseRemoved                       = true;
    this->_keepOrRemove                       = false;
    this->_usePhase                           = false;
    
}

/*! \brief This function removes the phase information from the density map for the overlay purposes.
 
 This function uses FFTW transform to get the phase and magnitude values and then sets phase to 0, thus removing all
 the phase information. It then processes the Patterson (or rather Patterson-like, as the coefficient could be different)
 map differently to the other function, this time with emphasis of data format similariry.
 
 \param[in] alpha The coefficient to which to raise the structure factors. 1 = Patterson map.
 \param[in] bFac At this point, it is still possible to change the B-factors, so if you so please, here is where to do it.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_data::removePhaseFromMapOverlay ( double alpha,
                                                                   double bFac,
                                                                   ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Sanity check
    if ( !this->_densityMapComputed )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file " << this->_inputFileName << " !!! Attempted to remove phase information from map before computing the map. Call the getDensityMapFromPDB function BEFORE the removePhaseFromMap function." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Attempted to remove phase information from map before computing the map. Call the getDensityMapFromPDB function BEFORE the removePhaseFromMap function. This looks like an internal bug - please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Initialise internal values
    this->_fourierCoeffPower                  = alpha;
    this->_bFactorChange                      = bFac;
    
    //======================================== Initialise local variables for Fourier and Inverse Fourier
    fftw_complex *tmpOut;
    fftw_complex *tmpIn;
    fftw_plan p;
    fftw_plan pInv;
    int uIt, vIt, wIt;
    double real, imag;
    int hlpU                                  = ( this->_maxMapU + 1 );
    int hlpV                                  = ( this->_maxMapV + 1 );
    int hlpW                                  = ( this->_maxMapW + 1 );
    
    this->_densityMapCor                      = new double [hlpU * hlpV * hlpW];
    tmpIn                                     = new fftw_complex[hlpU * hlpV * hlpW];
    tmpOut                                    = new fftw_complex[hlpU * hlpV * hlpW];
    
    //======================================== Create plans (FFTW_MEASURE would change the arrays)
    p                                         = fftw_plan_dft_3d ( hlpU, hlpV, hlpW, tmpIn, tmpOut, FFTW_FORWARD, FFTW_ESTIMATE );
    pInv                                      = fftw_plan_dft_3d ( hlpU, hlpV, hlpW, tmpOut, tmpIn, FFTW_BACKWARD, FFTW_ESTIMATE );
    
    //======================================== Load map data for Fourier
    unsigned int arrayPos                  = 0;
    
    for ( uIt = 0; uIt < static_cast<int> (this->_maxMapU+1); uIt++ )
    {
        for ( vIt = 0; vIt < static_cast<int> (this->_maxMapV+1); vIt++ )
        {
            for ( wIt = 0; wIt < static_cast<int> (this->_maxMapW+1); wIt++ )
            {
                //============================ Initialisation
                arrayPos                      = wIt + (this->_maxMapW + 1) * ( vIt + (this->_maxMapV + 1) * uIt );
                
                if ( this->_densityMapMap[arrayPos] == this->_densityMapMap[arrayPos] )
                {
                    tmpIn[arrayPos][0]        = this->_densityMapMap[arrayPos];
                    tmpIn[arrayPos][1]        = 0.0;
                }
                else
                {
                    tmpIn[arrayPos][0]        = 0.0;
                    tmpIn[arrayPos][1]        = 0.0;
                }
            }
        }
    }
    
    //======================================== Calculate Fourier
    fftw_execute                              ( p );
    
    //======================================== Remove phase information
    arrayPos                                  = 0;
    int h, k, l;
    double mag, S;
    double normFactor                         = (hlpU * hlpV * hlpW);
    for ( uIt = 0; uIt < hlpU; uIt++ )
    {
        for ( vIt = 0; vIt < hlpV; vIt++ )
        {
            for ( wIt = 0; wIt < hlpW; wIt++ )
            {
                //============================ Var init
                arrayPos                      = wIt + hlpW * ( vIt + hlpV * uIt );
                real                          = tmpOut[arrayPos][0];
                imag                          = tmpOut[arrayPos][1];
                
                //============================ Change the B-factors, if required
                if ( uIt > (hlpU / 2) ) { h = uIt - hlpU; } else { h = uIt; }
                if ( vIt > (hlpV / 2) ) { k = vIt - hlpV; } else { k = vIt; }
                if ( wIt > (hlpW / 2) ) { l = wIt - hlpW; } else { l = wIt; }
                
                S                             = pow( static_cast<double> ( h ) / this->_xRange, 2.0 ) + pow( static_cast<double> ( k ) / this->_yRange, 2.0 ) + pow( static_cast<double> ( l ) / this->_zRange, 2.0 );
                mag                           = std::exp ( - ( ( this->_bFactorChange * S ) / 4.0 ) );
                
                //============================ Get magnitude and phase
                mag                          *= sqrt ( (real*real) + (imag*imag) );
                
                //============================ Magnitude power alpha
                mag                           = pow ( mag, this->_fourierCoeffPower );
                
                //============================ Phase removal
                tmpOut[arrayPos][0]           = mag;
                tmpOut[arrayPos][1]           = 0.0;
                
                //============================ Normalize
                tmpOut[arrayPos][0]          /= normFactor;
            }
        }
    }
    
    //======================================== Run backward Fourier
    fftw_execute                              ( pInv );
    
    //======================================== Free allocated memory
    fftw_destroy_plan                         ( p );
    fftw_destroy_plan                         ( pInv );
    
    //======================================== Re-order (i.e. centralise) the Patterson
    int cenPosU;
    int cenPosV;
    int cenPosW;
    int coordPos;
    
    for ( uIt = 0; uIt < static_cast<int> (this->_maxMapU + 1); uIt++ )
    {
        for ( vIt = 0; vIt < static_cast<int> (this->_maxMapV + 1); vIt++ )
        {
            for ( wIt = 0; wIt < static_cast<int> (this->_maxMapW + 1); wIt++ )
            {
                arrayPos                      = wIt + hlpW * ( vIt + hlpV * uIt );
                
                cenPosU                       = uIt + (hlpU / 2); if ( cenPosU >= hlpU ) { cenPosU -= hlpU; }
                cenPosV                       = vIt + (hlpV / 2); if ( cenPosV >= hlpV ) { cenPosV -= hlpV; }
                cenPosW                       = wIt + (hlpW / 2); if ( cenPosW >= hlpW ) { cenPosW -= hlpW; }
                coordPos                      = cenPosW + (this->_maxMapW + 1) * ( cenPosV + (this->_maxMapV + 1) * cenPosU );
                
                this->_densityMapCor[arrayPos]= tmpIn[coordPos][0];
            }
        }
    }
    
    //======================================== Free allocated memory
    delete[] tmpOut;
    delete[] tmpIn;
    
    //======================================== Free unnecessary memory
    if ( this->_densityMapMap != nullptr )
    {
        delete this->_densityMapMap;
        this->_densityMapMap                  = nullptr;
    }
    
    //======================================== Done
    this->_phaseRemoved                       = true;
    this->_usePhase                           = false;
    this->_keepOrRemove                       = false;
    
}

/*! \brief This function keeps the phase information from the density map and prepares the data for SH coefficient computation.
 
 This function does all the data processing that is required when the phase is kept, that is it applies the structure factor
 coefficients if needed, mask map data, removes islands of density, re-scales the box and re-calculates the SH computation
 parameters for the re-scaled box.
 
 \param[in] alpha The coefficient to which to raise the structure factors. 1 = Patterson map.
 \param[in] bFac At this point, it is still possible to change the B-factors, so if you so please, here is where to do it.
 \param[in] bandwidth A pointer to variable which will hold the optimal bandwidth as determined by the function or the value supplied by the user.
 \param[in] theta A pointer to variable which will hold the optimal theta angle as determined by the function or the value supplied by the user.
 \param[in] phi A pointer to variable which will hold the optimal phi angle as determined by the function or the value supplied by the user.
 \param[in] extraSpace A pointer to variable which will hold the optimal between cells distance as determined by the function or the value supplied by the user.
 \param[in] useCom A boolean value determining if Centre of Mass (COM) should be used to center the map.
 \param[in] maxMapIQR A double value specifying maximum number of interquartile ranges from mean to be used to threshold map masking.
 \param[in] verbose How loud should the function be in reporting progress.
 \param[in] clearMapData Should the map be cleaned?
 \param[in] rotDefaults Is this being done for rotating the structure?
 \param[in] overlapDefaults Is this being done for overlay mode?
 \param[in] blurFactor Masking blurring factor value.
 \param[in] maskBlurFactorGiven Did the user supply specific blurring factor value for masking?
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_data::keepPhaseInMap ( double alpha,
                                                        double bFac,
                                                        unsigned int *bandwidth,
                                                        unsigned int *theta,
                                                        unsigned int *phi,
                                                        unsigned int *glIntegOrder,
                                                        ProSHADE::ProSHADE_settings* settings,
                                                        bool useCom,
                                                        double maxMapIQR,
                                                        int verbose,
                                                        bool clearMapData,
                                                        bool rotDefaults,
                                                        bool overlapDefaults,
                                                        double blurFactor,
                                                        bool maskBlurFactorGiven )
{
    //======================================== Sanity check
    if ( !this->_densityMapComputed )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file " << this->_inputFileName << " !!! Attempted to keep phase information from map before computing the map. Call the getDensityMapFromPDB function BEFORE the keepPhaseInMap function." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Attempted to remove phase information from map before computing the map. Call the getDensityMapFromPDB function BEFORE the keepPhaseInMap function. This looks like an internal bug - please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Initialise internal values
    this->_fourierCoeffPower                  = alpha;
    this->_bFactorChange                      = bFac;
    
    //======================================== Local hlp variables
    int hlpU                                  = ( this->_maxMapU + 1 );
    int hlpV                                  = ( this->_maxMapV + 1 );
    int hlpW                                  = ( this->_maxMapW + 1 );
    
    //======================================== Initialise local variables for Fourier and Inverse Fourier
    fftw_complex *tmpOut;
    fftw_complex *tmpIn;
    fftw_plan p;
    fftw_plan pInv;
    int uIt, vIt, wIt;
    double real, imag;
    unsigned int arrayPos                     = 0;
    int arrPos                                = 0;
    
    this->_densityMapCor                      = new double      [hlpU * hlpV * hlpW];
    tmpIn                                     = new fftw_complex[hlpU * hlpV * hlpW];
    tmpOut                                    = new fftw_complex[hlpU * hlpV * hlpW];
    
    //======================================== Create plans (FFTW_MEASURE would change the arrays)
    p                                         = fftw_plan_dft_3d ( hlpU, hlpV, hlpW, tmpIn, tmpOut, FFTW_FORWARD, FFTW_ESTIMATE );
    pInv                                      = fftw_plan_dft_3d ( hlpU, hlpV, hlpW, tmpOut, tmpIn, FFTW_BACKWARD, FFTW_ESTIMATE );
    
    //======================================== Load map data for Fourier
    for ( uIt = 0; uIt < static_cast<int> (this->_maxMapU+1); uIt++ )
    {
        for ( vIt = 0; vIt < static_cast<int> (this->_maxMapV+1); vIt++ )
        {
            for ( wIt = 0; wIt < static_cast<int> (this->_maxMapW+1); wIt++ )
            {
                //============================ Initialisation
                arrPos                        = wIt + (this->_maxMapW + 1) * ( vIt + (this->_maxMapV + 1) * uIt );
                
                if ( this->_densityMapMap[arrPos] == this->_densityMapMap[arrPos] )
                {
                    tmpIn[arrPos][0]          = this->_densityMapMap[arrPos];
                    tmpIn[arrPos][1]          = 0.0;
                }
                else
                {
                    tmpIn[arrPos][0]          = 0.0;
                    tmpIn[arrPos][1]          = 0.0;
                }
            }
        }
    }
    
    //======================================== Check if alpha or B-factor change is required
    if ( ( this->_fourierCoeffPower != 1.0 ) || ( this->_bFactorChange != 0.0 ) )
    {
        //==================================== Calculate Fourier
        fftw_execute                          ( p );
        
        //==================================== Manipulate the B-factors and such
        int h, k, l;
        double mag, phase, S;
        double normFactor = (hlpU * hlpV * hlpW);
        for ( uIt = 0; uIt < hlpU; uIt++ )
        {
            for ( vIt = 0; vIt < hlpV; vIt++ )
            {
                for ( wIt = 0; wIt < hlpW; wIt++ )
                {
                    //======================== Var init
                    arrayPos                  = wIt + hlpW * ( vIt + hlpV * uIt );
                    real                      = tmpOut[arrayPos][0];
                    imag                      = tmpOut[arrayPos][1];
                    
                    //======================== Change the B-factors, if required
                    if ( uIt > (hlpU / 2) ) { h = uIt - hlpU; } else { h = uIt; }
                    if ( vIt > (hlpV / 2) ) { k = vIt - hlpV; } else { k = vIt; }
                    if ( wIt > (hlpW / 2) ) { l = wIt - hlpW; } else { l = wIt; }
                    
                    S                         = pow ( static_cast<double> ( h ) / this->_xRange, 2.0 ) +
                                                pow ( static_cast<double> ( k ) / this->_yRange, 2.0 ) +
                                                pow ( static_cast<double> ( l ) / this->_zRange, 2.0 );
                    mag                       = std::exp ( - ( ( this->_bFactorChange * S ) / 4.0 ) );
                    
                    //======================== Get magnitude and phase
                    mag                      *= sqrt ( (real*real) + (imag*imag) );
                    phase                     = atan2 ( imag, real );
                    
                    //======================== Magnitude power alpha
                    mag                       = pow ( mag, this->_fourierCoeffPower );
                    
                    //======================== Phase kept
                    tmpOut[arrayPos][0]       = mag * cos(phase);
                    tmpOut[arrayPos][1]       = mag * sin(phase);
                    
                    //======================== Normalize
                    tmpOut[arrayPos][0]      /= normFactor;
                    tmpOut[arrayPos][1]      /= normFactor;
                }
            }
        }
        
        //==================================== Run backward Fourier
        fftw_execute                          ( pInv );
    }
    
    //======================================== Free allocated memory
    fftw_destroy_plan                         ( p );
    fftw_destroy_plan                         ( pInv );
    
    //======================================== Copy map to new holder
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( hlpU * hlpV * hlpW ); iter++ )
    {
        this->_densityMapCor[iter]            = this->_densityMapMap[iter];
    }
    
    //======================================== Free unnecessary memory
    if ( this->_densityMapMap != nullptr )
    {
        free                                  ( this->_densityMapMap );
        this->_densityMapMap                  = nullptr;
    }

    //======================================== Map cleaning
    if ( !this->_fromPDB )
    {
        if ( clearMapData )
        {
            //================================ Save original map to restore for each cycle
            double *tmpMp                     = new double[hlpU * hlpV * hlpW];
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( hlpU * hlpV * hlpW ); iter++ )
            {
                tmpMp[iter]                       = this->_densityMapCor[iter];
            }
            
            bool notTooManyIslads                 = true;
            double blFr                           = 150.0;
            
            while ( notTooManyIslads )
            {
                //============================ Do not change user values
                blFr                         += 50.0;
                if ( maskBlurFactorGiven )
                {
                    blFr                      = blurFactor;
                    this->maskMap             ( hlpU, hlpV, hlpW, blFr, maxMapIQR, 3.0 );
                    this->removeIslands       ( hlpU, hlpV, hlpW, verbose, true );
                    break;
                }
                
                //============================ Compute mask and apply it
                this->maskMap                 ( hlpU, hlpV, hlpW, blFr, maxMapIQR, 3.0 );
                
                //============================ Detect islands
                if ( verbose > 3 )
                {
                    std::cout << ">>>>>>>> Island detection started." << std::endl;
                }
                
                notTooManyIslads              = this->removeIslands ( hlpU, hlpV, hlpW, verbose, false );

                if ( maskBlurFactorGiven )
                {
                    break;
                }
                
                if ( verbose > 3 )
                {
                    std::cout << ">>>>> Map masked with factor of " << blFr << " and small islands were then removed.";
                }
                if ( notTooManyIslads )
                {
                    if ( verbose > 3 )
                    {
                        std::cout << " However, too many islands remained. Trying higher blurring factor." << std::endl;
                    }
                    
                    if ( blFr <= 500.0 )
                    {
                        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( hlpU * hlpV * hlpW ); iter++ )
                        {
                            this->_densityMapCor[iter] = tmpMp[iter];
                        }
                    }
                }
                else
                {
                    if ( verbose > 3 )
                    {
                        std::cout << std::endl;
                    }
                }
                
                if ( blFr > 500.0 )
                {
                    std::cout << "!!! ProSHADE WARNING !!! Even blurring factor of 500 did not remove islands. Will not proceed with what we have, but be warned that either the masking is bugged or yout map has high levels of noise. You can consider using the -y ( or --no_clear ) options to avoid this message and the extra time for testing different blurring factors for this map." << std::endl;
                    
                    if ( settings->htmlReport )
                    {
                        std::stringstream hlpSS;
                        hlpSS << "<font color=\"orange\">" << "Even blurring factor of 500 did not remove islands. Will not proceed with what we have, but be warned that either the masking is bugged or yout map has high levels of noise. You can consider using the -y ( or --no_clear ) options to avoid this message and the extra time for testing different blurring factors for this map." << "</font>";
                        rvapi_set_text                    ( hlpSS.str().c_str(),
                                                           "ProgressSection",
                                                           settings->htmlReportLineProgress,
                                                           1,
                                                           1,
                                                           1 );
                        settings->htmlReportLineProgress += 1;
                        rvapi_flush                       ( );
                    }
                    
                    break;
                }
            }
            delete[] tmpMp;
        }
    }
    
    //======================================== Get real dimensions, instead of unit grid
    double xReal                              = this->_xRange / static_cast<double> ( this->_maxMapU + 1 );
    double yReal                              = this->_yRange / static_cast<double> ( this->_maxMapV + 1 );
    double zReal                              = this->_zRange / static_cast<double> ( this->_maxMapW + 1 );
    
    //======================================== Find the COM
    std::array<double,3> meanVals;
    meanVals[0]                               = 0.0;
    meanVals[1]                               = 0.0;
    meanVals[2]                               = 0.0;
    
    if ( !this->_firstLineCOM )
    {
        if ( useCom )
        {
            double totDens                    = 0.0;
            
            for ( uIt = 0; uIt < hlpU; uIt++ )
            {
                for ( vIt = 0; vIt < hlpV; vIt++ )
                {
                    for ( wIt = 0; wIt < hlpW; wIt++ )
                    {
                        arrayPos              = wIt + hlpW * ( vIt + hlpV * uIt );
                        
                        if ( !this->_fromPDB )
                        {
                            if ( this->_densityMapCor[arrayPos] > 0.0 )
                            {
                                meanVals[0]  += this->_densityMapCor[arrayPos] * uIt;
                                meanVals[1]  += this->_densityMapCor[arrayPos] * vIt;
                                meanVals[2]  += this->_densityMapCor[arrayPos] * wIt;
                                
                                totDens      += this->_densityMapCor[arrayPos];
                            }
                        }
                        else
                        {
                            if ( tmpIn[arrayPos][0] > 0.0 )
                            {
                                meanVals[0]  += tmpIn[arrayPos][0] * uIt;
                                meanVals[1]  += tmpIn[arrayPos][0] * vIt;
                                meanVals[2]  += tmpIn[arrayPos][0] * wIt;
                                
                                totDens      += tmpIn[arrayPos][0];
                            }
                        }
                    }
                }
            }
            
            meanVals[0]                      /= totDens;
            meanVals[1]                      /= totDens;
            meanVals[2]                      /= totDens;
        }
        else
        {
            meanVals[0]                       = static_cast<double> ( this->_maxMapU + 1 )/2.0;
            meanVals[1]                       = static_cast<double> ( this->_maxMapV + 1 )/2.0;
            meanVals[2]                       = static_cast<double> ( this->_maxMapW + 1 )/2.0;
        }
    }
    else
    {
        meanVals[0]                           = this->_xCorrErr / xReal;
        meanVals[1]                           = this->_yCorrErr / yReal;
        meanVals[2]                           = this->_zCorrErr / zReal;
    }
    
    //======================================== Re-index the map with COM in the centre
    this->_xCorrErr                           = meanVals[0] - static_cast<double> ( this->_maxMapU + 1 )/2.0;
    this->_yCorrErr                           = meanVals[1] - static_cast<double> ( this->_maxMapV + 1 )/2.0;
    this->_zCorrErr                           = meanVals[2] - static_cast<double> ( this->_maxMapW + 1 )/2.0;
    
    this->_xCorrection                        = std::ceil ( this->_xCorrErr );
    this->_yCorrection                        = std::ceil ( this->_yCorrErr );
    this->_zCorrection                        = std::ceil ( this->_zCorrErr );
    
    //======================================== Initialise internal values
    this->_densityMapCorCoords                = new std::array<double,3> [hlpU * hlpV * hlpW];
    int newU, newV, newW, coordPos;
    
    if ( this->_fromPDB )
    {
        if ( useCom )
        {
            for ( uIt = 0; uIt < hlpU; uIt++ )
            {
                for ( vIt = 0; vIt < hlpV; vIt++ )
                {
                    for ( wIt = 0; wIt < hlpW; wIt++ )
                    {
                        newU                  = static_cast<int> ( uIt ) - static_cast<int> ( this->_xCorrection );
                        newV                  = static_cast<int> ( vIt ) - static_cast<int> ( this->_yCorrection );
                        newW                  = static_cast<int> ( wIt ) - static_cast<int> ( this->_zCorrection );
                        
                        if ( newU < 0 ) { newU += hlpU; } if ( newU >= static_cast<int> ( hlpU ) ) { newU -= hlpU; }
                        if ( newV < 0 ) { newV += hlpV; } if ( newV >= static_cast<int> ( hlpV ) ) { newV -= hlpV; }
                        if ( newW < 0 ) { newW += hlpW; } if ( newW >= static_cast<int> ( hlpW ) ) { newW -= hlpW; }
                        
                        arrayPos              = wIt  + hlpW * ( vIt  + hlpV * uIt  );
                        coordPos              = newW + hlpW * ( newV  + hlpV * newU );
                        
                        this->_densityMapCor[coordPos]  = tmpIn[arrayPos][0];
                        
                        //======================== Save results
                        this->_densityMapCorCoords[coordPos][0] = ( static_cast<double> ( newU ) - static_cast<double> ( hlpU ) / 2.0 ) * xReal;
                        this->_densityMapCorCoords[coordPos][1] = ( static_cast<double> ( newV ) - static_cast<double> ( hlpV ) / 2.0 ) * yReal;
                        this->_densityMapCorCoords[coordPos][2] = ( static_cast<double> ( newW ) - static_cast<double> ( hlpW ) / 2.0 ) * zReal;
                    }
                }
            }
        }
        else
        {
            for ( uIt = 0; uIt < hlpU; uIt++ )
            {
                for ( vIt = 0; vIt < hlpV; vIt++ )
                {
                    for ( wIt = 0; wIt < hlpW; wIt++ )
                    {
                        arrayPos              = wIt  + hlpW * ( vIt  + hlpV * uIt  );
                        
                        this->_densityMapCor[arrayPos] = tmpIn[arrayPos][0];
                        this->_densityMapCorCoords[arrayPos][0] = ( static_cast<double> ( uIt ) - ( static_cast<double> ( hlpU ) / 2.0 ) ) * xReal;
                        this->_densityMapCorCoords[arrayPos][1] = ( static_cast<double> ( vIt ) - ( static_cast<double> ( hlpV ) / 2.0 ) ) * yReal;
                        this->_densityMapCorCoords[arrayPos][2] = ( static_cast<double> ( wIt ) - ( static_cast<double> ( hlpW ) / 2.0 ) ) * zReal;
                    }
                }
            }
        }
    }
    //======================================== Speedup for maps
    else
    {
        if ( !clearMapData )
        {
            this->_xCorrection                = 0.0;
            this->_yCorrection                = 0.0;
            this->_zCorrection                = 0.0;
        }
        
        //==================================== Save the final coords
        for ( uIt = 0; uIt < hlpU; uIt++ )
        {
            for ( vIt = 0; vIt < hlpV; vIt++ )
            {
                for ( wIt = 0; wIt < hlpW; wIt++ )
                {
                    newU                      = static_cast<int> ( uIt ) - static_cast<int> ( this->_xCorrection );
                    newV                      = static_cast<int> ( vIt ) - static_cast<int> ( this->_yCorrection );
                    newW                      = static_cast<int> ( wIt ) - static_cast<int> ( this->_zCorrection );
                    
                    if ( newU < 0 ) { newU += hlpU; } if ( newU >= static_cast<int> ( hlpU ) ) { newU -= hlpU; }
                    if ( newV < 0 ) { newV += hlpV; } if ( newV >= static_cast<int> ( hlpV ) ) { newV -= hlpV; }
                    if ( newW < 0 ) { newW += hlpW; } if ( newW >= static_cast<int> ( hlpW ) ) { newW -= hlpW; }
                    
                    arrayPos                  = wIt  + hlpW * ( vIt  + hlpV * uIt  );
                    coordPos                  = newW + hlpW * ( newV  + hlpV * newU );
                    
                    this->_densityMapCorCoords[coordPos][0] = ( static_cast<double> ( newU ) - static_cast<double> ( hlpU ) / 2.0 ) * xReal;
                    this->_densityMapCorCoords[coordPos][1] = ( static_cast<double> ( newV ) - static_cast<double> ( hlpV ) / 2.0 ) * yReal;
                    this->_densityMapCorCoords[coordPos][2] = ( static_cast<double> ( newW ) - static_cast<double> ( hlpW ) / 2.0 ) * zReal;
                }
            }
        }
        
        //==================================== Determine the real size of the shape
        double maxX                           = 0.0;
        double minX                           = 0.0;
        double maxY                           = 0.0;
        double minY                           = 0.0;
        double maxZ                           = 0.0;
        double minZ                           = 0.0;
        double maxXTot                        = 0.0;
        double minXTot                        = 0.0;
        double maxYTot                        = 0.0;
        double minYTot                        = 0.0;
        double maxZTot                        = 0.0;
        double minZTot                        = 0.0;
        
        for ( uIt = 0; uIt < hlpU; uIt++ )
        {
            for ( vIt = 0; vIt < hlpV; vIt++ )
            {
                for ( wIt = 0; wIt < hlpW; wIt++ )
                {
                    coordPos     = wIt + hlpW * ( vIt  + hlpV * uIt );
                    
                    if ( this->_densityMapCor[coordPos] > 0.0 )
                    {
                        maxX                  = std::max ( maxX, this->_densityMapCorCoords[coordPos][0] );
                        minX                  = std::min ( minX, this->_densityMapCorCoords[coordPos][0] );
                        maxY                  = std::max ( maxY, this->_densityMapCorCoords[coordPos][1] );
                        minY                  = std::min ( minY, this->_densityMapCorCoords[coordPos][1] );
                        maxZ                  = std::max ( maxZ, this->_densityMapCorCoords[coordPos][2] );
                        minZ                  = std::min ( minZ, this->_densityMapCorCoords[coordPos][2] );
                    }
                    maxXTot                   = std::max ( maxXTot, this->_densityMapCorCoords[coordPos][0] );
                    minXTot                   = std::min ( minXTot, this->_densityMapCorCoords[coordPos][0] );
                    maxYTot                   = std::max ( maxYTot, this->_densityMapCorCoords[coordPos][1] );
                    minYTot                   = std::min ( minYTot, this->_densityMapCorCoords[coordPos][1] );
                    maxZTot                   = std::max ( maxZTot, this->_densityMapCorCoords[coordPos][2] );
                    minZTot                   = std::min ( minZTot, this->_densityMapCorCoords[coordPos][2] );
                }
            }
        }
        
        //==================================== If masking did nothing, submit full map
        if ( ( ( maxX == maxXTot ) && ( minX == minXTot ) ) &&
             ( ( maxY == maxYTot ) && ( minY == minYTot ) ) &&
             ( ( maxZ == maxZTot ) && ( minZ == minZTot ) ) )
        {
            //================================ Done
            this->_phaseRemoved               = true;
            this->_keepOrRemove               = true;
            this->_usePhase                   = true;
            
            return ;
        }
        
        //==================================== If rotating map, do not change it!
        if ( rotDefaults || overlapDefaults )
        {
            //================================ Done
            this->_phaseRemoved               = true;
            this->_keepOrRemove               = true;
            this->_usePhase                   = true;
            
            return ;
        }
        
        //==================================== Calculate the grid for only the shape
        int newUStart                         = std::floor ( ( ( minX - minXTot ) / ( maxXTot - minXTot ) ) * ( this->_maxMapU + 1 ) );
        int newUEnd                           = std::ceil  ( ( ( maxX - minXTot ) / ( maxXTot - minXTot ) ) * ( this->_maxMapU + 1 ) );
        int newVStart                         = std::floor ( ( ( minY - minYTot ) / ( maxYTot - minYTot ) ) * ( this->_maxMapV + 1 ) );
        int newVEnd                           = std::ceil  ( ( ( maxY - minYTot ) / ( maxYTot - minYTot ) ) * ( this->_maxMapV + 1 ) );
        int newWStart                         = std::floor ( ( ( minZ - minZTot ) / ( maxZTot - minZTot ) ) * ( this->_maxMapW + 1 ) );
        int newWEnd                           = std::ceil  ( ( ( maxZ - minZTot ) / ( maxZTot - minZTot ) ) * ( this->_maxMapW + 1 ) );
        
        int newURange                         = std::max ( std::abs ( newUStart - static_cast<int>( this->_maxMapU + 1 )/2 ), std::abs ( newUEnd - static_cast<int>( this->_maxMapU + 1 )/2 ) );
        int newVRange                         = std::max ( std::abs ( newVStart - static_cast<int>( this->_maxMapV + 1 )/2 ), std::abs ( newVEnd - static_cast<int>( this->_maxMapV + 1 )/2 ) );
        int newWRange                         = std::max ( std::abs ( newWStart - static_cast<int>( this->_maxMapW + 1 )/2 ), std::abs ( newWEnd - static_cast<int>( this->_maxMapW + 1 )/2 ) );
        
        newUStart                             = ( this->_maxMapU + 1 )/2 - newURange;
        newUEnd                               = ( this->_maxMapU + 1 )/2 + newURange;
        newVStart                             = ( this->_maxMapV + 1 )/2 - newVRange;
        newVEnd                               = ( this->_maxMapV + 1 )/2 + newVRange;
        newWStart                             = ( this->_maxMapW + 1 )/2 - newWRange;
        newWEnd                               = ( this->_maxMapW + 1 )/2 + newWRange;
        
        newURange                             = newURange * 2 + 1;
        newVRange                             = newVRange * 2 + 1;
        newWRange                             = newWRange * 2 + 1;
        
        //==================================== Create new map and coords structures
        double* newMapHlp                     = new double [newURange * newVRange * newWRange];
        std::array<double,3>* newMapCoords    = new std::array<double,3> [newURange * newVRange * newWRange];
        
        //==================================== Copy map portion to new structures
        unsigned int newUIt, newVIt, newWIt;
        
        newUIt                                = 0;
        for ( uIt = 0; uIt < hlpU; uIt++ )
        {
            if ( ( uIt < newUStart ) || ( uIt > newUEnd ) ) { continue; }
            
            newVIt                            = 0;
            for ( vIt = 0; vIt < hlpV; vIt++ )
            {
                if ( ( vIt < newVStart ) || ( vIt > newVEnd ) ) { continue; }
                
                newWIt                        = 0;
                for ( wIt = 0; wIt < hlpW; wIt++ )
                {
                    if ( ( wIt < newWStart ) || ( wIt > newWEnd ) ) { continue; }
                    
                    coordPos                  = wIt    + hlpW * ( vIt     + hlpV * uIt    );
                    arrayPos                  = newWIt + newWRange * ( newVIt  + newVRange * newUIt );
                    
                    newMapHlp[arrayPos]       = this->_densityMapCor[coordPos];
                    newMapCoords[arrayPos][0] = this->_densityMapCorCoords[coordPos][0];
                    newMapCoords[arrayPos][1] = this->_densityMapCorCoords[coordPos][1];
                    newMapCoords[arrayPos][2] = this->_densityMapCorCoords[coordPos][2];
                    
                    newWIt                   += 1;
                }
                
                newVIt                       += 1;
            }
            
            newUIt                           += 1;
        }
        
        //==================================== Delete old structures
        delete this->_densityMapCor;
        delete this->_densityMapCorCoords;
        this->_densityMapCor                  = nullptr;
        this->_densityMapCorCoords            = nullptr;
        
        //==================================== Copy
        this->_densityMapCor                  = newMapHlp;
        newMapHlp                             = nullptr;
        this->_densityMapCorCoords            = newMapCoords;
        newMapCoords                          = nullptr;
        
        //==================================== Re-set internal variables
        this->_xRange                         = ( static_cast<double> (newURange - 1) / static_cast<double> ( this->_maxMapU ) ) * this->_xRange;
        this->_yRange                         = ( static_cast<double> (newVRange - 1) / static_cast<double> ( this->_maxMapV ) ) * this->_yRange;
        this->_zRange                         = ( static_cast<double> (newWRange - 1) / static_cast<double> ( this->_maxMapW ) ) * this->_zRange;
        this->_maxMapRange                    = std::max ( this->_xRange, std::max( this->_yRange, this->_zRange ) );
        
        this->_maxMapU                        = newURange - 1;
        this->_maxMapV                        = newVRange - 1;
        this->_maxMapW                        = newWRange - 1;
        
        //==================================== Automatically determine maximum number of shells
        this->_shellPlacement                 = std::vector<double> ( std::floor ( this->_maxMapRange / this->_shellSpacing ) );
        
        for ( unsigned int shellIter = 0; shellIter < static_cast<unsigned int> ( this->_shellPlacement.size() ); shellIter++ )
        {
            this->_shellPlacement.at(shellIter) = (shellIter+1) * this->_shellSpacing;
        }
        
        //==================================== Determine sampling ranges
        this->_xSamplingRate                  = this->_xRange / static_cast<double> ( this->_maxMapU );
        this->_ySamplingRate                  = this->_yRange / static_cast<double> ( this->_maxMapV );
        this->_zSamplingRate                  = this->_zRange / static_cast<double> ( this->_maxMapW );
        double maxSamplingRate                = std::max ( this->_xSamplingRate, std::max( this->_ySamplingRate, this->_zSamplingRate ) );
        
        //==================================== Set sampling according to user set resolution and not the real map sampling
        if ( ( this->_mapResolution / 2.0 ) > maxSamplingRate )
        {
            maxSamplingRate                   = ( this->_mapResolution / 2.0 );
        }
        
        //==================================== If bandwidth is not given, determine it
        if ( !this->_wasBandwithGiven )
        {
            *bandwidth                        =  std::ceil ( std::ceil ( ( ( std::ceil ( this->_maxMapRange / this->_shellSpacing ) * this->_shellSpacing ) / maxSamplingRate ) * 2.0 ) / 2.0 );
        }
        if ( settings->maxRotError != 0 )
        {
            *bandwidth                        = static_cast<unsigned int> ( 180 / settings->maxRotError );
            this->_wasBandwithGiven           = true;
        }
        
        //==================================== If theta and phi are not given, determine them
        if ( !this->_wasThetaGiven ) { *theta = 2 * (*bandwidth); this->_thetaAngle = *theta; }
        if ( !this->_wasPhiGiven   ) { *phi   = 2 * (*bandwidth); this->_phiAngle   = *phi;   }
        
        //==================================== If Gauss-Legendre integration order is not given, decide it
        double distPerPointFraction;
        if ( !this->_wasGlInterGiven )
        {
            distPerPointFraction              = static_cast<double> ( this->_shellSpacing ) / ( this->_maxMapRange / 2.0 );
            
            for ( unsigned int iter           = 2; iter < static_cast<unsigned int> ( ProSHADE_internal_misc::glIntMaxDists.size() ); iter++ )
            {
                if ( ProSHADE_internal_misc::glIntMaxDists.at(iter) >= distPerPointFraction )
                {
                    *glIntegOrder             = iter;
                }
            }
        }
    }
    
    //======================================== Set internal values
    this->_bandwidthLimit                     = *bandwidth;
    this->_thetaAngle                         = static_cast<double> ( *theta );
    this->_phiAngle                           = static_cast<double> ( *phi );

    //======================================== Free allocated memory
    delete[] tmpOut;
    delete[] tmpIn;
    
    //======================================== Done
    this->_phaseRemoved                       = true;
    this->_usePhase                           = true;
    this->_keepOrRemove                       = true;
    
}

/*! \brief This is the main information output for the ProSHADE_mapFeatures class.
 
 This function prints all the information gathered about the map by the ProSHADE_mapFeatures class onto
 the screen into a formatted table.
 
 \param[in] verbose Int value stating how quiet or loud the function should be. In this case, it only affects whether the header for the results table is printed, but the resular are printed in any case.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_mapFeatures::printInfo ( int verbose )
{
    if ( verbose > 0 )
    {
        std::cout << "-----------------------------------------------------------" << std::endl;
        std::cout << "|                          RESULTS                        |" << std::endl;
        std::cout << "-----------------------------------------------------------" << std::endl << std::endl;
    }
    
    if ( verbose > -1 )
    {
        printf ( "File name:              %s\n", this->one->_inputFileName.c_str() );
        printf ( "\n" );
        printf ( "Cell dimmensions (A):   %+.1f x %+.1f x %+.1f\n", this->origRanges[0], this->origRanges[1], this->origRanges[2] );
        printf ( "Cell U, V and W:        %+.1f x %+.1f x %+.1f\n", this->origDims[0]+1, this->origDims[1]+1, this->origDims[2]+1 );
        printf ( "\n" );
        printf ( "Min/Max density:        %+.3f / %+.3f\n", this->minDensPreNorm, this->maxDensPreNorm );
        printf ( "Density mean:           %+.3f\n", this->one->_mapMean );
        printf ( "Density std. dev:       %+.3f\n", this->one->_mapSdev );
        printf ( "\n" );
        printf ( "Min/Max norm density:   %+.3f / %+.3f\n", this->minDensPostNorm, this->maxDensPostNorm );
        printf ( "Norm. density mean:     %+.3f\n", this->postNormMean );
        printf ( "Norm. density std. dev: %+.3f\n", this->postNormSdev );
        printf ( "\n" );
        printf ( "Total volume (A^3):     %+.1f\n", this->totalVolume );
        printf ( "Mask volume (A^3):      %+.1f\n", this->maskVolume );
        printf ( "\n" );
        printf ( "Min/Max mask density:   %+.3f / %+.3f\n", this->maskMin, this->maskMax );
        printf ( "Mask density mean:      %+.3f\n", this->maskMean );
        printf ( "Mask density std. dev:  %+.3f\n", this->maskSdev );
        printf ( "\n" );
        printf ( "All density RMS:        %+.3f\n", this->allDensityRMS );
        printf ( "Mask density RMS:       %+.3f\n", this->maskDensityRMS );
        printf ( "\n\n" );
    }
    
}

/*! \brief This function takes a map and a threshold on it and proceeds to detect islands (continuous fragments of density above the threshold not bordering any other such region).
 
 This function takes the map dimmensions, the map and a threshold on it and proceeds to index the map into above and below threshold. It
 then iterates through the map, checking all the neighbour. If a neighbour is above threshold, it assigns the same island index and otherwise
 it leaves it alone. It then deals with boardering islands and joins these into a single one. Thus, the resulting array is the array of islands
 boardered only by values under the threshold. This is basically the Watershed algorithm with some twists in the end.
 
 \param[in] hlpU The map X dimmension.
 \param[in] hlpV The map Y dimmension.
 \param[in] hlpW The map Z dimmension.
 \param[in] map A pointer to an array with all the map values.
 \param[in] threshold A threshold value under which the map values are considered background.
 \param[out] X A vector of islands as detected by the procedure.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::vector<int> > ProSHADE_internal::ProSHADE_data::findMapIslands ( int hlpU,
                                                                                   int hlpV,
                                                                                   int hlpW,
                                                                                   double *map,
                                                                                   double threshold )
{
    //======================================== Initialise local variables
    int newU, newV, newW, coordPos, arrayPos;
    int uIt, vIt, wIt;
    
    //======================================== Island detection:
    std::vector< std::array<int,5> > clMap ( hlpU * hlpV * hlpW );
    std::array<int,5> hlpArr;
    for ( uIt = 0; uIt < hlpU; uIt++ )
    {
        for ( vIt = 0; vIt < hlpV; vIt++ )
        {
            for ( wIt = 0; wIt < hlpW; wIt++ )
            {
                hlpArr[0]                     = uIt;
                hlpArr[1]                     = vIt;
                hlpArr[2]                     = wIt;
                hlpArr[3]                     = wIt  + hlpW * ( vIt  + hlpV * uIt  );
                
                if ( map[hlpArr[3]] <= threshold )
                {
                    hlpArr[4]                 = -999;
                }
                else
                {
                    hlpArr[4]                 = -1;
                }
                clMap.at(hlpArr[3])           = hlpArr;
            }
        }
    }
    
    //======================================== Island detection algorithm
    std::vector< std::vector<int> > clusters;
    std::vector<int> unclNeigh;
    std::vector<int> clNeigh;
    std::vector<int> hlpVec;
    std::vector<int> XXXVec;
    int clusterNo                             = 0;
    bool allSame                              = false;
    int hlpVal                                = 0;
    int minCl                                 = 0;
    
    for ( uIt = 0; uIt < hlpU; uIt++ )
    {
        for ( vIt = 0; vIt < hlpV; vIt++ )
        {
            for ( wIt = 0; wIt < hlpW; wIt++ )
            {
                //============================ Find current array pos
                arrayPos                      = wIt  + hlpW * ( vIt  + hlpV * uIt  );
                
                //============================ If zero, ignore
                if ( clMap.at(arrayPos)[4] == -999 ) { continue; }
                
                //============================ Check neighbours
                unclNeigh.clear               ( );
                clNeigh.clear                 ( );
                for ( int uCh = -1; uCh < 2; uCh++ )
                {
                    for ( int vCh = -1; vCh < 2; vCh++ )
                    {
                        for ( int wCh = -1; wCh < 2; wCh++ )
                        {
                            if ( ( uCh == 0 ) && ( vCh == 0 ) && ( wCh == 0 ) ) { continue; }
                            
                            newU              = static_cast<int> ( uIt ) + uCh;
                            newV              = static_cast<int> ( vIt ) + vCh;
                            newW              = static_cast<int> ( wIt ) + wCh;
                            
                            if ( newU < 0 ) { newU += hlpU; } if ( newU >= static_cast<int> ( hlpU ) ) { newU -= hlpU; }
                            if ( newV < 0 ) { newV += hlpV; } if ( newV >= static_cast<int> ( hlpV ) ) { newV -= hlpV; }
                            if ( newW < 0 ) { newW += hlpW; } if ( newW >= static_cast<int> ( hlpW ) ) { newW -= hlpW; }
                            
                            coordPos          = newW + hlpW * ( newV  + hlpV * newU );
                            
                            //================ If zero, ignore
                            if ( clMap.at(coordPos)[4] == -999 ) { continue; }
                            
                            //================ If not clustered to anyone
                            if ( clMap.at(coordPos)[4] == -1 )
                            {
                                unclNeigh.emplace_back ( coordPos );
                                continue;
                            }
                            
                            clNeigh.emplace_back ( coordPos );
                        }
                    }
                }
                
                //============================ If single point
                if ( ( unclNeigh.size() == 0 ) && ( clNeigh.size() == 0 ) )
                {
                    //======================== Assign into new cluster
                    clMap.at(arrayPos)[4]     = clusterNo;
                    
                    unclNeigh.emplace_back    ( arrayPos );
                    clusters.emplace_back     ( unclNeigh );
                    unclNeigh.clear           ( );
                    
                    clusterNo                += 1;
                    continue;
                }
                
                //============================ If only unclustered neighbours
                if ( ( unclNeigh.size() > 0 ) && ( clNeigh.size() == 0 ) )
                {
                    //======================== Assign all new cluster
                    clMap.at(arrayPos)[4] = clusterNo;
                    
                    clNeigh.emplace_back      ( arrayPos );
                    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( unclNeigh.size() ); iter++ )
                    {
                        clNeigh.emplace_back  ( clMap.at(unclNeigh.at(iter))[3] );
                        clMap.at(unclNeigh.at(iter))[4] = clusterNo;
                    }
                    
                    clusters.emplace_back     ( clNeigh );
                    clNeigh.clear             ( );
                    
                    clusterNo                += 1;
                    continue;
                }
                
                //============================ If some clustered neighbours
                if ( clNeigh.size() > 0 )
                {
                    allSame                   = true;
                    hlpVal                    = clMap.at(clNeigh.at(0))[4];
                    for ( unsigned int iter = 1; iter < static_cast<unsigned int> ( clNeigh.size() ); iter++ )
                    {
                        if ( clMap.at(clNeigh.at(iter))[4] != hlpVal )
                        {
                            allSame           = false;
                        }
                    }
                    
                    if ( allSame )
                    {
                        //==================== Assign this point to the same cluster as neighbours
                        if ( clMap.at(arrayPos)[4] == -1 )
                        {
                            clMap.at(arrayPos)[4] = clMap.at(clNeigh.at(0))[4];
                            clusters.at(clMap.at(clNeigh.at(0))[4]).emplace_back ( arrayPos );
                        }
                        
                        //==================== Assign all yet unclustered neighbours also
                        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( unclNeigh.size() ); iter++ )
                        {
                            clusters.at(clMap.at(clNeigh.at(0))[4]).emplace_back ( clMap.at(unclNeigh.at(iter))[3] );
                            clMap.at(unclNeigh.at(iter))[4] = clMap.at(clNeigh.at(0))[4];
                        }
                        continue;
                    }
                    else
                    {
                        //==================== This is where two clusters meet. Start by finding the smalles cluster (smallest ID)
                        hlpVec.clear          ( );
                        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( clNeigh.size() ); iter++ )
                        {
                            hlpVec.emplace_back ( clMap.at(clNeigh.at(iter))[4] );
                        }
                        
                        std::sort             ( hlpVec.begin(), hlpVec.end() );
                        hlpVec.erase          ( std::unique( hlpVec.begin(), hlpVec.end() ), hlpVec.end() );
                        
                        minCl                 = hlpVec.at(0);
                        
                        //==================== Now, enter this point to the cluster
                        if ( clMap.at(arrayPos)[4] == -1 )
                        {
                            clMap.at(arrayPos)[4] = minCl;
                            clusters.at(minCl).emplace_back ( arrayPos );
                        }
                        
                        //==================== Then, copy all unclustered points to the smallest ID cluster
                        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( unclNeigh.size() ); iter++ )
                        {
                            clMap.at(unclNeigh.at(iter))[4] = minCl;
                            clusters.at(minCl).emplace_back ( unclNeigh.at(iter) );
                        }
                        
                        //==================== And copy all other clusters to the one with smallest ID
                        for ( unsigned int iter = 1; iter < static_cast<unsigned int> ( hlpVec.size() ); iter++ )
                        {
                            for ( unsigned int it = 0; it < static_cast<unsigned int> ( clusters.at(hlpVec.at(iter)).size() ); it++ )
                            {
                                clMap.at(clusters.at(hlpVec.at(iter)).at(it))[4] = minCl;
                                clusters.at(minCl).emplace_back ( clusters.at(hlpVec.at(iter)).at(it) );
                            }
                            clusters.at(hlpVec.at(iter)).clear();
                        }
                    }
                }
            }
        }
    }
    
    //======================================== Find best clusters
    std::vector< std::array<double,3> > clVec;
    std::array<double,3> hArr;
    for ( unsigned int clIt = 0; clIt < static_cast<unsigned int> ( clusters.size() ); clIt++ )
    {
        if ( clusters.at(clIt).size() > 1 )
        {
            unsigned int minU                 = clMap.at(clusters.at(clIt).at(0))[0];
            unsigned int maxU                 = clMap.at(clusters.at(clIt).at(0))[0];
            unsigned int minV                 = clMap.at(clusters.at(clIt).at(0))[1];
            unsigned int maxV                 = clMap.at(clusters.at(clIt).at(0))[1];
            unsigned int minW                 = clMap.at(clusters.at(clIt).at(0))[2];
            unsigned int maxW                 = clMap.at(clusters.at(clIt).at(0))[2];
            for ( unsigned int el = 0; el < static_cast<unsigned int> ( clusters.at(clIt).size() ); el++ )
            {
                minU                          = std::min ( minU, static_cast<unsigned int> ( clMap.at(clusters.at(clIt).at(el))[0] ) );
                maxU                          = std::max ( maxU, static_cast<unsigned int> ( clMap.at(clusters.at(clIt).at(el))[0] ) );
                minV                          = std::min ( minV, static_cast<unsigned int> ( clMap.at(clusters.at(clIt).at(el))[1] ) );
                maxV                          = std::max ( maxV, static_cast<unsigned int> ( clMap.at(clusters.at(clIt).at(el))[1] ) );
                minW                          = std::min ( minW, static_cast<unsigned int> ( clMap.at(clusters.at(clIt).at(el))[2] ) );
                maxW                          = std::max ( maxW, static_cast<unsigned int> ( clMap.at(clusters.at(clIt).at(el))[2] ) );
            }
            double encVol                     = ( maxU - minU + 1 ) * ( maxV - minV + 1 ) * ( maxW - minW + 1 );
            hArr[0]                           = encVol / static_cast<double> ( clusters.at(clIt).size() );
            hArr[1]                           = static_cast<double> ( clusters.at(clIt).size() );
            hArr[2]                           = clIt;
            clVec.emplace_back                ( hArr );
        }
    }
    std::sort                                 ( clVec.begin(), clVec.end(), [](const std::array<double,3>& a, const std::array<double,3>& b) { return a[1] > b[1]; });
    
    std::vector<double> hlpVals;
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( clVec.size() ); iter++ )
    {
        hlpVals.emplace_back                  ( clVec.at(iter)[1] );
    }
    
    double mean                               = std::accumulate ( hlpVals.begin(), hlpVals.end(), 0.0 ) / static_cast<double> ( hlpVals.size() );
    double sd                                 = sqrt ( std::inner_product( hlpVals.begin(), hlpVals.end(), hlpVals.begin(), 0.0 ) / static_cast<double> ( hlpVals.size() ) - mean * mean );
    double thres                              = 0.0;
    
    std::vector< std::array<double,3> > hVec;
    for ( double iter = 5.0; iter >= 0.0; iter = iter - 0.5 )
    {
        thres                                 = mean + ( iter * sd );
        hVec.clear                            ( );
        
        for ( unsigned int it = 0; it < static_cast<unsigned int> ( clVec.size() ); it++ )
        {
            if ( clVec.at(it)[1] >= thres )
            {
                hArr[0]                       = clVec.at(it)[0];
                hArr[1]                       = clVec.at(it)[2];
                hVec.emplace_back             ( hArr );
            }
        }
        
        if ( ( hVec.size() > std::max ( std::floor ( clVec.size() * 0.05 ), 10.0 ) ) && ( hVec.size() < clVec.size() ) )
        {
            break;
        }
    }
    
    std::sort                                 ( hVec.begin(), hVec.end(), [](const std::array<double,3>& a, const std::array<double,3>& b) { return a[0] < b[0]; });
    std::vector< std::vector<int> > clusts;
    
    hlpVals.clear                             ( );
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( hVec.size() ); iter++ )
    {
        hlpVals.emplace_back                  ( hVec.at(iter)[0] );
    }
    
    if ( hVec.size() == 0 )
    {
        return ( clusts );
    }
    if ( hVec.size() < 5 )
    {
        mean                                  = hVec.at(hVec.size()-1)[0] + 0.0001;
        sd                                    = mean / 12.0;
    }
    else
    {
        mean                                  = std::accumulate ( hlpVals.begin(), hlpVals.end(), 0.0 ) / static_cast<double> ( hlpVals.size() );
        sd                                    = sqrt ( std::inner_product( hlpVals.begin(), hlpVals.end(), hlpVals.begin(), 0.0 ) / static_cast<double> ( hlpVals.size() ) - mean * mean );
    }
    
    for ( double it = 3.0; it >= 0.0; it = it - 0.25 )
    {
        thres                                 = mean - ( it * sd );
        clusts.clear                          ( );
        
        if ( it == 0.0 )
        {
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( hVec.size() ); iter++ )
            {
                thres                         = std::max ( hVec.at(iter)[0], thres );
            }
        }

        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( hVec.size() ); iter++ )
        {
            if ( hVec.at(iter)[0] <= thres )
            {
                clusts.emplace_back           ( clusters.at(hVec.at(iter)[1]) );
            }
        }
        
        if ( ( clusts.size() > 0 ) && ( clusts.size() < 5 ) )
        {
            if ( ( clusts.size() ) < hVec.size() )
            {
                if ( ( hVec.at(clusts.size()-1)[0] > 3.0 ) && ( ( 2.0 * hVec.at(clusts.size()-1)[0] ) < hVec.at(clusts.size())[0] ) )
                {
                    break;
                }
            }
        }

        if ( ( clusts.size() > std::max ( std::floor ( hVec.size() * 0.1 ), 5.0 ) ) && ( clusts.size() < hVec.size() ) ) { break; }
    }

    //======================================== Done
    return ( clusts );
    
}

/*! \brief This function takes the clear map produced already and fragments it into overlapping boxes of given size.
 
 This function takes a map and fragments it into boxes of given size (in Angstroms). These boxes do overlap by 1/2 and the final
 box of each dimmension may be larger due to the map dimmensions not being perfectly divisible by the required box size, if such
 is the case. The resulting boxes are checked for containg given fraction of density and then printed into files.
 
 \param[in] axOrder The order of axes which should be used to save the fragments.
 \param[in] verbose Int value stating how loud in the standard output the function should be.
 
 \warning This is an internal function which should not be used by the user. The user should set the parameters and it will be done automatically.
 */
void ProSHADE_internal::ProSHADE_mapFeatures::fragmentMap ( std::string axOrder,
                                                            int verbose,
                                                            ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Sanity check
    if ( this->fragBoxSize <= 0.0 )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Tried to fragment the map to boxes of size " << this->fragBoxSize << " . Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "The box size to which the map should be fragmented in not in the allowed range ( " << this->fragBoxSize << " )" << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    if ( verbose > 0 )
    {
        std::cout << "-----------------------------------------------------------" << std::endl;
        std::cout << "|                      MAP FRAGMENTATION                  |" << std::endl;
        std::cout << "-----------------------------------------------------------" << std::endl << std::endl;
    }
    
    //======================================== Find X, Y and Z starts and ends of the required boxes
    std::vector< std::array<unsigned int,2> > XDim;
    std::vector< std::array<unsigned int,2> > YDim;
    std::vector< std::array<unsigned int,2> > ZDim;
    std::array<unsigned int,2> hlpArrX;
    std::array<unsigned int,2> hlpArrY;
    std::array<unsigned int,2> hlpArrZ;
    double maxMapAngPerIndex                  = std::max ( this->one->_xRange / static_cast<double> ( this->one->_maxMapU ),
                                                           std::max ( this->one->_yRange / static_cast<double> ( this->one->_maxMapV ),
                                                                      this->one->_zRange / static_cast<double> ( this->one->_maxMapW ) ) );
    unsigned int boxDimInIndices              = static_cast<unsigned int> ( std::round ( this->fragBoxSize / maxMapAngPerIndex ) );
    if ( ( boxDimInIndices % 2 ) == 1 )
    {
        //==================================== If odd, change to even
        boxDimInIndices         += 1;
    }
    
    //======================================== Problem! The box is too small
    if ( boxDimInIndices < 2 )
    {
        std::cerr << "!!! ProSHADE ERROR !!! The box size for fragmentation is too small, please increase the box size or submit more detailed map. (Increasing the number of points by interpolation could work without changing the map here...)" << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "The box size for fragmentation is too small, please increase the box size or submit more detailed map. (Increasing the number of points by interpolation could work without changing the map here...)." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    if ( ( boxDimInIndices >= this->one->_maxMapU ) || ( boxDimInIndices >= this->one->_maxMapV ) || ( boxDimInIndices >= this->one->_maxMapW ) )
    {
        std::cerr << "!!! ProSHADE ERROR !!! The box size for fragmentation is too large, please decrease the box size so that it is smaller than the map dimmensions." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "The box size for fragmentation is too large, please decrease the box size so that it is smaller than the map dimmensions." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }

    //======================================== Generate box starts and ends
    for ( unsigned int xStart = 0; xStart <= ( this->one->_maxMapU - boxDimInIndices ); xStart += ( boxDimInIndices / 2 ) )
    {
        hlpArrX[0]                            = xStart;
        hlpArrX[1]                            = xStart + boxDimInIndices;
        
        //==================================== Check for terminal boxes, they may need to have larger size
        if ( ( xStart + ( boxDimInIndices / 2 ) ) > ( this->one->_maxMapU - boxDimInIndices ) )
        {
            //================================ This is the last X
            hlpArrX[1]                        = this->one->_maxMapU;
        }
        
        for ( unsigned int yStart = 0; yStart <= ( this->one->_maxMapV - boxDimInIndices ); yStart += ( boxDimInIndices / 2 ) )
        {
            hlpArrY[0]                        = yStart;
            hlpArrY[1]                        = yStart + boxDimInIndices;
            
            //================================ Check for terminal boxes, they may need to have larger size
            if ( ( yStart + ( boxDimInIndices / 2 ) ) > ( this->one->_maxMapV - boxDimInIndices ) )
            {
                //============================ This is the last Y
                hlpArrY[1]                    = this->one->_maxMapV;
            }
            
            for ( unsigned int zStart = 0; zStart <= ( this->one->_maxMapW - boxDimInIndices ); zStart += ( boxDimInIndices / 2 ) )
            {
                hlpArrZ[0]                    = zStart;
                hlpArrZ[1]                    = zStart + boxDimInIndices;
                
                //============================ Check for terminal boxes, they may need to have larger size
                if ( ( zStart + ( boxDimInIndices / 2 ) ) > ( this->one->_maxMapW - boxDimInIndices ) )
                {
                    //======================== This is the last Z
                    hlpArrZ[1]                = this->one->_maxMapW;
                }
                
                XDim.emplace_back             ( hlpArrX );
                YDim.emplace_back             ( hlpArrY );
                ZDim.emplace_back             ( hlpArrZ );
            }
        }
    }
    
    if ( verbose > 2 )
    {
        std::cout << ">> Box borders generated." << std::endl;
    }
    
    //======================================== If the box has enough density, write it out!
    unsigned int noDensityPoints              = 0;
    unsigned int arrayPos                     = 0;
    unsigned int hlpPos                       = 0;
    int fileIter                              = 0;
    double boxVolume                          = 0.0;
    int newU, newV, newW;
    std::string fileName;
    
    for ( unsigned int boxIt = 0; boxIt < static_cast<unsigned int> ( XDim.size() ); boxIt++ )
    {
        //==================================== Check the box density fraction
        noDensityPoints                       = 0;
        for ( unsigned int x = XDim.at(boxIt)[0]; x <= XDim.at(boxIt)[1]; x++ )
        {
            for ( unsigned int y = YDim.at(boxIt)[0]; y <= YDim.at(boxIt)[1]; y++ )
            {
                for ( unsigned int z = ZDim.at(boxIt)[0]; z <= ZDim.at(boxIt)[1]; z++ )
                {
                    arrayPos                  = z  + (this->one->_maxMapW+1) * ( y  + (this->one->_maxMapV+1) * x  );
                    
                    if ( this->one->_densityMapCor[arrayPos] > 0.0 )
                    {
                        noDensityPoints      += 1;
                    }
                }
            }
        }
        
        //==================================== If passing, write out the box
        boxVolume                             = ( ( XDim.at(boxIt)[1] - XDim.at(boxIt)[0] ) + 1 ) * ( ( YDim.at(boxIt)[1] - YDim.at(boxIt)[0] ) + 1 ) * ( ( ZDim.at(boxIt)[1] - ZDim.at(boxIt)[0] ) + 1 );
        
        if ( ( static_cast<double> ( noDensityPoints ) / boxVolume ) > this->mapFragBoxFraction )
        {
            //================================ Solve file names
            fileName                          = this->mapFragName + std::to_string ( fileIter ) + ".map";
            this->fragFiles.emplace_back      ( fileName );
            fileIter                         += 1;
            if ( verbose > 3 )
            {
                std::cout << ">>>>> Writing out box " << fileIter-1 << std::endl;
            }
            
            //================================ Create map fragment
            float *boxMap                     = nullptr;            
            boxMap                            = (float*) malloc ( (XDim.at(boxIt)[1]-XDim.at(boxIt)[0]+1) * (YDim.at(boxIt)[1]-YDim.at(boxIt)[0]+1) * (ZDim.at(boxIt)[1]-ZDim.at(boxIt)[0]+1) * sizeof ( float ) );
            if ( boxMap == nullptr )
            {
                std::cerr << "!!! ProSHADE ERROR !!! Cannot allocate memory for map data. Terminating... " << std::endl;
                
                if ( settings->htmlReport )
                {
                    std::stringstream hlpSS;
                    hlpSS << "<font color=\"red\">" << "Cannot allocate memory. Could you have run out of memory?" << "</font>";
                    rvapi_set_text                    ( hlpSS.str().c_str(),
                                                       "ProgressSection",
                                                       settings->htmlReportLineProgress,
                                                       1,
                                                       1,
                                                       1 );
                    settings->htmlReportLineProgress += 1;
                    rvapi_flush                       ( );
                }
                
                exit ( -1 );
            }

            for ( unsigned int x = XDim.at(boxIt)[0]; x <= XDim.at(boxIt)[1]; x++ )
            {
                for ( unsigned int y = YDim.at(boxIt)[0]; y <= YDim.at(boxIt)[1]; y++ )
                {
                    for ( unsigned int z = ZDim.at(boxIt)[0]; z <= ZDim.at(boxIt)[1]; z++ )
                    {
                        arrayPos              = z  + (this->one->_maxMapW+1) * ( y  + (this->one->_maxMapV+1) * x  );
                        
                        newU                  = x - XDim.at(boxIt)[0];
                        newV                  = y - YDim.at(boxIt)[0];
                        newW                  = z - ZDim.at(boxIt)[0];
                        hlpPos                = newW  + (ZDim.at(boxIt)[1]-ZDim.at(boxIt)[0]) * ( newV  + (YDim.at(boxIt)[1]-YDim.at(boxIt)[0]) * newU );
                        
                        boxMap[hlpPos]        = this->one->_densityMapCor[arrayPos];
                    }
                }
            }
            
            float xFrom                       = XDim.at(boxIt)[0] + this->one->_xFrom;
            float xTo                         = XDim.at(boxIt)[1] + this->one->_xFrom;
            float yFrom                       = YDim.at(boxIt)[0] + this->one->_yFrom;
            float yTo                         = YDim.at(boxIt)[1] + this->one->_yFrom;
            float zFrom                       = ZDim.at(boxIt)[0] + this->one->_zFrom;
            float zTo                         = ZDim.at(boxIt)[1] + this->one->_zFrom;
            
            this->one->writeMap               ( fileName.c_str(),
                                                boxMap,
                                                axOrder,
                                                xFrom,
                                                yFrom,
                                                zFrom,
                                                xTo,
                                                yTo,
                                                zTo,
                                                ( this->one->_xSamplingRate * ( XDim.at(boxIt)[1] - XDim.at(boxIt)[0] ) ),
                                                ( this->one->_ySamplingRate * ( YDim.at(boxIt)[1] - YDim.at(boxIt)[0] ) ),
                                                ( this->one->_zSamplingRate * ( ZDim.at(boxIt)[1] - ZDim.at(boxIt)[0] ) ) );

            free                              ( boxMap );
            boxMap                            = nullptr;
        }
    }
    
    if ( verbose > 0 )
    {
        if ( fileIter > 0 )
        {
            std::cout << ">> " << fileIter << " boxes written in " << this->mapFragName << "xx ." << std::endl << std::endl;
        }
        else
        {
            std::cout << ">> No boxes conform to your criteria. NO BOXES WRITTEN." << std::endl << std::endl;
        }
    }
    if ( fileIter == 0 )
    {
        std::cout << "!!! ProSHADE WARNING !!! No map fragments produced - no box passed the required criteria." << std::endl << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"orange\">" << "No map fragments produced - no box passed the required criteria.." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
    }
    
    //======================================== Note that fragmentation was attempted
    this->fragFilesExist                      = true;
    
    //======================================== Done
    return ;
    
}

/*! \brief This function takes the clear map produced already and fragments it into overlapping boxes of given size.
 
 This function takes a map and fragments it into boxes of given size (in Angstroms). These boxes do overlap by 1/2 and the final
 box of each dimmension may be larger due to the map dimmensions not being perfectly divisible by the required box size, if such
 is the case. The resulting boxes are checked for containg given fraction of density and then printed into files.
 
 \param[in] settings An instance of the ProSHADE_settings class containing all the values required for the function to make decisions.
 \param[in] xTranslate A variable to which the amount of translation along X-axis required to match the model to the outputted map is saved.
 \param[in] yTranslate A variable to which the amount of translation along Y-axis required to match the model to the outputted map is saved.
 \param[in] zTranslate A variable to which the amount of translation along Z-axis required to match the model to the outputted map is saved.
 
 \warning This is an internal function which should not be used by the user. The user should set the parameters and it will be done automatically.
 */
void ProSHADE_internal::ProSHADE_mapFeatures::dealWithHalfMaps ( ProSHADE::ProSHADE_settings* settings,
                                                                 double *xTranslate,
                                                                 double *yTranslate,
                                                                 double *zTranslate )
{
    //======================================== Initialise local variables
    ProSHADE_data* halfMap1                   = new ProSHADE_data ();
    ProSHADE_data* halfMap2                   = new ProSHADE_data ();
    
    double shSpacing1                         = settings->shellSpacing;
    double shSpacing2                         = settings->shellSpacing;
    unsigned int bandwidth1                   = settings->bandwidth;
    unsigned int bandwidth2                   = settings->bandwidth;
    unsigned int theta1                       = settings->theta;
    unsigned int theta2                       = settings->theta;
    unsigned int phi1                         = settings->phi;
    unsigned int phi2                         = settings->phi;
    unsigned int glInteg1                     = settings->glIntegOrder;
    unsigned int glInteg2                     = settings->glIntegOrder;
    
    //======================================== Read in the first half map
    halfMap1->getDensityMapFromMAP            ( settings->structFiles.at(0),
                                               &shSpacing1,
                                                settings->mapResolution,
                                               &bandwidth1,
                                               &theta1,
                                               &phi1,
                                               &glInteg1,
                                               &settings->extraSpace,
                                                settings->mapResDefault,
                                                settings->rotChangeDefault,
                                                settings,
                                                settings->overlayDefaults );
    
    if ( settings->verbose > 0 )
    {
        std::cout << "Read in the first half map." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Read in the first half map." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Read in the second half map
    halfMap2->getDensityMapFromMAP            ( settings->structFiles.at(1),
                                               &shSpacing2,
                                               settings->mapResolution,
                                               &bandwidth2,
                                               &theta2,
                                               &phi2,
                                               &glInteg2,
                                               &settings->extraSpace,
                                               settings->mapResDefault,
                                               settings->rotChangeDefault,
                                               settings,
                                               settings->overlayDefaults );
    
    if ( settings->verbose > 0 )
    {
        std::cout << "Read in the second half map." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Read in the second half map." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Size and sampling checks
    bool sizeAndSampleIdentical               = true;
    if ( ( halfMap1->_xRange != halfMap2->_xRange ) || ( halfMap1->_yRange != halfMap2->_yRange ) || ( halfMap1->_zRange != halfMap2->_zRange ) )
    {
        std::cout << "!!! ProSHADE WARNING !!! The cell ranges differ between the two half maps - this is not allowed for half maps. Will attempt to re-size and re-sample the maps, but beware this may not be what you wanted. Please check the output manually." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"orange\">" << "The cell ranges differ between the two half maps - this is not allowed for half maps. Will attempt to re-size and re-sample the maps, but beware this may not be what you wanted. Please check the output manually." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        sizeAndSampleIdentical                = false;
    }
    
    if ( ( halfMap1->_maxMapU != halfMap2->_maxMapU ) || ( halfMap1->_maxMapV != halfMap2->_maxMapV ) || ( halfMap1->_maxMapW != halfMap2->_maxMapW ) )
    {
        std::cout << "!!! ProSHADE WARNING !!! The cell dimensions differ between the two half maps - this is not allowed for half maps. Will attempt to re-size and re-sample the maps, but beware this may not be what you wanted. Please check the output manually." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"orange\">" << "The cell dimensions differ between the two half maps - this is not allowed for half maps. Will attempt to re-size and re-sample the maps, but beware this may not be what you wanted. Please check the output manually." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        sizeAndSampleIdentical                = false;
    }
    
    if ( !sizeAndSampleIdentical )
    {
        if ( settings->verbose > 0 )
        {
            std::cout << "Starting the re-sampling and re-sizing procedure. This may take some time." << std::endl;
        }
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"green\">" << "Starting the re-sampling and re-sizing procedure. This may take some time." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        //==================================== Copy from initial hoder to full holder
        halfMap1->_densityMapCor              = new double[(halfMap1->_maxMapU+1)*(halfMap1->_maxMapV+1)*(halfMap1->_maxMapW+1)];
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (halfMap1->_maxMapU+1)*(halfMap1->_maxMapV+1)*(halfMap1->_maxMapW+1) ); iter++ )
        {
            halfMap1->_densityMapCor[iter]    = halfMap1->_densityMapMap[iter];
        }
        delete[] halfMap1->_densityMapMap;
        halfMap1->_densityMapMap              = nullptr;
        
        halfMap2->_densityMapCor              = new double[(halfMap2->_maxMapU+1)*(halfMap2->_maxMapV+1)*(halfMap2->_maxMapW+1)];
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (halfMap2->_maxMapU+1)*(halfMap2->_maxMapV+1)*(halfMap2->_maxMapW+1) ); iter++ )
        {
            halfMap2->_densityMapCor[iter]    = halfMap2->_densityMapMap[iter];
        }
        delete[] halfMap2->_densityMapMap;
        halfMap2->_densityMapMap              = nullptr;
        
        //==================================== Match sampling and sizes
        ProSHADE_comparePairwise* cmpObj      = new ProSHADE_comparePairwise ( halfMap2,
                                                                               halfMap2,
                                                                               settings->mPower,
                                                                               settings->ignoreLs,
                                                                               std::max ( glInteg1, glInteg2 ),
                                                                               settings );
        std::array<double,4> translationVec;
        double xMapMov, yMapMov, zMapMov;
        ProSHADE_data str1Copy                = ProSHADE_data ( halfMap1 );
        translationVec                        = cmpObj->getTranslationFunctionMap ( &str1Copy, halfMap2, &xMapMov, &yMapMov, &zMapMov );
        
        delete cmpObj;
    }
    
    //======================================== Print headers of the input files
    if ( settings->htmlReport )
    {
        if ( sizeAndSampleIdentical )
        {
            rvapi_add_section                 ( "h1Header",
                                                "Half Map 1 Header",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                false );
        }
        else
        {
            rvapi_add_section                 ( "h1Header",
                                                "Half Map 1 Header (after re-sampling)",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                false );
        }
        settings->htmlReportLine             += 1;
        
        //==================================== Print max U, V and W
        std::stringstream hlpSS;
        hlpSS << "<pre><b>" << "Rows, Columns and Sections sizes: " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        int prec                              = 6;
        int UVWMAX                            = std::max ( ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap1->_maxMapU+1 ), prec ).length(),
                                                           std::max ( ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap1->_maxMapV+1 ), prec ).length(),
                                                                      ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap1->_maxMapW+1 ), prec ).length() ) );
        int FMAX                              = std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_xFrom, prec ).length(),
                                                           std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_yFrom, prec ).length(),
                                                                      ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_zFrom, prec ).length() ) );
        int TMAX                              = std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_xTo, prec ).length(),
                                                           std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_yTo, prec ).length(),
                                                                      ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_zTo, prec ).length() ) );
        int CDMAX                             = std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_xRange, prec ).length(),
                                                           std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_yRange, prec ).length(),
                                                                      ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_zRange, prec ).length() ) );
        int spacer                            = std::max ( UVWMAX, std::max ( FMAX, std::max ( TMAX, CDMAX ) ) );
        if ( spacer < 5 ) { spacer            = 5; }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << static_cast<int> ( halfMap1->_maxMapU+1 );
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap1->_maxMapU+1 ), prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << static_cast<int> ( halfMap1->_maxMapV+1 );
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap1->_maxMapV+1 ), prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << static_cast<int> ( halfMap1->_maxMapW+1 );
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap1->_maxMapW+1 ), prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "h1Header",
                                                0,
                                                0,
                                                1,
                                                1 );
        
        //==================================== Print from and to lims
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Start and stop points on columns, rows, sections: " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << halfMap1->_xFrom;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_xFrom, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap1->_xTo;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_xTo, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap1->_yFrom;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_yFrom, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap1->_yTo;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_yTo, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << " " << halfMap1->_zFrom;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_zFrom, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap1->_zTo;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_zTo, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "h1Header",
                                                1,
                                                0,
                                                1,
                                                1 );
        
        //==================================== Print cell dimensions
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Cell dimensions (Angstrom): " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << halfMap1->_xRange;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_xRange, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap1->_yRange;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_yRange, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap1->_zRange;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_zRange, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "h1Header",
                                                2,
                                                0,
                                                1,
                                                1 );
        
        //==================================== Print cell dimensions
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Cell angles (Degrees): " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << 90.0;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( 90.0, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << 90.0;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( 90.0, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << 90.0;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( 90.0, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "h1Header",
                                                3,
                                                0,
                                                1,
                                                1 );
        
        //==================================== Print cell dimensions
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Fast, medium, slow axes: " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        std::string hlpAO                     = settings->axisOrder;
        std::transform                        ( hlpAO.begin(), hlpAO.end(), hlpAO.begin(), ::toupper);
        hlpSS << " " << hlpAO;
        for ( int iter = 0; iter < static_cast<int> ( spacer - 1 ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "h1Header",
                                                4,
                                                0,
                                                1,
                                                1 );
        
        rvapi_flush                           ( );
    }
    
    if ( settings->htmlReport )
    {
        if ( sizeAndSampleIdentical )
        {
            rvapi_add_section                 ( "h2Header",
                                                "Half Map 2 Header",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                false );
        }
        else
        {
            rvapi_add_section                 ( "h2Header",
                                                "Half Map 2 Header (after re-sampling)",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                false );
        }
        settings->htmlReportLine             += 1;
        
        //==================================== Print max U, V and W
        std::stringstream hlpSS;
        hlpSS << "<pre><b>" << "Rows, Columns and Sections sizes: " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        int prec                              = 6;
        int UVWMAX                            = std::max ( ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap2->_maxMapU+1 ), prec ).length(),
                                                          std::max ( ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap2->_maxMapV+1 ), prec ).length(),
                                                                    ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap2->_maxMapW+1 ), prec ).length() ) );
        int FMAX                              = std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_xFrom, prec ).length(),
                                                          std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_yFrom, prec ).length(),
                                                                    ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_zFrom, prec ).length() ) );
        int TMAX                              = std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_xTo, prec ).length(),
                                                          std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_yTo, prec ).length(),
                                                                    ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_zTo, prec ).length() ) );
        int CDMAX                             = std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_xRange, prec ).length(),
                                                          std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_yRange, prec ).length(),
                                                                    ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_zRange, prec ).length() ) );
        int spacer                            = std::max ( UVWMAX, std::max ( FMAX, std::max ( TMAX, CDMAX ) ) );
        if ( spacer < 5 ) { spacer            = 5; }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << static_cast<int> ( halfMap2->_maxMapU+1 );
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap2->_maxMapU+1 ), prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << static_cast<int> ( halfMap2->_maxMapV+1 );
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap2->_maxMapV+1 ), prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << static_cast<int> ( halfMap2->_maxMapW+1 );
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap2->_maxMapW+1 ), prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "h2Header",
                                                0,
                                                0,
                                                1,
                                                1 );
        
        //==================================== Print from and to lims
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Start and stop points on columns, rows, sections: " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << halfMap2->_xFrom;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_xFrom, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap2->_xTo;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_xTo, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap2->_yFrom;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_yFrom, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap2->_yTo;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_yTo, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << " " << halfMap2->_zFrom;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_zFrom, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap2->_zTo;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_zTo, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "h2Header",
                                                1,
                                                0,
                                                1,
                                                1 );
        
        //==================================== Print cell dimensions
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Cell dimensions (Angstrom): " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << halfMap2->_xRange;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_xRange, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap2->_yRange;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_yRange, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap2->_zRange;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_zRange, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "h2Header",
                                                2,
                                                0,
                                                1,
                                                1 );
        
        //==================================== Print cell dimensions
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Cell angles (Degrees): " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << 90.0;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( 90.0, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << 90.0;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( 90.0, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << 90.0;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( 90.0, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "h2Header",
                                                3,
                                                0,
                                                1,
                                                1 );
        
        //==================================== Print cell dimensions
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Fast, medium, slow axes: " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        std::string hlpAO                     = settings->axisOrder;
        std::transform                        ( hlpAO.begin(), hlpAO.end(), hlpAO.begin(), ::toupper);
        hlpSS << " " << hlpAO;
        for ( int iter = 0; iter < static_cast<int> ( spacer - 1 ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "h2Header",
                                                4,
                                                0,
                                                1,
                                                1 );
        
        rvapi_flush                           ( );
    }

    //======================================== Initialise local variables for Fourier and Inverse Fourier
    fftw_complex *tmpOutA;
    fftw_complex *tmpInA;
    fftw_complex *tmpOutB;
    fftw_complex *tmpInB;
    fftw_plan pA;
    fftw_plan pB;
    
    int hlpU                                  = halfMap1->_maxMapU + 1;
    int hlpV                                  = halfMap1->_maxMapV + 1;
    int hlpW                                  = halfMap1->_maxMapW + 1;

    //======================================== Allocate the memory
    tmpInA                                    = new fftw_complex[hlpU * hlpV * hlpW];
    tmpOutA                                   = new fftw_complex[hlpU * hlpV * hlpW];
    tmpInB                                    = new fftw_complex[hlpU * hlpV * hlpW];
    tmpOutB                                   = new fftw_complex[hlpU * hlpV * hlpW];

    //======================================== Create plans (FFTW_MEASURE would change the arrays)
    pA                                        = fftw_plan_dft_3d ( hlpU, hlpV, hlpW, tmpInA, tmpOutA, FFTW_FORWARD, FFTW_ESTIMATE );
    pB                                        = fftw_plan_dft_3d ( hlpU, hlpV, hlpW, tmpInB, tmpOutB, FFTW_FORWARD, FFTW_ESTIMATE );

    //======================================== Load map data for Fourier
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( hlpU * hlpV * hlpW ); iter++ )
    {
        if ( halfMap1->_densityMapCor[iter] == halfMap1->_densityMapCor[iter] )
        {
            //================================ Map value is NOT nan
            tmpInA[iter][0]                   = halfMap1->_densityMapCor[iter];
            tmpInA[iter][1]                   = 0.0;
        }
        else
        {
            //================================ Map value is nan - this sometimes occurs for a couple of values, I have no clue as to why...
            tmpInA[iter][0]                   = 0.0;
            tmpInA[iter][1]                   = 0.0;
        }

        if ( halfMap2->_densityMapCor[iter] == halfMap2->_densityMapCor[iter] )
        {
            //================================ Map value is NOT nan
            tmpInB[iter][0]                   = halfMap2->_densityMapCor[iter];
            tmpInB[iter][1]                   = 0.0;
        }
        else
        {
            //================================ Map value is nan - this sometimes occurs for a couple of values, I have no clue as to why...
            tmpInB[iter][0]                   = 0.0;
            tmpInB[iter][1]                   = 0.0;
        }
    }
    
    //======================================== Execute FFTs
    fftw_execute                              ( pA );
    fftw_execute                              ( pB );
    fftw_destroy_plan                         ( pA );
    fftw_destroy_plan                         ( pB );

    //======================================== Initialise the mask data structure
    fftw_complex* maskDataA                   = new fftw_complex [ hlpU * hlpV * hlpW ];
    fftw_complex* maskDataInvA                = new fftw_complex [ hlpU * hlpV * hlpW ];
    fftw_complex* maskDataB                   = new fftw_complex [ hlpU * hlpV * hlpW ];
    fftw_complex* maskDataInvB                = new fftw_complex [ hlpU * hlpV * hlpW ];
    
    //======================================== Initialise local variables
    int uIt, vIt, wIt;
    double real, imag;
    unsigned int arrayPos                     = 0;
    double normFactor                         = (hlpU * hlpV * hlpW);
    double bFacChange                         = settings->maskBlurFactor;
    int h, k, l;
    double mag, phase, S;
    
    //======================================== Prepare FFTW plan for mask FFTW
    fftw_plan pMaskInvA                       = fftw_plan_dft_3d ( hlpU, hlpV, hlpW, maskDataInvA, maskDataA, FFTW_BACKWARD, FFTW_ESTIMATE );
    fftw_plan pMaskInvB                       = fftw_plan_dft_3d ( hlpU, hlpV, hlpW, maskDataInvB, maskDataB, FFTW_BACKWARD, FFTW_ESTIMATE );
    
    for ( uIt = 0; uIt < hlpU; uIt++ )
    {
        for ( vIt = 0; vIt < hlpV; vIt++ )
        {
            for ( wIt = 0; wIt < hlpW; wIt++ )
            {
                //============================ FOR MAP A
                // ...  Var init
                arrayPos                      = wIt + hlpW * ( vIt + hlpV * uIt );
                real                          = tmpOutA[arrayPos][0];
                imag                          = tmpOutA[arrayPos][1];
                
                // ... Change the B-factors, if required
                if ( uIt > (hlpU / 2) ) { h = uIt - hlpU; } else { h = uIt; }
                if ( vIt > (hlpV / 2) ) { k = vIt - hlpV; } else { k = vIt; }
                if ( wIt > (hlpW / 2) ) { l = wIt - hlpW; } else { l = wIt; }
                
                // ... Get magnitude and phase with mask parameters
                S                             = ( pow( static_cast<double> ( h ) / halfMap1->_xRange, 2.0 ) +
                                                  pow( static_cast<double> ( k ) / halfMap1->_yRange, 2.0 ) +
                                                  pow( static_cast<double> ( l ) / halfMap1->_zRange, 2.0 ) );
                mag                           = sqrt ( (real*real) + (imag*imag) ) * std::exp ( - ( ( bFacChange * S ) / 4.0 ) );
                phase                         = atan2 ( imag, real );
                
                // ... Save the mask data
                maskDataInvA[arrayPos][0] = ( mag * cos(phase) ) / normFactor;
                maskDataInvA[arrayPos][1] = ( mag * sin(phase) ) / normFactor;
                
                //============================ FOR MAP B
                // ... Var init
                real                          = tmpOutB[arrayPos][0];
                imag                          = tmpOutB[arrayPos][1];
                
                // ... Get magnitude and phase with mask parameters
                S                             = ( pow( static_cast<double> ( h ) / halfMap2->_xRange, 2.0 ) +
                                                  pow( static_cast<double> ( k ) / halfMap2->_yRange, 2.0 ) +
                                                  pow( static_cast<double> ( l ) / halfMap2->_zRange, 2.0 ) );
                mag                           = sqrt ( (real*real) + (imag*imag) ) * std::exp ( - ( ( bFacChange * S ) / 4.0 ) );
                phase                         = atan2 ( imag, real );
                
                // ... Save the mask data
                maskDataInvB[arrayPos][0]     = ( mag * cos(phase) ) / normFactor;
                maskDataInvB[arrayPos][1]     = ( mag * sin(phase) ) / normFactor;
            }
        }
    }
    
    //======================================== Execute the inversions
    fftw_execute                              ( pMaskInvA );
    fftw_execute                              ( pMaskInvB );
    fftw_destroy_plan                         ( pMaskInvA );
    fftw_destroy_plan                         ( pMaskInvB );
    delete[] maskDataInvA;
    delete[] maskDataInvB;
    delete[] tmpOutA;
    delete[] tmpOutB;
    
    if ( settings->verbose > 2 )
    {
        std::cout << ">>>>> Map blurring complete." << std::endl;
    }
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Map blurring complete." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Find mask threshold
    double mapThresholdPlusA                  = 0.0;
    double mapThresholdPlusB                  = 0.0;
    unsigned int hlpIt                        = 0;
    std::vector<double> maskValsA             ( hlpU * hlpV * hlpW );
    std::vector<double> maskValsB             ( hlpU * hlpV * hlpW );
    
    for ( uIt = 0; uIt < hlpU; uIt++ ) { for ( vIt = 0; vIt < hlpV; vIt++ ) { for ( wIt = 0; wIt < hlpW; wIt++ ) { arrayPos = wIt + hlpW * ( vIt + hlpV * uIt ); maskValsA.at(hlpIt) = maskDataA[arrayPos][0]; maskValsB.at(hlpIt) = maskDataB[arrayPos][0]; hlpIt += 1; } } }
    
    unsigned int medianPos                    = static_cast<unsigned int> ( maskValsA.size() / 2 );
    std::sort                                 ( maskValsA.begin(), maskValsA.end() );
    std::sort                                 ( maskValsB.begin(), maskValsB.end() );
    
    double interQuartileRangeA                = maskValsA.at(medianPos+(medianPos/2)) - maskValsA.at(medianPos-(medianPos/2));
    mapThresholdPlusA                         = maskValsA.at(medianPos+(medianPos/2)) + ( interQuartileRangeA * settings->noIQRsFromMap );
    
    double interQuartileRangeB                = maskValsB.at(medianPos+(medianPos/2)) - maskValsB.at(medianPos-(medianPos/2));
    mapThresholdPlusB                         = maskValsB.at(medianPos+(medianPos/2)) + ( interQuartileRangeB * settings->noIQRsFromMap );
    
    maskValsA.clear                           ( );
    maskValsB.clear                           ( );

    //======================================== Apply the mask
    for ( uIt = 0; uIt < hlpU; uIt++ )
    {
        for ( vIt = 0; vIt < hlpV; vIt++ )
        {
            for ( wIt = 0; wIt < hlpW; wIt++ )
            {
                //============================ Var init
                arrayPos                      = wIt + hlpW * ( vIt + hlpV * uIt );
                
                //============================ Apply mask
                if ( mapThresholdPlusA >= maskDataA[arrayPos][0] )
                {
                    tmpInA[arrayPos][0]       = 0.0;
                }
                if ( mapThresholdPlusB >= maskDataB[arrayPos][0] )
                {
                    tmpInB[arrayPos][0]       = 0.0;
                }
            }
        }
    }
    
    //======================================== Free unnecessary memory
    delete[] maskDataA;
    delete[] maskDataB;
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Map masking complete." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    
    //======================================== Check for obvious errors
    bool atLeastSingleValueA                  = false;
    bool atLeastSingleValueB                  = false;
    for ( uIt = 0; uIt < hlpU; uIt++ )
    {
        for ( vIt = 0; vIt < hlpV; vIt++ )
        {
            for ( wIt = 0; wIt < hlpW; wIt++ )
            {
                //============================ Find current array pos
                arrayPos                      = wIt  + hlpW * ( vIt  + hlpV * uIt  );
                
                if ( !atLeastSingleValueA ) { if ( tmpInA[arrayPos][0] > 0.0 ) { atLeastSingleValueA = true; } }
                if ( !atLeastSingleValueB ) { if ( tmpInB[arrayPos][0] > 0.0 ) { atLeastSingleValueB = true; } }
            }
        }
    }
    
    if ( !atLeastSingleValueA || !atLeastSingleValueB )
    {
        printf ( "!!! ProSHADE ERROR !!! The masking procedure failed to detect any significant density in the map!\n" );
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "The masking procedure failed to detect any significant density in the map. Please consider changing the blurring coefficient value." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                                "ProgressSection",
                                                settings->htmlReportLineProgress,
                                                1,
                                                1,
                                                1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Determine the real size of the shape
    int maxX                                  = 0;
    int minX                                  = hlpU;
    int maxY                                  = 0;
    int minY                                  = hlpV;
    int maxZ                                  = 0;
    int minZ                                  = hlpW;
    int arrPos                                = 0;
    
    for ( uIt = 0; uIt < hlpU; uIt++ )
    {
        for ( vIt = 0; vIt < hlpV; vIt++ )
        {
            for ( wIt = 0; wIt < hlpW; wIt++ )
            {
                arrPos                        = wIt + hlpW * ( vIt  + hlpV * uIt );
                
                if ( tmpInA[arrPos][0] > 0.0 )
                {
                    maxX                      = std::max ( maxX, uIt );
                    minX                      = std::min ( minX, uIt );
                    maxY                      = std::max ( maxY, vIt );
                    minY                      = std::min ( minY, vIt );
                    maxZ                      = std::max ( maxZ, wIt );
                    minZ                      = std::min ( minZ, wIt );
                }
                if ( tmpInB[arrPos][0] > 0.0 )
                {
                    maxX                      = std::max ( maxX, uIt );
                    minX                      = std::min ( minX, uIt );
                    maxY                      = std::max ( maxY, vIt );
                    minY                      = std::min ( minY, vIt );
                    maxZ                      = std::max ( maxZ, wIt );
                    minZ                      = std::min ( minZ, wIt );
                }
            }
        }
    }
    
    delete[] tmpInA;
    delete[] tmpInB;
    
    //======================================== Add 7.5+ angstroms to make sure inter-cell interactions do not occur
    maxX                                     += std::ceil ( 7.5 / halfMap1->_xSamplingRate );
    maxY                                     += std::ceil ( 7.5 / halfMap1->_ySamplingRate );
    maxZ                                     += std::ceil ( 7.5 / halfMap1->_zSamplingRate );
    minX                                     -= std::ceil ( 7.5 / halfMap1->_xSamplingRate );
    minY                                     -= std::ceil ( 7.5 / halfMap1->_ySamplingRate );
    minZ                                     -= std::ceil ( 7.5 / halfMap1->_zSamplingRate );
    
    if ( maxX > static_cast<int> ( halfMap1->_maxMapU ) )  { maxX = halfMap1->_maxMapU; }
    if ( maxY > static_cast<int> ( halfMap1->_maxMapV ) )  { maxY = halfMap1->_maxMapV; }
    if ( maxZ > static_cast<int> ( halfMap1->_maxMapW ) )  { maxZ = halfMap1->_maxMapW; }
    if ( minX < 0                  )                       { minX = 0;                  }
    if ( minY < 0                  )                       { minY = 0;                  }
    if ( minZ < 0                  )                       { minZ = 0;                  }
    
    //======================================== Make cube
    if ( settings->useCubicMaps )
    {
        minX                                  = std::max ( 0, std::min ( minX, std::min ( minY, minZ ) ) );
        minY                                  = std::max ( 0, std::min ( minX, std::min ( minY, minZ ) ) );
        minZ                                  = std::max ( 0, std::min ( minX, std::min ( minY, minZ ) ) );
        maxX                                  = std::min ( hlpU, std::max ( maxX , std::max ( maxY , maxZ ) ) );
        maxY                                  = std::min ( hlpV, std::max ( maxX , std::max ( maxY , maxZ ) ) );
        maxZ                                  = std::min ( hlpW, std::max ( maxX , std::max ( maxY , maxZ ) ) );
        
        if ( ( maxX != maxY ) || ( maxX != maxZ ) || ( maxY != maxZ ) )
        {
            std::cout << "!!! ProSHADE Warning !!! Although cubic map was requested, this cannot be done as the input is not cubic and cutting it to cubic would result in density being removed. Maps will not be cubic." << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"orange\">" << "Although cubic map was requested, this cannot be done as the input is not cubic and cutting it to cubic would result in density being removed. Maps will not be cubic." << "</font>";
                rvapi_set_text                    ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                       ( );
            }
        }
    }

    //======================================== Create new maps
    int xDim                                  = 1 + maxX - minX;
    int yDim                                  = 1 + maxY - minY;
    int zDim                                  = 1 + maxZ - minZ;
    int hlpPos                                = 0;
    
    double* newHalf1Map                       = new double[xDim * yDim * zDim];
    double* newHalf2Map                       = new double[xDim * yDim * zDim];
    
    for ( int xIt = 0; xIt < hlpU; xIt++ )
    {
        for ( int yIt = 0; yIt < hlpV; yIt++ )
        {
            for ( int zIt = 0; zIt < hlpW; zIt++ )
            {
                if ( ( xIt < minX ) || ( xIt > maxX ) ) { continue; }
                if ( ( yIt < minY ) || ( yIt > maxY ) ) { continue; }
                if ( ( zIt < minZ ) || ( zIt > maxZ ) ) { continue; }
                
                arrPos                        = zIt + hlpW * ( yIt  + hlpV * xIt );
                hlpPos                        = (zIt - minZ) + zDim * ( (yIt - minY)  + yDim * (xIt - minX) );
                
                newHalf1Map[hlpPos]           = halfMap1->_densityMapCor[arrPos];
                newHalf2Map[hlpPos]           = halfMap2->_densityMapCor[arrPos];
            }
        }
    }
    
    //======================================== Copy
    delete[] halfMap1->_densityMapCor;
    delete[] halfMap2->_densityMapCor;
    halfMap1->_densityMapCor                  = new double[xDim * yDim * zDim];
    halfMap2->_densityMapCor                  = new double[xDim * yDim * zDim];
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( xDim * yDim * zDim ); iter++ )
    {
        halfMap1->_densityMapCor[iter]        = newHalf1Map[iter];
        halfMap2->_densityMapCor[iter]        = newHalf2Map[iter];
    }
    delete[] newHalf1Map;
    delete[] newHalf2Map;
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Half maps re-boxed." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    //======================================== Set object headers
    int oDimX1                                = halfMap1->_maxMapU;
    int oDimY1                                = halfMap1->_maxMapV;
    int oDimZ1                                = halfMap1->_maxMapW;
    
    int oDimX2                                = halfMap2->_maxMapU;
    int oDimY2                                = halfMap2->_maxMapV;
    int oDimZ2                                = halfMap2->_maxMapW;
    
    halfMap1->_maxMapU                        = xDim;
    halfMap1->_maxMapV                        = yDim;
    halfMap1->_maxMapW                        = zDim;
    
    halfMap1->_xTo                            = maxX + halfMap1->_xFrom;
    halfMap1->_yTo                            = maxY + halfMap1->_yFrom;
    halfMap1->_zTo                            = maxZ + halfMap1->_zFrom;
    
    halfMap1->_xFrom                          = minX + halfMap1->_xFrom;
    halfMap1->_yFrom                          = minY + halfMap1->_yFrom;
    halfMap1->_zFrom                          = minZ + halfMap1->_zFrom;
    
    halfMap1->_xRange                         = xDim * halfMap1->_xSamplingRate;
    halfMap1->_yRange                         = yDim * halfMap1->_ySamplingRate;
    halfMap1->_zRange                         = zDim * halfMap1->_zSamplingRate;
    
    halfMap2->_maxMapU                        = xDim;
    halfMap2->_maxMapV                        = yDim;
    halfMap2->_maxMapW                        = zDim;
    
    halfMap2->_xTo                            = maxX + halfMap2->_xFrom;
    halfMap2->_yTo                            = maxY + halfMap2->_yFrom;
    halfMap2->_zTo                            = maxZ + halfMap2->_zFrom;
    
    halfMap2->_xFrom                          = minX + halfMap2->_xFrom;
    halfMap2->_yFrom                          = minY + halfMap2->_yFrom;
    halfMap2->_zFrom                          = minZ + halfMap2->_zFrom;
    
    halfMap2->_xRange                         = xDim * halfMap2->_xSamplingRate;
    halfMap2->_yRange                         = yDim * halfMap2->_ySamplingRate;
    halfMap2->_zRange                         = zDim * halfMap2->_zSamplingRate;
    
    //======================================== Print headers of the output files
    if ( settings->htmlReport )
    {
        rvapi_add_section                     ( "h1OutHeader",
                                                "Half Map 1 Re-boxed Header",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                false );
        settings->htmlReportLine             += 1;
        
        //==================================== Print max U, V and W
        std::stringstream hlpSS;
        hlpSS << "<pre><b>" << "Rows, Columns and Sections sizes: " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        int prec                              = 6;
        int UVWMAX                            = std::max ( ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap1->_maxMapU+1 ), prec ).length(),
                                                           std::max ( ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap1->_maxMapV+1 ), prec ).length(),
                                                                      ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap1->_maxMapW+1 ), prec ).length() ) );
        int FMAX                              = std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_xFrom, prec ).length(),
                                                           std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_yFrom, prec ).length(),
                                                                      ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_zFrom, prec ).length() ) );
        int TMAX                              = std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_xTo, prec ).length(),
                                                           std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_yTo, prec ).length(),
                                                                      ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_zTo, prec ).length() ) );
        int CDMAX                             = std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_xRange, prec ).length(),
                                                           std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_yRange, prec ).length(),
                                                                      ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_zRange, prec ).length() ) );
        int spacer                            = std::max ( UVWMAX, std::max ( FMAX, std::max ( TMAX, CDMAX ) ) );
        if ( spacer < 5 ) { spacer            = 5; }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << static_cast<int> ( halfMap1->_maxMapU+1 );
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap1->_maxMapU+1 ), prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << static_cast<int> ( halfMap1->_maxMapV+1 );
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap1->_maxMapV+1 ), prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << static_cast<int> ( halfMap1->_maxMapW+1 );
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap1->_maxMapW+1 ), prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "h1OutHeader",
                                               0,
                                               0,
                                               1,
                                               1 );
        
        //==================================== Print from and to lims
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Start and stop points on columns, rows, sections: " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << halfMap1->_xFrom;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_xFrom, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap1->_xTo;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_xTo, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap1->_yFrom;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_yFrom, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap1->_yTo;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_yTo, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << " " << halfMap1->_zFrom;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_zFrom, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap1->_zTo;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_zTo, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "h1OutHeader",
                                               1,
                                               0,
                                               1,
                                               1 );
        
        //==================================== Print cell dimensions
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Cell dimensions (Angstrom): " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << halfMap1->_xRange;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_xRange, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap1->_yRange;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_yRange, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap1->_zRange;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap1->_zRange, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "h1OutHeader",
                                               2,
                                               0,
                                               1,
                                               1 );
        
        //==================================== Print cell dimensions
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Cell angles (Degrees): " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << 90.0;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( 90.0, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << 90.0;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( 90.0, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << 90.0;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( 90.0, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "h1OutHeader",
                                               3,
                                               0,
                                               1,
                                               1 );
        
        //==================================== Print cell dimensions
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Fast, medium, slow axes: " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        std::string hlpAO                     = settings->axisOrder;
        std::transform                        ( hlpAO.begin(), hlpAO.end(), hlpAO.begin(), ::toupper);
        hlpSS << " " << hlpAO;
        for ( int iter = 0; iter < static_cast<int> ( spacer - 1 ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "h1OutHeader",
                                               4,
                                               0,
                                               1,
                                               1 );
        
        rvapi_flush                           ( );
    }
    
    if ( settings->htmlReport )
    {
        rvapi_add_section                     ( "h2OutHeader",
                                                "Half Map 2 Re-boxed Header",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                false );
        settings->htmlReportLine             += 1;
        
        //==================================== Print max U, V and W
        std::stringstream hlpSS;
        hlpSS << "<pre><b>" << "Rows, Columns and Sections sizes: " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        int prec                              = 6;
        int UVWMAX                            = std::max ( ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap2->_maxMapU+1 ), prec ).length(),
                                                          std::max ( ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap2->_maxMapV+1 ), prec ).length(),
                                                                    ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap2->_maxMapW+1 ), prec ).length() ) );
        int FMAX                              = std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_xFrom, prec ).length(),
                                                          std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_yFrom, prec ).length(),
                                                                    ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_zFrom, prec ).length() ) );
        int TMAX                              = std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_xTo, prec ).length(),
                                                          std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_yTo, prec ).length(),
                                                                    ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_zTo, prec ).length() ) );
        int CDMAX                             = std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_xRange, prec ).length(),
                                                          std::max ( ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_yRange, prec ).length(),
                                                                    ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_zRange, prec ).length() ) );
        int spacer                            = std::max ( UVWMAX, std::max ( FMAX, std::max ( TMAX, CDMAX ) ) );
        if ( spacer < 5 ) { spacer            = 5; }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << static_cast<int> ( halfMap2->_maxMapU+1 );
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap2->_maxMapU+1 ), prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << static_cast<int> ( halfMap2->_maxMapV+1 );
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap2->_maxMapV+1 ), prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << static_cast<int> ( halfMap2->_maxMapW+1 );
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( static_cast<int> ( halfMap2->_maxMapW+1 ), prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "h2OutHeader",
                                               0,
                                               0,
                                               1,
                                               1 );
        
        //==================================== Print from and to lims
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Start and stop points on columns, rows, sections: " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << halfMap2->_xFrom;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_xFrom, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap2->_xTo;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_xTo, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap2->_yFrom;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_yFrom, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap2->_yTo;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_yTo, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << " " << halfMap2->_zFrom;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_zFrom, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap2->_zTo;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_zTo, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "h2OutHeader",
                                               1,
                                               0,
                                               1,
                                               1 );
        
        //==================================== Print cell dimensions
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Cell dimensions (Angstrom): " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << halfMap2->_xRange;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_xRange, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap2->_yRange;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_yRange, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << halfMap2->_zRange;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( halfMap2->_zRange, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "h2OutHeader",
                                               2,
                                               0,
                                               1,
                                               1 );
        
        //==================================== Print cell dimensions
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Cell angles (Degrees): " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        hlpSS << std::showpos << std::setprecision ( prec ) << " " << 90.0;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( 90.0, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << 90.0;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( 90.0, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "  ";
        
        hlpSS << std::showpos << 90.0;
        for ( int iter = 0; iter < static_cast<int> ( spacer - ProSHADE_internal_misc::to_string_with_precision ( 90.0, prec ).length() ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "h2OutHeader",
                                               3,
                                               0,
                                               1,
                                               1 );
        
        //==================================== Print cell dimensions
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Fast, medium, slow axes: " << "</b>";
        for ( int iter = static_cast<int> ( hlpSS.str().length() ); iter < 70; iter++ )
        {
            hlpSS << ".";
        }
        
        std::string hlpAO                     = settings->axisOrder;
        std::transform                        ( hlpAO.begin(), hlpAO.end(), hlpAO.begin(), ::toupper);
        hlpSS << " " << hlpAO;
        for ( int iter = 0; iter < static_cast<int> ( spacer - 1 ); iter++ )
        {
            hlpSS << " ";
        }
        hlpSS << "</pre>";
        
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "h2OutHeader",
                                               4,
                                               0,
                                               1,
                                               1 );
        
        rvapi_flush                           ( );
    }
    
    //======================================== Write out the re-boxed half maps
    std::stringstream ss;
    ss << settings->clearMapFile << "_half1.map";
    halfMap1->writeMap ( ss.str(), halfMap1->_densityMapCor );
    
    ss.str( std::string ( ) );
    ss << settings->clearMapFile << "_half2.map";
    halfMap2->writeMap ( ss.str(), halfMap2->_densityMapCor );
    
    if ( settings->htmlReport )
    {
        std::stringstream hlpSS;
        hlpSS << "<font color=\"green\">" << "Half maps saved." << "</font>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
        settings->htmlReportLineProgress     += 1;
        rvapi_flush                           ( );
    }
    
    if ( settings->htmlReport )
    {
        rvapi_add_section                     ( "ResultsSection",
                                                "Results",
                                                "body",
                                                settings->htmlReportLine,
                                                0,
                                                1,
                                                1,
                                                true );
        settings->htmlReportLine             += 1;
        
        std::stringstream hlpSS;
        hlpSS << "<pre><b>" << "The re-boxed structures are available at:   </b>" << settings->clearMapFile << "_half1.map" << "</pre>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                0,
                                                0,
                                                1,
                                                1 );
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << " ... and                                :   </b>" << settings->clearMapFile << "_half2.map" << "</pre>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                1,
                                                0,
                                                1,
                                                1 );
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Original structure dims:                    </b>" << oDimX1+1 << " x " << oDimY1+1 << " x " << oDimZ1+1 << "</pre>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                2,
                                                0,
                                                1,
                                                1 );
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << " ... and               :                    </b>" << oDimX2+1 << " x " << oDimY2+1 << " x " << oDimZ2+1 << "</pre>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                3,
                                                0,
                                                1,
                                                1 );
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Re-boxed structure dims:                    </b>" << halfMap1->_maxMapU << " x " << halfMap1->_maxMapV << " x " << halfMap1->_maxMapW << "</pre>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                4,
                                                0,
                                                1,
                                                1 );
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << " ... and               :                    </b>" << halfMap2->_maxMapU << " x " << halfMap2->_maxMapV << " x " << halfMap2->_maxMapW << "</pre>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                5,
                                                0,
                                                1,
                                                1 );
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "New volume as percentage of old volume:     </b>" << ProSHADE_internal_misc::to_string_with_precision ( ( static_cast<double> ( (halfMap1->_maxMapU+1) * (halfMap1->_maxMapV+1) * (halfMap1->_maxMapW+1) ) / static_cast<double> ( (oDimX1+1) * (oDimY1+1) * (oDimZ1+1) ) ) * 100 ) << " %</pre>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                6,
                                                0,
                                                1,
                                                1 );
        
        hlpSS.str                             ( std::string ( ) );
        hlpSS << "<pre><b>" << "Linear processing speed-up:                 </b>" << ProSHADE_internal_misc::to_string_with_precision ( ( ( (oDimX1+1) * (oDimY1+1) * (oDimZ1+1) ) / ( (halfMap1->_maxMapU+1) * (halfMap1->_maxMapV+1) * (halfMap1->_maxMapW+1) ) ) - 1.0 ) << " times</pre>";
        rvapi_set_text                        ( hlpSS.str().c_str(),
                                                "ResultsSection",
                                                7,
                                                0,
                                                1,
                                                1 );
        
        rvapi_flush                           ( );
    }
    
    //======================================== Release memory
    delete halfMap1;
    delete halfMap2;

    //======================================== Done
    return ;
    
}

/*! \brief This function takes the map and fragments it into boxes of given size, returning vector of data objects, one for each box.
 
 This function is a rather specific one. It takes the map ( assumes at least that it has been computed and processed ) and fragments into boxes using the same
 approach as the ProSHADE_features::fragmentMap function. For each box passing the tests, it then creates a new ProSHADE_data object in a vector of such objects
 it returns. Consequently, it fills this object as if it has been constructed, data read in from a file and phase kept function called. However, this is not
 a constructor, it rather constructs an empty object and fills it in... The purpose of this function is singular in the fragmented map vs. database distances
 computation.
 
 \param[in] settings The settings object to be used to determine the settings.
 \param[in] userCOM This boolean value determines is the Center of Mass (COM) should be used to center the density in the boxes, or whether the density should be left as is.
 \param[out] X A vector of ProSHADE_data class pointers to data objects allowing access to all the fragments found.
 
 \warning This is an internal function which should not be used by the user. The user should set the parameters and it will be done automatically.
 */
std::vector<ProSHADE_internal::ProSHADE_data*> ProSHADE_internal::ProSHADE_data::fragmentMap ( ProSHADE::ProSHADE_settings* settings,
                                                                                               bool userCOM )
{
    //======================================== Sanity check
    if ( !this->_densityMapComputed )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Tried to fragment map without first computing it. Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Tried to fragment map without first computing it. This looks like an internal bug - please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    if ( settings->verbose > 1 )
    {
        std::cout << ">> Fragmenting map." << std::endl;
    }
    
    //======================================== Initialise local variables
    std::vector<ProSHADE_data*> ret;
    std::vector< std::array<unsigned int,2> > XDim;
    std::vector< std::array<unsigned int,2> > YDim;
    std::vector< std::array<unsigned int,2> > ZDim;
    std::array<unsigned int,2> hlpArrX;
    std::array<unsigned int,2> hlpArrY;
    std::array<unsigned int,2> hlpArrZ;
    double maxMapAngPerIndex                  = std::max ( this->_xRange / static_cast<double> ( this->_maxMapU ),
                                                           std::max ( this->_yRange / static_cast<double> ( this->_maxMapV ),
                                                                      this->_zRange / static_cast<double> ( this->_maxMapW ) ) );

    unsigned int boxDimInIndices              = static_cast<unsigned int> ( std::round ( settings->mapFragBoxSize / maxMapAngPerIndex ) );
    if ( ( boxDimInIndices % 2 ) == 1 )
    {
        //==================================== If odd, change to even
        boxDimInIndices                      += 1;
    }
    
    //======================================== Problem! The box is too small
    if ( boxDimInIndices < 2 )
    {
        std::cerr << "!!! ProSHADE ERROR !!! The box size for fragmentation is too small, please increase the box size or submit more detailed map. (Increasing the number of points by interpolation could work without changing the map here...)" << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "The box size for fragmentation is too small, please increase the box size or submit more detailed map. (Increasing the number of points by interpolation could work without changing the map here...)." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    if ( ( boxDimInIndices >= this->_maxMapU ) || ( boxDimInIndices >= this->_maxMapV ) || ( boxDimInIndices >= this->_maxMapW ) )
    {
        std::cerr << "!!! ProSHADE ERROR !!! The box size for fragmentation is too large, please decrease the box size so that it is smaller than the clearmap dimmensions." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "The box size for fragmentation is too large, please decrease the box size so that it is smaller than the map dimmensions." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    if ( settings->verbose > 3 )
    {
        std::cout << ">>>>>>>> Sanity checks passed." << std::endl;
    }
    
    //======================================== Generate box starts and ends
    for ( unsigned int xStart = 0; xStart <= ( this->_maxMapU - boxDimInIndices ); xStart += ( boxDimInIndices / 2 ) )
    {
        hlpArrX[0]                            = xStart;
        hlpArrX[1]                            = xStart + boxDimInIndices;
        
        //==================================== Check for terminal boxes, they may need to have larger size
        if ( ( xStart + ( boxDimInIndices / 2 ) ) > ( this->_maxMapU - boxDimInIndices ) )
        {
            //================================ This is the last X
            hlpArrX[1]                        = this->_maxMapU;
        }
        
        for ( unsigned int yStart = 0; yStart <= ( this->_maxMapV - boxDimInIndices ); yStart += ( boxDimInIndices / 2 ) )
        {
            hlpArrY[0]                        = yStart;
            hlpArrY[1]                        = yStart + boxDimInIndices;
            
            //================================ Check for terminal boxes, they may need to have larger size
            if ( ( yStart + ( boxDimInIndices / 2 ) ) > ( this->_maxMapV - boxDimInIndices ) )
            {
                //============================ This is the last Y
                hlpArrY[1]                    = this->_maxMapV;
            }
            
            for ( unsigned int zStart = 0; zStart <= ( this->_maxMapW - boxDimInIndices ); zStart += ( boxDimInIndices / 2 ) )
            {
                hlpArrZ[0]                    = zStart;
                hlpArrZ[1]                    = zStart + boxDimInIndices;
                
                //============================ Check for terminal boxes, they may need to have larger size
                if ( ( zStart + ( boxDimInIndices / 2 ) ) > ( this->_maxMapW - boxDimInIndices ) )
                {
                    //======================== This is the last Z
                    hlpArrZ[1]                = this->_maxMapW;
                }
                
                XDim.emplace_back             ( hlpArrX );
                YDim.emplace_back             ( hlpArrY );
                ZDim.emplace_back             ( hlpArrZ );
            }
        }
    }
    if ( settings->verbose > 2 )
    {
        std::cout << ">>>>> Box sizes computed." << std::endl;
    }
    
    //======================================== If the box has enough density, create a new object!
    unsigned int noDensityPoints              = 0;
    unsigned int strCounter                   = 0;
    unsigned int arrayPos                     = 0;
    int coordPos                              = 0;
    double boxVolume                          = 0.0;
    double xCOM                               = 0.0;
    double yCOM                               = 0.0;
    double zCOM                               = 0.0;
    double densTot                            = 0.0;
    int newU, newV, newW;
    int hlpU, hlpV, hlpW;
    std::vector<double> mapVals;
    std::string fileName;
    
    for ( unsigned int boxIt = 0; boxIt < static_cast<unsigned int> ( XDim.size() ); boxIt++ )
    {
        if ( settings->verbose > 2 )
        {
            std::cout << ">>>>> Trying box " << boxIt << " ." << std::endl;
        }
        
        //==================================== Check the box density fraction
        noDensityPoints                       = 0;
        for ( unsigned int x = XDim.at(boxIt)[0]; x <= XDim.at(boxIt)[1]; x++ )
        {
            for ( unsigned int y = YDim.at(boxIt)[0]; y <= YDim.at(boxIt)[1]; y++ )
            {
                for ( unsigned int z = ZDim.at(boxIt)[0]; z <= ZDim.at(boxIt)[1]; z++ )
                {
                    arrayPos                  = z  + (this->_maxMapW+1) * ( y  + (this->_maxMapV+1) * x  );
                    
                    if ( this->_densityMapCor[arrayPos] > 0.0 )
                    {
                        noDensityPoints      += 1;
                    }
                }
            }
        }
        
        //==================================== If passing, write out the box
        boxVolume                             = ( ( XDim.at(boxIt)[1] - XDim.at(boxIt)[0] ) + 1 ) * ( ( YDim.at(boxIt)[1] - YDim.at(boxIt)[0] ) + 1 ) * ( ( ZDim.at(boxIt)[1] - ZDim.at(boxIt)[0] ) + 1 );
        if ( ( static_cast<double> ( noDensityPoints ) / boxVolume ) > settings->mapFragBoxFraction )
        {
            //================================ Create new structure
            ret.emplace_back                  ( new ProSHADE_data () );
            
            //================================ Fill in the obvious parameters
            ret.at(strCounter)->_fromPDB      = false;
            ret.at(strCounter)->_shellSpacing = settings->shellSpacing;
            ret.at(strCounter)->_maxExtraCellularSpace = settings->extraSpace;
            ret.at(strCounter)->_xRange       = ( XDim.at(boxIt)[1] - XDim.at(boxIt)[0] ) * ( this->_xRange / static_cast<double> ( this->_maxMapU ) );
            ret.at(strCounter)->_yRange       = ( YDim.at(boxIt)[1] - YDim.at(boxIt)[0] ) * ( this->_yRange / static_cast<double> ( this->_maxMapV ) );
            ret.at(strCounter)->_zRange       = ( ZDim.at(boxIt)[1] - ZDim.at(boxIt)[0] ) * ( this->_zRange / static_cast<double> ( this->_maxMapW ) );
            
            //================================ Set xyzFrom and xyzTo
            ret.at(strCounter)->_xFrom        = XDim.at(boxIt)[0] + this->_xFrom;
            ret.at(strCounter)->_yFrom        = YDim.at(boxIt)[0] + this->_yFrom;;
            ret.at(strCounter)->_zFrom        = ZDim.at(boxIt)[0] + this->_zFrom;;
            
            ret.at(strCounter)->_xTo          = XDim.at(boxIt)[1] + this->_xFrom;;
            ret.at(strCounter)->_yTo          = YDim.at(boxIt)[1] + this->_yFrom;;
            ret.at(strCounter)->_zTo          = ZDim.at(boxIt)[1] + this->_zFrom;;
            
            //================================ Automatically determine maximum number of shells
            ret.at(strCounter)->_shellPlacement = std::vector<double> ( std::floor ( std::max ( ret.at(strCounter)->_xRange, std::max ( ret.at(strCounter)->_yRange, ret.at(strCounter)->_zRange ) ) / ret.at(strCounter)->_shellSpacing ) );
            
            for ( unsigned int shellIter = 0; shellIter < static_cast<unsigned int> ( ret.at(strCounter)->_shellPlacement.size() ); shellIter++ )
            {
                ret.at(strCounter)->_shellPlacement.at(shellIter) = (shellIter+1) * ret.at(strCounter)->_shellSpacing;
            }
            
            //================================ Determine other basic parameters
            ret.at(strCounter)->_mapResolution= settings->mapResolution;
            ret.at(strCounter)->_maxMapU      = ( XDim.at(boxIt)[1] - XDim.at(boxIt)[0] );
            ret.at(strCounter)->_maxMapV      = ( YDim.at(boxIt)[1] - YDim.at(boxIt)[0] );
            ret.at(strCounter)->_maxMapW      = ( ZDim.at(boxIt)[1] - ZDim.at(boxIt)[0] );
            ret.at(strCounter)->_densityMapComputed = true;
            
            hlpU                              = ( ret.at(strCounter)->_maxMapU + 1 );
            hlpV                              = ( ret.at(strCounter)->_maxMapV + 1 );
            hlpW                              = ( ret.at(strCounter)->_maxMapW + 1 );
            
            //================================ Fill the density map
            mapVals.clear                     ( );
            ret.at(strCounter)->_densityMapCor= new double [(ret.at(strCounter)->_maxMapU+1) * (ret.at(strCounter)->_maxMapV+1) * (ret.at(strCounter)->_maxMapW + 1)];
            xCOM                              = 0.0;
            yCOM                              = 0.0;
            zCOM                              = 0.0;
            densTot                           = 0.0;
            for ( unsigned int x = XDim.at(boxIt)[0]; x <= XDim.at(boxIt)[1]; x++ )
            {
                for ( unsigned int y = YDim.at(boxIt)[0]; y <= YDim.at(boxIt)[1]; y++ )
                {
                    for ( unsigned int z = ZDim.at(boxIt)[0]; z <= ZDim.at(boxIt)[1]; z++ )
                    {
                        arrayPos              = z  + (this->_maxMapW+1) * ( y  + (this->_maxMapV+1) * x  );
                        coordPos              = (z-ZDim.at(boxIt)[0]) + (ret.at(strCounter)->_maxMapW+1) * ( (y-YDim.at(boxIt)[0])  + (ret.at(strCounter)->_maxMapV+1) * (x-XDim.at(boxIt)[0])  );
                        
                        ret.at(strCounter)->_densityMapCor[coordPos] = this->_densityMapCor[arrayPos];
                        
                        if ( this->_densityMapCor[arrayPos] > 0.0 )
                        {
                            mapVals.emplace_back ( this->_densityMapCor[arrayPos] );
                            xCOM             += ( x - XDim.at(boxIt)[0] ) * this->_densityMapCor[arrayPos];
                            yCOM             += ( y - YDim.at(boxIt)[0] ) * this->_densityMapCor[arrayPos];
                            zCOM             += ( z - ZDim.at(boxIt)[0] ) * this->_densityMapCor[arrayPos];
                            densTot          += this->_densityMapCor[arrayPos];
                        }
                    }
                }
            }
            
            //================================ Determine other parameters
            ret.at(strCounter)->_mapMean      = std::accumulate ( mapVals.begin(), mapVals.end(), 0.0 ) / static_cast<double> ( mapVals.size() );
            ret.at(strCounter)->_mapSdev      = std::sqrt ( static_cast<double> ( std::inner_product ( mapVals.begin(), mapVals.end(), mapVals.begin(), 0.0 ) ) /
                                                                                   static_cast<double> ( mapVals.size() ) - ret.at(strCounter)->_mapMean * ret.at(strCounter)->_mapMean );
            mapVals.clear                     ( );
            
            //====== Determine COM
            std::array<double,3> meanVals;
            if ( userCOM )
            {
                meanVals[0]                   = xCOM / densTot;
                meanVals[1]                   = yCOM / densTot;
                meanVals[2]                   = zCOM / densTot;
            }
            else
            {
                meanVals[0]                   = static_cast<double> ( hlpU ) / 2.0;
                meanVals[1]                   = static_cast<double> ( hlpV ) / 2.0;
                meanVals[2]                   = static_cast<double> ( hlpW ) / 2.0;
            }
            
            //================================ Re-index the map with COM in the centre
            ret.at(strCounter)->_xCorrErr     = meanVals[0] - static_cast<double> ( hlpU ) / 2.0;
            ret.at(strCounter)->_yCorrErr     = meanVals[1] - static_cast<double> ( hlpV ) / 2.0;
            ret.at(strCounter)->_zCorrErr     = meanVals[2] - static_cast<double> ( hlpW ) / 2.0;
            
            ret.at(strCounter)->_xCorrection  = std::ceil ( ret.at(strCounter)->_xCorrErr );
            ret.at(strCounter)->_yCorrection  = std::ceil ( ret.at(strCounter)->_yCorrErr );
            ret.at(strCounter)->_zCorrection  = std::ceil ( ret.at(strCounter)->_zCorrErr );
            
            ret.at(strCounter)->_densityMapCorCoords = new std::array<double,3> [hlpU * hlpV * hlpW];
            double *tmpIn                     = new double [hlpU * hlpV * hlpW];
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( hlpU * hlpV * hlpW ); iter++ )
            {
                tmpIn[iter]                   = ret.at(strCounter)->_densityMapCor[iter];
            }
            
            for ( int uIt = 0; uIt < hlpU; uIt++ )
            {
                for ( int vIt = 0; vIt < hlpV; vIt++ )
                {
                    for ( int wIt = 0; wIt < hlpW; wIt++ )
                    {
                        newU                  = static_cast<int> ( uIt ) - static_cast<int> ( ret.at(strCounter)->_xCorrection );
                        newV                  = static_cast<int> ( vIt ) - static_cast<int> ( ret.at(strCounter)->_yCorrection );
                        newW                  = static_cast<int> ( wIt ) - static_cast<int> ( ret.at(strCounter)->_zCorrection );
                        
                        if ( newU < 0 ) { newU += hlpU; } if ( newU >= static_cast<int> ( hlpU ) ) { newU -= hlpU; }
                        if ( newV < 0 ) { newV += hlpV; } if ( newV >= static_cast<int> ( hlpV ) ) { newV -= hlpV; }
                        if ( newW < 0 ) { newW += hlpW; } if ( newW >= static_cast<int> ( hlpW ) ) { newW -= hlpW; }
                        
                        arrayPos              = wIt  + hlpW * ( vIt  + hlpV * uIt  );
                        coordPos              = newW + hlpW * ( newV  + hlpV * newU );
                        
                        ret.at(strCounter)->_densityMapCor[coordPos] = tmpIn[arrayPos];
                        
                        //==================== Save results
                        ret.at(strCounter)->_densityMapCorCoords[coordPos][0] = ( static_cast<double> ( newU ) - static_cast<double> ( hlpU ) /2.0 ) * ( ret.at(strCounter)->_xRange / static_cast<double> ( hlpU ) );
                        ret.at(strCounter)->_densityMapCorCoords[coordPos][1] = ( static_cast<double> ( newV ) - static_cast<double> ( hlpV ) /2.0 ) * ( ret.at(strCounter)->_yRange / static_cast<double> ( hlpV ) );;
                        ret.at(strCounter)->_densityMapCorCoords[coordPos][2] = ( static_cast<double> ( newW ) - static_cast<double> ( hlpW ) /2.0 ) * ( ret.at(strCounter)->_zRange / static_cast<double> ( hlpW ) );;
                    }
                }
            }
            delete[] tmpIn;
            
            //================================ Set final batch of parameters
            ret.at(strCounter)->_fourierCoeffPower = this->_fourierCoeffPower;
            ret.at(strCounter)->_bFactorChange= this->_bFactorChange;
            ret.at(strCounter)->_maxMapRange  = std::max ( ret.at(strCounter)->_xRange, std::max ( ret.at(strCounter)->_yRange, ret.at(strCounter)->_zRange ) );
            ret.at(strCounter)->_phaseRemoved = this->_phaseRemoved;
            ret.at(strCounter)->_keepOrRemove = this->_keepOrRemove;
            ret.at(strCounter)->_thetaAngle   = this->_thetaAngle;
            ret.at(strCounter)->_phiAngle     = this->_phiAngle;
            ret.at(strCounter)->_noShellsWithData = 0;
            ret.at(strCounter)->_sphereMapped = false;
            ret.at(strCounter)->_firstLineCOM = false;
            ret.at(strCounter)->_bandwidthLimit = this->_bandwidthLimit;
            ret.at(strCounter)->_oneDimmension= ret.at(strCounter)->_bandwidthLimit * 2;
            ret.at(strCounter)->_sphericalCoefficientsComputed = false;
            ret.at(strCounter)->_rrpMatricesPrecomputed = false;
            ret.at(strCounter)->_shellMappedData = nullptr;
            ret.at(strCounter)->_realSHCoeffs = nullptr;
            ret.at(strCounter)->_imagSHCoeffs = nullptr;
            ret.at(strCounter)->_sphericalHarmonicsWeights= nullptr;
            ret.at(strCounter)->_semiNaiveTable = nullptr;
            ret.at(strCounter)->_semiNaiveTableSpace = nullptr;
            ret.at(strCounter)->_shWorkspace  = nullptr;
            ret.at(strCounter)->_invRealData  = nullptr;
            ret.at(strCounter)->_invImagData  = nullptr;
            ret.at(strCounter)->_rrpMatrices  = nullptr;
            
            //================================ Object initialised!
            strCounter                       += 1;
        }
        else
        {
            if ( settings->verbose > 3 )
            {
                std::cout << ">>>>>>>> Density check NOT passed." << std::endl;
            }
        }
        
        if ( settings->verbose > 3 )
        {
            std::cout << ">>>>>>>> Box " << boxIt << " processed." << std::endl;
        }
    }
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief This function takes model and finds the maximum distance between any two atoms.
 
 The premise of this function is that if you need to know how to fragment your density in order to best find some models in it, you need to figure the maximum distance between
 any two atoms in the model, i.e. the model radius. To facilitate this, this function returns exactly such value.
 
 \param[in] modelPath A path to the file containing the model for which radius should be determined.
 
 \warning This is an internal function which should not be used by the user.
 */
double ProSHADE_internal_misc::modelRadiusCalc ( std::string modelPath )
{
    //======================================== Read in the PDB
    clipper::mmdb::CMMDBManager *mfile        = new clipper::mmdb::CMMDBManager ( );
    if ( mfile->ReadCoorFile ( modelPath.c_str() ) )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Cannot read model file: " << modelPath.c_str() << std::endl;
        exit ( -1 );
    }
    
    //======================================== Initialise MMDB crawl
    int noChains                              = 0;
    int noResidues                            = 0;
    int noAtoms                               = 0;
    std::vector<std::array<double,3>> coords;
    std::array<double,3> hlpArr;
    
    clipper::mmdb::PPCChain chain;
    clipper::mmdb::PPCResidue residue;
    clipper::mmdb::PPCAtom atom;
    
    //======================================== Get all chains
    mfile->GetChainTable                      ( 1, chain, noChains );
    for ( unsigned int nCh = 0; nCh < static_cast<unsigned int> ( noChains ); nCh++ )
    {
        if ( chain[nCh] )
        {
            //================================ Get all residues
            chain[nCh]->GetResidueTable       ( residue, noResidues );
            
            for ( unsigned int nRes = 0; nRes < static_cast<unsigned int> ( noResidues ); nRes++ )
            {
                if ( residue[nRes] )
                {
                    //======================== Get all atoms
                    residue[nRes]->GetAtomTable ( atom, noAtoms );
                    
                    for ( unsigned int aNo = 0; aNo < static_cast<unsigned int> ( noAtoms ); aNo++ )
                    {
                        if ( atom[aNo] )
                        {
                            //================ Check for termination 'residue'
                            if ( atom[aNo]->Ter ) { continue; }
                            
                            //================ Save the coords
                            hlpArr[0]         = atom[aNo]->x;
                            hlpArr[1]         = atom[aNo]->y;
                            hlpArr[2]         = atom[aNo]->z;
                            
                            //================ And save to vector
                            coords.emplace_back ( hlpArr );
                        }
                    }
                }
            }
        }
    }
    
    //======================================== Free memory
    delete mfile;
    
    //======================================== Full distance check
//    double maxQuadDist                        = 0.0;
//    double distHlp                            = 0.0;
//    for ( unsigned int at1 = 0; at1 < static_cast<unsigned int> ( coords.size() ); at1++ )
//    {
//        for ( unsigned int at2 = 1; at2 < static_cast<unsigned int> ( coords.size() ); at2++ )
//        {
//            if ( at1 >= at2 ) { continue; }
//
//            distHlp                           = sqrt ( pow ( std::abs( coords.at(at1)[0] - coords.at(at2)[0] ), 2.0 ) +
//                                                       pow ( std::abs( coords.at(at1)[1] - coords.at(at2)[1] ), 2.0 ) +
//                                                       pow ( std::abs( coords.at(at1)[2] - coords.at(at2)[2] ), 2.0 ) );
//            if ( maxQuadDist < distHlp )
//            {
//                maxQuadDist                   = distHlp;
//            }
//        }
//    }
    
    //======================================== Firt approximation
    double fullDist                           = 0.0;
    double firstApprox                        = 0.0;
    double hlpDist                            = 0.0;
    double xMean                              = 0.0;
    double yMean                              = 0.0;
    double zMean                              = 0.0;
    unsigned int furthestAtom                 = 0;
    for ( unsigned int at1 = 0; at1 < static_cast<unsigned int> ( coords.size() ); at1++ )
    {
        xMean                                += coords.at(at1)[0];
        yMean                                += coords.at(at1)[1];
        zMean                                += coords.at(at1)[2];
    }
    xMean                                    /= static_cast<double> ( coords.size() );
    yMean                                    /= static_cast<double> ( coords.size() );
    zMean                                    /= static_cast<double> ( coords.size() );
    
    std::vector<double> dists                 ( coords.size(), 0.0 );
    for ( unsigned int at1 = 0; at1 < static_cast<unsigned int> ( coords.size() ); at1++ )
    {
        hlpDist                               = sqrt ( pow ( std::abs ( xMean - coords.at(at1)[0] ), 2.0 ) +
                                                       pow ( std::abs ( yMean - coords.at(at1)[1] ), 2.0 ) +
                                                       pow ( std::abs ( zMean - coords.at(at1)[2] ), 2.0 ) );
        dists.at(at1)                         = hlpDist;
        
        if ( firstApprox < hlpDist )
        {
            firstApprox                       = hlpDist;
            furthestAtom                      = at1;
        }
    }
    
    for ( unsigned int at1 = 0; at1 < static_cast<unsigned int> ( dists.size() ); at1++ )
    {
        if ( at1 == furthestAtom ) { continue; }
        
        hlpDist                               = sqrt ( pow ( std::abs ( coords.at(furthestAtom)[0] - coords.at(at1)[0] ), 2.0 ) +
                                                       pow ( std::abs ( coords.at(furthestAtom)[1] - coords.at(at1)[1] ), 2.0 ) +
                                                       pow ( std::abs ( coords.at(furthestAtom)[2] - coords.at(at1)[2] ), 2.0 ) );
        
        if ( fullDist < hlpDist )
        {
            fullDist                          = hlpDist;
        }
    }
    
    //======================================== Done
    return ( fullDist );
    
}
