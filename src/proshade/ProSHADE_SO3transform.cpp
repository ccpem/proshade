/*! \file ProSHADE_SO3transform.cpp
 \brief This file contains functions relating to the SO3 transform and symmetry detection in general.
 
 This file is where functions required for computation of the SO3 transform and its inverse
 live. It also contains functions for detecting symmetries and the related content, as well
 as functions required for distances computation (the rotation function minimisation part) and
 functions required for finding the optimal overlay of structures in general.
 
 This file is part of the ProSHADE library for calculating
 shape descriptors and symmetry operators of protein structures.
 This is a prototype code, which is by no means complete or fully
 tested. Its use is at your own risk only. There is no quarantee
 that the results are correct.
 
 \author    Michal Tykac
 \author    Garib N. Murshudov
 \version   0.6.6
 \date      JAN 2019
 */

//============================================ FFTW3 + SOFT
#ifdef __cplusplus
extern "C" {
#endif
#include <fftw3.h>
#include <wrap_fftw.h>
#include <makeweights.h>
#include <s2_primitive.h>
#include <s2_cospmls.h>
#include <s2_legendreTransforms.h>
#include <s2_semi_fly.h>
#include <rotate_so3_utils.h>
#include <utils_so3.h>
#include <soft_fftw.h>
#include <rotate_so3_fftw.h>
#ifdef __cplusplus
}
#endif

//============================================ RVAPI
#include <rvapi_interface.h>

//============================================ ProSHADE
#include "ProSHADE.h"
#include "ProSHADE_internal.h"
#include "ProSHADE_misc.h"

/*! \brief This function is responsible for computing the SO3 inverse transform.
 
 The SO3 transform and its inverse are required by the symmetry detection class to obtain
 peaks where the structure overlays itself well and this function provides the transform
 to allow it.
 
 \param[in] settings The settings class container with information about how to print the HTML report.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_comparePairwise::getSO3InverseMap ( ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Require E matrices
    if ( !this->_trSigmaPreComputed )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Finding the optimal overlay requires the data from pre-computation of the TrSigma descriptor. This is not really logical for the user, but it saves a lot of time in case both overlay and TrSigma are to be computed. Therefore, you need to call the precomputeTrSigmaDescriptor function. Sorry about the inconvenience." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Finding the optimal overlay requires the data from pre-computation of the integral over the spheres (E matrices), which has not been done. This looks like an internal bug, please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Initialise internal values
    this->_so3Coeffs                          = reinterpret_cast<fftw_complex*> ( fftw_malloc ( sizeof ( fftw_complex ) * ( ( 4 * pow( static_cast<double> ( this->_bandwidthLimit ),3.0 ) - static_cast<double> ( this->_bandwidthLimit ) ) / 3.0 ) ) );
    
    //======================================== Set all coeffs to 0
    memset                                    ( this->_so3Coeffs,
                                                0.0,
                                                sizeof ( fftw_complex ) * totalCoeffs_so3 ( this->_bandwidthLimit ) );
    
    //======================================== Initialise local variables
    double wigNorm                            = 0.0;
    unsigned int indexO                       = 0;
    double signValue                          = 1.0;
    bool toBeIgnored                          = false;
    unsigned int bandUsageIndex               = 0;
    
    //======================================== Combine the coefficients into SO(3) Array
    for ( unsigned int bandIter = 0; bandIter < this->_bandwidthLimit; bandIter++ )
    {
        //==================================== Ignore odd l's as they are 0 anyway
        if ( !this->_keepOrRemove ) { if ( ( bandIter % 2 ) != 0 ) { continue; } }
        
        //==================================== Ignore l's designated to be ignored
        toBeIgnored                           = false;
        for ( unsigned int igIt = 0; igIt < static_cast<unsigned int> ( this->_lsToIgnore.size() ); igIt++ ) { if ( bandIter == static_cast<unsigned int> ( this->_lsToIgnore.at(igIt) ) ) { toBeIgnored = true; } }
        if ( toBeIgnored ) { continue; }
        
        //==================================== Get wigner norm factor
        wigNorm                               = 2.0 * M_PI * sqrt ( 2.0 / (2.0 * bandIter + 1.0 ) ) ;
        
        //==================================== For each order
        for ( unsigned int ordIter = 0; ordIter < static_cast<unsigned int> ( ( bandIter  * 2 ) + 1 ); ordIter++ )
        {
            //================================ Set the sign
            if ( ( ordIter-bandIter + bandIter ) % 2 ) { signValue = -1.0 ; }
            else                                       { signValue =  1.0 ; }
            
            //================================ For each order2
            for ( unsigned int ordIter2 = 0; ordIter2 < static_cast<unsigned int> ( ( bandIter  * 2 ) + 1 ); ordIter2++ )
            {
                //============================ Find output index
                indexO                        = so3CoefLoc ( ordIter-bandIter,
                                                             ordIter2-bandIter,
                                                             bandIter,
                                                             this->_bandwidthLimit );
                
                //============================ Use E Matrix value (it is the integral over radii of c x c*, which is exactly what is needed here)
                this->_so3Coeffs[indexO][0]   = this->_trSigmaEMatrix[bandUsageIndex][ordIter][ordIter2][0] * wigNorm * signValue;
                this->_so3Coeffs[indexO][1]   = this->_trSigmaEMatrix[bandUsageIndex][ordIter][ordIter2][1] * wigNorm * signValue;
                
                //============================ Iterate sign value
                signValue                    *= -1.0;
            }
        }
        bandUsageIndex                       += 1;
    }
    
    //======================================== Allocate memory for the inverse SO(3) transform
    this->_so3InvCoeffs                       = reinterpret_cast<fftw_complex*> ( fftw_malloc ( sizeof ( fftw_complex ) *  8 * pow( static_cast<double> ( this->_bandwidthLimit ), 3.0 ) ) );
    this->_so3Workspace1                      = reinterpret_cast<fftw_complex*> ( fftw_malloc ( sizeof ( fftw_complex ) *  8 * pow( static_cast<double> ( this->_bandwidthLimit ), 3.0 ) ) );
    this->_so3Workspace2                      = reinterpret_cast<fftw_complex*> ( fftw_malloc ( sizeof ( fftw_complex ) * 14 * pow( static_cast<double> ( this->_bandwidthLimit ), 2.0 ) + 48 * this->_bandwidthLimit ) );
    this->_so3Workspace3                      = new double [static_cast<unsigned int> ( 24 * this->_bandwidthLimit + 2 * pow( static_cast<double> ( this->_bandwidthLimit ), 2.0 ) )];
    
    //======================================== Inverse SO(3) FFTW plan
    fftw_plan inverseSO3;
    
    int howmany                               = 4 * this->_bandwidthLimit * this->_bandwidthLimit;
    int idist                                 = 2 * this->_bandwidthLimit;
    int odist                                 = 2 * this->_bandwidthLimit;
    int rank                                  = 2;
    
    int inembed[2], onembed[2];
    inembed[0]                                = 2 * this->_bandwidthLimit;
    inembed[1]                                = 4 * this->_bandwidthLimit * this->_bandwidthLimit;
    onembed[0]                                = 2 * this->_bandwidthLimit;
    onembed[1]                                = 4 * this->_bandwidthLimit * this->_bandwidthLimit;
    
    int istride                               = 1;
    int ostride                               = 1;
    
    int na[2];
    na[0]                                     = 1;
    na[1]                                     = 2 * this->_bandwidthLimit;;
    
    inverseSO3                                = fftw_plan_many_dft ( rank,
                                                                     na,
                                                                     howmany,
                                                                     this->_so3Workspace1,
                                                                     inembed,
                                                                     istride,
                                                                     idist,
                                                                     this->_so3InvCoeffs,
                                                                     onembed,
                                                                     ostride,
                                                                     odist,
                                                                     FFTW_FORWARD,
                                                                     FFTW_ESTIMATE );
    
    //======================================== Inverse SO(3) Transform computation
    Inverse_SO3_Naive_fftw                    ( this->_bandwidthLimit,
                                                this->_so3Coeffs,
                                                this->_so3InvCoeffs,
                                                this->_so3Workspace1,
                                                this->_so3Workspace2,
                                                this->_so3Workspace3,
                                               &inverseSO3,
                                                0 ) ;
    
    //======================================== Free memory
    fftw_free                                 ( this->_so3Coeffs       );
    fftw_free                                 ( this->_so3Workspace1   );
    fftw_free                                 ( this->_so3Workspace2   );
    delete this->_so3Workspace3;
    
    this->_so3Coeffs                          = nullptr;
    this->_so3Workspace1                      = nullptr;
    this->_so3Workspace2                      = nullptr;
    this->_so3Workspace3                      = nullptr;
    
    fftw_destroy_plan                         ( inverseSO3 );
    
    //============== Done
    this->_so3InvMapComputed                  = true;

}

/*! \brief This function is responsible for computing the SO3 inverse transform.
 
 The SO3 transform and its inverse are required by the distances computing class to obtain
 the best angles at which the two structures overlay, so that the full rotation function
 distance could be computed using these.
 
 \param[in] settings The settings class container with information about how to print the HTML report.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_compareOneAgainstAll::getSO3InverseMap ( ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Sanity check
    if ( !this->_trSigmaPreComputed )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Finding the optimal overlay requires the data from pre-computation of the TrSigma descriptor. This is not really logical for the user, but it saves a lot of time in case both overlay and TrSigma are to be computed. Therefore, you need to call the precomputeTrSigmaDescriptor function. Sorry about the inconvenience." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Finding the optimal overlay requires the data from pre-computation of the integral over the spheres (E matrices), which has not been done. This looks like an internal bug, please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Initialise internal values
    this->_so3InverseCoeffs                   = std::vector<fftw_complex*> ( this->all->size() );
    std::vector<fftw_complex*> _so3Coeffs     = std::vector<fftw_complex*> ( this->all->size() );
    
    //======================================== Initialise local variables
    double wigNorm                            = 0.0;
    unsigned int indexO                       = 0;
    double signValue                          = 1.0;
    bool toBeIgnored                          = false;
    unsigned int bandUsageIndex               = 0;
    unsigned int fileUsageIndex               = 0;
    unsigned int localComparisonBandLim       = 0;
    
    //======================================== Combine the coefficients into SO(3) Array
    for ( unsigned int strIt = 0; strIt < static_cast<unsigned int> ( this->all->size() ); strIt++ )
    {
        //==================================== If not needed, do not compute
        if ( this->_enLevelsDoNotFollow.at(strIt) == 1 ) { continue; }
        if ( this->_trSigmaDoNotFollow.at(strIt)  == 1 ) { fileUsageIndex += 1; continue; }
        
        //==================================== Find the appropriate bandwidth
        localComparisonBandLim                = std::min( this->one->_bandwidthLimit, this->all->at(strIt)->_bandwidthLimit );
        
        //==================================== Allocate memory for results
        _so3Coeffs.at(strIt)                  = reinterpret_cast<fftw_complex*> ( fftw_malloc ( sizeof ( fftw_complex ) * ( ( 4 * pow( static_cast<double> ( localComparisonBandLim ), 3.0 ) - static_cast<double> ( localComparisonBandLim ) ) / 3.0 ) ) );
        
        //==================================== Set all coeffs to 0
        memset                                ( _so3Coeffs.at(strIt),
                                                0.0,
                                                sizeof ( fftw_complex ) * totalCoeffs_so3 ( localComparisonBandLim ) );
        
        bandUsageIndex                        = 0;
        for ( unsigned int bandIter = 0; bandIter < localComparisonBandLim; bandIter++ )
        {
            //================================ Ignore odd l's as they are 0 anyway
            if ( !this->_keepOrRemove ) { if ( ( bandIter % 2 )  != 0 ) { continue; } }
            
            //================================ Ignore l's designated to be ignored
            toBeIgnored                       = false;
            for ( unsigned int igIt = 0; igIt < static_cast<unsigned int> ( this->_lsToIgnore.size() ); igIt++ ) { if ( bandIter == static_cast<unsigned int> ( this->_lsToIgnore.at(igIt) ) ) { toBeIgnored = true; } }
            if ( toBeIgnored ) { continue; }
            
            //================================ Get wigner norm factor
            wigNorm                           = 2.0 * M_PI * sqrt ( 2.0 / (2.0 * bandIter + 1.0 ) ) ;
            
            //================================ For each order
            for ( unsigned int ordIter = 0; ordIter < static_cast<unsigned int> ( ( bandIter  * 2 ) + 1 ); ordIter++ )
            {
                //============================ Set the sign
                if ( ( ordIter-bandIter + bandIter ) % 2 ) { signValue = -1.0 ; }
                else                                       { signValue =  1.0 ; }
                
                //============================ For each order2
                for ( unsigned int ordIter2 = 0; ordIter2 < static_cast<unsigned int> ( ( bandIter  * 2 ) + 1 ); ordIter2++ )
                {
                    //======================== Find output index
                    indexO                    = so3CoefLoc ( ordIter-bandIter, ordIter2-bandIter, bandIter, localComparisonBandLim );
                    
                    //======================== Use E Matrix value (it is the integral over radii of c x c*, which is exactly what is needed here)
                    _so3Coeffs.at(strIt)[indexO][0] = this->_EMatrices.at(fileUsageIndex).at(bandUsageIndex).at(ordIter).at(ordIter2)[0] * wigNorm * signValue;
                    _so3Coeffs.at(strIt)[indexO][1] = this->_EMatrices.at(fileUsageIndex).at(bandUsageIndex).at(ordIter).at(ordIter2)[1] * wigNorm * signValue;
                    
                    //======================== Iterate sign value
                    signValue                *= -1.0;
                }
            }
            bandUsageIndex                   += 1;
        }
        fileUsageIndex                       += 1;
    }
    
    //======================================== Compute the inverse SO3 transform
    for ( unsigned int strIt = 0; strIt < static_cast<unsigned int> ( this->all->size() ); strIt++ )
    {
        //==================================== If not needed, do not compute
        if ( this->_enLevelsDoNotFollow.at(strIt) == 1 ) { continue; }
        if ( this->_trSigmaDoNotFollow.at(strIt)  == 1 ) { continue; }
        
        //==================================== Find the appropriate bandwidth
        localComparisonBandLim                = std::min( this->one->_bandwidthLimit, this->all->at(strIt)->_bandwidthLimit );
        
        //==================================== Allocate memory for the inverse SO(3) transform
        fftw_complex *_so3Workspace1          = reinterpret_cast<fftw_complex*> ( fftw_malloc ( sizeof ( fftw_complex ) *  8 * pow( static_cast<double> ( localComparisonBandLim ), 3.0 ) ) );
        fftw_complex *_so3Workspace2          = reinterpret_cast<fftw_complex*> ( fftw_malloc ( sizeof ( fftw_complex ) * 14 * pow( static_cast<double> ( localComparisonBandLim ), 2.0 ) + 48 * localComparisonBandLim ) );
        double *_so3Workspace3                = new double [static_cast<unsigned int> ( 24 * localComparisonBandLim + 2 * pow( static_cast<double> ( localComparisonBandLim ), 2.0 ) )];
        
        
        this->_so3InverseCoeffs.at(strIt)     = reinterpret_cast<fftw_complex*> ( fftw_malloc ( sizeof ( fftw_complex ) *  8 * pow( static_cast<double> ( localComparisonBandLim ), 3.0 ) ) );
        
        //==================================== Inverse SO(3) FFTW plan
        fftw_plan inverseSO3;
        
        int howmany                           = 4 * localComparisonBandLim * localComparisonBandLim;
        int idist                             = 2 * localComparisonBandLim;
        int odist                             = 2 * localComparisonBandLim;
        int rank                              = 2;
        
        int inembed[2], onembed[2];
        inembed[0]                            = 2 * localComparisonBandLim;
        inembed[1]                            = 4 * localComparisonBandLim * localComparisonBandLim;
        onembed[0]                            = 2 * localComparisonBandLim;
        onembed[1]                            = 4 * localComparisonBandLim * localComparisonBandLim;
        
        int istride                           = 1;
        int ostride                           = 1;
        
        int na[2];
        na[0]                                 = 1;
        na[1]                                 = 2 * localComparisonBandLim;
        
        inverseSO3                            = fftw_plan_many_dft ( rank,
                                                                     na,
                                                                     howmany,
                                                                     _so3Workspace1,
                                                                     inembed,
                                                                     istride,
                                                                     idist,
                                                                     this->_so3InverseCoeffs.at(strIt),
                                                                     onembed,
                                                                     ostride,
                                                                     odist,
                                                                     FFTW_FORWARD,
                                                                     FFTW_ESTIMATE );
        
        //==================================== Inverse SO(3) Transform computation
        Inverse_SO3_Naive_fftw                ( localComparisonBandLim,
                                                _so3Coeffs.at(strIt),
                                                this->_so3InverseCoeffs.at(strIt),
                                                _so3Workspace1,
                                                _so3Workspace2,
                                                _so3Workspace3,
                                               &inverseSO3,
                                                0 ) ;
        
        //==================================== Free memory
        fftw_destroy_plan                     ( inverseSO3 );
        fftw_free                             ( _so3Workspace1   );
        fftw_free                             ( _so3Workspace2   );
        fftw_free                             ( _so3Coeffs.at(strIt) );
        delete[] _so3Workspace3;
    }
    
    //======================================== Done
    this->_so3InvMapComputed                  = true;
    
}

/*! \brief This function is used to set the Euler angles for further processing.
 
 This function is called when the proper rotation between two structures has already been determined to
 set this rotation for further processing, specifically computation of Wigner D matrices using these.
 Except for setting the internal variables, it makes sure the sanity checks further down the processing
 pipeline are passed, so it needs to be used instead of setting the internal variables directly.
 
 \param[in] alpha The alpha angle of the optimal overlay Euler angles set.
 \param[in] beta The beta angle of the optimal overlay Euler angles set.
 \param[in] gamma The gamma angle of the optimal overlay Euler angles set.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_comparePairwise::setEulerAngles ( double alpha, double beta, double gamma )
{
    //======================================== Set
    this->_eulerAngles[0]                     = alpha;
    this->_eulerAngles[1]                     = beta;
    this->_eulerAngles[2]                     = gamma;
    
    //======================================== Angles set
    this->_eulerAnglesFound                   = true;
    
    //======================================== Done
    return;
    
}

/*! \brief This function finds the highest peak in the SO3 inverse transform map and sets it as the optimal overlay angle.
 
 This function simply finds the highest peak in the SO3 inverse transform map, converst the coordinates to ZXZ Euler angles and
 sets these as the optimal overlay angles. It does not check for the zero-angle peaks, as it assumes the two strucrures are
 different in which case the zero-angle peaks are valied as well. It should be used only for distances computation and never
 for symmetry detection purposes.
 
 \param[in] settings The settings class container with information about how to print the HTML report.
 \param[in] correlation The maximum peak height for the detected best overlay angles.
 \param[out] X This function returns a vector of Euler angles (array of three numbers) for all structures which are compared to the single structure in multiple structure comparison.
 
 \warning This is an internal function which should not be used by the user.
 */
std::array<double,3> ProSHADE_internal::ProSHADE_comparePairwise::getEulerAngles ( ProSHADE::ProSHADE_settings* settings, double* correlation )
{
    //======================================== If done, do not repeat
    if ( this->_eulerAnglesFound )
    {
        return ( this->_eulerAngles );
    }
    
    //======================================== Sanity check
    if ( !this->_so3InvMapComputed )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file compatison !!! Attempted to get Euler angles without pre-computing the required data (SO3 inverse transform). Call the getSO3InverseMap function BEFORE calling the getEulerAngles function." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Attempted to get Euler angles without pre-computing the required data (SO3 inverse transform). This looks like an internal bug, please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Initialise global variable
    this->_eulerAngles                        = std::array< double, 3 > { { 0.0, 0.0, 0.0 } };

    //======================================== Find the maximum value and determine the Euler angles
    unsigned int maxLoc                       = 0;
    double maxVal                             = 0.0;
    double tmpVal                             = 0.0;
    
    for ( unsigned int iter = 0; iter < ( 8 * pow( static_cast<unsigned int> ( this->_bandwidthLimit ), 3 ) ); iter++ )
    {
        tmpVal                                = pow( this->_so3InvCoeffs[iter][0], 2.0 ) + pow( this->_so3InvCoeffs[iter][1], 2.0 );
        if ( maxVal < tmpVal )
        {
            maxVal                            = tmpVal;
            maxLoc                            = iter;
        }
    }
    
    //======================================== Save max peak height if required
   *correlation                               = maxVal;
    
    //======================================== Determine the Euler angles
    int ii, jj, kk, tmp;
    ii                                        = std::floor ( maxLoc / ( 4.0 * this->_bandwidthLimit * this->_bandwidthLimit ) );
    tmp                                       = maxLoc - ( ii * ( 4.0 * this->_bandwidthLimit * this->_bandwidthLimit ) );
    jj                                        = std::floor ( tmp / ( 2.0 * this->_bandwidthLimit ) );
    kk                                        = maxLoc - ( ii * ( 4.0 * this->_bandwidthLimit * this->_bandwidthLimit ) ) -
                                                           jj * ( 2.0 * this->_bandwidthLimit );
    
    this->_eulerAngles[2]                     = ( M_PI * jj / ( static_cast<double> ( this->_bandwidthLimit ) ) );
    this->_eulerAngles[1]                     = ( M_PI * ( 2.0 * ii + 1.0 ) / ( 4.0 * this->_bandwidthLimit ) )  ;
    this->_eulerAngles[0]                     = ( M_PI * kk / ( static_cast<double> ( this->_bandwidthLimit ) ) );
    
    //==================================== Free memory
    fftw_free                                 ( this->_so3InvCoeffs );
    this->_so3InvCoeffs                       = nullptr;
    
    //======================================== Done
    this->_eulerAnglesFound                   = true;
    
    //======================================== Return
    return ( this->_eulerAngles );
    
}

/*! \brief This function finds the highest peak in the SO3 inverse transform map and sets it as the optimal overlay angle.
 
 This function simply finds the highest peak in the SO3 inverse transform map, converst the coordinates to ZXZ Euler angles and
 sets these as the optimal overlay angles. It does not check for the zero-angle peaks, as it assumes the two strucrures are
 different in which case the zero-angle peaks are valied as well. It should be used only for distances computation and never
 for symmetry detection purposes.
 
 \param[in] settings The settings class container with information about how to print the HTML report.
 \param[out] X This function returns a vector of Euler angles (array of three numbers) for all structures which are compared to the single structure in multiple structure comparison.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::array<double,3> > ProSHADE_internal::ProSHADE_compareOneAgainstAll::getEulerAngles ( ProSHADE::ProSHADE_settings* settings )
{
    //======================================== If done, do not repeat
    if ( this->_eulerAnglesFound )
    {
        return ( this->_eulerAngles );
    }

    //======================================== Sanity check
    if ( !this->_so3InvMapComputed )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file compatison !!! Attempted to get Euler angles without pre-computing the required data (SO3 inverse transform). Call the getSO3InverseMap function BEFORE calling the getEulerAngles function." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Attempted to get Euler angles without pre-computing the required data (SO3 inverse transform). This looks like an internal bug, please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Initialise global variable
    this->_eulerAngles                        = std::vector< std::array<double,3> > ( this->all->size() );

    //======================================== Find the maximum value and determine the Euler angles
    unsigned int maxLoc                       = 0;
    double maxVal                             = 0.0;
    double tmpVal                             = 0.0;
    unsigned int localComparisonBandLim       = 0;

    for ( unsigned int strIt = 0; strIt < static_cast<unsigned int> ( this->all->size() ); strIt++ )
    {
        //==================================== If not needed, do not compute
        if ( this->_enLevelsDoNotFollow.at(strIt) == 1 ) { continue; }
        if ( this->_trSigmaDoNotFollow.at(strIt)  == 1 ) { continue; }
        
        //==================================== Find maximum peak
        maxLoc                                = 0;
        maxVal                                = 0.0;
        tmpVal                                = 0.0;
        
        //==================================== Find the appropriate bandwidth
        localComparisonBandLim                = std::min ( this->one->_bandwidthLimit, this->all->at(strIt)->_bandwidthLimit );

        for ( unsigned int iter = 0; iter < ( 8 * pow( static_cast<unsigned int> ( localComparisonBandLim ), 3 ) ); iter++ )
        {
            tmpVal                            = pow( this->_so3InverseCoeffs.at(strIt)[iter][0], 2.0 ) + pow( this->_so3InverseCoeffs.at(strIt)[iter][1], 2.0 );
            if ( maxVal < tmpVal )
            {
                maxVal                        = tmpVal;
                maxLoc                        = iter;
            }
        }

        //==================================== Determine the Euler angles
        int ii, jj, kk, tmp;
        ii                                    = std::floor ( maxLoc / ( 4.0 * localComparisonBandLim * localComparisonBandLim ) );
        tmp                                   = maxLoc - ( ii * ( 4.0 * localComparisonBandLim * localComparisonBandLim ) );
        jj                                    = std::floor ( tmp / ( 2.0 * localComparisonBandLim ) );
        kk                                    = maxLoc - ( ii * ( 4.0 * localComparisonBandLim * localComparisonBandLim ) ) -
                                                           jj * ( 2.0 * localComparisonBandLim );

        this->_eulerAngles.at(strIt)[2]       = ( M_PI * jj / ( static_cast<double> ( localComparisonBandLim ) ) );
        this->_eulerAngles.at(strIt)[1]       = ( M_PI * ( 2.0 * ii + 1.0 ) / ( 4.0 * localComparisonBandLim ) )  ;
        this->_eulerAngles.at(strIt)[0]       = ( M_PI * kk / ( static_cast<double> ( localComparisonBandLim ) ) );
        
        //==================================== Free memory
        fftw_free                             ( this->_so3InverseCoeffs.at(strIt) );
        this->_so3InverseCoeffs.at(strIt)     = nullptr;
    }

    //======================================== Done
    this->_eulerAnglesFound                   = true;

    //======================================== Return
    return ( this->_eulerAngles );

}

/*! \brief This function detects 'true' peaks from the background of the SO3 inverse transform map.
 
 This function looks through the SO3 inverse map and searches for peaks. This is done by first finding all
 peaks (i.e. values which are higher than all neighouring values) and then determining background threshold
 so that only 'true' peaks are processed. It then optimises the 'true' peaks and converts them onto the
 angle-axis representation. This if finally returned by the function.
 
 \param[in] settings The ProSHADE_settings class container with infromation about the HTML report output, if needed.
 \param[in] noIQRs The number of interquartile ranges to determine the peak threshold.
 \param[in] freeMem Should the memory be released, or will there be more peak operations to follow?
 \param[in] peakSize How many neighbours in each direction need to be less than the cental value to be considered a peak?
 \param[in] realDist The full rotation function descriptor value which needs to be surpassed by a 'missing' peak in order to be 'found'.
 \param[in] verbose Int value specifying how quiet (0) or loud (4) the function should be when reporting progress.
 \param[out] X A list of peaks with high overlay value.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::array<double,8> > ProSHADE_internal::ProSHADE_comparePairwise::getSO3Peaks ( ProSHADE::ProSHADE_settings* settings,
                                                                                               double noIQRs,
                                                                                               bool freeMem,
                                                                                               int peakSize,
                                                                                               double realDist,
                                                                                               int verbose )
{
    //======================================== Sanity check
    if ( !this->_so3InvMapComputed )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file compatison !!! Attempted to get Euler angles without pre-computing the required data (SO3 inverse transform). Call the getSO3InverseMap function BEFORE calling the getSO3Peaks function." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Attempted to get Euler angles without pre-computing the required data (SO3 inverse transform). This looks like an internal bug, please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    if ( this->_so3InvCoeffs == nullptr )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in function getSO3Peaks. The pointer to the SO3 data is empty. Common cause is when you call this function more than once, or when you call the getEulerAngles function before this one. If either is the case, please add \"false\" as an additional parameter to all previous calls of both functions." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "The pointer to the SO3 data is empty. This looks like an internal bug, please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Initialise local variables
    int x                                     = 0;
    int y                                     = 0;
    int z                                     = 0;
    int newIter                               = 0;
    int xDim                                  = 4 * pow ( this->_bandwidthLimit, 2 );
    int yDim                                  = 2 * this->_bandwidthLimit;
    
    std::array<double,8> hlpArr;
    std::vector< std::array<double,8> > peakList;
    std::array<double,4> hlpInterp;
    std::vector< std::array<double,4> > peakInterpVals;
    std::vector< std::vector< std::array<double,4> > > peakInterpValsList;
    std::vector< std::vector<double> > tMat;
    std::vector< std::vector<double> > avgMat;
    std::vector< std::vector< std::vector<double> > > uAndVMat;
    std::vector< std::array<double,4> > rotAxes;
    std::array<double,4> curAxis;
    std::vector<double> hlpVec;
    std::vector< std::array<double,8> > ret;
    std::vector<double> peakHeights;
    
    double tmpVal                             = 0.0;
    double angAlpha                           = 0.0;
    double angBeta                            = 0.0;
    double angGamma                           = 0.0;
    double singularityErrTolerance            = 0.05;
    double avgMatWeight                       = 0.0;
    double rotVal                             = 0.0;
    double median                             = 0.0;
    double interQuartileRange                 = 0.0;
    
    int peakX                                 = 0;
    int peakY                                 = 0;
    int peakZ                                 = 0;
    
    unsigned int medianPos                    = 0;
    
    bool breakPeak                            = false;
    
    //======================================== Find peak heights
    for ( unsigned int iter = 0; iter < ( 8 * pow( static_cast<unsigned int> ( this->_bandwidthLimit ), 3 ) ); iter++ )
    {
        //==================================== Find point value
        tmpVal                                = pow( this->_so3InvCoeffs[iter][0], 2.0 ) + pow( this->_so3InvCoeffs[iter][1], 2.0 );
        
        //==================================== Find the x, y and z
        x                                     = std::floor ( iter / xDim );
        y                                     = std::floor ( ( iter - x * xDim ) / yDim );
        z                                     = iter - x * xDim - y * yDim;
        
        //==================================== Get the X surrounding values as well as the peak
        breakPeak                             = false;
        for ( int xCh = -peakSize; xCh <= +peakSize; xCh++ )
        {
            if ( breakPeak ) { break; }
            for ( int yCh = -peakSize; yCh <= +peakSize; yCh++ )
            {
                if ( breakPeak ) { break; }
                for ( int zCh = -peakSize; zCh <= +peakSize; zCh++ )
                {
                    if ( breakPeak ) { break; }
                    if ( ( xCh == 0 ) && ( yCh == 0 ) && ( zCh == 0 ) ) { continue; }
                    
                    peakX                     = x + xCh; if ( peakX >= yDim ) { peakX -= yDim; }; if ( peakX < 0 ) { peakX += yDim; }
                    peakY                     = y + yCh; if ( peakY >= yDim ) { peakY -= yDim; }; if ( peakY < 0 ) { peakY += yDim; }
                    peakZ                     = z + zCh; if ( peakZ >= yDim ) { peakZ -= yDim; }; if ( peakZ < 0 ) { peakZ += yDim; }
                    newIter                   = peakX * xDim + peakY * yDim + peakZ;

                    if ( (pow( this->_so3InvCoeffs[newIter][0], 2.0 ) + pow( this->_so3InvCoeffs[newIter][1], 2.0 )) > tmpVal ) { breakPeak = true; break; }
                }
            }
        }
        if ( breakPeak ) { continue; }
        
        //==================================== Find Euler angles
        angGamma                              = ( M_PI * y / ( static_cast<double> ( this->_bandwidthLimit ) ) );
        angBeta                               = ( M_PI * ( 2.0 * x + 1.0 ) / ( 4.0 * this->_bandwidthLimit ) )  ;
        angAlpha                              = ( M_PI * z / ( static_cast<double> ( this->_bandwidthLimit ) ) );
        
        //==================================== Convert ZXZ Euler angle ranges
        if ( angAlpha > M_PI ) { angAlpha = angAlpha - M_PI * 2.0; }
        if ( angBeta  > M_PI ) { angBeta  = angBeta  - M_PI * 2.0; }
        if ( angGamma > M_PI ) { angGamma = angGamma - M_PI * 2.0; }
        
        //==================================== Remove 0 angle peaks
        ProSHADE_internal_misc::getAxisAngleFromEuler  ( angAlpha, angBeta, angGamma, &median, &median, &median, &rotVal, true );
        if ( ( rotVal < ( 0.0 + singularityErrTolerance ) ) && ( rotVal > ( 0.0 - singularityErrTolerance ) ) ) { continue; }
        
        //==================================== Save the peak heights to determine threshold
        peakHeights.emplace_back ( tmpVal );
    }
    
    //======================================== Find the threshold for peaks
    medianPos                                 = static_cast<unsigned int> ( peakHeights.size() / 2 );
    std::sort                                 ( peakHeights.begin(), peakHeights.end() );
    median                                    = peakHeights.at(medianPos);
    
    interQuartileRange                        = peakHeights.at(medianPos+(medianPos/2)) - peakHeights.at(medianPos-(medianPos/2));
    this->_peakHeightThr                      = peakHeights.at(medianPos+(medianPos/2)) + ( interQuartileRange * noIQRs );
    
    //======================================== Find peaks
    for ( unsigned int iter = 0; iter < ( 8 * pow( static_cast<unsigned int> ( this->_bandwidthLimit ), 3 ) ); iter++ )
    {
        //==================================== Find point value
        tmpVal                                = pow( this->_so3InvCoeffs[iter][0], 2.0 ) + pow( this->_so3InvCoeffs[iter][1], 2.0 );
        
        //==================================== If below threshold, go to next
        if ( tmpVal < this->_peakHeightThr ) { continue; }
        
        //==================================== Find the x, y and z
        x                                     = std::floor ( iter / xDim );
        y                                     = std::floor ( ( iter - x * xDim ) / yDim );
        z                                     = iter - x * xDim - y * yDim;
        
        //==================================== Initialise interpolation values
        peakInterpVals.clear                  ( );
        
        //==================================== Get the X surrounding values as well as the peak
        breakPeak                             = false;
        for ( int xCh = -peakSize; xCh <= +peakSize; xCh++ )
        {
            if ( breakPeak ) { break; }
            for ( int yCh = -peakSize; yCh <= +peakSize; yCh++ )
            {
                if ( breakPeak ) { break; }
                for ( int zCh = -peakSize; zCh <= +peakSize; zCh++ )
                {
                    if ( breakPeak ) { break; }
                    if ( ( xCh == 0 ) && ( yCh == 0 ) && ( zCh == 0 ) ) { continue; }
                    
                    hlpArr[0]                 = x + xCh; if ( hlpArr[0] >= yDim ) { hlpArr[0] -= yDim; }; if ( hlpArr[0] < 0 ) { hlpArr[0] += yDim; }
                    hlpArr[1]                 = y + yCh; if ( hlpArr[1] >= yDim ) { hlpArr[1] -= yDim; }; if ( hlpArr[1] < 0 ) { hlpArr[1] += yDim; }
                    hlpArr[2]                 = z + zCh; if ( hlpArr[2] >= yDim ) { hlpArr[2] -= yDim; }; if ( hlpArr[2] < 0 ) { hlpArr[2] += yDim; }
                    newIter                   = hlpArr[0] * xDim + hlpArr[1] * yDim + hlpArr[2];
                    hlpArr[3]                 = pow( this->_so3InvCoeffs[newIter][0], 2.0 ) + pow( this->_so3InvCoeffs[newIter][1], 2.0 );
                    if ( hlpArr[3] > tmpVal ) { breakPeak = true; break; }
                    hlpInterp[0]              = hlpArr[0]; hlpInterp[1] = hlpArr[1]; hlpInterp[2] = hlpArr[2]; hlpInterp[3] = hlpArr[3];
                    peakInterpVals.emplace_back ( hlpInterp );
                }
            }
        }
        if ( breakPeak ) { continue; }

        //==================================== This value is surrounded by smaller values! Check if it is not a 0 angle
        angGamma                              = ( M_PI * y / ( static_cast<double> ( this->_bandwidthLimit ) ) );
        angBeta                               = ( M_PI * ( 2.0 * x + 1.0 ) / ( 4.0 * this->_bandwidthLimit ) )  ;
        angAlpha                              = ( M_PI * z / ( static_cast<double> ( this->_bandwidthLimit ) ) );
        
        ProSHADE_internal_misc::getAxisAngleFromEuler ( angAlpha, angBeta, angGamma, &rotVal, &rotVal, &rotVal, &rotVal, true );
        if ( ( rotVal < ( 0.0 + singularityErrTolerance ) ) && ( rotVal > ( 0.0 - singularityErrTolerance ) ) ) { continue; }
        
        //==================================== OK, now check if this peak actually leads to something similar
        if ( this->checkPeakExistence ( angAlpha, angBeta, angGamma, settings, realDist ) )
        {
            hlpArr[0]                         = x + 0; if ( hlpArr[0] >= yDim ) { hlpArr[0] -= yDim; }; if ( hlpArr[0] < 0 ) { hlpArr[0] += yDim; }
            hlpArr[1]                         = y + 0; if ( hlpArr[1] >= yDim ) { hlpArr[1] -= yDim; }; if ( hlpArr[1] < 0 ) { hlpArr[1] += yDim; }
            hlpArr[2]                         = z + 0; if ( hlpArr[2] >= yDim ) { hlpArr[2] -= yDim; }; if ( hlpArr[2] < 0 ) { hlpArr[2] += yDim; }
            newIter                           = hlpArr[0] * xDim + hlpArr[1] * yDim + hlpArr[2];
            hlpArr[3]                         = pow( this->_so3InvCoeffs[newIter][0], 2.0 ) + pow( this->_so3InvCoeffs[newIter][1], 2.0 );
            peakList.emplace_back             ( hlpArr );
            hlpInterp[0]                      = hlpArr[0];
            hlpInterp[1]                      = hlpArr[1];
            hlpInterp[2]                      = hlpArr[2];
            hlpInterp[3]                      = hlpArr[3];
            peakInterpVals.emplace_back       ( hlpInterp );
            peakInterpValsList.emplace_back   ( peakInterpVals );
        }
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>> " << peakList.size() << " peaks detected." << std::endl;
    }

    //======================================== Free memory
    if ( freeMem )
    {
        fftw_free                             ( this->_so3InvCoeffs );
        this->_so3InvCoeffs                   = nullptr;
    }
    
    //======================================== Interpolate angles
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( peakList.size() ); iter++ )
    {
        //==================================== Init
        avgMat.clear                          ();
        hlpVec.clear                          ();
        hlpVec.emplace_back                   ( 0.0 );
        hlpVec.emplace_back                   ( 0.0 );
        hlpVec.emplace_back                   ( 0.0 );
        avgMat.emplace_back                   ( hlpVec );
        avgMat.emplace_back                   ( hlpVec );
        avgMat.emplace_back                   ( hlpVec );
        
        avgMatWeight                          = 0.0;
        
        //==================================== For each surrounding value (including the peak value)
        for ( unsigned int valIter = 0; valIter < static_cast<unsigned int> ( peakInterpValsList.at(iter).size() ); valIter++ )
        {
            //================================ Get angles
            angGamma                          = ( M_PI * peakInterpValsList.at(iter).at(valIter)[1] / ( static_cast<double> ( this->_bandwidthLimit ) ) );
            angBeta                           = ( M_PI * ( 2.0 * peakInterpValsList.at(iter).at(valIter)[0] + 1.0 ) / ( 4.0 * this->_bandwidthLimit ) )  ;
            angAlpha                          = ( M_PI * peakInterpValsList.at(iter).at(valIter)[2] / ( static_cast<double> ( this->_bandwidthLimit ) ) );
            
            //================================ Convert ZXZ Euler angle ranges
            if ( angAlpha > M_PI ) { angAlpha = angAlpha - M_PI * 2.0; }
            if ( angBeta  > M_PI ) { angBeta  = angBeta  - M_PI * 2.0; }
            if ( angGamma > M_PI ) { angGamma = angGamma - M_PI * 2.0; }
            
            //================================ Init
            tMat.clear                        ( );
            
            //================================ Get rotation matrices
            hlpVec.clear                      ( );
            hlpVec.emplace_back               (  cos ( angAlpha ) * cos ( angBeta  ) * cos ( angGamma ) - sin ( angAlpha ) * sin ( angGamma ) );
            hlpVec.emplace_back               (  sin ( angAlpha ) * cos ( angBeta  ) * cos ( angGamma ) + cos ( angAlpha ) * sin ( angGamma ) );
            hlpVec.emplace_back               ( -sin ( angBeta  ) * cos ( angGamma ) );
            tMat.emplace_back                 ( hlpVec );
            
            hlpVec.clear                      ( );
            hlpVec.emplace_back               ( -cos ( angAlpha ) * cos ( angBeta  ) * sin ( angGamma ) - sin ( angAlpha ) * cos ( angGamma ) );
            hlpVec.emplace_back               ( -sin ( angAlpha ) * cos ( angBeta  ) * sin ( angGamma ) + cos ( angAlpha ) * cos ( angGamma ) );
            hlpVec.emplace_back               (  sin ( angBeta  ) * sin ( angGamma ) );
            tMat.emplace_back                 ( hlpVec );
            
            hlpVec.clear                      ( );
            hlpVec.emplace_back               (  cos ( angAlpha ) * sin ( angBeta  ) );
            hlpVec.emplace_back               (  sin ( angAlpha ) * sin ( angBeta  ) );
            hlpVec.emplace_back               (  cos ( angBeta  ) );
            tMat.emplace_back                 ( hlpVec );
            
            //================================ Add to average matrix
            avgMat.at(0).at(0)               += tMat.at(0).at(0) * peakInterpValsList.at(iter).at(valIter)[3];
            avgMat.at(0).at(1)               += tMat.at(0).at(1) * peakInterpValsList.at(iter).at(valIter)[3];
            avgMat.at(0).at(2)               += tMat.at(0).at(2) * peakInterpValsList.at(iter).at(valIter)[3];
            
            avgMat.at(1).at(0)               += tMat.at(1).at(0) * peakInterpValsList.at(iter).at(valIter)[3];
            avgMat.at(1).at(1)               += tMat.at(1).at(1) * peakInterpValsList.at(iter).at(valIter)[3];
            avgMat.at(1).at(2)               += tMat.at(1).at(2) * peakInterpValsList.at(iter).at(valIter)[3];
            
            avgMat.at(2).at(0)               += tMat.at(2).at(0) * peakInterpValsList.at(iter).at(valIter)[3];
            avgMat.at(2).at(1)               += tMat.at(2).at(1) * peakInterpValsList.at(iter).at(valIter)[3];
            avgMat.at(2).at(2)               += tMat.at(2).at(2) * peakInterpValsList.at(iter).at(valIter)[3];
            
            //================================ and to average weight
            avgMatWeight                     += peakInterpValsList.at(iter).at(valIter)[3];
        }
        
        //==================================== Normalise average mats by weights
        avgMat.at(0).at(0)                   /= avgMatWeight;
        avgMat.at(0).at(1)                   /= avgMatWeight;
        avgMat.at(0).at(2)                   /= avgMatWeight;
        
        avgMat.at(1).at(0)                   /= avgMatWeight;
        avgMat.at(1).at(1)                   /= avgMatWeight;
        avgMat.at(1).at(2)                   /= avgMatWeight;
        
        avgMat.at(2).at(0)                   /= avgMatWeight;
        avgMat.at(2).at(1)                   /= avgMatWeight;
        avgMat.at(2).at(2)                   /= avgMatWeight;
        
        //==================================== SVD average mat
        uAndVMat                              = this->getSingularValuesUandVMatrix ( avgMat, 3, settings );
        
        //==================================== Init matrix for multiplication
        tMat.clear                            ( );
        hlpVec.clear                          ( );
        hlpVec.emplace_back                   ( 0.0 );
        hlpVec.emplace_back                   ( 0.0 );
        hlpVec.emplace_back                   ( 0.0 );
        tMat.emplace_back ( hlpVec ); tMat.emplace_back ( hlpVec ); tMat.emplace_back ( hlpVec );
        
        //==================================== Compute U * V^T
        for ( unsigned int row = 0; row < 3; row++ )
        {
            for ( unsigned int col  = 0; col < 3; col++ )
            {
                for ( unsigned int inner = 0; inner < 3; inner++ )
                {
                    tMat.at(row).at(col)     += uAndVMat.at(0).at(inner).at(row) * uAndVMat.at(1).at(col).at(inner);
                }
            }
        }
        
        //==================================== Find alpha, beta and gamma from matrix
        //=! Deal with beta angle first
        if      ( ( angBeta >= 0.0 ) )        { angBeta  =  acos ( tMat.at(2).at(2) ); }
        else                                  { angBeta  = -acos ( tMat.at(2).at(2) ); }
        
        
        //=! Now alpha
        if      ( angAlpha >  (  M_PI / 2.0 ) ) { angAlpha =  M_PI - asin ( tMat.at(2).at(1) / sin ( angBeta ) ); }
        else if ( angAlpha <  ( -M_PI / 2.0 ) ) { angAlpha = -M_PI - asin ( tMat.at(2).at(1) / sin ( angBeta ) ); }
        else                                  { angAlpha =         asin ( tMat.at(2).at(1) / sin ( angBeta ) ); }
        
        //=! Now gamma
        if      ( angGamma >  (  M_PI / 2.0 ) ) { angGamma =  M_PI - asin ( tMat.at(1).at(2) / sin ( angBeta ) ); }
        else if ( angGamma <  ( -M_PI / 2.0 ) ) { angGamma = -M_PI - asin ( tMat.at(1).at(2) / sin ( angBeta ) ); }
        else                                  { angGamma =         asin ( tMat.at(1).at(2) / sin ( angBeta ) ); }
        
        //==================================== Now, change the peak angles to interpolated angles
        peakList.at(iter)[0]                  = angAlpha;
        peakList.at(iter)[1]                  = angBeta;
        peakList.at(iter)[2]                  = angGamma;
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>> Peaks angles interpolated." << std::endl;
    }
    
    //======================================== Get axis-angle representation values
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( peakList.size() ); iter++ )
    {
        //==================================== Get angles
        angGamma                              = peakList.at(iter)[2];
        angBeta                               = peakList.at(iter)[1];
        angAlpha                              = peakList.at(iter)[0];
        
        //==================================== Find the axis-angle representation axis
        ProSHADE_internal_misc::getAxisAngleFromEuler  ( angAlpha, angBeta, angGamma, &curAxis[0], &curAxis[1], &curAxis[2], &curAxis[3] );
        
        //==================================== Save axis-angle representation results
        rotAxes.emplace_back                  ( curAxis );
    }
    
    //======================================== Save
    for ( unsigned int rot = 0; rot < static_cast<unsigned int> ( peakList.size() ); rot++ )
    {
        hlpArr[0]                             = peakList.at(rot)[0];
        hlpArr[1]                             = peakList.at(rot)[1];
        hlpArr[2]                             = peakList.at(rot)[2];
        hlpArr[3]                             = peakList.at(rot)[3];
        hlpArr[4]                             = rotAxes.at(rot)[0];
        hlpArr[5]                             = rotAxes.at(rot)[1];
        hlpArr[6]                             = rotAxes.at(rot)[2];
        hlpArr[7]                             = rotAxes.at(rot)[3];
        ret.emplace_back                      ( hlpArr );
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>> Peaks converted to angle-axis." << std::endl;
    }
    
    //======================================== Sort (descending order)
    std::sort ( ret.begin(), ret.end(), [](const std::array<double,8>& a, const std::array<double,8>& b) { return a[3] > b[3]; });
    
    //======================================== Return
    return ( ret );
    
}

/*! \brief This function determines the maximum SO3 inverse transform value given angle-axis representation values.
 
 This function is used in detecting 'missing' peaks, those are peaks which have high enough height so that they could be
 'true' peaks, but were missed due to one neighbout having even higher value. To do this, the function check the SO3 inverse
 map again, this time checking if the coordinates match the input and saving the max height.
 
 \param[in] X The X-axis element of the angle-axis rotation representation for which to check.
 \param[in] Y The Y-axis element of the angle-axis rotation representation for which to check.
 \param[in] Z The Z-axis element of the angle-axis rotation representation for which to check.
 \param[in] Angle The angle of the angle-axis rotation representation for which to check.
 \param[in] settings The ProSHADE_settings container class instance with instructions on how to write the HTML report.
 \param[out] X Value of the highest 'missing' peak found.
 
 \warning This is an internal function which should not be used by the user.
 */
double ProSHADE_internal::ProSHADE_comparePairwise::maxPeakHeightFromAngleAxis ( double X,
                                                                                 double Y,
                                                                                 double Z,
                                                                                 double Angle,
                                                                                 ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Sanity check
    if ( this->_so3InvCoeffs == nullptr )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in function checkPeakFromAngleAxis. The pointer to the SO3 data is empty. Common cause is when you call this function more than once, or when you call the getEulerAngles function before this one. If either is the case, please add \"false\" as an additional parameter to all previous calls of both functions." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "The pointer to the SO3 data is empty. This looks like an internal bug, please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Initialise
    double peakHeight                         = 0.0;
    double x                                  = 0.0;
    double y                                  = 0.0;
    double z                                  = 0.0;
    double angAlpha                           = 0.0;
    double angBeta                            = 0.0;
    double angGamma                           = 0.0;
    double axX                                = 0.0;
    double axY                                = 0.0;
    double axZ                                = 0.0;
    double angle                              = 0.0;
    
    int xDim                                  = 4 * pow ( this->_bandwidthLimit, 2 );
    int yDim                                  = 2 * this->_bandwidthLimit;
    
    double ret                                = 0.0;
    
    //======================================== Iterate through SO3 Inv data and check all coordinates which give the correct Angle-Axis representation
    for ( unsigned int iter = 0; iter < ( 8 * pow( static_cast<unsigned int> ( this->_bandwidthLimit ), 3 ) ); iter++ )
    {
        //==================================== Find point value
        peakHeight                            = pow( this->_so3InvCoeffs[iter][0], 2.0 ) + pow( this->_so3InvCoeffs[iter][1], 2.0 );
        
        //==================================== Quick check
        if ( peakHeight <= ret ) { continue; }
        
        //==================================== Find the x, y and z
        x                                     = std::floor ( iter / xDim );
        y                                     = std::floor ( ( iter - x * xDim ) / yDim );
        z                                     = iter - x * xDim - y * yDim;
        
        //==================================== Find Euler angles
        angGamma                              = ( M_PI * y / ( static_cast<double> ( this->_bandwidthLimit ) ) );
        angBeta                               = ( M_PI * ( 2.0 * x + 1.0 ) / ( 4.0 * this->_bandwidthLimit ) )  ;
        angAlpha                              = ( M_PI * z / ( static_cast<double> ( this->_bandwidthLimit ) ) );
        
        //==================================== Convert ZXZ Euler angle ranges
        if ( angAlpha > M_PI ) { angAlpha = angAlpha - M_PI * 2.0; }
        if ( angBeta  > M_PI ) { angBeta  = angBeta  - M_PI * 2.0; }
        if ( angGamma > M_PI ) { angGamma = angGamma - M_PI * 2.0; }
        
        //==================================== Convert to Angle-Axis representation and check compatibility to tested values
        ProSHADE_internal_misc::getAxisAngleFromEuler  ( angAlpha, angBeta, angGamma, &axX, &axY, &axZ, &angle );
        if ( !( ( angle < ( Angle + 0.1 ) ) && ( ( angle > ( Angle - 0.1 ) ) ) ) )
        {
            continue;
        }
        
        if ( !( ( axX < ( X + 0.1 ) ) && ( ( axX > ( X - 0.1 ) ) ) ) ||
             !( ( axY < ( Y + 0.1 ) ) && ( ( axY > ( Y - 0.1 ) ) ) ) ||
             !( ( axZ < ( Z + 0.1 ) ) && ( ( axZ > ( Z - 0.1 ) ) ) ) )
        {
            continue;
        }
        
        //==================================== Save the peak height
        if ( ret < peakHeight ) { ret = peakHeight; }
    }

    //======================================== Done
    return ( ret );
    
}

/*! \brief This function takes angle and axis and checks the SO(3) map for this specific symmetry only.
 
 This function takes angle and axis and checks the SO(3) map for this specific symmetry only.
 
 \param[in] X The X-axis element of the angle-axis rotation representation for which to check.
 \param[in] Y The Y-axis element of the angle-axis rotation representation for which to check.
 \param[in] Z The Z-axis element of the angle-axis rotation representation for which to check.
 \param[in] Angle The angle of the angle-axis rotation representation for which to check.
 \param[in] settings The ProSHADE_settings container class instance with instructions on how to write the HTML report.
 \param[out] X Value of the average peak height for the best possible axis found.
 
 \warning This is an internal function which should not be used by the user.
 */
double ProSHADE_internal::ProSHADE_comparePairwise::maxAvgPeakForSymmetry ( double X,
                                                                            double Y,
                                                                            double Z,
                                                                            double Angle,
                                                                            ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Sanity check
    if ( this->_so3InvCoeffs == nullptr )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in function maxAvgPeakForSymmetry. The pointer to the SO3 data is empty. Common cause is when you call this function more than once, or when you call the getEulerAngles function before this one. If either is the case, please add \"false\" as an additional parameter to all previous calls of both functions." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "The pointer to the SO3 data is empty. This looks like an internal bug, please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Initialise
    double x                                  = 0.0;
    double y                                  = 0.0;
    double z                                  = 0.0;
    double angAlpha                           = 0.0;
    double angBeta                            = 0.0;
    double angGamma                           = 0.0;
    double axX                                = 0.0;
    double axY                                = 0.0;
    double axZ                                = 0.0;
    double angle                              = 0.0;
    int xDim                                  = 4 * pow ( this->_bandwidthLimit, 2 );
    int yDim                                  = 2 * this->_bandwidthLimit;
    double ret                                = 0.0;
    std::vector< std::array<double,2> > peaks;
    std::array<double,2> hlpArr;
    bool xPos                                 = true; if ( X < 0 ) { xPos = false; }
    bool yPos                                 = true; if ( Y < 0 ) { yPos = false; }
    bool zPos                                 = true; if ( Z < 0 ) { zPos = false; }
    
    //======================================== Iterate through SO3 Inv data and check all coordinates which give the correct Angle-Axis representation
    for ( unsigned int iter = 0; iter < ( 8 * pow( static_cast<unsigned int> ( this->_bandwidthLimit ), 3 ) ); iter++ )
    {
        //==================================== Find the x, y and z
        x                                     = std::floor ( iter / xDim );
        y                                     = std::floor ( ( iter - x * xDim ) / yDim );
        z                                     = iter - x * xDim - y * yDim;
        
        //==================================== Find Euler angles
        angGamma                              = ( M_PI * y / ( static_cast<double> ( this->_bandwidthLimit ) ) );
        angBeta                               = ( M_PI * ( 2.0 * x + 1.0 ) / ( 4.0 * this->_bandwidthLimit ) )  ;
        angAlpha                              = ( M_PI * z / ( static_cast<double> ( this->_bandwidthLimit ) ) );
        
        //==================================== Convert to Angle-Axis representation and check compatibility to tested values
        ProSHADE_internal_misc::getAxisAngleFromEuler  ( angAlpha, angBeta, angGamma, &axX, &axY, &axZ, &angle );
        if ( !( ( std::abs( axX ) < ( std::abs( X ) + 0.1 ) ) && ( ( std::abs( axX ) > ( std::abs( X ) - 0.1 ) ) ) ) ||
             !( ( std::abs( axY ) < ( std::abs( Y ) + 0.1 ) ) && ( ( std::abs( axY ) > ( std::abs( Y ) - 0.1 ) ) ) ) ||
             !( ( std::abs( axZ ) < ( std::abs( Z ) + 0.1 ) ) && ( ( std::abs( axZ ) > ( std::abs( Z ) - 0.1 ) ) ) ) )
        {
            continue;
        }
        else
        {
            if ( ( ( xPos && ( axX > 0.0 ) ) || ( !xPos && ( axX < 0.0 ) ) ) &&
                 ( ( yPos && ( axY > 0.0 ) ) || ( !yPos && ( axY < 0.0 ) ) ) &&
                 ( ( zPos && ( axZ > 0.0 ) ) || ( !zPos && ( axZ < 0.0 ) ) ) )
            {
                //============================ Same signs
                ;
            }
            else
            {
                if ( ( ( xPos && ( axX < 0.0 ) ) || ( !xPos && ( axX > 0.0 ) ) ) &&
                     ( ( yPos && ( axY < 0.0 ) ) || ( !yPos && ( axY > 0.0 ) ) ) &&
                     ( ( zPos && ( axZ < 0.0 ) ) || ( !zPos && ( axZ > 0.0 ) ) ) )
                {
                    //======================== All opposite signs
                    angle                    *= -1.0;
                }
                else
                {
                    continue;
                }
            }
        }
        
        //==================================== Save vals
        hlpArr[0]                             = angle + M_PI;
        hlpArr[1]                             = pow( this->_so3InvCoeffs[iter][0], 2.0 ) + pow( this->_so3InvCoeffs[iter][1], 2.0 );
        peaks.emplace_back                    ( hlpArr );
    }
    
    //======================================== Sort the vector
    std::sort ( peaks.begin(), peaks.end(), [](const std::array<double,2>& a, const std::array<double,2>& b) { return a[0] < b[0]; } );
    
    //======================================== Find the best X peaks with correct distances
    double angStep                            = 0.1;
    std::vector<double> sums                  ( std::floor ( (2.0*M_PI/angStep) / Angle ) );
    double curSum                             = 0.0;
    double maxVal                             = 0.0;
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( std::floor ( (2.0*M_PI/angStep) / Angle ) ); iter++ )
    {
        curSum                                = 0.0;
        for ( unsigned int angCmb = 0; angCmb < static_cast<unsigned int> ( Angle ); angCmb++ )
        {
            maxVal                            = 0.0;
            for ( unsigned int angIt = 0; angIt < static_cast<unsigned int> ( peaks.size() ); angIt++ )
            {
                if ( peaks.at(angIt)[0] < ( ( iter*angStep )     + ( ( 2.0 * M_PI / Angle ) * angCmb ) ) ) { continue; }
                if ( peaks.at(angIt)[0] > ( ( (iter+1)*angStep ) + ( ( 2.0 * M_PI / Angle ) * angCmb ) ) ) { break; }
                
                if ( peaks.at(angIt)[1] > maxVal ) { maxVal = peaks.at(angIt)[1]; }
            }
            curSum                           += maxVal;
        }
        curSum                               /= Angle;
        if ( ret < curSum ) { ret = curSum; }
    }
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief This function attempts to detect the C-symmetries present in the list of overlay peaks.
 
 This function takes the list of SO3 inverse transform peaks (courtesy of getSO3Peaks) and searches
 for C symmetries. This is done by first grouping the peaks by the axis of rotation and then for
 each group with the same axis, repetitively finding the smallest angle between the angle values
 and checking if such symmetry is supported by the rest of the peaks.
 
 \param[in] peaks The list of SO3 inverse transform peaks as produced by the getSO3Peaks function.
 \param[in] settings The ProSHADE_settings container class instance with instructions on how to write the HTML report.
 \param[in] axisErrorTolerance Tolerance value on matching the peaks axis together.
 \param[in] freeMem Should the memory be released, or is this search going to be repeated?
 \param[in] percAllowedToMiss The percentage (0 to 1) of peaks that are allowed to be missing.
 \param[in] verbose An integer value stating how quiet (0) or loud (4) the function reports should be. In this case, it will be quiet completely unless the value is 4.
 \param[out] X List of C symmetries detected.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::array<double,5> > ProSHADE_internal::ProSHADE_comparePairwise::findCnSymmetry ( std::vector< std::array<double,8> > peaks,
                                                                                                  ProSHADE::ProSHADE_settings* settings,
                                                                                                  double axisErrorTolerance,
                                                                                                  bool freeMem,
                                                                                                  double percAllowedToMiss,
                                                                                                  int verbose )
{
    //======================================== Sanity check
    if ( peaks.size() < 1 )
    {
        std::cerr << "!!! ProSHADE ERROR !!! No peaks in the data - cannot search for symmetry. Either loosen up the criteria for the getSO3Peaks function, or conclude no symmetry." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Found no peaks in the rotation function map. This could signify no symmetry being present, or very low sampling of the map. Please try increasing resolution and bandwidth if you believe symmetry should be present." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Initialise local variables
    std::vector< std::array<double,5> > ret;
    std::vector< std::array<double,7> > grpPeaks;
    std::vector< std::array<double,7> > additionalPeaks;
    std::vector<double> angVals;
    std::array<double,7> hlpArr;
    std::array<double,5> retArr;
    bool sameAxisFound                        = false;
    bool groupContinuous                      = false;
    bool oneDifferent                         = false;
    bool matchedThisPeak                      = false;
    bool additionalPeakFound                  = false;
    double errTolerance                       = 0.0;
    double angTolerance                       = 0.0;
    double angDivisionRemainder               = 0.0;
    double angDivisionBasis                   = 0.0;
    double groupAngle                         = 0.0;
    double meanX                              = 0.0;
    double meanY                              = 0.0;
    double meanZ                              = 0.0;
    double totVals                            = 0.0;
    double meanPeak                           = 0.0;
    double nextPeakError                      = ( M_PI * 2.0 ) / ( static_cast<double> ( this->_bandwidthLimit ) * 2.0 );
    double nextSymmetryError                  = 0.0;
    double peakVal                            = 0.0;
    double minAngDist                         = 0.0;
    double currentAverage                     = 0.0;
    int peaksNeededMinimum                    = 0;
    int peaksNeededRemaining                  = 0;
    int peaksNeededFound                      = 0;
    std::vector<double> groupAngleHits;
    std::vector< std::array<double,2> > groupMembers;
    std::array<double,2> hlpArr2;
    std::vector<double> missingPeaks;
    std::vector<double> angsToTry;
    std::vector<unsigned int> thisAxisSyms;
    std::vector<unsigned int> delVecElems;
    
    std::vector< std::vector<unsigned int> > uniqueAxisGroups;
    
    if ( axisErrorTolerance == 0.0 ) { errTolerance = 0.1; }
    else                             { errTolerance = axisErrorTolerance; }
    
    //======================================== Find unique axes
    for ( unsigned int peakIter = 0; peakIter < static_cast<unsigned int> ( peaks.size() ); peakIter++ )
    {
        //==================================== If axis is 0 ; 0 ; 0 then ignore, this will be dealt with later
        if ( ( peaks.at(peakIter)[4] == 0.0 ) && ( peaks.at(peakIter)[5] == 0.0 ) && ( peaks.at(peakIter)[6] == 0.0 ) ) { continue; }
        
        //==================================== Initialise variables for next peak
        sameAxisFound                         = false;
   
        //==================================== Give same direction to maximum vector elements
        if ( ( std::abs(peaks.at(peakIter)[4]) > std::abs(peaks.at(peakIter)[5]) ) && ( std::abs(peaks.at(peakIter)[4]) > std::abs(peaks.at(peakIter)[6]) ) )
        {
            if ( peaks.at(peakIter)[4] < 0 )
            {
                peaks.at(peakIter)[4]        *= -1.0;
                peaks.at(peakIter)[5]        *= -1.0;
                peaks.at(peakIter)[6]        *= -1.0;
                peaks.at(peakIter)[7]        *= -1.0;
            }
        }
        else if ( ( std::abs(peaks.at(peakIter)[5]) > std::abs(peaks.at(peakIter)[4]) ) && ( std::abs(peaks.at(peakIter)[5]) > std::abs(peaks.at(peakIter)[6]) ) )
        {
            if ( peaks.at(peakIter)[5] < 0 )
            {
                peaks.at(peakIter)[4]        *= -1.0;
                peaks.at(peakIter)[5]        *= -1.0;
                peaks.at(peakIter)[6]        *= -1.0;
                peaks.at(peakIter)[7]        *= -1.0;
            }
        }
        else if ( ( std::abs(peaks.at(peakIter)[6]) > std::abs(peaks.at(peakIter)[4]) ) && ( std::abs(peaks.at(peakIter)[6]) > std::abs(peaks.at(peakIter)[5]) ) )
        {
            if ( peaks.at(peakIter)[6] < 0 )
            {
                peaks.at(peakIter)[4]        *= -1.0;
                peaks.at(peakIter)[5]        *= -1.0;
                peaks.at(peakIter)[6]        *= -1.0;
                peaks.at(peakIter)[7]        *= -1.0;
            }
        }
        
        //==================================== If angle is 0, ignore
        if ( ( peaks.at(peakIter)[7] < ( 0.0 + errTolerance ) ) && ( peaks.at(peakIter)[7] > ( 0.0 - errTolerance ) ) )
        {
            continue;
        }
        
        //==================================== Compare to all already present axes
        for ( unsigned int sameAxisGrp = 0; sameAxisGrp < static_cast<unsigned int> ( uniqueAxisGroups.size() ); sameAxisGrp++ )
        {
            //====== Try matching
            for ( unsigned int sameAxis = 0; sameAxis < static_cast<unsigned int> ( uniqueAxisGroups.at(sameAxisGrp).size() ); sameAxis++ )
            {
                //== Is there identical axis?
                if ( ( ( (peaks.at(uniqueAxisGroups.at(sameAxisGrp).at(sameAxis))[4]-errTolerance) <= peaks.at(peakIter)[4] ) &&
                       ( (peaks.at(uniqueAxisGroups.at(sameAxisGrp).at(sameAxis))[4]+errTolerance) >  peaks.at(peakIter)[4] ) ) &&
                     ( ( (peaks.at(uniqueAxisGroups.at(sameAxisGrp).at(sameAxis))[5]-errTolerance) <= peaks.at(peakIter)[5] ) &&
                       ( (peaks.at(uniqueAxisGroups.at(sameAxisGrp).at(sameAxis))[5]+errTolerance) >  peaks.at(peakIter)[5] ) ) &&
                     ( ( (peaks.at(uniqueAxisGroups.at(sameAxisGrp).at(sameAxis))[6]-errTolerance) <= peaks.at(peakIter)[6] ) &&
                       ( (peaks.at(uniqueAxisGroups.at(sameAxisGrp).at(sameAxis))[6]+errTolerance) >  peaks.at(peakIter)[6] ) ) )
                {
                    sameAxisFound             = true;
                    uniqueAxisGroups.at(sameAxisGrp).emplace_back ( peakIter );
                    break;
                }
            }
        }
        
        //==================================== If same axis was found, do nothing
        if ( sameAxisFound ) { continue; }
        
        //==================================== No similar axis was found
        std::vector<unsigned int> hlpVec;
        hlpVec.emplace_back                   ( peakIter );
        uniqueAxisGroups.emplace_back         ( hlpVec );
    }
    
    //======================================== Now deal with the axis = 0 ; 0 ; 0
    for ( unsigned int peakIter = 0; peakIter < static_cast<unsigned int> ( peaks.size() ); peakIter++ )
    {
        //==================================== If angle is 0, ignore
        if ( ( peaks.at(peakIter)[7] < ( 0.0 + errTolerance ) ) && ( peaks.at(peakIter)[7] > ( 0.0 - errTolerance ) ) )
        {
            continue;
        }
        
        //==================================== Now, deal with the axis = 0 ; 0 ; 0
        if ( ( peaks.at(peakIter)[4] == 0.0 ) && ( peaks.at(peakIter)[5] == 0.0 ) && ( peaks.at(peakIter)[6] == 0.0 ) )
        {
            for ( unsigned int sameAxisGrp = 0; sameAxisGrp < static_cast<unsigned int> ( uniqueAxisGroups.size() ); sameAxisGrp++ )
            {
                uniqueAxisGroups.at(sameAxisGrp).emplace_back ( peakIter );
            }
        }
    }
    
    //======================================== Check each unique axis group for pairs of identical angle-axis representation with different Euler angles and similar peak height.
    for ( unsigned int axisGroup = 0; axisGroup < static_cast<unsigned int> ( uniqueAxisGroups.size() ); axisGroup++ )
    {
        for ( unsigned int ex1 = 0; ex1 < static_cast<unsigned int> ( uniqueAxisGroups.at(axisGroup).size() ); ex1++ )
        {
            for ( unsigned int ex2 = 1; ex2 < static_cast<unsigned int> ( uniqueAxisGroups.at(axisGroup).size() ); ex2++ )
            {
                //============================ Keep only unique pairs
                if ( ex1 >= ex2 ) { continue; }
                
                //============================ Check if the pair has the same angle representation (it has to have the same axis)
                if ( ( peaks.at(uniqueAxisGroups.at(axisGroup).at(ex1))[7] < ( peaks.at(uniqueAxisGroups.at(axisGroup).at(ex2))[7] + errTolerance ) ) &&
                     ( peaks.at(uniqueAxisGroups.at(axisGroup).at(ex1))[7] > ( peaks.at(uniqueAxisGroups.at(axisGroup).at(ex2))[7] - errTolerance ) ) )
                {
                    //======================== Same angles. Check if different Euler angles
                    oneDifferent              = false;
                    if ( ( peaks.at(uniqueAxisGroups.at(axisGroup).at(ex1))[0] < ( peaks.at(uniqueAxisGroups.at(axisGroup).at(ex2))[0] + errTolerance ) ) &&
                         ( peaks.at(uniqueAxisGroups.at(axisGroup).at(ex1))[0] > ( peaks.at(uniqueAxisGroups.at(axisGroup).at(ex2))[0] - errTolerance ) ) )
                    {
                        ;
                    }
                    else
                    {
                        oneDifferent          = true;
                    }
                    
                    if ( ( peaks.at(uniqueAxisGroups.at(axisGroup).at(ex1))[1] < ( peaks.at(uniqueAxisGroups.at(axisGroup).at(ex2))[1] + errTolerance ) ) &&
                         ( peaks.at(uniqueAxisGroups.at(axisGroup).at(ex1))[1] > ( peaks.at(uniqueAxisGroups.at(axisGroup).at(ex2))[1] - errTolerance ) ) )
                    {
                        ;
                    }
                    else
                    {
                        oneDifferent          = true;
                    }
                    
                    if ( ( peaks.at(uniqueAxisGroups.at(axisGroup).at(ex1))[2] < ( peaks.at(uniqueAxisGroups.at(axisGroup).at(ex2))[2] + errTolerance ) ) &&
                         ( peaks.at(uniqueAxisGroups.at(axisGroup).at(ex1))[2] > ( peaks.at(uniqueAxisGroups.at(axisGroup).at(ex2))[2] - errTolerance ) ) )
                    {
                        ;
                    }
                    else
                    {
                        oneDifferent          = true;
                    }
                    
                    if ( !oneDifferent )
                    {
                        continue;
                    }
                }
            }
        }
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>> Peaks clustered by symmetry axis." << std::endl;
    }
    
    //======================================== For each group of peaks with the same axis
    for ( unsigned int axisGroup = 0; axisGroup < static_cast<unsigned int> ( uniqueAxisGroups.size() ); axisGroup++ )
    {
        //==================================== Re-organise the peak data
        thisAxisSyms.clear                    ( );
        additionalPeaks.clear                 ( );
        
    ROUGH_RESTART:
        
        additionalPeakFound                   = false;
        grpPeaks.clear                        ( );
        for ( unsigned int grpIter = 0; grpIter < static_cast<unsigned int> ( uniqueAxisGroups.at(axisGroup).size() ); grpIter++ )
        {
            hlpArr[0]                         = peaks.at(uniqueAxisGroups.at(axisGroup).at(grpIter))[4];
            hlpArr[1]                         = peaks.at(uniqueAxisGroups.at(axisGroup).at(grpIter))[5];
            hlpArr[2]                         = peaks.at(uniqueAxisGroups.at(axisGroup).at(grpIter))[6];
            hlpArr[3]                         = peaks.at(uniqueAxisGroups.at(axisGroup).at(grpIter))[7];
            hlpArr[4]                         = peaks.at(uniqueAxisGroups.at(axisGroup).at(grpIter))[3];
            hlpArr[5]                         = static_cast<double> ( grpIter );
            hlpArr[6]                         = 0.0;
            
            grpPeaks.emplace_back             ( hlpArr );
        }
        
        if ( additionalPeaks.size () > 0 )
        {
            for ( unsigned int adIt = 0; adIt < static_cast<unsigned int> ( additionalPeaks.size() ); adIt++ )
            {
                grpPeaks.emplace_back         ( additionalPeaks.at(adIt) );
            }
        }
        
        if ( verbose > 3 )
        {
            printf ( ">>>>> Symmetry axis group:\n" );
            printf ( "      Group\tPeak\t   x\t   y\t   z\t Angle\t Peak\n" );
            printf ( "      Index\tIndex\t elem.\t elem.\t elem.\t (rad)\theight\n" );
            for ( int i = 0; i < static_cast<int> ( grpPeaks.size() ); i++ )
            {
                std::sort ( grpPeaks.begin(), grpPeaks.end(), [](const std::array<double,7>& a, const std::array<double,7>& b) { return a[4] > b[4]; } );
                
                printf ( "        %d\t  %d\t%+.3f\t%+.3f\t%+.3f\t%+.3f\t%+.3f\n", axisGroup, i, grpPeaks.at(i)[0], grpPeaks.at(i)[1], grpPeaks.at(i)[2], grpPeaks.at(i)[3], grpPeaks.at(i)[4] );
            }
        }
        
        while ( true )
        {
            //================================ Init
            angsToTry.clear                   ( );
            
            //================================ Find the peak with highest value
            std::sort ( grpPeaks.begin(), grpPeaks.end(), [](const std::array<double,7>& a, const std::array<double,7>& b) { return a[4] > b[4]; });
            
            //================================ If no peak is over err, exit loop
            if ( grpPeaks.at(0)[4] < ( 0.0 + this->_peakHeightThr ) )
            {
                break;
            }
            
            //================================ Find smallest distance to all other peaks
            minAngDist                        = 999.9;
            for ( unsigned int pAD = 1; pAD < static_cast<unsigned int> ( grpPeaks.size() ); pAD++ )
            {
                if ( ( std::abs( std::abs ( grpPeaks.at(0)[3] ) - std::abs ( grpPeaks.at(pAD)[3] ) ) < minAngDist ) && std::abs( std::abs ( grpPeaks.at(0)[3] ) - std::abs ( grpPeaks.at(pAD)[3] ) ) > 0.1 )
                {
                    minAngDist                = std::abs( std::abs ( grpPeaks.at(0)[3] ) - std::abs ( grpPeaks.at(pAD)[3] ) );
                }
            }
            
            //================================ Should this symmetry be tested?
            angDivisionRemainder = std::modf ( static_cast<double> ( (2.0*M_PI) / std::abs ( minAngDist ) ), &angDivisionBasis );
            if ( angDivisionRemainder > 0.8 )
            {
                angDivisionRemainder         -= 1.0;
                angDivisionBasis             += 1.0;
            }
            angDivisionRemainder             /= angDivisionBasis;
            nextSymmetryError                 = ( M_PI * 2.0 / angDivisionBasis ) - ( M_PI * 2.0 / ( angDivisionBasis + 1.0 ) );
            angTolerance                      = ( nextPeakError / nextSymmetryError  );
            
            if ( ( angDivisionRemainder < ( 0.0 + angTolerance ) ) && ( angDivisionRemainder > ( 0.0 - angTolerance ) ) )
            {
                if ( angTolerance < 0.5 )
                {
                    angsToTry.emplace_back    ( angDivisionBasis );
                }
                else
                {
                    angsToTry.emplace_back    ( angDivisionBasis - 1.0 );
                    angsToTry.emplace_back    ( angDivisionBasis );
                    angsToTry.emplace_back    ( angDivisionBasis + 1.0 );
                }
            }
            
            //================================ Is there symmetry with the highest peak angle?
            angDivisionRemainder              = std::modf ( static_cast<double> ( (2.0*M_PI) / std::abs ( grpPeaks.at(0)[3] ) ), &angDivisionBasis );
            
            if ( angDivisionRemainder > 0.8 )
            {
                angDivisionRemainder         -= 1.0;
                angDivisionBasis             += 1.0;
            }
            angDivisionRemainder             /= angDivisionBasis;
            
            //================================ Find appropriate error tolerance
            nextSymmetryError                 = ( M_PI * 2.0 / angDivisionBasis ) - ( M_PI * 2.0 / ( angDivisionBasis + 1.0 ) );
            angTolerance                      = ( nextPeakError / nextSymmetryError  );
            
            if ( ( angDivisionRemainder < ( 0.0 + angTolerance ) ) && ( angDivisionRemainder > ( 0.0 - angTolerance ) ) )
            {
                if ( angTolerance < 0.5 )
                {
                    angsToTry.emplace_back    ( angDivisionBasis );
                }
                else
                {
                    angsToTry.emplace_back    ( angDivisionBasis - 1.0 );
                    angsToTry.emplace_back    ( angDivisionBasis );
                    angsToTry.emplace_back    ( angDivisionBasis + 1.0 );
                }
            }
            
            //================================ Use only unique values
            std::sort                         ( angsToTry.begin(), angsToTry.end(), std::greater<double>() );
            angsToTry.erase                   ( std::unique( angsToTry.begin(), angsToTry.end() ), angsToTry.end() );
            
            //================================ Check if the symmetry has not already been found for this axis
            delVecElems.clear();
            for ( unsigned int tAS = 0; tAS < static_cast<unsigned int> ( thisAxisSyms.size() ); tAS++ )
            {
                for ( unsigned int nTS = 0; nTS < static_cast<unsigned int> ( angsToTry.size() ); nTS++ )
                {
                    if ( thisAxisSyms.at(tAS) == static_cast<unsigned int> ( angsToTry.at(nTS) ) )
                    {
                        delVecElems.emplace_back ( nTS );
                    }
                }
            }
            
            std::sort                         ( delVecElems.begin(), delVecElems.end(), std::greater<double>() );
            for ( unsigned int remEl = 0; remEl < static_cast<unsigned int> ( delVecElems.size() ); remEl++ )
            {
                angsToTry.erase               ( angsToTry.begin() + delVecElems.at(remEl) );
            }
            
            if ( verbose > 3 )
            {
                printf ( ">>>>>>>>> Testing the following symmetries: " );
                for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( angsToTry.size() ); iter++ )
                {
                    printf ( "C%.0f\t", angsToTry.at(iter) );
                }
                printf ( "\n" );
            }
            
            if ( angsToTry.size () == 0 )
            {
                grpPeaks.at(0)[4]             = 0.0;
                continue;
            }
            
            for ( unsigned int angToTry = 0; angToTry < static_cast<unsigned int> ( angsToTry.size() ); angToTry++ )
            {
                //============================ Yes! Determine other peaks expected positions
                groupAngle                    = ( 2.0 * M_PI ) / angsToTry.at(angToTry);
                angVals.clear                 ( );
                
                for ( int iter = static_cast<int> ( -(angsToTry.at(angToTry)/2.0 + 1.0) ); iter <= static_cast<int> ( angsToTry.at(angToTry)/2.0 + 1.0 ); iter++ )
                {
                    angVals.emplace_back      ( static_cast<double> ( iter * groupAngle ) );
                }
                
                //============================ Now check for existence of these peaks
                groupAngleHits.clear          ( );
                groupMembers.clear            ( );
                missingPeaks.clear            ( );
                for ( unsigned int grpAngIt = 0; grpAngIt < static_cast<unsigned int> ( angVals.size() ); grpAngIt++ )
                {
                    matchedThisPeak           = false;
                    for ( unsigned int peakIt = 0; peakIt < static_cast<unsigned int> ( grpPeaks.size() ); peakIt++ )
                    {
                        if ( angVals.at(grpAngIt) == 0.0 )
                        {
                            groupAngleHits.emplace_back ( angVals.at(grpAngIt) );
                            hlpArr2[0]        = static_cast<double> ( peakIt );
                            hlpArr2[1]        = peaks.at(uniqueAxisGroups.at(axisGroup).at(grpPeaks.at(peakIt)[5]))[3];
                            groupMembers.emplace_back ( hlpArr2 );
                            matchedThisPeak   = true;
                            break;
                        }
                        
                        if ( ( angVals.at(grpAngIt) < ( grpPeaks.at(peakIt)[3] + errTolerance ) ) &&
                             ( angVals.at(grpAngIt) > ( grpPeaks.at(peakIt)[3] - errTolerance ) ) )
                        {
                            groupAngleHits.emplace_back ( angVals.at(grpAngIt) );
                            hlpArr2[0]        = static_cast<double> ( peakIt );
                            hlpArr2[1]        = peaks.at(uniqueAxisGroups.at(axisGroup).at(grpPeaks.at(peakIt)[5]))[3];
                            groupMembers.emplace_back ( hlpArr2 );
                            matchedThisPeak   = true;
                            break;
                        }
                    }
                    
                    if ( !matchedThisPeak )
                    {
                        missingPeaks.emplace_back ( angVals.at(grpAngIt) );
                    }
                }
                
                //============================ And finally check for continuity
                groupContinuous               = false;
                if ( groupAngleHits.size () > 1 )
                {
                    groupContinuous = true;
                    for ( unsigned int iter = 1; iter < static_cast<unsigned int> ( groupAngleHits.size () ); iter++ )
                    {
                        if ( ( ( groupAngleHits.at(iter-1) + groupAngle ) < ( groupAngleHits.at(iter) + errTolerance ) ) &&
                             ( ( groupAngleHits.at(iter-1) + groupAngle ) > ( groupAngleHits.at(iter) - errTolerance ) ) )
                        {
                            ;
                        }
                        else
                        {
                            groupContinuous   = false;
                            break;
                        }
                    }
                }
                
                //============================ Could it be that there are a few peaks missing (not highest around, but high enough) and so the symmetry fails?
                if ( !groupContinuous || ( static_cast<int> ( groupAngleHits.size() ) < static_cast<int> ( angsToTry.at(angToTry) ) ) )
                {
                    if ( verbose > 3 )
                    {
                        printf ( ">>>>>>>>>>>>>> Checking for missing peaks...\n" );
                    }
                    
                    //======================== Is it few or many?
                    if ( ( ( static_cast<double> ( angsToTry.at(angToTry) ) - static_cast<double> ( groupAngleHits.size() ) ) / static_cast<double> ( angsToTry.at(angToTry) ) ) < percAllowedToMiss )
                    {
                        //==================== Few. Initialise the values
                        peaksNeededMinimum    = static_cast<int> ( angsToTry.at(angToTry) ) - static_cast<int> ( groupAngleHits.size() );
                        peaksNeededFound      = 0;
                        peaksNeededRemaining  = static_cast<int> ( missingPeaks.size() );
                        currentAverage        = 0.0;
                        for ( unsigned int grpHlpIt = 0; grpHlpIt < static_cast<unsigned int> ( grpPeaks.size() ); grpHlpIt++ )
                        {
                            currentAverage   += grpPeaks.at(grpHlpIt)[4];
                        }
                        currentAverage       /= static_cast<double> ( grpPeaks.size() );
                        currentAverage        = std::max ( currentAverage - this->_peakHeightThr, this->_peakHeightThr );
                        if ( angsToTry.at(0) < 9 )
                        {
                            currentAverage    = std::max ( this->_peakHeightThr, currentAverage / 4.0 );
                        }
                        
                        //==================== Now, for each missing, check if it exists
                        for ( unsigned int misPeak = 0; misPeak < static_cast<unsigned int> ( missingPeaks.size() ); misPeak++ )
                        {
                            if ( peaksNeededMinimum > ( peaksNeededRemaining + peaksNeededFound ) ) { break; }
                            
                            if ( missingPeaks.at(misPeak) >   M_PI ) { peaksNeededRemaining -= 1; continue; }
                            if ( missingPeaks.at(misPeak) <  -M_PI ) { peaksNeededRemaining -= 1; continue; }
                            if ( missingPeaks.at(misPeak) ==  0.0  ) { peaksNeededRemaining -= 1; continue; }
                            
                            double newX       = 0.0;
                            double newY       = 0.0;
                            double newZ       = 0.0;
                            bool pExists      = false;
                            
                            for ( unsigned int grpHlpIt = 0; grpHlpIt < static_cast<unsigned int> ( grpPeaks.size() ); grpHlpIt++ )
                            {
                                newX         += grpPeaks.at(grpHlpIt)[0];
                                newY         += grpPeaks.at(grpHlpIt)[1];
                                newZ         += grpPeaks.at(grpHlpIt)[2];
                            }
                            newX             /= static_cast<double> ( grpPeaks.size() );
                            newY             /= static_cast<double> ( grpPeaks.size() );
                            newZ             /= static_cast<double> ( grpPeaks.size() );
                            
                            if ( missingPeaks.at(misPeak) > 0.0 )
                            {
                                peakVal       = this->maxPeakHeightFromAngleAxis ( newX, newY, newZ, missingPeaks.at(misPeak), settings );
                                if ( peakVal >= currentAverage )
                                {
                                    peaksNeededFound += 1;
                                    
                                    hlpArr[0] = newX;
                                    hlpArr[1] = newY;
                                    hlpArr[2] = newZ;
                                    hlpArr[3] = missingPeaks.at(misPeak);
                                    hlpArr[4] = 0.0;
                                    hlpArr[5] = 0.0;
                                    hlpArr[6] = 1.0;
                                    
                                    additionalPeaks.emplace_back ( hlpArr );
                                    additionalPeakFound = true;
                                    pExists   = true;
                                }
                                
                                if ( !pExists )
                                {
                                    peakVal = this->maxPeakHeightFromAngleAxis ( -newX, -newY, -newZ, -missingPeaks.at(misPeak), settings );
                                    if ( peakVal >= currentAverage )
                                    {
                                        peaksNeededFound += 1;
                                        
                                        hlpArr[0] = newX;
                                        hlpArr[1] = newY;
                                        hlpArr[2] = newZ;
                                        hlpArr[3] = missingPeaks.at(misPeak);
                                        hlpArr[4] = 0.0;
                                        hlpArr[5] = 0.0;
                                        hlpArr[6] = 1.0;
                                        
                                        additionalPeaks.emplace_back ( hlpArr );
                                        additionalPeakFound = true;
                                        pExists = true;
                                    }
                                }
                            }
                            else
                            {
                                peakVal = this->maxPeakHeightFromAngleAxis ( -newX, -newY, -newZ, -missingPeaks.at(misPeak), settings );
                                if ( peakVal >= currentAverage )
                                {
                                    peaksNeededFound += 1;
                                    
                                    hlpArr[0] = newX;
                                    hlpArr[1] = newY;
                                    hlpArr[2] = newZ;
                                    hlpArr[3] = missingPeaks.at(misPeak);
                                    hlpArr[4] = 0.0;
                                    hlpArr[5] = 0.0;
                                    hlpArr[6] = 1.0;
                                    
                                    additionalPeaks.emplace_back ( hlpArr );
                                    additionalPeakFound = true;
                                    pExists = true;
                                }
                                
                                if ( !pExists )
                                {
                                    peakVal = this->maxPeakHeightFromAngleAxis ( newX, newY, newZ, missingPeaks.at(misPeak), settings );
                                    if ( peakVal >= currentAverage )
                                    {
                                        peaksNeededFound += 1;
                                        
                                        hlpArr[0] = newX;
                                        hlpArr[1] = newY;
                                        hlpArr[2] = newZ;
                                        hlpArr[3] = missingPeaks.at(misPeak);
                                        hlpArr[4] = 0.0;
                                        hlpArr[5] = 0.0;
                                        hlpArr[6] = 1.0;
                                        
                                        additionalPeaks.emplace_back ( hlpArr );
                                        additionalPeakFound = true;
                                        pExists = true;
                                    }
                                }
                            }
                            
                            peaksNeededRemaining -= 1;
                        }
                    }
                }
                
                //============================ I really hate to do this... SORRY!
                if ( additionalPeakFound )
                {
                    if ( verbose > 3 )
                    {
                        std::cout << ">>>>>>>>>>>>>>>>> Missing peaks discovered. Redoing the analysis." << std::endl;
                    }
                    goto ROUGH_RESTART;
                }
                
                //============================ if not continuous, check if 0 angle helps
                if ( !groupContinuous )
                {
                    groupAngleHits.emplace_back ( 0.0 );
                    std::sort ( groupAngleHits.begin(), groupAngleHits.end() );
                    
                    if ( groupAngleHits.size () > 1 )
                    {
                        groupContinuous = true;
                        for ( unsigned int iter = 1; iter < static_cast<unsigned int> ( groupAngleHits.size () ); iter++ )
                        {
                            if ( ( ( groupAngleHits.at(iter-1) + groupAngle ) < ( groupAngleHits.at(iter) + errTolerance ) ) &&
                                 ( ( groupAngleHits.at(iter-1) + groupAngle ) > ( groupAngleHits.at(iter) - errTolerance ) ) )
                            {
                                ;
                            }
                            else
                            {
                                groupContinuous = false;
                                break;
                            }
                        }
                    }
                }
                
                //============================ Remove this symmetry for this axis as it has been tried
                thisAxisSyms.emplace_back     ( angsToTry.at(angToTry) );
                
                //============================ If symmetry found, save it and set group peak heights to 0
                if ( groupContinuous )
                {
                    // If ALL peaks are found
                    if ( static_cast<int> ( groupAngleHits.size() ) >= static_cast<int> ( angsToTry.at(angToTry) ) )
                    {
                        // Init
                        std::sort             ( groupMembers.begin(), groupMembers.end(), [](const std::array<double,2>& a, const std::array<double,2>& b) { return a[1] > b[1]; } );
                        groupMembers.erase    ( std::unique( groupMembers.begin(), groupMembers.end() ), groupMembers.end() );
                        
                        if ( verbose > 3 )
                        {
                            printf ( ">>>>>>>>>>> Found symmetry C%+.0f on lines ", angsToTry.at(angToTry) );
                            for ( unsigned int i = 0; i < static_cast<unsigned int> ( groupMembers.size() ); i++ )
                            {
                                printf ( "%+.0f ", groupMembers.at(i)[0] );
                            }
                            printf ( "\n" );
                        }
                        
                        // Save
                        retArr[0]             = angsToTry.at(angToTry);
                       
                        meanX                 = 0.0;
                        meanY                 = 0.0;
                        meanZ                 = 0.0;
                        meanPeak              = 0.0;
                        totVals               = 0.0;
                        
                        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( groupMembers.size() ); iter++ )
                        {
                            if ( ( grpPeaks.at(groupMembers.at(iter)[0])[0] == 0.0 ) && ( grpPeaks.at(groupMembers.at(iter)[0])[1] == 0.0 ) &&
                                 ( grpPeaks.at(groupMembers.at(iter)[0])[2] == 0.0 ) ) { continue; }
                            if ( groupMembers.at(iter)[1] == 0.0 ) { continue; }
                            
                            meanX            += grpPeaks.at(groupMembers.at(iter)[0])[0];
                            meanY            += grpPeaks.at(groupMembers.at(iter)[0])[1];
                            meanZ            += grpPeaks.at(groupMembers.at(iter)[0])[2];
                            if ( grpPeaks.at(groupMembers.at(iter)[0])[6] == 0.0 )
                            {
                                meanPeak     += groupMembers.at(iter)[1];
                            }
                            totVals          += 1.0;
                        }
                        
                        if ( ( meanX == 0.0 ) && ( meanY == 0.0 ) && ( meanZ == 0.0 ) )
                        {
                            retArr[1]         = 0.0;
                            retArr[2]         = 0.0;
                            retArr[3]         = 1.0;
                            retArr[4]         = groupMembers.at(0)[1];
                        }
                        else
                        {
                            meanX            /= totVals;
                            meanY            /= totVals;
                            meanZ            /= totVals;
                            meanPeak         /= totVals;
                            
                            retArr[1]         = meanX;
                            retArr[2]         = meanY;
                            retArr[3]         = meanZ;
                            retArr[4]         = meanPeak;
                        }
                        
                        ret.emplace_back ( retArr );
                        
                        grpPeaks.at(0)[4]     = 0.0;
                    }
                    else
                    {
                        //==================== The group is continuous, but does not include ALL angles and so symmetry is NOT found
                        grpPeaks.at(0)[4]     = 0.0;
                    }
                }
                
                //============================ Set the main peak height to 0 and try again
                if ( !groupContinuous )
                {
                    grpPeaks.at(0)[4]         = 0.0;
                }
            }
        }
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>> C symmetries detection complete." << std::endl;
    }
    
    //======================================== Check for identical symmetries (coming from finding two peaks with almost identical angle)
    if ( ret.size() > 1 )
    {
        std::vector<unsigned int> removeThese;
        for ( unsigned int symIt = 0; symIt < static_cast<unsigned int> ( ret.size() ); symIt++ )
        {
            for ( unsigned int existSym = 0; existSym < static_cast<unsigned int> ( ret.size() ); existSym++ )
            {
                if ( symIt >= existSym ) { continue; }
                
                //============================ Is axis the same?
                if ( ( ( ret.at(existSym)[1] < ( ret.at(symIt)[1] + errTolerance ) ) && ( ret.at(existSym)[1] > ( ret.at(symIt)[1] - errTolerance ) ) ) &&
                     ( ( ret.at(existSym)[2] < ( ret.at(symIt)[2] + errTolerance ) ) && ( ret.at(existSym)[2] > ( ret.at(symIt)[2] - errTolerance ) ) ) &&
                     ( ( ret.at(existSym)[3] < ( ret.at(symIt)[3] + errTolerance ) ) && ( ret.at(existSym)[3] > ( ret.at(symIt)[3] - errTolerance ) ) ) )
                {
                    //======================== Is symmetry the same?
                    if ( ret.at(existSym)[0] == ret.at(symIt)[0] )
                    {
                        //==================== Mark for removal
                        if ( ret.at(existSym)[4] > ret.at(symIt)[4] )
                        {
                            removeThese.emplace_back ( symIt );
                        }
                        else
                        {
                            removeThese.emplace_back ( existSym );
                        }
                    }
                    
                }
            }
        }
        
        std::sort                             ( removeThese.begin(), removeThese.end(), std::greater<unsigned int>() );
        removeThese.erase                     ( std::unique( removeThese.begin(), removeThese.end() ), removeThese.end() );
        
        for ( unsigned int remIt = 0; remIt < static_cast<unsigned int> ( removeThese.size() ); remIt++ )
        {
            ret.erase                         ( ret.begin() + removeThese.at(remIt) );
        }
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>> Duplicate C symmetries removal complete." << std::endl;
    }
    
    //======================================== Free memory
    if ( freeMem )
    {
        fftw_free                             ( this->_so3InvCoeffs );
        this->_so3InvCoeffs                   = nullptr;
    }
    
    //======================================== Sort by mean peak height
    std::sort                                 ( ret.begin(), ret.end(), [](const std::array<double,5>& a, const std::array<double,5>& b) { return a[4] > b[4]; });
    
    //======================================== Done
    this->_CSymmsFound                        = true;
    return ( ret );
    
}

/*! \brief This function frees the SOFT inverse map.
 
 The need for this function is specific to the symmetry detection, where the inverse SO(3) Fourier transform map is required to linger a bit longer than just the peak detection. This is
 because missing axis searches do require it, so that specific timing of the memory freeing needs to be done.
 
 \warning This is an internal function which should not be used by the user.
 */

void ProSHADE_internal::ProSHADE_comparePairwise::freeInvMap ( )
{
    //======================================== Free SO3 Inv Map
    if ( this->_so3InvCoeffs != nullptr )
    {
        fftw_free                             ( this->_so3InvCoeffs );
        this->_so3InvCoeffs                   = nullptr;
    }
    
    //============== Done
    return ;
    
}

/*! \brief This is an accessor function to the peak height threshold.
 
 This function provides access to the private class member peak threshold. It is needed for symmetry detection, where missing axes needs to know the minimum height required, but does not have
 access to the private members.
 
 \param[out] X The value of the peak threshold used in peak detection.
 
 \warning This is an internal function which should not be used by the user.
 */

double ProSHADE_internal::ProSHADE_comparePairwise::getPeakThreshold ( )
{
    return ( this->_peakHeightThr );
    
}

/*! \brief This function takes the detected C-symmetries list, removes low probability symmetries and sorts as to make the output clearer.
 
 This is more of a visualisation/presentation function, as it does not compute anything important, it just removes C symmetries
 which have considerably higher one already present and sorts as to make the highest symmetry order first (if several symmetries
 are found) or according to the peak height (if there are many reported symmetries and the user will have to interfere).
 
 \param[in] CnSymm List of the detected C symmetries as produced by the findCnSymmetry function.
 \param[in] settings The ProSHADE_settings container class instance with information about how to write the HTML report.
 \param[in] maxGap The minimum gap required between the symmetries to cut the output.
 \param[in] pf Pointer to boolean variable to decide whether full or clear output should be printed.
 \param[out] X List of C symmetries passing these more stringent conditions.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::array<double,5> > ProSHADE_internal::ProSHADE_comparePairwise::findCnSymmetryClear ( std::vector< std::array<double,5> > CnSymm,
                                                                                                       ProSHADE::ProSHADE_settings* settings,
                                                                                                       double maxGap,
                                                                                                       bool *pf )
{
    //======================================== Sanity check
    if ( !this->_CSymmsFound )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Tried to simplify the C symmetry peaks before detecting them. Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Tried to simplify the C symmetry peaks before detecting them. This looks like internal bug, please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    if ( CnSymm.size() < 1 )
    {
        std::cout << "!!! ProSHADE WARNING !!! Tried to simplify the C-symmetries list, but the list has less than 1 entry." << std::endl;
        if ( pf != nullptr )
        {
            *pf                      = true;
        }
        
        return ( CnSymm );
    }
    
    //======================================== Initialise
    std::vector< std::array<double,5> > ret;
    std::vector< std::array<double,2> > pHeights;
    std::array<double,2> hlpArr;
    
    //======================================== Get all the peak heights
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( CnSymm.size() ); iter++ )
    {
        hlpArr[0]                             = static_cast<double> ( iter );
        hlpArr[1]                             = CnSymm.at(iter)[4];
        pHeights.emplace_back                 ( hlpArr );
    }
    
    //============== Find gaps
    std::sort                                 ( pHeights.begin(), pHeights.end(), [](const std::array<double,2>& a, const std::array<double,2>& b) { return a[1] > b[1]; });
    double maxPeakThres                       = pHeights.at(0)[1] - ( pHeights.at(0)[1] * maxGap );
    
    ret.emplace_back                          ( CnSymm.at(pHeights.at(0)[0]) );
    for ( unsigned int iter = 1; iter < static_cast<unsigned int> ( pHeights.size() ); iter++ )
    {
        if ( pHeights.at(iter)[1] >= maxPeakThres )
        {
            ret.emplace_back                  ( CnSymm.at(pHeights.at(iter)[0]) );
        }
    }
    
    //======================================== Give the highest symmetry first, if small, else sort by peak
    std::sort                                 ( ret.begin(), ret.end(), [](const std::array<double,5>& a, const std::array<double,5>& b) { return a[0] > b[0]; });
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief This function detects dihedral (D) symmetries from the list of already detected C symmetries.
 
 Given that the C symmetries have been determined, it is now rather simple to detect the D symmetries, as there is
 a D symmetry whenever two C symmetries have dot product of their axes 0 (are perpendicular). Thus, this function
 does all the required work and reports the detected D-symmetries.
 
 \param[in] CnSymm List of the detected C symmetries as produced by the findCnSymmetry function.
 \param[in] axisErrorTolerance Tolerance value on matching the peaks axis together.
 \param[out] X List of detected D symmetries.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::vector< std::array<double,6> > > ProSHADE_internal::ProSHADE_comparePairwise::findDnSymmetry ( std::vector< std::array<double,5> > CnSymm,
                                                                                                                 double axisErrorTolerance )
{
    //======================================== Initialise local values
    double dotProduct                         = 0.0;
    bool changeFound                          = true;
    bool matchedAll                           = true;
    bool identical                            = true;
    std::vector< std::vector<std::array<double,6> > > ret;
    std::vector< std::vector<std::array<double,6> > > retHlp;
    std::vector< std::array<double,6> > hlpVec;
    std::array<double,6> hlpArr;
    
    //======================================== Create all pairs
    for ( unsigned int cSym1 = 0; cSym1 < static_cast<unsigned int> ( CnSymm.size() ); cSym1++ )
    {
        for ( unsigned int cSym2 = 1; cSym2 < static_cast<unsigned int> ( CnSymm.size() ); cSym2++ )
        {
            //================================ Use unique only
            if ( cSym1 >= cSym2 ) { continue; }
            
            //================================ Initialise
            hlpVec.clear                      ( );
            
            //================================ Compare
            dotProduct                        = ( CnSymm.at(cSym1)[1] * CnSymm.at(cSym2)[1] +
                                                  CnSymm.at(cSym1)[2] * CnSymm.at(cSym2)[2] +
                                                  CnSymm.at(cSym1)[3] * CnSymm.at(cSym2)[3] );
            
            //================================ Check if it is a pair
            if ( std::abs ( dotProduct ) < axisErrorTolerance )
            {
                hlpArr[0]                     = CnSymm.at(cSym1)[0];
                hlpArr[1]                     = CnSymm.at(cSym1)[1];
                hlpArr[2]                     = CnSymm.at(cSym1)[2];
                hlpArr[3]                     = CnSymm.at(cSym1)[3];
                hlpArr[4]                     = CnSymm.at(cSym1)[4];
                hlpVec.emplace_back           ( hlpArr );
                
                hlpArr[0]                     = CnSymm.at(cSym2)[0];
                hlpArr[1]                     = CnSymm.at(cSym2)[1];
                hlpArr[2]                     = CnSymm.at(cSym2)[2];
                hlpArr[3]                     = CnSymm.at(cSym2)[3];
                hlpArr[4]                     = CnSymm.at(cSym2)[4];
                hlpVec.emplace_back           ( hlpArr );
                retHlp.emplace_back           ( hlpVec );
            }
        }
    }
    
    //======================================== If nothing found, done
    if ( retHlp.size() == 0 ) { this->_DSymmsFound = true; return ( ret ); }
    
    //======================================== Keep adding matches until no more are found
    changeFound                               = true;
    while ( changeFound )
    {
        //==================================== Initialise
        changeFound                           = false;
        
        //==================================== Compare each CSym to all existing DSyms
        for ( unsigned int cSym1 = 0; cSym1 < static_cast<unsigned int> ( CnSymm.size() ); cSym1++ )
        {
            //================================ Compare to all existing DSyms
            for ( unsigned int eSym = 0; eSym < static_cast<unsigned int> ( retHlp.size() ); eSym++ )
            {
                //============================ Initialise
                matchedAll                    = true;
                identical                     = false;
                
                //============================ Check all members if ALL are orthogonal
                for ( unsigned int mem = 0; mem < static_cast<unsigned int> ( retHlp.at(eSym).size() ); mem++ )
                {
                    //======================== Are identical
                    if ( ( CnSymm.at(cSym1)[0] == retHlp.at(eSym).at(mem)[0] ) &&
                         ( CnSymm.at(cSym1)[1] == retHlp.at(eSym).at(mem)[1] ) &&
                         ( CnSymm.at(cSym1)[2] == retHlp.at(eSym).at(mem)[2] ) &&
                         ( CnSymm.at(cSym1)[3] == retHlp.at(eSym).at(mem)[3] ) )
                    {
                        identical             = true;
                    }
                    
                    //======================== Compare
                    dotProduct                = ( CnSymm.at(cSym1)[1] * retHlp.at(eSym).at(mem)[1] +
                                                  CnSymm.at(cSym1)[2] * retHlp.at(eSym).at(mem)[2] +
                                                  CnSymm.at(cSym1)[3] * retHlp.at(eSym).at(mem)[3] );
                    
                    if ( std::abs ( dotProduct ) < axisErrorTolerance ) { ; }
                    else { matchedAll = false; }
                }
                
                //============================ If identical, go to next
                if ( identical ) { continue; }
                
                //============================ If ALL are, then add
                if ( matchedAll )
                {
                    hlpArr[0]                 = CnSymm.at(cSym1)[0];
                    hlpArr[1]                 = CnSymm.at(cSym1)[1];
                    hlpArr[2]                 = CnSymm.at(cSym1)[2];
                    hlpArr[3]                 = CnSymm.at(cSym1)[3];
                    hlpArr[4]                 = CnSymm.at(cSym1)[4];
                    retHlp.at(eSym).emplace_back ( hlpArr );
                    
                    changeFound               = true;
                }
            }
        }
    }
    
    //======================================== Remove identical groups
    for ( unsigned int eSym1 = 0; eSym1 < static_cast<unsigned int> ( retHlp.size() ); eSym1++ )
    {
        //==================================== Already removed?
        if ( retHlp.at(eSym1).at(0)[0] == -1.0 ) { continue; }
        
        for ( unsigned int eSym2 = 1; eSym2 < static_cast<unsigned int> ( retHlp.size() ); eSym2++ )
        {
            //================================ Already removed?
            if ( retHlp.at(eSym2).at(0)[0] == -1.0 ) { continue; }
            
            //================================ Use unique only
            if ( eSym1 >= eSym2 ) { continue; }
            
            //================================ Are identical?
            identical                         = true;
            for ( unsigned int mem1 = 0; mem1 < static_cast<unsigned int> ( retHlp.at(eSym1).size() ); mem1++ )
            {
                matchedAll                    = false;
                for ( unsigned int mem2 = 0; mem2 < static_cast<unsigned int> ( retHlp.at(eSym2).size() ); mem2++ )
                {
                    if ( ( retHlp.at(eSym1).at(mem1)[0] == retHlp.at(eSym2).at(mem2)[0] ) &&
                         ( retHlp.at(eSym1).at(mem1)[1] == retHlp.at(eSym2).at(mem2)[1] ) &&
                         ( retHlp.at(eSym1).at(mem1)[2] == retHlp.at(eSym2).at(mem2)[2] ) &&
                         ( retHlp.at(eSym1).at(mem1)[3] == retHlp.at(eSym2).at(mem2)[3] ) )
                    {
                        matchedAll            = true;
                    }
                }
                
                if ( !matchedAll ) { identical = false; break; }
            }
            
            if ( identical )
            {
                retHlp.at(eSym2).at(0)[0] = -1.0;
            }
        }
    }
    
    //======================================== Re-save only non-identical
    double avgPeak                            = 0.0;
    double sumSym                             = 0.0;
    
    for ( unsigned int eSym1 = 0; eSym1 < static_cast<unsigned int> ( retHlp.size() ); eSym1++ )
    {
        if ( retHlp.at(eSym1).at(0)[0] == -1.0 ) { continue; }
        else
        {
            //================================ This is a true result. Now find the average peak
            avgPeak                           = retHlp.at(eSym1).at(0)[4] * retHlp.at(eSym1).at(0)[0];
            sumSym                            = retHlp.at(eSym1).at(0)[0];
            for ( unsigned int mem = 1; mem < static_cast<unsigned int> ( retHlp.at(eSym1).size() ); mem++ )
            {
                avgPeak                      += retHlp.at(eSym1).at(mem)[4] * retHlp.at(eSym1).at(mem)[0];
                sumSym                       += retHlp.at(eSym1).at(mem)[0];
            }
            avgPeak                          /= sumSym;
            
            hlpVec.clear                      ( );
            for ( unsigned int mem = 0; mem < static_cast<unsigned int> ( retHlp.at(eSym1).size() ); mem++ )
            {
                hlpArr[0]                     = retHlp.at(eSym1).at(mem)[0];
                hlpArr[1]                     = retHlp.at(eSym1).at(mem)[1];
                hlpArr[2]                     = retHlp.at(eSym1).at(mem)[2];
                hlpArr[3]                     = retHlp.at(eSym1).at(mem)[3];
                hlpArr[4]                     = retHlp.at(eSym1).at(mem)[4];
                hlpArr[5]                     = avgPeak;
                hlpVec.emplace_back           ( hlpArr );
            }
            ret.emplace_back                  ( hlpVec );
        }
    }
    
    //======================================== Sort by average peak height
    std::sort                                 ( ret.begin(), ret.end(), [](const std::vector< std::array<double,6> >& a, const std::vector< std::array<double,6> >& b) { return a.at(0)[5] > b.at(0)[5]; });
    
    //======================================== Done
    this->_DSymmsFound                        = true;
    return ( ret );
    
}

/*! \brief This function sorts the D symmetries and removed low probability ones.
 
 Given that the D symmetries are determined, they are now sorted by the symmetry order and peak height. Also, D symmetries
 with are much lower that the top ones are omitted to make for a clearer results screen. Finally, only the highest two D
 symmetries are reported and the Dx-x instead of all matching.
 
 \param[in] CnSymm List of detected D symmetries as produced by the findDnSymmetry function.
 \param[in] settings The ProSHADE_settings container class instance with information about how to write the HTML report.
 \param[in] maxGap The minimum gap required between the symmetries to cut the output.
 \param[in] pf Pointer to boolean variable to decide whether full or clear output should be printed.
 \param[out] X List of D symmetries passing these more stringent conditions.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::vector< std::array<double,6> > > ProSHADE_internal::ProSHADE_comparePairwise::findDnSymmetryClear ( std::vector< std::vector< std::array<double,6> > > DnSymm,
                                                                                                                      ProSHADE::ProSHADE_settings* settings,
                                                                                                                      double maxGap,
                                                                                                                      bool *pf )
{
    //======================================== Sanity check
    if ( !this->_DSymmsFound )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Tried to simplify the D symmetry peaks before detecting them. Terminating..." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Tried to simplify the D symmetry peaks before detecting them. This looks like internal bug, please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    if ( DnSymm.size() < 1 )
    {
        std::cout << "!!! ProSHADE WARNING !!! Tried to simplify the D-symmetries list, but the list has less than 1 entry." << std::endl;
        if ( pf != nullptr )
        {
            *pf                               = true;
        }
        std::vector< std::vector< std::array<double,6> > > ret;
        return                                ( ret );
    }
    
    //======================================== Initialise
    std::vector< std::vector<std::array<double,6> > > ret;
    std::vector< std::array<double,6> > hVec;
    std::vector< std::vector<std::array<double,4> > > pHeights;
    std::vector< std::array<double,4> > hlpVec;
    std::array<double,4> hlpArr;
    
    //======================================== Get all the average peak heights
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( DnSymm.size() ); iter++ )
    {
        hlpVec.clear                          ( );
        hlpArr[0]                             = static_cast<double> ( iter );
        hlpArr[3]                             = 0;
        
        for ( unsigned int it = 0; it < static_cast<unsigned int> ( DnSymm.at(iter).size() ); it++ )
        {
            hlpArr[1]                         = static_cast<double> ( it );
            hlpArr[2]                         = DnSymm.at(iter).at(it)[0];
            hlpArr[3]                         = DnSymm.at(iter).at(it)[4];
            hlpVec.emplace_back               ( hlpArr );
        }
        
        pHeights.emplace_back                 ( hlpVec );
    }
    
    //======================================== Sort peak groups by C symmetry and then by peak height
    double lSym                               = 0.0;
    double mPeak                              = 0.0;
    double firstIt                            = 0.0;
    double secondIt                           = 0.0;
    double avgPeak                            = 0.0;
    std::vector< std::array<double,2> > combsEx;
    std::array<double,2> hA;
    bool uniqCombo                            = true;
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( pHeights.size() ); iter++ )
    {
        //==================================== Sort by symmetry
        std::sort                             ( pHeights.at(iter).begin(), pHeights.at(iter).end(), [](const std::array<double,4>& a, const std::array<double,4>& b) { return a[2] > b[2]; });
        
        //==================================== For the highest symmetry, find highest peak
        lSym                                  = pHeights.at(iter).at(0)[2];
        mPeak                                 = 0.0;
        for ( unsigned int it = 0; it < static_cast<unsigned int> ( pHeights.at(iter).size() ); it++ )
        {
            if ( pHeights.at(iter).at(it)[2] == lSym )
            {
                if ( mPeak < pHeights.at(iter).at(it)[3] )
                {
                    mPeak                     = pHeights.at(iter).at(it)[3];
                    firstIt                   = static_cast<double> ( it );
                }
            }
            else
            {
                secondIt                      = std::max ( secondIt, pHeights.at(iter).at(it)[2] );
            }
        }
        if ( secondIt == 0.0 ) {                        ; }
        else { lSym                           = secondIt; }
        mPeak                                 = 0.0;
        for ( unsigned int it = 0; it < static_cast<unsigned int> ( pHeights.at(iter).size() ); it++ )
        {
            if ( pHeights.at(iter).at(it)[2] == lSym )
            {
                if ( mPeak < pHeights.at(iter).at(it)[3] )
                {
                    if ( static_cast<double> ( it ) != firstIt )
                    {
                        mPeak                 = pHeights.at(iter).at(it)[3];
                        secondIt              = static_cast<double> ( it );
                    }
                }
            }
        }
        
        if ( iter == 0 )
        {
            hVec.clear                        ( );
            hVec.emplace_back                 ( DnSymm.at(iter).at(pHeights.at(iter).at(firstIt)[1]) );
            hVec.emplace_back                 ( DnSymm.at(iter).at(pHeights.at(iter).at(secondIt)[1]) );
            avgPeak                           = ( DnSymm.at(iter).at(pHeights.at(iter).at(firstIt)[1])[4] + DnSymm.at(iter).at(pHeights.at(iter).at(secondIt)[1])[4] ) / 2.0;
            hVec.at(0)[5]                     = avgPeak;
            hVec.at(1)[5]                     = avgPeak;
            ret.emplace_back                  ( hVec );
            hA[0]                             = DnSymm.at(iter).at(pHeights.at(iter).at(firstIt)[1])[0];
            hA[1]                             = DnSymm.at(iter).at(pHeights.at(iter).at(secondIt)[1])[0];
            combsEx.emplace_back              ( hA );
        }
        else
        {
            uniqCombo                         = true;
            for ( unsigned int it = 0; it < static_cast<unsigned int> ( combsEx.size() ); it++ )
            {
                if ( ( combsEx.at(it)[0] == DnSymm.at(iter).at(pHeights.at(iter).at(firstIt)[1])[0] ) &&
                     ( combsEx.at(it)[1] == DnSymm.at(iter).at(pHeights.at(iter).at(secondIt)[1])[0] ) )
                {
                    uniqCombo                 = false;
                }
            }
            
            if ( uniqCombo )
            {
                hVec.clear                    ( );
                hVec.emplace_back             ( DnSymm.at(iter).at(pHeights.at(iter).at(firstIt)[1]) );
                hVec.emplace_back             ( DnSymm.at(iter).at(pHeights.at(iter).at(secondIt)[1]) );
                ret.emplace_back              ( hVec );
                hA[0]                         = DnSymm.at(iter).at(pHeights.at(iter).at(firstIt)[1])[0];
                hA[1]                         = DnSymm.at(iter).at(pHeights.at(iter).at(secondIt)[1])[0];
                combsEx.emplace_back          ( hA );
            }
        }
    }
    
    //======================================== Sort by symmetry size if possible
    if ( ret.size () < 6 )
    {
        std::sort                             ( ret.begin(), ret.end(), [](const std::vector< std::array<double,6> >& a, const std::vector< std::array<double,6> >& b) { return a.at(0)[0] > b.at(0)[0]; });
    }
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief This function checks if a peak Euler angles actually produce something the descriptors would agree is similar.
 
 This function is used internally to check the passing 'true' peaks for actually leading to structure overlay which is
 similar to the original. This is done in attempt to remove 'false' peaks where the background noise produces a peak which
 however is not symmetry related.
 
 \param[in] alpha The Euler ZXZ alpha angle of the peak to be checked.
 \param[in] beta The Euler ZXZ beta angle of the peak to be checked.
 \param[in] gamma The Euler ZXZ gamma angle of the peak to be checked.
 \param[in] settings The Settings class with information about the HTML report writing, if needed.
 \param[in] passThreshold The distance threshold for passing as 'true' peak instead of being considered 'background' peak.
 \param[out] X Boolean value whether the peak is 'true'.
 
 \warning This is an internal function which should not be used by the user.
 */
bool ProSHADE_internal::ProSHADE_comparePairwise::checkPeakExistence ( double alpha,
                                                                       double beta,
                                                                       double gamma,
                                                                       ProSHADE::ProSHADE_settings* settings,
                                                                       double passThreshold )
{
    //======================================== Initialise
    bool ret                                  = false;
    double dist                               = 0.0;
    
    //======================================== Get Wigner D matrices for the given angles
    this->setEulerAngles                      ( alpha, beta, gamma );
    this->generateWignerMatrices              ( settings );
    
    //======================================== Compute full rotation function distance
    dist                                      = this->getRotCoeffDistance ( settings );
    
    //======================================== Decide if rotation is real or ghost
    if ( dist >= passThreshold )              { ret = true;  }
    else                                      { ret = false; }
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief This function detects the tetrahedral symmetries in the structure.
 
 This function takes the C symmetries list and checks for existence of C3 symmetries. All of these are then tested against all the different C3
 symmetries found in the data and if the relative angle of their axes is equal to acos ( 1.0 / 3.0 ), then the structure has to have the
 tetrahedral symmetry.
 
 \param[in] CnSymm List of the already detected C symmetries.
 \param[in] tetrSymmPeakAvg A pointer to variable holding the average peak value for all tetrahedral symmetry supporting C symmetries.
 \param[in] axisErrorTolerance The tolerance on the axes angle error.
 \param[out] X List of tetrahedral symmetry showing C symmetries.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::vector< std::array<double,5> > > ProSHADE_internal::ProSHADE_comparePairwise::findTetrSymmetry  ( std::vector< std::array<double,5> > CnSymm,
                                                                                                                    double *tetrSymmPeakAvg,
                                                                                                                    double axisErrorTolerance )
{
    //======================================== Intialise local variables
    std::vector< std::vector<std::array<double,5> > > ret;
    std::vector< std::array<double,5> > hlpVec;
    std::array<double,5> hlpArr;
    std::vector<unsigned int> C3s;
    double dotProduct                         = 0.0;
    *tetrSymmPeakAvg                          = 0.0;
    double totPairs                           = 0.0;
    
    //======================================== Find all C3 symmetries
    for ( unsigned int cSym = 0; cSym < static_cast<unsigned int> ( CnSymm.size() ); cSym++ )
    {
        if ( CnSymm.at(cSym)[0] == 3 )
        {
            C3s.emplace_back                  ( cSym );
        }
    }
    
    //======================================== Check for any C3 symmetry with axis angle acos ( 1.0 / 3.0 )
    for ( unsigned int c3Sym = 0; c3Sym < static_cast<unsigned int> ( C3s.size() ); c3Sym++ )
    {
        for ( unsigned int cSym = 0; cSym < static_cast<unsigned int> ( CnSymm.size() ); cSym++ )
        {
            if ( CnSymm.at(cSym)[0] == 3 )
            {
                if ( cSym == C3s.at(c3Sym) ) { continue; }
                
                dotProduct                    = ( CnSymm.at(C3s.at(c3Sym))[1] * CnSymm.at(cSym)[1] +
                                                  CnSymm.at(C3s.at(c3Sym))[2] * CnSymm.at(cSym)[2] +
                                                  CnSymm.at(C3s.at(c3Sym))[3] * CnSymm.at(cSym)[3] );
                
                if ( ( ( 1.0 / 3.0 ) > ( dotProduct - axisErrorTolerance ) ) &&
                     ( ( 1.0 / 3.0 ) < ( dotProduct + axisErrorTolerance ) ) )
                {
                    //======================== Found tetrahedral symmetry
                    hlpVec.clear              ( );
                    
                    hlpArr[0]                 = CnSymm.at(C3s.at(c3Sym))[0];
                    hlpArr[1]                 = CnSymm.at(C3s.at(c3Sym))[1];
                    hlpArr[2]                 = CnSymm.at(C3s.at(c3Sym))[2];
                    hlpArr[3]                 = CnSymm.at(C3s.at(c3Sym))[3];
                    hlpArr[4]                 = ( CnSymm.at(C3s.at(c3Sym))[4] + CnSymm.at(cSym)[4] ) / 2.0;
                    hlpVec.emplace_back       ( hlpArr );
                    
                    hlpArr[0]                 = CnSymm.at(cSym)[0];
                    hlpArr[1]                 = CnSymm.at(cSym)[1];
                    hlpArr[2]                 = CnSymm.at(cSym)[2];
                    hlpArr[3]                 = CnSymm.at(cSym)[3];
                    hlpVec.emplace_back       ( hlpArr );
                    ret.emplace_back          ( hlpVec );
                    *tetrSymmPeakAvg         += hlpArr[4];
                    totPairs                 += 1.0;
                }
            }
        }
    }
    
    //======================================== Find overall average peak
    *tetrSymmPeakAvg                         /= totPairs;
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief This function detects the octahedral symmetries in the structure.
 
 This function takes the C symmetries list and checks for existence of C4 symmetries. All of these are then tested against all the different C3
 symmetries found in the data and if the relative angle of their axes is equal to PI - acos ( 1.0 / 3.0 ), then the structure has to have the
 octahedral symmetry.
 
 \param[in] CnSymm List of the already detected C symmetries.
 \param[in] octaSymmPeakAvg A pointer to variable holding the average peak value for all octahedral symmetry supporting C symmetries.
 \param[in] axisErrorTolerance The tolerance on the axes angle error.
 \param[out] X List of octahedral symmetry showing C symmetries.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::vector< std::array<double,5> > > ProSHADE_internal::ProSHADE_comparePairwise::findOctaSymmetry  ( std::vector< std::array<double,5> > CnSymm,
                                                                                                                    double *octaSymmPeakAvg,
                                                                                                                    double axisErrorTolerance )
{
    //======================================== Intialise local variables
    std::vector< std::vector< std::array<double,5 > > > ret;
    std::vector< std::array<double,5> > hlpVec;
    std::array<double,5> hlpArr;
    std::vector<unsigned int> C4s;
    double dotProduct                         = 0.0;
    *octaSymmPeakAvg                          = 0.0;
    double totPairs                           = 0.0;
    
    //======================================== Find all C4 symmetries
    for ( unsigned int cSym = 0; cSym < static_cast<unsigned int> ( CnSymm.size() ); cSym++ )
    {
        if ( CnSymm.at(cSym)[0] == 4 )
        {
            C4s.emplace_back                  ( cSym );
        }
    }
    
    //======================================== Check for any C3 symmetry with axis angle PI - acos ( 1.0 / 3.0 )
    for ( unsigned int c4Sym = 0; c4Sym < static_cast<unsigned int> ( C4s.size() ); c4Sym++ )
    {
        for ( unsigned int cSym = 0; cSym < static_cast<unsigned int> ( CnSymm.size() ); cSym++ )
        {
            if ( CnSymm.at(cSym)[0] == 3 )
            {
                dotProduct                    = ( CnSymm.at(C4s.at(c4Sym))[1] * CnSymm.at(cSym)[1] +
                                                  CnSymm.at(C4s.at(c4Sym))[2] * CnSymm.at(cSym)[2] +
                                                  CnSymm.at(C4s.at(c4Sym))[3] * CnSymm.at(cSym)[3] );
                
                if ( ( ( 1.0 / sqrt ( 3.0 ) ) > ( dotProduct - axisErrorTolerance ) ) &&
                     ( ( 1.0 / sqrt ( 3.0 ) ) < ( dotProduct + axisErrorTolerance ) ) )
                {
                    //======================== Found octahedral symmetry
                    hlpVec.clear              ( );
                    
                    hlpArr[0]                 = CnSymm.at(C4s.at(c4Sym))[0];
                    hlpArr[1]                 = CnSymm.at(C4s.at(c4Sym))[1];
                    hlpArr[2]                 = CnSymm.at(C4s.at(c4Sym))[2];
                    hlpArr[3]                 = CnSymm.at(C4s.at(c4Sym))[3];
                    hlpArr[4]                 = ( CnSymm.at(C4s.at(c4Sym))[4] + CnSymm.at(cSym)[4] ) / 2.0;
                    hlpVec.emplace_back       ( hlpArr );
                    
                    hlpArr[0]                 = CnSymm.at(cSym)[0];
                    hlpArr[1]                 = CnSymm.at(cSym)[1];
                    hlpArr[2]                 = CnSymm.at(cSym)[2];
                    hlpArr[3]                 = CnSymm.at(cSym)[3];
                    hlpVec.emplace_back       ( hlpArr );
                    ret.emplace_back          ( hlpVec );
                    *octaSymmPeakAvg         += hlpArr[4];
                    totPairs                 += 1.0;
                }
            }
        }
    }
    
    //======================================== Find overall average peak
    *octaSymmPeakAvg                         /= totPairs;
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief This function detects the icosahedral symmetries in the structure.
 
 This function takes the C symmetries list and checks for existence of C5 symmetries. All of these are then tested against all the C3 symmetries
 found in the data and if the relative angle of their axes is equal to acos ( sqrt ( 5.0 ) / 3.0 ), then the structure has to have the
 icosahedral symmetry.
 
 \param[in] CnSymm List of the already detected C symmetries.
 \param[in] icosSymmPeakAvg A pointer to variable holding the average peak value for all icosahedral symmetry supporting C symmetries.
 \param[in] axisErrorTolerance The tolerance on the axes angle error.
 \param[out] X List of icosahedral symmetry showing C symmetries.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::vector< std::array<double,5> > > ProSHADE_internal::ProSHADE_comparePairwise::findIcosSymmetry  ( std::vector< std::array<double,5> > CnSymm,
                                                                                                                    double *icosSymmPeakAvg,
                                                                                                                    double axisErrorTolerance )
{
    //======================================== Intialise local variables
    std::vector< std::vector< std::array<double,5> > > ret;
    std::vector< std::array<double,5> > hlpVec;
    std::array<double,5> hlpArr;
    std::vector<unsigned int> C5s;
    double dotProduct                         = 0.0;
    *icosSymmPeakAvg                          = 0.0;
    double totPairs                           = 0.0;
    
    //======================================== Find all C5 symmetries
    for ( unsigned int cSym = 0; cSym < static_cast<unsigned int> ( CnSymm.size() ); cSym++ )
    {
        if ( CnSymm.at(cSym)[0] == 5 )
        {
            C5s.emplace_back                  ( cSym );
        }
    }
    
    //======================================== Check for any C3 symmetry with axis angle acos ( sqrt ( 5.0 ) / 3.0 )
    for ( unsigned int c5Sym = 0; c5Sym < static_cast<unsigned int> ( C5s.size() ); c5Sym++ )
    {
        for ( unsigned int cSym = 0; cSym < static_cast<unsigned int> ( CnSymm.size() ); cSym++ )
        {
            if ( CnSymm.at(cSym)[0] == 3 )
            {
                dotProduct                    = ( CnSymm.at(C5s.at(c5Sym))[1] * CnSymm.at(cSym)[1] +
                                                  CnSymm.at(C5s.at(c5Sym))[2] * CnSymm.at(cSym)[2] +
                                                   CnSymm.at(C5s.at(c5Sym))[3] * CnSymm.at(cSym)[3] );
                
                if ( ( ( sqrt ( 5.0 ) / 3.0 ) > ( dotProduct - axisErrorTolerance ) ) &&
                     ( ( sqrt ( 5.0 ) / 3.0 ) < ( dotProduct + axisErrorTolerance ) ) )
                {
                    //======================== Found icosahedral symmetry
                    hlpVec.clear              ( );
                    
                    hlpArr[0]                 = CnSymm.at(C5s.at(c5Sym))[0];
                    hlpArr[1]                 = CnSymm.at(C5s.at(c5Sym))[1];
                    hlpArr[2]                 = CnSymm.at(C5s.at(c5Sym))[2];
                    hlpArr[3]                 = CnSymm.at(C5s.at(c5Sym))[3];
                    hlpArr[4]                 = ( CnSymm.at(C5s.at(c5Sym))[4] + CnSymm.at(cSym)[4] ) / 2.0;
                    hlpVec.emplace_back       ( hlpArr );
                    
                    hlpArr[0]                 = CnSymm.at(cSym)[0];
                    hlpArr[1]                 = CnSymm.at(cSym)[1];
                    hlpArr[2]                 = CnSymm.at(cSym)[2];
                    hlpArr[3]                 = CnSymm.at(cSym)[3];
                    hlpVec.emplace_back       ( hlpArr );
                    ret.emplace_back          ( hlpVec );
                    *icosSymmPeakAvg         += hlpArr[4];
                    totPairs                 += 1.0;
                }
            }
        }
    }
    
    //======================================== Find overall average peak
    *icosSymmPeakAvg                         /= totPairs;
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief This function generates all the tetrahedral symmetry axes from a pair detected by findTetrSymmetry function.
 
 This function generates the tetrahedral symmetry group axes. It takes two C3 symmetries with angle acos(1/3) between them as a starting point
 and then proceeds to search for 2 most C3s, so that there would be alltogether four C3 symmetries, each with angle acos(1/3) to any other. If one of the C3's is not found, it will search
 for it in the inverse SO(3) FT map. Once these are located, three C2 symmetries perpendicular to each other are located each in between two C3s. Again, if there is one missing, the
 function will try to search for it. Once these are found, all the seven axes are returned; if not all seven axes are found, the function returns an empty vector.
 
 \param[in] cmpObj This is the object of ProSHADE_comparePairwise class, which allows access to the original inverse SO(3) FT map, so that missing axes detection can be done.
 \param[in] settings The ProSHADE_settings container class instance with information about how to write the HTML report.
 \param[in] tetrSymm This is the pair of symmetry axes detected by the findTetrSymmetry function.
 \param[in] allCs The list of all symmetry axes in the whole object, from which the appropriate axes will be selected.
 \param[in] axisErrorTolerance The tolerance on the axis angle error.
 \param[in] verbose The amount of progress comments to be outputted to the std out.
 \param[out] X List of all axes making the tetrahedral symmetry group.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::array<double,5> > ProSHADE_internal::ProSHADE_symmetry::generateTetrAxes ( ProSHADE_comparePairwise* cmpObj,
                                                                                             ProSHADE::ProSHADE_settings* settings,
                                                                                             std::vector< std::array<double,5> > tetrSymms,
                                                                                             std::vector< std::array<double,5> > allCs,
                                                                                             double axisErrorTolerance,
                                                                                             int verbose )
{
    //======================================== Initialise
    std::vector< std::array<double,5> > ret;
    std::array<double,5> hlpArr;
    bool allAnglesMet                         = false;
    double dotProduct                         = 0.0;
    
    //======================================== Push the two already known C3 to ret. This is to allow different starting points, should the first one fail
    ret.emplace_back                          ( tetrSymms.at(0) );
    ret.emplace_back                          ( tetrSymms.at(1) );
    
    if ( verbose > 1 )
    {
        std::cout << ">> Searching for tetrahederal symmetry axes." << std::endl;
    }
    if ( verbose > 2 )
    {
        std::cout << ">>>>> Searching for the four C3 symmetry axes." << std::endl;
    }
    
    //======================================== Find four C3 symmetries with acos(1/3) angle between them
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( allCs.size() ); iter++ )
    {
        //==================================== If already found four, do not bother
        if ( ret.size() > 2 )
        {
            break;
        }
        
        //==================================== Search only using C3s
        if ( allCs.at(iter)[0] != 3.0 )
        {
            continue;
        }
        
        //==================================== If this is the first C3, then just save it
        if ( ret.size() == 0 )
        {
            ret.emplace_back                  ( allCs.at(iter) );
            continue;
        }
        
        //==================================== If second or more C3, check if it has the correct angle to all other already found C3s
        allAnglesMet                          = true;
        for ( unsigned int i = 0; i < static_cast<unsigned int> ( ret.size() ); i++ )
        {
            dotProduct                        = ( ret.at(i)[1] * allCs.at(iter)[1] +
                                                  ret.at(i)[2] * allCs.at(iter)[2] +
                                                  ret.at(i)[3] * allCs.at(iter)[3] );
            
            if ( ( ( 1.0 / 3.0 ) > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                 ( ( 1.0 / 3.0 ) < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
            {
                ;
            }
            else
            {
                allAnglesMet              = false;
            }
        }

        if ( allAnglesMet )
        {
            ret.emplace_back                  ( allCs.at(iter) );
        }
    }
        
    //======================================== Not enough C3's. Report empty - no tetrahedral elements found
    if ( ret.size() != 3 )
    {
        if ( verbose > 3 )
        {
            std::cout << ">>>>>>>> Found only " << ret.size() << " C3 symmetries. Searching for the missing ones." << std::endl;
        }
        
        //==================================== If just one is missing, try to find it
        if ( ret.size() == 2 )
        {
            //================================ Find the missing axes
            std::vector< std::array<double,3> > axesVec;
            std::array<double,3> hArr;
            double axNorm, axX, axY, axZ;
            for ( double xIt = -1.0; xIt < 1.001; xIt += 0.2 )
            {
                for ( double yIt = -1.0; yIt < 1.001; yIt += 0.2 )
                {
                    for ( double zIt = -1.0; zIt < 1.001; zIt += 0.2 )
                    {
                        if ( xIt == 0.0 && yIt == 0.0 && zIt == 0.0 ) { continue; }
                        
                        axNorm                = sqrt ( pow ( xIt, 2.0 ) + pow ( yIt, 2.0 ) + pow ( zIt, 2.0 ) );
                        axX                   = xIt / axNorm;
                        axY                   = yIt / axNorm;
                        axZ                   = zIt / axNorm;
                        
                        allAnglesMet          = true;
                        for ( unsigned int it = 0; it < static_cast<unsigned int> ( ret.size() ); it++ )
                        {
                            dotProduct        = ( ret.at(it)[1] * axX +
                                                  ret.at(it)[2] * axY +
                                                  ret.at(it)[3] * axZ );
                            
                            if ( ( ( 1.0 / 3.0 ) > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                                 ( ( 1.0 / 3.0 ) < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
                            {
                                ;
                            }
                            else
                            {
                                allAnglesMet  = false;
                                continue;
                            }
                        }
                        
                        if ( allAnglesMet )
                        {
                            hArr[0]           = axX;
                            hArr[1]           = axY;
                            hArr[2]           = axZ;
                            
                            allAnglesMet = true;
                            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( axesVec.size() ); axIt++ )
                            {
                                if ( ( ( axesVec.at(axIt)[0] > ( axX - axisErrorTolerance ) ) && ( axesVec.at(axIt)[0] < ( axX + axisErrorTolerance ) ) ) &&
                                     ( ( axesVec.at(axIt)[1] > ( axY - axisErrorTolerance ) ) && ( axesVec.at(axIt)[1] < ( axY + axisErrorTolerance ) ) ) &&
                                     ( ( axesVec.at(axIt)[2] > ( axZ - axisErrorTolerance ) ) && ( axesVec.at(axIt)[2] < ( axZ + axisErrorTolerance ) ) ) )
                                {
                                    allAnglesMet = false;
                                }
                            }
                            
                            if ( allAnglesMet )
                            {
                                axesVec.emplace_back ( hArr );
                            }
                        }
                    }
                }
            }
            
            std::vector< std::array<double,5> > hRes;
            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( axesVec.size() ); axIt++ )
            {
                //============================ Check if this axis solves the missing problem
                double res                    = cmpObj->maxAvgPeakForSymmetry ( axesVec.at(axIt)[0], axesVec.at(axIt)[1], axesVec.at(axIt)[2], 3.0, settings );
                
                hlpArr[0]                     = 3.0;
                hlpArr[1]                     = axesVec.at(axIt)[0];
                hlpArr[2]                     = axesVec.at(axIt)[1];
                hlpArr[3]                     = axesVec.at(axIt)[2];
                hlpArr[4]                     = res;
                hRes.emplace_back             ( hlpArr );
            }
            
            std::sort                         ( hRes.begin(), hRes.end(), [](const std::array<double,5>& a, const std::array<double,5>& b) { return a[4] > b[4]; });
            
            if ( hRes.at(0)[4] > ( cmpObj->getPeakThreshold ( ) ) )
            {
                ret.emplace_back              ( hRes.at(0) );
            }
            
            if ( ret.size() != 4 )
            {
                ret.clear                     ( );
                return ( ret );
            }
        }
        else
        {
            ret.clear                         ( );
            return ( ret );
        }
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> All C3 symmetries located." << std::endl;
    }
    
    if ( verbose > 2 )
    {
        std::cout << ">>>>> Searching for the three perpendicular C2 symmetry axes." << std::endl;
    }
    
    //======================================== Search for the D222 required; find three C2s which are perpendicular to each other and have angle acos(1/6) to the first C3
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( allCs.size() ); iter++ )
    {
        //==================================== Search only using C2s
        if ( allCs.at(iter)[0] != 2.0 )
        {
            continue;
        }

        //==================================== C2 having angle acos(0.5) to the first C3?
        dotProduct                            = ( ret.at(0)[1] * allCs.at(iter)[1] +
                                                  ret.at(0)[2] * allCs.at(iter)[2] +
                                                  ret.at(0)[3] * allCs.at(iter)[3] );

        if ( ( ( 1.0 / 2.0 ) > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
             ( ( 1.0 / 2.0 ) < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
        {
            //================================ If first C2, just save it
            if ( ret.size() == 4 )
            {
                ret.emplace_back              ( allCs.at(iter) );
                continue;
            }
            
            //================================ Else, check if perpendicular to other C2s
            allAnglesMet                      = true;
            for ( unsigned int i = 4; i < static_cast<unsigned int> ( ret.size() ); i++ )
            {
                dotProduct                    = ( ret.at(i)[1] * allCs.at(iter)[1] +
                                                  ret.at(i)[2] * allCs.at(iter)[2] +
                                                  ret.at(i)[3] * allCs.at(iter)[3] );
                
                if ( ( 0.0 > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                     ( 0.0 < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
                {
                    ;
                }
                else
                {
                    allAnglesMet              = false;
                }
            }
            
            if ( allAnglesMet )
            {
                ret.emplace_back              ( allCs.at(iter) );
            }
        }
    }
    
    //======================================== If not found all, report so
    if ( ret.size() != 6 )
    {
        if ( verbose > 3 )
        {
            std::cout << ">>>>>>>> Found only " << ret.size() - 4 << " C3 symmetries. Searching for the missing ones." << std::endl;
        }
        
        //==================================== If just one is missing, try to find it
        if ( ret.size() == 6 )
        {
            //================================ Find the missing axes
            std::vector< std::array<double,3> > axesVec;
            std::array<double,3> hArr;
            double axNorm, axX, axY, axZ;
            for ( double xIt = -1.0; xIt < 1.001; xIt += 0.2 )
            {
                for ( double yIt = -1.0; yIt < 1.001; yIt += 0.2 )
                {
                    for ( double zIt = -1.0; zIt < 1.001; zIt += 0.2 )
                    {
                        if ( xIt == 0.0 && yIt == 0.0 && zIt == 0.0 ) { continue; }
                        
                        axNorm                = sqrt ( pow ( xIt, 2.0 ) + pow ( yIt, 2.0 ) + pow ( zIt, 2.0 ) );
                        axX                   = xIt / axNorm;
                        axY                   = yIt / axNorm;
                        axZ                   = zIt / axNorm;
                        
                        allAnglesMet          = true;
                        for ( unsigned int it = 0; it < static_cast<unsigned int> ( ret.size() ); it++ )
                        {
                            dotProduct        = ( ret.at(it)[1] * axX +
                                                  ret.at(it)[2] * axY +
                                                  ret.at(it)[3] * axZ );
                            
                            if ( ( 0.0 > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                                 ( 0.0 < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
                            {
                                ;
                            }
                            else
                            {
                                allAnglesMet  = false;
                                continue;
                            }
                        }
                        
                        if ( allAnglesMet )
                        {
                            hArr[0]           = axX;
                            hArr[1]           = axY;
                            hArr[2]           = axZ;
                            
                            allAnglesMet      = true;
                            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( axesVec.size() ); axIt++ )
                            {
                                if ( ( ( axesVec.at(axIt)[0] > ( axX - axisErrorTolerance ) ) && ( axesVec.at(axIt)[0] < ( axX + axisErrorTolerance ) ) ) &&
                                     ( ( axesVec.at(axIt)[1] > ( axY - axisErrorTolerance ) ) && ( axesVec.at(axIt)[1] < ( axY + axisErrorTolerance ) ) ) &&
                                     ( ( axesVec.at(axIt)[2] > ( axZ - axisErrorTolerance ) ) && ( axesVec.at(axIt)[2] < ( axZ + axisErrorTolerance ) ) ) )
                                {
                                    allAnglesMet = false;
                                }
                            }
                            
                            if ( allAnglesMet )
                            {
                                axesVec.emplace_back ( hArr );
                            }
                        }
                    }
                }
            }
            
            std::vector< std::array<double,5> > hRes;
            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( axesVec.size() ); axIt++ )
            {
                //============================ Check if this axis solves the missing problem
                double res                    = cmpObj->maxAvgPeakForSymmetry ( axesVec.at(axIt)[0], axesVec.at(axIt)[1], axesVec.at(axIt)[2], 2.0, settings );
                
                hlpArr[0]                     = 2.0;
                hlpArr[1]                     = axesVec.at(axIt)[0];
                hlpArr[2]                     = axesVec.at(axIt)[1];
                hlpArr[3]                     = axesVec.at(axIt)[2];
                hlpArr[4]                     = res;
                hRes.emplace_back             ( hlpArr );
            }
            
            std::sort                         ( hRes.begin(), hRes.end(), [](const std::array<double,5>& a, const std::array<double,5>& b) { return a[4] > b[4]; });
            
            if ( hRes.at(0)[4] > ( cmpObj->getPeakThreshold ( ) ) )
            {
                ret.emplace_back              ( hRes.at(0) );
            }
            
            if ( ret.size() != 7 )
            {
                ret.clear                     ( );
                return                        ( ret );
            }
        }
        else
        {
            ret.clear                         ( );
            return                            ( ret );
        }
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> All C2 symmetries located." << std::endl;
    }
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief This function generates the 12 unique tetrahedral symmetry group elements from the already detected axes.
 
 \param[in] symmAxes The axes forming the tetrahedral symmetry as returned by the generateTetrAxes () function.
 \param[in] settings The ProSHADE_settings container class instance with information about how to write the HTML report.
 \param[in] verbose The amount of progress comments to be outputted to the std out.
 \param[out] X List of all elements making the tetrahedral symmetry group.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::array<double,5> > ProSHADE_internal::ProSHADE_symmetry::generateTetrElements ( std::vector< std::array<double,5> > symmAxes,
                                                                                                 ProSHADE::ProSHADE_settings* settings,
                                                                                                 int verbose )
{
    //======================================== Initialise
    std::vector< std::array<double,5> > ret;
    std::array<double,5> hlpArr;
    if ( verbose > 1 )
    {
        std::cout << ">> Searching for tetrahederal symmetry elements." << std::endl;
    }
    
    //============== Identity element
    hlpArr[0]                                 = 1.0;
    hlpArr[1]                                 = 1.0;
    hlpArr[2]                                 = 0.0;
    hlpArr[3]                                 = 0.0;
    hlpArr[4]                                 = 0.0;
    ret.emplace_back                          ( hlpArr );
    
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> Identity element." << std::endl;
    }
    
    //======================================== 3 times 120 degrees by each vertex axes clockwise
    for ( unsigned int iter = 0; iter < 3; iter++ )
    {
        hlpArr[0]                             = 3.0;
        hlpArr[1]                             = symmAxes.at(iter)[1];
        hlpArr[2]                             = symmAxes.at(iter)[2];
        hlpArr[3]                             = symmAxes.at(iter)[3];
        hlpArr[4]                             = 120.0;
        ret.emplace_back                      ( hlpArr );
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> 4 clockwise C3 rotations." << std::endl;
    }
    
    //======================================== 4 times 120 degrees by each vertex axes anti-clockwise
    for ( unsigned int iter = 0; iter < 3; iter++ )
    {
        hlpArr[0]                             = 3.0;
        hlpArr[1]                             = symmAxes.at(iter)[1];
        hlpArr[2]                             = symmAxes.at(iter)[2];
        hlpArr[3]                             = symmAxes.at(iter)[3];
        hlpArr[4]                             = -120.0;
        ret.emplace_back                      ( hlpArr );
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> 4 anti-clockwise C3 rotations." << std::endl;
    }
    
    //======================================== 3 times 180 degrees by each D222 axis
    for ( unsigned int iter = 3; iter < 6; iter++ )
    {
        hlpArr[0]                             = 2.0;
        hlpArr[1]                             = symmAxes.at(iter)[1];
        hlpArr[2]                             = symmAxes.at(iter)[2];
        hlpArr[3]                             = symmAxes.at(iter)[3];
        hlpArr[4]                             = 180.0;
        ret.emplace_back                      ( hlpArr );
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> 3 clockwise D222 rotations." << std::endl;
    }
    if ( verbose > 2 )
    {
        std::cout << ">>>>> Tetrahedral symmetry elements generated." << std::endl;
    }
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief This function generates all the ctahedral symmetry elements from a pair detected by findOctaSymmetry function.
 
 This function searches the already detected Cn's for the presence of (cub)octahedral symmetry. It does so by first detecting three perpendicular C4 symmetries; in the case of one of these
 missing, it goes to search for it in the inverse SO(3) FT map. Then, it searches for the four C3 symmetries, which should have angle to all C4's of cos ( 1 / sqrt(3) ) - the dihedral angle.
 If not all C3 symmetries are found, it again goes to search for them. Finally, the function searches for the six C2 symmetries which should have angle of either cos(1/sqrt(2)) or cos(0) to
 the C4's. Again, if some are missing, it will search for them. The function then returns all 13 unique symmetry elements, or empty vector if it failed.
 
 \param[in] cmpObj This is the object of ProSHADE_comparePairwise class, which allows access to the original inverse SO(3) FT map, so that missing axes detection can be done.
 \param[in] settings The ProSHADE_settings container class instance with information about how to write the HTML report.
 \param[in] octaSymm This is the pair of symmetry axes detected by the findOctaSymmetry function.
 \param[in] allCs The list of all symmetry axes in the whole object, from which the appropriate axes will be selected.
 \param[in] axisErrorTolerance The tolerance on the axis angle error.
 \param[in] verbose The amount of progress comments to be outputted to the std out.
 \param[out] X List of all elements making the (cub)octahedral symmetry group, or empty if the function failed.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::array<double,5> > ProSHADE_internal::ProSHADE_symmetry::generateOctaAxes ( ProSHADE_comparePairwise* cmpObj,
                                                                                             ProSHADE::ProSHADE_settings* settings,
                                                                                             std::vector< std::array<double,5> > octaSymms,
                                                                                             std::vector< std::array<double,5> > allCs,
                                                                                             double axisErrorTolerance,
                                                                                             int verbose )
{
    //======================================== Initialise
    std::vector< std::array<double,5> > ret;
    std::array<double,5> hlpArr;
    bool allAnglesMet                         = false;
    double dotProduct                         = 0.0;

    //======================================== Push the already known C4 to ret. This is to allow different starting points, should the first one fail
    if ( octaSymms.at(0)[0] == 4.0 )
    {
        ret.emplace_back                      ( octaSymms.at(0) );
    }
    else
    {
        ret.emplace_back                      ( octaSymms.at(1) );
    }
    
    if ( verbose > 1 )
    {
        std::cout << ">> Searching for (cub)octahedral symmetry elements." << std::endl;
    }
    if ( verbose > 2 )
    {
        std::cout << ">>>>> Searching for the three C4 symmetry elements." << std::endl;
    }
    
    //======================================== Search for the other two C4's perpendicular to the starting C4
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( allCs.size() ); iter++ )
    {
        //==================================== Search only using C4s
        if ( allCs.at(iter)[0] != 4.0 )
        {
            continue;
        }
        
        //==================================== If first C4, check for it being perpendicular to the one already present
        if ( ret.size() == 1 )
        {
            dotProduct                        = ( ret.at(0)[1] * allCs.at(iter)[1] +
                                                  ret.at(0)[2] * allCs.at(iter)[2] +
                                                  ret.at(0)[3] * allCs.at(iter)[3] );
            
            if ( ( 0.0 > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                 ( 0.0 < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
            {
                ret.emplace_back              ( allCs.at(iter) );
                continue;
            }
        }
        
        //======================================== If two C4s already exist, check both against this one
        if ( ret.size() == 2 )
        {
            allAnglesMet                      = true;
            for ( unsigned int it = 0; it < static_cast<unsigned int> ( ret.size() ); it++ )
            {
                dotProduct                    = ( ret.at(it)[1] * allCs.at(iter)[1] +
                                                  ret.at(it)[2] * allCs.at(iter)[2] +
                                                  ret.at(it)[3] * allCs.at(iter)[3] );
                
                if ( ( 0.0 > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                     ( 0.0 < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
                {
                    ;
                }
                else
                {
                    allAnglesMet              = false;
                }
            }
            
            if ( allAnglesMet )
            {
                ret.emplace_back              ( allCs.at(iter) );
            }
        }
    }
    
    //======================================== Not enough C4's. Try to find the missing C4
    if ( ret.size() != 3 )
    {
        if ( verbose > 3 )
        {
            std::cout << ">>>>>>>> Found only " << ret.size() << " C4 symmetries. Searching for the missing ones." << std::endl;
        }
        
        //==================================== If just one is missing, try to find it
        if ( ret.size() == 2 )
        {
            //================================ Find the missing axes
            std::vector< std::array<double,3> > axesVec;
            std::array<double,3> hArr;
            double axNorm, axX, axY, axZ;
            for ( double xIt = -1.0; xIt < 1.001; xIt += 0.2 )
            {
                for ( double yIt = -1.0; yIt < 1.001; yIt += 0.2 )
                {
                    for ( double zIt = -1.0; zIt < 1.001; zIt += 0.2 )
                    {
                        if ( xIt == 0.0 && yIt == 0.0 && zIt == 0.0 ) { continue; }
                        
                        axNorm                = sqrt ( pow ( xIt, 2.0 ) + pow ( yIt, 2.0 ) + pow ( zIt, 2.0 ) );
                        axX                   = xIt / axNorm;
                        axY                   = yIt / axNorm;
                        axZ                   = zIt / axNorm;
                        
                        allAnglesMet          = true;
                        for ( unsigned int it = 0; it < static_cast<unsigned int> ( ret.size() ); it++ )
                        {
                            dotProduct        = ( ret.at(it)[1] * axX +
                                                  ret.at(it)[2] * axY +
                                                  ret.at(it)[3] * axZ );
                            
                            if ( ( 0.0 > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                                 ( 0.0 < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
                            {
                                ;
                            }
                            else
                            {
                                allAnglesMet  = false;
                                continue;
                            }
                        }
                        
                        if ( allAnglesMet )
                        {
                            hArr[0]           = axX;
                            hArr[1]           = axY;
                            hArr[2]           = axZ;
                            
                            allAnglesMet = true;
                            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( axesVec.size() ); axIt++ )
                            {
                                if ( ( ( axesVec.at(axIt)[0] > ( axX - axisErrorTolerance ) ) && ( axesVec.at(axIt)[0] < ( axX + axisErrorTolerance ) ) ) &&
                                     ( ( axesVec.at(axIt)[1] > ( axY - axisErrorTolerance ) ) && ( axesVec.at(axIt)[1] < ( axY + axisErrorTolerance ) ) ) &&
                                     ( ( axesVec.at(axIt)[2] > ( axZ - axisErrorTolerance ) ) && ( axesVec.at(axIt)[2] < ( axZ + axisErrorTolerance ) ) ) )
                                {
                                    allAnglesMet = false;
                                }
                            }
                            
                            if ( allAnglesMet )
                            {
                                axesVec.emplace_back ( hArr );
                            }
                        }
                    }
                }
            }
            
            std::vector< std::array<double,5> > hRes;
            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( axesVec.size() ); axIt++ )
            {
                //============================ Check if this axis solves the missing problem
                double res                    = cmpObj->maxAvgPeakForSymmetry ( axesVec.at(axIt)[0], axesVec.at(axIt)[1], axesVec.at(axIt)[2], 4.0, settings );
                
                hlpArr[0]                     = 4.0;
                hlpArr[1]                     = axesVec.at(axIt)[0];
                hlpArr[2]                     = axesVec.at(axIt)[1];
                hlpArr[3]                     = axesVec.at(axIt)[2];
                hlpArr[4]                     = res;
                hRes.emplace_back             ( hlpArr );
            }
            
            std::sort                         ( hRes.begin(), hRes.end(), [](const std::array<double,5>& a, const std::array<double,5>& b) { return a[4] > b[4]; });
            
            if ( hRes.at(0)[4] > ( cmpObj->getPeakThreshold ( ) ) )
            {
                ret.emplace_back              ( hRes.at(0) );
            }
            
            if ( ret.size() != 3 )
            {
                ret.clear                     ( );
                return                        ( ret );
            }
        }
        else
        {
            ret.clear                         ( );
            return                            ( ret );
        }
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> All C4 symmetries located." << std::endl;
    }
    
    if ( verbose > 2 )
    {
        std::cout << ">>>>> Searching for the four C3 symmetry elements." << std::endl;
    }

    //======================================== Search for the 4 unique C3s with angles 1/sqrt(3) to the C4s
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( allCs.size() ); iter++ )
    {
        //==================================== Search only using C3s
        if ( allCs.at(iter)[0] != 3.0 )
        {
            continue;
        }
        
        //==================================== Check that the prospective C3 has angle 1/sqrt(3) (dihedral angle) to the C4s
        allAnglesMet                          = true;
        for ( unsigned int it = 0; it < static_cast<unsigned int> ( ret.size() ); it++ )
        {
            //================================ Do not check the C3s against themselves
            if ( ret.at(it)[0] == 3.0 )
            {
                continue;
            }
            
            dotProduct                        = ( ret.at(it)[1] * allCs.at(iter)[1] +
                                                  ret.at(it)[2] * allCs.at(iter)[2] +
                                                  ret.at(it)[3] * allCs.at(iter)[3] );

            if ( ( ( 1.0 / sqrt ( 3.0 ) ) > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                 ( ( 1.0 / sqrt ( 3.0 ) ) < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
            {
                ;
            }
            else
            {
                allAnglesMet                  = false;
            }
        }
        
        if ( allAnglesMet )
        {
            ret.emplace_back                  ( allCs.at(iter) );
        }
    }
    
    //======================================== Not enough C3's. Try to find it
    if ( ret.size() != 7 )
    {
        if ( verbose > 3 )
        {
            std::cout << ">>>>>>>> Found only " << ret.size() - 3 << " C3 symmetries. Searching for the missing ones." << std::endl;
        }
        
        //==================================== If just one is missing, try to find it
        if ( ret.size() == 6 )
        {
            //================================ Find the missing axes
            std::vector< std::array<double,3> > axesVec;
            std::array<double,3> hArr;
            double axNorm, axX, axY, axZ;
            for ( double xIt = -1.0; xIt < 1.001; xIt += 0.2 )
            {
                for ( double yIt = -1.0; yIt < 1.001; yIt += 0.2 )
                {
                    for ( double zIt = -1.0; zIt < 1.001; zIt += 0.2 )
                    {
                        if ( xIt == 0.0 && yIt == 0.0 && zIt == 0.0 ) { continue; }
                        
                        axNorm                = sqrt ( pow ( xIt, 2.0 ) + pow ( yIt, 2.0 ) + pow ( zIt, 2.0 ) );
                        axX                   = xIt / axNorm;
                        axY                   = yIt / axNorm;
                        axZ                   = zIt / axNorm;
                        
                        allAnglesMet          = true;
                        for ( unsigned int it = 0; it < static_cast<unsigned int> ( ret.size() ); it++ )
                        {
                            //================ Do not check the C3s against themselves
                            if ( ret.at(it)[0] == 3.0 )
                            {
                                continue;
                            }
                            
                            dotProduct        = ( ret.at(it)[1] * axX +
                                                  ret.at(it)[2] * axY +
                                                  ret.at(it)[3] * axZ );
                            
                            if ( ( ( 1.0 / sqrt ( 3.0 ) ) > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                                 ( ( 1.0 / sqrt ( 3.0 ) ) < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
                            {
                                ;
                            }
                            else
                            {
                                allAnglesMet  = false;
                                continue;
                            }
                        }
                        
                        for ( unsigned int it = 0; it < static_cast<unsigned int> ( ret.size() ); it++ )
                        {
                            //================ Do check the C3s against themselves
                            if ( ret.at(it)[0] == 4.0 )
                            {
                                continue;
                            }
                            
                            dotProduct        = ( ret.at(it)[1] * axX +
                                                  ret.at(it)[2] * axY +
                                                  ret.at(it)[3] * axZ );
           
                            if ( ( 1.0 > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                                 ( 1.0 < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
                            {
                                allAnglesMet  = false;
                                continue;
                            }
                            else
                            {
                                ;
                            }
                        }
                    
                        if ( allAnglesMet )
                        {
                            hArr[0]           = axX;
                            hArr[1]           = axY;
                            hArr[2]           = axZ;
                            
                            allAnglesMet      = true;
                            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( axesVec.size() ); axIt++ )
                            {
                                if ( ( ( axesVec.at(axIt)[0] > ( axX - axisErrorTolerance ) ) && ( axesVec.at(axIt)[0] < ( axX + axisErrorTolerance ) ) ) &&
                                     ( ( axesVec.at(axIt)[1] > ( axY - axisErrorTolerance ) ) && ( axesVec.at(axIt)[1] < ( axY + axisErrorTolerance ) ) ) &&
                                     ( ( axesVec.at(axIt)[2] > ( axZ - axisErrorTolerance ) ) && ( axesVec.at(axIt)[2] < ( axZ + axisErrorTolerance ) ) ) )
                                {
                                    allAnglesMet = false;
                                }
                            }
                            
                            if ( allAnglesMet )
                            {
                                axesVec.emplace_back ( hArr );
                            }
                        }
                    }
                }
            }
            
            std::vector< std::array<double,5> > hRes;
            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( axesVec.size() ); axIt++ )
            {
                //============================ Check if this axis solves the missing problem
                double res                    = cmpObj->maxAvgPeakForSymmetry ( axesVec.at(axIt)[0], axesVec.at(axIt)[1], axesVec.at(axIt)[2], 3.0, settings );
                
                hlpArr[0]                     = 3.0;
                hlpArr[1]                     = axesVec.at(axIt)[0];
                hlpArr[2]                     = axesVec.at(axIt)[1];
                hlpArr[3]                     = axesVec.at(axIt)[2];
                hlpArr[4]                     = res;
                hRes.emplace_back             ( hlpArr );
            }
            
            std::sort                         ( hRes.begin(), hRes.end(), [](const std::array<double,5>& a, const std::array<double,5>& b) { return a[4] > b[4]; });
            
            if ( hRes.at(0)[4] > ( cmpObj->getPeakThreshold ( ) ) )
            {
                ret.emplace_back              ( hRes.at(0) );
            }
            
            if ( ret.size() != 7 )
            {
                ret.clear                     ( );
                return                        ( ret );
            }
        }
        else
        {
            ret.clear                         ( );
            return                            ( ret );
        }
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> All C3 symmetries located." << std::endl;
    }
    
    if ( verbose > 2 )
    {
        std::cout << ">>>>> Searching for the six C2 symmetry elements." << std::endl;
    }
    
    //======================================== Search for the 6 unique C2s with angles 1/sqrt(2) or 0.0 to the C4s
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( allCs.size() ); iter++ )
    {
        //==================================== Search only using C2s
        if ( allCs.at(iter)[0] != 2.0 )
        {
            continue;
        }
        
        //==================================== Check that the prospective C2 has angle 1/sqrt(2) or 0.0 to the C4s
        allAnglesMet                          = true;
        for ( unsigned int it = 0; it < static_cast<unsigned int> ( ret.size() ); it++ )
        {
            //====== Take only the C4s
            if ( ret.at(it)[0] != 4.0 )
            {
                continue;
            }
 
            dotProduct                        = ( ret.at(it)[1] * allCs.at(iter)[1] +
                                                  ret.at(it)[2] * allCs.at(iter)[2] +
                                                  ret.at(it)[3] * allCs.at(iter)[3] );
 
            if ( ( ( 0.0 > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                   ( 0.0 < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) ) ||
                 ( ( ( 1.0 / sqrt ( 2.0 ) ) > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                   ( ( 1.0 / sqrt ( 2.0 ) ) < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) ) )
            {
                ;
            }
            else
            {
                allAnglesMet                  = false;
            }
        }

        if ( allAnglesMet )
        {
            ret.emplace_back                  ( allCs.at(iter) );
        }
    }
    
    //======================================== Not enough C2's. Try to find them
    if ( ret.size() != 13 )
    {
        if ( verbose > 3 )
        {
            std::cout << ">>>>>>>> Found only " << ret.size() - 13 << " C2 symmetries. Searching for the missing ones." << std::endl;
        }
        
        //==================================== If just one is missing, try to find it
        if ( ret.size() > 10 )
        {
            //================================ Find the missing axes
            std::vector< std::array<double,3> > axesVec;
            std::array<double,3> hArr;
            double axNorm, axX, axY, axZ;
            for ( double xIt = -1.0; xIt < 1.001; xIt += 0.2 )
            {
                for ( double yIt = -1.0; yIt < 1.001; yIt += 0.2 )
                {
                    for ( double zIt = -1.0; zIt < 1.001; zIt += 0.2 )
                    {
                        if ( xIt == 0.0 && yIt == 0.0 && zIt == 0.0 ) { continue; }

                        axNorm                = sqrt ( pow ( xIt, 2.0 ) + pow ( yIt, 2.0 ) + pow ( zIt, 2.0 ) );
                        axX                   = xIt / axNorm;
                        axY                   = yIt / axNorm;
                        axZ                   = zIt / axNorm;

                        allAnglesMet          = true;
                        for ( unsigned int it = 0; it < static_cast<unsigned int> ( ret.size() ); it++ )
                        {
                            //================ Do not check the C3s and C4s
                            if ( ( ret.at(it)[0] == 3.0 ) || ( ret.at(it)[0] == 4.0 ) )
                            {
                                continue;
                            }

                            dotProduct        = ( ret.at(it)[1] * axX +
                                                  ret.at(it)[2] * axY +
                                                  ret.at(it)[3] * axZ );

                            if ( ( ( ( 1.0 / 2.0 ) > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                                   ( ( 1.0 / 2.0 ) < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) ) ||
                                 ( ( 0.0 > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                                   ( 0.0 < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) ) )
                            {
                                ;
                            }
                            else
                            {
                                allAnglesMet  = false;
                                continue;
                            }
                        }

                        if ( allAnglesMet )
                        {
                            hArr[0]           = axX;
                            hArr[1]           = axY;
                            hArr[2]           = axZ;

                            allAnglesMet      = true;
                            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( axesVec.size() ); axIt++ )
                            {
                                if ( ( ( axesVec.at(axIt)[0] > ( axX - axisErrorTolerance ) ) && ( axesVec.at(axIt)[0] < ( axX + axisErrorTolerance ) ) ) &&
                                     ( ( axesVec.at(axIt)[1] > ( axY - axisErrorTolerance ) ) && ( axesVec.at(axIt)[1] < ( axY + axisErrorTolerance ) ) ) &&
                                     ( ( axesVec.at(axIt)[2] > ( axZ - axisErrorTolerance ) ) && ( axesVec.at(axIt)[2] < ( axZ + axisErrorTolerance ) ) ) )
                                {
                                    allAnglesMet = false;
                                }
                            }
                            
                            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( ret.size() ); axIt++ )
                            {
                                if ( ( ret.at(axIt)[0] == 3.0 ) || ( ret.at(axIt)[0] == 4.0 ) )
                                {
                                    continue;
                                }
                                
                                if ( ( ( ret.at(axIt)[1] > ( axX - axisErrorTolerance ) ) && ( ret.at(axIt)[1] < ( axX + axisErrorTolerance ) ) ) &&
                                     ( ( ret.at(axIt)[2] > ( axY - axisErrorTolerance ) ) && ( ret.at(axIt)[2] < ( axY + axisErrorTolerance ) ) ) &&
                                     ( ( ret.at(axIt)[3] > ( axZ - axisErrorTolerance ) ) && ( ret.at(axIt)[3] < ( axZ + axisErrorTolerance ) ) ) )
                                {
                                    allAnglesMet = false;
                                }
                            }

                            if ( allAnglesMet )
                            {
                                axesVec.emplace_back ( hArr );
                            }
                        }
                    }
                }
            }

            std::vector< std::array<double,5> > hRes;
            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( axesVec.size() ); axIt++ )
            {
                //============================ Check if this axis solves the missing problem
                double res                    = cmpObj->maxAvgPeakForSymmetry ( axesVec.at(axIt)[0], axesVec.at(axIt)[1], axesVec.at(axIt)[2], 2.0, settings );

                hlpArr[0]                     = 2.0;
                hlpArr[1]                     = axesVec.at(axIt)[0];
                hlpArr[2]                     = axesVec.at(axIt)[1];
                hlpArr[3]                     = axesVec.at(axIt)[2];
                hlpArr[4]                     = res;
                hRes.emplace_back             ( hlpArr );
            }

            std::sort                         ( hRes.begin(), hRes.end(), [](const std::array<double,5>& a, const std::array<double,5>& b) { return a[4] > b[4]; });

            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( hRes.size() ); axIt++ )
            {
                if ( ret.size () != 13 )
                {
                    if ( hRes.at(axIt)[4] > ( cmpObj->getPeakThreshold ( ) ) )
                    {
                        ret.emplace_back      ( hRes.at(axIt) );
                    }
                }
            }

            if ( ret.size() != 13 )
            {
                ret.clear                     ( );
                return                        ( ret );
            }
        }
        else
        {
            ret.clear                         ( );
            return                            ( ret );
        }
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> All C2 symmetries located." << std::endl;
    }
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief This function generates the 24 octahedral symmetry group elements from the axes generated by generateOctaAxes function.
 
 \param[in] symmAxes The axes forming the tetrahedral symmetry as returned by the generateOctaAxes () function.
 \param[in] settings The ProSHADE_settings container class instance with information about how to write the HTML report.
 \param[in] verbose The amount of progress comments to be outputted to the std out.
 \param[out] X List of all elements making the octahedral symmetry group.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::array<double,5> > ProSHADE_internal::ProSHADE_symmetry::generateOctaElements ( std::vector< std::array<double,5> > symmAxes,
                                                                                                 ProSHADE::ProSHADE_settings* settings,
                                                                                                 int verbose )
{
    //======================================== Initialise
    std::vector< std::array<double,5> > ret;
    std::array<double,5> hlpArr;
    if ( verbose > 1 )
    {
        std::cout << ">> Searching for octahedral symmetry elements." << std::endl;
    }
    
    //======================================== Identity element
    hlpArr[0]                                 = 1.0;
    hlpArr[1]                                 = 1.0;
    hlpArr[2]                                 = 0.0;
    hlpArr[3]                                 = 0.0;
    hlpArr[4]                                 = 0.0;
    ret.emplace_back                          ( hlpArr );
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> Identity element." << std::endl;
    }
    
    //======================================== 3 x 180 around each C4 axis clockwise
    for ( unsigned int iter = 0; iter < 3; iter++ )
    {
        hlpArr[0]                             = 4.0;
        hlpArr[1]                             = symmAxes.at(iter)[1];
        hlpArr[2]                             = symmAxes.at(iter)[2];
        hlpArr[3]                             = symmAxes.at(iter)[3];
        hlpArr[4]                             = 180.0;
        ret.emplace_back                      ( hlpArr );
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> 3 clockwise 180 C4 rotations." << std::endl;
    }
    
    //======================================== 6 x +-90 around each C4 axis clockwise
    for ( unsigned int iter = 0; iter < 3; iter++ )
    {
        hlpArr[0]                             = 4.0;
        hlpArr[1]                             = symmAxes.at(iter)[1];
        hlpArr[2]                             = symmAxes.at(iter)[2];
        hlpArr[3]                             = symmAxes.at(iter)[3];
        hlpArr[4]                             = 90.0;
        ret.emplace_back                      ( hlpArr );
        
        hlpArr[0]                             = 4.0;
        hlpArr[1]                             = symmAxes.at(iter)[1];
        hlpArr[2]                             = symmAxes.at(iter)[2];
        hlpArr[3]                             = symmAxes.at(iter)[3];
        hlpArr[4]                             = -90.0;
        ret.emplace_back                      ( hlpArr );
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> 6 (anti)clockwise +-90 C4 rotations." << std::endl;
    }
    
    //======================================== 8 x +-120 around each C3 axis clockwise
    for ( unsigned int iter = 3; iter < 7; iter++ )
    {
        hlpArr[0]                             = 3.0;
        hlpArr[1]                             = symmAxes.at(iter)[1];
        hlpArr[2]                             = symmAxes.at(iter)[2];
        hlpArr[3]                             = symmAxes.at(iter)[3];
        hlpArr[4]                             = 120.0;
        ret.emplace_back                      ( hlpArr );
        
        hlpArr[0]                             = 3.0;
        hlpArr[1]                             = symmAxes.at(iter)[1];
        hlpArr[2]                             = symmAxes.at(iter)[2];
        hlpArr[3]                             = symmAxes.at(iter)[3];
        hlpArr[4]                             = -120.0;
        ret.emplace_back                      ( hlpArr );
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> 8 (anti)clockwise +-120 C3 rotations." << std::endl;
    }
    
    //======================================== 6 x 180 around each C2 axis clockwise
    for ( unsigned int iter = 7; iter < 13; iter++ )
    {
        hlpArr[0]                             = 2.0;
        hlpArr[1]                             = symmAxes.at(iter)[1];
        hlpArr[2]                             = symmAxes.at(iter)[2];
        hlpArr[3]                             = symmAxes.at(iter)[3];
        hlpArr[4]                             = 180.0;
        ret.emplace_back                      ( hlpArr );
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> 6 clockwise 120 C2 rotations." << std::endl;
    }
    
    if ( verbose > 2 )
    {
        std::cout << ">>>>> Octahederal symmetry elements generated." << std::endl;
    }
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief This function generates all the icosahedral symmetry elements from a pair detected by findIcosSymmetry function.
 
 This function generates the 31 unique symmetry group elements for the icosahedral symmetry group. It does so by first determining the six C5 symmetries with cos(0.5) angle to each other,
 searching for missing ones if it cannot find them in the allCs list. It then searches for the ten C3 symmetries, which have the dihedral angle cos(-sqrt(5)/3) to the closer three C5s and
 the ( cos( 1.0 - ( sqrt(5)/3 ) ) ) angle to the further three C5s. If any are missing, it will attempt to locate these missing axes in the map. Finally, searching for the fifteen C2
 symmetries with angles cos(0.0), cos(0.5) and cos(sqrt(3)/2) to the closest, mid-distance and furthest C5s. If all 31 axes are found, they are reported back, otherwise an empty vector is
 returned.
 
 \param[in] cmpObj This is the object of ProSHADE_comparePairwise class, which allows access to the original inverse SO(3) FT map, so that missing axes detection can be done.
 \param[in] settings The ProSHADE_settings container class instance with information about how to write the HTML report.
 \param[in] icosSymm This is the pair of symmetry axes detected by the findIcosSymmetry function.
 \param[in] allCs The list of all symmetry axes in the whole object, from which the appropriate axes will be selected.
 \param[in] axisErrorTolerance The tolerance on the axis angle error.
 \param[in] verbose The amount of progress comments to be outputted to the std out.
 \param[out] X List of all elements making the icosahedral symmetry group, or empty if the function failed.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::array<double,5> > ProSHADE_internal::ProSHADE_symmetry::generateIcosAxes ( ProSHADE_comparePairwise* cmpObj,
                                                                                             ProSHADE::ProSHADE_settings* settings,
                                                                                             std::vector< std::array<double,5> > icosSymms,
                                                                                             std::vector< std::array<double,5> > allCs,
                                                                                             double axisErrorTolerance,
                                                                                             int verbose )
{
    //======================================== Initialise
    std::vector< std::array<double,5> > ret;
    std::array<double,5> hlpArr;
    bool allAnglesMet                         = false;
    double dotProduct                         = 0.0;

    //======================================== Push the already known C5 to ret. This is to allow different starting points, should the first one fail
    if ( icosSymms.at(0)[0] == 5.0 )
    {
        ret.emplace_back                      ( icosSymms.at(0) );
    }
    else
    {
        ret.emplace_back                      ( icosSymms.at(1) );
    }

    if ( verbose > 1 )
    {
        std::cout << ">> Searching for icosahedral symmetry elements." << std::endl;
    }
    if ( verbose > 2 )
    {
        std::cout << ">>>>> Searching for the six C5 symmetry elements." << std::endl;
    }
    //======================================== Search for the other five C5's with the angle cos(0.5) to each other
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( allCs.size() ); iter++ )
    {
        //==================================== Search only using C5s
        if ( allCs.at(iter)[0] != 5.0 )
        {
            continue;
        }

        //==================================== Check the known C5's against the prospective one
        allAnglesMet                          = true;
        for ( unsigned int it = 0; it < static_cast<unsigned int> ( ret.size() ); it++ )
        {
            dotProduct                        = ( ret.at(it)[1] * allCs.at(iter)[1] +
                                                  ret.at(it)[2] * allCs.at(iter)[2] +
                                                  ret.at(it)[3] * allCs.at(iter)[3] );

            if ( ( ( 1.0 / 2.0 ) > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                 ( ( 1.0 / 2.0 ) < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
            {
                ;
            }
            else
            {
                allAnglesMet                  = false;
            }
        }
        
        if ( allAnglesMet )
        {
            
            bool alreadyExists                = false;
            for ( unsigned int i = 0; i < static_cast<unsigned int> ( ret.size() ); i++ )
            {
                if ( ( ( ( ret.at(i)[1] + axisErrorTolerance ) < ( allCs.at(iter)[1] ) ) && ( ( ret.at(i)[1] - axisErrorTolerance ) > ( allCs.at(iter)[1] ) ) ) &&
                     ( ( ( ret.at(i)[2] + axisErrorTolerance ) < ( allCs.at(iter)[2] ) ) && ( ( ret.at(i)[2] - axisErrorTolerance ) > ( allCs.at(iter)[2] ) ) ) &&
                     ( ( ( ret.at(i)[3] + axisErrorTolerance ) < ( allCs.at(iter)[3] ) ) && ( ( ret.at(i)[3] - axisErrorTolerance ) > ( allCs.at(iter)[3] ) ) ) )
                {
                    alreadyExists             = true;
                }
            }

            if ( !alreadyExists )
            {
                ret.emplace_back              ( allCs.at(iter) );
            }
            
        }
    }
    
    //======================================== Not enough C5's. Try to find the missing C5
    if ( ret.size() != 6 )
    {
        if ( verbose > 3 )
        {
            std::cout << ">>>>>>>> Found only " << ret.size() << " C5 symmetries. Searching for the missing ones." << std::endl;
        }
        
        //==================================== If few are missing, try to find them
        if ( ret.size() > 2 )
        {
            //================================ Find the missing axes
            std::vector< std::array<double,3> > axesVec;
            std::array<double,3> hArr;
            double axNorm, axX, axY, axZ;
            for ( double xIt = -1.0; xIt < 1.001; xIt += 0.2 )
            {
                for ( double yIt = -1.0; yIt < 1.001; yIt += 0.2 )
                {
                    for ( double zIt = -1.0; zIt < 1.001; zIt += 0.2 )
                    {
                        if ( xIt == 0.0 && yIt == 0.0 && zIt == 0.0 ) { continue; }
                        
                        axNorm                = sqrt ( pow ( xIt, 2.0 ) + pow ( yIt, 2.0 ) + pow ( zIt, 2.0 ) );
                        axX                   = xIt / axNorm;
                        axY                   = yIt / axNorm;
                        axZ                   = zIt / axNorm;
                        
                        allAnglesMet          = true;
                        for ( unsigned int it = 0; it < static_cast<unsigned int> ( ret.size() ); it++ )
                        {
                            dotProduct        = ( ret.at(it)[1] * axX +
                                                  ret.at(it)[2] * axY +
                                                  ret.at(it)[3] * axZ );
                            
                            if ( ( ( 1.0 / 2.0 ) > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                                 ( ( 1.0 / 2.0 ) < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
                            {
                                ;
                            }
                            else
                            {
                                allAnglesMet  = false;
                                continue;
                            }
                        }
                        
                        if ( allAnglesMet )
                        {
                            hArr[0]           = axX;
                            hArr[1]           = axY;
                            hArr[2]           = axZ;
                            
                            allAnglesMet      = true;
                            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( axesVec.size() ); axIt++ )
                            {
                                if ( ( ( axesVec.at(axIt)[0] > ( axX - axisErrorTolerance ) ) && ( axesVec.at(axIt)[0] < ( axX + axisErrorTolerance ) ) ) &&
                                     ( ( axesVec.at(axIt)[1] > ( axY - axisErrorTolerance ) ) && ( axesVec.at(axIt)[1] < ( axY + axisErrorTolerance ) ) ) &&
                                     ( ( axesVec.at(axIt)[2] > ( axZ - axisErrorTolerance ) ) && ( axesVec.at(axIt)[2] < ( axZ + axisErrorTolerance ) ) ) )
                                {
                                    allAnglesMet = false;
                                }
                            }
                            
                            if ( allAnglesMet )
                            {
                                axesVec.emplace_back ( hArr );
                            }
                        }
                    }
                }
            }
            
            std::vector< std::array<double,5> > hRes;
            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( axesVec.size() ); axIt++ )
            {
                //============================ Check if this axis solves the missing problem
                double res                    = cmpObj->maxAvgPeakForSymmetry ( axesVec.at(axIt)[0], axesVec.at(axIt)[1], axesVec.at(axIt)[2], 5.0, settings );
                
                hlpArr[0]                     = 5.0;
                hlpArr[1]                     = axesVec.at(axIt)[0];
                hlpArr[2]                     = axesVec.at(axIt)[1];
                hlpArr[3]                     = axesVec.at(axIt)[2];
                hlpArr[4]                     = res;
                hRes.emplace_back             ( hlpArr );
            }
            
            std::sort                         ( hRes.begin(), hRes.end(), [](const std::array<double,5>& a, const std::array<double,5>& b) { return a[4] > b[4]; });
            
            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( hRes.size() ); axIt++ )
            {
                if ( ret.size() != 6 )
                {
                    if ( hRes.at(axIt)[4] > ( cmpObj->getPeakThreshold ( ) ) )
                    {
                        ret.emplace_back      ( hRes.at(axIt) );
                    }
                }
            }
            
            if ( ret.size() != 6 )
            {
                ret.clear                     ( );
                return                        ( ret );
            }
        }
        else
        {
            ret.clear                         ( );
            return                            ( ret );
        }
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> All C5 symmetries located." << std::endl;
    }
    
    if ( verbose > 2 )
    {
        std::cout << ">>>>> Searching for the ten C3 symmetry elements." << std::endl;
    }
    
    //======================================== Search for the ten C3's with the angle cos(-sqrt(5)/3) and 1.0 - cos(-sqrt(5)/3) to all the C5's
    double closeC5s                           = 0.0;
    double awayC5s                            = 0.0;
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( allCs.size() ); iter++ )
    {
        //==================================== Search only using C3s
        if ( allCs.at(iter)[0] != 3.0 )
        {
            continue;
        }
        
        //==================================== Check the known C5's against the new C3
        allAnglesMet                          = true;
        closeC5s                              = 0.0;
        awayC5s                               = 0.0;
        for ( unsigned int it = 0; it < static_cast<unsigned int> ( ret.size() ); it++ )
        {
            //================================ Use only the C5's
            if ( ret.at(it)[0] != 5.0 )
            {
                continue;
            }
            
            dotProduct                        = ( ret.at(it)[1] * allCs.at(iter)[1] +
                                                  ret.at(it)[2] * allCs.at(iter)[2] +
                                                  ret.at(it)[3] * allCs.at(iter)[3] );
        
            if ( ( ( sqrt ( 5.0 ) / 3.0 ) > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                 ( ( sqrt ( 5.0 ) / 3.0 ) < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
            {
                closeC5s                     += 1.0;
                continue;
            }
            if ( ( ( 1.0 - ( sqrt ( 5.0 ) / 3.0 ) ) > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                 ( ( 1.0 - ( sqrt ( 5.0 ) / 3.0 ) ) < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
            {
                awayC5s                      += 1.0;
                continue;
            }
        }
        
        if ( ( closeC5s == 3.0 ) && ( awayC5s == 3.0 ) )
        {
            ret.emplace_back ( allCs.at(iter) );
        }
    }
    
    //======================================== Not enough C3's. Try to find the missing C3
    if ( ret.size() != 16 )
    {
        if ( verbose > 3 )
        {
            std::cout << ">>>>>>>> Found only " << ret.size() - 6 << " C3 symmetries. Searching for the missing ones." << std::endl;
        }
        
        //==================================== If few are missing, try to find them
        if ( ret.size() > 5 )
        {
            //====== Find the missing axes
            std::vector< std::array<double,3 >> axesVec;
            std::array<double,3> hArr;
            double axNorm, axX, axY, axZ;
            for ( double xIt = -1.0; xIt < 1.001; xIt += 0.2 )
            {
                for ( double yIt = -1.0; yIt < 1.001; yIt += 0.2 )
                {
                    for ( double zIt = -1.0; zIt < 1.001; zIt += 0.2 )
                    {
                        if ( xIt == 0.0 && yIt == 0.0 && zIt == 0.0 ) { continue; }
                        
                        axNorm                = sqrt ( pow ( xIt, 2.0 ) + pow ( yIt, 2.0 ) + pow ( zIt, 2.0 ) );
                        axX                   = xIt / axNorm;
                        axY                   = yIt / axNorm;
                        axZ                   = zIt / axNorm;
                        
                        closeC5s              = 0.0;
                        awayC5s               = 0.0;
                        for ( unsigned int it = 0; it < static_cast<unsigned int> ( ret.size() ); it++ )
                        {
                            //====== Use only the C5's
                            if ( ret.at(it)[0] != 5.0 )
                            {
                                continue;
                            }
                            
                            dotProduct        = ( ret.at(it)[1] * axX +
                                                  ret.at(it)[2] * axY +
                                                  ret.at(it)[3] * axZ );
                            
                            if ( ( ( sqrt ( 5.0 ) / 3.0 ) > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                                 ( ( sqrt ( 5.0 ) / 3.0 ) < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
                            {
                                closeC5s     += 1.0;
                                continue;
                            }
                            if ( ( ( 1.0 - ( sqrt ( 5.0 ) / 3.0 ) ) > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                                 ( ( 1.0 - ( sqrt ( 5.0 ) / 3.0 ) ) < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
                            {
                                awayC5s      += 1.0;
                                continue;
                            }
                        }
                        
                        if ( ( closeC5s == 3.0 ) && ( awayC5s == 3.0 ) )
                        {
                            hArr[0]           = axX;
                            hArr[1]           = axY;
                            hArr[2]           = axZ;
                            
                            allAnglesMet      = true;
                            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( axesVec.size() ); axIt++ )
                            {
                                if ( ( ( axesVec.at(axIt)[0] > ( axX - axisErrorTolerance ) ) && ( axesVec.at(axIt)[0] < ( axX + axisErrorTolerance ) ) ) &&
                                     ( ( axesVec.at(axIt)[1] > ( axY - axisErrorTolerance ) ) && ( axesVec.at(axIt)[1] < ( axY + axisErrorTolerance ) ) ) &&
                                     ( ( axesVec.at(axIt)[2] > ( axZ - axisErrorTolerance ) ) && ( axesVec.at(axIt)[2] < ( axZ + axisErrorTolerance ) ) ) )
                                {
                                    allAnglesMet = false;
                                }
                            }
                            
                            if ( allAnglesMet )
                            {
                                axesVec.emplace_back ( hArr );
                            }
                        }
                    }
                }
            }
            
            std::vector< std::array<double,5> > hRes;
            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( axesVec.size() ); axIt++ )
            {
                //============================ Check if this axis solves the missing problem
                double res                    = cmpObj->maxAvgPeakForSymmetry ( axesVec.at(axIt)[0], axesVec.at(axIt)[1], axesVec.at(axIt)[2], 3.0, settings );
                
                hlpArr[0]                     = 3.0;
                hlpArr[1]                     = axesVec.at(axIt)[0];
                hlpArr[2]                     = axesVec.at(axIt)[1];
                hlpArr[3]                     = axesVec.at(axIt)[2];
                hlpArr[4]                     = res;
                hRes.emplace_back             ( hlpArr );
            }
            
            std::sort                         ( hRes.begin(), hRes.end(), [](const std::array<double,5>& a, const std::array<double,5>& b) { return a[4] > b[4]; });
            
            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( hRes.size() ); axIt++ )
            {
                if ( ret.size() != 16 )
                {
                    if ( hRes.at(axIt)[4] > ( cmpObj->getPeakThreshold ( ) ) )
                    {
                        ret.emplace_back ( hRes.at(axIt) );
                    }
                }
            }
            
            if ( ret.size() != 16 )
            {
                ret.clear                     ( );
                return                        ( ret );
            }
        }
        else
        {
            ret.clear                         ( );
            return                            ( ret );
        }
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> All C3 symmetries located." << std::endl;
    }
    
    if ( verbose > 2 )
    {
        std::cout << ">>>>> Searching for the fifteen C2 symmetry elements." << std::endl;
    }
    
    //======================================== Search for the fifteen C2's with the angle cos(0), cos(0.5) and cos(sqrt(3)/2) to all the C5's
    double perpC2s                            = 0.0;
    double sixthC2s                           = 0.0;
    double halfSixtbC2s                       = 0.0;
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( allCs.size() ); iter++ )
    {
        //==================================== Search only using C2s
        if ( allCs.at(iter)[0] != 2.0 )
        {
            continue;
        }
        
        //==================================== Check the known C5's against the new C2
        for ( unsigned int it = 0; it < static_cast<unsigned int> ( ret.size() ); it++ )
        {
            //================================ Use only the C5's
            if ( ret.at(it)[0] != 5.0 )
            {
                continue;
            }
            
            dotProduct                        = ( ret.at(it)[1] * allCs.at(iter)[1] +
                                                  ret.at(it)[2] * allCs.at(iter)[2] +
                                                  ret.at(it)[3] * allCs.at(iter)[3] );
            
            if ( ( 0.0 > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                 ( 0.0 < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
            {
                perpC2s                      += 1.0;
                continue;
            }
            if ( ( ( 1.0 / 2.0 ) > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                 ( ( 1.0 / 2.0 ) < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
            {
                sixthC2s                     += 1.0;
                continue;
            }
            if ( ( ( sqrt ( 3.0 ) / 2.0 ) > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                 ( ( sqrt ( 3.0 ) / 2.0 ) < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
            {
                halfSixtbC2s                 += 1.0;
                continue;
            }
        }
        
        if ( ( perpC2s == 2.0 ) && ( sixthC2s == 2.0 ) && ( halfSixtbC2s == 2.0 ) )
        {
            ret.emplace_back                  ( allCs.at(iter) );
        }
    }
    
    //======================================== Not enough C3's. Try to find the missing C3
    if ( ret.size() != 31 )
    {
        if ( verbose > 3 )
        {
            std::cout << ">>>>>>>> Found only " << ret.size() - 16 << " C2 symmetries. Searching for the missing ones." << std::endl;
        }
        
        //==================================== If few are missing, try to find them
        if ( ret.size() > 15 )
        {
            //====== Find the missing axes
            std::vector< std::array<double,3> > axesVec;
            std::array<double,3> hArr;
            double axNorm, axX, axY, axZ;
            for ( double xIt = -1.0; xIt < 1.001; xIt += 0.2 )
            {
                for ( double yIt = -1.0; yIt < 1.001; yIt += 0.2 )
                {
                    for ( double zIt = -1.0; zIt < 1.001; zIt += 0.2 )
                    {
                        if ( xIt == 0.0 && yIt == 0.0 && zIt == 0.0 ) { continue; }
                        
                        axNorm                = sqrt ( pow ( xIt, 2.0 ) + pow ( yIt, 2.0 ) + pow ( zIt, 2.0 ) );
                        axX                   = xIt / axNorm;
                        axY                   = yIt / axNorm;
                        axZ                   = zIt / axNorm;
                        
                        perpC2s               = 0.0;
                        sixthC2s              = 0.0;
                        halfSixtbC2s          = 0.0;
                        for ( unsigned int it = 0; it < static_cast<unsigned int> ( ret.size() ); it++ )
                        {
                            //================ Use only the C5's
                            if ( ret.at(it)[0] != 5.0 )
                            {
                                continue;
                            }
                            
                            dotProduct        = ( ret.at(it)[1] * axX +
                                                  ret.at(it)[2] * axY +
                                                  ret.at(it)[3] * axZ );
                             
                            if ( ( 0.0 > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                                 ( 0.0 < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
                            {
                                perpC2s      += 1.0;
                                continue;
                            }
                            if ( ( ( 1.0 / 2.0 ) > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                                 ( ( 1.0 / 2.0 ) < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
                            {
                                sixthC2s     += 1.0;
                                continue;
                            }
                            if ( ( ( sqrt ( 3.0 ) / 2.0 ) > ( std::abs ( dotProduct ) - axisErrorTolerance ) ) &&
                                 ( ( sqrt ( 3.0 ) / 2.0 ) < ( std::abs ( dotProduct ) + axisErrorTolerance ) ) )
                            {
                                halfSixtbC2s += 1.0;
                                continue;
                            }
                        }
                        
                        if ( ( perpC2s == 2.0 ) && ( sixthC2s == 2.0 ) && ( halfSixtbC2s == 2.0 ) )
                        {
                            hArr[0]           = axX;
                            hArr[1]           = axY;
                            hArr[2]           = axZ;
                            
                            allAnglesMet      = true;
                            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( axesVec.size() ); axIt++ )
                            {
                                if ( ( ( axesVec.at(axIt)[0] > ( axX - axisErrorTolerance ) ) && ( axesVec.at(axIt)[0] < ( axX + axisErrorTolerance ) ) ) &&
                                     ( ( axesVec.at(axIt)[1] > ( axY - axisErrorTolerance ) ) && ( axesVec.at(axIt)[1] < ( axY + axisErrorTolerance ) ) ) &&
                                     ( ( axesVec.at(axIt)[2] > ( axZ - axisErrorTolerance ) ) && ( axesVec.at(axIt)[2] < ( axZ + axisErrorTolerance ) ) ) )
                                {
                                    allAnglesMet = false;
                                }
                            }
                            
                            if ( allAnglesMet )
                            {
                                axesVec.emplace_back ( hArr );
                            }
                        }
                    }
                }
            }
            
            std::vector< std::array<double,5> > hRes;
            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( axesVec.size() ); axIt++ )
            {
                //============================ Check if this axis solves the missing problem
                double res                    = cmpObj->maxAvgPeakForSymmetry ( axesVec.at(axIt)[0], axesVec.at(axIt)[1], axesVec.at(axIt)[2], 2.0, settings );
                
                hlpArr[0]                     = 2.0;
                hlpArr[1]                     = axesVec.at(axIt)[0];
                hlpArr[2]                     = axesVec.at(axIt)[1];
                hlpArr[3]                     = axesVec.at(axIt)[2];
                hlpArr[4]                     = res;
                hRes.emplace_back             ( hlpArr );
            }
            
            std::sort                         ( hRes.begin(), hRes.end(), [](const std::array<double,5>& a, const std::array<double,5>& b) { return a[4] > b[4]; });
            
            for ( unsigned int axIt = 0; axIt < static_cast<unsigned int> ( hRes.size() ); axIt++ )
            {
                if ( ret.size() != 31 )
                {
                    if ( hRes.at(axIt)[4] > ( cmpObj->getPeakThreshold ( ) ) )
                    {
                        ret.emplace_back      ( hRes.at(axIt) );
                    }
                }
            }
            
            if ( ret.size() != 31 )
            {
                ret.clear                     ( );
                return                        ( ret );
            }
        }
        else
        {
            ret.clear                         ( );
            return                            ( ret );
        }
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> All C2 symmetries located." << std::endl;
    }
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief This function generates the 60 icosahedral symmetry group elements from the axes generated by generateIcosAxes function.
 
 \param[in] symmAxes The axes forming the tetrahedral symmetry as returned by the generateIcosAxes () function.
 \param[in] settings The ProSHADE_settings container class instance with information about how to write the HTML report.
 \param[in] verbose The amount of progress comments to be outputted to the std out.
 \param[out] X List of all elements making the icosahedral symmetry group.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector< std::array<double,5> > ProSHADE_internal::ProSHADE_symmetry::generateIcosElements ( std::vector< std::array<double,5> > symmAxes,
                                                                                                 ProSHADE::ProSHADE_settings* settings,
                                                                                                 int verbose )
{
    //======================================== Initialise
    std::vector< std::array<double,5> > ret;
    std::array<double,5> hlpArr;
    if ( verbose > 1 )
    {
        std::cout << ">> Searching for icosahedral symmetry elements." << std::endl;
    }
    
    //======================================== Identity element
    hlpArr[0]                                 = 1.0;
    hlpArr[1]                                 = 1.0;
    hlpArr[2]                                 = 0.0;
    hlpArr[3]                                 = 0.0;
    hlpArr[4]                                 = 0.0;
    ret.emplace_back                          ( hlpArr );
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> Identity element." << std::endl;
    }
    
    //======================================== 12 x +-72 around each C5 axis (anti)clockwise
    for ( unsigned int iter = 0; iter < 6; iter++ )
    {
        hlpArr[0]                             = 5.0;
        hlpArr[1]                             = symmAxes.at(iter)[1];
        hlpArr[2]                             = symmAxes.at(iter)[2];
        hlpArr[3]                             = symmAxes.at(iter)[3];
        hlpArr[4]                             = 72.0;
        ret.emplace_back                      ( hlpArr );
        
        hlpArr[0]                             = 5.0;
        hlpArr[1]                             = symmAxes.at(iter)[1];
        hlpArr[2]                             = symmAxes.at(iter)[2];
        hlpArr[3]                             = symmAxes.at(iter)[3];
        hlpArr[4]                             = -72.0;
        ret.emplace_back                      ( hlpArr );
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> 12 (anti)clockwise 144 C5 rotations." << std::endl;
    }
    
    //======================================== 12 x +-72 around each C5 axis (anti)clockwise
    for ( unsigned int iter = 0; iter < 6; iter++ )
    {
        hlpArr[0]                             = 5.0;
        hlpArr[1]                             = symmAxes.at(iter)[1];
        hlpArr[2]                             = symmAxes.at(iter)[2];
        hlpArr[3]                             = symmAxes.at(iter)[3];
        hlpArr[4]                             = 144.0;
        ret.emplace_back                      ( hlpArr );
        
        hlpArr[0]                             = 5.0;
        hlpArr[1]                             = symmAxes.at(iter)[1];
        hlpArr[2]                             = symmAxes.at(iter)[2];
        hlpArr[3]                             = symmAxes.at(iter)[3];
        hlpArr[4]                             = -144.0;
        ret.emplace_back                      ( hlpArr );
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> 12 (anti)clockwise 144 C5 rotations." << std::endl;
    }
    
    //======================================== 20 x +-120 around each C3 axis (anti)clockwise
    for ( unsigned int iter = 6; iter < 16; iter++ )
    {
        hlpArr[0]                             = 3.0;
        hlpArr[1]                             = symmAxes.at(iter)[1];
        hlpArr[2]                             = symmAxes.at(iter)[2];
        hlpArr[3]                             = symmAxes.at(iter)[3];
        hlpArr[4]                             = 120.0;
        ret.emplace_back                      ( hlpArr );
        
        hlpArr[0]                             = 3.0;
        hlpArr[1]                             = symmAxes.at(iter)[1];
        hlpArr[2]                             = symmAxes.at(iter)[2];
        hlpArr[3]                             = symmAxes.at(iter)[3];
        hlpArr[4]                             = -120.0;
        ret.emplace_back                      ( hlpArr );
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> 20 (anti)clockwise 120 C3 rotations." << std::endl;
    }
    
    //======================================== 15 x 180 around each C2 axis clockwise
    for ( unsigned int iter = 16; iter < 31; iter++ )
    {
        hlpArr[0]                             = 2.0;
        hlpArr[1]                             = symmAxes.at(iter)[1];
        hlpArr[2]                             = symmAxes.at(iter)[2];
        hlpArr[3]                             = symmAxes.at(iter)[3];
        hlpArr[4]                             = 180.0;
        ret.emplace_back                      ( hlpArr );
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> 15 clockwise 180 C2 rotations." << std::endl;
    }
    
    if ( verbose > 2 )
    {
        std::cout << ">>>>> Icosahedral symmetry elements generated." << std::endl;
    }
    
    //======================================== Done
    return ( ret );
    
}

/*! \brief Computes the translation function for the two objects and returns the position of the highest peak.
 
 This function takes two ProSHADE_data objects and proceeds to make sure they have the same dimensions and map sampling. This does change the maps, so keep this in mind.
 Then, it computes Fourier transform on both structures, combines the Fourier coefficients which are now on the same scale and inverts the Fourier transform. This results
 in the translation function map. Finally, finding the highest peak and its coordinates, the optimal overlay translation is obtained and transformed into translation in
 angstroms instead of indices. This translation in ANGSTROMS is then returned.
 
 \param[in] obj1 Pointer to the ProSHADE_data object which has the density map to be matched against obj2.
 \param[in] obj2 Pointer to the ProSHADE_data object which has the density map to be matched against obj1.
 \param[out] X Array of three doubles specifying the optimal overlay translation in angstroms between the two input objects.
 
 \warning This is an internal function which should not be used by the user.
 */
std::array<double,4> ProSHADE_internal::ProSHADE_comparePairwise::getTranslationFunctionMap ( ProSHADE_data *obj1,
                                                                                              ProSHADE_data *obj2,
                                                                                              double *ob2XMov,
                                                                                              double *ob2YMov,
                                                                                              double *ob2ZMov )
{
    //======================================== Initalise
    std::array<double,4> ret                  = std::array<double,4> { { 0.0, 0.0, 0.0, 0.0 } };
    int arrPos                                = 0;
    int hlpPos                                = 0;
    
    double newXSampling                       = std::min ( obj1->_xSamplingRate, obj2->_xSamplingRate );
    double newYSampling                       = std::min ( obj1->_ySamplingRate, obj2->_ySamplingRate );
    double newZSampling                       = std::min ( obj1->_zSamplingRate, obj2->_zSamplingRate );
    
    int newU                                  = 0;
    int newV                                  = 0;
    int newW                                  = 0;
    
    double newXRange                          = 0.0;
    double newYRange                          = 0.0;
    double newZRange                          = 0.0;
    
    //======================================== For structure 1, get the sampling right
    {
        //================================ Find cell parameters
        newU                                  = std::max ( obj1->_maxMapU, obj2->_maxMapU );
        newV                                  = std::max ( obj1->_maxMapV, obj2->_maxMapV );
        newW                                  = std::max ( obj1->_maxMapW, obj2->_maxMapW );

        if ( obj1->_maxMapU == static_cast<unsigned int> ( newU ) ) { newXRange = obj1->_xRange; } else { newXRange = obj2->_xRange; }
        if ( obj1->_maxMapV == static_cast<unsigned int> ( newV ) ) { newYRange = obj1->_yRange; } else { newYRange = obj2->_yRange; }
        if ( obj1->_maxMapW == static_cast<unsigned int> ( newW ) ) { newZRange = obj1->_zRange; } else { newZRange = obj2->_zRange; }
        
        int uDiff                             = newU - obj1->_maxMapU;
        int vDiff                             = newV - obj1->_maxMapV;
        int wDiff                             = newW - obj1->_maxMapW;
        
        int newXTo                            = 0;
        int newXFrom                          = 0;
        int newYTo                            = 0;
        int newYFrom                          = 0;
        int newZTo                            = 0;
        int newZFrom                          = 0;
        
        if ( uDiff % 2 == 0 )
        {
            newXTo                            = obj1->_xTo + (uDiff/2);
            newXFrom                          = obj1->_xFrom - (uDiff/2);
        }
        else
        {
            newXTo                            = obj1->_xTo + ((uDiff+1)/2);
            newXFrom                          = obj1->_xFrom - (((uDiff+1)/2)-1);
        }
        if ( vDiff % 2 == 0 )
        {
            newYTo                            = obj1->_yTo + (vDiff/2);
            newYFrom                          = obj1->_yFrom - (vDiff/2);
        }
        else
        {
            newYTo                            = obj1->_yTo + ((vDiff+1)/2);
            newYFrom                          = obj1->_yFrom - (((vDiff+1)/2)-1);
            newV                             += 1;
        }
        if ( wDiff % 2 == 0 )
        {
            newZTo                            = obj1->_zTo + (wDiff/2);
            newZFrom                          = obj1->_zFrom - (wDiff/2);
        }
        else
        {
            newZTo                            = obj1->_zTo + ((wDiff+1)/2);
            newZFrom                          = obj1->_zFrom - (((wDiff+1)/2)-1);
            newW                             += 1;
        }
        
        //================================ Interpolate map
        double *hlpMap                        = new double [(newU+1) * (newV+1) * (newW+1)];
        double newX                           = 0.0;
        double newY                           = 0.0;
        double newZ                           = 0.0;
        int oldX                              = 0;
        int oldY                              = 0;
        int oldZ                              = 0;
        bool xOver                            = false;
        bool yOver                            = false;
        bool zOver                            = false;
        bool xFound                           = false;
        bool yFound                           = false;
        bool zFound                           = false;
        double xd, yd, zd;
        std::array<double,4> p000, p001, p010, p011, p100, p101, p110, p111;
        std::array<double,3> p00, p01, p10, p11;
        std::array<double,2> p0, p1;
        for ( int uIt = 0; uIt < (newU+1); uIt++ )
        {
            for ( int vIt = 0; vIt < (newV+1); vIt++ )
            {
                for ( int wIt = 0; wIt < (newW+1); wIt++ )
                {
                    //==================== Init
                    arrPos                    = wIt + (newW+1) * ( vIt + (newV+1) * uIt );
                    
                    xFound                    = false;
                    yFound                    = false;
                    zFound                    = false;
                    
                    //==================== Get new X, Y and Z in angstroms
                    newX                      = static_cast<double> ( uIt ) * newXSampling;
                    newY                      = static_cast<double> ( vIt ) * newYSampling;
                    newZ                      = static_cast<double> ( wIt ) * newZSampling;
                    
                    //==================== Find corresponding old X, Y and Z indices
                    for ( int iter = 0; iter < static_cast<int> (obj1->_maxMapU+1); iter++ )
                    {
                        if ( ( newX >= ( static_cast<double> ( iter ) * obj1->_xSamplingRate ) ) && ( newX < ( static_cast<double> ( iter + 1 ) * obj1->_xSamplingRate ) ) )
                        {
                            oldX              = iter;
                            xFound            = true;
                            break;
                        }
                    }
                    xOver                     = false;
                    if ( ( oldX + 1 ) >= static_cast<int> (obj1->_maxMapU+1) ) { xOver = true; }
                    if ( !xFound ) { hlpMap[arrPos] = 0.0; continue; }
                    
                    for ( int iter = 0; iter < static_cast<int> (obj1->_maxMapV+1); iter++ )
                    {
                        if ( ( newY >= ( static_cast<double> ( iter ) * obj1->_ySamplingRate ) ) && ( newY < ( static_cast<double> ( iter + 1 ) * obj1->_ySamplingRate ) ) )
                        {
                            oldY              = iter;
                            yFound            = true;
                            break;
                        }
                    }
                    yOver                     = false;
                    if ( ( oldY + 1 ) >= static_cast<int> (obj1->_maxMapV+1) ) { yOver = true; }
                    if ( !yFound ) { hlpMap[arrPos] = 0.0; continue; }
                    
                    for ( int iter = 0; iter < static_cast<int> (obj1->_maxMapW+1); iter++ )
                    {
                        if ( ( newZ >= ( static_cast<double> ( iter ) * obj1->_zSamplingRate ) ) && ( newZ < ( static_cast<double> ( iter + 1 ) * obj1->_zSamplingRate ) ) )
                        {
                            oldZ              = iter;
                            zFound            = true;
                            break;
                        }
                    }
                    zOver                     = false;
                    if ( ( oldZ + 1 ) >= static_cast<int> (obj1->_maxMapW+1) ) { zOver = true; }
                    if ( !zFound ) { hlpMap[arrPos] = 0.0; continue; }
                    
                    //==================== Get the surrounding values and distances
                    p000[0]                   = oldX;
                    p000[1]                   = oldY;
                    p000[2]                   = oldZ;
                    hlpPos                    = p000[2] + (obj1->_maxMapW+1) * ( p000[1] + (obj1->_maxMapV+1) * p000[0] );
                    p000[3]                   = obj1->_densityMapCor[hlpPos];
                    
                    p001[0]                   = oldX;
                    p001[1]                   = oldY;
                    p001[2]                   = oldZ + 1;
                    if ( zOver ) { p001[2]    = 0.0; }
                    hlpPos                    = p001[2] + (obj1->_maxMapW+1) * ( p001[1] + (obj1->_maxMapV+1) * p001[0] );
                    p001[3]                   = obj1->_densityMapCor[hlpPos];
                    
                    p010[0]                   = oldX;
                    p010[1]                   = oldY + 1;
                    p010[2]                   = oldZ;
                    if ( yOver ) { p010[1]    = 0.0; }
                    hlpPos                    = p010[2] + (obj1->_maxMapW+1) * ( p010[1] + (obj1->_maxMapV+1) * p010[0] );
                    p010[3]                   = obj1->_densityMapCor[hlpPos];
                    
                    p011[0]                   = oldX;
                    p011[1]                   = oldY + 1;
                    p011[2]                   = oldZ + 1;
                    if ( yOver ) { p011[1]    = 0.0; }
                    if ( zOver ) { p011[2]    = 0.0; }
                    hlpPos                    = p011[2] + (obj1->_maxMapW+1) * ( p011[1] + (obj1->_maxMapV+1) * p011[0] );
                    p011[3]                   = obj1->_densityMapCor[hlpPos];
                    
                    p100[0]                   = oldX + 1;
                    p100[1]                   = oldY;
                    p100[2]                   = oldZ;
                    if ( xOver ) { p100[0]    = 0.0; }
                    hlpPos                    = p100[2] + (obj1->_maxMapW+1) * ( p100[1] + (obj1->_maxMapV+1) * p100[0] );
                    p100[3]                   = obj1->_densityMapCor[hlpPos];
                    
                    p101[0]                   = oldX + 1;
                    p101[1]                   = oldY;
                    p101[2]                   = oldZ + 1;
                    if ( xOver ) { p101[0]    = 0.0; }
                    if ( zOver ) { p101[2]    = 0.0; }
                    hlpPos                    = p101[2] + (obj1->_maxMapW+1) * ( p101[1] + (obj1->_maxMapV+1) * p101[0] );
                    p101[3]                   = obj1->_densityMapCor[hlpPos];
                    
                    p110[0]                   = oldX + 1;
                    p110[1]                   = oldY + 1;
                    p110[2]                   = oldZ;
                    if ( xOver ) { p110[0]    = 0.0; }
                    if ( yOver ) { p110[1]    = 0.0; }
                    hlpPos                    = p110[2] + (obj1->_maxMapW+1) * ( p110[1] + (obj1->_maxMapV+1) * p110[0] );
                    p110[3]                   = obj1->_densityMapCor[hlpPos];
                    
                    p111[0]                   = oldX + 1;
                    p111[1]                   = oldY + 1;
                    p111[2]                   = oldZ + 1;
                    if ( xOver ) { p111[0]    = 0.0; }
                    if ( yOver ) { p111[1]    = 0.0; }
                    if ( zOver ) { p111[2]    = 0.0; }
                    hlpPos                    = p111[2] + (obj1->_maxMapW+1) * ( p111[1] + (obj1->_maxMapV+1) * p111[0] );
                    p111[3]                   = obj1->_densityMapCor[hlpPos];
                    
                    //==================== Interpolate
                    xd                        = 1.0 - ( ( newX - ( static_cast<double> ( oldX ) * obj1->_xSamplingRate ) ) / obj1->_xSamplingRate );
                    p00[0]                    = p000[1]; p00[1] = p000[2]; p00[2] = ( xd * p000[3] ) + ( (1.0 - xd) * p100[3] );
                    p01[0]                    = p001[1]; p01[1] = p001[2]; p01[2] = ( xd * p001[3] ) + ( (1.0 - xd) * p101[3] );
                    p10[0]                    = p010[1]; p10[1] = p010[2]; p10[2] = ( xd * p010[3] ) + ( (1.0 - xd) * p110[3] );
                    p11[0]                    = p011[1]; p11[1] = p011[2]; p11[2] = ( xd * p011[3] ) + ( (1.0 - xd) * p111[3] );
                    
                    yd                        = 1.0 - ( ( newY - ( static_cast<double> ( oldY ) * obj1->_ySamplingRate ) ) / obj1->_ySamplingRate );
                    p0[0]                     = p00[1]; p0[1] = ( yd * p00[2] ) + ( (1.0 - yd) * p10[2] );
                    p1[0]                     = p01[1]; p1[1] = ( yd * p01[2] ) + ( (1.0 - yd) * p11[2] );
                    
                    zd                        = 1.0 - ( ( newZ - ( static_cast<double> ( oldZ ) * obj1->_zSamplingRate ) ) / obj1->_zSamplingRate );
                    hlpMap[arrPos]            = ( zd * p0[1] ) + ( (1.0 - zd) * p1[1] );
                }
            }
        }
        
        //================================ Copy map
        delete[] obj1->_densityMapCor;
        obj1->_densityMapCor                  = new double [(newU+1) * (newV+1) * (newW+1)];
        for ( int iter = 0; iter < static_cast<int> ( (newU+1) * (newV+1) * (newW+1) ); iter++ )
        {
            obj1->_densityMapCor[iter]        = hlpMap[iter];
        }
        
        //================================ Free memory
        delete[] hlpMap;
        
        //================================ Set new cell parameters
        int xMov1                             = std::round ( ( ( obj1->_xFrom * obj1->_xSamplingRate ) - ( newXFrom * newXSampling ) ) / newXSampling );
        int yMov1                             = std::round ( ( ( obj1->_yFrom * obj1->_ySamplingRate ) - ( newYFrom * newYSampling ) ) / newYSampling );
        int zMov1                             = std::round ( ( ( obj1->_zFrom * obj1->_zSamplingRate ) - ( newZFrom * newZSampling ) ) / newZSampling );
        
        obj1->_maxMapU                        = newU;
        obj1->_maxMapV                        = newV;
        obj1->_maxMapW                        = newW;
        
        obj1->_xFrom                          = newXFrom + xMov1;
        obj1->_yFrom                          = newYFrom + yMov1;
        obj1->_zFrom                          = newZFrom + zMov1;
        
        obj1->_xTo                            = newXTo + xMov1;
        obj1->_yTo                            = newYTo + yMov1;
        obj1->_zTo                            = newZTo + zMov1;
        
        obj1->_xRange                         = newXRange;
        obj1->_yRange                         = newYRange;
        obj1->_zRange                         = newZRange;
        
        obj1->_xSamplingRate                  = newXSampling;
        obj1->_ySamplingRate                  = newYSampling;
        obj1->_zSamplingRate                  = newZSampling;
    }
    
    //======================================== For structure 2, get the sampling right
    {
        //================================ Find cell parameters
        newU                                  = std::max ( obj1->_maxMapU, obj2->_maxMapU );
        newV                                  = std::max ( obj1->_maxMapV, obj2->_maxMapV );
        newW                                  = std::max ( obj1->_maxMapW, obj2->_maxMapW );
        
        if ( obj1->_maxMapU == static_cast<unsigned int> ( newU ) ) { newXRange = obj1->_xRange; } else { newXRange = obj2->_xRange; }
        if ( obj1->_maxMapV == static_cast<unsigned int> ( newV ) ) { newYRange = obj1->_yRange; } else { newYRange = obj2->_yRange; }
        if ( obj1->_maxMapW == static_cast<unsigned int> ( newW ) ) { newZRange = obj1->_zRange; } else { newZRange = obj2->_zRange; }
        
        int uDiff                             = newU - obj2->_maxMapU;
        int vDiff                             = newV - obj2->_maxMapV;
        int wDiff                             = newW - obj2->_maxMapW;
        
        int newXTo                            = 0;
        int newXFrom                          = 0;
        int newYTo                            = 0;
        int newYFrom                          = 0;
        int newZTo                            = 0;
        int newZFrom                          = 0;
        
        if ( uDiff % 2 == 0 )
        {
            newXTo                            = obj2->_xTo + (uDiff/2);
            newXFrom                          = obj2->_xFrom - (uDiff/2);
        }
        else
        {
            newXTo                            = obj2->_xTo + ((uDiff+1)/2);
            newXFrom                          = obj2->_xFrom - (((uDiff+1)/2)-1);
            newU                             += 1;
        }
        if ( vDiff % 2 == 0 )
        {
            newYTo                            = obj2->_yTo + (vDiff/2);
            newYFrom                          = obj2->_yFrom - (vDiff/2);
        }
        else
        {
            newYTo                            = obj2->_yTo + ((vDiff+1)/2);
            newYFrom                          = obj2->_yFrom - (((vDiff+1)/2)-1);
            newV                             += 1;
        }
        if ( wDiff % 2 == 0 )
        {
            newZTo                            = obj2->_zTo + (wDiff/2);
            newZFrom                          = obj2->_zFrom - (wDiff/2);
        }
        else
        {
            newZTo                            = obj2->_zTo + ((wDiff+1)/2);
            newZFrom                          = obj2->_zFrom - (((wDiff+1)/2)-1);
            newW                             += 1;
        }
        
        //================================ Interpolate map
        double *hlpMap                        = new double [(newU+1) * (newV+1) * (newW+1)];
        double newX                           = 0.0;
        double newY                           = 0.0;
        double newZ                           = 0.0;
        int oldX                              = 0;
        int oldY                              = 0;
        int oldZ                              = 0;
        bool xOver                            = false;
        bool yOver                            = false;
        bool zOver                            = false;
        bool xFound                           = false;
        bool yFound                           = false;
        bool zFound                           = false;
        double xd, yd, zd;
        std::array<double,4> p000, p001, p010, p011, p100, p101, p110, p111;
        std::array<double,3> p00, p01, p10, p11;
        std::array<double,2> p0, p1;
        
        for ( int uIt = 0; uIt < (newU+1); uIt++ )
        {
            for ( int vIt = 0; vIt < (newV+1); vIt++ )
            {
                for ( int wIt = 0; wIt < (newW+1); wIt++ )
                {
                    //==================== Init
                    arrPos                    = wIt + (newW+1) * ( vIt + (newV+1) * uIt );
                    
                    xFound                    = false;
                    yFound                    = false;
                    zFound                    = false;
                    
                    //==================== Get new X, Y and Z in angstroms
                    newX                      = static_cast<double> ( uIt ) * newXSampling;
                    newY                      = static_cast<double> ( vIt ) * newYSampling;
                    newZ                      = static_cast<double> ( wIt ) * newZSampling;
                    
                    //==================== Find corresponding old X, Y and Z indices
                    for ( int iter = 0; iter < static_cast<int> (obj2->_maxMapU+1); iter++ )
                    {
                        if ( ( newX >= ( static_cast<double> ( iter ) * obj2->_xSamplingRate ) ) && ( newX < ( static_cast<double> ( iter + 1 ) * obj2->_xSamplingRate ) ) )
                        {
                            oldX              = iter;
                            xFound            = true;
                            break;
                        }
                    }
                    xOver                     = false;
                    if ( ( oldX + 1 ) >= static_cast<int> (obj2->_maxMapU+1) ) { xOver = true; }
                    if ( !xFound ) { hlpMap[arrPos] = 0.0; continue; }
                    
                    for ( int iter = 0; iter < static_cast<int> (obj2->_maxMapV+1); iter++ )
                    {
                        if ( ( newY >= ( static_cast<double> ( iter ) * obj2->_ySamplingRate ) ) && ( newY < ( static_cast<double> ( iter + 1 ) * obj2->_ySamplingRate ) ) )
                        {
                            oldY              = iter;
                            yFound            = true;
                            break;
                        }
                    }
                    yOver                     = false;
                    if ( ( oldY + 1 ) >= static_cast<int> (obj2->_maxMapV+1) ) { yOver = true; }
                    if ( !yFound ) { hlpMap[arrPos] = 0.0; continue; }
                    
                    for ( int iter = 0; iter < static_cast<int> (obj2->_maxMapW+1); iter++ )
                    {
                        if ( ( newZ >= ( static_cast<double> ( iter ) * obj2->_zSamplingRate ) ) && ( newZ < ( static_cast<double> ( iter + 1 ) * obj2->_zSamplingRate ) ) )
                        {
                            oldZ              = iter;
                            zFound            = true;
                            break;
                        }
                    }
                    zOver                     = false;
                    if ( ( oldZ + 1 ) >= static_cast<int> (obj2->_maxMapW+1) ) { zOver = true; }
                    if ( !zFound ) { hlpMap[arrPos] = 0.0; continue; }
                    
                    //==================== Get the surrounding values and distances
                    p000[0]                   = oldX;
                    p000[1]                   = oldY;
                    p000[2]                   = oldZ;
                    hlpPos                    = p000[2] + (obj2->_maxMapW+1) * ( p000[1] + (obj2->_maxMapV+1) * p000[0] );
                    p000[3]                   = obj2->_densityMapCor[hlpPos];
                    
                    p001[0]                   = oldX;
                    p001[1]                   = oldY;
                    p001[2]                   = oldZ + 1;
                    if ( zOver ) { p001[2]    = 0.0; }
                    hlpPos                    = p001[2] + (obj2->_maxMapW+1) * ( p001[1] + (obj2->_maxMapV+1) * p001[0] );
                    p001[3]                   = obj2->_densityMapCor[hlpPos];
                    
                    p010[0]                   = oldX;
                    p010[1]                   = oldY + 1;
                    p010[2]                   = oldZ;
                    if ( yOver ) { p010[1]    = 0.0; }
                    hlpPos                    = p010[2] + (obj2->_maxMapW+1) * ( p010[1] + (obj2->_maxMapV+1) * p010[0] );
                    p010[3]                   = obj2->_densityMapCor[hlpPos];
                    
                    p011[0]                   = oldX;
                    p011[1]                   = oldY + 1;
                    p011[2]                   = oldZ + 1;
                    if ( yOver ) { p011[1]    = 0.0; }
                    if ( zOver ) { p011[2]    = 0.0; }
                    hlpPos                    = p011[2] + (obj2->_maxMapW+1) * ( p011[1] + (obj2->_maxMapV+1) * p011[0] );
                    p011[3]                   = obj2->_densityMapCor[hlpPos];
                    
                    p100[0]                   = oldX + 1;
                    p100[1]                   = oldY;
                    p100[2]                   = oldZ;
                    if ( xOver ) { p100[0]    = 0.0; }
                    hlpPos                    = p100[2] + (obj2->_maxMapW+1) * ( p100[1] + (obj2->_maxMapV+1) * p100[0] );
                    p100[3]                   = obj2->_densityMapCor[hlpPos];
                    
                    p101[0]                   = oldX + 1;
                    p101[1]                   = oldY;
                    p101[2]                   = oldZ + 1;
                    if ( xOver ) { p101[0]    = 0.0; }
                    if ( zOver ) { p101[2]    = 0.0; }
                    hlpPos                    = p101[2] + (obj2->_maxMapW+1) * ( p101[1] + (obj2->_maxMapV+1) * p101[0] );
                    p101[3]                   = obj2->_densityMapCor[hlpPos];
                    
                    p110[0]                   = oldX + 1;
                    p110[1]                   = oldY + 1;
                    p110[2]                   = oldZ;
                    if ( xOver ) { p110[0]    = 0.0; }
                    if ( yOver ) { p110[1]    = 0.0; }
                    hlpPos                    = p110[2] + (obj2->_maxMapW+1) * ( p110[1] + (obj2->_maxMapV+1) * p110[0] );
                    p110[3]                   = obj2->_densityMapCor[hlpPos];
                    
                    p111[0]                   = oldX + 1;
                    p111[1]                   = oldY + 1;
                    p111[2]                   = oldZ + 1;
                    if ( xOver ) { p111[0]    = 0.0; }
                    if ( yOver ) { p111[1]    = 0.0; }
                    if ( zOver ) { p111[2]    = 0.0; }
                    hlpPos                    = p111[2] + (obj2->_maxMapW+1) * ( p111[1] + (obj2->_maxMapV+1) * p111[0] );
                    p111[3]                   = obj2->_densityMapCor[hlpPos];
                    
                    //==================== Interpolate
                    xd                        = 1.0 - ( ( newX - ( static_cast<double> ( oldX ) * obj2->_xSamplingRate ) ) / obj2->_xSamplingRate );
                    p00[0]                    = p000[1]; p00[1] = p000[2]; p00[2] = ( xd * p000[3] ) + ( (1.0 - xd) * p100[3] );
                    p01[0]                    = p001[1]; p01[1] = p001[2]; p01[2] = ( xd * p001[3] ) + ( (1.0 - xd) * p101[3] );
                    p10[0]                    = p010[1]; p10[1] = p010[2]; p10[2] = ( xd * p010[3] ) + ( (1.0 - xd) * p110[3] );
                    p11[0]                    = p011[1]; p11[1] = p011[2]; p11[2] = ( xd * p011[3] ) + ( (1.0 - xd) * p111[3] );
                    
                    yd                        = 1.0 - ( ( newY - ( static_cast<double> ( oldY ) * obj2->_ySamplingRate ) ) / obj2->_ySamplingRate );
                    p0[0]                     = p00[1]; p0[1] = ( yd * p00[2] ) + ( (1.0 - yd) * p10[2] );
                    p1[0]                     = p01[1]; p1[1] = ( yd * p01[2] ) + ( (1.0 - yd) * p11[2] );
                    
                    zd                        = 1.0 - ( ( newZ - ( static_cast<double> ( oldZ ) * obj2->_zSamplingRate ) ) / obj2->_zSamplingRate );
                    hlpMap[arrPos]            = ( zd * p0[1] ) + ( (1.0 - zd) * p1[1] );
                }
            }
        }
        
        //================================ Copy map
        delete[] obj2->_densityMapCor;
        obj2->_densityMapCor                  = new double [(newU+1) * (newV+1) * (newW+1)];
        for ( int iter = 0; iter < static_cast<int> ( (newU+1) * (newV+1) * (newW+1) ); iter++ )
        {
            obj2->_densityMapCor[iter]        = hlpMap[iter];
        }
        
        //================================ Free memory
        delete[] hlpMap;
        
        //================================ Make sure start is the same as structure 1
        *ob2XMov                               = std::round ( ( ( obj1->_xFrom * obj1->_xSamplingRate ) - ( newXFrom * newXSampling ) ) / newXSampling );
        *ob2YMov                               = std::round ( ( ( obj1->_yFrom * obj1->_ySamplingRate ) - ( newYFrom * newYSampling ) ) / newYSampling );
        *ob2ZMov                               = std::round ( ( ( obj1->_zFrom * obj1->_zSamplingRate ) - ( newZFrom * newZSampling ) ) / newZSampling );
        
        //================================ Set new cell parameters
        obj2->_maxMapU                        = newU;
        obj2->_maxMapV                        = newV;
        obj2->_maxMapW                        = newW;
        
        obj2->_xFrom                          = newXFrom + (*ob2XMov);
        obj2->_yFrom                          = newYFrom + (*ob2YMov);
        obj2->_zFrom                          = newZFrom + (*ob2ZMov);
        
        obj2->_xTo                            = newXTo + (*ob2XMov);
        obj2->_yTo                            = newYTo + (*ob2YMov);
        obj2->_zTo                            = newZTo + (*ob2ZMov);
        
        obj2->_xRange                         = newXRange;
        obj2->_yRange                         = newYRange;
        obj2->_zRange                         = newZRange;
        
        obj2->_xSamplingRate                  = newXSampling;
        obj2->_ySamplingRate                  = newYSampling;
        obj2->_zSamplingRate                  = newZSampling;
        
        *ob2XMov                              *= obj2->_xSamplingRate;
        *ob2YMov                              *= obj2->_ySamplingRate;
        *ob2ZMov                              *= obj2->_zSamplingRate;
    }
    
    //======================================== Add zero padding to the smaller structure to make sure number of points is the same
    newU                                      = std::max ( obj1->_maxMapU, obj2->_maxMapU );
    newV                                      = std::max ( obj1->_maxMapV, obj2->_maxMapV );
    newW                                      = std::max ( obj1->_maxMapW, obj2->_maxMapW );
    
    if ( ( static_cast<int> ( obj1->_maxMapU ) != newU ) || ( static_cast<int> ( obj1->_maxMapV ) != newV ) || ( static_cast<int> ( obj1->_maxMapW ) != newW ) )
    {
        //==================================== Initialise
        double *hlpMap                        = new double [(newU+1) * (newV+1) * (newW+1)];
        
        //==================================== Zero padd
        for ( int uIt = 0; uIt < ( newU + 1 ); uIt++ )
        {
            for ( int vIt = 0; vIt < ( newV + 1 ); vIt++ )
            {
                for ( int wIt = 0; wIt < ( newW + 1 ); wIt++ )
                {
                    arrPos                    = wIt + (newW+1) * ( vIt + (newV+1) * uIt );
                    hlpPos                    = wIt + (obj1->_maxMapW+1) * ( vIt + (obj1->_maxMapV+1) * uIt );
                    
                    if ( ( uIt < ( static_cast<int> ( obj1->_maxMapU ) + 1 ) ) &&
                        ( vIt < ( static_cast<int> ( obj1->_maxMapV ) + 1 ) ) &&
                        ( wIt < ( static_cast<int> ( obj1->_maxMapW ) + 1 ) ) )
                    {
                        hlpMap[arrPos]        = obj1->_densityMapCor[hlpPos];
                    }
                    else
                    {
                        hlpMap[arrPos]        = 0.0;
                    }
                }
            }
        }
        
        //==================================== Copy map
        delete[] obj1->_densityMapCor;
        obj1->_densityMapCor                  = new double [(newU+1) * (newV+1) * (newW+1)];
        for ( int iter = 0; iter < static_cast<int> ( (newU+1) * (newV+1) * (newW+1) ); iter++ )
        {
            obj1->_densityMapCor[iter]        = hlpMap[iter];
        }
        
        //=================================== Free memory
        delete[] hlpMap;
        
        //==================================== Set new cell parameters
        obj1->_xTo                           += newU - obj1->_maxMapU;
        obj1->_yTo                           += newV - obj1->_maxMapV;
        obj1->_zTo                           += newW - obj1->_maxMapW;
        
        obj1->_maxMapU                        = newU;
        obj1->_maxMapV                        = newV;
        obj1->_maxMapW                        = newW;
        
        obj1->_xRange                         = ( newU + 1 ) * obj1->_xSamplingRate;
        obj1->_yRange                         = ( newV + 1 ) * obj1->_ySamplingRate;
        obj1->_zRange                         = ( newW + 1 ) * obj1->_zSamplingRate;
    }
    
    if ( ( static_cast<int> ( obj2->_maxMapU ) != newU ) || ( static_cast<int> ( obj2->_maxMapV ) != newV ) || ( static_cast<int> ( obj2->_maxMapW ) != newW ) )
    {
        //==================================== Initialise
        double *hlpMap                        = new double [(newU+1) * (newV+1) * (newW+1)];
        
        //==================================== Zero padd
        for ( int uIt = 0; uIt < ( newU + 1 ); uIt++ )
        {
            for ( int vIt = 0; vIt < ( newV + 1 ); vIt++ )
            {
                for ( int wIt = 0; wIt < ( newW + 1 ); wIt++ )
                {
                    arrPos                    = wIt + (newW+1) * ( vIt + (newV+1) * uIt );
                    hlpPos                    = wIt + (obj2->_maxMapW+1) * ( vIt + (obj2->_maxMapV+1) * uIt );
                    
                    if ( ( uIt < ( static_cast<int> ( obj2->_maxMapU ) + 1 ) ) &&
                        ( vIt < ( static_cast<int> ( obj2->_maxMapV ) + 1 ) ) &&
                        ( wIt < ( static_cast<int> ( obj2->_maxMapW ) + 1 ) ) )
                    {
                        hlpMap[arrPos]        = obj2->_densityMapCor[hlpPos];
                    }
                    else
                    {
                        hlpMap[arrPos]        = 0.0;
                    }
                }
            }
        }
        
        //==================================== Copy map
        delete[] obj2->_densityMapCor;
        obj2->_densityMapCor                  = new double [(newU+1) * (newV+1) * (newW+1)];
        for ( int iter = 0; iter < static_cast<int> ( (newU+1) * (newV+1) * (newW+1) ); iter++ )
        {
            obj2->_densityMapCor[iter]        = hlpMap[iter];
        }
        
        //=================================== Free memory
        delete[] hlpMap;
        
        //==================================== Set new cell parameters
        obj2->_xTo                           += newU - obj2->_maxMapU;
        obj2->_yTo                           += newV - obj2->_maxMapV;
        obj2->_zTo                           += newW - obj2->_maxMapW;
        
        obj2->_maxMapU                        = newU;
        obj2->_maxMapV                        = newV;
        obj2->_maxMapW                        = newW;
        
        obj2->_xRange                         = ( newU + 1 ) * obj2->_xSamplingRate;
        obj2->_yRange                         = ( newV + 1 ) * obj2->_ySamplingRate;
        obj2->_zRange                         = ( newW + 1 ) * obj2->_zSamplingRate;
    }
    
    //======================================== Initialise FFTWs for Translation function
    int minU                                  = std::min ( (obj1->_maxMapU+1), (obj2->_maxMapU+1) );
    int minV                                  = std::min ( (obj1->_maxMapV+1), (obj2->_maxMapV+1) );
    int minW                                  = std::min ( (obj1->_maxMapW+1), (obj2->_maxMapW+1) );
    
    fftw_complex *tmpIn1                      = new fftw_complex[(obj1->_maxMapU+1) * (obj1->_maxMapV+1) * (obj1->_maxMapW+1)];
    fftw_complex *tmpOut1                     = new fftw_complex[(obj1->_maxMapU+1) * (obj1->_maxMapV+1) * (obj1->_maxMapW+1)];
    fftw_complex *tmpIn2                      = new fftw_complex[(obj2->_maxMapU+1) * (obj2->_maxMapV+1) * (obj2->_maxMapW+1)];
    fftw_complex *tmpOut2                     = new fftw_complex[(obj2->_maxMapU+1) * (obj2->_maxMapV+1) * (obj2->_maxMapW+1)];
    fftw_complex *resIn                       = new fftw_complex[minU * minV * minW];
    fftw_complex *resOut                      = new fftw_complex[minU * minV * minW];
    
    //======================================== Get Fourier transforms of the maps
    fftw_plan forwardFourierObj1;
    fftw_plan forwardFourierObj2;
    fftw_plan inverseFourierCombo;
    
    forwardFourierObj1                        = fftw_plan_dft_3d ( (obj1->_maxMapU+1), (obj1->_maxMapV+1), (obj1->_maxMapW+1), tmpIn1, tmpOut1, FFTW_FORWARD,  FFTW_ESTIMATE );
    forwardFourierObj2                        = fftw_plan_dft_3d ( (obj2->_maxMapU+1), (obj2->_maxMapV+1), (obj2->_maxMapW+1), tmpIn2, tmpOut2, FFTW_FORWARD,  FFTW_ESTIMATE );
    inverseFourierCombo                       = fftw_plan_dft_3d ( minU, minV, minW, resOut, resIn, FFTW_BACKWARD, FFTW_ESTIMATE );
    
    //======================================== Fill in input data
    for ( int iter = 0; iter < static_cast<int> ( (obj1->_maxMapU+1) * (obj1->_maxMapV+1) * (obj1->_maxMapW+1) ); iter++ )
    {
        tmpIn1[iter][0]                       = obj1->_densityMapCor[iter];
        tmpIn1[iter][1]                       = 0.0;
    }
    
    for ( int iter = 0; iter < static_cast<int> ( (obj2->_maxMapU+1) * (obj2->_maxMapV+1) * (obj2->_maxMapW+1) ); iter++ )
    {
        tmpIn2[iter][0]                       = obj2->_densityMapCor[iter];
        tmpIn2[iter][1]                       = 0.0;
    }
    
    //======================================== Calculate Fourier
    fftw_execute                              ( forwardFourierObj1 );
    fftw_execute                              ( forwardFourierObj2 );
    
    //======================================== Combine Fourier coeffs
    double normFactor                         = static_cast<double> ( minU * minV * minW );
    std::array<double,2> hlpArr               = std::array<double,2> { { 2.0, 2.0 } };
    int h1, k1, l1;
    int uo2                                   = (obj1->_maxMapU+1) / 2;
    int vo2                                   = (obj1->_maxMapV+1) / 2;
    int wo2                                   = (obj1->_maxMapW+1) / 2;
    int muo2                                  = (minU+1) / 2;
    int mvo2                                  = (minV+1) / 2;
    int mwo2                                  = (minW+1) / 2;
    
    for ( int uIt = 0; uIt < static_cast<int> ( obj1->_maxMapU+1 ); uIt++ )
    {
        for ( int vIt = 0; vIt < static_cast<int> ( obj1->_maxMapV+1 ); vIt++ )
        {
            for ( int wIt = 0; wIt < static_cast<int> ( obj1->_maxMapW+1 ); wIt++ )
            {
                if ( uIt > uo2 ) { h1 = uIt - (obj1->_maxMapU+1); } else { h1 = uIt; }
                if ( vIt > vo2 ) { k1 = vIt - (obj1->_maxMapV+1); } else { k1 = vIt; }
                if ( wIt > wo2 ) { l1 = wIt - (obj1->_maxMapW+1); } else { l1 = wIt; }
                
                if ( ( std::abs ( h1 ) <= muo2 ) && ( std::abs ( k1 ) <= mvo2 ) && ( std::abs ( l1 ) <= mwo2 ) )
                {
                    if ( h1 < 0 ) { h1 += minU; }
                    if ( k1 < 0 ) { k1 += minV; }
                    if ( l1 < 0 ) { l1 += minW; }
                    
                    hlpPos                    = l1 + minW * ( k1 + minV * h1 );
                    arrPos                    = wIt + (obj1->_maxMapW+1) * ( vIt + (obj1->_maxMapV+1) * uIt );
                    resIn[hlpPos][0]          = tmpOut1[arrPos][0];
                    resIn[hlpPos][1]          = tmpOut1[arrPos][1];
                }
            }
        }
    }
    
    uo2                                       = (obj2->_maxMapU+1) / 2;
    vo2                                       = (obj2->_maxMapV+1) / 2;
    wo2                                       = (obj2->_maxMapW+1) / 2;
    for ( int uIt = 0; uIt < static_cast<int> ( obj2->_maxMapU+1 ); uIt++ )
    {
        for ( int vIt = 0; vIt < static_cast<int> ( obj2->_maxMapV+1 ); vIt++ )
        {
            for ( int wIt = 0; wIt < static_cast<int> ( obj2->_maxMapW+1 ); wIt++ )
            {
                if ( uIt > uo2 ) { h1 = uIt - (obj2->_maxMapU+1); } else { h1 = uIt; }
                if ( vIt > vo2 ) { k1 = vIt - (obj2->_maxMapV+1); } else { k1 = vIt; }
                if ( wIt > wo2 ) { l1 = wIt - (obj2->_maxMapW+1); } else { l1 = wIt; }
                
                if ( ( std::abs ( h1 ) <= muo2 ) && ( std::abs ( k1 ) <= mvo2 ) && ( std::abs ( l1 ) <= mwo2 ) )
                {
                    if ( h1 < 0 ) { h1 += minU; }
                    if ( k1 < 0 ) { k1 += minV; }
                    if ( l1 < 0 ) { l1 += minW; }
                    
                    hlpPos                    = l1 + minW * ( k1 + minV * h1 );
                    arrPos                    = wIt + (obj2->_maxMapW+1) * ( vIt + (obj2->_maxMapV+1) * uIt );
                    hlpArr                    = ProSHADE_internal_misc::complexMultiplicationConjug ( &resIn[hlpPos][0],
                                                                                                     &resIn[hlpPos][1],
                                                                                                     &tmpOut2[arrPos][0],
                                                                                                     &tmpOut2[arrPos][1] );
                    resOut[hlpPos][0]         = hlpArr[0] / normFactor;
                    resOut[hlpPos][1]         = hlpArr[1] / normFactor;
                    
                }
            }
        }
    }
    
    fftw_execute                              ( inverseFourierCombo );
    
    //======================================== Find highest peak in translation function
    double mapPeak                            = 0.0;
    for ( int uIt = 0; uIt < minU; uIt++ )
    {
        for ( int vIt = 0; vIt < minV; vIt++ )
        {
            for ( int wIt = 0; wIt < minW; wIt++ )
            {
                arrPos                        = wIt + minW * ( vIt + minV * uIt );
                if ( resIn[arrPos][0] > mapPeak )
                {
                    mapPeak                   = resIn[arrPos][0];
                    ret[0]                    = uIt;
                    ret[1]                    = vIt;
                    ret[2]                    = wIt;
                }
            }
        }
    }
    ret[3]                                    = mapPeak / ( ( obj2->_maxMapU+1 ) * ( obj2->_maxMapV+1 ) * ( obj2->_maxMapW+1 ) );
    
    //======================================== Dont translate over half
    if ( ret[0] > muo2 ) { ret[0] = ret[0] - minU; }
    if ( ret[1] > mvo2 ) { ret[1] = ret[1] - minV; }
    if ( ret[2] > mwo2 ) { ret[2] = ret[2] - minW; }
    
    //======================================== Free memory
    fftw_destroy_plan                         ( forwardFourierObj1 );
    fftw_destroy_plan                         ( forwardFourierObj2 );
    fftw_destroy_plan                         ( inverseFourierCombo );
    delete[] tmpIn1;
    delete[] tmpIn2;
    delete[] tmpOut1;
    delete[] tmpOut2;
    delete[] resIn;
    delete[] resOut;
    
    //======================================== Convert to angstroms
    ret[0]                                   *= obj2->_xSamplingRate;
    ret[1]                                   *= obj2->_ySamplingRate;
    ret[2]                                   *= obj2->_zSamplingRate;
    
    //======================================== Do not translate over half the cell size, do minus instead
    if ( ret[0] > ( obj2->_xRange / 2.0 ) ) { ret[0] -= obj2->_xRange; }
    if ( ret[1] > ( obj2->_yRange / 2.0 ) ) { ret[1] -= obj2->_yRange; }
    if ( ret[2] > ( obj2->_zRange / 2.0 ) ) { ret[2] -= obj2->_zRange; }
    
    if ( ret[0] < ( -obj2->_xRange / 2.0 ) ) { ret[0] += obj2->_xRange; }
    if ( ret[1] < ( -obj2->_yRange / 2.0 ) ) { ret[1] += obj2->_yRange; }
    if ( ret[2] < ( -obj2->_zRange / 2.0 ) ) { ret[2] += obj2->_zRange; }
    
    //======================================== Done
    return ( ret );
}

/*! \brief Rotates the density map be given Angle-Axis rotation using the Wigner matrices and inverse spharical harmonics transform.
 
 This function takes the density map of a structure, computes the Wigner D matrices using the Euler angles already set for the structure (this needs to be done beforehand) and then 
 proceeds to multiply the spherical coordiantes by the Wigner D matrices, thus computing the rotation in the spherical harmonics (Fourier) space. Consequently, it inverts the rotated
 spherical harmonics coefficients and coverts the resulting sphere mapped values back to Cartesian grid, which is finally outputted as a map. The result is a density map rotated by the
 required Euler angles without any interpolation in the real space, but with interpolation to and from spheres.
 
 \param[in] cmpObj1 Pointer to the ProSHADE_data object which has the density map and angles set.
 \param[in] settings The ProSHADE_settings container class instance with information about how to write the HTML report.
 \param[in] saveName String of the name to where the rotated structure should ba outputted to.
 \param[in] verbose Int specifying how loud the function should be when reporting progress.
 \param[in] axOrd String of three letters, specifying the order of axes, allowed characters are 'x', 'y' and 'z' in any order.
 \param[in] internalUse If this is for internal use, save some time on 0, 0, 0 angles.
 \param[in] noMap Boolean value specifying whether a map is at all needed and possible to obtain.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_comparePairwise::rotateStructure ( ProSHADE_data *cmpObj1,
                                                                    ProSHADE::ProSHADE_settings* settings,
                                                                    std::string saveName,
                                                                    int verbose,
                                                                    std::string axOrd,
                                                                    bool internalUse )
{
    //======================================== Sanity checks
    if ( ( this->_eulerAngles[0] == 0.0 ) && ( this->_eulerAngles[1] == 0.0 ) && ( this->_eulerAngles[2] == 0.0 ) && ( !internalUse ) )
    {
        cmpObj1->writeMap                         ( saveName, cmpObj1->_densityMapCor, axOrd );
        return ;
    }
    
    if ( ( this->_eulerAngles[0] == 0.0 ) && ( this->_eulerAngles[1] == 0.0 ) && ( this->_eulerAngles[2] == 0.0 ) && ( internalUse ) )
    {
        return ;
    }
    
    //======================================== Initalise
    std::array<double,2> hlpArr;
    int arrPos;
    int arrPos2;
    double **realSHCoeffsRes                  = new double* [cmpObj1->_noShellsWithData];
    double **imagSHCoeffsRes                  = new double* [cmpObj1->_noShellsWithData];
    for ( unsigned int i = 0; i < cmpObj1->_noShellsWithData; i++ )
    {
        realSHCoeffsRes[i]                    = new double [cmpObj1->_oneDimmension * cmpObj1->_oneDimmension];
        imagSHCoeffsRes[i]                    = new double [cmpObj1->_oneDimmension * cmpObj1->_oneDimmension];
        
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( cmpObj1->_oneDimmension * cmpObj1->_oneDimmension ); iter++ )
        {
            realSHCoeffsRes[i][iter]          = 0.0;
            imagSHCoeffsRes[i][iter]          = 0.0;
        }
    }
    
    //======================================== Get Wigner matrices
    this->generateWignerMatrices              ( settings );
    
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> Wigner matrices for given Euler angles obtained." << std::endl;
    }
    
    //======================================== Multiply coeffs by Wigner
    // ... for each shell
    for ( int shell = 0; shell < static_cast<int> ( cmpObj1->_noShellsWithData ); shell++ )
    {
        //==================================== ... for each band
        for ( int bandIter = 0; bandIter < static_cast<int> ( this->_bandwidthLimit ); bandIter++ )
        {
            //================================ ... for each order
            for ( int ord1 = 0; ord1 < (bandIter * 2 + 1); ord1++ )
            {
                arrPos2                       = seanindex (  ord1-bandIter, bandIter, this->_bandwidthLimit );
                
                //============================ ... again, for each order prime
                for ( int ord2 = 0; ord2 < (bandIter * 2 + 1); ord2++ )
                {
                    arrPos                    = seanindex (  ord2-bandIter, bandIter, this->_bandwidthLimit );
                    hlpArr                    = ProSHADE_internal_misc::complexMultiplication ( &cmpObj1->_realSHCoeffs[shell][arrPos],
                                                                                                &cmpObj1->_imagSHCoeffs[shell][arrPos],
                                                                                                &this->_wignerMatrices.at(bandIter).at(ord1).at(ord2)[0],
                                                                                                &this->_wignerMatrices.at(bandIter).at(ord1).at(ord2)[1] );
                    
                    realSHCoeffsRes[shell][arrPos2] += hlpArr[0];
                    imagSHCoeffsRes[shell][arrPos2] += hlpArr[1];
                }
            }
        }
    }
    if ( verbose > 0 )
    {
        std::cout << "Rotated all coefficients." << std::endl;
    }
    
    //======================================== Inverse the SH coeffs to shells
    // ... Initalise memory
    fftw_plan idctPlan, ifftPlan;
    int rank, howmany_rank ;
    fftw_iodim dims[1], howmany_dims[1];
    
    double *sigR                              = new double [cmpObj1->_oneDimmension * cmpObj1->_oneDimmension];
    double *sigI                              = new double [cmpObj1->_oneDimmension * cmpObj1->_oneDimmension];
    double *rcoeffs                           = new double [cmpObj1->_oneDimmension * cmpObj1->_oneDimmension];
    double *icoeffs                           = new double [cmpObj1->_oneDimmension * cmpObj1->_oneDimmension];
    double *weights                           = new double [4 * this->_bandwidthLimit];
    double *workspace                         = new double [( 10 * this->_bandwidthLimit * this->_bandwidthLimit ) + ( 24 * this->_bandwidthLimit )];
    
    //======================================== Initialise internal values ( and allocate memory )
    double **newShellMappedData               = new double*[cmpObj1->_noShellsWithData];
    for ( unsigned int sh = 0; sh < cmpObj1->_noShellsWithData; sh++ )
    {
        newShellMappedData[sh]                = new double[static_cast<unsigned int> ( 4 * this->_bandwidthLimit * this->_bandwidthLimit )];
    }
    if ( verbose > 2 )
    {
        std::cout << ">>>>> Memory allocation complete." << std::endl;
    }
    
    //======================================== Initalise FFTW plans
    idctPlan = fftw_plan_r2r_1d               ( 2 * this->_bandwidthLimit,
                                                weights,
                                                workspace,
                                                FFTW_REDFT01,
                                                FFTW_ESTIMATE );
    
    rank                                      = 1;
    dims[0].n                                 = 2 * this->_bandwidthLimit;
    dims[0].is                                = 2 * this->_bandwidthLimit;
    dims[0].os                                = 1;
    howmany_rank                              = 1;
    howmany_dims[0].n                         = 2 * this->_bandwidthLimit;
    howmany_dims[0].is                        = 1;
    howmany_dims[0].os                        = 2 * this->_bandwidthLimit;
    
    //======================================== Inverse FFT
    ifftPlan                                  = fftw_plan_guru_split_dft ( rank,
                                                                           dims,
                                                                           howmany_rank,
                                                                           howmany_dims,
                                                                           sigR,
                                                                           sigI,
                                                                           rcoeffs,
                                                                           icoeffs,
                                                                           FFTW_ESTIMATE );
    
    //======================================== ... for each shell
    for ( int shell = 0; shell < static_cast<int> ( cmpObj1->_noShellsWithData ); shell++ )
    {
        //==================================== Load SH coeffs to arrays
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( cmpObj1->_oneDimmension * cmpObj1->_oneDimmension ); iter++ )
        {
            rcoeffs[iter]                     = realSHCoeffsRes[shell][iter];
            icoeffs[iter]                     = imagSHCoeffsRes[shell][iter];
        }
        
        //==================================== Get inverse spherical harmonics transform for the shell
        InvFST_semi_fly                       ( rcoeffs,
                                                icoeffs,
                                                sigR,
                                                sigI,
                                                this->_bandwidthLimit,
                                                workspace,
                                                0,
                                                this->_bandwidthLimit,
                                                &idctPlan,
                                                &ifftPlan );
        
        //==================================== Copy the results to the rotated shells array
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( cmpObj1->_oneDimmension * cmpObj1->_oneDimmension ); iter++ )
        {
            newShellMappedData[shell][iter]   = sigR[iter];
        }
    }
    if ( verbose > 1 )
    {
        std::cout << ">> Inverted all rotated spherical harmonics coefficients." << std::endl;
    }
    
    //======================================== Clear memory
    delete[] sigR;
    delete[] sigI;
    delete[] rcoeffs;
    delete[] icoeffs;
    delete[] weights;
    delete[] workspace;
    
    //======================================== Clear memory
    for ( unsigned int i = 0; i < cmpObj1->_noShellsWithData; i++ )
    {
        delete[] realSHCoeffsRes[i];
        delete[] imagSHCoeffsRes[i];
    }
    delete[] realSHCoeffsRes;
    delete[] imagSHCoeffsRes;
    
    //======================================== Convert the shell data back to cartesian grid
    // ... Create the Cartesian grid
    double *densityMapRotated                 = new double [(cmpObj1->_maxMapU+1)*(cmpObj1->_maxMapV+1)*(cmpObj1->_maxMapW+1)];
    
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ((cmpObj1->_maxMapU+1)*(cmpObj1->_maxMapV+1)*(cmpObj1->_maxMapW+1)); iter++ )
    {
        densityMapRotated[iter]               = 0.0;
    }
    
    //======================================== Find spherical cut-offs
    std::vector<double> lonCO                 ( cmpObj1->_thetaAngle + 1 );
    for ( unsigned int iter = 0; iter <= cmpObj1->_thetaAngle; iter++ ) { lonCO.at(iter) = static_cast<double> ( iter ) * ( ( static_cast<double> ( M_PI ) * 2.0 ) / static_cast<double> ( this->_thetaAngle ) ) - ( static_cast<double> ( M_PI ) ); }
    std::vector<double> latCO                 ( cmpObj1->_phiAngle + 1 );
    for ( unsigned int iter = 0; iter <= cmpObj1->_phiAngle; iter++ ) { latCO.at(iter) = ( static_cast<double> ( iter ) * ( static_cast<double> ( M_PI ) / static_cast<double> ( this->_phiAngle ) ) - ( static_cast<double> ( M_PI ) / 2.0 ) ); }
    
    //======================================== Interpolate onto cartesian grid
    double rad                                = 0.0;
    double lon                                = 0.0;
    double lat                                = 0.0;
    double newU                               = 0.0;
    double newV                               = 0.0;
    double newW                               = 0.0;
    unsigned int lowerLon                     = 0;
    unsigned int upperLon                     = 0;
    unsigned int lowerLat                     = 0;
    unsigned int upperLat                     = 0;
    unsigned int lowerShell                   = 0;
    unsigned int upperShell                   = 0;
    double x00                                = 0.0;
    double x01                                = 0.0;
    double x10                                = 0.0;
    double x11                                = 0.0;
    double distLLon                           = 0.0;
    double distLLat                           = 0.0;
    double distLRad                           = 0.0;
    double valLLon                            = 0.0;
    double valULon                            = 0.0;
    double lowerShellValue                    = 0.0;
    double upperShellValue                    = 0.0;
    
    for ( int uIt = 0; uIt < static_cast<int> (cmpObj1->_maxMapU+1); uIt++ )
    {
        for ( int vIt = 0; vIt < static_cast<int> (cmpObj1->_maxMapV+1); vIt++ )
        {
            for ( int wIt = 0; wIt < static_cast<int> (cmpObj1->_maxMapW+1); wIt++ )
            {
                
                //============================ Convert to centered coords
                newU                          = static_cast<double> ( uIt - ( static_cast<int> (cmpObj1->_maxMapU+1) / 2 ) );
                newV                          = static_cast<double> ( vIt - ( static_cast<int> (cmpObj1->_maxMapV+1) / 2 ) );
                newW                          = static_cast<double> ( wIt - ( static_cast<int> (cmpObj1->_maxMapW+1) / 2 ) );
                
                //============================ Deal with 0 ; 0 ; 0
                if ( ( newU == 0.0 ) && ( newV == 0.0 ) && ( newW == 0.0 ) )
                {
                    arrPos                    = wIt + (cmpObj1->_maxMapW + 1) * ( vIt + (cmpObj1->_maxMapV + 1) * uIt );
                    densityMapRotated[arrPos] = cmpObj1->_densityMapCor[arrPos];
                    continue;
                }
                
                //============================ Convert to spherical coords
                rad                           = sqrt  ( pow( (newU*cmpObj1->_xSamplingRate), 2.0 ) +
                                                        pow( (newV*cmpObj1->_ySamplingRate), 2.0 ) +
                                                        pow( (newW*cmpObj1->_zSamplingRate), 2.0 ) );
                lon                           = atan2 ( (newV*cmpObj1->_ySamplingRate) , (newU*cmpObj1->_xSamplingRate) );
                lat                           = asin  ( (newW*cmpObj1->_zSamplingRate) / rad );
                
                //============================ Deal with nan's
                if ( rad   != rad )   { rad   = 0.0; }
                if ( lon   != lon )   { lon   = 0.0; }
                if ( lat   != lat )   { lat   = 0.0; }
                
                //============================ Find the angle cutoffs around the point
                for ( unsigned int iter = 0; iter < cmpObj1->_thetaAngle; iter++ )
                {
                    if ( ( std::trunc(10000. * lonCO.at(iter)) <= std::trunc(10000. * lon) ) && ( std::trunc(10000. * lonCO.at(iter+1)) > std::trunc(10000. * lon) ) )
                    {
                        lowerLon              = iter;
                        upperLon              = iter+1;
                        break;
                    }
                }
                if ( upperLon == cmpObj1->_thetaAngle ) { upperLon = 0; }
                
                for ( unsigned int iter = 0; iter < cmpObj1->_phiAngle; iter++ )
                {
                    if ( ( std::trunc(10000. * latCO.at(iter)) <=  std::trunc(10000. * lat) ) && ( std::trunc(10000. * latCO.at(iter+1)) > std::trunc(10000. * lat) ) )
                    {
                        lowerLat              = iter;
                        upperLat              = iter+1;
                        break;
                    }
                }
                if ( upperLat == cmpObj1->_phiAngle ) { upperLat = 0; }
                                
                //============================ Find shells above and below
                lowerShell                    = 0;
                upperShell                    = 0;
                for ( unsigned int iter = 0; iter < (cmpObj1->_noShellsWithData-1); iter++ )
                {
                    if ( ( cmpObj1->_shellPlacement.at(iter) <= rad ) && ( cmpObj1->_shellPlacement.at(iter+1) > rad ) )
                    {
                        lowerShell            = iter;
                        upperShell            = iter+1;
                        break;
                    }
                }
                if ( upperShell == 0 )
                {
                    arrPos                    = wIt + (cmpObj1->_maxMapW + 1) * ( vIt + (cmpObj1->_maxMapV + 1) * uIt );
                    densityMapRotated[arrPos] = 0.0; 
                    continue;
                }
                
                //============================ Interpolate lower shell
                x00                           = newShellMappedData[lowerShell][static_cast<int> ( lowerLat * cmpObj1->_thetaAngle + lowerLon )];
                x01                           = newShellMappedData[lowerShell][static_cast<int> ( lowerLat * cmpObj1->_thetaAngle + upperLon )];
                x10                           = newShellMappedData[lowerShell][static_cast<int> ( upperLat * cmpObj1->_thetaAngle + lowerLon )];
                x11                           = newShellMappedData[lowerShell][static_cast<int> ( upperLat * cmpObj1->_thetaAngle + upperLon )];
                
                distLLon                      = std::abs ( lon - lonCO.at(lowerLon) ) / ( std::abs( lon - lonCO.at(lowerLon) ) + std::abs( lon - lonCO.at(upperLon) ) );
                valLLon                       = ( ( 1.0 - distLLon ) * x00 ) + ( distLLon * x01 );
                valULon                       = ( ( 1.0 - distLLon ) * x10 ) + ( distLLon * x11 );
                
                distLLat                      = std::abs ( lat - latCO.at(lowerLat) ) / ( std::abs( lat - latCO.at(lowerLat) ) + std::abs( lat - latCO.at(upperLat) ) );
                lowerShellValue               = ( ( 1.0 - distLLat ) * valLLon ) + ( distLLat * valULon );
                
                //============================ Interpolate upper shell
                x00                           = newShellMappedData[upperShell][static_cast<int> ( lowerLat * cmpObj1->_thetaAngle + lowerLon )];
                x01                           = newShellMappedData[upperShell][static_cast<int> ( lowerLat * cmpObj1->_thetaAngle + upperLon )];
                x10                           = newShellMappedData[upperShell][static_cast<int> ( upperLat * cmpObj1->_thetaAngle + lowerLon )];
                x11                           = newShellMappedData[upperShell][static_cast<int> ( upperLat * cmpObj1->_thetaAngle + upperLon )];
                
                valLLon                       = ( ( 1.0 - distLLon ) * x00 ) + ( distLLon * x01 );
                valULon                       = ( ( 1.0 - distLLon ) * x10 ) + ( distLLon * x11 );
                
                upperShellValue               = ( ( 1.0 - distLLat ) * valLLon ) + ( distLLat * valULon );
                
                //============================ Interpolate between shells
                distLRad                      = std::abs ( rad - cmpObj1->_shellPlacement.at(lowerShell) ) / ( std::abs( rad - cmpObj1->_shellPlacement.at(lowerShell) ) + std::abs( rad - cmpObj1->_shellPlacement.at(upperShell) ) );

                arrPos                        = wIt + (cmpObj1->_maxMapW + 1) * ( vIt + (cmpObj1->_maxMapV + 1) * uIt );
                densityMapRotated[arrPos]     = ( ( 1.0 - distLRad ) * lowerShellValue ) + ( distLRad * upperShellValue );
                
                continue;
            }
        }
    }    
    
    //======================================== Normalise
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (cmpObj1->_maxMapU+1)*(cmpObj1->_maxMapV+1)*(cmpObj1->_maxMapW+1) ); iter++ )
    {
        if ( densityMapRotated[iter] == densityMapRotated[iter] ) { ; }
        else                                                      { densityMapRotated[iter] = 0.0; }
    }
    
    //======================================== Remove all outside the max shell
    double dist                               = std::max ( (cmpObj1->_maxMapU+1)/2.0, std::max ( (cmpObj1->_maxMapV+1)/2.0, (cmpObj1->_maxMapW+1)/2.0 ) );
    
    for ( int uIt = 0; uIt < static_cast<int> (cmpObj1->_maxMapU+1); uIt++ )
    {
        for ( int vIt = 0; vIt < static_cast<int> (cmpObj1->_maxMapV+1); vIt++ )
        {
            for ( int wIt = 0; wIt < static_cast<int> (cmpObj1->_maxMapW+1); wIt++ )
            {
                arrPos                        = wIt + (cmpObj1->_maxMapW + 1) * ( vIt + (cmpObj1->_maxMapV + 1) * uIt );
                
                if ( sqrt ( pow( uIt-((cmpObj1->_maxMapU+1)/2.0), 2.0 ) +
                            pow( vIt-((cmpObj1->_maxMapV+1)/2.0), 2.0 ) +
                            pow( wIt-((cmpObj1->_maxMapW+1)/2.0), 2.0 ) ) > dist )
                {
                    densityMapRotated[arrPos] = 0.0;
                    cmpObj1->_densityMapCor[arrPos] = 0.0;
                }
            }
        }
    }
    
    //======================================== Clear memory
    for ( unsigned int sh = 0; sh < cmpObj1->_noShellsWithData; sh++ )
    {
        delete[] newShellMappedData[sh];
    }
    delete[] newShellMappedData;
    
    //======================================== Write out the rotated map and clean up
    if ( !internalUse )
    {
        cmpObj1->writeMap                         ( saveName, densityMapRotated, axOrd );
        
        delete[] cmpObj1->_densityMapCor;
        cmpObj1->_densityMapCor                   = nullptr;
    }
    else
    {
        //==================================== ... or save the rotated map internally
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( (cmpObj1->_maxMapU+1) * (cmpObj1->_maxMapV+1) * (cmpObj1->_maxMapW+1) ); iter++ )
        {
            cmpObj1->_densityMapCor[iter]     = densityMapRotated[iter];
        }
    }
    
    //======================================== Clear memory
    delete[] densityMapRotated;
    
    //======================================== Done
    return ;
    
}

/*! \brief Takes the internal objects with and without phases and aligns them to all the other objects, which should be read with reverse phased database.
 
 This function is called if a structure is compared to a database with reverse phased (i.e. with both phased and phaseless data) structures. It will use both of
 the data types to find the optimal translation and rotation and then it will apply these to the structures, releasing the old intertwined vector and returning a
 new vector of aligned structures. This is a very particular functions, use only for the purpose specified.
 
 \param[in] settings A settings object having all the required values.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_compareOneAgainstAll::alignDensities ( ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Sanity checks
    if ( ( this->all->size() < 1 ) || ( ( this->all->size() % 2 ) != 0 ) )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file comparison !!! The compare against part has incorrect number of entries." << std::endl;
        exit ( -1 );
    }
    
    int checkValue                            = static_cast<double> ( this->one->_keepOrRemove );
    for ( unsigned int strIt = 0; strIt < static_cast<unsigned int> ( this->all->size() ); strIt += 2 )
    {
        if ( checkValue != static_cast<double> ( this->all->at(strIt)->_keepOrRemove ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Error in file comparison between " << this->one->_inputFileName << " AND " << this->all->at(strIt)->_inputFileName << " !!! The phase treatment is different, use the same phase treatment (i.e. remove all, or keep all) to make the strucutres comparable." << std::endl;
            exit ( -1 );
        }
    }
    
    checkValue                                = static_cast<double> ( this->two->_keepOrRemove );
    for ( unsigned int strIt = 1; strIt < static_cast<unsigned int> ( this->all->size() ); strIt += 2 )
    {
        if ( checkValue != static_cast<double> ( this->all->at(strIt)->_keepOrRemove ) )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Error in file comparison between " << this->one->_inputFileName << " AND " << this->all->at(strIt)->_inputFileName << " !!! The phase treatment is different, use the same phase treatment (i.e. remove all, or keep all) to make the strucutres comparable." << std::endl;
            exit ( -1 );
        }
    }
    
    //======================================== Initialise
    std::vector<ProSHADE_internal::ProSHADE_data*> newAll;
    std::vector<double> pattCorrelationVec;
    std::vector<std::array<double,4>> translationVec;
    int totStrIt                              = 0;
    bool reverseOrder                         = false;

    //======================================== Find phaseless translation
    for ( unsigned int strIt = 0; strIt < static_cast<unsigned int> ( this->all->size() ); strIt += 2 )
    {
        if ( this->one->_keepOrRemove == false )
        {
            //================================ Pick the right range of objects (phase-wise)
            if ( ( strIt == 0 ) && ( this->all->at(strIt)->_keepOrRemove != false ) )
            {
                strIt                        += 1;
                reverseOrder                  = true;
            }
            
            //================================ Create Patterson comparison object
            ProSHADE_comparePairwise* cmpObj  = new ProSHADE_comparePairwise ( this->one,
                                                                               this->all->at(strIt),
                                                                               settings->mPower,
                                                                               settings->ignoreLs,
                                                                               settings->glIntegOrder,
                                                                               settings );
            
            //================================ Get the angles and correlation
            pattCorrelationVec.emplace_back   ( 0.0 );
            cmpObj->precomputeTrSigmaDescriptor ( );
            cmpObj->getSO3InverseMap          ( settings );
            double xMov1                      = 0.0;
            double yMov1                      = 0.0;
            double zMov1                      = 0.0;
            std::array<double,3> euAngs       = cmpObj->getEulerAngles ( settings, &pattCorrelationVec.at(totStrIt) );
            
            //================================ Clear the memory
            delete cmpObj;
            
            //================================ Create the object for rotation
            if ( reverseOrder )
            {
                cmpObj                        = new ProSHADE_comparePairwise ( this->all->at(strIt-1),
                                                                               this->all->at(strIt-1),
                                                                               settings->mPower,
                                                                               settings->ignoreLs,
                                                                               settings->glIntegOrder,
                                                                               settings );
            }
            else
            {
                cmpObj                        = new ProSHADE_comparePairwise ( this->all->at(strIt+1),
                                                                               this->all->at(strIt+1),
                                                                               settings->mPower,
                                                                               settings->ignoreLs,
                                                                               settings->glIntegOrder,
                                                                               settings );
            }
            
            //================================ Find 'opposite' Euler angles and set them
            // Get mat from Euler
            double mat22                      =  cos ( euAngs[1]  );
            double mat02                      = -sin ( euAngs[1]  ) * cos ( euAngs[2] );
            double mat12                      =  sin ( euAngs[1]  ) * sin ( euAngs[2] );
            double mat20                      =  cos ( euAngs[0] ) * sin ( euAngs[1]  );
            double mat21                      =  sin ( euAngs[0] ) * sin ( euAngs[1]  );
            
            // Transpose
            double rMat02                     = mat20;
            double rMat20                     = mat02;
            double rMat21                     = mat12;
            double rMat12                     = mat21;
            
            // Get Euler from map
            euAngs[0]                         = atan2 ( rMat21,  rMat20 );
            euAngs[1]                         = acos  ( mat22 );
            euAngs[2]                         = atan2 ( rMat12, -rMat02 );
            
            if ( euAngs[0] < 0.0 ) { euAngs[0]= 2.0 * M_PI + euAngs[0]; }
            if ( euAngs[1] < 0.0 ) { euAngs[1]=       M_PI + euAngs[1]; }
            if ( euAngs[2] < 0.0 ) { euAngs[2]= 2.0 * M_PI + euAngs[2]; }
            
            cmpObj->setEulerAngles            ( euAngs[0], euAngs[1], euAngs[2] );
            
            //================================ Rotate by the Euler
            if ( reverseOrder )
            {
                cmpObj->rotateStructure       ( this->all->at(strIt-1),
                                                settings,
                                                settings->clearMapFile,
                                                settings->verbose,
                                                settings->axisOrder,
                                                true );
                
                ProSHADE_data str1Copy        = ProSHADE_data ( this->one );
                translationVec.emplace_back   ( cmpObj->getTranslationFunctionMap ( &str1Copy, this->all->at(strIt-1), &xMov1, &yMov1, &zMov1 ) );
                
                this->all->at(strIt-1)->translateMap ( -translationVec.at(totStrIt)[0],
                                                       -translationVec.at(totStrIt)[1],
                                                       -translationVec.at(totStrIt)[2] );
            }
            else
            {
                cmpObj->rotateStructure       ( this->all->at(strIt+1),
                                                settings,
                                                settings->clearMapFile,
                                                settings->verbose,
                                                settings->axisOrder,
                                                true );
                
                ProSHADE_data str1Copy        = ProSHADE_data ( this->one );
                translationVec.emplace_back   ( cmpObj->getTranslationFunctionMap ( &str1Copy, this->all->at(strIt+1), &xMov1, &yMov1, &zMov1 ) );
                
                this->all->at(strIt+1)->translateMap ( -translationVec.at(totStrIt)[0],
                                                       -translationVec.at(totStrIt)[1],
                                                       -translationVec.at(totStrIt)[2] );
            }
        }
        else
        {
            //================================ Pick the right range of objects (phase-wise)
            if ( ( strIt == 0 ) && ( this->all->at(strIt)->_keepOrRemove != false ) )
            {
                strIt                        += 1;
                reverseOrder                  = true;
            }
            
            //================================ Create Patterson comparison object
            ProSHADE_comparePairwise* cmpObj  = new ProSHADE_comparePairwise ( this->two,
                                                                               this->all->at(strIt),
                                                                               settings->mPower,
                                                                               settings->ignoreLs,
                                                                               settings->glIntegOrder,
                                                                               settings );
            
            //================================ Get the angles and correlation
            pattCorrelationVec.emplace_back   ( 0.0 );
            cmpObj->precomputeTrSigmaDescriptor ( );
            cmpObj->getSO3InverseMap          ( settings );
            double xMov1                      = 0.0;
            double yMov1                      = 0.0;
            double zMov1                      = 0.0;
            std::array<double,3> euAngs       = cmpObj->getEulerAngles ( settings, &pattCorrelationVec.at(totStrIt) );
            
            //================================ Clear the memory
            delete cmpObj;
            
            //================================ Create the object for rotation
            if ( reverseOrder )
            {
                cmpObj                        = new ProSHADE_comparePairwise ( this->all->at(strIt-1),
                                                                               this->all->at(strIt-1),
                                                                               settings->mPower,
                                                                               settings->ignoreLs,
                                                                               settings->glIntegOrder,
                                                                               settings );
            }
            else
            {
                cmpObj                        = new ProSHADE_comparePairwise ( this->all->at(strIt+1),
                                                                               this->all->at(strIt+1),
                                                                               settings->mPower,
                                                                               settings->ignoreLs,
                                                                               settings->glIntegOrder,
                                                                               settings );
            }
            
            //================================ Find 'opposite' Euler angles and set them
            // Get mat from Euler
            double mat22                      =  cos ( euAngs[1]  );
            double mat02                      = -sin ( euAngs[1]  ) * cos ( euAngs[2] );
            double mat12                      =  sin ( euAngs[1]  ) * sin ( euAngs[2] );
            double mat20                      =  cos ( euAngs[0] ) * sin ( euAngs[1]  );
            double mat21                      =  sin ( euAngs[0] ) * sin ( euAngs[1]  );
            
            // Transpose
            double rMat02                     = mat20;
            double rMat20                     = mat02;
            double rMat21                     = mat12;
            double rMat12                     = mat21;
            
            // Get Euler from map
            euAngs[0]                         = atan2 ( rMat21,  rMat20 );
            euAngs[1]                         = acos  ( mat22 );
            euAngs[2]                         = atan2 ( rMat12, -rMat02 );
            
            if ( euAngs[0] < 0.0 ) { euAngs[0]= 2.0 * M_PI + euAngs[0]; }
            if ( euAngs[1] < 0.0 ) { euAngs[1]=       M_PI + euAngs[1]; }
            if ( euAngs[2] < 0.0 ) { euAngs[2]= 2.0 * M_PI + euAngs[2]; }
            
            cmpObj->setEulerAngles            ( euAngs[0], euAngs[1], euAngs[2] );
            
            //================================ Rotate by the Euler
            if ( reverseOrder )
            {
                cmpObj->rotateStructure       ( this->all->at(strIt-1),
                                                settings,
                                                settings->clearMapFile,
                                                settings->verbose,
                                                settings->axisOrder,
                                                true );
                
                ProSHADE_data str1Copy            = ProSHADE_data ( this->two );
                translationVec.emplace_back       ( cmpObj->getTranslationFunctionMap ( &str1Copy, this->all->at(strIt-1), &xMov1, &yMov1, &zMov1 ) );
                
                this->all->at(strIt-1)->translateMap ( -translationVec.at(totStrIt)[0],
                                                       -translationVec.at(totStrIt)[1],
                                                       -translationVec.at(totStrIt)[2] );
            }
            else
            {
                cmpObj->rotateStructure       ( this->all->at(strIt+1),
                                               settings,
                                               settings->clearMapFile,
                                               settings->verbose,
                                               settings->axisOrder,
                                               true );
                
                ProSHADE_data str1Copy        = ProSHADE_data ( this->two );
                translationVec.emplace_back   ( cmpObj->getTranslationFunctionMap ( &str1Copy, this->all->at(strIt+1), &xMov1, &yMov1, &zMov1 ) );
                
                this->all->at(strIt+1)->translateMap ( -translationVec.at(totStrIt)[0],
                                                       -translationVec.at(totStrIt)[1],
                                                       -translationVec.at(totStrIt)[2] );
            }
        }
        
        totStrIt                             += 1;
    }
    
    //======================================== Delete the Patterson maps, they are no longer required
    for ( signed int strIt = static_cast<signed int> ( this->all->size() - 1 ); strIt >= 0 ; strIt-- )
    {
        if ( this->all->at(strIt)->_keepOrRemove == false )
        {
            delete this->all->at(strIt);
            this->all->at(strIt)              = nullptr;
            this->all->erase                  ( this->all->begin ( ) + strIt );
        }
    }
    
    //======================================== Done
    return ;
}








