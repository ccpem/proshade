/*! \file ProSHADE_internal.h
 \brief The main header file containing all declarations for the innter workings of the library.
 
 This is the header file containig declarations of all classes
 and functions required by the library. 
 
 This file is part of the ProSHADE library for calculating
 shape descriptors and symmetry operators of protein structures.
 This is a prototype code, which is by no means complete or fully
 tested. Its use is at your own risk only. There is no quarantee
 that the results are correct.
 
 \author    Michal Tykac
 \author    Garib N. Murshudov
 \version   0.6.6
 \date      JAN 2019
 */

//============================================ ProSHADE
#include "ProSHADE_version.h"
#include "ProSHADE_rvapi.h"

//============================================ Include once
#ifndef __PROSHADE_INTERNAL_LIBRARY__
#define __PROSHADE_INTERNAL_LIBRARY__

/*! \namespace ProSHADE_internal
 \brief This namespace contains all the internal objects and their forward declarations.
 
 The ProSHADE namespace wraps around everything in the library and thus makes sure that any library with
 identically called function will be easily distinguishable.
 */
namespace ProSHADE_internal
{
    //======================================== Forward declaration
    unsigned int checkFileType ( std::string fileName );
    
    //======================================== Forward declaration
    class ProSHADE_comparePairwise;
    
    /*! \class ProSHADE_symmetry
     \brief This is the class which computes the symmetry in a single structure.
     
     This class takes a single structure and the settings decided by the use and proceeds to compute all the requred
     information for symmetry detection in the constructor. Thus, once the object is constructed, the user only needs
     to call the accessor functions provided to get the deta and information rapidly.
     */
    class ProSHADE_symmetry
    {
        //==================================== Friends
        friend class ProSHADE_comparePairwise;
        
    private:
        //==================================== Settings regarding resolutions
        double                                mapResolution;                       //!< This is the internal resolution at which the calculations are done, not necessarily the resolution of the data.
        std::vector<unsigned int>             bandwidth;                           //!< This parameter determines the angular resolution of the spherical harmonics decomposition.
        std::vector<unsigned int>             glIntegOrder;                        //!< This parameter controls the Gauss-Legendre integration order and so the radial resolution.
        std::vector<unsigned int>             theta;                               //!< This parameter is the longitude of the spherical grid mapping. It should be 2 * bandwidth unless there is a specific reason to change it.
        std::vector<unsigned int>             phi;                                 //!< This parameter is the latitudd of the spherical grid mapping. It should be 2 * bandwidth unless there is a specific reason to change it.
        
        //==================================== Settings regarding B factors
        double                                bFactorValue;                        //!< This is the value to which all B-factors of PDB files will be changed to.
        double                                bFactorChange;                       //!< This value will be used to change the B-factors if required by the user.
        
        //==================================== Setting regarding maps and removing noise
        double                                noIQRsFromMap;                       //!< This is the number of interquartile distances from mean that is used to threshold the map masking.
        
        //==================================== Settings regarding concentric shells
        double                                shellSpacing;                        //!< This parameter determines how far the radial shells should be from each other.
        unsigned int                          manualShells;                        //!< Should the user require so, the maximum number of radial shells can be set.
        
        //==================================== Settings regarding map with phases
        bool                                  useCOM;                              //!< Should the Centre of Mass (COM) be used to center the structure in the cell?
        bool                                  firstLineCOM;                        //!< This is a special option for metal detection, please leave false.
        
        //==================================== Settings regarding space around the structure in lattice
        double                                extraSpace;                          //!< What should be the distance added on both sides to the structure, so that the next cell density would not affect the results?
        
        //==================================== Settings regarding weighting the distances
        double                                alpha;                               //!< This parameter determines the power to which the |F|'s should be raised.
        double                                mPower;                              //!< This parameter determines the scaling for trace sigma descriptor.
        
        //==================================== Settings regarding bands to ignore
        std::vector<int>                      ignoreLs;                            //!< This vector lists all the bandwidth values which should be ignored and not part of the computations.
        
        //==================================== Settings regarding the structures to use
        std::vector <std::string>             structFiles;                         //!< This vector should contain all the structures that are being dealt with, but this does not yet work!
        
        //==================================== Settings regarding the position of the structures
        double                                xPos;                                //!< This parameter holds the distance along x-axis from the origin to the centre of rotation.
        double                                yPos;                                //!< This parameter holds the distance along y-axis from the origin to the centre of rotation.
        double                                zPos;                                //!< This parameter holds the distance along z-axis from the origin to the centre of rotation.
        
        //==================================== Variables holding the results
        std::vector< std::array<double,8> >   rfPeaks;                             //!< This variable holds the complete symmetry peak results.
        std::vector< std::vector<std::array<double,5> > > tetrSymm;                //!< This variable holds the complete tetrahedral symmetries list.
        double tetrSymmPeakAvg;                                                    //!< This variable holds the averaged peaks value of all the icosahedral symmetry supporting symmetries.
        std::vector< std::array<double,5> >   tetrElems;                           //!< This variable holds the 12 unique symmetry elements of the tetrahedral symmetry, if they are found.
        std::vector< std::vector<std::array<double,5> > > octaSymm;                //!< This variable holds the complete octahedral symmetries list.
        double octaSymmPeakAvg;                                                    //!< This variable holds the averaged peaks value of all the icosahedral symmetry supporting symmetries.
        std::vector< std::array<double,5> >   octaElems;                           //!< This variable holds the 24 unique symmetry elements of the (cub)octahedral symmetry, if they are found.
        std::vector< std::vector<std::array<double,5> > > icosSymm;                //!< This variable holds the complete icosahedral symmetries list.
        double icosSymmPeakAvg;                                                    //!< This variable holds the averaged peaks value of all the icosahedral symmetry supporting symmetries.
        std::vector< std::array<double,5> >   icosElems;                           //!< This variable holds the 60 unique symmetry elements of the icosahedral symmetry, if they are found.
        
        //==================================== Settings regarding peak finding
        double                                peakHeightNoIQRs;                    //!< How many interquartile ranges should be used to distinguish 'false' peaks from the true ones?
        double                                peakDistanceForReal;                 //!< Threshold for determining 'missing peaks' existence.
        int                                   peakSurroundingPoints;               //!< For a peak to exist, how many points in every direction need to be smalled than the middle value?
        
        //==================================== Settings regarding axis and angle error tolerance
        double                                aaErrorTolerance;                    //!< The tolerance parameter on matching axes for the angle-axis representation of rotations.
        double                                symGapTolerance;                     //!< For C-symmetries - if there are many, only those with average peak height - parameter * top symmetry value will be shown in the output.
        
        //==================================== Are full results needed?
        bool                                  printFull;                           //!< This variable determines if the results should be printed in full, or just the short and clear version.
        
    public:
        //==================================== Public variables holding the results
        std::vector< std::array<double,5> >   cnSymm;                              //!< This variable holds the complete Cyclic symmetry results.
        std::vector< std::array<double,5> >   cnSymmClear;                         //!< This variable holds the gap corrected Cyclic symmetry results.
        std::vector< std::vector<std::array<double,6> > > dnSymm;                  //!< This variable holds the complete Dihedral symmetry results.
        std::vector< std::vector<std::array<double,6> > > dnSymmClear;             //!< This variable holds the gap corrected Dihedral symmetry results.
        std::vector< std::array<double,5> >   tetrAxes;                            //!< This variable holds the 7 unique axes of the tetrahedral symmetry, if they are found.
        std::vector< std::array<double,5> >   octaAxes;                            //!< This variable holds the 13 unique axes of the octahedral symmetry, if they are found.
        std::vector< std::array<double,5> >   icosAxes;                            //!< This variable holds the 31 unique axes of the octahedral symmetry, if they are found.
        bool                                  inputStructureDataType;              //!< This variable tells whether input data type is PDB or not.
        
        //==================================== Public functions
        ProSHADE_symmetry                                                ( ProSHADE::ProSHADE_settings* settings );
        ProSHADE_symmetry                                                ( );
        void                                  printResultsClear          ( int verbose );
        void                                  printResultsClearHTML      ( ProSHADE::ProSHADE_settings* settings );
        void                                  printResultsRequest        ( std::string symmetryType,
                                                                           unsigned int symmetryFold,
                                                                           int verbose );
        void                                  printResultsRequestHTML    ( std::string symmetryType,
                                                                           unsigned int symmetryFold,
                                                                           ProSHADE::ProSHADE_settings* settings );
        void                                  saveSymmetryElementsJSON   ( std::string symmetryType,
                                                                           unsigned int symmetryFold,
                                                                           ProSHADE::ProSHADE_settings* settings );
        std::vector< std::array<double,8> >   getRotFnPeaks              ( );
        std::vector< std::array<double,5> >   getCSymmetries             ( );
        std::vector< std::vector<std::array<double,6> > > getDSymmetries    ( );
        std::vector< std::array<double,5> >   generateTetrAxes           ( ProSHADE_comparePairwise* cmpObj,
                                                                           ProSHADE::ProSHADE_settings* settings,
                                                                           std::vector< std::array<double,5> > tetrSymm,
                                                                           std::vector< std::array<double,5> > allCs,
                                                                           double axisErrorTolerance = 0.1,
                                                                           int verbose = 0 );
        std::vector< std::array<double,5> >   generateTetrElements       ( std::vector< std::array<double,5> > symmAxes,
                                                                           ProSHADE::ProSHADE_settings* settings,
                                                                           int verbose = 0 );
        std::vector< std::array<double,5> >   generateOctaAxes           ( ProSHADE_comparePairwise* cmpObj,
                                                                           ProSHADE::ProSHADE_settings* settings,
                                                                           std::vector< std::array<double,5> > octaSymm,
                                                                           std::vector< std::array<double,5> > allCs,
                                                                           double axisErrorTolerance = 0.1,
                                                                           int verbose = 0 );
        std::vector< std::array<double,5> >   generateOctaElements       ( std::vector< std::array<double,5> > symmAxes,
                                                                           ProSHADE::ProSHADE_settings* settings,
                                                                           int verbose = 0 );
        std::vector< std::array<double,5> >   generateIcosAxes           ( ProSHADE_comparePairwise* cmpObj,
                                                                           ProSHADE::ProSHADE_settings* settings,
                                                                           std::vector< std::array<double,5> > icosSymm,
                                                                           std::vector< std::array<double,5> > allCs,
                                                                           double axisErrorTolerance = 0.1,
                                                                           int verbose = 0 );
        std::vector< std::array<double,5> >   generateIcosElements       ( std::vector< std::array<double,5> > symmAxes,
                                                                           ProSHADE::ProSHADE_settings* settings,
                                                                           int verbose = 0 );
    };
    
    /*! \class ProSHADE_data
     \brief This class deals with reading in the data and computing structure specific information including the spherical harmonics coefficients.
     
     The purpose of this class is the contain all the information regarding a single structure, but no information regarding any pair or comparison
     data. Therefore, it deals with reading in the data, formatting and mapping onto spheres, computing spherical harmonics coefficients and also
     pre-computing the energy level descriptor tables; all of these action are strictly related to each single structure file.
     */
    class ProSHADE_data
    {
        //==================================== Friends
        friend class ProSHADE_comparePairwise;
        friend class ProSHADE_compareOneAgainstAll;
        friend class ProSHADE_symmetry;
        friend class ProSHADE_distances;
        friend class ProSHADE_mapFeatures;
        
    private:
        //==================================== Variables regarding the reading in of the data
        std::string                          _inputFileName;                       //!< The name of the file from which the data come.
        bool                                 _fromPDB;                             //!< Is the file PDB or MAP?
        
        //==================================== Variables regarding the shells
        double                               _shellSpacing;                        //!< This parameter determines how far the radial shells should be from each other.
        double                               _maxExtraCellularSpace;               //!< What should be the distance added on both sides to the structure, so that the next cell density would not affect the results?
        int                                  _exCeSpDiffX;                         //!< No points added for extra space along X-axis
        int                                  _exCeSpDiffY;                         //!< No points added for extra space along Y-axis
        int                                  _exCeSpDiffZ;                         //!< No points added for extra space along Z-axis
        double                               _xRange;                              //!< The total size of the structure in angstrom in the direction of the X-axis.
        double                               _yRange;                              //!< The total size of the structure in angstrom in the direction of the Y-axis.
        double                               _zRange;                              //!< The total size of the structure in angstrom in the direction of the Z-axis.
        double                               _xSamplingRate;                       //!< The sampling rate along the X axis.
        double                               _ySamplingRate;                       //!< The sampling rate along the Y axis.
        double                               _zSamplingRate;                       //!< The sampling rate along the Z axis.
        float                                _xFrom;                               //!< The start of the x axis in maps.
        float                                _yFrom;                               //!< The start of the y axis in maps.
        float                                _zFrom;                               //!< The start of the z axis in maps.
        float                                _xTo;                                 //!< The end of the x axis in maps.
        float                                _yTo;                                 //!< The end of the y axis in maps.
        float                                _zTo;                                 //!< The end of the z axis in maps.
        std::vector<double>                  _shellPlacement;                      //!< Vector of distance for all shells.
        
        //==================================== Variables regarding the density map
        double                               _mapResolution;                       //!< This is the internal resolution at which the calculations are done, not necessarily the resolution of the data.
        unsigned int                         _maxMapU;                             //!< The total number of grid points along the X-axis.
        unsigned int                         _maxMapV;                             //!< The total number of grid points along the Y-axis.
        unsigned int                         _maxMapW;                             //!< The total number of grid points along the Z-axis.
        float                               *_densityMapMap;                       //!< The CMAPLIB map variable.
        bool                                 _densityMapComputed;                  //!< Was the density computed yet?
        double                               _mapMean;                             //!< Mean of the total density in the map (whole cell).
        double                               _mapSdev;                             //!< Standard deviation of the total density in the map (whole cell).
        int                                  _preCBSU;                             //!< The maxMapU before cell border space is applied.
        int                                  _preCBSV;                             //!< The maxMapV before cell border space is applied.
        int                                  _preCBSW;                             //!< The maxMapW before cell border space is applied.
        
        //==================================== Variables regarding the phase removal from map
        double                               _fourierCoeffPower;                   //!< This parameter determines the power to which the |F|'s should be raised.
        double                               _bFactorChange;                       //!< This value will be used to change the B-factors if required by the user.
        double                              *_densityMapCor;                       //!< Variable containing the final, processed density map.
        double                               _maxMapRange;                         //!< The size of the maximum dimmension in angstrom.
        std::array<double,3>                *_densityMapCorCoords;                 //!< Variable containing the final, processed density map coordinates.
        bool                                 _phaseRemoved;                        //!< Was the map processingg done already?
        bool                                 _usePhase;                            //!< Was the phase information removed, or is it used?
        bool                                 _keepOrRemove;                        //!< Should the odd bands be used in the computations? (I.e. it can be proven that of the phase is removed, the odd band spherical harmonics have to be 0 anyway, so this speeds the computations in such case).
        
        //==================================== Variables regarding the sphere mapping of phaseless data
        double                               _thetaAngle;                          //!< This parameter is the longitude of the spherical grid mapping. It should be 2 * bandwidth unless there is a specific reason to change it.
        double                               _phiAngle;                            //!< This parameter is the latitude of the spherical grid mapping. It should be 2 * bandwidth unless there is a specific reason to change it.
        unsigned int                         _noShellsWithData;                    //!< Number of shells which actually have data in them.
        double                             **_shellMappedData;                     //!< Array containing the structure mapping onto the concentric spheres.
        int                                  _xCorrection;                         //!< The COM centering correction to be applied along the X axis.
        int                                  _yCorrection;                         //!< The COM centering correction to be applied along the Y axis.
        int                                  _zCorrection;                         //!< The COM centering correction to be applied along the Z axis.
        double                               _xCorrErr;                            //!< The COM centering correction helper variable regarding the X axis.
        double                               _yCorrErr;                            //!< The COM centering correction helper variable regarding the Y axis.
        double                               _zCorrErr;                            //!< The COM centering correction helper variable regarding the Z axis.
        bool                                 _sphereMapped;                        //!< Were the data mapped onto the concentric spheres yet?
        bool                                 _firstLineCOM;                        //!< This is a special option for metal detection, please leave false.
        
        //==================================== Variables regarding the spherical harmonics decomposition
        unsigned int                         _bandwidthLimit;                      //!< This parameter determines the angular resolution of the spherical harmonics decomposition.
        unsigned int                         _oneDimmension;                       //!< This is a helper variables having twice the bandwidth value.
        double                             **_realSHCoeffs;                        //!< Array of the real values of the spherical harmonics decomposition coefficients.
        double                             **_imagSHCoeffs;                        //!< Array of the imaginary values of the spherical harmonics decomposition coefficients.
        double                              *_sphericalHarmonicsWeights;           //!< Helper array in the spherical harmonics computation.
        double                             **_semiNaiveTable;                      //!< Helper array in the spherical harmonics computation.
        double                              *_semiNaiveTableSpace;                 //!< Helper array in the spherical harmonics computation.
        fftw_complex                        *_shWorkspace;                         //!< Helper array in the spherical harmonics computation.
        bool                                 _sphericalCoefficientsComputed;       //!< Were the spherical harmonics computed yet?
        bool                                 _wasBandwithGiven;                    //!< Helper variable stating whether new optimal bandwidth should be computed after input data processing.
        bool                                 _wasThetaGiven;                       //!< Helper variable stating whether new optimal theta should be computed after input data processing.
        bool                                 _wasPhiGiven;                         //!< Helper variable stating whether new optimal phi should be computed after input data processing.
        bool                                 _wasGlInterGiven;                     //!< Helper variable stating whether new optimal integration order should be computed after input data processing.
        
        //==================================== Variables regarding spherical harmonics inverse
        double                             **_invRealData;                         //!< Array of the real values of the INVERTED spherical harmonics decomposition coefficients.
        double                             **_invImagData;                         //!< Array of the imaginary values of the INVERTED spherical harmonics decomposition coefficients.
        
        //==================================== Variables regarding the energy levels descriptor pre-calculation
        double                            ***_rrpMatrices;                         //!< Array of matrices required by the energy level descriptor which can be pre-computed for each structure independently on the other structure it will eventually be compared against.
        bool                                 _rrpMatricesPrecomputed;              //!< Were the energy level descriptor matrice pre-computed yet?
        
    public:
        //==================================== Public functions
        ProSHADE_data ( );
        ProSHADE_data ( ProSHADE_data *copyFrom );
        ProSHADE_data ( std::ifstream* dbFile, std::string fName, double volThreMin, double volThreMax, int verbose, ProSHADE::ProSHADE_settings* settings, bool saveWithAndWithout = false );
        ~ProSHADE_data ( );
        
        void                                  getDensityMapFromPDB       ( std::string fileName, double *shellDistance, double resolution,
                                                                           unsigned int *bandwidth, unsigned int *theta,
                                                                           unsigned int *phi, unsigned int *glIntegOrder,
                                                                           double *extraSpace, bool mapResDefault, ProSHADE::ProSHADE_settings* settings,
                                                                           double Bfactor = 80.0, bool hpFirstLineCom = false, bool overlayDefaults = false );
        void                                  getDensityMapFromMAP       ( std::string fileName, double *shellDistance, double resolution,
                                                                           unsigned int *bandwidth, unsigned int *theta,
                                                                           unsigned int *phi, unsigned int *glIntegOrder,
                                                                           double *extraSpace, bool mapResDefault, bool rotDefaults, ProSHADE::ProSHADE_settings* settings,
                                                                           bool overlayDefaults = false );
        void                                  maskMap                    ( int hlpU, int hlpV, int hlpW, double blurBy = 250.0, double maxMapIQR = 3.0, double extraMaskSpace = 3.0 );
        void                                  normaliseMap               ( ProSHADE::ProSHADE_settings* settings );
        std::array<double,4>                  getCOMandDist              ( ProSHADE::ProSHADE_settings* settings );
        bool                                  removeIslands              ( int hlpU, int hlpV, int hlpW, int verbose = 0, bool runAll = false );
        void                                  translateMap               ( double xShift, double yShift, double zShift );
        void                                  shiftMap                   ( double xShift, double yShift, double zShift );
        std::array<double,6>                  getDensityMapFromMAPRebox  ( std::string fileName, double maxMapIQR, double extraCS, int verbose,
                                                                           bool useCubicMaps, double maskBlurFactor, bool maskBlurFactorGiven,
                                                                           ProSHADE::ProSHADE_settings* settings );
        void                                  getDensityMapFromMAPFeatures ( std::string fileName, double *minDensPreNorm, double *maxDensPreNorm,
                                                                             double *minDensPostNorm, double *maxDensPostNorm, double *postNormMean,
                                                                             double *postNormSdev, double *maskVolume, double* totalVolume, double *maskMean,
                                                                             double *maskSdev, double *maskMin, double *maskMax, double *maskDensityRMS,
                                                                             double *allDensityRMS, std::array<double,3> *origRanges, std::array<double,3> *origDims,
                                                                             double maxMapIQR, double extraCS, int verbose, bool useCubicMaps, double maskBlurFactor, bool maskBlurFactorGiven,
                                                                             bool reboxAtAll, ProSHADE::ProSHADE_settings* settings );
        void                                  removePhaseFromMap         ( double alpha, double bFac, ProSHADE::ProSHADE_settings* settings );
        void                                  removePhaseFromMapOverlay  ( double alpha, double bFac, ProSHADE::ProSHADE_settings* settings );
        void                                  keepPhaseInMap             ( double alpha, double bFac,
                                                                           unsigned int *bandwidth, unsigned int *theta,
                                                                           unsigned int *phi, unsigned int *glIntegOrder, ProSHADE::ProSHADE_settings* settings,
                                                                           bool useCom = true,
                                                                           double maxMapIQR = 10.0, int verbose = 0,
                                                                           bool clearMapData = true, bool rotDefaults = false, bool overlapDefaults = false,
                                                                           double blurFactor = 500.0, bool maskBlurFactorGiven = false );
        void                                  mapPhaselessToSphere       ( ProSHADE::ProSHADE_settings* settings, double theta, double phi, double shellSz, unsigned int manualShells = 0, bool keepInMemory = false, bool rotDefaults = false );
        void                                  getSphericalHarmonicsCoeffs ( unsigned int bandwidth, ProSHADE::ProSHADE_settings* settings );
        void                                  precomputeRotInvDescriptor ( ProSHADE::ProSHADE_settings* settings );
        std::vector<ProSHADE_data*>           fragmentMap                ( ProSHADE::ProSHADE_settings* settings, bool userCOM );
        std::vector< std::vector<int> >       findMapIslands             ( int hlpU, int hlpV, int hlpW, double *map, double threshold = 0.0 );
        void                                  writeMap                   ( std::string fileName,
                                                                           double *map,
                                                                           std::string axOrder = "xyz",
                                                                           float xFrom  = std::numeric_limits<float>::infinity(),
                                                                           float yFrom  = std::numeric_limits<float>::infinity(),
                                                                           float zFrom  = std::numeric_limits<float>::infinity(),
                                                                           float xTo    = std::numeric_limits<float>::infinity(),
                                                                           float yTo    = std::numeric_limits<float>::infinity(),
                                                                           float zTo    = std::numeric_limits<float>::infinity(),
                                                                           float xRange = std::numeric_limits<float>::infinity(),
                                                                           float yRange = std::numeric_limits<float>::infinity(),
                                                                           float zRange = std::numeric_limits<float>::infinity() );
        void                                  writeMap                   ( std::string fileName,
                                                                           float *map,
                                                                           std::string axOrder = "xyz",
                                                                           float xFrom  = std::numeric_limits<float>::infinity(),
                                                                           float yFrom  = std::numeric_limits<float>::infinity(),
                                                                           float zFrom  = std::numeric_limits<float>::infinity(),
                                                                           float xTo    = std::numeric_limits<float>::infinity(),
                                                                           float yTo    = std::numeric_limits<float>::infinity(),
                                                                           float zTo    = std::numeric_limits<float>::infinity(),
                                                                           float xRange = std::numeric_limits<float>::infinity(),
                                                                           float yRange = std::numeric_limits<float>::infinity(),
                                                                           float zRange = std::numeric_limits<float>::infinity() );
        void                                  writePDB                   ( std::string templateName, std::string outputName,
                                                                           double rotEulA = 0.0,
                                                                           double rotEulB = 0.0,
                                                                           double rotEulG = 0.0,
                                                                           double trsX    = 0.0,
                                                                           double trsY    = 0.0,
                                                                           double trsZ    = 0.0 );
        void                                  deleteModel                ( std::string modelPath );
        
        //==================================== Accessor functions
        inline double                         getMapXRange               ( void ) { return ( this->_xRange ); };
        inline double                         getMapYRange               ( void ) { return ( this->_yRange ); };
        inline double                         getMapZRange               ( void ) { return ( this->_zRange ); };
        inline double*                        getMap                     ( void ) { return ( this->_densityMapCor ); };
        
    };
    
    /*! \class ProSHADE_comparePairwise
     \brief This is the executive class responsible for comparing strictly two structures.
     
     This class is used to compare strictly two structures and is currently used for symmetry detection,
     but should not really be used for the distance computation, as there is designated class for this
     (ProSHADE_distances) and this class uses the current and best tested approach.
     */
    class ProSHADE_comparePairwise
    {
    private:
        //==================================== Variables regarding sanity check
        unsigned int                         _bandwidthLimit;                    //!< This parameter determines the angular resolution of the spherical harmonics decomposition.
        double                               _shellSpacing;                      //!< This parameter determines how far the radial shells should be from each other.
        double                               _thetaAngle;                        //!< This parameter is the longitude of the spherical grid mapping. It should be 2 * bandwidth unless there is a specific reason to change it.
        double                               _phiAngle;                          //!< This parameter is the latitude of the spherical grid mapping. It should be 2 * bandwidth unless there is a specific reason to change it.
        double                               _matrixPowerWeight;                 //!< This parameter determines the power to which the |F|'s should be raised.
        unsigned int                         _minShellsToUse;                    //!< This is the minimum of shells with data of the two comparison structures.
        std::vector<int>                     _lsToIgnore;                        //!< This vector lists all the bandwidth values which should be ignored and not part of the computations.
        bool                                 _bothRRPsPreComputed;               //!< Were the energy level descriptors pre-computed for both structures?
        bool                                 _keepOrRemove;                      //!< Should the phase information be kept, or was it removed and therefore all the odd bands are 0?
        
        //==================================== Variables regarding RotInv distance
        double                            ***_obj1RRPs;                          //!< Array of the RRP matrices for energy level distance for structure 1.
        double                            ***_obj2RRPs;                          //!< Array of the RRP matrices for energy level distance for structure 2.
        double                               _distanceRotInv;                    //!< The actual value of the energy level descriptor distance for the two compared structures.
        bool                                 _rotInvComputed;                    //!< Was the energy levels descriptor computed for this pair of structures yet?
        
        //==================================== Variables regarding TrSigma distance pre-calculation
        unsigned int                         _noShellsObj1;                      //!< Number of shells with data for structure 1.
        unsigned int                         _noShellsObj2;                      //!< Number of shells with data for structure 2.
        unsigned int                         _maxShellsToUse;                    //!< The maximum number of shells with data for this comparison.
        double                             **_obj1RealCoeffs;                    //!< The real part of the spherical harmonics coefficients decomposition for structure 1.
        double                             **_obj1ImagCoeffs;                    //!< The imaginary part of the spherical harmonics coefficients decomposition for structure 1.
        double                             **_obj2RealCoeffs;                    //!< The real part of the spherical harmonics coefficients decomposition for structure 2.
        double                             **_obj2ImagCoeffs;                    //!< The imaginary part of the spherical harmonics coefficients decomposition for structure 2.
        std::vector<double>                  _trSigmaWeights;                    //!< The normalisation coefficients for trace sigma distance computation.
        std::array<double,2>              ***_trSigmaEMatrix;                    //!< The E matrices (integrating out r from spherical harmonics multiplication).
        bool                                 _trSigmaPreComputed;                //!< Was the trace sigma descriptor value computed yet?
        
        //==================================== Variables regarding TrSigma distance computation
        double                               _distanceTrSigma;                   //!< The actual value of the trace sigma descriptor distance.
        bool                                 _trSigmaComputed;                   //!< Was the trace sigma descriptor computed yet?
        
        //==================================== Variables regarding the SO3 Transform
        fftw_complex                        *_so3Coeffs;                         //!< Array holding the full SO3 transformation values.
        fftw_complex                        *_so3InvCoeffs;                      //!< Array holding the full inverse of SO3 transformation values.
        fftw_complex                        *_so3Workspace1;                     //!< Helper array for the SO3 transform/inverse transform computations.
        fftw_complex                        *_so3Workspace2;                     //!< Helper array for the SO3 transform/inverse transform computations.
        double                              *_so3Workspace3;                     //!< Helper array for the SO3 transform/inverse transform computations.
        bool                                 _so3InvMapComputed;                 //!< Was the SO3 inverse computation done yet?
        
        //==================================== Variables regarding the Euler angles
        std::array<double,3>                 _eulerAngles;                       //!< The Euler angles to be used to compute the full rotation function distance.
        bool                                 _eulerAnglesFound;                  //!< Were the Euler angles for full rotation function distance computed yet?
        
        //==================================== Variables regarding symmetry detection
        double                               _peakHeightThr;                     //!< Threshold used to determine whether peak is believed to be 'real' or 'fake'.
        bool                                 _CSymmsFound;                       //!< Were the C-symmetries detected yet?
        bool                                 _DSymmsFound;                       //!< Were the C-symmetries detected yet?
        
        //==================================== Variables regarding the Wigner d matrices computation
        std::vector< std::vector< std::vector< std::array<double,2> > > > _wignerMatrices; //!< The complete set of Wigner D matrices for the given Euler angles.
        bool                                 _wignerMatricesComputed;            //!< Were the Wigner D matrices computed yet?
        
        //==================================== Variables regarding Gauss-Legendre integration
        unsigned int                         _glIntegrationOrder;                //!< The order of the Gauss-Legendre integration to be used.
        std::vector<double>                  _glAbscissas;                       //!< Complete list of the abscissas for the Gauss-Legendre integration given the order.
        std::vector<double>                  _glWeights;                         //!< Complete list of the weights for the Gauss-Legendre integration given the order.
        
        //==================================== Variables regarding the coefficient rotation
        bool                                 _structureCoeffsRotated;            //!< Were the spherical harmonics coefficients rotated by the Wigner D matrices yet?
        
        //==================================== Internal functions
        double                                gl20IntRR                  ( std::vector<double>* vals );
        std::array<double,2>                  gl20IntCR                  ( std::vector< std::array<double,2> >* vals );
        std::vector<double>                   getSingularValues          ( unsigned int band, unsigned int dim, ProSHADE::ProSHADE_settings* settings );
        std::vector< std::vector< std::vector<double> > > getSingularValuesUandVMatrix ( std::vector< std::vector<double> > mat, unsigned int dim, ProSHADE::ProSHADE_settings* settings );
        double                                maxPeakHeightFromAngleAxis ( double X, double Y, double Z, double Angle, ProSHADE::ProSHADE_settings* settings );
        
    public:
        //==================================== Public functions
        ProSHADE_comparePairwise                                         ( ProSHADE_data *cmpObj1, ProSHADE_data *cmpObj2, double mPower, std::vector<int> ignoreL, unsigned int order, ProSHADE::ProSHADE_settings* settings );
        ~ProSHADE_comparePairwise                                        ( );
        
        void                                  precomputeTrSigmaDescriptor ( );
        void                                  getSO3InverseMap           ( ProSHADE::ProSHADE_settings* settings );
        std::array<double,3>                  getEulerAngles             ( ProSHADE::ProSHADE_settings* settings, double* correlation = nullptr );
        void                                  setEulerAngles             ( double alpha, double beta, double gamma );
        std::vector< std::array<double,8> >   getSO3Peaks                ( ProSHADE::ProSHADE_settings* settings, double noIQRs = 1.5, bool freeMem = true, int peakSize = 1, double realDist = 0.4, int verbose = 1 );
        bool                                  checkPeakExistence         ( double alpha, double beta, double gamma, ProSHADE::ProSHADE_settings* settings, double passThreshold = 0.3 );
        std::vector< std::array<double,5> >   findCnSymmetry             ( std::vector< std::array<double,8> > peaks,
                                                                           ProSHADE::ProSHADE_settings* settings,
                                                                           double axisErrorTolerance = 0.0,
                                                                           bool freeMem = true,
                                                                           double percAllowedToMiss = 0.33,
                                                                           int verbose = 1 );
        std::vector< std::array<double,5> >   findCnSymmetryClear        ( std::vector< std::array<double,5> > CnSymm, ProSHADE::ProSHADE_settings* settings, double maxGap = 0.2, bool *pf = nullptr );
        std::vector< std::vector< std::array<double,6> > > findDnSymmetry ( std::vector< std::array<double,5> > CnSymm, double axisErrorTolerance = 0.1 );
        std::vector< std::vector< std::array<double,6> > > findDnSymmetryClear ( std::vector< std::vector< std::array<double,6> > > DnSymm, ProSHADE::ProSHADE_settings* settings, double maxGap = 0.2, bool *pf = nullptr );
        std::vector< std::vector< std::array<double,5> > > findTetrSymmetry ( std::vector< std::array<double,5> > CnSymm, double *tetrSymmPeakAvg, double axisErrorTolerance = 0.1 );
        std::vector< std::vector< std::array<double,5> > > findOctaSymmetry ( std::vector< std::array<double,5> > CnSymm, double *octaSymmPeakAvg, double axisErrorTolerance = 0.1 );
        std::vector< std::vector< std::array<double,5> > > findIcosSymmetry ( std::vector< std::array<double,5> > CnSymm, double *icosSymmPeakAvg, double axisErrorTolerance = 0.1 );
        void                                  generateWignerMatrices     ( ProSHADE::ProSHADE_settings* settings );
        double                                getRotCoeffDistance        ( ProSHADE::ProSHADE_settings* settings );
        double                                maxAvgPeakForSymmetry      ( double X, double Y, double Z, double Angle, ProSHADE::ProSHADE_settings* settings );
        void                                  freeInvMap                 ( );
        double                                getPeakThreshold           ( );
        void                                  rotateStructure            ( ProSHADE_data *cmpObj1, ProSHADE::ProSHADE_settings* settings,
                                                                           std::string saveName, int verbose = 0, std::string axOrd = "xyz",
                                                                           bool internalUse = false );
        std::array<double,4>                  getTranslationFunctionMap  ( ProSHADE_data *obj1, ProSHADE_data *obj2, double *ob2XMov = nullptr,
                                                                           double *ob2YMov = nullptr, double *ob2ZMov = nullptr );
    };
    
    /*! \class ProSHADE_compareOneAgainstAll
     \brief This is the executive class responsible for comparing two or more structures.
     
     This class is used to compare two or more structures and is currently used for distances computation,
     but should not really be used for directly by the user, as there is designated class for this
     (ProSHADE_distances) and this class uses the current and best tested approach.
     */
    class ProSHADE_compareOneAgainstAll
    {
        //==================================== Friends
        friend class ProSHADE_distances;
        
    private:
        //==================================== Just save the input data and make the internal functions do the checks themselves
        ProSHADE_data                        *one;                               //!< A pointer to the original data for the structure to compare against the rest.
        ProSHADE_data                        *two;                               //!< A pointer to the original data for the structure to compare against the rest, but this time with phase reversal.
        std::vector<ProSHADE_data*>          *all;                               //!< A vector of pointers to the original data for the structures to compare against the one.
        
        //==================================== These needs to be the same
        std::vector<int>                     _lsToIgnore;                        //!< This vector lists all the bandwidth values which should be ignored and not part of the computations.
        double                               _matrixPowerWeight;                 //!< This parameter determines the power to which the |F|'s should be raised.
        
        //==================================== Simple checks
        bool                                 _energyLevelsComputed;              //!< Were the energy level distances computed yet?
        bool                                 _trSigmaPreComputed;                //!< Were the E matrices computed yet?
        bool                                 _trSigmaComputed;                   //!< Were the trace sigma distances computed yet?
        bool                                 _keepOrRemove;                      //!< Should the phase information of kept, or can the odd bands be ignored as they are 0 for Patterson-like maps.
        bool                                 _so3InvMapComputed;                 //!< Were the SO3 inverse transformations computed yet?
        bool                                 _eulerAnglesFound;                  //!< Were all the Euler angles required for the full rotation function distance computation obtained yet?
        bool                                 _wignerMatricesComputed;            //!< Were the Wigner D matrices computed yet?
        bool                                 _fullDistComputed;                  //!< Were the full rotation function distances computed yet?
        
        //==================================== Result holders
        std::vector<double>                  _distancesEnergyLevels;             //!< The actual energy levels distances from the one structure to all the other structres.
        std::vector<double>                  _distancesTraceSigma;               //!< The actual trace sigma distances from the one structure to all the other structres.
        std::vector<double>                  _distancesFullRotation;             //!< The actual full rotation function distances from the one structure to all the other structres.
        
        //==================================== Internal value holders
        std::vector< std::vector< std::vector< std::vector< std::array<double,2> > > > > _EMatrices; //!< Vector of all the E matrices.
        std::vector<fftw_complex*>           _so3InverseCoeffs;                  //!< Vector of all the SO3 inverse transform maps.
        std::vector< std::array<double,3> >    _eulerAngles;                     //!< Vector containing the Euler angles for all the comparisons.
        std::vector< std::vector< std::vector< std::vector< std::array<double,2> > > > > _wignerMatrices; //!< Vector of the Wigner D matrices for all the comparisons.
        std::vector<unsigned int>            _enLevelsDoNotFollow;               //!< Vector indicating whether descriptors past the energy level ones should be computed due to threshold (used for hierarchical distance computation).
        std::vector<unsigned int>            _trSigmaDoNotFollow;                //!< Vector indicating whether descriptors past the trace sigma ones should be computed due to threshold (used for hierarchical distance computation).
        
        //==================================== Internal functions
        double                                glIntRR                    ( std::vector<double>* vals, double shellSep, std::vector<double>* glAbscissas, std::vector<double>* glWeights );
        std::array<double,2>                  glIntCR                    ( std::vector< std::array<double,2> >* vals, double shellSep, std::vector<double> *glAbscissas, std::vector<double> *glWeights );
        std::vector<double>                   getSingularValues          ( unsigned int strNo, unsigned int band, unsigned int dim, ProSHADE::ProSHADE_settings* settings );
        
    public:
        //==================================== Public functions
        ProSHADE_compareOneAgainstAll                                    ( ProSHADE_data *oneStr, std::vector<ProSHADE_data*> *allStrs,
                                                                           std::vector<int> ignoreL, double matrixPowerWeight, int verbose );
        ProSHADE_compareOneAgainstAll                                    ( ProSHADE_data *oneStr, ProSHADE_data *twoStr, std::vector<ProSHADE_data*> *allStrs,
                                                                           std::vector<int> ignoreL, double matrixPowerWeight, int verbose );
        ~ProSHADE_compareOneAgainstAll                                   ( );
        
        std::vector<double>                   getEnergyLevelsDistance    ( int verbose, ProSHADE::ProSHADE_settings* settings );
        void                                  precomputeTrSigmaDescriptor ( double shellSpacing, std::vector<unsigned int>* glIntegOrderVec, ProSHADE::ProSHADE_settings* settings );
        std::vector<double>                   getTrSigmaDistance         ( int verbose, ProSHADE::ProSHADE_settings* settings );
        void                                  getSO3InverseMap           ( ProSHADE::ProSHADE_settings* settings );
        std::vector< std::array<double,3> >   getEulerAngles             ( ProSHADE::ProSHADE_settings* settings );
        void                                  generateWignerMatrices     ( ProSHADE::ProSHADE_settings* settings );
        std::vector<double>                   getRotCoeffDistance        ( int verbose, ProSHADE::ProSHADE_settings* settings );
        void                                  alignDensities             ( ProSHADE::ProSHADE_settings* settings );
    };
    
    /*! \class ProSHADE_distances
     \brief This is the executive class for computing distances between two or more structures.
     
     This class is intended to be accessed by the user in order to obtain distances between two or more structures. It takes the ProSHADE_settings class
     instance and determines everything from these, proceeding to compute all required distances in its constructor. This can take some time for multitude
     of structures, but accessing and using the results once the constructor is done is immediate and easy.
     */
    class ProSHADE_distances
    {
    private:
        //==================================== Settings regarding resolutions
        double                                mapResolution;                     //!< This is the internal resolution at which the calculations are done, not necessarily the resolution of the data.
        unsigned int                          bandwidth;                         //!< This parameter determines the angular resolution of the spherical harmonics decomposition.
        unsigned int                          glIntegOrder;                      //!< This parameter controls the Gauss-Legendre integration order and so the radial resolution.
        unsigned int                          theta;                             //!< This parameter is the longitude of the spherical grid mapping. It should be 2 * bandwidth unless there is a specific reason to change it.
        unsigned int                          phi;                               //!< This parameter is the latitudd of the spherical grid mapping. It should be 2 * bandwidth unless there is a specific reason to change it.
        std::vector<unsigned int>             bandwidthVec;                      //!< This is a vector containing the optimal bandwidths for each structure.
        std::vector<unsigned int>             glIntegOrderVec;                   //!< This is a vector containing the optimal integration orders for each structure.
        std::vector<unsigned int>             thetaVec;                          //!< This is a vector containing the optimal theta angles for each structure.
        std::vector<unsigned int>             phiVec;                            //!< This is a vector containing the optimal phi angles for each structure.
        
        //==================================== Settings regarding B factors
        double                                bFactorValue;                      //!< This is the value to which all B-factors of PDB files will be changed to.
        double                                bFactorChange;                     //!< This value will be used to change the B-factors if required by the user.
        
        //==================================== Setting regarding maps and removing noise
        double                                noIQRsFromMap;                     //!< This is the number of interquartile distances from mean that is used to threshold the map masking.
        
        //==================================== Settings regarding concentric shells
        double                                shellSpacing;                      //!< This parameter determines how far the radial shells should be from each other.
        unsigned int                          manualShells;                      //!< Should the user require so, the maximum number of radial shells can be set.
        
        //==================================== Settings regarding map with phases
        bool                                  useCOM;                            //!< Should the Centre of Mass (COM) be used to center the structure in the cell?
        bool                                  firstLineCOM;                      //!< This is a special option for metal detection, please leave false.
        
        //==================================== Settings regarding space around the structure in lattice
        double                                extraSpace;                        //!< What should be the distance added on both sides to the structure, so that the next cell density would not affect the results?
        std::vector<double>                   extraSpaceVec;                     //!< This vector contains the optimal extra-cellular spacing for each comparison structure.
        
        //==================================== Settings regarding weighting the distances
        double                                alpha;                             //!< This parameter determines the power to which the |F|'s should be raised.
        double                                mPower;                            //!< This parameter determines the scaling for trace sigma descriptor.
        
        //==================================== Settings regarding bands to ignore
        std::vector<int>                      ignoreLs;                          //!< This vector lists all the bandwidth values which should be ignored and not part of the computations.
        
        //==================================== Settings regarding the structures to use
        std::vector <std::string>             structFiles;                       //!< This vector contains the file names of the structures to be compared
        
        //==================================== Settings regarding which distances to compute
        bool                                  energyLevelDist;                   //!< Should the energy level distance be computed?
        bool                                  traceSigmaDist;                    //!< Should the trace sigma distance be computed?
        bool                                  fullRotFnDist;                     //!< Should the full rotation function distance be computed?
        
        //==================================== Settings regarding dealing with phases
        bool                                  usePhase;                          //!< Should the phase information be used?
        bool                                  saveWithAndWithout;                //!< Should both with and without phase be saved?
        
        //==================================== Distances holder variables
        std::vector<double>                   energyLevelsDistances;             //!< Vector containing the energy level distances between the first and all the other structures.
        std::vector<double>                   traceSigmaDistances;               //!< Vector containing the trace sigma distances between the first and all the other structures.
        std::vector<double>                   fullRotationDistances;             //!< Vector containing the full rotation function distances between the first and all the other structures.
        std::vector< std::vector<double> >    fragEnergyLevelsDistances;         //!< This is a vector hoding the energy level distances for each fragment - only applicable to fragmented map approach.
        std::vector< std::vector<double> >    fragTraceSigmaDistances;           //!< This is a vector hoding the trace sigma distances for each fragment - only applicable to fragmented map approach.
        std::vector< std::vector<double> >    fragfullRotationDistances;         //!< This is a vector hoding the full rotation function distances for each fragment - only applicable to fragmented map approach.
        
        //==================================== Settings regarding thresholds for hierarchical distance computation
        double                                enLevelsThreshold;                 //!< All structure pairs with energy level descriptor value less than this will not be subjected to any further descriptor computations.
        double                                trSigmaThreshold;                  //!< All structure pairs with trace sigma descriptor value less than this will not be subjected to any further descriptor computations.
        
        //==================================== Pointer to the comparison object
        ProSHADE_compareOneAgainstAll        *cmpObj;                            //!< A pointer to the comparison object which contains all the key calculations.
        
    public:
        //==================================== Public functions
        ProSHADE_distances                                               ( ProSHADE::ProSHADE_settings* settings );
       ~ProSHADE_distances                                               ( );
        void                                  saveDatabase               ( ProSHADE::ProSHADE_settings* settings );
        void                                  compareAgainstDatabase     ( ProSHADE::ProSHADE_settings* settings,
                                                                          std::vector<std::string>* matchedStrNames );
        void                                  compareFragAgainstDatabase ( ProSHADE::ProSHADE_settings* settings,
                                                                          std::vector<std::string>* matchedStrNames );
        std::vector<double>                   getEnergyLevelsDistances   ( );
        std::vector<double>                   getTraceSigmaDistances     ( );
        std::vector<double>                   getFullRotationDistances   ( );
        std::vector< std::vector<double> >    getFragEnergyLevelsDistances( );
        std::vector< std::vector<double> >    getFragTraceSigmaDistances  ( );
        std::vector< std::vector<double> >    getFragFullRotationDistances( );
    };
    
    /*! \class ProSHADE_mapFeatures
     \brief This class is responsible for reading in map and computing all its features and other possible map manipulation.
     
     This class is responsible for reading in any map file and computing the mask. It then proceeds to compute multiple statistical
     descriptors of the map as a whole and the masked part only, so that cleaner EM maps can be produced. It will also contain the
     code for splitting maps into fragments.
     */
    class ProSHADE_mapFeatures
    {
    private:
        //==================================== Private variables
        ProSHADE_internal::ProSHADE_data*     one;                               //!< A pointer to the structure data containing object.
        double                                fragBoxSize;                       //!< If map fragmentatin is to be done, this size boxes will be made.
        std::string                           mapFragName;                       //!< Filename and path to which the fragmented boxes are to be saved. Indices will be added automatically.
        double                                mapFragBoxFraction;                //!< Fraction of the fragmentation boxes that needs to have density in order to be considered.
        double                                maxMapU;                           //!< The map maximum X-axis index.
        double                                maxMapV;                           //!< The map maximum Y-axis index.
        double                                maxMapW;                           //!< The map maximum Z-axis index.
        double                                xRange;                            //!< The map X-axis dimmension in Angstroms.
        double                                yRange;                            //!< The map Y-axis dimmension in Angstroms.
        double                                zRange;                            //!< The map Z-axis dimmension in Angstroms.
        
        double                                minDensPreNorm;                    //!< Whole map minimum density value before normalisation.
        double                                maxDensPreNorm;                    //!< Whole map maximum density value before normalisation.
        double                                minDensPostNorm;                   //!< Whole map minimum density value after normalisation.
        double                                maxDensPostNorm;                   //!< Whole map maximum density value after normalisation.
        double                                postNormMean;                      //!< Whole map mean density value after normalisation.
        double                                postNormSdev;                      //!< Whole map standard deviation of density values after normalisation.
        double                                maskVolume;                        //!< The volume of masked density in A^3.
        double                                totalVolume;                       //!< Total volume of the cell in A^3.
        double                                maskMean;                          //!< Masked part of the map mean density value after normalisation.
        double                                maskSdev;                          //!< Masked part of the map standard deviation density value after normalisation.
        double                                maskMax;                           //!< Masked part of the map maximum density value after normalisation.
        double                                maskMin;                           //!< Masked part of the map minimum density value after normalisation.
        double                                maskDensityRMS;                    //!< Masked part of the map RMS value after normalisation.
        double                                allDensityRMS;                     //!< Whole map RMS value after normalisation.
        std::array<double,3>                  origRanges;                        //!< The original map ranges before clearmap space removal.
        std::array<double,3>                  origDims;                          //!< The original map dimmensions before clearmap space removal.
        std::vector<std::string>              fragFiles;                         //!< The paths to all fragment files resulting from a fragmentation run.
        bool                                  fragFilesExist;                    //!< Were fragments produced?
        
    public:
        //==================================== Public functions
        ProSHADE_mapFeatures                                             ( ProSHADE::ProSHADE_settings *settings );
        ~ProSHADE_mapFeatures                                            ( );
        
        void                                  printInfo                  ( int verbose );
        void                                  fragmentMap                ( std::string axOrder, int verbose, ProSHADE::ProSHADE_settings* settings );
        std::vector<std::string>              getFragmentsList           ( void );
        void                                  dealWithHalfMaps           ( ProSHADE::ProSHADE_settings* settings, double *xTranslate, double *yTranslate, double *zTranslate );
    };
    
}


//============================================ Include once
#endif
