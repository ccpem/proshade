/*! \file ProSHADE_files.h
 \brief This header file contains function and globals required for platform-independent file detection.
 
 The following code is a simple header for function to locate all files
 within a given path and with given extension. To use the function,
 simply supply the address of vector of strings to which the paths
 to all recovered files are to be saved, the path to start
 searching and the extension required. For no extension, use "".
 Also, note that the function is recursive by default and minor
 changes to the code would be required to remove this behaviour.
 
 This file is part of the ProSHADE library for calculating
 shape descriptors and symmetry operators of protein structures.
 This is a prototype code, which is by no means complete or fully
 tested. Its use is at your own risk only. There is no quarantee
 that the results are correct.
 
 \author    Michal Tykac
 \author    Garib N. Murshudov
 \version   0.6.6
 \date      JAN 2019
 */
 
#ifndef __FILES_PROSHADE__
#define __FILES_PROSHADE__

#include <iostream>
#include <cstring>
#include <dirent.h>
#include <vector>


#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
  static const std::string systemDirectorySeparator = "\\"; /*!< This global variable contains forward or backward slash, depending on the platform */
#else
  static const std::string systemDirectorySeparator = "/";
#endif

/*! \namespace ProSHADE_files
    \brief This namespace contains a single function to find files in file tree.
 
    The findFiles namespace contains a recursive function for searching all files with given
    extension in all subfolders of a given file path. Note: The function is recursive by
    default! Also, the search should be platform independent, but this was never tested.
 */
namespace ProSHADE_internal_files
{
    /*! \brief Function to recursively search for files with given extension in the file path supplied.
     
        This function searches a given folder and all subfolders (recursively) for any and all files with
        the extension given. It will return all files if no extension is given (i.e. the extension parameter
        is ""). Upon completing the search, the funciton will save the full paths to all found files in a 
        vector and returns it.
     
        \param[in] saveFiles This is a pointer to the vector of strings to which all found file paths are to be saved.
        \param[in] path This is the path which should be searched (including all sub-directories).
        \param[in] ext This is the extension which the files are required to have. Leave "" for any extension.
        \param[out] X Bool value indicating whether the function have failed or suceeded.
     
        \warning The function is recursive by default.
        \warning The function was never tested on windows.
        \warning There is no guarantee that the function return order will not change between runs with the same parameters.
        \warning This is an internal function which should not be used by the user.
     */
    bool findFiles ( std::vector <std::string>* saveFiles, const char* path, const char* ext );
}
    
#endif
