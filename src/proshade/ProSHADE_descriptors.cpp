/*! \file ProSHADE_descriptors.cpp
 \brief This file contains all the functions for computing and pre-computing the three shape descriptors.
 
 This file contains the functions required to compute and pre-compute the energy levels, trace sigma and full
 rotation function shape descriptors. These are present in two forms, one for the symmetry computation class
 and one for the actual distance computation class. This is because some of the pre-computation results are
 actually required by the symmetry detection process.
 
 This file is part of the ProSHADE library for calculating
 shape descriptors and symmetry operators of protein structures.
 This is a prototype code, which is by no means complete or fully
 tested. Its use is at your own risk only. There is no quarantee
 that the results are correct.
 
 \author    Michal Tykac
 \author    Garib N. Murshudov
 \version   0.6.6
 \date      JAN 2019
 */

//============================================ FFTW3 + SOFT
#ifdef __cplusplus
extern "C" {
#endif
#include <fftw3.h>
#include <wrap_fftw.h>
#include <makeweights.h>
#include <s2_primitive.h>
#include <s2_cospmls.h>
#include <s2_legendreTransforms.h>
#include <s2_semi_fly.h>
#include <rotate_so3_utils.h>
#include <utils_so3.h>
#include <soft_fftw.h>
#include <rotate_so3_fftw.h>
#ifdef __cplusplus
}
#endif

//============================================ RVAPI
#include <rvapi_interface.h>

//============================================ ProSHADE
#include "ProSHADE.h"
#include "ProSHADE_internal.h"
#include "ProSHADE_misc.h"
#include "ProSHADE_legendre.h"

/*! \brief Function for computing the Pearson correlation coefficient.
 
 This function takes two numerical arrays of same length and proceeds to compute the Pearson's
 correlation coefficient, which it then returns.
 
 \param[in] valSet1 This is the set of x-values.
 \param[in] valSet2 This is the set of y-values.
 \param[in] length The length of both arrays (both arrays have to have the same length).
 \param[out] X The Pearson's correlation coefficient value.
 
 \warning This function only works with double precision.
 \warning This is an internal function which should not be used by the user.
 */
double ProSHADE_internal_misc::pearsonCorrCoeff ( double *valSet1,
                                                  double *valSet2,
                                                  unsigned int length )
{
    //======================================== Find vector means
    double xMean                              = 0.0;
    double yMean                              = 0.0;
    for ( unsigned int iter = 0; iter < length; iter++ )
    {
        xMean                                += valSet1[iter];
        yMean                                += valSet2[iter];
    }
    xMean                                    /= static_cast<double> ( length );
    yMean                                    /= static_cast<double> ( length );
    
    //======================================== Get Pearson's correlation coefficient
    double xmmymm                             = 0.0;
    double xmmsq                              = 0.0;
    double ymmsq                              = 0.0;
    for ( unsigned int iter = 0; iter < length; iter++ )
    {
        xmmymm                               += ( valSet1[iter] - xMean ) * ( valSet2[iter] - yMean );
        xmmsq                                += pow( valSet1[iter] - xMean, 2.0 );
        ymmsq                                += pow( valSet2[iter] - yMean, 2.0 );
    }
    
    //======================================== Done
    double ret                                = xmmymm / ( sqrt(xmmsq) * sqrt(ymmsq) );
    if ( std::isnan (ret) )
    {
        return ( 0.0 );
    }
    return ( ret );
    
}

/*! \brief Function to multiply two complex numbers.
 
 This function takes pointers to the real and imaginary parst of two complex numbers and
 returns the result of their multiplication.
 
 \param[in] r1 Pointer to the real value of number 1.
 \param[in] i1 Pointer to the imaginary value of number 1.
 \param[in] r2 Pointer to the real value of number 2.
 \param[in] i2 Pointer to the imaginary value of number 2.
 \param[out] X The result of multiplication of the two complex numbers.
 
 \warning This function only works with double precision.
 \warning This is an internal function which should not be used by the user.
 */
std::array<double,2> ProSHADE_internal_misc::complexMultiplication ( double* r1,
                                                                     double* i1,
                                                                     double* r2,
                                                                     double* i2 )
{
    //======================================== Initialisation
    std::array<double,2> ret;
    
    //======================================== Multiplication
    ret[0]                                    = (*r1)*(*r2) - (*i1)*(*i2);
    ret[1]                                    = (*r1)*(*i2) + (*i1)*(*r2);
    
    //======================================== Return
    return ( ret );
    
}

/*! \brief Function to multiply two complex numbers where the second number is conjugated before the multiplication.
 
 This function takes pointers to the real and imaginary parst of two complex numbers and
 returns the result of their multiplication after the second number has been conjugated.
 
 \param[in] r1 Pointer to the real value of number 1.
 \param[in] i1 Pointer to the imaginary value of number 1.
 \param[in] r2 Pointer to the real value of number 2.
 \param[in] i2 Pointer to the imaginary value of number 2.
 \param[out] X The result of multiplication of the two complex numbers after the second number conjugate has been obtained and used.
 
 \warning This function only works with double precision.
 \warning This is an internal function which should not be used by the user.
 */
std::array<double,2> ProSHADE_internal_misc::complexMultiplicationConjug ( double* r1,
                                                                           double* i1,
                                                                           double* r2,
                                                                           double* i2 )
{
    //======================================== Initialisation
    std::array<double,2> ret;
    
    //======================================== Multiplication
    ret[0]                                    =  (*r1)*(*r2) + (*i1)*(*i2);
    ret[1]                                    = -(*r1)*(*i2) + (*i1)*(*r2);
    
    //======================================== Return
    return ( ret );
    
}

/*! \brief Function to multiply two complex numbers, returning only the real part of the result.
 
 This function takes pointers to the real and imaginary parst of two complex numbers and
 returns the result of their multiplication, returning only the real part of the result.
 
 \param[in] r1 Pointer to the real value of number 1.
 \param[in] i1 Pointer to the imaginary value of number 1.
 \param[in] r2 Pointer to the real value of number 2.
 \param[in] i2 Pointer to the imaginary value of number 2.
 \param[out] X The real part of the result of multiplication of the two complex numbers.
 
 \warning This function only works with double precision.
 \warning This is an internal function which should not be used by the user.
 */
double ProSHADE_internal_misc::complexMultiplicationReal ( double* r1,
                                                           double* i1,
                                                           double* r2,
                                                           double* i2 )
{
    //======================================== Return
    return ( (*r1)*(*r2) - (*i1)*(*i2) );
    
}

/*! \brief Function to multiply two complex numbers where the second number is conjugated before the multiplication, returning only the real part of the result.
 
 This function takes pointers to the real and imaginary parst of two complex numbers and
 returns the result of their multiplication after the second number has been conjugated,
 returning only the real part of the result.
 
 \param[in] r1 Pointer to the real value of number 1.
 \param[in] i1 Pointer to the imaginary value of number 1.
 \param[in] r2 Pointer to the real value of number 2.
 \param[in] i2 Pointer to the imaginary value of number 2.
 \param[out] X The real part of result of multiplication of the two complex numbers after the second number conjugate has been obtained and used.
 
 \warning This function only works with double precision.
 \warning This is an internal function which should not be used by the user.
 */
double ProSHADE_internal_misc::complexMultiplicationConjugReal ( double* r1,
                                                                 double* i1,
                                                                 double* r2,
                                                                 double* i2 )
{
    //======================================== Return
    return ( (*r1)*(*r2) + (*i1)*(*i2) );
    
}

/*! \brief This function computes the RRP matrices, which are required for the computation of the energy levels descriptor.
 
 This function computes the SUM_OVER_L { c_{l,m}(r) * c_{l,m'}(r')) } products of the spherical harmonics coefficients and saves them in
 in a set of matrices with dimmensions r and r' (therefore referred to as RRP matrices). The RRP matrices are the basis of the descriptor
 and can be pre-computed for each structure independently.
 
 \param[in] settings The settings object allowing to write the HTML report, if need be.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_data::precomputeRotInvDescriptor ( ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Sanity check
    if ( !this->_sphericalCoefficientsComputed )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file " << this->_inputFileName << " !!! Attempted to pre-compute RRP matrices for RotInv descriptor before computing the spherical harmonics decomposition. Call the getSphericalHarmonicsCoeffs function BEFORE the precomputeRotInvDescriptor function." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Attempted to pre-compute RRP matrices for RotInv descriptor before computing the spherical harmonics decomposition. This looks like an internal bug, please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Initialise internal values
    this->_rrpMatrices                        = new double** [this->_bandwidthLimit];
    for ( unsigned int bwIt = 0; bwIt < this->_bandwidthLimit; bwIt++ )
    {
        //==================================== Odd bands are 0, so just ignore them
        if ( !this->_keepOrRemove ) { if ( ( bwIt % 2 ) != 0 ) { continue; } }
            
        this->_rrpMatrices[bwIt]              = new double*  [this->_noShellsWithData];
        for ( unsigned int shIt = 0; shIt < this->_noShellsWithData; shIt++ )
        {
            this->_rrpMatrices[bwIt][shIt]    = new double   [this->_noShellsWithData];
        }
    }
    
    //======================================== Initialise local variables
    double descValR                           = 0.0;
    unsigned int arrPos                       = 0;
    
    //======================================== For each band (l)
    for ( unsigned int band = 0; band < this->_bandwidthLimit; band++ )
    {
        //==================================== Ignore odd bands, they are always 0
        if ( !this->_keepOrRemove ) { if ( band % 2 == 1 ) { continue; } }
        
        //==================================== For each unique shell couple
        for ( unsigned int shell1 = 0; shell1 < this->_noShellsWithData; shell1++ )
        {
            for ( unsigned int shell2 = 0; shell2 < this->_noShellsWithData; shell2++ )
            {
                //============================ Make use of symmetry
                if ( shell1 > shell2 ) { continue; }
                
                //============================ Initialise
                descValR                      = 0.0;
                
                //============================ Sum over order (m)
                for ( unsigned int order = 0; order < static_cast<unsigned int>  ( ( 2 * band ) + 1 ); order++ )
                {
                    arrPos                    = seanindex (  order-band, band, this->_bandwidthLimit );
                    descValR                 += ( this->_realSHCoeffs[shell1][arrPos] * this->_realSHCoeffs[shell2][arrPos] ) +
                                                ( this->_imagSHCoeffs[shell1][arrPos] * this->_imagSHCoeffs[shell2][arrPos] );
                }
                
                //============================ Save the matrices
                this->_rrpMatrices[band][shell1][shell2] = descValR;
                this->_rrpMatrices[band][shell2][shell1] = descValR;
            }
        }
    }
    
    //======================================== Done
    this->_rrpMatricesPrecomputed             = true;
    return ;
    
}

/*! \brief This function computes the energy level descriptor value from the first structure to all remaining structures.
 
 This function computes the Pearson's correlation coefficient between the weighted RRP matrices of two structures and
 proceeds to compute the energy level descriptor from these.
 
 \param[in] verbose Int value stating how loud standard output should be.
 \param[in] settings The settings contained with inforamation about printing into the HTML report file, if need be.
 \param[out] X A vector of energy level distances from the first structure to all the other structeres.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector<double> ProSHADE_internal::ProSHADE_compareOneAgainstAll::getEnergyLevelsDistance ( int verbose, ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Sanity check
    if ( !this->one->_rrpMatricesPrecomputed )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file compatison !!! Attempted to get energy level distance for structure objects where at least one of them did not have the RotInv pre-computed. Call the precomputeRotInvDescriptor on all structure objects before giving them to ProSHADE_compareOneAgainstAll constructor." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Attempted to pre-compute RRP matrices for RotInv descriptor before computing the spherical harmonics decomposition. This looks like an internal bug, please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( this->all->size() ); iter++ )
    {
        if ( !this->all->at(iter)->_rrpMatricesPrecomputed )
        {
            std::cerr << "!!! ProSHADE ERROR !!! Error in file compatison !!! Attempted to get energy level distance for structure objects where at least one of them did not have the RotInv pre-computed. Call the precomputeRotInvDescriptor on all structure objects before giving them to ProSHADE_compareOneAgainstAll constructor." << std::endl;
            
            if ( settings->htmlReport )
            {
                std::stringstream hlpSS;
                hlpSS << "<font color=\"red\">" << "Attempted to get energy level distance for structure objects where at least one of them did not have the RotInv pre-computed. This looks like an internal bug, please report this case." << "</font>";
                rvapi_set_text                    ( hlpSS.str().c_str(),
                                                   "ProgressSection",
                                                   settings->htmlReportLineProgress,
                                                   1,
                                                   1,
                                                   1 );
                settings->htmlReportLineProgress += 1;
                rvapi_flush                       ( );
            }
            
            exit ( -1 );
        }
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> Sanity checks passed." << std::endl;
    }
    
    //======================================== If already done, do not repeat :-)
    if ( this->_energyLevelsComputed )
    {
        return ( this->_distancesEnergyLevels );
    }
    
    //======================================== Initialise local variables
    unsigned int arrIter                      = 0;
    bool ignoreThisL                          = false;
    std::vector<double> bandDists;
    this->_distancesEnergyLevels              = std::vector<double> ( static_cast<int> ( this->all->size() ) );
    
    for ( unsigned int strIt = 0; strIt < static_cast<unsigned int> ( this->all->size() ); strIt++ )
    {
        if ( verbose > 3 )
        {
            std::cout << ">>>>>>>> Now computing distance for structure 0 against structure " << strIt+1 << " ." << std::endl;
        }
        
        //==================================== Clean
        bandDists.clear                       ( );
        
        //==================================== Set up
        unsigned int minShellsToUse           = std::min ( this->one->_noShellsWithData, this->all->at(strIt)->_noShellsWithData );
        unsigned int bandLimit                = std::min ( this->one->_bandwidthLimit, this->all->at(strIt)->_bandwidthLimit );
        
        //==================================== Initialise local variables
        double *str1Vals                      = new double[minShellsToUse * minShellsToUse];
        double *str2Vals                      = new double[minShellsToUse * minShellsToUse];
        
        //==================================== For each band (l)
        for ( unsigned int band = 0; band < bandLimit; band++ )
        {
            //================================ Check if this band is to be used
            if ( !this->_keepOrRemove ) { if ( band % 2 != 0 ) { continue; } }
            
            ignoreThisL                       = false;
            for ( unsigned int igIt = 0; igIt < static_cast<unsigned int> ( this->_lsToIgnore.size() ); igIt++ ) { if ( static_cast<unsigned int> ( this->_lsToIgnore.at(igIt) ) == band ) { ignoreThisL = true; } }
            if  ( ignoreThisL )  { continue; }
            
            //================================ Reset local counter
            arrIter                           = 0;
            
            //================================ For each shell pair
            for ( unsigned int shell1 = 0; shell1 < minShellsToUse; shell1++ )
            {
                for ( unsigned int shell2 = 0; shell2 < minShellsToUse; shell2++ )
                {
                    str1Vals[arrIter]         = this->one->_rrpMatrices[band][shell1][shell2] *
                                                pow ( static_cast<double> ( shell1 ), this->_matrixPowerWeight ) *
                                                pow ( static_cast<double> ( shell2 ), this->_matrixPowerWeight );
                    str2Vals[arrIter]         = this->all->at(strIt)->_rrpMatrices[band][shell1][shell2] *
                                                pow ( static_cast<double> ( shell1 ), this->_matrixPowerWeight ) *
                                                pow ( static_cast<double> ( shell2 ), this->_matrixPowerWeight );
                    
                    arrIter                  += 1;
                }
            }
            
            //================================ Get Pearson's Correlation Coefficient
            bandDists.emplace_back ( ProSHADE_internal_misc::pearsonCorrCoeff ( str1Vals, str2Vals, ( minShellsToUse * minShellsToUse ) ) );
        }
        
        //==================================== Get distance
        this->_distancesEnergyLevels.at( strIt )  = static_cast<double> ( std::accumulate ( bandDists.begin(), bandDists.end(), 0.0 ) ) / static_cast<double> ( bandDists.size() );
        
        //==================================== Clean up
        delete[] str1Vals;
        delete[] str2Vals;
        
        if ( verbose > 2 )
        {
            std::cout << ">>>>> Distance for structure 0 against structure " << strIt+1 << " complete." << std::endl;
        }
    }
    
    //======================================== Done
    this->_energyLevelsComputed               = true;
    
    //======================================== Return
    return ( this->_distancesEnergyLevels );
    
}

/*! \brief This function computes the E matrices required for the trace sigma descriptor, the rotation function descriptor and SO3 transform.
 
 This function computes the integral over r for the c1_{l,m} * c2_{l,m} spherical harmonics values. These are used by all
 three of the following: the trace sigma descriptor, the SO3 transform and the full rotation function. As such, there are
 two copies of this function, one for the symmetry detection class and one for the distances computation; this is the version
 for the symmetry detection.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_comparePairwise::precomputeTrSigmaDescriptor ( )
{
    //======================================== Figure required number of bands
    unsigned int numberBandsToUse             = 0;
    bool toBeIgnored                          = false;
    
    for ( unsigned int bandIter = 0; bandIter < this->_bandwidthLimit; bandIter++ )
    {
        //==================================== Ignore odd l's as they are 0 anyway
        if ( !this->_keepOrRemove ) { if ( ( bandIter % 2 ) != 0 ) { continue; } }
        
        //==================================== Ignore l's designated to be ignored
        toBeIgnored                           = false;
        for ( unsigned int igIt = 0; igIt < static_cast<unsigned int> ( this->_lsToIgnore.size() ); igIt++ ) { if ( bandIter == static_cast<unsigned int> ( this->_lsToIgnore.at(igIt) ) ) { toBeIgnored = true; } }
        if ( toBeIgnored ) { continue; }
        
        //==================================== This band will be used!
        numberBandsToUse                     += 1;
    }
    
    //======================================== Initialise local variables
    std::vector<double> radiiVals1;
    std::vector<double> radiiVals2;
    std::vector< std::array<double,2> > radiiVals;
    std::array<double,2> arrVal1;
    unsigned int index1                       = 0;
    unsigned int index2                       = 0;
    unsigned int bandUsageIndex               = 0;
    double correlNormFactor                   = 0.0;
    
    //======================================== Initialise internal values
    this->_trSigmaWeights                     = std::vector<double> ( 2 );
    this->_trSigmaWeights.at(0)               = 0.0;
    this->_trSigmaWeights.at(1)               = 0.0;
    
    this->_trSigmaEMatrix                     = new std::array<double,2>** [numberBandsToUse];
    for ( unsigned int bandIter = 0; bandIter < this->_bandwidthLimit; bandIter++ )
    {
        //==================================== Ignore odd l's as they are 0 anyway
        if ( !this->_keepOrRemove ) { if ( ( bandIter % 2 ) != 0 ) { continue; } }
        
        //==================================== Ignore l's designated to be ignored
        toBeIgnored                           = false;
        for ( unsigned int igIt = 0; igIt < static_cast<unsigned int> ( this->_lsToIgnore.size() ); igIt++ ) { if ( bandIter == static_cast<unsigned int> ( this->_lsToIgnore.at(igIt) ) ) { toBeIgnored = true; } }
        if ( toBeIgnored ) { continue; }
        
        this->_trSigmaEMatrix[bandUsageIndex] = new std::array<double,2>*  [static_cast<unsigned int> ( ( bandIter * 2 ) + 1 )];
        for ( unsigned int lIter = 0; lIter < static_cast<unsigned int> ( ( bandIter * 2 ) + 1 ); lIter++ )
        {
            this->_trSigmaEMatrix[bandUsageIndex][lIter] = new std::array<double,2> [static_cast<unsigned int> ( ( bandIter * 2 ) + 1 )];
        }
        
        bandUsageIndex                       += 1;
    }
    
    //======================================== For each band (l)
    bandUsageIndex                            = 0;
    for ( unsigned int bandIter = 0; bandIter < this->_bandwidthLimit; bandIter++ )
    {
        //==================================== Ignore odd l's as they are 0 anyway
        if ( !this->_keepOrRemove ) { if ( ( bandIter % 2 ) != 0 ) { continue; } }
        
        //==================================== Ignore l's designated to be ignored
        toBeIgnored                           = false;
        for ( unsigned int igIt = 0; igIt < static_cast<unsigned int> ( this->_lsToIgnore.size() ); igIt++ ) { if ( bandIter == static_cast<unsigned int> ( this->_lsToIgnore.at(igIt) ) ) { toBeIgnored = true; } }
        if ( toBeIgnored ) { continue; }
        
        //==================================== For each order (m)
        for ( unsigned int orderIter = 0; orderIter < ( ( bandIter * 2 ) + 1 ); orderIter++ )
        {
            //================================ Clear values for weights
            radiiVals1.clear                  ( );
            radiiVals2.clear                  ( );
            radiiVals1                        = std::vector<double> ( this->_noShellsObj1 );
            radiiVals2                        = std::vector<double> ( this->_noShellsObj2 );
            
            //================================ Get index for m
            index1                            = seanindex (  orderIter-bandIter, bandIter, this->_bandwidthLimit );
            
            //================================ For each radius, deal with weights
            for ( unsigned int radiusIter  = 0; radiusIter < this->_maxShellsToUse; radiusIter++ )
            {
                if ( radiusIter < this->_noShellsObj1  )
                {
                    //======================== Multiply with itself - i.e. square
                    radiiVals1.at(radiusIter) = ProSHADE_internal_misc::complexMultiplicationConjugReal ( &this->_obj1RealCoeffs[radiusIter][index1],
                                                                                                          &this->_obj1ImagCoeffs[radiusIter][index1],
                                                                                                          &this->_obj1RealCoeffs[radiusIter][index1],
                                                                                                          &this->_obj1ImagCoeffs[radiusIter][index1] );
                    
                    //======================== Apply the integral r^2
                    radiiVals1.at(radiusIter)*= pow ( ( static_cast<double> ( radiusIter + 1 ) * this->_shellSpacing ), 2.0 );
                }

                if ( radiusIter < this->_noShellsObj2 )
                {
                    //======================== Multiply with itself - i.e. square
                    radiiVals2.at(radiusIter) = ProSHADE_internal_misc::complexMultiplicationConjugReal ( &this->_obj2RealCoeffs[radiusIter][index1],
                                                                                                          &this->_obj2ImagCoeffs[radiusIter][index1],
                                                                                                          &this->_obj2RealCoeffs[radiusIter][index1],
                                                                                                          &this->_obj2ImagCoeffs[radiusIter][index1] );
                    
                    //======================== Apply the integral r^2
                    radiiVals2.at(radiusIter)*= pow ( ( static_cast<double> ( radiusIter + 1 ) * this->_shellSpacing ), 2.0 );
                }
            }
            
            //================================ Integrate weights
            this->_trSigmaWeights.at(0)      += this->gl20IntRR ( &radiiVals1 );
            this->_trSigmaWeights.at(1)      += this->gl20IntRR ( &radiiVals2 );
            
            //================================ For each combination of m and m' for E matrices
            for ( unsigned int order2Iter = 0; order2Iter < ( ( bandIter * 2 ) + 1 ); order2Iter++ )
            {
                //============================ Set index for order 2 (i.e. m' in E_{l,m,m'})
                index2                        = seanindex (  order2Iter-bandIter, bandIter, this->_bandwidthLimit );
                
                //============================ Clear the vector to integrate over
                radiiVals.clear               ( );
                radiiVals                     = std::vector< std::array<double,2> > ( this->_minShellsToUse );
                
                //============================ Find the c*c values for different radii
                for ( unsigned int radiusIter = 0; radiusIter < this->_minShellsToUse; radiusIter++ )
                {
                    //======================== Multiply coeffs
                    radiiVals.at(radiusIter)  = ProSHADE_internal_misc::complexMultiplicationConjug ( &this->_obj1RealCoeffs[radiusIter][index1],
                                                                                                      &this->_obj1ImagCoeffs[radiusIter][index1],
                                                                                                      &this->_obj2RealCoeffs[radiusIter][index2],
                                                                                                      &this->_obj2ImagCoeffs[radiusIter][index2] );
                    
                    //======================== Apply r^2 integral weight
                    radiiVals.at(radiusIter)[0] *= pow ( ( static_cast<double> ( radiusIter + 1 ) * this->_shellSpacing ), 2.0 );
                    radiiVals.at(radiusIter)[1] *= pow ( ( static_cast<double> ( radiusIter + 1 ) * this->_shellSpacing ), 2.0 );
                }
                
                //============================ Integrate over all radii using X-point Gauss-Legendre
                arrVal1                       = this->gl20IntCR ( &radiiVals );
                
                //============================ Save the result into E matrices
                this->_trSigmaEMatrix[bandUsageIndex][orderIter][order2Iter][0] = arrVal1[0];
                this->_trSigmaEMatrix[bandUsageIndex][orderIter][order2Iter][1] = arrVal1[1];
            }
        }
        bandUsageIndex                       += 1;
    }
    
    //======================================== For each band (l), normalise using the correlation-like formula
    bandUsageIndex                            = 0;
    correlNormFactor                          = sqrt ( this->_trSigmaWeights.at(0) * this->_trSigmaWeights.at(1) );

    for ( unsigned int bandIter = 0; bandIter < this->_bandwidthLimit; bandIter++ )
    {
        //==================================== Ignore odd l's as they are 0 anyway
        if ( !this->_keepOrRemove ) { if ( ( bandIter % 2 ) != 0 ) { continue; } }
        
        //==================================== Ignore l's designated to be ignored
        toBeIgnored                           = false;
        for ( unsigned int igIt = 0; igIt < static_cast<unsigned int> ( this->_lsToIgnore.size() ); igIt++ ) { if ( bandIter == static_cast<unsigned int> ( this->_lsToIgnore.at(igIt) ) ) { toBeIgnored = true; } }
        if ( toBeIgnored ) { continue; }
        
        //==================================== For each combination of m and m' for E matrices
        for ( unsigned int order1Iter = 0; order1Iter < ( ( bandIter * 2 ) + 1 ); order1Iter++ )
        {
            for ( unsigned int order2Iter = 0; order2Iter < ( ( bandIter * 2 ) + 1 ); order2Iter++ )
            {
                //============================ Normalisation using the correlation formula
                this->_trSigmaEMatrix[bandUsageIndex][order1Iter][order2Iter][0] /= correlNormFactor;
                this->_trSigmaEMatrix[bandUsageIndex][order1Iter][order2Iter][1] /= correlNormFactor;
            }
        }
        bandUsageIndex                       += 1;
    }

    //======================================== Done
    this->_trSigmaPreComputed                 = true;
    
}

/*! \brief This function computes the E matrices required for the trace sigma descriptor, the rotation function descriptor and SO3 transform.
 
 This function computes the integral over r for the c1_{l,m} * c2_{l,m} spherical harmonics values. These are used by all
 three of the following: the trace sigma descriptor, the SO3 transform and the full rotation function. As such, there are
 two copies of this function, one for the symmetry detection class and one for the distances computation; this is the version
 for the distances computation.
 
 \param[in] shellSpacing How far apart individual shells are in angstroms?
 \param[in] glIntegOrderVec Pointer to the vector of integration values for multiple structure comparisons.
 \param[in] settings ProSHADE_settings container with infotmation about HTML reporting.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_compareOneAgainstAll::precomputeTrSigmaDescriptor ( double shellSpacing, std::vector<unsigned int>* glIntegOrderVec, ProSHADE::ProSHADE_settings* settings )
{    
    //======================================== Initialise local variables
    std::vector<double> radiiVals1;
    std::vector<double> radiiVals2;
    std::vector< std::array<double,2> > radiiVals;
    std::array<double,2> arrVal1;
    std::vector<double> glAbscissas;
    std::vector<double> glWeights;
    unsigned int glIntegrationOrder           = 0;
    unsigned int index11                      = 0;
    unsigned int index12                      = 0;
    unsigned int index21                      = 0;
    unsigned int index22                      = 0;
    double correlNormFactor                   = 0.0;
    unsigned int bandUsageIndex               = 0;
    unsigned int fileUsageIndex               = 0;
    unsigned int noBandsUsed                  = 0;
    unsigned int localComparisonBandLim       = 0;
    bool toBeIgnored                          = false;
    unsigned int maxShellsToUse               = 0;
    unsigned int minShellsToUse               = 0;
    bool firstDoneAlready                     = false;
    
    //======================================== Initialise internal values
    std::vector<double> trSigmaWeights        = std::vector<double> ( this->all->size() + 1 );
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( trSigmaWeights.size() ); iter++ )
    {
        trSigmaWeights.at(iter)               = 0.0;
    }
    
    //======================================== For each comparison against the one
    fileUsageIndex                            = 0;
    for ( unsigned int strIt = 0; strIt < static_cast<unsigned int> ( this->all->size() ); strIt++ )
    {
        //==================================== If not needed, do not compute
        if ( this->_enLevelsDoNotFollow.at(strIt) == 1 ) { continue; }
        
        //==================================== Figure required number of bands
        localComparisonBandLim                = std::min( this->one->_bandwidthLimit, this->all->at(strIt)->_bandwidthLimit );
        for ( unsigned int bandIter = 0; bandIter < localComparisonBandLim ; bandIter++ )
        {
            //================================ Ignore odd l's as they are 0 anyway
            if ( !this->_keepOrRemove ) { if ( ( bandIter % 2 ) != 0 ) { continue; } }
            
            //================================ Ignore l's designated to be ignored
            toBeIgnored                       = false;
            for ( unsigned int igIt = 0; igIt < static_cast<unsigned int> ( this->_lsToIgnore.size() ); igIt++ ) { if ( bandIter == static_cast<unsigned int> ( this->_lsToIgnore.at(igIt) ) ) { toBeIgnored = true; } }
            if ( toBeIgnored ) { continue; }
            
            //================================ This band will be used!
            noBandsUsed                      += 1;
        }
        
        //==================================== Allocate memory
        bandUsageIndex                        = 0;
        this->_EMatrices.emplace_back         ( std::vector< std::vector< std::vector< std::array<double,2> > > > ( noBandsUsed ) );
        for ( unsigned int bandIter = 0; bandIter < localComparisonBandLim; bandIter++ )
        {
            //================================ Ignore odd l's as they are 0 anyway
            if ( !this->_keepOrRemove ) { if ( ( bandIter % 2 ) != 0 ) { continue; } }
            
            //================================ Ignore l's designated to be ignored
            toBeIgnored                       = false;
            for ( unsigned int igIt = 0; igIt < static_cast<unsigned int> ( this->_lsToIgnore.size() ); igIt++ ) { if ( bandIter == static_cast<unsigned int> ( this->_lsToIgnore.at(igIt) ) ) { toBeIgnored = true; } }
            if ( toBeIgnored ) { continue; }
            
            //================================ Allocate vector of order1
            this->_EMatrices.at(fileUsageIndex).at(bandUsageIndex) = std::vector< std::vector< std::array<double,2> > > ( static_cast<unsigned int> ( ( bandIter * 2 ) + 1 ) );
            
            //================================ Allocate vector of order2
            for ( unsigned int lIter = 0; lIter < static_cast<unsigned int> ( ( bandIter * 2 ) + 1 ); lIter++ )
            {
                this->_EMatrices.at(fileUsageIndex).at(bandUsageIndex).at(lIter) = std::vector< std::array<double,2> > ( static_cast<unsigned int> ( ( bandIter * 2 ) + 1 ) );
            }
            
            bandUsageIndex                   += 1;
        }
        
        //==================================== Set up
        maxShellsToUse                        = std::max ( this->one->_noShellsWithData, this->all->at(strIt)->_noShellsWithData );
        minShellsToUse                        = std::min ( this->one->_noShellsWithData, this->all->at(strIt)->_noShellsWithData );
        
        //==================================== Compute weights and abscissas for GL Integration
        glIntegrationOrder                    = std::max ( glIntegOrderVec->at(0), glIntegOrderVec->at(strIt+1) );
        glAbscissas                           = std::vector<double> ( glIntegrationOrder );
        glWeights                             = std::vector<double> ( glIntegrationOrder );
        ProSHADE_internal_legendre::getLegendreAbscAndWeights ( glIntegrationOrder, &glAbscissas, &glWeights, settings );
        
        //==================================== For each band (l)
        bandUsageIndex                        = 0;
        for ( unsigned int bandIter = 0; bandIter < localComparisonBandLim; bandIter++ )
        {            
            //================================ Ignore odd l's as they are 0 anyway
            if ( !this->_keepOrRemove ) { if ( ( bandIter % 2 ) != 0 ) { continue; } }
            
            //================================ Ignore l's designated to be ignored
            toBeIgnored                       = false;
            for ( unsigned int igIt = 0; igIt < static_cast<unsigned int> ( this->_lsToIgnore.size() ); igIt++ ) { if ( bandIter == static_cast<unsigned int> ( this->_lsToIgnore.at(igIt) ) ) { toBeIgnored = true; } }
            if ( toBeIgnored ) { continue; }
            
            //================================ For each order (m)
            for ( unsigned int orderIter      = 0; orderIter < ( ( bandIter * 2 ) + 1 ); orderIter++ )
            {
                //============================ Clear values for weights
                radiiVals1.clear              ( );
                radiiVals2.clear              ( );
                radiiVals1                    = std::vector<double> ( this->one->_noShellsWithData );
                radiiVals2                    = std::vector<double> ( this->all->at(strIt)->_noShellsWithData );
                
                //============================ Get index for m
                index11                       = seanindex (  orderIter-bandIter, bandIter, this->one->_bandwidthLimit );
                index12                       = seanindex (  orderIter-bandIter, bandIter, this->all->at(strIt)->_bandwidthLimit );
                
                //============================ For each radius, deal with weights
                for ( unsigned int radiusIter = 0; radiusIter < maxShellsToUse; radiusIter++ )
                {
                    if ( !firstDoneAlready )
                    {
                        if ( radiusIter < this->one->_noShellsWithData  )
                        {
                            //================ Multiply with itself - i.e. square
                            radiiVals1.at(radiusIter)  = ProSHADE_internal_misc::complexMultiplicationConjugReal ( &this->one->_realSHCoeffs[radiusIter][index11],
                                                                                                                   &this->one->_imagSHCoeffs[radiusIter][index11],
                                                                                                                   &this->one->_realSHCoeffs[radiusIter][index11],
                                                                                                                   &this->one->_imagSHCoeffs[radiusIter][index11] );
                            
                            //================ Apply the integral r^2
                            radiiVals1.at(radiusIter) *= pow ( ( static_cast<double> ( radiusIter + 1 ) * shellSpacing ), 2.0 );
                        }
                    }
                    
                    if ( radiusIter < this->all->at(strIt)->_noShellsWithData )
                    {
                        //==================== Multiply with itself - i.e. square
                        radiiVals2.at(radiusIter) = ProSHADE_internal_misc::complexMultiplicationConjugReal ( &this->all->at(strIt)->_realSHCoeffs[radiusIter][index12],
                                                                                                              &this->all->at(strIt)->_imagSHCoeffs[radiusIter][index12],
                                                                                                              &this->all->at(strIt)->_realSHCoeffs[radiusIter][index12],
                                                                                                              &this->all->at(strIt)->_imagSHCoeffs[radiusIter][index12] );
                        
                        //==================== Apply the integral r^2
                        radiiVals2.at(radiusIter) *= pow ( ( static_cast<double> ( radiusIter + 1 ) * shellSpacing ), 2.0 );
                    }
                }
                
                //============================ Integrate weights
                if ( !firstDoneAlready ) { trSigmaWeights.at(0) += this->glIntRR ( &radiiVals1, shellSpacing, &glAbscissas, &glWeights ); }
                trSigmaWeights.at(strIt+1)   += this->glIntRR ( &radiiVals2, shellSpacing, &glAbscissas, &glWeights );
                
                //============================ For each combination of m and m' for E matrices
                for ( unsigned int order2Iter = 0; order2Iter < ( ( bandIter * 2 ) + 1 ); order2Iter++ )
                {
                    //======================== Set index for order 2 (i.e. m' in E_{l,m,m'})
                    index21                   = seanindex (  orderIter-bandIter, bandIter, this->one->_bandwidthLimit );
                    index22                   = seanindex (  order2Iter-bandIter, bandIter, this->all->at(strIt)->_bandwidthLimit );
                    
                    //======================== Clear the vector to integrate over
                    radiiVals.clear           ( );
                    radiiVals                 = std::vector< std::array<double,2> > ( minShellsToUse );
                    
                    //======================== Find the c*c values for different radii
                    for ( unsigned int radiusIter = 0; radiusIter < minShellsToUse; radiusIter++ )
                    {
                        //==================== Multiply coeffs
                        radiiVals.at(radiusIter) = ProSHADE_internal_misc::complexMultiplicationConjug ( &this->one->_realSHCoeffs[radiusIter][index21],
                                                                                                         &this->one->_imagSHCoeffs[radiusIter][index21],
                                                                                                         &this->all->at(strIt)->_realSHCoeffs[radiusIter][index22],
                                                                                                         &this->all->at(strIt)->_imagSHCoeffs[radiusIter][index22] );
                        
                        //==================== Apply r^2 integral weight
                        radiiVals.at(radiusIter)[0] *= pow ( ( static_cast<double> ( radiusIter + 1 ) * shellSpacing ), 2.0 );
                        radiiVals.at(radiusIter)[1] *= pow ( ( static_cast<double> ( radiusIter + 1 ) * shellSpacing ), 2.0 );
                    }
                    
                    //======================== Integrate over all radii using 20-point Gauss-Legendre
                    arrVal1 = this->glIntCR ( &radiiVals, shellSpacing, &glAbscissas, &glWeights );
                    
                    //======================== Save the result into E matrices
                    this->_EMatrices.at(fileUsageIndex).at(bandUsageIndex).at(orderIter).at(order2Iter)[0] = arrVal1[0];
                    this->_EMatrices.at(fileUsageIndex).at(bandUsageIndex).at(orderIter).at(order2Iter)[1] = arrVal1[1];
                }
            }
            bandUsageIndex                   += 1;
        }
        
        //==================================== For each band (l), normalise using the correlation-like formula
        bandUsageIndex                        = 0;
        correlNormFactor                      = sqrt ( trSigmaWeights.at(0) * trSigmaWeights.at(strIt+1) );
        
        for ( unsigned int bandIter = 0; bandIter < localComparisonBandLim; bandIter++ )
        {
            //================================ Ignore odd l's as they are 0 anyway
            if ( !this->_keepOrRemove ) { if ( ( bandIter % 2 ) != 0 ) { continue; } }
            
            //================================ Ignore l's designated to be ignored
            toBeIgnored                       = false;
            for ( unsigned int igIt = 0; igIt < static_cast<unsigned int> ( this->_lsToIgnore.size() ); igIt++ ) { if ( bandIter == static_cast<unsigned int> ( this->_lsToIgnore.at(igIt) ) ) { toBeIgnored = true; } }
            if ( toBeIgnored ) { continue; }
            
            //================================ For each combination of m and m' for E matrices
            for ( unsigned int orderIter = 0; orderIter < ( ( bandIter  * 2 ) + 1 ); orderIter++  )
            {
                for ( unsigned int order2Iter = 0; order2Iter < ( ( bandIter * 2 ) + 1 ); order2Iter++ )
                {
                    //======================== Normalisation using the correlation formula
                    this->_EMatrices.at(fileUsageIndex).at(bandUsageIndex).at(orderIter).at(order2Iter)[0] /= correlNormFactor;
                    this->_EMatrices.at(fileUsageIndex).at(bandUsageIndex).at(orderIter).at(order2Iter)[1] /= correlNormFactor;
                }
            }
            bandUsageIndex                   += 1;
        }
        
        firstDoneAlready                      = true;
        fileUsageIndex                       += 1;
    }
    
    //======================================== Done
    this->_trSigmaPreComputed                 = true;
    
}

/*! \brief This function computes the trace sigma descriptor distances.
 
 This function basically takes the E matrix and calls LAPACK to decompose it onto the singular values. The returned singular
 values matrix then has its diagonal summed and this is the trace sigma descriptor in simplicity. The function does several other
 minor things, but they are not of much interest here.
 
 \param[in] verbose Int value stating how loud standard output should be.
 \param[in] settings ProSHADE_settings container class with information about the HTML report printing, if needed.
 \param[out] X A vector of double precision numbers which are the distances from the first to all remaining structures.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector<double> ProSHADE_internal::ProSHADE_compareOneAgainstAll::getTrSigmaDistance ( int verbose, ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Sanity check
    if ( !this->_trSigmaPreComputed )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file compatison !!! Attempted to get TrSigma distance without pre-computing the required data (E matrices). Call the precomputeTrSigmaDescriptor function BEFORE calling the getTrSigmaDistance function." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Attempted to get TrSigma distance without pre-computing the required data (E matrices). This looks like an internal bug, please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== If already done, do not repeat :-)
    if ( this->_trSigmaComputed )
    {
        return ( this->_distancesTraceSigma );
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> Sanity checks passed." << std::endl;
    }
    
    //======================================== Initialise local variables
    this->_distancesTraceSigma                = std::vector<double> ( this->all->size() );
    std::vector<double> hlpVec;
    unsigned int bandUsageIndex               = 0;
    bool toBeIgnored                          = false;
    unsigned int localComparisonBandLim       = 0;
    unsigned int fileUsageIndex               = 0;

    //======================================== For each comparison against the one
    for ( unsigned int strIt = 0; strIt < static_cast<unsigned int> ( this->all->size() ); strIt++ )
    {
        //===================================== If not needed, do not compute
        if ( this->_enLevelsDoNotFollow.at(strIt) == 1 ) { this->_distancesTraceSigma.at(strIt) = -999.9; continue; }
        
        if ( verbose > 3 )
        {
            std::cout << ">>>>>>>> Now computing distance between structure 0 and structure " << strIt+1 << " ." << std::endl;
        }
        
        //==================================== Set up
        bandUsageIndex                        = 0;
        localComparisonBandLim                = std::min( this->one->_bandwidthLimit, this->all->at(strIt)->_bandwidthLimit );
        this->_distancesTraceSigma.at(strIt)  = 0.0;
        
        //==================================== For each l (band)
        for ( unsigned int lIter = 0; lIter < localComparisonBandLim; lIter++ )
        {
            //================================ Ignore odd l's as they are 0 anyway
            if ( !this->_keepOrRemove ) { if ( ( lIter % 2 ) != 0 ) { continue; } }
            
            //================================ Ignore l's designated to be ignored
            toBeIgnored                       = false;
            for ( unsigned int igIt = 0; igIt < static_cast<unsigned int> ( this->_lsToIgnore.size() ); igIt++ ) { if ( lIter == static_cast<unsigned int> ( this->_lsToIgnore.at(igIt) ) ) { toBeIgnored = true; } }
            if ( toBeIgnored ) { continue; }
            
            //================================ Find the complex matrix SVD singular values
            hlpVec                            = this->getSingularValues ( fileUsageIndex, bandUsageIndex, static_cast<unsigned int> ( ( lIter * 2 ) + 1 ), settings );
            
            //================================ Now sum the trace
            for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( hlpVec.size() ); iter++  )
            {
                this->_distancesTraceSigma.at(strIt) += hlpVec.at(iter);
            }
            
            bandUsageIndex                   += 1;
        }
        
        fileUsageIndex                       += 1;
        
        if ( verbose > 2 )
        {
            std::cout << ">>>>> Distance between structure 0 and structure " << strIt+1 << " computed." << std::endl;
        }
    }
    
    //======================================== Done
    this->_trSigmaComputed                    = true;
    
    //======================================== Return
    return ( this->_distancesTraceSigma );
    
}

/*! \brief This function computes the full rotation function descriptor distances.
 
 Since it would require one extra array the size of the spherical harmonics array to hold the Wigner D * C
 (C is spherical harmonics coefficients matrix) matrix multiplication result, this function takes the already
 available E matrices and multiplies these with the Wigner D matrices, this saving space. Otherwise, this is
 the rotation function known from molecular replacement and the weighted sum of the results is then the distance.
 
 \param[in] settings ProSHADE_settings container class with information about the HTML report printing, if needed.
 \param[out] X A double precision number which is the distance.
 
 \warning This is an internal function which should not be used by the user.
 */
double ProSHADE_internal::ProSHADE_comparePairwise::getRotCoeffDistance ( ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Sanity check
    if ( !this->_wignerMatricesComputed )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file compatison !!! Attempted to get Full Rotation distance without pre-computing the Wigner D matrices. Call the generateWignerMatrices function BEFORE calling the getRotCoeffDistance function." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Attempted to get Full Rotation distance without pre-computing the Wigner D matrices. This looks like an internal bug, please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Initialise local variables
    bool toBeIgnored                          = false;
    double totSum                             = 0.0;
    unsigned int bandUsageIndex               = 0;
    
    //======================================== For each band
    for ( int bandIter = 0; bandIter < static_cast<int> ( this->_bandwidthLimit ); bandIter++ )
    {
        //==================================== Ignore odd l's as they are 0 anyway
        if ( !this->_keepOrRemove ) { if ( ( bandIter % 2 ) != 0 ) { continue; } }
        
        //==================================== Ignore l's designated to be ignored
        toBeIgnored                           = false;
        for ( unsigned int igIt = 0; igIt < static_cast<unsigned int> ( this->_lsToIgnore.size() ); igIt++ ) { if ( bandIter == static_cast<int> ( this->_lsToIgnore.at(igIt) ) ) { toBeIgnored = true; } }
        if ( toBeIgnored ) { continue; }
        
        //==================================== For each order1
        for ( int order1 = 0; order1 < static_cast<int> ( ( bandIter * 2 ) + 1 ); order1++ )
        {
            //================================ For each order2
            for ( int order2 = 0; order2 < static_cast<int> ( ( bandIter * 2 ) + 1 ); order2++ )
            {
                //============================ Multiply D_{l} * E_{l} and get sum over l of traces (i.e. just sum all together)
                totSum                       += ProSHADE_internal_misc::complexMultiplicationReal ( &this->_wignerMatrices.at(bandIter).at(order1).at(order2)[0],
                                                                                                    &this->_wignerMatrices.at(bandIter).at(order1).at(order2)[1],
                                                                                                    &this->_trSigmaEMatrix[bandUsageIndex][order2][order1][0],
                                                                                                    &this->_trSigmaEMatrix[bandUsageIndex][order2][order1][1] );
            }
        }
        
        //==================================== Prepare for next loop
        bandUsageIndex                       += 1;
    }
    
    //======================================== Done
    return ( totSum );
}

/*! \brief This function computes the full rotation function descriptor distances.
 
 Since it would require one extra array the size of the spherical harmonics array to hold the Wigner D * C
 (C is spherical harmonics coefficients matrix) matrix multiplication result, this function takes the already
 available E matrices and multiplies these with the Wigner D matrices, this saving space. Otherwise, this is
 the rotation function known from molecular replacement and the weighted sum of the results is then the distance.
 
 \param[in] verbose Int value stating how loud standard output should be.
 \param[in] settings The ProSHADE_settings class instance specifying how to write the HTML report output.
 \param[out] X A vector of double precision numbers which are the distances from the first structure to all remaining structures.
 
 \warning This is an internal function which should not be used by the user.
 */
std::vector<double> ProSHADE_internal::ProSHADE_compareOneAgainstAll::getRotCoeffDistance ( int verbose, ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Sanity check
    if ( !this->_wignerMatricesComputed )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file compatison !!! Attempted to get Full Rotation distance without pre-computing the Wigner D matrices. Call the generateWignerMatrices function BEFORE calling the getRotCoeffDistance function." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Attempted to get Full Rotation distance without pre-computing the Wigner D matrices. This looks like an internal bug, please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Save some time on re-runs
    if ( this->_fullDistComputed )
    {
        return ( this->_distancesFullRotation );
    }
    if ( verbose > 3 )
    {
        std::cout << ">>>>>>>> Sanity checks passed." << std::endl;
    }
    
    //======================================== Initialise local variables
    bool toBeIgnored                          = false;
    double totSum                             = 0.0;
    unsigned int bandUsageIndex               = 0;
    unsigned int fileUsageIndex               = 0;
    int localComparisonBandLim                = 0;
    
    for ( unsigned int strIt = 0; strIt < static_cast<unsigned int> ( this->all->size() ); strIt++ )
    {
        //==================================== If not needed, do not compute
        if ( this->_enLevelsDoNotFollow.at(strIt) == 1 ) { this->_distancesFullRotation.emplace_back( -999.9 ); continue; }
        if ( this->_trSigmaDoNotFollow.at(strIt)  == 1 ) { this->_distancesFullRotation.emplace_back( -999.9 ); fileUsageIndex += 1; continue; }
        
        if ( verbose > 3 )
        {
            std::cout << ">>>>>>>> Now computing distance between structure 0 and structure " << strIt+1 << " ." << std::endl;
        }
        
        //==================================== Find the appropriate bandwidth
        localComparisonBandLim                = std::min ( this->one->_bandwidthLimit, this->all->at(strIt)->_bandwidthLimit );
        
        //==================================== Initialise local variables
        toBeIgnored                           = false;
        totSum                                = 0.0;
        bandUsageIndex                        = 0;
        
        //==================================== For each band
        for ( int bandIter = 0; bandIter < static_cast<int> ( localComparisonBandLim ); bandIter++ )
        {
            //================================ Ignore odd l's as they are 0 anyway
            if ( !this->_keepOrRemove ) { if ( ( bandIter % 2 ) != 0 ) { continue; } }
            
            //================================ Ignore l's designated to be ignored
            toBeIgnored                       = false;
            for ( unsigned int igIt = 0; igIt < static_cast<unsigned int> ( this->_lsToIgnore.size() ); igIt++ ) { if ( bandIter == static_cast<int> ( this->_lsToIgnore.at(igIt) ) ) { toBeIgnored = true; } }
            if ( toBeIgnored ) { continue; }
            
            //================================ For each order1
            for ( int order1 = 0; order1 < static_cast<int> ( ( bandIter * 2 ) + 1 ); order1++ )
            {
                //============================ For each order2
                for ( int order2 = 0; order2 < static_cast<int> ( ( bandIter * 2 ) + 1 ); order2++ )
                {
                    //======================== Multiply D_{l} * E_{l} and get sum over l of traces (i.e. just sum all together)
                    totSum                   += ProSHADE_internal_misc::complexMultiplicationReal ( &this->_wignerMatrices.at(strIt).at(bandIter).at(order1).at(order2)[0],
                                                                                                    &this->_wignerMatrices.at(strIt).at(bandIter).at(order1).at(order2)[1],
                                                                                                    &this->_EMatrices.at(fileUsageIndex).at(bandUsageIndex).at(order2).at(order1)[0],
                                                                                                    &this->_EMatrices.at(fileUsageIndex).at(bandUsageIndex).at(order2).at(order1)[1] );
                }
            }
            
            //================================ Prepare for next loop
            bandUsageIndex                   += 1;
        }
        
        //==================================== Save
        this->_distancesFullRotation.emplace_back ( totSum );
        fileUsageIndex                       += 1;
        
        if ( verbose > 2 )
        {
            std::cout << ">>>>> Distance between structure 0 and structure " << strIt+1 << " computed." << std::endl;
        }
    }
    
    //======================================== Done
    this->_fullDistComputed                   = true;
    
    //======================================== Return
    return ( this->_distancesFullRotation );
}
