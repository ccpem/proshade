/*! \file ProSHADE_wigner.cpp
 \brief This file contains the functions required to compute Wigner D matrices for rotation in spherical harmonics space.
 
 This file contains two version of the same function; one for the detection of symmetries and one for computation
 of distances. This function uses part of the SOFT2.0 library (http://www.cs.dartmouth.edu/~geelong/soft/) and
 some direct calls to it to compute the Wigner D matrices, which have the property that if spherical harmonics
 coefficients are multiplied by these, the result is identical as rotating the structure and then computing the
 spherical harmonics; in other words, these are the rotation matrices in spherical harmonics space.
 
 This file is part of the ProSHADE library for calculating
 shape descriptors and symmetry operators of protein structures.
 This is a prototype code, which is by no means complete or fully
 tested. Its use is at your own risk only. There is no quarantee
 that the results are correct.
 
 \author    Michal Tykac
 \author    Garib N. Murshudov
 \version   0.6.6
 \date      JAN 2019
 */

//============================================ FFTW3 + SOFT
#ifdef __cplusplus
extern "C" {
#endif
#include <fftw3.h>
#include <wrap_fftw.h>
#include <makeweights.h>
#include <s2_primitive.h>
#include <s2_cospmls.h>
#include <s2_legendreTransforms.h>
#include <s2_semi_fly.h>
#include <rotate_so3_utils.h>
#include <utils_so3.h>
#include <soft_fftw.h>
#include <rotate_so3_fftw.h>
#ifdef __cplusplus
}
#endif

//============================================ RVAPI
#include <rvapi_interface.h>

//============================================ ProSHADE
#include "ProSHADE.h"
#include "ProSHADE_internal.h"

/*! \brief This function is responsible for computing the Wigner D matrices for symmetry detection purposes.
 
 This function makes use of the SOFT2.0 package functions and logic to compute the Wigner d matrices and consequently
 the Wigner D matrices, which it stores internally in the symmetry detection class. The main purpose of the Wigner D
 matrices is to rotate the spherical harmonics coefficients.
 
 \param[in] settings The ProSHADE_settings container class with information about how to write the HTML report.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_comparePairwise::generateWignerMatrices ( ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Sanity check
    if ( !this->_eulerAnglesFound )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file comparison !!! Attempted to get Wigner d matrices without computing the Euler angles first. Call the getEulerAngles function BEFORE the generateWignerMatrices function." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Attempted to get Wigner d matrices without computing the Euler angles first. This looks like an internal bug, please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }
    
    //======================================== Initialise local variables
    double       trigVals[2];
    unsigned int noOrders                    = 0;
    unsigned int arrConvIter                 = 0;
    double      *expARStart;
    double      *expAIStart;
    double      *expGRStart;
    double      *expGIStart;
    double       Dij;
    double       eARi;
    double       eAIi;
    double       eGRj;
    double       eGIj;
    double       iSign                        = 1.0;
    double       rSign                        = 1.0;
    
    double       angAlpha                     = this->_eulerAngles[0];
    double       angBeta                      = this->_eulerAngles[1];
    double       angGamma                     = this->_eulerAngles[2];
    
    if ( angAlpha < 0.0 ) { angAlpha = M_PI + ( angAlpha - M_PI ); }
    if ( angBeta  < 0.0 ) { angBeta  = M_PI + ( angBeta  - M_PI ); }
    if ( angGamma < 0.0 ) { angGamma = M_PI + ( angGamma - M_PI ); }
    
    //======================================== Initialise internal values
    this->_wignerMatrices                     = std::vector< std::vector< std::vector< std::array<double,2> > > > ( this->_bandwidthLimit );
    
    //======================================== Allocate memory
    double      *matIn                        = new double[static_cast<unsigned int> ( 4 * pow( this->_bandwidthLimit, 2.0 ) - 4 * this->_bandwidthLimit + 1 )];
    double      *matOut                       = new double[static_cast<unsigned int> ( 4 * pow( this->_bandwidthLimit, 2.0 ) - 4 * this->_bandwidthLimit + 1 )];
    double      *sqrts                        = new double[static_cast<unsigned int> ( 2 * this->_bandwidthLimit )];
    double      *workspace                    = new double[static_cast<unsigned int> ( 4 * pow( this->_bandwidthLimit, 2.0 ) )];
    double      *exponentAlphaReal            = new double[static_cast<unsigned int> ( 2 * this->_bandwidthLimit - 1 )];
    double      *exponentAlphaImag            = new double[static_cast<unsigned int> ( 2 * this->_bandwidthLimit - 1 )];
    double      *exponentGammaReal            = new double[static_cast<unsigned int> ( 2 * this->_bandwidthLimit - 1 )];
    double      *exponentGammaImag            = new double[static_cast<unsigned int> ( 2 * this->_bandwidthLimit - 1 )];

    //======================================== Compute the square roots
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( 2 * this->_bandwidthLimit ); iter++ )
    {
        sqrts[iter]                           = static_cast<double> ( sqrt ( static_cast<double> ( iter ) ) );
    }
    
    //======================================== Compute the trig values
    trigVals[0]                               = static_cast<double> ( cos ( 0.5 * -angBeta ) );
    trigVals[1]                               = static_cast<double> ( sin ( 0.5 * -angBeta ) );
    
    //======================================== Get alpha and gamma exponents
    genExp                                    ( this->_bandwidthLimit, angAlpha, exponentAlphaReal, exponentAlphaImag );
    genExp                                    ( this->_bandwidthLimit, angGamma, exponentGammaReal, exponentGammaImag );
    
    //======================================== For each band, find the Wigned d matrix
    for ( unsigned int bandIter = 0; bandIter < this->_bandwidthLimit; bandIter++ )
    {
        //==================================== Initialise
        noOrders                              =  2 * bandIter + 1;
        arrConvIter                           =  0;
        
        //==================================== Initialise exponent pointers
        expARStart                            = &exponentAlphaReal[ (this->_bandwidthLimit - 1) - bandIter ];
        expAIStart                            = &exponentAlphaImag[ (this->_bandwidthLimit - 1) - bandIter ];
        expGRStart                            = &exponentGammaReal[ (this->_bandwidthLimit - 1) - bandIter ];
        expGIStart                            = &exponentGammaImag[ (this->_bandwidthLimit - 1) - bandIter ];
        iSign                                 = 1.0;
        rSign                                 = 1.0;
        
        //==================================== Initialise the matrix holding structure
        this->_wignerMatrices.at(bandIter)    = std::vector< std::vector< std::array<double,2> > > ( noOrders );
        for ( unsigned int iter = 0; iter < noOrders; iter++ )
        {
            this->_wignerMatrices.at(bandIter).at(iter) = std::vector< std::array<double,2> > ( noOrders );
        }
        
        //==================================== Get wigner d matrix values using beta angles only
        wignerdmat                            ( bandIter, matIn, matOut, trigVals, sqrts, workspace );
        
        //==================================== Multiply the wigner d matrix by alpha and gamma values and save the wigner D matrix to output array
        for ( unsigned int d1Iter = 0; d1Iter < noOrders; d1Iter++ )
        {
            eARi                              = expARStart[d1Iter];
            eAIi                              = expAIStart[d1Iter];
            
            for ( unsigned int d2Iter = 0; d2Iter < noOrders; d2Iter++ )
            {
                Dij                           = matOut[arrConvIter];
                eGRj                          = expGRStart[d2Iter];
                eGIj                          = expGIStart[d2Iter];
                
                this->_wignerMatrices.at(bandIter).at(d1Iter).at(d2Iter)[0] = ( Dij * eGRj * eARi - Dij * eGIj * eAIi ) * rSign;
                this->_wignerMatrices.at(bandIter).at(d1Iter).at(d2Iter)[1] = ( Dij * eGRj * eAIi + Dij * eGIj * eARi ) * iSign;
                
                arrConvIter                  += 1;
                iSign                        *= -1.0;
                rSign                        *= -1.0;
            }
        }
        
        //==================================== Get ready for next wigner matrix calculation
        memcpy                                ( matIn, matOut, sizeof ( double ) * ( noOrders * noOrders ) );
    }
    
    //======================================== Free memory
    delete[]     matIn;
    delete[]     matOut;
    delete[]     sqrts;
    delete[]     workspace;
    delete[]     exponentAlphaReal;
    delete[]     exponentAlphaImag;
    delete[]     exponentGammaReal;
    delete[]     exponentGammaImag;
    
    //======================================== Done
    this->_wignerMatricesComputed             = true;
}

/*! \brief This function is responsible for computing the Wigner D matrices for full rotation function distance computation purposes.
 
 This function makes use of the SOFT2.0 package functions and logic to compute the Wigner d matrices and consequently
 the Wigner D matrices, which it stores internally in the distances computation class. The main purpose of the Wigner D
 matrices is to rotate the spherical harmonics coefficients.
 
 \param[in] settings The ProSHADE_settings container class with information about how to write the HTML report.
 
 \warning This is an internal function which should not be used by the user.
 */
void ProSHADE_internal::ProSHADE_compareOneAgainstAll::generateWignerMatrices ( ProSHADE::ProSHADE_settings* settings )
{
    //======================================== Sanity check
    if ( !this->_eulerAnglesFound )
    {
        std::cerr << "!!! ProSHADE ERROR !!! Error in file comparison !!! Attempted to get Wigner d matrices without computing the Euler angles first. Call the getEulerAngles function BEFORE the generateWignerMatrices function." << std::endl;
        
        if ( settings->htmlReport )
        {
            std::stringstream hlpSS;
            hlpSS << "<font color=\"red\">" << "Attempted to get Wigner d matrices without computing the Euler angles first. This looks like an internal bug, please report this case." << "</font>";
            rvapi_set_text                    ( hlpSS.str().c_str(),
                                               "ProgressSection",
                                               settings->htmlReportLineProgress,
                                               1,
                                               1,
                                               1 );
            settings->htmlReportLineProgress += 1;
            rvapi_flush                       ( );
        }
        
        exit ( -1 );
    }

    //======================================== Declare internal values
    double trigVals[2];
    unsigned int noOrders;
    unsigned int arrConvIter;
    double *expARStart;
    double *expAIStart;
    double *expGRStart;
    double *expGIStart;
    double Dij;
    double eARi;
    double eAIi;
    double eGRj;
    double eGIj;
    double iSign;
    double rSign;
    
    double angAlpha;
    double angBeta;
    double angGamma;
    
    int localComparisonBandLim;
    
    std::vector< std::vector< std::vector< std::array<double,2> > > > wignerMatricesHlp;
    std::vector< std::vector< std::array<double,2> > > wignerMatricesHlpHlp;
    
    for ( unsigned int strIt = 0; strIt < static_cast<unsigned int> ( this->all->size() ); strIt++ )
    {
        //==================================== Initialise local variables
        noOrders                              = 0;
        arrConvIter                           = 0;
        
        iSign                                 = 1.0;
        rSign                                 = 1.0;
        
        angAlpha                              = this->_eulerAngles.at(strIt)[0];
        angBeta                               = this->_eulerAngles.at(strIt)[1];
        angGamma                              = this->_eulerAngles.at(strIt)[2];
        
        wignerMatricesHlp.clear               ( );
        wignerMatricesHlpHlp.clear            ( );
        
        //==================================== Find the appropriate bandwidth
        localComparisonBandLim                = std::min ( this->one->_bandwidthLimit, this->all->at(strIt)->_bandwidthLimit );
        
        //==================================== Allocate memory
        double *matIn                         = new double[static_cast<unsigned int> ( 4 * pow( localComparisonBandLim, 2.0 ) - 4 * localComparisonBandLim + 1 )];
        double *matOut                        = new double[static_cast<unsigned int> ( 4 * pow( localComparisonBandLim, 2.0 ) - 4 * localComparisonBandLim + 1 )];
        double *sqrts                         = new double[static_cast<unsigned int> ( 2 * localComparisonBandLim )];
        double *workspace                     = new double[static_cast<unsigned int> ( 4 * pow( localComparisonBandLim, 2.0 ) )];
        double *exponentAlphaReal             = new double[static_cast<unsigned int> ( 2 * localComparisonBandLim - 1 )];
        double *exponentAlphaImag             = new double[static_cast<unsigned int> ( 2 * localComparisonBandLim - 1 )];
        double *exponentGammaReal             = new double[static_cast<unsigned int> ( 2 * localComparisonBandLim - 1 )];
        double *exponentGammaImag             = new double[static_cast<unsigned int> ( 2 * localComparisonBandLim - 1 )];
        
        //==================================== Compute the square roots
        for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( 2 * localComparisonBandLim ); iter++ )
        {
            sqrts[iter]                       = static_cast<double> ( sqrt ( static_cast<double> ( iter ) ) );
        }
        
        //==================================== Compute the trig values
        trigVals[0]                           = static_cast<double> ( cos ( 0.5 * -angBeta ) );
        trigVals[1]                           = static_cast<double> ( sin ( 0.5 * -angBeta ) );
        
        //==================================== Get alpha and gamma exponents
        genExp                                ( localComparisonBandLim, angAlpha, exponentAlphaReal, exponentAlphaImag );
        genExp                                ( localComparisonBandLim, angGamma, exponentGammaReal, exponentGammaImag );
        
        //==================================== For each band, find the Wigned d matrix
        for ( int bandIter = 0; bandIter < localComparisonBandLim; bandIter++ )
        {
            //================================ Initialise
            noOrders                          =  2 * bandIter + 1;
            arrConvIter                       =  0;
            
            //================================ Initialise exponent pointers
            expARStart                        = &exponentAlphaReal[ (localComparisonBandLim - 1) - bandIter ];
            expAIStart                        = &exponentAlphaImag[ (localComparisonBandLim - 1) - bandIter ];
            expGRStart                        = &exponentGammaReal[ (localComparisonBandLim - 1) - bandIter ];
            expGIStart                        = &exponentGammaImag[ (localComparisonBandLim - 1) - bandIter ];
            iSign                             = 1.0;
            rSign                             = 1.0;
            
            //================================ Initialise the matrix holding structure
            wignerMatricesHlpHlp              = std::vector< std::vector< std::array<double,2> > > ( noOrders );
            for ( unsigned int iter = 0; iter < noOrders; iter++ )
            {
                wignerMatricesHlpHlp.at(iter) = std::vector< std::array<double,2> > ( noOrders );
            }
            
            //================================ Get wigner d matrix values using beta angles only
            wignerdmat                        ( bandIter, matIn, matOut, trigVals, sqrts, workspace );
            
            //================================ Multiply the wigner d matrix by alpha and gamma values and save the wigner D matrix to output array
            for ( unsigned int d1Iter = 0; d1Iter < noOrders; d1Iter++ )
            {
                eARi                          = expARStart[d1Iter];
                eAIi                          = expAIStart[d1Iter];
                
                for ( unsigned int d2Iter = 0; d2Iter < noOrders; d2Iter++ )
                {
                    Dij                       = matOut[arrConvIter];
                    eGRj                      = expGRStart[d2Iter];
                    eGIj                      = expGIStart[d2Iter];
                    
                    wignerMatricesHlpHlp.at(d1Iter).at(d2Iter)[0] = ( Dij * eGRj * eARi - Dij * eGIj * eAIi ) * rSign;
                    wignerMatricesHlpHlp.at(d1Iter).at(d2Iter)[1] = ( Dij * eGRj * eAIi + Dij * eGIj * eARi ) * iSign;
                    
                    arrConvIter              += 1;
                    iSign                    *= -1.0;
                    rSign                    *= -1.0;
                }
            }
            
            //================================ Get ready for next wigner matrix calculation
            memcpy                            ( matIn, matOut, sizeof ( double ) * ( noOrders * noOrders ) );
            
            //================================ Save results
            wignerMatricesHlp.emplace_back    ( wignerMatricesHlpHlp );
        }
        
        //==================================== Free memory
        delete[] matIn;
        delete[] matOut;
        delete[] sqrts;
        delete[] workspace;
        delete[] exponentAlphaReal;
        delete[] exponentAlphaImag;
        delete[] exponentGammaReal;
        delete[] exponentGammaImag;
        
        //==================================== Save results
        this->_wignerMatrices.emplace_back    ( wignerMatricesHlp );
    }
    
    //======================================== Done
    this->_wignerMatricesComputed             = true;
}

