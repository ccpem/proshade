##########################################################################################################################
##################################### Testing the ProSHADE distances capability ##########################################
##########################################################################################################################

##########################################################################################################################
############################################## Include the python module
import sys;
import proshade;

##########################################################################################################################
############################################## Get the settings object
setUp                                         = proshade.ProSHADE_settings ();

############################################## Settings regarding resolutions
# ... Settings regarding which distances to compute
setUp.energyLevelDist                         = True;
setUp.traceSigmaDist                          = True;
setUp.fullRotFnDist                           = True;
    
# Settings regarding hierarchical distance computation
setUp.enLevelsThreshold                       = -999.9;
setUp.trSigmaThreshold                        = -999.9;
    
# ... Settings regarding the task
setUp.taskToPerform                           = proshade.Distances;
    
# Settings regarding the database
setUp.databaseName                            = "";
setUp.volumeTolerance                         = 0.4;
setUp.clearMapData                            = False;
    
# ... Settings regarding loudness
setUp.verbose                                 = -1;
setUp.htmlReport                              = False;

# Get command line info
if len(sys.argv) != 3:
    print ( "Usage: python useDatabase.py [dbName] [filename1] to get distances from [filename1] to all files in the database." );
    quit ( -1 );
else:
    setUp.databaseName                        = str( sys.argv[1] );
    setUp.appendStructure                     ( str( sys.argv[ 2 ] ) );

##########################################################################################################################
############################################## Now run proshade
runProshade                                   = proshade.ProSHADE ( setUp );

##########################################################################################################################
############################################## ... and get the results
crossCorrDists                                = runProshade.getCrossCorrDists   ( );
traceSigmaDists                               = runProshade.getTraceSigmaDists  ( );
rotationFunctionDists                         = runProshade.getRotFunctionDists ( );

##########################################################################################################################
############################################## ... printing them
sys.stdout.write                              ( "ProSHADE module version: " + runProshade.getProSHADEVersion() + "\n" );
sys.stdout.write                              ( "\n" );
sys.stdout.flush                              ( );

if len(crossCorrDists) > 0:
    ccOut                                     = "Cross-correlation distances        : " + "%+1.5f" % crossCorrDists[0];
    for it in range ( 1, len(crossCorrDists) ):
        ccOut                                 = ccOut + "\t" + "%+1.5f" % crossCorrDists[it];
    sys.stdout.write                          ( ccOut + "\n" );
sys.stdout.flush                              ( );
        
if len(traceSigmaDists) > 0:
    tsOut                                     = "Trace sigma distances              : " + "%+1.5f" % traceSigmaDists[0];
    for it in range ( 1, len(traceSigmaDists) ):
        tsOut                                 = tsOut + "\t" + "%+1.5f" % traceSigmaDists[it];
    sys.stdout.write                          ( tsOut + "\n" );
sys.stdout.flush                              ( );
        
if len(rotationFunctionDists) > 0:
    rfOut                                     = "Rotation function distances        : " + "%+1.5f" % rotationFunctionDists[0];
    for it in range ( 1, len(rotationFunctionDists) ):
        rfOut                                 = rfOut + "\t" + "%+1.5f" % rotationFunctionDists[it];
    sys.stdout.write                          ( rfOut + "\n" );
sys.stdout.flush                              ( );

##########################################################################################################################
############################################## Done
quit                                           ( );
