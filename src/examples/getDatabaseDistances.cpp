/*! \file getDistances.cpp
 \brief This is an example code which shows how to use the ProSHADE library to obtain the distances between the first and any other files.
 
 This file is part of the ProSHADE library for calculating
 shape descriptors for protein domains. This is a testing code,
 which is by no means complete or fully tested. Its use is at your
 own risk only. There is no quarantee that the results are correct.
 
 \author    Michal Tykac
 \author    Garib N. Murshudov
 \version   0.6.6
 \date      DEC 2018
 */

//============================================ ProSHADE
#include "../proshade/ProSHADE.h"

//============================================ Main
int main ( int argc,
           char **argv )
{
    //======================================== Default settings
    ProSHADE::ProSHADE_settings* setUp        = new ProSHADE::ProSHADE_settings ( );
    
    // Settings regarding which distances to compute
    setUp->energyLevelDist                    = true;
    setUp->traceSigmaDist                     = true;
    setUp->fullRotFnDist                      = true;
    
    // Settings regarding hierarchical distance computation
    setUp->enLevelsThreshold                  = -999.9;
    setUp->trSigmaThreshold                   = -999.9;
    
    // ... Settings regarding the task
    setUp->taskToPerform                      = ProSHADE::Distances;
    
    // Settings regarding the database
    setUp->databaseName                       = "";
    setUp->volumeTolerance                    = 0.4;
    setUp->clearMapData                       = false;
    
    // ... Settings regarding loudness
    setUp->verbose                            = -1;
    setUp->htmlReport                         = false;
    
    //======================================== Get files
    if ( argc != 3 )
    {
        std::cout << std::endl << "Usage: useDatabase [dbName] [filename1] to get distances from [filename1] to all files in the database." << std::endl << std::endl;
        exit ( 0 );
    }
    else
    {
        setUp->databaseName                   = std::string ( argv[1] );
        setUp->structFiles.emplace_back       ( std::string ( argv[2] ) );
    }
    
    //======================================== Run ProSHADE
    ProSHADE::ProSHADE *run                   = new ProSHADE::ProSHADE ( setUp );
    
    //======================================== Get results
    std::vector<double> crossCorrDists        = run->getCrossCorrDists ( );
    std::vector<double> traceSigmaDists       = run->getTraceSigmaDists ( );
    std::vector<double> rotFunDists           = run->getRotFunctionDists ( );
    
    //======================================== Print results
    printf ( "ProSHADE library version: %s\n\n", run->getProSHADEVersion().c_str() );
    
    if ( crossCorrDists.size() > 0 )
    {
        printf ( "Energy Level Descriptor distances      :           %+.5f", crossCorrDists.at(0) );
        for ( unsigned int iter = 1; iter < static_cast<unsigned int> ( crossCorrDists.size() ); iter++ )
        {
            printf ( "\t%+.4f", crossCorrDists.at(iter) );
        }
        printf ( "\n" );
    }
    else
    {
        std::cerr << "!!! Error !!! ProSHADE failed to obtain distances, please see the standard error stream for details. Did you set the energyLevelDist settings to false? Terminating..." << std::endl;
        exit ( -1 );
    }
    
    if ( traceSigmaDists.size() > 0 )
    {
        printf ( "Trace Sigma Descriptor distances       :           %+.5f", traceSigmaDists.at(0) );
        for ( unsigned int iter = 1; iter < static_cast<unsigned int> ( traceSigmaDists.size() ); iter++ )
        {
            printf ( "\t%+.4f", traceSigmaDists.at(iter) );
        }
        printf ( "\n" );
    }
    else
    {
        std::cerr << "!!! Error !!! ProSHADE failed to obtain distances, please see the standard error stream for details. Did you set the traceSigmaDist settings to false? Terminating..." << std::endl;
        exit ( -1 );
    }
    
    if ( rotFunDists.size() > 0 )
    {
        printf ( "Rotation Function Descriptor distances :           %+.5f", rotFunDists.at(0) );
        for ( unsigned int iter = 1; iter < static_cast<unsigned int> ( rotFunDists.size() ); iter++ )
        {
            printf ( "\t%+.4f", rotFunDists.at(iter) );
        }
        printf ( "\n" );
    }
    else
    {
        std::cerr << "!!! Error !!! ProSHADE failed to obtain distances, please see the standard error stream for details. Did you set the fullRotFnDist settings to false? Terminating..." << std::endl;
        exit ( -1 );
    }
    
    for ( unsigned int iter = 1; iter < static_cast<unsigned int> ( setUp->structFiles.size() ); iter++ )
    {
        printf ( "Matching structure names               : %40s vs. %-40s\n", setUp->structFiles.at(0).c_str(), setUp->structFiles.at(iter).c_str() );
    }
    std::cout << std::endl;
    
    //======================================== Free memory
    delete setUp;
    delete run;

    //======================================== Done
    return 0;
}

