/*! \file getDistances.cpp
 \brief This is an example code which shows how to use the ProSHADE library to obtain the distances between the first and any other files.
 
 This file is part of the ProSHADE library for calculating
 shape descriptors for protein domains. This is a testing code,
 which is by no means complete or fully tested. Its use is at your
 own risk only. There is no quarantee that the results are correct.
 
 \author    Michal Tykac
 \author    Garib N. Murshudov
 \version   0.6.6
 \date      DEC 2018
 */

//============================================ ProSHADE
#include "../proshade/ProSHADE.h"

//============================================ Main
int main ( int argc,
           char **argv )
{
    //======================================== Default settings
    ProSHADE::ProSHADE_settings* setUp        = new ProSHADE::ProSHADE_settings ( );
    
    // ... Settings regarding resolutions
    setUp->mapResolution                      = 11.0;
        
    // ... Settings regarding space around the structure in lattice
    setUp->extraSpace                         = 0.0;
    
    // ... Settings regarding rotation module, these should to be set this way!
    setUp->clearMapData                       = false;
    setUp->useCOM                             = false;
    setUp->rotChangeDefault                   = true;
    setUp->ignoreLs                           = std::vector<int> ();
    setUp->maskBlurFactor                     = 500.0;
    setUp->maskBlurFactorGiven                = false;

    
    // ... Settings regarding the task
    setUp->taskToPerform                      = ProSHADE::RotateMap;
    
    // ... Settings regarding where and if to save the clear map
    setUp->clearMapFile                       = "";
    
    // ... Settings regarding the map rotation mode
    setUp->rotAngle                           = 0.0;
    setUp->rotXAxis                           = 0.0;
    setUp->rotYAxis                           = 0.0;
    setUp->rotZAxis                           = 0.0;
    
    // ... Settings regarding the map saving mode
    setUp->axisOrder                          = "xyz";
    
    // ... Settings regarding the standard output amount
    setUp->verbose                            = -1;
    setUp->htmlReport                         = false;
    
    //======================================== Get files
    if ( argc != 7 )
    {
        std::cout << std::endl << "Usage: rotateMap [filename1] [filename2] [xAxis] [yAxis] [zAxis] [angle] to rotate [filename1] by [angle] along the rotation axis ( [xAxis], [yAxis], [zAxis] ) and save the resulting map in [filename2]." << std::endl << std::endl;
        exit ( 0 );
    }
    else
    {
        setUp->structFiles.emplace_back       ( std::string ( argv[1] ) );
        setUp->clearMapFile                   = ( std::string ( argv[2] ) );
        setUp->rotXAxis                       = std::stod ( argv[3] );
        setUp->rotYAxis                       = std::stod ( argv[4] );
        setUp->rotZAxis                       = std::stod ( argv[5] );
        setUp->rotAngle                       = std::stod ( argv[6] );
    }

    //======================================== Run ProSHADE
    ProSHADE::ProSHADE *run                   = new ProSHADE::ProSHADE ( setUp );
    
    //======================================== Print results
    printf ( "ProSHADE library version: %s\n\n", run->getProSHADEVersion().c_str() );
    
    //======================================== Free memory
    delete setUp;
    delete run;

    //======================================== Done
    return 0;
}

