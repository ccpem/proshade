/*! \file getDistances.cpp
 \brief This is an example code which shows how to use the ProSHADE library to obtain the distances between the first and any other files.
 
 This file is part of the ProSHADE library for calculating
 shape descriptors for protein domains. This is a testing code,
 which is by no means complete or fully tested. Its use is at your
 own risk only. There is no quarantee that the results are correct.
 
 \author    Michal Tykac
 \author    Garib N. Murshudov
 \version   0.6.6
 \date      DEC 2018
 */

//============================================ ProSHADE
#include "../proshade/ProSHADE.h"

//============================================ Main
int main ( int argc,
           char **argv )
{
    //======================================== Default settings
    ProSHADE::ProSHADE_settings* setUp        = new ProSHADE::ProSHADE_settings ( );
    
    // ... Settings regarding resolutions
    setUp->mapResolution                      = 10.0;
    setUp->bandwidth                          = 0;
    setUp->glIntegOrder                       = 0;
    setUp->theta                              = 0;
    setUp->phi                                = 0;
    
    // ... Settings regarding B factors
    setUp->bFactorValue                       = 80.0;
    setUp->bFactorChange                      = 0.0;
    
    // ... Setting regarding maps and removing noise
    setUp->noIQRsFromMap                      = 4.0;
    
    // ... Settings regarding concentric shells
    setUp->shellSpacing                       = 0.0;
    setUp->manualShells                       = 0;
    
    // ... Settings regarding phase
    setUp->usePhase                           = true;
    
    // ... Settings regarding map with phases
    setUp->useCOM                             = true;
    setUp->firstLineCOM                       = false;
    
    // ... Settings regarding space around the structure in lattice
    setUp->extraSpace                         = 8.0;
    
    // ... Settings regarding bands to be ignored
    std::vector<int> lsToIgnore;
    lsToIgnore.emplace_back ( 0 );
    setUp->ignoreLs                           = lsToIgnore;
    
    // ... Settings regarding the task
    setUp->taskToPerform                      = ProSHADE::BuildDB;
    
    // ... Settings regarding where and if to save the clear map
    setUp->clearMapData                       = true;
    
    // ... Settings regarding loudness
    setUp->verbose                            = -1;
    setUp->htmlReport                         = false;
    
    // ... Settings regarding the database
    setUp->databaseName                       = "";
    
    //======================================== Get files
    if ( argc < 3 )
    {
        std::cout << std::endl << "Usage: buildDatabase [dbName] [filename1] [filename2] ... [filenameX] build the database in [dbName] using files [filename1] to [filenameX]. Minimum of two files." << std::endl << std::endl;
        exit ( 0 );
    }
    else
    {
        setUp->databaseName                   = std::string ( argv[1] ) ;
        for ( unsigned int iter = 2; iter < static_cast<unsigned int> ( argc ); iter++ )
        {
            setUp->structFiles.emplace_back   ( std::string ( argv[iter] ) );
        }
    }
    
    //======================================== Run ProSHADE
    ProSHADE::ProSHADE *run                   = new ProSHADE::ProSHADE ( setUp );
    
    //======================================== Print results
    printf ( "ProSHADE library version: %s\n\n", run->getProSHADEVersion().c_str() );
    
    //======================================== Free memory
    delete setUp;
    delete run;

    //======================================== Done
    return 0;
}

