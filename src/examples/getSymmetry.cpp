/*! \file getDistances.cpp
 \brief This is an example code which shows how to use the ProSHADE library to obtain the distances between the first and any other files.
 
 This file is part of the ProSHADE library for calculating
 shape descriptors for protein domains. This is a testing code,
 which is by no means complete or fully tested. Its use is at your
 own risk only. There is no quarantee that the results are correct.
 
 \author    Michal Tykac
 \author    Garib N. Murshudov
 \version   0.6.6
 \date      DEC 2018
 */

//============================================ ProSHADE
#include "../proshade/ProSHADE.h"

//============================================ Main
int main ( int argc,
           char **argv )
{
    //======================================== Default settings
    ProSHADE::ProSHADE_settings* setUp        = new ProSHADE::ProSHADE_settings ( );
    
    // ... Settings regarding resolutions
    setUp->mapResolution                      = 6.0;
    setUp->bandwidth                          = 0;
    setUp->glIntegOrder                       = 0;
    setUp->theta                              = 0;
    setUp->phi                                = 0;
    
    // ... Settings regarding B factors
    setUp->bFactorValue                       = 80.0;
    setUp->bFactorChange                      = 0.0;
    
    // ... Setting regarding maps and removing noise
    setUp->noIQRsFromMap                      = 4.0;
    
    // ... Settings regarding concentric shells
    setUp->shellSpacing                       = 0.0;
    setUp->manualShells                       = 0;
    
    // ... Settings regarding map with phases
    setUp->useCOM                             = false;
    setUp->maskBlurFactor                     = 500.0;
    setUp->maskBlurFactorGiven                = false;
    
    // ... Settings regarding space around the structure in lattice
    setUp->extraSpace                         = 8.0;
    
    // ... Settings regarding peak detection
    setUp->peakHeightNoIQRs                   = 3.0;
    setUp->peakDistanceForReal                = 0.20;
    setUp->peakSurroundingPoints              = 1;
    
    // ... Settings regarding tolerances
    setUp->aaErrorTolerance                   = 0.1;
    setUp->symGapTolerance                    = 0.3;
    
    // ... Settings regarding the task
    setUp->taskToPerform                      = ProSHADE::Symmetry;
    
    // ... Settings regarding the symmetry type required
    setUp->symmetryFold                       = 0;
    setUp->symmetryType                       = "";
    
    // ... Settings regarding loudness
    setUp->verbose                            = -1;
    setUp->htmlReport                         = false;
    
    //======================================== Get files
    if ( argc != 2 )
    {
        std::cout << std::endl << "Usage: getSymmetry [filename1] to get symmetry information for structure in [filename1]." << std::endl << std::endl;
        exit ( 0 );
    }
    else
    {
        setUp->structFiles.emplace_back   ( std::string ( argv[1] ) );
    }
    
    //======================================== Run ProSHADE
    ProSHADE::ProSHADE *run                   = new ProSHADE::ProSHADE ( setUp );
    
    //======================================== Get results
    std::vector< std::vector< double > > cSyms = run->getCyclicSymmetries ( );
    std::vector< std::vector< std::array<double,6> > > dSyms = run->getDihedralSymmetries ( );
    std::vector< std::array<double,5> > tetSym= run->getTetrahedralSymmetries ( );
    std::vector< std::array<double,5> > octSym= run->getOctahedralSymmetries ( );
    std::vector< std::array<double,5> > icoSym= run->getIcosahedralSymmetries ( );
    std::vector< std::array<double,5> > symRecom = run->getRecommendedSymmetry ( );
    std::vector< std::array<double,5> > symSpecElems = run->getSpecificSymmetryElements ( "C", 4 );
    
    //======================================== Print results
    printf ( "ProSHADE library version: %s\n\n", run->getProSHADEVersion().c_str() );
    
    printf ( "Cyclic symmetry axes detected:\n" );
    printf ( "-----------------------------------------------------------\n" );
    printf ( "Symmetry Fold     x       y       z      Angle      Peak\n" );
    printf ( "  Type                                             height\n" );
    printf ( "-----------------------------------------------------------\n" );
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( cSyms.size() ); iter++ )
    {
        printf ( "   C      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( cSyms.at(iter)[0] ), cSyms.at(iter)[1], cSyms.at(iter)[2], cSyms.at(iter)[3], static_cast<int> ( cSyms.at(iter)[0] ), static_cast<double> ( cSyms.at(iter)[4] ) );
    }
    printf ( "\n" );
    
    printf ( "Dihedral symmetry axes table:\n" );
    printf ( "-----------------------------------------------------------\n" );
    printf ( "Symmetry Fold     x       y       z      Angle      Peak\n" );
    printf ( "  Type                                             height\n" );
    printf ( "-----------------------------------------------------------\n" );
    for ( unsigned int iter = 0; iter < static_cast<unsigned int> ( dSyms.size() ); iter++ )
    {
        printf ( "   D      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( dSyms.at(iter).at(0)[0] ), dSyms.at(iter).at(0)[1], dSyms.at(iter).at(0)[2], dSyms.at(iter).at(0)[3], static_cast<int> ( dSyms.at(iter).at(0)[0] ), dSyms.at(iter).at(0)[4] );
        for ( unsigned int it = 1; it < static_cast<unsigned int> ( dSyms.at(iter).size() ); it++ )
        {
            printf ( "          %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( dSyms.at(iter).at(it)[0] ), dSyms.at(iter).at(it)[1], dSyms.at(iter).at(it)[2], dSyms.at(iter).at(it)[3], static_cast<int> ( dSyms.at(iter).at(it)[0] ), dSyms.at(iter).at(it)[4] );
        }

    }
    printf ( "\n" );
    
    printf ( "Tetrahedral symmetry axes table:\n" );
    printf ( "-----------------------------------------------------------\n" );
    printf ( "Symmetry Fold     x       y       z      Angle      Peak\n" );
    printf ( "  Type                                             height\n" );
    printf ( "-----------------------------------------------------------\n" );
    if ( tetSym.size() > 0 )
    {
        printf ( "   T      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( tetSym.at(0)[0] ), tetSym.at(0)[1], tetSym.at(0)[2], tetSym.at(0)[3], static_cast<int> ( tetSym.at(0)[0] ), static_cast<double> ( tetSym.at(0)[4] ) );
        for ( unsigned int iter = 1; iter < static_cast<unsigned int> ( tetSym.size() ); iter++ )
        {
            printf ( "          %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( tetSym.at(iter)[0] ), tetSym.at(iter)[1], tetSym.at(iter)[2], tetSym.at(iter)[3], static_cast<int> ( tetSym.at(iter)[0] ), static_cast<double> ( tetSym.at(iter)[4] ) );
        }
    }
    printf ( "\n" );
    
    printf ( "Octahedral symmetry axes table:\n" );
    printf ( "-----------------------------------------------------------\n" );
    printf ( "Symmetry Fold     x       y       z      Angle      Peak\n" );
    printf ( "  Type                                             height\n" );
    printf ( "-----------------------------------------------------------\n" );
    if ( octSym.size() > 0 )
    {
        printf ( "   O      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( octSym.at(0)[0] ), octSym.at(0)[1], octSym.at(0)[2], octSym.at(0)[3], static_cast<int> ( octSym.at(0)[0] ), static_cast<double> ( octSym.at(0)[4] ) );
        for ( unsigned int iter = 1; iter < static_cast<unsigned int> ( octSym.size() ); iter++ )
        {
            printf ( "          %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( octSym.at(iter)[0] ), octSym.at(iter)[1], octSym.at(iter)[2], octSym.at(iter)[3], static_cast<int> ( octSym.at(iter)[0] ), static_cast<double> ( octSym.at(iter)[4] ) );
        }
    }
    printf ( "\n" );
    
    printf ( "Icosahedral symmetry axes table:\n" );
    printf ( "-----------------------------------------------------------\n" );
    printf ( "Symmetry Fold     x       y       z      Angle      Peak\n" );
    printf ( "  Type                                             height\n" );
    printf ( "-----------------------------------------------------------\n" );
    if ( icoSym.size() > 0 )
    {
        printf ( "   I      %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( icoSym.at(0)[0] ), icoSym.at(0)[1], icoSym.at(0)[2], icoSym.at(0)[3], static_cast<int> ( icoSym.at(0)[0] ), static_cast<double> ( icoSym.at(0)[4] ) );
        for ( unsigned int iter = 1; iter < static_cast<unsigned int> ( icoSym.size() ); iter++ )
        {
            printf ( "          %d    %+.2f   %+.2f   %+.2f    2pi / %d    %+.3f\n", static_cast<int> ( icoSym.at(iter)[0] ), icoSym.at(iter)[1], icoSym.at(iter)[2], icoSym.at(iter)[3], static_cast<int> ( icoSym.at(iter)[0] ), static_cast<double> ( icoSym.at(iter)[4] ) );
        }
    }
    printf ( "\n" );

    printf ( "-----------------------------------------------------------\n" );
    printf ( "-----------------------------------------------------------\n" );
    printf ( "Recommended symmetry: " );
    if ( icoSym.size() > 0 ) { printf ( "ICOSAHEDRAL\n" ); }
    else if ( octSym.size() > 0 ) { printf ( "OCTAHEDRAL\n" ); }
    else if ( tetSym.size() > 0 ) { printf ( "TETRAHEDRAL\n" ); }
    else if ( dSyms.size() > 0 ) { printf ( "DIHEDRAL\n" ); }
    else if ( cSyms.size() > 0 ) { printf ( "CYCLIC\n" ); }
    printf ( "-----------------------------------------------------------\n" );
    printf ( "-----------------------------------------------------------\n\n" );
    printf ( "Recommended symmetry elements table:\n" );
    printf ( "-----------------------------------------------------------\n" );
    printf ( "Symmetry          x          y          z          Angle        \n" );
    printf ( "  Type                                             (rad)        \n" );
    printf ( "-----------------------------------------------------------\n" );
    if ( symRecom.size() > 0 )
    {
        printf ( "   E            %+.2f      %+.2f      %+.2f        %+.3f    \n", symRecom.at(0)[1], symRecom.at(0)[2], symRecom.at(0)[3], symRecom.at(0)[4] );
        for ( unsigned int iter = 1; iter < static_cast<unsigned int> ( symRecom.size() ); iter++ )
        {
            printf ( "   C%d           %+.2f      %+.2f      %+.2f        %+.3f    \n", static_cast<int> ( symRecom.at(iter)[0] ), symRecom.at(iter)[1], symRecom.at(iter)[2], symRecom.at(iter)[3], symRecom.at(iter)[4] );
        }
    }
    printf ( "\n" );
    
    printf ( "Requested symmetry elements table:\n" );
    printf ( "-----------------------------------------------------------\n" );
    printf ( "Symmetry          x          y          z          Angle        \n" );
    printf ( "  Type                                             (rad)        \n" );
    printf ( "-----------------------------------------------------------\n" );
    if ( symSpecElems.size() > 0 )
    {
        printf ( "   E            %+.2f      %+.2f      %+.2f        %+.3f    \n", symSpecElems.at(0)[1], symSpecElems.at(0)[2], symSpecElems.at(0)[3], symSpecElems.at(0)[4] );
        for ( unsigned int iter = 1; iter < static_cast<unsigned int> ( symSpecElems.size() ); iter++ )
        {
            printf ( "   C%d           %+.2f      %+.2f      %+.2f        %+.3f    \n", static_cast<int> ( symSpecElems.at(iter)[0] ), symSpecElems.at(iter)[1], symSpecElems.at(iter)[2], symSpecElems.at(iter)[3], symSpecElems.at(iter)[4] );
        }
    }
    
    //======================================== Free memory
    delete setUp;
    delete run;

    //======================================== Done
    return 0;
}

