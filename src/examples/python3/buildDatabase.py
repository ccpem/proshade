##########################################################################################################################
##################################### Testing the ProSHADE distances capability ##########################################
##########################################################################################################################

##########################################################################################################################
############################################## Include the python module
import sys;
import proshade3 as proshade;

##########################################################################################################################
############################################## Get the settings object
setUp                                         = proshade.ProSHADE_settings ();

############################################## Settings regarding resolutions
# ... Settings regarding resolutions
setUp.mapResolution                           = 10.0;
setUp.bandwidth                               = 0;
setUp.glIntegOrder                            = 0;
setUp.theta                                   = 0;
setUp.phi                                     = 0;
    
# ... Settings regarding B factors
setUp.bFactorValue                            = 80.0;
setUp.bFactorChange                           = 0.0;
    
# ... Setting regarding maps and removing noise
setUp.noIQRsFromMap                           = 4.0;
    
# ... Settings regarding concentric shells
setUp.shellSpacing                            = 0.0;
setUp.manualShells                            = 0;
    
# ... Settings regarding phase
setUp.usePhase                                = True;
    
# ... Settings regarding map with phases
setUp.useCOM                                  = True;
setUp.firstLineCOM                            = False;
    
# ... Settings regarding space around the structure in lattice
setUp.extraSpace                              = 8.0;
    
# ... Settings regarding bands to be ignored
setUp.ignoreLsAddValuePy                      ( 0 );
    
# ... Settings regarding the task
setUp.taskToPerform                           = proshade.BuildDB;
    
# ... Settings regarding where and if to save the clear map
setUp.clearMapData                            = True;
    
# ... Settings regarding loudness
setUp.verbose                                 = -1;
setUp.htmlReport                              = False;
    
# ... Settings regarding the database
setUp.databaseName                            = "";
    
############################################## Get files
if len( sys.argv ) < 3:
    sys.stdout.write ( "Usage: python buildDatabase.py [dbName] [filename1] [filename2] ... [filenameX] build the database in [dbName] using files [filename1] to [filenameX]. Minimum of two files.\n\n" );
    quit ( -1 );
else:
    setUp.databaseName                        = str ( sys.argv[1] ) ;
    hlpPyStrVec                               = proshade.StringList ( len(sys.argv)-2 );
    for it in range ( 2, len(sys.argv) ):
        hlpPyStrVec[ it-2 ]                   = str(sys.argv[it]);

# ... set the file list
setUp.structFiles                             = hlpPyStrVec;

##########################################################################################################################
############################################## Now run proshade
runProshade                                   = proshade.ProSHADE ( setUp );

##########################################################################################################################
############################################## ... printing them
print                                         ( "ProSHADE module version: " + runProshade.getProSHADEVersion() );
print                                         ( "" );

##########################################################################################################################
############################################## Done
quit                                           ( );
