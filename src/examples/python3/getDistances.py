##########################################################################################################################
##################################### Testing the ProSHADE distances capability ##########################################
##########################################################################################################################

##########################################################################################################################
############################################## Include the python module
import sys;
import proshade3 as proshade;

##########################################################################################################################
############################################## Get the settings object
setUp                                         = proshade.ProSHADE_settings ();

############################################## Settings regarding resolutions
setUp.mapResolution                           = 10.0;
setUp.bandwidth                               = 0;
setUp.glIntegOrder                            = 0;
setUp.theta                                   = 0;
setUp.phi                                     = 0;

# ... Settings regarding B factors
setUp.bFactorValue                            = 80.0;
setUp.bFactorChange                           = 0.0;

# ... Settings regarding concentric shells
setUp.shellSpacing                            = 0.0;
setUp.manualShells                            = 0;
    
# ... Settings regarding phase
setUp.usePhase                                = True;

# ... Settings regarding map with phases
setUp.useCOM                                  = True;
setUp.maskBlurFactor                          = 500.0;
setUp.maskBlurFactorGiven                     = False;

# ... Settings regarding weighting the distances
setUp.alpha                                   = 1.0;
setUp.mPower                                  = 1.0;
    
# ... Settings regarding bands to be ignored
setUp.ignoreLsAddValuePy                      ( 0 );

# ... Settings regarding which distances to compute
setUp.energyLevelDist                         = True;
setUp.traceSigmaDist                          = True;
setUp.fullRotFnDist                           = True;

# ... Settings regarding hierarchical distance computation
setUp.enLevelsThreshold                       = -999.9;
setUp.trSigmaThreshold                        = -999.9;
    
# ... Settings regarding the task
setUp.taskToPerform                           = proshade.Distances;
    
# ... Settings regarding loudness
setUp.verbose                                 = -1;
setUp.htmlReport                              = False;

# Get command line info
if len(sys.argv) < 3:
    print ( "Usage: python getDistances.py [filename1] [filename2] ... [filenameX] to get distances from [filename1] to all other files. Minimum of two files." );
    quit ( -1 );
else:
    hlpPyStrVec                               = proshade.StringList ( len(sys.argv)-1 );
    for it in range ( 1, len(sys.argv) ):
        hlpPyStrVec[ it-1 ]                   = str(sys.argv[it]);

# ... set the file list
setUp.structFiles                             = hlpPyStrVec;

##########################################################################################################################
############################################## Now run proshade
runProshade                                   = proshade.ProSHADE ( setUp );

##########################################################################################################################
############################################## ... and get the results
crossCorrDists                                = runProshade.getCrossCorrDists   ( );
traceSigmaDists                               = runProshade.getTraceSigmaDists  ( );
rotationFunctionDists                         = runProshade.getRotFunctionDists ( );

##########################################################################################################################
############################################## ... printing them
print                                         ( "ProSHADE module version: " + runProshade.getProSHADEVersion() );
print                                         ( "" );
if len(crossCorrDists) > 0:
    ccOut                                     = "Cross-correlation distances        : " + "%1.5f" % crossCorrDists[0];
    for it in range ( 1, len(crossCorrDists) ):
        ccOut                                 = ccOut + "\t" + "%1.5f" % crossCorrDists[it];
    sys.stdout.write                          ( ccOut + "\n" );
sys.stdout.flush                              ( );
        
if len(traceSigmaDists) > 0:
    tsOut                                     = "Trace sigma distances              : " + "%1.5f" % traceSigmaDists[0];
    for it in range ( 1, len(traceSigmaDists) ):
        tsOut                                 = tsOut + "\t" + "%1.5f" % traceSigmaDists[it];
    sys.stdout.write                          ( tsOut + "\n" );
sys.stdout.flush                              ( );
        
if len(rotationFunctionDists) > 0:
    rfOut                                     = "Rotation function distances        : " + "%1.5f" % rotationFunctionDists[0];
    for it in range ( 1, len(rotationFunctionDists) ):
        rfOut                                 = rfOut + "\t" + "%1.5f" % rotationFunctionDists[it];
    sys.stdout.write                          ( rfOut + "\n" );
sys.stdout.flush                              ( );

##########################################################################################################################
############################################## Done
quit                                           ( );
