##########################################################################################################################
##################################### Testing the ProSHADE distances capability ##########################################
##########################################################################################################################

##########################################################################################################################
############################################## Include the python module
import sys;
import proshade3 as proshade;

##########################################################################################################################
############################################## Get the settings object
setUp                                         = proshade.ProSHADE_settings ();

# ... Settings regarding resolutions
setUp.mapResolution                           = 11.0;
    
# ... Settings regarding space around the structure in lattice
setUp.extraSpace                              = 0.0;
    
# ... Settings regarding the task
setUp.taskToPerform                           = proshade.RotateMap;
    
# ... Settings regarding rotation module, these should to be set this way!
setUp.clearMapData                            = False;
setUp.useCOM                                  = False;
setUp.rotChangeDefault                        = True;
setUp.maskBlurFactor                          = 0.0;
setUp.maskBlurFactorGiven                     = False;
    
# ... Settings regarding the map rotation mode
setUp.rotAngle                                = 0.0;
setUp.rotXAxis                                = 0.0;
setUp.rotYAxis                                = 0.0;
setUp.rotZAxis                                = 0.0;
    
# ... Settings regarding the map saving mode
setUp.axisOrder                               = str ( "xyz" );
    
# ... Settings regarding the standard output amount
setUp.verbose                                 = -1;
setUp.htmlReport                              = False;

# Get command line info
if len(sys.argv) != 7:
    print ( "Usage: python rotateMap.py [filename1] [filename2] [xAxis] [yAxis] [zAxis] [angle] to rotate [filename1] by [angle] along the rotation axis ( [xAxis], [yAxis], [zAxis] ) and save the resulting map in [filename2]." );
    quit ( -1 );
else:
    hlpPyStrVec                               = proshade.StringList ( 1 );
    hlpPyStrVec[ 0 ]                          = str ( sys.argv[ 1 ] );
    setUp.structFiles                         = hlpPyStrVec;
    setUp.clearMapFile                        = str ( sys.argv[ 2 ] );
    setUp.rotXAxis                            = float ( sys.argv[ 3 ] );
    setUp.rotYAxis                            = float ( sys.argv[ 4 ] );
    setUp.rotZAxis                            = float ( sys.argv[ 5 ] );
    setUp.rotAngle                            = float ( sys.argv[ 6 ] );

##########################################################################################################################
############################################## Now run proshade
runProshade                                   = proshade.ProSHADE ( setUp );

##########################################################################################################################
############################################## ... printing them
print                                         ( "ProSHADE module version: " + runProshade.getProSHADEVersion() );
print                                         ( "" );

##########################################################################################################################
############################################## Done
quit                                           ( );
