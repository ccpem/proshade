##########################################################################################################################
##################################### Testing the ProSHADE symmetry capability ###########################################
##########################################################################################################################

##########################################################################################################################
############################################## Include the python module
import sys;
import proshade3 as proshade;

##########################################################################################################################
############################################## Define ProSHADE vector parsing functions
def parseDVecFromProshade ( cSyms ):
    vecIt                                     = 0;
    myAxis                                    = [];
    myAxes                                    = [];
    while True:
        if vecIt == len ( cSyms ):
            break;
        if cSyms [ vecIt ] == -999.999:
            myAxes.append                     ( myAxis )
            myAxis                            = [];
            vecIt                             = vecIt + 1;
        else:
            myAxis.append                     ( cSyms[vecIt] );
            vecIt                             = vecIt + 1;
    return myAxes;

def parseDVecDVecFromProshade ( dSym ):
    vecIt                                     = 0;
    myAxis                                    = [];
    myAxes                                    = [];
    mySymmetry                                = [];
    while True:
        if vecIt == len ( dSym ):
            break;
        if dSym [ vecIt ] == -999.999:
            myAxes.append                     ( myAxis );
            myAxis                            = [];
            vecIt                             = vecIt + 1;
        elif dSym [ vecIt ] == -777.777:
            mySymmetry.append                 ( myAxes );
            myAxes                            = [];
            myAxis                            = [];
            vecIt                             = vecIt + 1;
        else:
            myAxis.append                     ( dSym[vecIt] );
            vecIt                             = vecIt + 1;
    return mySymmetry;

##########################################################################################################################
############################################## Get the settings object
setUp                                         = proshade.ProSHADE_settings ();

############################################## Settings setup
# ... Settings regarding resolutions
setUp.mapResolution                           = 6.0;
setUp.bandwidth                               = 0;
setUp.glIntegOrder                            = 0;
setUp.theta                                   = 0;
setUp.phi                                     = 0;

# ... Settings regarding B factors
setUp.bFactorValue                            = 80.0;
setUp.bFactorChange                           = 0.0;

# ... Setting regarding maps and removing noise
setUp.noIQRsFromMap                           = 4.0;

# ... Settings regarding concentric shells
setUp.shellSpacing                            = 0.0;
setUp.manualShells                            = 0;

# ... Settings regarding map with phases
setUp.useCOM                                  = False;
setUp.maskBlurFactor                          = 500.0;
setUp.maskBlurFactorGiven                     = False;

# ... Settings regarding space around the structure in lattice
setUp.extraSpace                              = 8.0;

# ... Settings regarding peak detection
setUp.peakHeightNoIQRs                        = 3.0;
setUp.peakDistanceForReal                     = 0.20;
setUp.peakSurroundingPoints                   = 1;

# ... Settings regarding tolerances
setUp.aaErrorTolerance                        = 0.1;
setUp.symGapTolerance                         = 0.3;

# ... Settings regarding the task
setUp.taskToPerform                           = proshade.Symmetry;

# ... Settings regarding the symmetry type required
setUp.symmetryFold                            = 0;
setUp.symmetryType                            = "";

# ... Settings regarding loudness
setUp.verbose                                 = -1;
setUp.htmlReport                              = False;

# ... Get command line info
if len(sys.argv) < 2:
    print                                     ( "Usage: python getSymmetry.py [filename1] to get symmetry information for structure in [filename1]." );
    quit                                      ( -1 );
else:
    hlpPyStrVec                                   = proshade.StringList( len(sys.argv)-1 );
    hlpPyStrVec[ 0 ]                              = str( sys.argv[ 1 ] );

# ... set the file list
setUp.structFiles                             = hlpPyStrVec;

##########################################################################################################################
############################################## Now run proshade
runProshade                                   = proshade.ProSHADE ( setUp );

##########################################################################################################################
############################################## ... and get the results
cyclicSymmetries                              = runProshade.getCyclicSymmetries           ( );
dihedralSymmetries                            = runProshade.getDihedralSymmetriesPy       ( );
tetrahedralSymmetries                         = runProshade.getTetrahedralSymmetriesPy    ( );
octahedralSymmetries                          = runProshade.getOctahedralSymmetriesPy     ( );
icosahedralSymmetries                         = runProshade.getIcosahedralSymmetriesPy    ( );
symElemsRecommended                           = runProshade.getRecommendedSymmetryElementsPy ( );
symElemsRequested                             = runProshade.getSpecificSymmetryElementsPy ( "C", 4 );

##########################################################################################################################
############################################## ... printing them
sys.stdout.write                              ( "ProSHADE module version: " + runProshade.getProSHADEVersion ( ) + "\n" );
sys.stdout.write                              ( "\n" );
sys.stdout.flush                              ( );

# ... Cyclic symmetries
sys.stdout.write                              ( "Cyclic symmetry axes detected:\n" );
sys.stdout.write                              ( "-----------------------------------------------------------\n" );
sys.stdout.write                              ( "Symmetry Fold     x       y       z      Angle      Peak   \n" );
sys.stdout.write                              ( "  Type                                   (rad)      height \n" );
sys.stdout.write                              ( "-----------------------------------------------------------\n" );

for it in range ( 0, len ( cyclicSymmetries ) ):
    sys.stdout.write                          ( "   C       %d    %+1.3f  %+1.3f  %+1.3f   %+1.3f    %+1.3f" % ( cyclicSymmetries[it][0], cyclicSymmetries[it][1], cyclicSymmetries[it][2], cyclicSymmetries[it][3], 6.282 / cyclicSymmetries[it][0], cyclicSymmetries[it][4] ) + "\n" );
sys.stdout.write                              ( "\n" );
sys.stdout.flush                              ( );

# ... Dihedral symmetries
sys.stdout.write                              ( "Dihedral symmetry axes detected:\n" );
sys.stdout.write                              ( "-----------------------------------------------------------\n" );
sys.stdout.write                              ( "Symmetry Fold     x       y       z      Angle      Peak   \n" );
sys.stdout.write                              ( "  Type                                   (rad)      height \n" );
sys.stdout.write                              ( "-----------------------------------------------------------\n" );

mySymmetry                                    = parseDVecDVecFromProshade ( dihedralSymmetries );
for sym in range ( 0, len( mySymmetry ) ):
    myAxes                                    = mySymmetry[sym];
    sys.stdout.write                          ( "   D       %d    %+1.3f  %+1.3f  %+1.3f   %+1.3f    %+1.3f" % ( myAxes[0][0], myAxes[0][1], myAxes[0][2], myAxes[0][3], 6.282 / myAxes[0][0], myAxes[0][4] ) + "\n" );
    for subAx in range ( 1, len( mySymmetry[sym] ) ):
        sys.stdout.write                      ( "           %d    %+1.3f  %+1.3f  %+1.3f   %+1.3f    %+1.3f" % ( myAxes[subAx][0], myAxes[subAx][1], myAxes[subAx][2], myAxes[subAx][3], 6.282 / myAxes[subAx][0], myAxes[subAx][4] ) + "\n" );
sys.stdout.write                              ( "\n" );
sys.stdout.flush                              ( );

# ... Tetrahedral symmetries
sys.stdout.write                              ( "Tetrahedral symmetry axes detected:\n" );
sys.stdout.write                              ( "-----------------------------------------------------------\n" );
sys.stdout.write                              ( "Symmetry Fold     x       y       z      Angle      Peak   \n" );
sys.stdout.write                              ( "  Type                                   (rad)      height \n" );
sys.stdout.write                              ( "-----------------------------------------------------------\n" );

myAxes                                        = parseDVecFromProshade ( tetrahedralSymmetries );
for it in range ( 0, len ( myAxes ) ):
    sys.stdout.write                          ( "   T       %d    %+1.3f  %+1.3f  %+1.3f   %+1.3f    %+1.3f" % ( myAxes[it][0], myAxes[it][1], myAxes[it][2], myAxes[it][3], 6.282 / myAxes[it][0], myAxes[it][4] ) + "\n" );
sys.stdout.write                              ( "\n" );
sys.stdout.flush                              ( );

# ... Octahedral symmetries
sys.stdout.write                              ( "Octahedral symmetry axes detected:\n" );
sys.stdout.write                              ( "-----------------------------------------------------------\n" );
sys.stdout.write                              ( "Symmetry Fold     x       y       z      Angle      Peak   \n" );
sys.stdout.write                              ( "  Type                                   (rad)      height \n" );
sys.stdout.write                              ( "-----------------------------------------------------------\n" );

myAxes                                        = parseDVecFromProshade ( octahedralSymmetries );
for it in range ( 0, len ( myAxes ) ):
    sys.stdout.write                          ( "   O       %d    %+1.3f  %+1.3f  %+1.3f   %+1.3f    %+1.3f" % ( myAxes[it][0], myAxes[it][1], myAxes[it][2], myAxes[it][3], 6.282 / myAxes[it][0], myAxes[it][4] ) + "\n" );
sys.stdout.write                              ( "\n" );
sys.stdout.flush                              ( );

# ... Icosahedral symmetries
sys.stdout.write                              ( "Icosahedral symmetry axes detected:\n" );
sys.stdout.write                              ( "-----------------------------------------------------------\n" );
sys.stdout.write                              ( "Symmetry Fold     x       y       z      Angle      Peak   \n" );
sys.stdout.write                              ( "  Type                                   (rad)      height \n" );
sys.stdout.write                              ( "-----------------------------------------------------------\n" );

myAxes                                        = parseDVecFromProshade ( icosahedralSymmetries );
for it in range ( 0, len ( myAxes ) ):
    sys.stdout.write                          ( "   I       %d    %+1.3f  %+1.3f  %+1.3f   %+1.3f    %+1.3f" % ( myAxes[it][0], myAxes[it][1], myAxes[it][2], myAxes[it][3], 6.282 / myAxes[it][0], myAxes[it][4] ) + "\n" );
sys.stdout.write                              ( "\n" );
sys.stdout.flush                              ( );

# ... Recommended (simple highest peak approach, this will be improved in the future, do not take the work 'recommended' too seriously) symmetry elements
sys.stdout.write                              ( "-----------------------------------------------------------\n" );
sys.stdout.write                              ( "-----------------------------------------------------------\n" );
sys.stdout.write                              ( "Recommended symmetry: " );
if len ( parseDVecFromProshade ( icosahedralSymmetries ) ) > 0:
    sys.stdout.write                          ( "ICOSAHEDRAL\n" );
elif len ( parseDVecFromProshade ( octahedralSymmetries ) ) > 0:
    sys.stdout.write                          ( "OCTAHEDRAL\n" );
elif len ( parseDVecFromProshade ( tetrahedralSymmetries ) ) > 0:
    sys.stdout.write                          ( "TETRAHEDRAL\n" );
elif len ( parseDVecDVecFromProshade ( dihedralSymmetries ) ) > 0:
    sys.stdout.write                          ( "DIHEDRAL\n" );
elif len ( parseDVecFromProshade ( cyclicSymmetries ) ) > 0:
    sys.stdout.write                          ( "CYCLIC\n" );
sys.stdout.write                              ( "Recommended symmetry elements table:                       \n" );
sys.stdout.write                              ( "-----------------------------------------------------------\n" );
sys.stdout.write                              ( "Symmetry          x          y          z          Angle   \n" );
sys.stdout.write                              ( "  Type                                             (rad)   \n" );
sys.stdout.write                              ( "-----------------------------------------------------------\n" );

myElems                                       = parseDVecFromProshade ( symElemsRecommended );
if len ( myElems ) > 0:
    sys.stdout.write                          ( "   E           %+1.3f     %+1.3f     %+1.3f       %+1.3f" % ( myElems[0][1], myElems[0][2], myElems[0][3], myElems[0][4] ) + "\n" );
    for elem in range ( 1, len( myElems ) ):
        sys.stdout.write                      ( "   C%d          %+1.3f     %+1.3f     %+1.3f       %+1.3f" % ( myElems[elem][0], myElems[elem][1], myElems[elem][2], myElems[elem][3], myElems[elem][4] ) + "\n" );
sys.stdout.write                              ( "\n" );
sys.stdout.flush                              ( );

# ... Requested symmetry elements
sys.stdout.write                              ( "Requested symmetry elements table:                       \n" );
sys.stdout.write                              ( "-----------------------------------------------------------\n" );
sys.stdout.write                              ( "Symmetry          x          y          z          Angle   \n" );
sys.stdout.write                              ( "  Type                                             (rad)   \n" );
sys.stdout.write                              ( "-----------------------------------------------------------\n" );

myElems                                       = parseDVecFromProshade ( symElemsRequested );
if len ( myElems ) > 0:
    sys.stdout.write                          ( "   E           %+1.3f     %+1.3f     %+1.3f       %+1.3f" % ( myElems[0][1], myElems[0][2], myElems[0][3], myElems[0][4] ) + "\n" );
    for elem in range ( 1, len( myElems ) ):
        sys.stdout.write                      ( "   C%d          %+1.3f     %+1.3f     %+1.3f       %+1.3f" % ( myElems[elem][0], myElems[elem][1], myElems[elem][2], myElems[elem][3], myElems[elem][4] ) + "\n" );
sys.stdout.write                              ( "\n" );
sys.stdout.flush                              ( );

##########################################################################################################################
############################################## Done
quit                                          ( );

