##########################################################################################################################
##################################### Testing the ProSHADE distances capability ##########################################
##########################################################################################################################

##########################################################################################################################
############################################## Include the python module
import sys;
import proshade3 as proshade;

##########################################################################################################################
############################################## Get the settings object
setUp                                         = proshade.ProSHADE_settings ();

# ... Settings regarding resolutions
setUp.mapResolution                           = 1.0;
    
# ... Settings regarding space around the structure in lattice
setUp.extraSpace                              = -777.7; # this is a magic number, which will make the ProSHADE overlay mode use the appropriate extra space for computing maps and their centering, but it will then remove this extra space, so that the user will not see it. Change at your own risk, you have been warned.
    
# ... Settings regarding the task
setUp.taskToPerform                           = proshade.OverlayMap;
    
# ... Settings regarding rotation module, these should to be set this way!
setUp.clearMapData                            = False;
setUp.useCOM                                  = False;
setUp.rotChangeDefault                        = True;
setUp.overlayDefaults                         = True;
setUp.maskBlurFactor                          = 500.0;
setUp.maskBlurFactorGiven                     = False;
setUp.maxRotError                             = 0;

# ... Settings regarding the map saving mode
setUp.axisOrder                               = str ( "xyz" );
    
# ... Settings regarding the standard output amount
setUp.verbose                                 = 2;
setUp.htmlReport                              = False;

# Get command line info
if len(sys.argv) < 3 or len(sys.argv) > 4:
    print ( "Usage: python overlayMaps.py [filename1] [filename2] [filename3] to overlay [filename1] to [filename2] and save the resulting map in [filename3], where [filename3] is optional." );
    quit ( -1 );
else:
    hlpPyStrVec                               = proshade.StringList ( 2 );
    hlpPyStrVec[ 0 ]                          = str ( sys.argv[ 1 ] );
    hlpPyStrVec[ 1 ]                          = str ( sys.argv[ 2 ] );
    setUp.structFiles                         = hlpPyStrVec;
    if len(sys.argv) == 4:
        setUp.clearMapFile                    = str ( sys.argv[ 3 ] );

##########################################################################################################################
############################################## Now run proshade
runProshade                                   = proshade.ProSHADE ( setUp );

##########################################################################################################################
############################################## ... printing them
print                                         ( "ProSHADE module version: " + runProshade.getProSHADEVersion() );
print                                         ( "" );

##########################################################################################################################
############################################## Done
quit                                           ( );
