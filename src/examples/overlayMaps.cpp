/*! \file getDistances.cpp
 \brief This is an example code which shows how to use the ProSHADE library to obtain the distances between the first and any other files.
 
 This file is part of the ProSHADE library for calculating
 shape descriptors for protein domains. This is a testing code,
 which is by no means complete or fully tested. Its use is at your
 own risk only. There is no quarantee that the results are correct.
 
 \author    Michal Tykac
 \author    Garib N. Murshudov
 \version   0.6.6
 \date      DEC 2018
 */

//============================================ ProSHADE
#include "../proshade/ProSHADE.h"

//============================================ Main
int main ( int argc,
           char **argv )
{
    //======================================== Default settings
    ProSHADE::ProSHADE_settings* setUp        = new ProSHADE::ProSHADE_settings ( );
    
    // ... Settings regarding resolutions
    setUp->mapResolution                      = 2.0;
        
    // ... Settings regarding space around the structure in lattice
    setUp->extraSpace                         = -777.7; // this is a magic number, which will make the ProSHADE overlay mode use the appropriate extra space for computing maps and their centering, but it will then remove this extra space, so that the user will not see it. Change at your own risk, you have been warned.
    
    // ... Settings regarding rotation module, these should to be set this way!
    setUp->clearMapData                       = false;
    setUp->useCOM                             = false;
    setUp->rotChangeDefault                   = true;
    setUp->overlayDefaults                    = true;
    setUp->ignoreLs                           = std::vector<int> ();
    setUp->maskBlurFactor                     = 500.0;
    setUp->maskBlurFactorGiven                = false;
    setUp->maxRotError                        = 0;
    
    // ... Settings regarding the task
    setUp->taskToPerform                      = ProSHADE::OverlayMap;
    
    // ... Settings regarding where and if to save the clear map
    setUp->clearMapFile                       = "";
    
    // ... Settings regarding the map saving mode
    setUp->axisOrder                          = "xyz";
    
    // ... Settings regarding the standard output amount
    setUp->verbose                            = 2;
    setUp->htmlReport                         = false;
    
    //======================================== Get files
    if ( ( argc < 3 ) || ( argc > 4 ) )
    {
        std::cout << std::endl << "Usage: overlayMaps [filename1] [filename2] [filename3] to overlay [filename1] to [filename2] and save the resulting map in [filename3], where [filename3] is optional." << std::endl << std::endl;
        exit ( 0 );
    }
    else
    {
        setUp->structFiles.emplace_back       ( std::string ( argv[1] ) );
        setUp->structFiles.emplace_back       ( std::string ( argv[2] ) );
        if ( argc == 4 )
        {
            setUp->clearMapFile               = ( std::string ( argv[3] ) );
        }
    }

    //======================================== Run ProSHADE
    ProSHADE::ProSHADE *run                   = new ProSHADE::ProSHADE ( setUp );
    
    //======================================== Print results
    printf ( "ProSHADE library version: %s\n\n", run->getProSHADEVersion().c_str() );
    
    //======================================== Free memory
    delete setUp;
    delete run;

    //======================================== Done
    return 0;
}

