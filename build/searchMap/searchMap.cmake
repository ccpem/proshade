##########################################################################################
##########################################################################################
############################## ProSHADE searchMap cmake file #############################
##########################################################################################
##########################################################################################

##########################################################################################
################################### Find the source files
file    ( GLOB SOURCES  "${CMAKE_SOURCE_DIR}/../src/proshade/*.cpp"                       )
file    ( GLOB SMAP_SRC "${CMAKE_SOURCE_DIR}/../src/mapSearch/*.cpp"                      )

##########################################################################################
################################### Add includes
link_directories      ( ${MY_INSTALL_LOCATION}/lib                                        )
include_directories   ( ${CMAKE_SOURCE_DIR}/src/mapSearch                                 )

add_executable        ( searchMapSoft ${OBJS} ${SMAP_SRC}                                 )
target_link_libraries ( searchMapSoft  proshade1                                          )

##########################################################################################
################################ Deal with RPATH
if    ( APPLE )
   set_target_properties ( searchMapSoft     PROPERTIES MACOSX_RPATH TRUE                 )
endif ( APPLE )

set_target_properties ( searchMapSoft  PROPERTIES INSTALL_RPATH "${MY_INSTALL_LOCATION}/lib" )
set_target_properties ( searchMapSoft  PROPERTIES INSTALL_RPATH_USE_LINK_PATH TRUE        )
set_property (
              	TARGET searchMapSoft
                PROPERTY INSTALL_RPATH
                             "${CLIPPER_LINK}"
                             "${SOFT_LINK}"
                             "${LAPACK_LINK}"
                             "${FFTW_LINK}"
                             "${CMAKE_SOURCE_DIR}/lib"
                             "${LIBGFORTRAN_LINK}"
                             "${RVAPI_LINK}"
)

##########################################################################################
################################### Install to install/searchMap
install                 ( TARGETS searchMapSoft DESTINATION ${CMAKE_SOURCE_DIR}/../install/searchMap )