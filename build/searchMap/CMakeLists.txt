##########################################################################################
##########################################################################################
############################## ProSHADE mapSearch cmake file #############################
##########################################################################################
##########################################################################################

##########################################################################################
################################### Link the executable
add_executable          ( searchMap ${SMAP_SRC} ${OBJS}                                   )
target_link_libraries   ( searchMap clipper-ccp4 clipper-cif clipper-minimol clipper-mmdb clipper-cns clipper-phs clipper-contrib clipper-core ccp4c mmdb2 )
target_link_libraries   ( searchMap fftw3                                                 )
target_link_libraries   ( searchMap lapacke lapack blas                                   )
target_link_libraries   ( searchMap gfortran                                              )
target_link_libraries   ( searchMap soft1                                                 )
target_link_libraries   ( searchMap rvapi                                                 )

##########################################################################################
################################### Set RPATH for the installed executable
set_property (
              	TARGET searchMap
                PROPERTY INSTALL_RPATH
                        "${STDCPPLIB_PATH}"
                        "${CLIPPER_LINK}"
                        "${FFTW_LINK}"
 						"${SOFT_LINK}"
						"${LAPACK_LINK}"
						"${LIBGFORTRAN_LINK}"
						"${RVAPI_LINK}"
)

##########################################################################################
################################### Install to install/searchMap
install                 ( TARGETS searchMap DESTINATION ${CMAKE_SOURCE_DIR}/../install/searchMap )
