
#!/bin/bash
###########################################################################
############################################## ProSHADE installation script
###########################################################################

########################## General settings
## CMake path
CMAKE="/Users/mysak/Repositories/cmake/cmake-3.11.0/bin/cmake"
## CMakeLists.txt path
CMAKE_LISTS="/Users/mysak/LMB/proshade/trunk/build"
## No cores for make
NOCORES=12
## Verbose
VRBS=0

########################## Path to libgfortran.dylib (or .so) containing folder
LIBGFORTRAN="/usr/local/lib/x86_64"

########################## Path to standard c++ libraries (this is not needed unless you have multiple gcc installs)
STDCPPLIB="/usr/lib"

########################## Path to clipper.h, libclipper-core.* and cmaplib.h files containing folder
CLIPPER_INC="/Users/mysak/Repositories/CCP4/devtoolsCCP4/install/include"
CLIPPER_LIB="/Users/mysak/Repositories/CCP4/devtoolsCCP4/install/lib"
CCP4CMAP_INC="/Users/mysak/Repositories/CCP4/devtoolsCCP4/install/include/ccp4"

########################## Path to folder containing lapacke.dylib and lapacke.h file
## NOTE: This needs to be installed with the shared libraries enabled; use 'cmake -DBUILD_SHARED_LIBS=ON -DLAPACKE=ON' when installing
LAPACKE_LIB="/Users/mysak/Repositories/lapack-3.8.0/install/lib"
LAPACKE_INC="/Users/mysak/Repositories/lapack-3.8.0/install/include"

########################## Path to the fftw3.h and libfftw3.a files containing folders
FFTW3_INC="$CMAKE_LISTS/../dependencies/fftw-3.3.7/include"
FFTW3_LIB="$CMAKE_LISTS/../dependencies/fftw-3.3.7/lib"

########################## Path to folder containing fftw3.h file
## Note1: This line is not necessary, as SOFT is now attached to the ProSHADE installation and will be linked automatically. The use of this line is only for advanced users who want to install particullarly on their system.
## NOTE2: If installing manually, this needs to be installed with the -fPID flag being manually added to the Makefile (on line 40)
SOFT_LIB="$CMAKE_LISTS/../dependencies/soft-2.0/lib"
SOFT_INC="$CMAKE_LISTS/../dependencies/soft-2.0/include"

########################## Path to folder containing liblapacke.dylib file
## Note1: This line is not necessary, as RVAPI is now attached to the ProSHADE installation and will be linked automatically. The use of this line is only for advanced users who want to install particullarly on their system.
RVAPI_LIB="$CMAKE_LISTS/../dependencies/rvapi"
RVAPI_INC="$CMAKE_LISTS/../dependencies/rvapi/src"

########################## Where should the binary, library and include be installed?
## You can comment out this line to make the installation local to the proshade folder.
INSTALL_PREFIX="$CMAKE_LISTS/../install"

########################## Where should the python (v2) module be installed?
## You can comment out this line to make the installation use the python executable module path.
## If you supply this path, please make sure the folder does exist
PYTHON2_MODULES="$CMAKE_LISTS/../install/python"

########################## Where should the python3 module be installed?
## You can comment out this line to make the installation use the python3 executable module path.
## If you supply this path, please make sure the folder does exist
PYTHON3_MODULES="$CMAKE_LISTS/../install/python3"

########################## Install FFTW3
## Leave this alone for automatic soft installation, remove if you have installed soft manually and are happy supplying the paths yourself
if [[ -e "$CMAKE_LISTS/../dependencies/fftw-3.3.7/lib/libfftw3.a" ]]; then
    echo ""
else
    if [[ "$1" == "remove" ]]; then
        echo ""
    else
        eval "CURPWD=$(pwd)"
        eval "cd $CMAKE_LISTS/../dependencies/fftw-3.3.7; ./configure --enable-shared --prefix \"$CMAKE_LISTS/../dependencies/fftw-3.3.7/\"; make; make install"
        eval "cd $CURPWD"
    fi
fi

########################## Install soft
## Leave this alone for automatic soft installation, remove if you have installed soft manually and are happy supplying the paths yourself
if [[ -e "$CMAKE_LISTS/../dependencies/soft-2.0/lib/libsoft1.a" ]]; then
    echo ""
else
    if [[ "$1" == "remove" ]]; then
        echo ""
    else
        sh $CMAKE_LISTS/../dependencies/configure-soft.sh $FFTW3_INC $FFTW3_LIB $CMAKE_LISTS/../dependencies
    fi
fi

########################## Install RVAPI
## Leave this alone for automatic rvapi installation, remove if you have installed soft manually and are happy supplying the paths yourself
if [[ -e "$CMAKE_LISTS/../dependencies/rvapi/librvapi.dylib" || -e "$CMAKE_LISTS/../dependencies/rvapi/librvapi.so" ]]; then
    echo ""
else
    if [[ "$1" == "remove" ]]; then
        echo ""
    else
        sh $CMAKE_LISTS/../dependencies/configure-rvapi.sh $CMAKE_LISTS/../dependencies/rvapi $CMAKE
    fi
fi

########################## Using cmake to produce Makefiles
$CMAKE $CMAKE_LISTS  -DCCPEM_PYTHON_BUILD=OFF \
                     -DSTDCPPLIB_PATH="$STDCPPLIB" \
                     -DLIBGFORTRAN_PATH="$LIBGFORTRAN" \
                     -DCLIPPER_INC="$CLIPPER_INC" \
                     -DCLIPPER_LIB="$CLIPPER_LIB" \
                     -DCCP4_CMAP_INC="$CCP4CMAP_INC" \
                     -DFFTW3_INC_PATH="$FFTW3_INC" \
                     -DFFTW3_LIB_PATH="$FFTW3_LIB" \
                     -DSOFT_LIB="$SOFT_LIB" \
                     -DSOFT_INC="$SOFT_INC" \
                     -DRVAPI_LIB="$RVAPI_LIB" \
                     -DRVAPI_INC="$RVAPI_INC" \
                     -DLAPACKE_LIB="$LAPACKE_LIB" \
                     -DLAPACKE_INC="$LAPACKE_INC" \
                     -DCMAKE_INSTALL_PREFIX="$INSTALL_PREFIX" 
#                    -DMY_PYTHON2_INSTALL_PATH="$PYTHON2_MODULES" \
#                     -DMY_PYTHON3_INSTALL_PATH="$PYTHON3_MODULES"

########################## If so requested, remove and exit now
if [[ "$1" == "remove" ]]; then
    make remove
    exit
fi

########################## Compile and link
if [[ "$VRBS" == "1" ]]; then
    make VERBOSE=1 -j$NOCORES
else
    make -j$NOCORES
fi

########################## Install (sudo may be needed for system installation)
make install

########################## If so requested, exit now
if [[ "$1" == "build-only" ]]; then
exit
fi

########################## Test installation (you may want to skip this, it takes several minutes)
make testProshade

########################## This will remove all CMake files, including the Makefile (no more targets will be available), but will keep the targets and installed files intact.
make clear
