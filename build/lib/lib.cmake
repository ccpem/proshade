##########################################################################################
##########################################################################################
################################# ProSHADE lib cmake file ################################
##########################################################################################
##########################################################################################

##########################################################################################
################################### Find the sources
file                    ( GLOB SOURCES "${CMAKE_SOURCE_DIR}/../src/proshade/*.cpp" )

##########################################################################################
################################### Link the dynamic lib
add_library             ( ${PROJECT_NAME}1 SHARED ${SOURCES}                              )
target_link_libraries   ( ${PROJECT_NAME}1 clipper-ccp4 clipper-cif clipper-minimol clipper-mmdb clipper-cns clipper-phs clipper-contrib clipper-core ccp4c mmdb2 )
target_link_libraries   ( ${PROJECT_NAME}1 fftw3                                          )
target_link_libraries   ( ${PROJECT_NAME}1 lapacke lapack blas                            )
target_link_libraries   ( ${PROJECT_NAME}1 gfortran                                       )
target_link_libraries   ( ${PROJECT_NAME}1 soft1                                          )
target_link_libraries   ( ${PROJECT_NAME}1 rvapi                                          )

##########################################################################################
################################### Deal with RPATH
if (APPLE) 
   set_target_properties ( ${PROJECT_NAME}1   PROPERTIES MACOSX_RPATH TRUE                )
   set_target_properties ( ${PROJECT_NAME}1   PROPERTIES CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

endif()

set_target_properties ( ${PROJECT_NAME}1 PROPERTIES INSTALL_RPATH "${MY_INSTALL_LOCATION}/lib")
set_target_properties ( ${PROJECT_NAME}1 PROPERTIES INSTALL_RPATH_USE_LINK_PATH TRUE      )
set_property (
            	TARGET ${PROJECT_NAME}1
                PROPERTY INSTALL_RPATH
                         "${STDCPPLIB_PATH}"
                	     "${CLIPPER_LINK}"
                     	 "${SOFT_LINK}"
                	     "${LAPACK_LINK}"
			             "${CMAKE_SOURCE_DIR}/lib"
			             "${LIBGFORTRAN_LINK}"
			             "${RVAPI_LINK}"
)

##########################################################################################
################################### Install to lib
set_target_properties   ( ${PROJECT_NAME}1 PROPERTIES OUTPUT_NAME "${PROJECT_NAME}"       )
install                 ( TARGETS ${PROJECT_NAME}1 DESTINATION ${MY_INSTALL_LOCATION}/lib ) 
install                 ( FILES   ${CMAKE_SOURCE_DIR}/../src/proshade/ProSHADE.h DESTINATION ${MY_INSTALL_LOCATION}/include )
install                 ( TARGETS ${PROJECT_NAME}1 DESTINATION ${CMAKE_SOURCE_DIR}/../install/lib ) 
install                 ( FILES   ${CMAKE_SOURCE_DIR}/../src/proshade/ProSHADE.h DESTINATION ${CMAKE_SOURCE_DIR}/../install/include ) 