##########################################################################################
##########################################################################################
################################# ProSHADE bin cmake file ################################
##########################################################################################
##########################################################################################

##########################################################################################
################################### Find the sources
file                    ( GLOB SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/../src/proshade/*.cpp")
file                    ( GLOB EXEC_SRC "${CMAKE_CURRENT_SOURCE_DIR}/../src/bin/*.cpp"	  )

##########################################################################################
################################### Link the executable
add_executable          ( ${PROJECT_NAME} ${EXEC_SRC} ${SOURCES}                          )
target_link_libraries   ( ${PROJECT_NAME} clipper-ccp4 clipper-cif clipper-minimol clipper-mmdb clipper-cns clipper-phs clipper-contrib clipper-core ccp4c mmdb2 )
target_link_libraries   ( ${PROJECT_NAME} fftw3                                           )
target_link_libraries   ( ${PROJECT_NAME} lapacke lapack blas                             )
target_link_libraries   ( ${PROJECT_NAME} gfortran                                        )
target_link_libraries   ( ${PROJECT_NAME} soft1                                           )
target_link_libraries   ( ${PROJECT_NAME} rvapi                                           )

##########################################################################################
################################### Set RPATH for the installed executable
set_property (
              	TARGET ${PROJECT_NAME}
                PROPERTY INSTALL_RPATH
                        "${STDCPPLIB_PATH}"                
                        "${CLIPPER_LINK}"
                        "${FFTW_LINK}"
 						"${SOFT_LINK}"
						"${LAPACK_LINK}"
						"${LIBGFORTRAN_LINK}"
						"${RVAPI_LINK}"
)

##########################################################################################
################################### Install to bin
install                 ( TARGETS ${PROJECT_NAME} DESTINATION ${MY_INSTALL_LOCATION}/bin  )
install                 ( FILES ${MY_INSTALL_LOCATION}/bin/${PROJECT_NAME} DESTINATION ${CMAKE_SOURCE_DIR}/bin PERMISSIONS OWNER_EXECUTE OWNER_WRITE OWNER_READ )
