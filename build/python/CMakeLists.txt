##########################################################################################
##########################################################################################
############################## ProSHADE python2 cmake file ###############################
##########################################################################################
##########################################################################################

##########################################################################################
################################### Copy header to python interface file
execute_process (
    				COMMAND bash -c "cp ${CMAKE_SOURCE_DIR}/../src/proshade/ProSHADE.h ${CMAKE_SOURCE_DIR}/python/ProSHADE.h"
				)
execute_process (
    				COMMAND bash -c "cp ${CMAKE_SOURCE_DIR}/../src/proshade/ProSHADE.h ${CMAKE_SOURCE_DIR}/../src/python/ProSHADE.h"
				)
execute_process (
    				COMMAND bash -c "cp ${CMAKE_SOURCE_DIR}/../src/proshade/ProSHADE.h ${CMAKE_BINARY_DIR}/python/ProSHADE.h"
				)

##########################################################################################
################################### Get packages
find_package         ( SWIG REQUIRED                                                      )
find_package         ( PythonLibs 2                                                       )

##########################################################################################
################################### Find python version and path
execute_process (
    				COMMAND bash -c "which python" OUTPUT_VARIABLE REAL_PYTHON
				)

execute_process (
    				COMMAND bash -c "python ${CMAKE_SOURCE_DIR}/python/getVersion.py" OUTPUT_VARIABLE PYTHON_VR
				)

##########################################################################################
################################### Include files
include      ( ${SWIG_USE_FILE}                                                          )      

message ( STATUS "Detected python at " ${REAL_PYTHON} "-- ... The proshade python module will work solely for this python version." )
execute_process (
    				COMMAND python -c "import sysconfig as sysconfig; print(sysconfig.get_path(scheme='posix_prefix',name='include'))" OUTPUT_VARIABLE REAL_PYTHON_INC
                )
execute_process (
					COMMAND python -c "import distutils.sysconfig as sysconfig; print(sysconfig.get_config_var('LIBDIR'))" OUTPUT_VARIABLE REAL_PYTHON_LIB
                )
include_directories  ( ${REAL_PYTHON_INC}                                                 )
string ( REGEX REPLACE "\n$" "" REAL_PYTHON_LIB "${REAL_PYTHON_LIB}"                      )
if     ( ${PYTHON_VR} EQUAL 2 )
	string ( CONCAT REAL_PYTHON_LIB ${REAL_PYTHON_LIB} "/libpython2.7.dylib"              )
else   ( ${PYTHON_VR} EQUAL 2 )
	message ( FATAL_ERROR "Your 'python' command defaults to python3. ProSHADE install assumes that the 'python' command is python version 2 and that python version 3 is under command 'python3'. Please use aliases to make your shell so and then re-run the install. After the install, your shell settings do not matter." )
endif ( )
set ( PYTHON_LIBRARIES ${REAL_PYTHON_LIB} )
set ( PYTHON_INCLUDE   ${REAL_PYTHON_INC} )

##########################################################################################
################################### Make sure proshade.i is compiles as C++
set_source_files_properties ( ${CMAKE_SOURCE_DIR}/../src/python/proshade.i PROPERTIES CPLUSPLUS ON )

##########################################################################################
################################### Create target using SWIG

if     ( ${CMAKE_MINOR_VERSION} LESS 8 )
	swig_add_module ( ${PROJECT_NAME}_python python ${CMAKE_SOURCE_DIR}/../src/python/proshade.i )
else   ( ${CMAKE_MINOR_VERSION} LESS 8 )
	swig_add_library     ( ${PROJECT_NAME}_python LANGUAGE python SOURCES ${CMAKE_SOURCE_DIR}/../src/python/proshade.i )
endif  ( ${CMAKE_MINOR_VERSION} LESS 8 )

include_directories ( ${CMAKE_SOURCE_DIR}/../src/python                                   )

##########################################################################################
################################### Link the library to the target
if     ( APPLE )
	swig_link_libraries  ( ${PROJECT_NAME}_python ${PROJECT_NAME}1 ${PYTHON_LIBRARIES}    )
else   ( APPLE )
	swig_link_libraries  ( ${PROJECT_NAME}_python ${PROJECT_NAME}1                        )
endif  ( APPLE ) 

##########################################################################################
################################### Deal with RPATH for module
if     ( APPLE ) 
	set_target_properties ( _${PROJECT_NAME}_python PROPERTIES MACOSX_RPATH TRUE          )
    set_target_properties ( _${PROJECT_NAME}_python PROPERTIES CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE )
    set_property (
            		TARGET _${PROJECT_NAME}_python
                	PROPERTY INSTALL_RPATH
                             "${STDCPPLIB_PATH}"
              		       	 "${CLIPPER_LINK}"
              		       	 "${FFTW_LINK}"
	                     	 "${SOFT_LINK}"
    	                 	 "${LAPACK_LINK}"
			             	 "${MY_INSTALL_LOCATION}/lib"
		    	             "${LIBGFORTRAN_LINK}"
		    	             "${RVAPI_LINK}"
	)

else   ( APPLE )
	set_target_properties ( _${PROJECT_NAME}_python PROPERTIES INSTALL_RPATH_USE_LINK_PATH TRUE )
	set_property (
            		TARGET _${PROJECT_NAME}_python
                	PROPERTY INSTALL_RPATH
                             "${STDCPPLIB_PATH}"
	                     	 "${CLIPPER_LINK}"
	                     	 "${FFTW_LINK}"
    	                 	 "${SOFT_LINK}"
        	             	 "${LAPACK_LINK}"
		    	         	 "${MY_INSTALL_LOCATION}/lib"
		        	         "${LIBGFORTRAN_LINK}"
		        	         "${RVAPI_LINK}"
	)

endif  ( APPLE )


##########################################################################################
################################### Rename the output module to _proshade
set_target_properties    ( _${PROJECT_NAME}_python PROPERTIES OUTPUT_NAME "_${PROJECT_NAME}")

##########################################################################################
################################### Install to python variable
if     ( EXISTS ${MY_PYTHON2_INSTALL_PATH} )
	install                  ( TARGETS _${PROJECT_NAME}_python                         DESTINATION ${MY_PYTHON2_INSTALL_PATH} ) 
	install                  ( FILES    ${CMAKE_BINARY_DIR}/python/${PROJECT_NAME}.py  DESTINATION ${MY_PYTHON2_INSTALL_PATH} )
else   ( EXISTS ${MY_PYTHON2_INSTALL_PATH} )
	install                  ( TARGETS _${PROJECT_NAME}_python                         DESTINATION ${INTERNAL_PYTHON2_INSTALL_PATH} ) 
	install                  ( FILES    ${CMAKE_BINARY_DIR}/python/${PROJECT_NAME}.py  DESTINATION ${INTERNAL_PYTHON2_INSTALL_PATH} )
endif  ( EXISTS ${MY_PYTHON2_INSTALL_PATH} )

install                      ( TARGETS _${PROJECT_NAME}_python                         DESTINATION ${CMAKE_SOURCE_DIR}/../install/python ) 
install                      ( FILES    ${CMAKE_BINARY_DIR}/python/${PROJECT_NAME}.py  DESTINATION ${CMAKE_SOURCE_DIR}/../install/python )